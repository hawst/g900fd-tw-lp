.class Lcom/google/android/music/net/NetworkMonitorServiceConnection$1;
.super Lcom/google/android/music/utils/SafeServiceConnection;
.source "NetworkMonitorServiceConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/net/NetworkMonitorServiceConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/net/NetworkMonitorServiceConnection;


# direct methods
.method constructor <init>(Lcom/google/android/music/net/NetworkMonitorServiceConnection;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection$1;->this$0:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-direct {p0}, Lcom/google/android/music/utils/SafeServiceConnection;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 40
    iget-object v3, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection$1;->this$0:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    monitor-enter v3

    .line 41
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection$1;->this$0:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-static {p2}, Lcom/google/android/music/net/INetworkMonitor$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/net/INetworkMonitor;

    move-result-object v4

    # setter for: Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;
    invoke-static {v2, v4}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->access$002(Lcom/google/android/music/net/NetworkMonitorServiceConnection;Lcom/google/android/music/net/INetworkMonitor;)Lcom/google/android/music/net/INetworkMonitor;

    .line 42
    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection$1;->this$0:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v2}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->registerListeners()V

    .line 44
    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection$1;->this$0:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    # getter for: Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mRunOnServiceConnected:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->access$100(Lcom/google/android/music/net/NetworkMonitorServiceConnection;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 45
    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection$1;->this$0:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    # getter for: Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mRunOnServiceConnected:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->access$100(Lcom/google/android/music/net/NetworkMonitorServiceConnection;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 46
    .local v1, "run":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 51
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "run":Ljava/lang/Runnable;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 49
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection$1;->this$0:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    const/4 v4, 0x0

    # setter for: Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mRunOnServiceConnected:Ljava/util/List;
    invoke-static {v2, v4}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->access$102(Lcom/google/android/music/net/NetworkMonitorServiceConnection;Ljava/util/List;)Ljava/util/List;

    .line 50
    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection$1;->this$0:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 51
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 55
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection$1;->this$0:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    monitor-enter v1

    .line 56
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection$1;->this$0:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;
    invoke-static {v0, v2}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->access$002(Lcom/google/android/music/net/NetworkMonitorServiceConnection;Lcom/google/android/music/net/INetworkMonitor;)Lcom/google/android/music/net/INetworkMonitor;

    .line 57
    monitor-exit v1

    .line 58
    return-void

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
