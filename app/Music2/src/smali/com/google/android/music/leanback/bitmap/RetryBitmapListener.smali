.class Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;
.super Ljava/lang/Object;
.source "RetryBitmapListener.java"

# interfaces
.implements Lcom/google/android/music/leanback/bitmap/BitmapListener;
.implements Ljava/lang/Runnable;


# instance fields
.field private mAsyncRunner:Lcom/google/android/music/utils/async/AsyncRunner;

.field private final mBitmapListener:Lcom/google/android/music/leanback/bitmap/BitmapListener;

.field private final mHandler:Landroid/os/Handler;

.field private mRetryTimeout:J


# direct methods
.method constructor <init>(Landroid/os/Handler;Lcom/google/android/music/leanback/bitmap/BitmapListener;)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "bitmapListener"    # Lcom/google/android/music/leanback/bitmap/BitmapListener;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const-wide/16 v0, 0x32

    iput-wide v0, p0, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;->mRetryTimeout:J

    .line 19
    iput-object p1, p0, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;->mHandler:Landroid/os/Handler;

    .line 20
    iput-object p2, p0, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;->mBitmapListener:Lcom/google/android/music/leanback/bitmap/BitmapListener;

    .line 21
    return-void
.end method


# virtual methods
.method public onBitmapLoaded(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 29
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;->mAsyncRunner:Lcom/google/android/music/utils/async/AsyncRunner;

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;->mHandler:Landroid/os/Handler;

    iget-wide v2, p0, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;->mRetryTimeout:J

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 31
    iget-wide v0, p0, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;->mRetryTimeout:J

    iget-wide v2, p0, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;->mRetryTimeout:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;->mRetryTimeout:J

    .line 35
    :goto_0
    return-void

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;->mBitmapListener:Lcom/google/android/music/leanback/bitmap/BitmapListener;

    invoke-interface {v0, p1}, Lcom/google/android/music/leanback/bitmap/BitmapListener;->onBitmapLoaded(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;->mAsyncRunner:Lcom/google/android/music/utils/async/AsyncRunner;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 40
    return-void
.end method

.method setAsyncRunner(Lcom/google/android/music/utils/async/AsyncRunner;)V
    .locals 0
    .param p1, "asyncRunner"    # Lcom/google/android/music/utils/async/AsyncRunner;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;->mAsyncRunner:Lcom/google/android/music/utils/async/AsyncRunner;

    .line 25
    return-void
.end method
