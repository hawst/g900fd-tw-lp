.class Lcom/google/android/music/playback/LocalDevicePlayback$27;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->open(Lcom/google/android/music/download/ContentIdentifier;ZZJLcom/google/android/music/store/MusicFile;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

.field final synthetic val$playOnSuccess:Z

.field final synthetic val$songId:Lcom/google/android/music/download/ContentIdentifier;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/ContentIdentifier;Z)V
    .locals 0

    .prologue
    .line 3585
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$27;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iput-object p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$27;->val$songId:Lcom/google/android/music/download/ContentIdentifier;

    iput-boolean p3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$27;->val$playOnSuccess:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Z)V
    .locals 3
    .param p1, "skipToNext"    # Z

    .prologue
    .line 3589
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to open MusicId ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$27;->val$songId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") for playback"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 3590
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$27;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # operator++ for: Lcom/google/android/music/playback/LocalDevicePlayback;->mOpenFailedCounter:I
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7808(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    .line 3591
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$27;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mOpenFailedCounter:I
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 3592
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$27;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->tryNext()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9500(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 3596
    :goto_0
    return-void

    .line 3594
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$27;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->onPlaybackFailure()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9600(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    goto :goto_0
.end method

.method public onSuccess()V
    .locals 2

    .prologue
    .line 3600
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$27;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mOpenFailedCounter:I
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 3601
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$27;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->isStreaming()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3602
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$27;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onOpenComplete()V

    .line 3604
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$27;->val$playOnSuccess:Z

    if-eqz v0, :cond_1

    .line 3607
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$27;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->play()V

    .line 3609
    :cond_1
    return-void
.end method
