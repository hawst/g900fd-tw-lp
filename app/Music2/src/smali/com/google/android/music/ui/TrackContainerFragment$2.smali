.class Lcom/google/android/music/ui/TrackContainerFragment$2;
.super Ljava/lang/Object;
.source "TrackContainerFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/TrackContainerFragment;->extractIds()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mSavedAlbumId:J

.field private mSavedAlbumMetajamId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/ui/TrackContainerFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/TrackContainerFragment;)V
    .locals 2

    .prologue
    .line 282
    iput-object p1, p0, Lcom/google/android/music/ui/TrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/ui/TrackContainerFragment$2;->mSavedAlbumId:J

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 4

    .prologue
    .line 287
    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/TrackContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 288
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 294
    :goto_0
    return-void

    .line 290
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    iget-object v1, v1, Lcom/google/android/music/ui/TrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v1, v1, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    if-eqz v1, :cond_1

    .line 291
    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    iget-object v1, v1, Lcom/google/android/music/ui/TrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v1, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    invoke-virtual {v1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getNautilusId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment$2;->mSavedAlbumMetajamId:Ljava/lang/String;

    .line 293
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    iget-object v1, v1, Lcom/google/android/music/ui/TrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v1, v0}, Lcom/google/android/music/medialist/SongList;->getAlbumId(Landroid/content/Context;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/ui/TrackContainerFragment$2;->mSavedAlbumId:J

    goto :goto_0
.end method

.method public taskCompleted()V
    .locals 4

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    iget-wide v2, p0, Lcom/google/android/music/ui/TrackContainerFragment$2;->mSavedAlbumId:J

    iput-wide v2, v0, Lcom/google/android/music/ui/TrackContainerFragment;->mAlbumId:J

    .line 298
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment$2;->mSavedAlbumMetajamId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/ui/TrackContainerFragment;->mAlbumMetajamId:Ljava/lang/String;

    .line 301
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    iget-object v0, v0, Lcom/google/android/music/ui/TrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v0}, Lcom/google/android/music/medialist/SongList;->supportsVideoCluster()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/TrackContainerFragment;->initVideoCluster()V

    .line 305
    :cond_0
    return-void
.end method
