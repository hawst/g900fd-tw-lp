.class public Lcom/google/android/music/ui/GenreAlbumGridFragment;
.super Lcom/google/android/music/ui/AlbumGridFragment;
.source "GenreAlbumGridFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;
    }
.end annotation


# instance fields
.field private mAllSongs:Lcom/google/android/music/ui/AllSongsView;

.field private mGenreId:J

.field private mGenreName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/music/ui/AlbumGridFragment;-><init>()V

    .line 95
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/GenreAlbumGridFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/GenreAlbumGridFragment;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment;->mGenreName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/music/ui/GenreAlbumGridFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/GenreAlbumGridFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment;->mGenreName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/GenreAlbumGridFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/ui/GenreAlbumGridFragment;

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment;->mGenreId:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/GenreAlbumGridFragment;)Lcom/google/android/music/ui/AllSongsView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/GenreAlbumGridFragment;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment;->mAllSongs:Lcom/google/android/music/ui/AllSongsView;

    return-object v0
.end method

.method public static newInstance(Lcom/google/android/music/medialist/GenreAlbumList;)Lcom/google/android/music/ui/GenreAlbumGridFragment;
    .locals 1
    .param p0, "medialist"    # Lcom/google/android/music/medialist/GenreAlbumList;

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/music/ui/GenreAlbumGridFragment;

    invoke-direct {v0}, Lcom/google/android/music/ui/GenreAlbumGridFragment;-><init>()V

    .line 36
    .local v0, "fragment":Lcom/google/android/music/ui/GenreAlbumGridFragment;
    invoke-virtual {v0, p0}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->init(Lcom/google/android/music/medialist/AlbumList;)V

    .line 37
    invoke-virtual {v0}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->saveMediaListAsArguments()V

    .line 38
    return-object v0
.end method


# virtual methods
.method newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 92
    new-instance v0, Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;

    iget-object v1, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0, p0, p1, v1}, Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;-><init>(Lcom/google/android/music/ui/GenreAlbumGridFragment;Lcom/google/android/music/ui/AlbumGridFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    return-object v0
.end method

.method newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;

    iget-object v1, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;-><init>(Lcom/google/android/music/ui/GenreAlbumGridFragment;Lcom/google/android/music/ui/AlbumGridFragment;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    return-object v0
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment;->mAllSongs:Lcom/google/android/music/ui/AllSongsView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/AllSongsView;->show()V

    .line 82
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/AlbumGridFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    .line 83
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 28
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 43
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 44
    .local v1, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    .line 45
    .local v2, "lv":Landroid/widget/ListView;
    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    .line 46
    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    .line 49
    const v3, 0x7f04001a

    invoke-virtual {v1, v3, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/ui/AllSongsView;

    iput-object v3, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment;->mAllSongs:Lcom/google/android/music/ui/AllSongsView;

    .line 50
    iget-object v3, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment;->mAllSongs:Lcom/google/android/music/ui/AllSongsView;

    invoke-virtual {v3}, Lcom/google/android/music/ui/AllSongsView;->hide()V

    .line 51
    iget-object v3, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment;->mAllSongs:Lcom/google/android/music/ui/AllSongsView;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->getMediaList()Lcom/google/android/music/medialist/MediaList;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/GenreAlbumList;

    .line 54
    .local v0, "genreList":Lcom/google/android/music/medialist/GenreAlbumList;
    invoke-virtual {v0}, Lcom/google/android/music/medialist/GenreAlbumList;->getGenreId()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment;->mGenreId:J

    .line 56
    new-instance v3, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;-><init>(Lcom/google/android/music/ui/GenreAlbumGridFragment;Lcom/google/android/music/medialist/GenreAlbumList;)V

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 74
    return-void
.end method
