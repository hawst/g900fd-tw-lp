.class Lcom/google/android/music/leanback/LeanbackPlayActivity$3;
.super Landroid/content/BroadcastReceiver;
.source "LeanbackPlayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$3;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v4, 0x1

    .line 208
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$100()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$200()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive: action="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_0
    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 215
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$3;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->updateTrackInfo(Landroid/content/Intent;)V
    invoke-static {v1, p2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$800(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/content/Intent;)V

    .line 216
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$3;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->queueNextRefresh(J)V
    invoke-static {v1, v4, v5}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$600(Lcom/google/android/music/leanback/LeanbackPlayActivity;J)V

    .line 239
    :cond_1
    :goto_0
    return-void

    .line 217
    :cond_2
    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 219
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$3;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->queueNextRefresh(J)V
    invoke-static {v1, v4, v5}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$600(Lcom/google/android/music/leanback/LeanbackPlayActivity;J)V

    goto :goto_0

    .line 220
    :cond_3
    const-string v1, "com.android.music.asyncopencomplete"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "com.android.music.asyncopenstart"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "com.android.music.playbackfailed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 224
    :cond_4
    const-string v1, "com.android.music.asyncopenstart"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 225
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$3;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$900(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 233
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$3;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->updateTrackInfo(Landroid/content/Intent;)V
    invoke-static {v1, p2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$800(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/content/Intent;)V

    goto :goto_0

    .line 226
    :cond_5
    const-string v1, "playing"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 227
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$3;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->queueNextRefresh(J)V
    invoke-static {v1, v4, v5}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$600(Lcom/google/android/music/leanback/LeanbackPlayActivity;J)V

    goto :goto_1

    .line 230
    :cond_6
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$3;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->refreshNow()J
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$500(Lcom/google/android/music/leanback/LeanbackPlayActivity;)J

    goto :goto_1

    .line 234
    :cond_7
    const-string v1, "com.google.android.music.mix.playbackmodechanged"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 235
    const-string v1, "com.android.music.queuechanged"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 236
    const-string v1, "com.google.android.music.refreshcomplete"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 237
    const-string v1, "com.google.android.music.refreshfailed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0
.end method
