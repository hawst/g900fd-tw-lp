.class final Lcom/google/android/music/ui/DownloadContainerFragment$onDownloadButtonClickListener;
.super Ljava/lang/Object;
.source "DownloadContainerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/DownloadContainerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "onDownloadButtonClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/DownloadContainerFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/DownloadContainerFragment;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/music/ui/DownloadContainerFragment$onDownloadButtonClickListener;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 110
    iget-object v1, p0, Lcom/google/android/music/ui/DownloadContainerFragment$onDownloadButtonClickListener;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/DownloadContainerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isKeepOnDownloadingPaused()Z

    move-result v0

    .line 111
    .local v0, "isDownloadPaused":Z
    iget-object v1, p0, Lcom/google/android/music/ui/DownloadContainerFragment$onDownloadButtonClickListener;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/DownloadContainerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setKeepOnDownloadPaused(Z)V

    .line 112
    iget-object v1, p0, Lcom/google/android/music/ui/DownloadContainerFragment$onDownloadButtonClickListener;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    # invokes: Lcom/google/android/music/ui/DownloadContainerFragment;->updateHeaderButton()V
    invoke-static {v1}, Lcom/google/android/music/ui/DownloadContainerFragment;->access$000(Lcom/google/android/music/ui/DownloadContainerFragment;)V

    .line 113
    return-void

    .line 111
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
