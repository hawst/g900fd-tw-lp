.class public final Lcom/google/android/music/wear/WearBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WearBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static isWearSyncGserviceEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "appContext"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_wear_sync_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static refreshCapabilities(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-static {p0}, Lcom/google/android/music/wear/WearBroadcastReceiver;->isWearSyncGserviceEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 36
    :goto_0
    return-void

    .line 34
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/wear/NodeCapabilityRefresher;->createFromContext(Landroid/content/Context;)Lcom/google/android/music/wear/NodeCapabilityRefresher;

    move-result-object v0

    .line 35
    .local v0, "refresher":Lcom/google/android/music/wear/NodeCapabilityRefresher;
    invoke-virtual {v0}, Lcom/google/android/music/wear/NodeCapabilityRefresher;->refreshNodeCapabilities()V

    goto :goto_0
.end method

.method public static startWearSyncService(Landroid/content/Context;)V
    .locals 2
    .param p0, "appContext"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-static {p0}, Lcom/google/android/music/wear/WearBroadcastReceiver;->isWearSyncGserviceEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/wear/WearMetadataSyncService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 44
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 46
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 21
    const-string v0, "WearBroadcastReceiver"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    const-string v0, "WearBroadcastReceiver"

    const-string v1, "Triggered Wear re-sync"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    :cond_0
    invoke-static {p1}, Lcom/google/android/music/wear/WearBroadcastReceiver;->startWearSyncService(Landroid/content/Context;)V

    .line 25
    invoke-static {p1}, Lcom/google/android/music/wear/WearBroadcastReceiver;->refreshCapabilities(Landroid/content/Context;)V

    .line 26
    return-void
.end method
