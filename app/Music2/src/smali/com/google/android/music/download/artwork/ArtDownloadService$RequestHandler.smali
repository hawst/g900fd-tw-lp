.class Lcom/google/android/music/download/artwork/ArtDownloadService$RequestHandler;
.super Landroid/os/Handler;
.source "ArtDownloadService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/artwork/ArtDownloadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RequestHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/artwork/ArtDownloadService;


# direct methods
.method public constructor <init>(Lcom/google/android/music/download/artwork/ArtDownloadService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 753
    iput-object p1, p0, Lcom/google/android/music/download/artwork/ArtDownloadService$RequestHandler;->this$0:Lcom/google/android/music/download/artwork/ArtDownloadService;

    .line 754
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 755
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 759
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 775
    new-instance v5, Ljava/lang/IllegalArgumentException;

    invoke-direct {v5}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v5

    .line 761
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    .line 762
    .local v0, "extras":Landroid/os/Bundle;
    const-string v5, "remoteUrls"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 763
    .local v3, "remoteUrls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v5, "prefetch"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 764
    .local v2, "isPrefetch":Z
    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 765
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 766
    .local v4, "url":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 767
    iget-object v5, p0, Lcom/google/android/music/download/artwork/ArtDownloadService$RequestHandler;->this$0:Lcom/google/android/music/download/artwork/ArtDownloadService;

    invoke-virtual {v5, v4, v2}, Lcom/google/android/music/download/artwork/ArtDownloadService;->queueArtRequest(Ljava/lang/String;Z)V

    goto :goto_0

    .line 771
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "url":Ljava/lang/String;
    :cond_1
    const-string v5, "ArtDownloadService"

    const-string v6, "Art request should have urls"

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    :cond_2
    return-void

    .line 759
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
