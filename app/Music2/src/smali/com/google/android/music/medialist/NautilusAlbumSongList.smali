.class public Lcom/google/android/music/medialist/NautilusAlbumSongList;
.super Lcom/google/android/music/medialist/NautilusSongList;
.source "NautilusAlbumSongList.java"


# instance fields
.field private mAlbumArtUrl:Ljava/lang/String;

.field private mAlbumName:Ljava/lang/String;

.field private mAlbumYear:I

.field private mArtistMetajamId:Ljava/lang/String;

.field private mArtistName:Ljava/lang/String;

.field private mDescription:Ljava/lang/String;

.field private mDescriptionAttribution:Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;

.field private mLocalAlbumId:J

.field private mMetadataPulled:Z

.field private final mNautilusAlbumId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "nautilusAlbumId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 42
    invoke-direct {p0}, Lcom/google/android/music/medialist/NautilusSongList;-><init>()V

    .line 36
    iput v2, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumYear:I

    .line 37
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mLocalAlbumId:J

    .line 40
    iput-boolean v2, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mMetadataPulled:Z

    .line 43
    iput-object p1, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mNautilusAlbumId:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "nautilusAlbumId"    # Ljava/lang/String;
    .param p2, "artistMetajamId"    # Ljava/lang/String;
    .param p3, "albumName"    # Ljava/lang/String;
    .param p4, "artistName"    # Ljava/lang/String;
    .param p5, "description"    # Ljava/lang/String;
    .param p6, "artUrl"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 54
    invoke-direct {p0}, Lcom/google/android/music/medialist/NautilusSongList;-><init>()V

    .line 36
    iput v2, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumYear:I

    .line 37
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mLocalAlbumId:J

    .line 40
    iput-boolean v2, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mMetadataPulled:Z

    .line 55
    invoke-static {p1}, Lcom/google/android/music/utils/StringUtils;->returnStringIfNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mNautilusAlbumId:Ljava/lang/String;

    .line 56
    invoke-static {p2}, Lcom/google/android/music/utils/StringUtils;->returnStringIfNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mArtistMetajamId:Ljava/lang/String;

    .line 57
    invoke-static {p3}, Lcom/google/android/music/utils/StringUtils;->returnStringIfNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumName:Ljava/lang/String;

    .line 58
    invoke-static {p4}, Lcom/google/android/music/utils/StringUtils;->returnStringIfNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mArtistName:Ljava/lang/String;

    .line 59
    invoke-static {p5}, Lcom/google/android/music/utils/StringUtils;->returnStringIfNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mDescription:Ljava/lang/String;

    .line 60
    invoke-static {p6}, Lcom/google/android/music/utils/StringUtils;->returnStringIfNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumArtUrl:Ljava/lang/String;

    .line 61
    return-void
.end method

.method private getNames(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 205
    iget-boolean v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mMetadataPulled:Z

    if-eqz v0, :cond_0

    .line 272
    :goto_0
    return-void

    .line 207
    :cond_0
    const/16 v0, 0xa

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "album_name"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "album_artist"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "album_art"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "album_year"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "ArtistMetajamId"

    aput-object v1, v2, v0

    const/4 v0, 0x5

    const-string v1, "album_description"

    aput-object v1, v2, v0

    const/4 v0, 0x6

    const-string v1, "album_description_attr_source_title"

    aput-object v1, v2, v0

    const/4 v0, 0x7

    const-string v1, "album_description_attr_source_url"

    aput-object v1, v2, v0

    const/16 v0, 0x8

    const-string v1, "album_description_attr_license_title"

    aput-object v1, v2, v0

    const/16 v0, 0x9

    const-string v1, "album_description_attr_license_url"

    aput-object v1, v2, v0

    .line 219
    .local v2, "cols":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mNautilusAlbumId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Albums;->getNautilusAlbumsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 222
    .local v9, "c":Landroid/database/Cursor;
    if-eqz v9, :cond_c

    .line 223
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 224
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 225
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumName:Ljava/lang/String;

    .line 227
    :cond_1
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 228
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mArtistName:Ljava/lang/String;

    .line 230
    :cond_2
    const/4 v0, 0x2

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 231
    const/4 v0, 0x2

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumArtUrl:Ljava/lang/String;

    .line 233
    :cond_3
    const/4 v0, 0x3

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 234
    const/4 v0, 0x3

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumYear:I

    .line 236
    :cond_4
    const/4 v0, 0x4

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 237
    const/4 v0, 0x4

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mArtistMetajamId:Ljava/lang/String;

    .line 239
    :cond_5
    const/4 v0, 0x5

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 240
    const/4 v0, 0x5

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mDescription:Ljava/lang/String;

    .line 243
    :cond_6
    const/4 v5, 0x0

    .line 244
    .local v5, "sourceTitle":Ljava/lang/String;
    const/4 v6, 0x0

    .line 245
    .local v6, "sourceUrl":Ljava/lang/String;
    const/4 v7, 0x0

    .line 246
    .local v7, "licenseTitle":Ljava/lang/String;
    const/4 v8, 0x0

    .line 247
    .local v8, "licenseUrl":Ljava/lang/String;
    const/4 v0, 0x6

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 248
    const/4 v0, 0x6

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 250
    :cond_7
    const/4 v0, 0x7

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 251
    const/4 v0, 0x7

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 253
    :cond_8
    const/16 v0, 0x8

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 254
    const/16 v0, 0x8

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 256
    :cond_9
    const/16 v0, 0x9

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_a

    .line 257
    const/16 v0, 0x9

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 259
    :cond_a
    new-instance v3, Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;-><init>(Lcom/google/android/music/medialist/MediaList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mDescriptionAttribution:Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;

    .line 262
    .end local v5    # "sourceTitle":Ljava/lang/String;
    .end local v6    # "sourceUrl":Ljava/lang/String;
    .end local v7    # "licenseTitle":Ljava/lang/String;
    .end local v8    # "licenseUrl":Ljava/lang/String;
    :cond_b
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 264
    :cond_c
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 265
    const v0, 0x7f0b00c5

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumName:Ljava/lang/String;

    .line 267
    :cond_d
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mArtistName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 268
    const v0, 0x7f0b00c4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mArtistName:Ljava/lang/String;

    .line 271
    :cond_e
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mMetadataPulled:Z

    goto/16 :goto_0
.end method


# virtual methods
.method public getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumArtUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 90
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getNames(Landroid/content/Context;)V

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumArtUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getAlbumArtist(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mArtistName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 165
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getNames(Landroid/content/Context;)V

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mArtistName:Ljava/lang/String;

    return-object v0
.end method

.method public getAlbumId(Landroid/content/Context;)J
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v10, -0x1

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 104
    iget-wide v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mLocalAlbumId:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_3

    .line 110
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "album"

    aput-object v0, v2, v4

    const-string v0, "AlbumArtist"

    aput-object v0, v2, v5

    .line 114
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 115
    .local v6, "albumName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 116
    .local v7, "artistName":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 118
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 120
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 123
    :cond_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 126
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move-wide v0, v10

    .line 139
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "albumName":Ljava/lang/String;
    .end local v7    # "artistName":Ljava/lang/String;
    .end local v8    # "c":Landroid/database/Cursor;
    :goto_0
    return-wide v0

    .line 123
    .restart local v2    # "cols":[Ljava/lang/String;
    .restart local v6    # "albumName":Ljava/lang/String;
    .restart local v7    # "artistName":Ljava/lang/String;
    .restart local v8    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    .line 133
    :cond_2
    new-instance v9, Lcom/google/android/music/store/TagNormalizer;

    invoke-direct {v9}, Lcom/google/android/music/store/TagNormalizer;-><init>()V

    .line 134
    .local v9, "normalizer":Lcom/google/android/music/store/TagNormalizer;
    invoke-virtual {v9, v6}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 135
    invoke-virtual {v9, v7}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x1f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mLocalAlbumId:J

    .line 139
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "albumName":Ljava/lang/String;
    .end local v7    # "artistName":Ljava/lang/String;
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v9    # "normalizer":Lcom/google/android/music/store/TagNormalizer;
    :cond_3
    iget-wide v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mLocalAlbumId:J

    goto :goto_0
.end method

.method public getAlbumYear(Landroid/content/Context;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 171
    iget v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumYear:I

    if-nez v0, :cond_0

    .line 172
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getNames(Landroid/content/Context;)V

    .line 174
    :cond_0
    iget v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumYear:I

    return v0
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 65
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mNautilusAlbumId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mArtistMetajamId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mArtistName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mDescription:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumArtUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getArtistMetajamId(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mArtistMetajamId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 97
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getNames(Landroid/content/Context;)V

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mArtistMetajamId:Ljava/lang/String;

    return-object v0
.end method

.method public getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 370
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getAlbumId(Landroid/content/Context;)J

    move-result-wide v0

    .line 371
    .local v0, "albumId":J
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 372
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getNautilusId()Ljava/lang/String;

    move-result-object v2

    .line 373
    .local v2, "extId":Ljava/lang/String;
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor;->newNautuilusAlbumDescriptor(JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v4

    return-object v4
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mNautilusAlbumId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Albums;->getAudioInNautilusAlbumUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getDescription(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mDescription:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 150
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getNames(Landroid/content/Context;)V

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getDescriptionAttribution(Landroid/content/Context;)Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mDescriptionAttribution:Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;

    if-nez v0, :cond_0

    .line 158
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getNames(Landroid/content/Context;)V

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mDescriptionAttribution:Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;

    return-object v0
.end method

.method public getDownloadedSongCount(Landroid/content/Context;)I
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 349
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getAlbumId(Landroid/content/Context;)J

    move-result-wide v8

    .line 350
    .local v8, "localAlbumId":J
    const-wide/16 v4, -0x1

    cmp-long v1, v8, v4

    if-nez v1, :cond_0

    .line 364
    :goto_0
    return v0

    .line 351
    :cond_0
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "keeponDownloadedSongCount"

    aput-object v1, v2, v0

    .line 354
    .local v2, "cols":[Ljava/lang/String;
    invoke-static {v8, v9}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 357
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    .line 358
    :cond_1
    const-string v0, "NautilusSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown album id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 359
    const/4 v0, -0x1

    .line 364
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .line 361
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 364
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getKeepOnSongCount(Landroid/content/Context;)I
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 328
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getAlbumId(Landroid/content/Context;)J

    move-result-wide v8

    .line 329
    .local v8, "localAlbumId":J
    const-wide/16 v4, -0x1

    cmp-long v1, v8, v4

    if-nez v1, :cond_0

    .line 343
    :goto_0
    return v0

    .line 330
    :cond_0
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "keeponSongCount"

    aput-object v1, v2, v0

    .line 333
    .local v2, "cols":[Ljava/lang/String;
    invoke-static {v8, v9}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 336
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    .line 337
    :cond_1
    const-string v0, "NautilusSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown album id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 338
    const/4 v0, -0x1

    .line 343
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .line 340
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 343
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 77
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getNames(Landroid/content/Context;)V

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mAlbumName:Ljava/lang/String;

    return-object v0
.end method

.method public getNautilusId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mNautilusAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondaryName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getAlbumArtist(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasArtistArt()Z
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x1

    return v0
.end method

.method public hasDifferentTrackArtists(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 179
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "HasDifferentTrackArtists"

    aput-object v0, v2, v8

    .line 182
    .local v2, "cols":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mNautilusAlbumId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Albums;->getNautilusAlbumsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 186
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 187
    :cond_0
    const-string v0, "NautilusSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown album id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList;->mNautilusAlbumId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v8

    :goto_0
    return v0

    .line 190
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-ne v0, v7, :cond_2

    move v0, v7

    .line 193
    :goto_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    move v0, v8

    .line 190
    goto :goto_1

    .line 193
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/store/IStoreService;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "storeService"    # Lcom/google/android/music/store/IStoreService;

    .prologue
    const/4 v1, 0x0

    .line 316
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getAlbumId(Landroid/content/Context;)J

    move-result-wide v2

    .line 317
    .local v2, "localAlbumId":J
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_0

    .line 322
    :goto_0
    return v1

    .line 319
    :cond_0
    :try_start_0
    invoke-interface {p2, v2, v3}, Lcom/google/android/music/store/IStoreService;->isAlbumSelectedAsKeepOn(J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 320
    :catch_0
    move-exception v0

    .line 321
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "NautilusSongList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not get keep on status for album id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public shouldShowTrackNumbers()Z
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x1

    return v0
.end method

.method protected supportsOfflineCaching()Z
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x1

    return v0
.end method

.method public supportsVideoCluster()Z
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x1

    return v0
.end method

.method public toggleOfflineCaching(Landroid/content/Context;Lcom/google/android/music/activitymanagement/KeepOnManager;Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keepOnManager"    # Lcom/google/android/music/activitymanagement/KeepOnManager;
    .param p3, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p4, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;

    .prologue
    .line 298
    new-instance v0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;-><init>(Lcom/google/android/music/medialist/NautilusAlbumSongList;Landroid/content/Context;Lcom/google/android/music/activitymanagement/KeepOnManager;Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 312
    return-void
.end method
