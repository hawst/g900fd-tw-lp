.class Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;
.super Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState;
.source "MusicSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/sync/google/MusicSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AllFeedsDownstreamState"
.end annotation


# instance fields
.field mUseReducedFeedsFlag:Z


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 2
    .param p1, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 291
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState;-><init>(Lcom/google/android/music/sync/google/MusicSyncAdapter$1;)V

    .line 294
    const-string v0, "music_sync_config"

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    sget-object v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->CONFIG:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;->mFeed:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    .line 301
    :goto_0
    const-string v0, "music_unaccepted_user_reduce_sync"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;->mUseReducedFeedsFlag:Z

    .line 304
    return-void

    .line 298
    :cond_0
    sget-object v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->TRACKS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;->mFeed:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    goto :goto_0
.end method


# virtual methods
.method public onDoneWithFeed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 318
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isAcceptedUser()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isNautilusEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;->mUseReducedFeedsFlag:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 321
    .local v0, "useReducedFeeds":Z
    :goto_0
    sget-object v1, Lcom/google/android/music/sync/google/MusicSyncAdapter$2;->$SwitchMap$com$google$android$music$sync$google$MusicSyncAdapter$DownstreamState$Feed:[I

    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;->mFeed:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    invoke-virtual {v2}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 370
    :goto_1
    :pswitch_0
    return-void

    .line 318
    .end local v0    # "useReducedFeeds":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 324
    .restart local v0    # "useReducedFeeds":Z
    :pswitch_1
    sget-object v1, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->TRACKS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    iput-object v1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;->mFeed:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    goto :goto_1

    .line 328
    :pswitch_2
    if-eqz v0, :cond_1

    .line 330
    iput-object v3, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;->mFeed:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    goto :goto_1

    .line 333
    :cond_1
    sget-object v1, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->PLAYLISTS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    iput-object v1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;->mFeed:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    goto :goto_1

    .line 339
    :pswitch_3
    sget-object v1, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->PLENTRIES:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    iput-object v1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;->mFeed:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    goto :goto_1

    .line 344
    :pswitch_4
    sget-object v1, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->SOUND_SEARCH:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    iput-object v1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;->mFeed:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    goto :goto_1

    .line 351
    :pswitch_5
    sget-object v1, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->RADIO_STATIONS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    iput-object v1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;->mFeed:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    goto :goto_1

    .line 356
    :pswitch_6
    sget-object v1, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->BLACKLIST_ITEMS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    iput-object v1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;->mFeed:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    goto :goto_1

    .line 361
    :pswitch_7
    sget-object v1, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->CLOUD_QUEUE:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    iput-object v1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;->mFeed:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    goto :goto_1

    .line 366
    :pswitch_8
    iput-object v3, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;->mFeed:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    goto :goto_1

    .line 321
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
