.class public Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;
.super Ljava/lang/Object;
.source "ExploreDocumentClusterBuilder.java"


# static fields
.field public static final ALBUM_COLUMNS:[Ljava/lang/String;

.field public static final ARTIST_COLUMNS:[Ljava/lang/String;

.field public static final SHARED_WITH_ME_PLAYLIST_COLUMNS:[Ljava/lang/String;

.field public static final SONG_COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 13
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "Nid"

    aput-object v1, v0, v4

    const-string v1, "StoreAlbumId"

    aput-object v1, v0, v5

    const-string v1, "ArtistMetajamId"

    aput-object v1, v0, v6

    const-string v1, "title"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "artist_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Vid"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->SONG_COLUMNS:[Ljava/lang/String;

    .line 41
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "StoreAlbumId"

    aput-object v1, v0, v4

    const-string v1, "ArtistMetajamId"

    aput-object v1, v0, v5

    const-string v1, "album_name"

    aput-object v1, v0, v6

    const-string v1, "album_artist"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "album_artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "album_art"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->ALBUM_COLUMNS:[Ljava/lang/String;

    .line 61
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "ArtistMetajamId"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    const-string v1, "artworkUrl"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->ARTIST_COLUMNS:[Ljava/lang/String;

    .line 73
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->NAME:Ljava/lang/String;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->DESCRIPTION:Ljava/lang/String;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->SHARE_TOKEN:Ljava/lang/String;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->OWNER_NAME:Ljava/lang/String;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->ART_URL:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->OWNER_PROFILE_PHOTO_URL:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->CREATION_TIMESTAMP:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->LAST_MODIFIED_TIMESTAMP:Ljava/lang/String;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->SHARED_WITH_ME_PLAYLIST_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public static buildAlbumDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 5
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 99
    .local v3, "listDoc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 100
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 101
    .local v1, "elementCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 102
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 103
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v0, p0}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->getAlbumDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 104
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 109
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v1    # "elementCount":I
    .end local v2    # "i":I
    :cond_0
    return-object v3

    .line 101
    .restart local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v1    # "elementCount":I
    .restart local v2    # "i":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static buildArtistDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 5
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 116
    .local v3, "listDoc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 117
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 118
    .local v1, "elementCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 119
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 120
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v0, p0}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->getArtistDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 121
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 126
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v1    # "elementCount":I
    .end local v2    # "i":I
    :cond_0
    return-object v3

    .line 118
    .restart local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v1    # "elementCount":I
    .restart local v2    # "i":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static buildPlaylistDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 5
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 132
    .local v3, "listDoc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 133
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 134
    .local v1, "elementCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 135
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 136
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v0, p0}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->getPlaylistDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 137
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 142
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v1    # "elementCount":I
    .end local v2    # "i":I
    :cond_0
    return-object v3

    .line 134
    .restart local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v1    # "elementCount":I
    .restart local v2    # "i":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static buildTrackDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 5
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v3, "listDoc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 149
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 150
    .local v1, "elementCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 151
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 152
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v0, p0}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->getTrackDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 153
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 158
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v1    # "elementCount":I
    .end local v2    # "i":I
    :cond_0
    return-object v3

    .line 150
    .restart local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v1    # "elementCount":I
    .restart local v2    # "i":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getAlbumDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 164
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 165
    invoke-static {p0, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->populateDocumentFromAlbumCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object p0

    .line 166
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 167
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 168
    return-object p0
.end method

.method public static getArtistDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 174
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 175
    invoke-static {p0, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->populateDocumentFromArtistCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object p0

    .line 176
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 177
    return-object p0
.end method

.method public static getPlaylistDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 182
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 183
    invoke-static {p0, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->populateDocumentFromPlaylistCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object p0

    .line 184
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 185
    return-object p0
.end method

.method public static getTrackDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 191
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 192
    invoke-static {p0, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->populateDocumentFromSongCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object p0

    .line 193
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 194
    return-object p0
.end method

.method public static populateDocumentFromAlbumCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 6
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 228
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 229
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumId(J)V

    .line 230
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumName(Ljava/lang/String;)V

    .line 231
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 232
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistId(J)V

    .line 233
    invoke-interface {p1, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 236
    :cond_0
    invoke-interface {p1, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 237
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 239
    :cond_1
    invoke-interface {p1, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 240
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistMetajamId(Ljava/lang/String;)V

    .line 243
    :cond_2
    return-object p0
.end method

.method public static populateDocumentFromArtistCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 4
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 248
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 249
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 250
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistId(J)V

    .line 251
    invoke-interface {p1, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 254
    :cond_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 255
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistMetajamId(Ljava/lang/String;)V

    .line 258
    :cond_1
    return-object p0
.end method

.method private static populateDocumentFromPlaylistCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 2
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 263
    const/16 v0, 0x46

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistType(I)V

    .line 264
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 265
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistName(Ljava/lang/String;)V

    .line 266
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setDescription(Ljava/lang/String;)V

    .line 267
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistShareToken(Ljava/lang/String;)V

    .line 268
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistOwnerName(Ljava/lang/String;)V

    .line 269
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 270
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setProfilePhotoUrl(Ljava/lang/String;)V

    .line 272
    return-object p0
.end method

.method public static populateDocumentFromSongCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 7
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/16 v6, 0xa

    const/16 v5, 0x9

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 199
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 200
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 201
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumId(J)V

    .line 202
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumName(Ljava/lang/String;)V

    .line 203
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 204
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistId(J)V

    .line 205
    invoke-interface {p1, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 208
    :cond_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 209
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTrackMetajamId(Ljava/lang/String;)V

    .line 211
    :cond_1
    invoke-interface {p1, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 212
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 214
    :cond_2
    invoke-interface {p1, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 215
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistMetajamId(Ljava/lang/String;)V

    .line 217
    :cond_3
    invoke-interface {p1, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 218
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setDuration(J)V

    .line 220
    :cond_4
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 221
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setVideoId(Ljava/lang/String;)V

    .line 223
    :cond_5
    return-object p0
.end method
