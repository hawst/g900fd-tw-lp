.class public Lcom/google/android/music/ui/NonUriMediaListCursorLoader;
.super Landroid/support/v4/content/CursorLoader;
.source "NonUriMediaListCursorLoader.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mMediaList:Lcom/google/android/music/medialist/MediaList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaList"    # Lcom/google/android/music/medialist/MediaList;
    .param p3, "projection"    # [Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object p2, p0, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    .line 22
    iput-object p1, p0, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;->mContext:Landroid/content/Context;

    .line 23
    invoke-virtual {p0, p3}, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;->setProjection([Ljava/lang/String;)V

    .line 24
    return-void
.end method


# virtual methods
.method public loadInBackground()Landroid/database/Cursor;
    .locals 5

    .prologue
    .line 28
    iget-object v1, p0, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    invoke-virtual {p0}, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/MediaList;->getFullContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    .line 29
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 30
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "MediaList not be content uri based"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 32
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    iget-object v2, p0, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;->getProjection()[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/music/medialist/MediaList;->getSyncMediaCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
