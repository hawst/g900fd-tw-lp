.class public Lcom/google/android/music/playback/session/MediaSessionCompatPreL;
.super Ljava/lang/Object;
.source "MediaSessionCompatPreL.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy;,
        Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;
    }
.end annotation


# direct methods
.method public static createCallback(Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;)Ljava/lang/Object;
    .locals 1
    .param p0, "callback"    # Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy;-><init>(Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;)V

    return-object v0
.end method

.method public static createRccCompat(Landroid/app/PendingIntent;Landroid/os/Looper;)Lcom/google/android/music/playback/RemoteControlClientCompat;
    .locals 1
    .param p0, "intent"    # Landroid/app/PendingIntent;
    .param p1, "looper"    # Landroid/os/Looper;

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/music/playback/RemoteControlClientCompat;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/playback/RemoteControlClientCompat;-><init>(Landroid/app/PendingIntent;Landroid/os/Looper;)V

    return-object v0
.end method

.method public static release(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "rccObj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 61
    move-object v0, p1

    check-cast v0, Lcom/google/android/music/playback/RemoteControlClientCompat;

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/RemoteControlClientCompat;->setOnGetPlaybackPositionListener(Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;)V

    move-object v0, p1

    .line 62
    check-cast v0, Lcom/google/android/music/playback/RemoteControlClientCompat;

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/RemoteControlClientCompat;->setPlaybackPositionUpdateListener(Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;)V

    move-object v0, p1

    .line 63
    check-cast v0, Lcom/google/android/music/playback/RemoteControlClientCompat;

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/RemoteControlClientCompat;->setOnMetadataUpdateListener(Lcom/google/android/music/playback/RemoteControlClientCompat$OnMetadataUpdateListener;)V

    .line 64
    invoke-static {p0}, Lcom/google/android/music/playback/AudioManagerCompat;->getAudioManagerCompat(Landroid/content/Context;)Lcom/google/android/music/playback/AudioManagerCompat;

    move-result-object v0

    check-cast p1, Lcom/google/android/music/playback/RemoteControlClientCompat;

    .end local p1    # "rccObj":Ljava/lang/Object;
    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/AudioManagerCompat;->unregisterRemoteControlClient(Lcom/google/android/music/playback/RemoteControlClientCompat;)V

    .line 66
    return-void
.end method

.method public static setActive(Landroid/content/Context;Ljava/lang/Object;Landroid/content/ComponentName;Z)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "rccObj"    # Ljava/lang/Object;
    .param p2, "mediaButtonReceiver"    # Landroid/content/ComponentName;
    .param p3, "active"    # Z

    .prologue
    .line 49
    if-eqz p3, :cond_0

    .line 50
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-virtual {v0, p2}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 52
    invoke-static {p0}, Lcom/google/android/music/playback/AudioManagerCompat;->getAudioManagerCompat(Landroid/content/Context;)Lcom/google/android/music/playback/AudioManagerCompat;

    move-result-object v0

    check-cast p1, Lcom/google/android/music/playback/RemoteControlClientCompat;

    .end local p1    # "rccObj":Ljava/lang/Object;
    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/AudioManagerCompat;->registerRemoteControlClient(Lcom/google/android/music/playback/RemoteControlClientCompat;)V

    .line 55
    :cond_0
    return-void
.end method

.method public static setCallback(Ljava/lang/Object;Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 2
    .param p0, "rccObj"    # Ljava/lang/Object;
    .param p1, "callbackObj"    # Ljava/lang/Object;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 34
    move-object v0, p0

    check-cast v0, Lcom/google/android/music/playback/RemoteControlClientCompat;

    move-object v1, p1

    check-cast v1, Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/RemoteControlClientCompat;->setOnGetPlaybackPositionListener(Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;)V

    move-object v0, p0

    .line 37
    check-cast v0, Lcom/google/android/music/playback/RemoteControlClientCompat;

    move-object v1, p1

    check-cast v1, Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/RemoteControlClientCompat;->setPlaybackPositionUpdateListener(Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;)V

    .line 40
    check-cast p0, Lcom/google/android/music/playback/RemoteControlClientCompat;

    .end local p0    # "rccObj":Ljava/lang/Object;
    check-cast p1, Lcom/google/android/music/playback/RemoteControlClientCompat$OnMetadataUpdateListener;

    .end local p1    # "callbackObj":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/music/playback/RemoteControlClientCompat;->setOnMetadataUpdateListener(Lcom/google/android/music/playback/RemoteControlClientCompat$OnMetadataUpdateListener;)V

    .line 42
    return-void
.end method

.method public static setMetadata(Ljava/lang/Object;Lcom/google/android/music/playback/session/MediaMetadataCompat;)V
    .locals 1
    .param p0, "rccObj"    # Ljava/lang/Object;
    .param p1, "metadataObj"    # Lcom/google/android/music/playback/session/MediaMetadataCompat;

    .prologue
    .line 77
    check-cast p0, Lcom/google/android/music/playback/RemoteControlClientCompat;

    .end local p0    # "rccObj":Ljava/lang/Object;
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/RemoteControlClientCompat;->editMetadata(Z)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionLegacyHelper;->setOldMetadata(Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;Lcom/google/android/music/playback/session/MediaMetadataCompat;)V

    .line 79
    return-void
.end method

.method public static setPlaybackState(Ljava/lang/Object;Lcom/google/android/music/playback/session/PlaybackStateCompat;)V
    .locals 4
    .param p0, "rccObj"    # Ljava/lang/Object;
    .param p1, "state"    # Lcom/google/android/music/playback/session/PlaybackStateCompat;

    .prologue
    .line 69
    move-object v0, p0

    check-cast v0, Lcom/google/android/music/playback/RemoteControlClientCompat;

    invoke-virtual {p1}, Lcom/google/android/music/playback/session/PlaybackStateCompat;->getActions()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/music/playback/session/MediaSessionLegacyHelper;->getRccTransportControlFlags(J)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/RemoteControlClientCompat;->setTransportControlFlags(I)V

    .line 71
    check-cast p0, Lcom/google/android/music/playback/RemoteControlClientCompat;

    .end local p0    # "rccObj":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/google/android/music/playback/session/PlaybackStateCompat;->getState()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/music/playback/session/MediaSessionLegacyHelper;->getRccStateFromState(I)I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/music/playback/session/PlaybackStateCompat;->getPosition()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/music/playback/session/PlaybackStateCompat;->getPlaybackSpeed()F

    move-result v1

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/google/android/music/playback/RemoteControlClientCompat;->setPlaybackState(IJF)V

    .line 74
    return-void
.end method
