.class Lcom/google/android/music/net/NetworkMonitor$2;
.super Ljava/lang/Object;
.source "NetworkMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/net/NetworkMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/net/NetworkMonitor;


# direct methods
.method constructor <init>(Lcom/google/android/music/net/NetworkMonitor;)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Lcom/google/android/music/net/NetworkMonitor$2;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 342
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$2;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # invokes: Lcom/google/android/music/net/NetworkMonitor;->getIsStreamingAvailable()Z
    invoke-static {v1}, Lcom/google/android/music/net/NetworkMonitor;->access$1100(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v0

    .line 346
    .local v0, "newIsAvail":Z
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$2;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # invokes: Lcom/google/android/music/net/NetworkMonitor;->notifyListenersNetworkChanged()V
    invoke-static {v1}, Lcom/google/android/music/net/NetworkMonitor;->access$1200(Lcom/google/android/music/net/NetworkMonitor;)V

    .line 347
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$2;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mIsStreamingAvailable:Z
    invoke-static {v1}, Lcom/google/android/music/net/NetworkMonitor;->access$1300(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v1

    if-ne v0, v1, :cond_0

    .line 362
    :goto_0
    return-void

    .line 351
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$2;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # setter for: Lcom/google/android/music/net/NetworkMonitor;->mIsStreamingAvailable:Z
    invoke-static {v1, v0}, Lcom/google/android/music/net/NetworkMonitor;->access$1302(Lcom/google/android/music/net/NetworkMonitor;Z)Z

    .line 353
    # getter for: Lcom/google/android/music/net/NetworkMonitor;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/net/NetworkMonitor;->access$200()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 354
    const-string v2, "NetworkMonitor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Connectivity status changed to ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$2;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mIsStreamingAvailable:Z
    invoke-static {v1}, Lcom/google/android/music/net/NetworkMonitor;->access$1300(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "CONNECTED"

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "unmetered wifi/eth: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/net/NetworkMonitor$2;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mUnmeteredWifiOrEthernetConnected:Z
    invoke-static {v3}, Lcom/google/android/music/net/NetworkMonitor;->access$700(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " mobileOrMetered: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/net/NetworkMonitor$2;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mMobileOrMeteredConnected:Z
    invoke-static {v3}, Lcom/google/android/music/net/NetworkMonitor;->access$600(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$2;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # invokes: Lcom/google/android/music/net/NetworkMonitor;->notifyListenersStreamabilityChanged(Z)V
    invoke-static {v1, v0}, Lcom/google/android/music/net/NetworkMonitor;->access$1400(Lcom/google/android/music/net/NetworkMonitor;Z)V

    .line 361
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$2;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    invoke-virtual {v1}, Lcom/google/android/music/net/NetworkMonitor;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    goto :goto_0

    .line 354
    :cond_2
    const-string v1, "NOT CONNECTED"

    goto :goto_1
.end method
