.class public Lcom/google/android/music/playback/PlaybackState;
.super Ljava/lang/Object;
.source "PlaybackState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/playback/PlaybackState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mErrorType:I

.field private final mHasValidPlaylist:Z

.field private final mIsCurrentSongLoaded:Z

.field private final mIsInErrorState:Z

.field private final mIsInFatalErrorState:Z

.field private final mIsInIniniteMixMode:Z

.field private final mIsLocalDevicePlayback:Z

.field private final mIsPlaying:Z

.field private final mIsPlaylistLoading:Z

.field private final mIsPreparing:Z

.field private final mIsStreaming:Z

.field private final mIsStreamingFullyBuffered:Z

.field private final mMediaList:Lcom/google/android/music/medialist/SongList;

.field private final mPosition:J

.field private final mQueuePosition:I

.field private final mQueueSize:I

.field private final mRepeatMode:I

.field private final mShuffleMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 197
    new-instance v0, Lcom/google/android/music/playback/PlaybackState$1;

    invoke-direct {v0}, Lcom/google/android/music/playback/PlaybackState$1;-><init>()V

    sput-object v0, Lcom/google/android/music/playback/PlaybackState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIZZLcom/google/android/music/medialist/SongList;JIIZZZIZZZZZZ)V
    .locals 2
    .param p1, "queuePosition"    # I
    .param p2, "queueSize"    # I
    .param p3, "isPlaying"    # Z
    .param p4, "isCurrentSongLoaded"    # Z
    .param p5, "mediaList"    # Lcom/google/android/music/medialist/SongList;
    .param p6, "position"    # J
    .param p8, "shuffleMode"    # I
    .param p9, "repeatMode"    # I
    .param p10, "isPreparing"    # Z
    .param p11, "isStreaming"    # Z
    .param p12, "isInErrorState"    # Z
    .param p13, "errorType"    # I
    .param p14, "isStreamingFullyBuffered"    # Z
    .param p15, "isInFatalErrorState"    # Z
    .param p16, "hasValidPlaylist"    # Z
    .param p17, "isPlaylistLoading"    # Z
    .param p18, "isLocalDevicePlayback"    # Z
    .param p19, "isInIniniteMixMode"    # Z

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lcom/google/android/music/playback/PlaybackState;->mQueuePosition:I

    .line 44
    iput p2, p0, Lcom/google/android/music/playback/PlaybackState;->mQueueSize:I

    .line 45
    iput-boolean p3, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPlaying:Z

    .line 46
    iput-boolean p4, p0, Lcom/google/android/music/playback/PlaybackState;->mIsCurrentSongLoaded:Z

    .line 47
    iput-object p5, p0, Lcom/google/android/music/playback/PlaybackState;->mMediaList:Lcom/google/android/music/medialist/SongList;

    .line 48
    iput-wide p6, p0, Lcom/google/android/music/playback/PlaybackState;->mPosition:J

    .line 49
    iput p8, p0, Lcom/google/android/music/playback/PlaybackState;->mShuffleMode:I

    .line 50
    iput p9, p0, Lcom/google/android/music/playback/PlaybackState;->mRepeatMode:I

    .line 51
    iput-boolean p10, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPreparing:Z

    .line 52
    iput-boolean p11, p0, Lcom/google/android/music/playback/PlaybackState;->mIsStreaming:Z

    .line 53
    iput-boolean p12, p0, Lcom/google/android/music/playback/PlaybackState;->mIsInErrorState:Z

    .line 54
    iput p13, p0, Lcom/google/android/music/playback/PlaybackState;->mErrorType:I

    .line 55
    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsStreamingFullyBuffered:Z

    .line 56
    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsInFatalErrorState:Z

    .line 57
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mHasValidPlaylist:Z

    .line 58
    move/from16 v0, p17

    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPlaylistLoading:Z

    .line 59
    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsLocalDevicePlayback:Z

    .line 60
    move/from16 v0, p19

    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsInIniniteMixMode:Z

    .line 61
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/playback/PlaybackState;->mQueuePosition:I

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/playback/PlaybackState;->mQueueSize:I

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPlaying:Z

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsCurrentSongLoaded:Z

    .line 68
    const-class v0, Lcom/google/android/music/medialist/MediaList;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/SongList;

    iput-object v0, p0, Lcom/google/android/music/playback/PlaybackState;->mMediaList:Lcom/google/android/music/medialist/SongList;

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/playback/PlaybackState;->mPosition:J

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/playback/PlaybackState;->mShuffleMode:I

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/playback/PlaybackState;->mRepeatMode:I

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPreparing:Z

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsStreaming:Z

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsInErrorState:Z

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/playback/PlaybackState;->mErrorType:I

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsStreamingFullyBuffered:Z

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsInFatalErrorState:Z

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mHasValidPlaylist:Z

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPlaylistLoading:Z

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsLocalDevicePlayback:Z

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_a

    :goto_a
    iput-boolean v1, p0, Lcom/google/android/music/playback/PlaybackState;->mIsInIniniteMixMode:Z

    .line 82
    return-void

    :cond_0
    move v0, v2

    .line 66
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 67
    goto :goto_1

    :cond_2
    move v0, v2

    .line 72
    goto :goto_2

    :cond_3
    move v0, v2

    .line 73
    goto :goto_3

    :cond_4
    move v0, v2

    .line 74
    goto :goto_4

    :cond_5
    move v0, v2

    .line 76
    goto :goto_5

    :cond_6
    move v0, v2

    .line 77
    goto :goto_6

    :cond_7
    move v0, v2

    .line 78
    goto :goto_7

    :cond_8
    move v0, v2

    .line 79
    goto :goto_8

    :cond_9
    move v0, v2

    .line 80
    goto :goto_9

    :cond_a
    move v1, v2

    .line 81
    goto :goto_a
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    return v0
.end method

.method public getErrorType()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/google/android/music/playback/PlaybackState;->mErrorType:I

    return v0
.end method

.method public getMediaList()Lcom/google/android/music/medialist/SongList;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/music/playback/PlaybackState;->mMediaList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method public getPosition()J
    .locals 2

    .prologue
    .line 105
    iget-wide v0, p0, Lcom/google/android/music/playback/PlaybackState;->mPosition:J

    return-wide v0
.end method

.method public getQueuePosition()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/google/android/music/playback/PlaybackState;->mQueuePosition:I

    return v0
.end method

.method public getQueueSize()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/music/playback/PlaybackState;->mQueueSize:I

    return v0
.end method

.method public getRepeatMode()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/google/android/music/playback/PlaybackState;->mRepeatMode:I

    return v0
.end method

.method public getShuffleMode()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/google/android/music/playback/PlaybackState;->mShuffleMode:I

    return v0
.end method

.method public hasValidPlaylist()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mHasValidPlaylist:Z

    return v0
.end method

.method public isCurrentSongLoaded()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsCurrentSongLoaded:Z

    return v0
.end method

.method public isInErrorState()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsInErrorState:Z

    return v0
.end method

.method public isInIniniteMixMode()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsInIniniteMixMode:Z

    return v0
.end method

.method public isLocalDevicePlayback()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsLocalDevicePlayback:Z

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPlaying:Z

    return v0
.end method

.method public isPlaylistLoading()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPlaylistLoading:Z

    return v0
.end method

.method public isPreparing()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPreparing:Z

    return v0
.end method

.method public isStreaming()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsStreaming:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PlaybackState [mQueuePosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/playback/PlaybackState;->mQueuePosition:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mQueueSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/playback/PlaybackState;->mQueueSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsPlaying="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPlaying:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsCurrentSongLoaded="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/playback/PlaybackState;->mIsCurrentSongLoaded:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMediaList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/playback/PlaybackState;->mMediaList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/playback/PlaybackState;->mPosition:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mShuffleMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/playback/PlaybackState;->mShuffleMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRepeatMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/playback/PlaybackState;->mRepeatMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsPreparing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPreparing:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsStreaming="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/playback/PlaybackState;->mIsStreaming:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsInErrorState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/playback/PlaybackState;->mIsInErrorState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mErrorType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/playback/PlaybackState;->mErrorType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsStreamingFullyBuffered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/playback/PlaybackState;->mIsStreamingFullyBuffered:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsInFatalErrorState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/playback/PlaybackState;->mIsInFatalErrorState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mHasValidPlaylist="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/playback/PlaybackState;->mHasValidPlaylist:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsPlaylistLoading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPlaylistLoading:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsLocalDevicePlayback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/playback/PlaybackState;->mIsLocalDevicePlayback:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsInIniniteMixMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/playback/PlaybackState;->mIsInIniniteMixMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 177
    iget v0, p0, Lcom/google/android/music/playback/PlaybackState;->mQueuePosition:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 178
    iget v0, p0, Lcom/google/android/music/playback/PlaybackState;->mQueueSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 179
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPlaying:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 180
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsCurrentSongLoaded:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 181
    iget-object v0, p0, Lcom/google/android/music/playback/PlaybackState;->mMediaList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 182
    iget-wide v4, p0, Lcom/google/android/music/playback/PlaybackState;->mPosition:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 183
    iget v0, p0, Lcom/google/android/music/playback/PlaybackState;->mShuffleMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 184
    iget v0, p0, Lcom/google/android/music/playback/PlaybackState;->mRepeatMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 185
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPreparing:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 186
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsStreaming:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 187
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsInErrorState:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 188
    iget v0, p0, Lcom/google/android/music/playback/PlaybackState;->mErrorType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 189
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsStreamingFullyBuffered:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 190
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsInFatalErrorState:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 191
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mHasValidPlaylist:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 192
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsPlaylistLoading:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 193
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsLocalDevicePlayback:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 194
    iget-boolean v0, p0, Lcom/google/android/music/playback/PlaybackState;->mIsInIniniteMixMode:Z

    if-eqz v0, :cond_a

    :goto_a
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 195
    return-void

    :cond_0
    move v0, v2

    .line 179
    goto :goto_0

    :cond_1
    move v0, v2

    .line 180
    goto :goto_1

    :cond_2
    move v0, v2

    .line 185
    goto :goto_2

    :cond_3
    move v0, v2

    .line 186
    goto :goto_3

    :cond_4
    move v0, v2

    .line 187
    goto :goto_4

    :cond_5
    move v0, v2

    .line 189
    goto :goto_5

    :cond_6
    move v0, v2

    .line 190
    goto :goto_6

    :cond_7
    move v0, v2

    .line 191
    goto :goto_7

    :cond_8
    move v0, v2

    .line 192
    goto :goto_8

    :cond_9
    move v0, v2

    .line 193
    goto :goto_9

    :cond_a
    move v1, v2

    .line 194
    goto :goto_a
.end method
