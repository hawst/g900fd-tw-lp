.class public Lcom/google/android/music/store/RecentItemsManager;
.super Ljava/lang/Object;
.source "RecentItemsManager.java"


# static fields
.field private static final LOGV:Z

.field private static final UPDATE_RECENT_ITEMS_MESSAGE_TYPE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/RecentItemsManager;->LOGV:Z

    .line 53
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-static {v0}, Lcom/google/android/music/utils/async/AsyncWorkers;->getUniqueMessageType(Lcom/google/android/music/utils/LoggableHandler;)I

    move-result v0

    sput v0, Lcom/google/android/music/store/RecentItemsManager;->UPDATE_RECENT_ITEMS_MESSAGE_TYPE:I

    return-void
.end method

.method public static addCreatedPlaylist(Landroid/content/Context;J)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistId"    # J

    .prologue
    .line 136
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/google/android/music/store/RecentItemsManager;->addPlaylist(Landroid/content/Context;JILjava/lang/String;)Z

    .line 137
    return-void
.end method

.method public static addFollowedPlaylist(Landroid/content/Context;J)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistId"    # J

    .prologue
    .line 152
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/google/android/music/store/RecentItemsManager;->addPlaylist(Landroid/content/Context;JILjava/lang/String;)Z

    .line 153
    return-void
.end method

.method public static addModifiedPlaylist(Landroid/content/Context;J)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistId"    # J

    .prologue
    .line 143
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/google/android/music/store/RecentItemsManager;->addPlaylist(Landroid/content/Context;JILjava/lang/String;)Z

    .line 144
    return-void
.end method

.method private static addNautilusAlbum(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 6
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "nautilusAlbumId"    # Ljava/lang/String;
    .param p2, "nautilusAlbumName"    # Ljava/lang/String;
    .param p3, "nautilusAlbumArt"    # Ljava/lang/String;
    .param p4, "nautilusAlbumArtist"    # Ljava/lang/String;
    .param p5, "nautilusAlbumArtistId"    # Ljava/lang/String;
    .param p6, "nautilusAlbumArtistProfileImage"    # Ljava/lang/String;
    .param p7, "nautilusAlbumDescription"    # Ljava/lang/String;
    .param p8, "reason"    # I
    .param p9, "reasonText"    # Ljava/lang/String;

    .prologue
    .line 458
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 459
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "RecentNautilusAlbumId"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const-string v3, "RecentNautilusAlbum"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const-string v3, "RecentNautilusAlbumArt"

    invoke-virtual {v2, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    const-string v3, "RecentNautilusAlbumArtist"

    invoke-virtual {v2, v3, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    const-string v3, "RecentNautilusAlbumArtistId"

    invoke-virtual {v2, v3, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    const-string v3, "RecentNautilusAlbumArtistProfileImage"

    invoke-virtual {v2, v3, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    const-string v3, "RecentNautilusAlbumDescription"

    invoke-virtual {v2, v3, p7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const-string v3, "ItemDate"

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 468
    const-string v3, "RecentReason"

    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 469
    const-string v3, "RecentReasonText"

    invoke-virtual {v2, v3, p9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    const-string v3, "RECENT"

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-virtual {p0, v3, v4, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v0

    .line 473
    .local v0, "newId":J
    const-wide/16 v4, 0x1

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 474
    const-string v3, "RecentItemsManager"

    const-string v4, "Failed to insert album into recent"

    invoke-static {v3, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/store/RecentItemsManager;->enforceMaxItemsLimit(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 479
    return-void
.end method

.method static addNautilusAlbum(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nautilusAlbumId"    # Ljava/lang/String;
    .param p2, "nautilusAlbumName"    # Ljava/lang/String;
    .param p3, "nautilusAlbumArt"    # Ljava/lang/String;
    .param p4, "nautilusAlbumArtist"    # Ljava/lang/String;
    .param p5, "nautilusAlbumArtistId"    # Ljava/lang/String;
    .param p6, "nautilusAlbumArtistProfileImage"    # Ljava/lang/String;
    .param p7, "nautilusAlbumDescription"    # Ljava/lang/String;
    .param p8, "reason"    # I
    .param p9, "reasonText"    # Ljava/lang/String;

    .prologue
    .line 315
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 317
    :cond_0
    const-string v1, "RecentItemsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Missing fields. Cannot add nautilus item to recent:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    const/4 v11, 0x0

    .line 337
    :cond_1
    :goto_0
    return v11

    .line 321
    :cond_2
    const/4 v11, 0x0

    .line 322
    .local v11, "success":Z
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v10

    .line 323
    .local v10, "store":Lcom/google/android/music/store/Store;
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    .line 325
    :try_start_0
    invoke-static/range {v0 .. v9}, Lcom/google/android/music/store/RecentItemsManager;->addNautilusAlbum(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    const/4 v11, 0x1

    .line 330
    invoke-virtual {v10, v0, v11}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 333
    if-eqz v11, :cond_1

    .line 334
    invoke-static {p0}, Lcom/google/android/music/store/RecentItemsManager;->notifyContentChange(Landroid/content/Context;)V

    goto :goto_0

    .line 330
    :catchall_0
    move-exception v1

    invoke-virtual {v10, v0, v11}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v1
.end method

.method private static addPlayedAlbum(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "albumId"    # J

    .prologue
    .line 435
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 436
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "RecentAlbumId"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 437
    const-string v1, "ItemDate"

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 438
    const-string v1, "RecentReason"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 443
    const-string v1, "RECENT"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 444
    invoke-static {p0}, Lcom/google/android/music/store/RecentItemsManager;->enforceMaxItemsLimit(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 445
    return-void
.end method

.method private static addPlayedAlbum(Landroid/content/Context;J)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 259
    cmp-long v3, p1, v4

    if-gez v3, :cond_1

    .line 260
    const-string v3, "RecentItemsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot add album to recent. Invalid id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_0
    :goto_0
    return v2

    .line 264
    :cond_1
    cmp-long v3, p1, v4

    if-eqz v3, :cond_0

    .line 269
    const/4 v2, 0x0

    .line 270
    .local v2, "success":Z
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    .line 271
    .local v1, "store":Lcom/google/android/music/store/Store;
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 273
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {v0, p1, p2}, Lcom/google/android/music/store/RecentItemsManager;->addPlayedAlbum(Landroid/database/sqlite/SQLiteDatabase;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 274
    const/4 v2, 0x1

    .line 276
    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 279
    if-eqz v2, :cond_0

    .line 280
    invoke-static {p0}, Lcom/google/android/music/store/RecentItemsManager;->notifyContentChange(Landroid/content/Context;)V

    goto :goto_0

    .line 276
    :catchall_0
    move-exception v3

    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method private static addPlayedArtist(Landroid/content/Context;J)Z
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistId"    # J

    .prologue
    .line 389
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-gez v3, :cond_1

    .line 390
    const-string v3, "RecentItemsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot add artist to recent. Invalid id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    const/16 v16, 0x0

    .line 430
    :cond_0
    :goto_0
    return v16

    .line 394
    :cond_1
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-nez v3, :cond_2

    .line 396
    const/16 v16, 0x0

    goto :goto_0

    .line 401
    :cond_2
    const/16 v16, 0x0

    .line 402
    .local v16, "success":Z
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v15

    .line 403
    .local v15, "store":Lcom/google/android/music/store/Store;
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 404
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v14, 0x0

    .line 406
    .local v14, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    .line 407
    .local v11, "artistIdStr":Ljava/lang/String;
    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v11, v6, v3

    const/4 v3, 0x1

    aput-object v11, v6, v3

    .line 408
    .local v6, "selectionArgs":[Ljava/lang/String;
    const-string v3, "MUSIC"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "AlbumId"

    aput-object v7, v4, v5

    const-string v5, "AlbumArtistId=? OR ArtistId=?"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "CanonicalName"

    const-string v10, "1"

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 416
    if-eqz v14, :cond_3

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 417
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 418
    .local v12, "albumId":J
    invoke-static {v2, v12, v13}, Lcom/google/android/music/store/RecentItemsManager;->addPlayedAlbum(Landroid/database/sqlite/SQLiteDatabase;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 419
    const/16 v16, 0x1

    .line 422
    .end local v12    # "albumId":J
    :cond_3
    invoke-static {v14}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 423
    move/from16 v0, v16

    invoke-virtual {v15, v2, v0}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 426
    if-eqz v16, :cond_0

    .line 427
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/RecentItemsManager;->notifyContentChange(Landroid/content/Context;)V

    goto :goto_0

    .line 422
    .end local v6    # "selectionArgs":[Ljava/lang/String;
    .end local v11    # "artistIdStr":Ljava/lang/String;
    :catchall_0
    move-exception v3

    invoke-static {v14}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 423
    move/from16 v0, v16

    invoke-virtual {v15, v2, v0}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method private static addPlayedNautilusAlbum(Landroid/content/Context;Lcom/google/android/music/medialist/NautilusAlbumSongList;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songlist"    # Lcom/google/android/music/medialist/NautilusAlbumSongList;

    .prologue
    const/4 v6, 0x0

    .line 290
    invoke-virtual {p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getNautilusId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p0}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, p0}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, p0}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getAlbumArtist(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, p0}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getArtistMetajamId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, p0}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getDescription(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    move-object v0, p0

    move-object v9, v6

    invoke-static/range {v0 .. v9}, Lcom/google/android/music/store/RecentItemsManager;->addNautilusAlbum(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static addPlayedRadio(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 7
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "radioStationId"    # J

    .prologue
    .line 498
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 499
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "RecentRadioId"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 500
    const-string v3, "ItemDate"

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 501
    const-string v3, "RecentReason"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 505
    const-string v3, "RECENT"

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-virtual {p0, v3, v4, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v0

    .line 507
    .local v0, "newId":J
    const-wide/16 v4, 0x1

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 508
    const-string v3, "RecentItemsManager"

    const-string v4, "Failed to insert radio into recent"

    invoke-static {v3, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/store/RecentItemsManager;->enforceMaxItemsLimit(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 511
    return-void
.end method

.method private static addPlayedRadio(Landroid/content/Context;J)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "radioStationId"    # J

    .prologue
    .line 366
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-gtz v3, :cond_1

    .line 367
    const-string v3, "RecentItemsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot add radio to recent. Invalid id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    const/4 v2, 0x0

    .line 384
    :cond_0
    :goto_0
    return v2

    .line 370
    :cond_1
    const/4 v2, 0x0

    .line 371
    .local v2, "success":Z
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    .line 372
    .local v1, "store":Lcom/google/android/music/store/Store;
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 374
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {v0, p1, p2}, Lcom/google/android/music/store/RecentItemsManager;->addPlayedRadio(Landroid/database/sqlite/SQLiteDatabase;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 375
    const/4 v2, 0x1

    .line 377
    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 380
    if-eqz v2, :cond_0

    .line 381
    invoke-static {p0}, Lcom/google/android/music/store/RecentItemsManager;->notifyContentChange(Landroid/content/Context;)V

    goto :goto_0

    .line 377
    :catchall_0
    move-exception v3

    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method private static addPlaylist(Landroid/database/sqlite/SQLiteDatabase;JILjava/lang/String;)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "reason"    # I
    .param p4, "reasonText"    # Ljava/lang/String;

    .prologue
    .line 483
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 484
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "RecentListId"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 485
    const-string v1, "ItemDate"

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 486
    const-string v1, "RecentReason"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 487
    const-string v1, "RecentReasonText"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    const-string v1, "RECENT"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 493
    invoke-static {p0}, Lcom/google/android/music/store/RecentItemsManager;->enforceMaxItemsLimit(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 494
    return-void
.end method

.method private static addPlaylist(Landroid/content/Context;JILjava/lang/String;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistId"    # J
    .param p3, "reason"    # I
    .param p4, "reasonText"    # Ljava/lang/String;

    .prologue
    .line 343
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-gtz v3, :cond_1

    .line 344
    const-string v3, "RecentItemsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot add playlist to recent. Invalid id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    const/4 v2, 0x0

    .line 361
    :cond_0
    :goto_0
    return v2

    .line 347
    :cond_1
    const/4 v2, 0x0

    .line 348
    .local v2, "success":Z
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    .line 349
    .local v1, "store":Lcom/google/android/music/store/Store;
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 351
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {v0, p1, p2, p3, p4}, Lcom/google/android/music/store/RecentItemsManager;->addPlaylist(Landroid/database/sqlite/SQLiteDatabase;JILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 352
    const/4 v2, 0x1

    .line 354
    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 357
    if-eqz v2, :cond_0

    .line 358
    invoke-static {p0}, Lcom/google/android/music/store/RecentItemsManager;->notifyContentChange(Landroid/content/Context;)V

    goto :goto_0

    .line 354
    :catchall_0
    move-exception v3

    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method private static addRecentlyAddedAlbums(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 30
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 625
    const/16 v17, 0x0

    .line 626
    .local v17, "count":I
    const-wide/16 v28, 0x0

    .line 628
    .local v28, "oldestItemDate":J
    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    .line 629
    .local v19, "existingTimeStamps":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Long;>;"
    const-string v3, "RECENT"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "ItemDate"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "RecentAlbumId"

    aput-object v5, v4, v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "ItemDate ASC"

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 633
    .local v16, "c":Landroid/database/Cursor;
    if-eqz v16, :cond_2

    :try_start_0
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 634
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v17

    .line 635
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v28

    .line 641
    :cond_0
    const/4 v2, 0x1

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 642
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 643
    .local v20, "date":J
    const/4 v2, 0x1

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 644
    .local v14, "albumId":J
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 646
    .end local v14    # "albumId":J
    .end local v20    # "date":J
    :cond_1
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 649
    :cond_2
    invoke-static/range {v16 .. v16}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 660
    const-string v3, "MUSIC"

    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "AlbumId"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "max(FileDate) as max_album_date"

    aput-object v5, v4, v2

    const/4 v2, 0x2

    const-string v5, "(max(TrackType) = 3 and min(TrackType) = 3) as isPromoContentNotSelectedByUser"

    aput-object v5, v4, v2

    const-string v5, "Domain=0"

    const/4 v6, 0x0

    const-string v7, "AlbumIdSourceText"

    const/4 v8, 0x0

    const-string v9, "max_album_date DESC"

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 680
    .local v11, "albumCursor":Landroid/database/Cursor;
    if-nez v11, :cond_3

    .line 732
    :goto_0
    return-void

    .line 649
    .end local v11    # "albumCursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v2

    invoke-static/range {v16 .. v16}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    .line 685
    .restart local v11    # "albumCursor":Landroid/database/Cursor;
    :cond_3
    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 686
    const/4 v2, 0x1

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    .line 688
    .local v26, "mostRecentlyAddedAlbumDate":J
    const/16 v2, 0xc8

    move/from16 v0, v17

    if-lt v0, v2, :cond_4

    cmp-long v2, v26, v28

    if-lez v2, :cond_a

    .line 698
    :cond_4
    const/4 v2, -0x1

    invoke-interface {v11, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 699
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 700
    .local v22, "insertValues":Landroid/content/ContentValues;
    const/16 v23, 0x0

    .line 701
    .local v23, "inserted":Z
    :cond_5
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 702
    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 703
    .restart local v14    # "albumId":J
    const/4 v2, 0x1

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 704
    .local v12, "albumDate":J
    const/4 v2, 0x2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_7

    const/16 v24, 0x1

    .line 706
    .local v24, "isNonUserPromo":Z
    :goto_2
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Long;

    .line 707
    .local v18, "currentDate":Ljava/lang/Long;
    if-eqz v18, :cond_6

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v12, v2

    if-lez v2, :cond_5

    .line 708
    :cond_6
    invoke-virtual/range {v22 .. v22}, Landroid/content/ContentValues;->clear()V

    .line 709
    const-string v2, "RecentAlbumId"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 710
    const-string v2, "ItemDate"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 711
    const-string v3, "Priority"

    if-eqz v24, :cond_8

    const/16 v2, -0xa

    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 714
    const-string v2, "RECENT"

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 715
    const/16 v23, 0x1

    goto :goto_1

    .line 704
    .end local v18    # "currentDate":Ljava/lang/Long;
    .end local v24    # "isNonUserPromo":Z
    :cond_7
    const/16 v24, 0x0

    goto :goto_2

    .line 711
    .restart local v18    # "currentDate":Ljava/lang/Long;
    .restart local v24    # "isNonUserPromo":Z
    :cond_8
    const/4 v2, 0x0

    goto :goto_3

    .line 719
    .end local v12    # "albumDate":J
    .end local v14    # "albumId":J
    .end local v18    # "currentDate":Ljava/lang/Long;
    .end local v24    # "isNonUserPromo":Z
    :cond_9
    if-eqz v23, :cond_a

    .line 722
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/RecentItemsManager;->enforceMaxItemsLimit(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 723
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/RecentItemsManager;->populateAlbumAddedReason(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 728
    .end local v22    # "insertValues":Landroid/content/ContentValues;
    .end local v23    # "inserted":Z
    .end local v26    # "mostRecentlyAddedAlbumDate":J
    :cond_a
    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 731
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/RecentItemsManager;->deleteDuplicateNautilusAlbums(Landroid/database/sqlite/SQLiteDatabase;)I

    goto/16 :goto_0

    .line 728
    :catchall_1
    move-exception v2

    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2
.end method

.method public static addRecentlyPlayedItem(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 165
    const/4 v0, 0x0

    .line 166
    .local v0, "added":Z
    instance-of v4, p1, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v4, :cond_2

    move-object v3, p1

    .line 167
    check-cast v3, Lcom/google/android/music/medialist/PlaylistSongList;

    .line 168
    .local v3, "playlist":Lcom/google/android/music/medialist/PlaylistSongList;
    invoke-virtual {v3}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistType()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/music/store/RecentItemsManager;->includePlaylistOfType(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 169
    invoke-virtual {v3}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistId()J

    move-result-wide v4

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {p0, v4, v5, v6, v7}, Lcom/google/android/music/store/RecentItemsManager;->addPlaylist(Landroid/content/Context;JILjava/lang/String;)Z

    move-result v0

    .line 186
    .end local v3    # "playlist":Lcom/google/android/music/medialist/PlaylistSongList;
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 190
    invoke-static {p0}, Lcom/google/android/music/MusicUserContentNotifier;->notifyRecentCanged(Landroid/content/Context;)V

    .line 193
    :cond_1
    return v0

    .line 172
    :cond_2
    instance-of v4, p1, Lcom/google/android/music/medialist/AlbumSongList;

    if-eqz v4, :cond_3

    move-object v1, p1

    .line 173
    check-cast v1, Lcom/google/android/music/medialist/AlbumSongList;

    .line 174
    .local v1, "album":Lcom/google/android/music/medialist/AlbumSongList;
    invoke-virtual {v1, p0}, Lcom/google/android/music/medialist/AlbumSongList;->getAlbumId(Landroid/content/Context;)J

    move-result-wide v4

    invoke-static {p0, v4, v5}, Lcom/google/android/music/store/RecentItemsManager;->addPlayedAlbum(Landroid/content/Context;J)Z

    move-result v0

    .line 175
    goto :goto_0

    .end local v1    # "album":Lcom/google/android/music/medialist/AlbumSongList;
    :cond_3
    instance-of v4, p1, Lcom/google/android/music/medialist/ArtistSongList;

    if-eqz v4, :cond_4

    move-object v2, p1

    .line 176
    check-cast v2, Lcom/google/android/music/medialist/ArtistSongList;

    .line 177
    .local v2, "artist":Lcom/google/android/music/medialist/ArtistSongList;
    invoke-virtual {v2, p0}, Lcom/google/android/music/medialist/ArtistSongList;->getArtistId(Landroid/content/Context;)J

    move-result-wide v4

    invoke-static {p0, v4, v5}, Lcom/google/android/music/store/RecentItemsManager;->addPlayedArtist(Landroid/content/Context;J)Z

    move-result v0

    .line 178
    goto :goto_0

    .end local v2    # "artist":Lcom/google/android/music/medialist/ArtistSongList;
    :cond_4
    instance-of v4, p1, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    if-eqz v4, :cond_0

    move-object v1, p1

    .line 179
    check-cast v1, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    .line 180
    .local v1, "album":Lcom/google/android/music/medialist/NautilusAlbumSongList;
    invoke-static {p0, v1}, Lcom/google/android/music/store/RecentItemsManager;->addPlayedNautilusAlbum(Landroid/content/Context;Lcom/google/android/music/medialist/NautilusAlbumSongList;)Z

    move-result v0

    goto :goto_0
.end method

.method public static addRecentlyPlayedRadio(Landroid/content/Context;J)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "radioStationId"    # J

    .prologue
    .line 205
    invoke-static {p0, p1, p2}, Lcom/google/android/music/store/RecentItemsManager;->addPlayedRadio(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    invoke-static {p0}, Lcom/google/android/music/MusicUserContentNotifier;->notifyRecentCanged(Landroid/content/Context;)V

    .line 207
    const/4 v0, 0x1

    .line 209
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static addRecommendedPlaylist(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 13
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "owner"    # Ljava/lang/String;
    .param p4, "ownerProfileImage"    # Ljava/lang/String;
    .param p5, "shareToken"    # Ljava/lang/String;
    .param p6, "urls"    # Ljava/lang/String;
    .param p7, "description"    # Ljava/lang/String;
    .param p8, "editorArtwork"    # Ljava/lang/String;
    .param p9, "recentReason"    # I
    .param p10, "reasonText"    # Ljava/lang/String;

    .prologue
    .line 566
    invoke-static {p1}, Lcom/google/android/music/store/Store;->computeAccountHash(Landroid/accounts/Account;)I

    move-result v3

    const/16 v7, 0x33

    move-object v2, p0

    move-object v4, p2

    move-object/from16 v5, p7

    move-object/from16 v6, p3

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p4

    move-object/from16 v11, p8

    invoke-static/range {v2 .. v11}, Lcom/google/android/music/store/PlayList;->createPlayList(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/PlayList;

    move-result-object v12

    .line 569
    .local v12, "playlist":Lcom/google/android/music/store/PlayList;
    invoke-virtual {v12}, Lcom/google/android/music/store/PlayList;->getId()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 570
    const-string v2, "RecentItemsManager"

    const-string v3, "Couldn\'t insert playlist recommendation (might already exist as a followed playlist)"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    :goto_0
    return-void

    .line 575
    :cond_0
    invoke-virtual {v12}, Lcom/google/android/music/store/PlayList;->getId()J

    move-result-wide v2

    move/from16 v0, p9

    move-object/from16 v1, p10

    invoke-static {p0, v2, v3, v0, v1}, Lcom/google/android/music/store/RecentItemsManager;->addPlaylist(Landroid/database/sqlite/SQLiteDatabase;JILjava/lang/String;)V

    goto :goto_0
.end method

.method static addRecommendedRadio(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 10
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "seedId"    # Ljava/lang/String;
    .param p3, "seedType"    # I
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "urls"    # Ljava/lang/String;
    .param p6, "description"    # Ljava/lang/String;
    .param p7, "highlightColor"    # Ljava/lang/String;
    .param p8, "profileImage"    # Ljava/lang/String;
    .param p9, "reason"    # I
    .param p10, "reasonText"    # Ljava/lang/String;

    .prologue
    .line 528
    invoke-static/range {p0 .. p8}, Lcom/google/android/music/store/RadioStation;->addRecommendedRadio(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    .line 530
    .local v2, "radioId":J
    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-gez v7, :cond_1

    .line 531
    const-string v7, "RecentItemsManager"

    const-string v8, "Couldn\'t insert radio recommendation (might already exist as a real radio station"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    :cond_0
    :goto_0
    return-void

    .line 536
    :cond_1
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 537
    .local v6, "values":Landroid/content/ContentValues;
    const-string v7, "RecentRadioId"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 538
    const-string v7, "RecentReason"

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 539
    const-string v7, "ItemDate"

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 540
    const-string v7, "RecentReasonText"

    move-object/from16 v0, p10

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    const-string v7, "RECENT"

    const/4 v8, 0x0

    const/4 v9, 0x5

    invoke-virtual {p0, v7, v8, v6, v9}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v4

    .line 544
    .local v4, "recentId":J
    const-wide/16 v8, 0x1

    cmp-long v7, v4, v8

    if-gez v7, :cond_0

    .line 545
    const-string v7, "RecentItemsManager"

    const-string v8, "Failed to insert radio into recent"

    invoke-static {v7, v8}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static cleanUpRecommendations(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v2, 0x0

    .line 952
    const-string v0, "RECENT"

    const-string v1, "RecentReason IN (7,8,4,9)"

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 956
    const-string v0, "RADIO_STATIONS"

    const-string v1, "SourceId IS NULL"

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 958
    const-string v0, "LISTS"

    const-string v1, "ListType=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const/16 v4, 0x33

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 960
    return-void
.end method

.method private static countItems(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 10
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 583
    const/4 v9, 0x0

    .line 584
    .local v9, "count":I
    const-string v1, "RECENT LEFT JOIN MUSIC ON (RecentAlbumId=MUSIC.AlbumId)  LEFT JOIN LISTS ON (RecentListId=LISTS.Id)  LEFT JOIN RADIO_STATIONS ON (RecentRadioId=RADIO_STATIONS.Id) "

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "count(1)"

    aput-object v0, v2, v3

    const-string v3, "LISTS.Id NOT NULL OR MUSIC.AlbumId NOT NULL OR RADIO_STATIONS.Id NOT NULL"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 591
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 595
    :cond_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 598
    return v9

    .line 595
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static countRecentItems(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 123
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    .line 124
    .local v1, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 126
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {v0}, Lcom/google/android/music/store/RecentItemsManager;->countItems(Landroid/database/sqlite/SQLiteDatabase;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 128
    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    return v2

    :catchall_0
    move-exception v2

    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v2
.end method

.method private static deleteDuplicateNautilusAlbums(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 6
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 789
    const-string v1, "RECENT"

    const-string v2, "EXISTS (SELECT 1 FROM RECENT as r2, MUSIC WHERE  r2.RecentAlbumId = MUSIC.AlbumId AND StoreAlbumId=RECENT.RecentNautilusAlbumId AND TrackType =?  AND r2.ItemDate > RECENT.ItemDate LIMIT 1)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 808
    .local v0, "count":I
    sget-boolean v1, Lcom/google/android/music/store/RecentItemsManager;->LOGV:Z

    if-eqz v1, :cond_0

    if-lez v0, :cond_0

    .line 809
    const-string v1, "RecentItemsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Deleted "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " duplicated nautilus albums"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 812
    :cond_0
    return v0
.end method

.method private static deleteItemsAndCloseCursor(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)I
    .locals 4
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v1, 0x0

    .line 877
    if-nez p1, :cond_0

    .line 894
    :goto_0
    return v1

    .line 882
    :cond_0
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 883
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 884
    .local v0, "idsToDelete":Ljava/lang/StringBuffer;
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 885
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 894
    .end local v0    # "idsToDelete":Ljava/lang/StringBuffer;
    :catchall_0
    move-exception v1

    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v1

    .line 887
    .restart local v0    # "idsToDelete":Ljava/lang/StringBuffer;
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 888
    const-string v1, "RECENT"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RecentId IN ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 894
    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v0    # "idsToDelete":Ljava/lang/StringBuffer;
    :cond_2
    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public static deleteNautilusAlbum(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumMetajamId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 220
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 221
    const-string v4, "RecentItemsManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "The metajamId is invalid:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :goto_0
    return v5

    .line 225
    :cond_0
    const/4 v3, 0x0

    .line 226
    .local v3, "success":Z
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v2

    .line 227
    .local v2, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v2}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 229
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v6, "RECENT"

    const-string v7, "RecentNautilusAlbumId=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-virtual {v1, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 232
    .local v0, "count":I
    if-lez v0, :cond_2

    move v3, v4

    .line 234
    :goto_1
    invoke-virtual {v2, v1, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 237
    if-eqz v3, :cond_1

    .line 238
    invoke-static {p0}, Lcom/google/android/music/store/RecentItemsManager;->notifyContentChange(Landroid/content/Context;)V

    :cond_1
    move v5, v3

    .line 241
    goto :goto_0

    :cond_2
    move v3, v5

    .line 232
    goto :goto_1

    .line 234
    .end local v0    # "count":I
    :catchall_0
    move-exception v4

    invoke-virtual {v2, v1, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v4
.end method

.method private static enforceMaxItemsLimit(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 10
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v4, 0x0

    .line 759
    invoke-static {p0}, Lcom/google/android/music/store/RecentItemsManager;->removeInvalidItems(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 763
    const-string v3, "NOT RecentReason IN (7,8,4,9)"

    .line 765
    .local v3, "excludeRecommendations":Ljava/lang/String;
    const-string v1, "RECENT"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v5, "RecentId"

    aput-object v5, v2, v0

    const-string v7, "Priority DESC, ItemDate DESC"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v5, 0xc9

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ",10000"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 774
    .local v9, "c":Landroid/database/Cursor;
    invoke-static {p0, v9}, Lcom/google/android/music/store/RecentItemsManager;->deleteItemsAndCloseCursor(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)I

    .line 775
    return-void
.end method

.method private static getCombinedImageUrls(Lcom/google/android/music/cloudclient/ListenNowItemJson;)Ljava/lang/String;
    .locals 4
    .param p0, "item"    # Lcom/google/android/music/cloudclient/ListenNowItemJson;

    .prologue
    .line 1037
    const/4 v0, 0x0

    .line 1038
    .local v0, "combinedUrls":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/music/cloudclient/ListenNowItemJson;->imageRefs:Ljava/util/List;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/music/cloudclient/ListenNowItemJson;->imageRefs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1039
    iget-object v3, p0, Lcom/google/android/music/cloudclient/ListenNowItemJson;->imageRefs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v2, v3, [Ljava/lang/String;

    .line 1040
    .local v2, "urls":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 1041
    iget-object v3, p0, Lcom/google/android/music/cloudclient/ListenNowItemJson;->imageRefs:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/cloudclient/ImageRefJson;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/ImageRefJson;->mUrl:Ljava/lang/String;

    aput-object v3, v2, v1

    .line 1040
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1043
    :cond_0
    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->encodeStringArray([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1045
    .end local v1    # "i":I
    .end local v2    # "urls":[Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method private static includePlaylistOfType(I)Z
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 247
    sparse-switch p0, :sswitch_data_0

    .line 253
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 251
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 247
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x47 -> :sswitch_0
    .end sparse-switch
.end method

.method private static insertAlbumRecommendation(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/music/cloudclient/ListenNowItemJson;)V
    .locals 10
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "item"    # Lcom/google/android/music/cloudclient/ListenNowItemJson;

    .prologue
    .line 968
    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mAlbum:Lcom/google/android/music/cloudclient/ListenNowAlbumJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ListenNowAlbumJson;->mId:Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;

    iget-object v1, v0, Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;->mMetajamId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mAlbum:Lcom/google/android/music/cloudclient/ListenNowAlbumJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ListenNowAlbumJson;->mId:Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;

    iget-object v2, v0, Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;->mTitle:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/music/store/RecentItemsManager;->getCombinedImageUrls(Lcom/google/android/music/cloudclient/ListenNowItemJson;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mAlbum:Lcom/google/android/music/cloudclient/ListenNowAlbumJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ListenNowAlbumJson;->mId:Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;

    iget-object v4, v0, Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;->mArtist:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mAlbum:Lcom/google/android/music/cloudclient/ListenNowAlbumJson;

    iget-object v5, v0, Lcom/google/android/music/cloudclient/ListenNowAlbumJson;->mArtistMetajamId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mAlbum:Lcom/google/android/music/cloudclient/ListenNowAlbumJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ListenNowAlbumJson;->mArtistProfileImage:Lcom/google/android/music/cloudclient/ImageRefJson;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mAlbum:Lcom/google/android/music/cloudclient/ListenNowAlbumJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ListenNowAlbumJson;->mArtistProfileImage:Lcom/google/android/music/cloudclient/ImageRefJson;

    iget-object v6, v0, Lcom/google/android/music/cloudclient/ImageRefJson;->mUrl:Ljava/lang/String;

    :goto_0
    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mAlbum:Lcom/google/android/music/cloudclient/ListenNowAlbumJson;

    iget-object v7, v0, Lcom/google/android/music/cloudclient/ListenNowAlbumJson;->mDescription:Ljava/lang/String;

    iget v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mSuggestionReason:I

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Mainstage;->translateLockerRecommendationReason(I)I

    move-result v8

    iget-object v9, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mSuggestionText:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v9}, Lcom/google/android/music/store/RecentItemsManager;->addNautilusAlbum(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 979
    return-void

    .line 968
    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static insertLockerRecommendations(Landroid/content/Context;Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "response"    # Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

    .prologue
    .line 908
    if-eqz p1, :cond_0

    iget-object v8, p1, Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;->mListenNowItems:Ljava/util/List;

    if-eqz v8, :cond_0

    iget-object v8, p1, Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;->mListenNowItems:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 910
    :cond_0
    const-string v8, "MusicSyncAdapter"

    const-string v9, "No listen now recommendations"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 945
    :goto_0
    return-void

    .line 913
    :cond_1
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v6

    .line 914
    .local v6, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v6}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 915
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v5, Ljava/lang/Object;

    invoke-direct {v5}, Ljava/lang/Object;-><init>()V

    .line 916
    .local v5, "refObject":Ljava/lang/Object;
    invoke-static {p0, v5}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    .line 917
    .local v4, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v7, 0x0

    .line 919
    .local v7, "success":Z
    :try_start_0
    invoke-static {v0}, Lcom/google/android/music/store/RecentItemsManager;->cleanUpRecommendations(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 920
    const/4 v3, 0x0

    .line 921
    .local v3, "itemsAdded":I
    iget-object v8, p1, Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;->mListenNowItems:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/ListenNowItemJson;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 922
    .local v2, "item":Lcom/google/android/music/cloudclient/ListenNowItemJson;
    const/16 v8, 0x64

    if-ne v3, v8, :cond_3

    .line 939
    .end local v2    # "item":Lcom/google/android/music/cloudclient/ListenNowItemJson;
    :cond_2
    const/4 v7, 0x1

    .line 941
    invoke-virtual {v6, v0, v7}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 942
    invoke-static {v5}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 944
    invoke-static {p0}, Lcom/google/android/music/store/RecentItemsManager;->notifyContentChange(Landroid/content/Context;)V

    goto :goto_0

    .line 925
    .restart local v2    # "item":Lcom/google/android/music/cloudclient/ListenNowItemJson;
    :cond_3
    :try_start_1
    iget v8, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mType:I

    sget-object v9, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;->ALBUM:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;

    invoke-virtual {v9}, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;->getRemoteValue()I

    move-result v9

    if-ne v8, v9, :cond_4

    .line 926
    invoke-static {v0, v2}, Lcom/google/android/music/store/RecentItemsManager;->insertAlbumRecommendation(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/music/cloudclient/ListenNowItemJson;)V

    .line 927
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 928
    :cond_4
    iget v8, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mType:I

    sget-object v9, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;->PLAYLIST:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;

    invoke-virtual {v9}, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;->getRemoteValue()I

    move-result v9

    if-ne v8, v9, :cond_5

    .line 930
    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v8

    invoke-static {v0, v2, v8}, Lcom/google/android/music/store/RecentItemsManager;->insertPlaylistRecommendation(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/music/cloudclient/ListenNowItemJson;Landroid/accounts/Account;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 941
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "item":Lcom/google/android/music/cloudclient/ListenNowItemJson;
    .end local v3    # "itemsAdded":I
    :catchall_0
    move-exception v8

    invoke-virtual {v6, v0, v7}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 942
    invoke-static {v5}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v8

    .line 931
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "item":Lcom/google/android/music/cloudclient/ListenNowItemJson;
    .restart local v3    # "itemsAdded":I
    :cond_5
    :try_start_2
    iget v8, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mType:I

    sget-object v9, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;->RADIO_STATION:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;

    invoke-virtual {v9}, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;->getRemoteValue()I

    move-result v9

    if-ne v8, v9, :cond_6

    .line 933
    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v8

    invoke-static {v0, v2, v8}, Lcom/google/android/music/store/RecentItemsManager;->insertRadioRecommendation(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/music/cloudclient/ListenNowItemJson;Landroid/accounts/Account;)V

    .line 934
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 936
    :cond_6
    const-string v8, "RecentItemsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unknown Locker Recommendation, dropping: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private static insertPlaylistRecommendation(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/music/cloudclient/ListenNowItemJson;Landroid/accounts/Account;)V
    .locals 11
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "item"    # Lcom/google/android/music/cloudclient/ListenNowItemJson;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v0, 0x0

    .line 1015
    iget-object v1, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mPlaylist:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;

    iget-object v2, v1, Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;->mName:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mPlaylist:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;

    iget-object v3, v1, Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;->mOwner:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mPlaylist:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;->mOwnerProfileImage:Lcom/google/android/music/cloudclient/ImageRefJson;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mPlaylist:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;->mOwnerProfileImage:Lcom/google/android/music/cloudclient/ImageRefJson;

    iget-object v4, v1, Lcom/google/android/music/cloudclient/ImageRefJson;->mUrl:Ljava/lang/String;

    :goto_0
    iget-object v1, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mPlaylist:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;->mId:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson$PlaylistIdJson;

    iget-object v5, v1, Lcom/google/android/music/cloudclient/ListenNowPlaylistJson$PlaylistIdJson;->mShareToken:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/music/store/RecentItemsManager;->getCombinedImageUrls(Lcom/google/android/music/cloudclient/ListenNowItemJson;)Ljava/lang/String;

    move-result-object v6

    iget-object v1, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mPlaylist:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;

    iget-object v7, v1, Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;->mDescription:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mPlaylist:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;->mEditorArtwork:Lcom/google/android/music/cloudclient/ImageRefJson;

    if-eqz v1, :cond_1

    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mPlaylist:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;->mEditorArtwork:Lcom/google/android/music/cloudclient/ImageRefJson;

    iget-object v8, v0, Lcom/google/android/music/cloudclient/ImageRefJson;->mUrl:Ljava/lang/String;

    :goto_1
    iget v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mSuggestionReason:I

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Mainstage;->translateLockerRecommendationReason(I)I

    move-result v9

    iget-object v10, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mSuggestionText:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p2

    invoke-static/range {v0 .. v10}, Lcom/google/android/music/store/RecentItemsManager;->addRecommendedPlaylist(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 1028
    return-void

    :cond_0
    move-object v4, v0

    .line 1015
    goto :goto_0

    :cond_1
    move-object v8, v0

    goto :goto_1
.end method

.method private static insertRadioRecommendation(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/music/cloudclient/ListenNowItemJson;Landroid/accounts/Account;)V
    .locals 12
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "item"    # Lcom/google/android/music/cloudclient/ListenNowItemJson;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 990
    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mRadioStation:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;->mId:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;->mSeeds:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mRadioStation:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;->mId:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;->mSeeds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 991
    :cond_0
    const-string v0, "RecentItemsManager"

    const-string v1, "Empty seed for radio recommendation, skipping"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1004
    :goto_0
    return-void

    .line 994
    :cond_1
    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mRadioStation:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;->mId:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;->mSeeds:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/model/RadioSeed;

    invoke-virtual {v0}, Lcom/google/android/music/sync/google/model/RadioSeed;->getSourceIdAndType()Landroid/util/Pair;

    move-result-object v11

    .line 996
    .local v11, "seed":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v2, v11, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v0, v11, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mRadioStation:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;

    iget-object v4, v0, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;->mName:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/music/store/RecentItemsManager;->getCombinedImageUrls(Lcom/google/android/music/cloudclient/ListenNowItemJson;)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mRadioStation:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;

    iget-object v6, v0, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;->mDescription:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mRadioStation:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;

    iget-object v7, v0, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;->mHighlightColor:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mRadioStation:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;->mProfileImage:Lcom/google/android/music/cloudclient/ImageRefJson;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mRadioStation:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;->mProfileImage:Lcom/google/android/music/cloudclient/ImageRefJson;

    iget-object v8, v0, Lcom/google/android/music/cloudclient/ImageRefJson;->mUrl:Ljava/lang/String;

    :goto_1
    iget v0, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mSuggestionReason:I

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Mainstage;->translateLockerRecommendationReason(I)I

    move-result v9

    iget-object v10, p1, Lcom/google/android/music/cloudclient/ListenNowItemJson;->mSuggestionText:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p2

    invoke-static/range {v0 .. v10}, Lcom/google/android/music/store/RecentItemsManager;->addRecommendedRadio(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v8, 0x0

    goto :goto_1
.end method

.method private static notifyContentChange(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 101
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 105
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/google/android/music/store/MusicContent$Recent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 106
    sget-object v1, Lcom/google/android/music/store/MusicContent$Mainstage;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 107
    sget-object v1, Lcom/google/android/music/store/MusicContent$Playlists;->RECENTS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 111
    invoke-static {p0}, Lcom/google/android/music/MusicUserContentNotifier;->notifyRecentCanged(Landroid/content/Context;)V

    .line 112
    return-void
.end method

.method private static populateAlbumAddedReason(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 737
    const-string v0, "UPDATE RECENT SET RecentReason = CASE (SELECT TrackType FROM MUSIC WHERE AlbumId=RecentAlbumId AND FileDate=ItemDate LIMIT 1) WHEN 1 THEN 2 ELSE 3 END  WHERE RecentAlbumId NOT NULL AND RecentReason=0"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 754
    return-void
.end method

.method private static removeInvalidItems(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 818
    const-string v1, "RECENT"

    const-string v2, "(RecentListId NOT NULL AND NOT EXISTS(SELECT LISTS.Id FROM LISTS WHERE LISTS.Id=RecentListId)) OR (RecentAlbumId NOT NULL AND NOT EXISTS(SELECT MUSIC.AlbumId FROM MUSIC WHERE MUSIC.AlbumId=RecentAlbumId AND +Domain=0)) OR (RecentRadioId NOT NULL AND NOT EXISTS(SELECT Id FROM RADIO_STATIONS WHERE Id=RecentRadioId)) OR (RecentListId NOT NULL AND RecentReason IN (7,8,4,9) AND NOT EXISTS(SELECT LISTS.Id FROM LISTS WHERE LISTS.Id=RecentListId AND LISTS.ListType=51)) OR (RecentRadioId NOT NULL AND RecentReason IN (7,8,4,9) AND NOT EXISTS(SELECT Id FROM RADIO_STATIONS WHERE Id=RecentRadioId AND SourceId IS NULL))"

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 863
    .local v0, "howManyDeleted":I
    if-lez v0, :cond_0

    .line 864
    const-string v1, "RecentItemsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Deleted "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " invalid recent items."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 866
    :cond_0
    return-void
.end method

.method static update(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 613
    invoke-static {p1}, Lcom/google/android/music/store/RecentItemsManager;->removeInvalidItems(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 615
    invoke-static {p1}, Lcom/google/android/music/store/RecentItemsManager;->addRecentlyAddedAlbums(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 618
    return-void
.end method

.method public static updateRecentItems(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    const/4 v2, 0x0

    .line 85
    .local v2, "success":Z
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    .line 86
    .local v1, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 88
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {p0, v0}, Lcom/google/android/music/store/RecentItemsManager;->update(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    const/4 v2, 0x1

    .line 91
    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 94
    if-eqz v2, :cond_0

    .line 95
    invoke-static {p0}, Lcom/google/android/music/store/RecentItemsManager;->notifyContentChange(Landroid/content/Context;)V

    .line 97
    :cond_0
    return-void

    .line 91
    :catchall_0
    move-exception v3

    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method public static updateRecentItemsAsync(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    .line 65
    .local v0, "bgWorker":Landroid/os/Handler;
    sget v2, Lcom/google/android/music/store/RecentItemsManager;->UPDATE_RECENT_ITEMS_MESSAGE_TYPE:I

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 67
    new-instance v2, Lcom/google/android/music/store/RecentItemsManager$1;

    invoke-direct {v2, p0}, Lcom/google/android/music/store/RecentItemsManager$1;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;

    move-result-object v1

    .line 73
    .local v1, "msg":Landroid/os/Message;
    sget v2, Lcom/google/android/music/store/RecentItemsManager;->UPDATE_RECENT_ITEMS_MESSAGE_TYPE:I

    iput v2, v1, Landroid/os/Message;->what:I

    .line 74
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 76
    return-void
.end method
