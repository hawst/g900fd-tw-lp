.class public Lcom/google/android/music/ui/UIStateManager;
.super Ljava/lang/Object;
.source "UIStateManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;,
        Lcom/google/android/music/ui/UIStateManager$DownloadabilityChangeListener;,
        Lcom/google/android/music/ui/UIStateManager$StreamabilityChangeListener;,
        Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static sInstance:Lcom/google/android/music/ui/UIStateManager;


# instance fields
.field private final mAccountReceiver:Landroid/content/BroadcastReceiver;

.field private final mApplicationContext:Landroid/content/Context;

.field private mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

.field private mCacheManagerSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

.field private mCachedNautilusStatus:Lcom/google/android/music/NautilusStatus;

.field private mCachedSelectedAccount:Landroid/accounts/Account;

.field private final mCurrentScreenSize:Landroid/graphics/Point;

.field private mCurrentUIVisibility:Z

.field private final mDownloadabilityChangeListener:Lcom/google/android/music/net/IDownloadabilityChangeListener;

.field mDownloadabilityChangeListeners:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/music/ui/UIStateManager$DownloadabilityChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

.field private final mHandler:Landroid/os/Handler;

.field private mIsDownloadingEnabled:Z

.field private mIsMobileConnected:Z

.field private mIsStreamingEnabled:Z

.field private mIsWifiOrEthernetConnected:Z

.field private final mLargestScreenSize:Landroid/graphics/Point;

.field private mMarkUIInvisibileRunnable:Ljava/lang/Runnable;

.field private mMarkUIVisibileRunnable:Ljava/lang/Runnable;

.field private mMediaStoreImportSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

.field private final mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

.field mNetworkChangeListeners:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

.field private final mPlayCommonNetworkStack:Lcom/google/android/play/utils/PlayCommonNetworkStack;

.field private final mPlaybackServiceConnection:Landroid/content/ServiceConnection;

.field private final mPlaybackServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

.field private final mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private mPreferenceChangeListenerRegistered:Z

.field private final mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

.field private mRunOnPlaybackServiceConnected:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final mSmallestScreenSize:Landroid/graphics/Point;

.field private final mStateChangeListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mStoreSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

.field private mStoreService:Lcom/google/android/music/store/IStoreService;

.field private final mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

.field mStreamabilityChangeListeners:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/music/ui/UIStateManager$StreamabilityChangeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/UIStateManager;->LOGV:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mSmallestScreenSize:Landroid/graphics/Point;

    .line 94
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mLargestScreenSize:Landroid/graphics/Point;

    .line 95
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mCurrentScreenSize:Landroid/graphics/Point;

    .line 101
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mStateChangeListeners:Ljava/util/List;

    .line 102
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mRunOnPlaybackServiceConnected:Ljava/util/List;

    .line 114
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mStreamabilityChangeListeners:Ljava/util/Collection;

    .line 115
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mDownloadabilityChangeListeners:Ljava/util/Collection;

    .line 116
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mNetworkChangeListeners:Ljava/util/Collection;

    .line 118
    new-instance v5, Lcom/google/android/music/ui/UIStateManager$1;

    invoke-direct {v5, p0}, Lcom/google/android/music/ui/UIStateManager$1;-><init>(Lcom/google/android/music/ui/UIStateManager;)V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mPlaybackServiceConnection:Landroid/content/ServiceConnection;

    .line 140
    new-instance v5, Lcom/google/android/music/ui/UIStateManager$2;

    invoke-direct {v5, p0}, Lcom/google/android/music/ui/UIStateManager$2;-><init>(Lcom/google/android/music/ui/UIStateManager;)V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

    .line 155
    new-instance v5, Lcom/google/android/music/ui/UIStateManager$3;

    invoke-direct {v5, p0}, Lcom/google/android/music/ui/UIStateManager$3;-><init>(Lcom/google/android/music/ui/UIStateManager;)V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mDownloadabilityChangeListener:Lcom/google/android/music/net/IDownloadabilityChangeListener;

    .line 170
    new-instance v5, Lcom/google/android/music/ui/UIStateManager$4;

    invoke-direct {v5, p0}, Lcom/google/android/music/ui/UIStateManager$4;-><init>(Lcom/google/android/music/ui/UIStateManager;)V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

    .line 191
    new-instance v5, Lcom/google/android/music/ui/UIStateManager$5;

    invoke-direct {v5, p0}, Lcom/google/android/music/ui/UIStateManager$5;-><init>(Lcom/google/android/music/ui/UIStateManager;)V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mMediaStoreImportSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    .line 202
    new-instance v5, Lcom/google/android/music/ui/UIStateManager$6;

    invoke-direct {v5, p0}, Lcom/google/android/music/ui/UIStateManager$6;-><init>(Lcom/google/android/music/ui/UIStateManager;)V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mStoreSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    .line 214
    new-instance v5, Lcom/google/android/music/ui/UIStateManager$7;

    invoke-direct {v5, p0}, Lcom/google/android/music/ui/UIStateManager$7;-><init>(Lcom/google/android/music/ui/UIStateManager;)V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mCacheManagerSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    .line 225
    iput-boolean v4, p0, Lcom/google/android/music/ui/UIStateManager;->mCurrentUIVisibility:Z

    .line 226
    new-instance v5, Lcom/google/android/music/ui/UIStateManager$8;

    invoke-direct {v5, p0}, Lcom/google/android/music/ui/UIStateManager$8;-><init>(Lcom/google/android/music/ui/UIStateManager;)V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mMarkUIVisibileRunnable:Ljava/lang/Runnable;

    .line 232
    new-instance v5, Lcom/google/android/music/ui/UIStateManager$9;

    invoke-direct {v5, p0}, Lcom/google/android/music/ui/UIStateManager$9;-><init>(Lcom/google/android/music/ui/UIStateManager;)V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mMarkUIInvisibileRunnable:Ljava/lang/Runnable;

    .line 241
    new-instance v5, Lcom/google/android/music/ui/UIStateManager$10;

    invoke-direct {v5, p0}, Lcom/google/android/music/ui/UIStateManager$10;-><init>(Lcom/google/android/music/ui/UIStateManager;)V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 254
    new-instance v5, Lcom/google/android/music/ui/UIStateManager$11;

    invoke-direct {v5, p0}, Lcom/google/android/music/ui/UIStateManager$11;-><init>(Lcom/google/android/music/ui/UIStateManager;)V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mAccountReceiver:Landroid/content/BroadcastReceiver;

    .line 263
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 264
    const-string v5, "UIStateManager should be initialized in UI process only"

    invoke-static {p1, v5}, Lcom/google/android/music/utils/MusicUtils;->assertUiProcess(Landroid/content/Context;Ljava/lang/String;)V

    .line 267
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    .line 268
    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    .line 269
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mHandler:Landroid/os/Handler;

    .line 270
    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v5, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    .line 271
    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/music/ui/UIStateManager;->mPlaybackServiceConnection:Landroid/content/ServiceConnection;

    invoke-static {v5, v6}, Lcom/google/android/music/utils/MusicUtils;->bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mPlaybackServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    .line 273
    new-instance v5, Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    iget-object v6, p0, Lcom/google/android/music/ui/UIStateManager;->mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

    iget-object v7, p0, Lcom/google/android/music/ui/UIStateManager;->mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

    iget-object v8, p0, Lcom/google/android/music/ui/UIStateManager;->mDownloadabilityChangeListener:Lcom/google/android/music/net/IDownloadabilityChangeListener;

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;-><init>(Lcom/google/android/music/net/INetworkChangeListener;Lcom/google/android/music/net/IStreamabilityChangeListener;Lcom/google/android/music/net/IDownloadabilityChangeListener;)V

    iput-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .line 275
    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    iget-object v6, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->bindToService(Landroid/content/Context;)V

    .line 278
    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v5}, Lcom/google/android/music/utils/AlbumArtUtils;->setPreferredConfig(Landroid/graphics/Bitmap$Config;)V

    .line 280
    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    iget-object v6, p0, Lcom/google/android/music/ui/UIStateManager;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v5, v6}, Lcom/google/android/music/preferences/MusicPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 281
    iput-boolean v0, p0, Lcom/google/android/music/ui/UIStateManager;->mPreferenceChangeListenerRegistered:Z

    .line 284
    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    const-string v6, "window"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    .line 286
    .local v3, "mgr":Landroid/view/WindowManager;
    if-eqz v3, :cond_0

    .line 288
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 289
    .local v1, "display":Landroid/view/Display;
    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mSmallestScreenSize:Landroid/graphics/Point;

    iget-object v6, p0, Lcom/google/android/music/ui/UIStateManager;->mLargestScreenSize:Landroid/graphics/Point;

    invoke-static {v1, v5, v6}, Lcom/google/android/music/utils/ViewUtils;->getCurrentSizeRange(Landroid/view/Display;Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 292
    .end local v1    # "display":Landroid/view/Display;
    :cond_0
    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mMediaStoreImportSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    iget-object v6, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    new-instance v7, Landroid/content/Intent;

    iget-object v8, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    const-class v9, Lcom/google/android/music/store/MediaStoreImportService;

    invoke-direct {v7, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v6, v7, v0}, Lcom/google/android/music/utils/SafeServiceConnection;->bindService(Landroid/content/Context;Landroid/content/Intent;I)Z

    .line 295
    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mStoreSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    iget-object v6, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    new-instance v7, Landroid/content/Intent;

    iget-object v8, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    const-class v9, Lcom/google/android/music/store/StoreService$StoreServiceBinder;

    invoke-direct {v7, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v6, v7, v0}, Lcom/google/android/music/utils/SafeServiceConnection;->bindService(Landroid/content/Context;Landroid/content/Intent;I)Z

    .line 299
    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mCacheManagerSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    iget-object v6, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    new-instance v7, Landroid/content/Intent;

    iget-object v8, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    const-class v9, Lcom/google/android/music/download/cache/TrackCacheService;

    invoke-direct {v7, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v6, v7, v0}, Lcom/google/android/music/utils/SafeServiceConnection;->bindService(Landroid/content/Context;Landroid/content/Intent;I)Z

    .line 303
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 304
    .local v2, "filter":Landroid/content/IntentFilter;
    const-string v5, "com.google.android.music.accountchanged"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 305
    const-string v5, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 306
    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/music/ui/UIStateManager;->mAccountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v5, v6, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 308
    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->isPlayCacheCleaned()Z

    move-result v5

    if-nez v5, :cond_3

    .line 309
    .local v0, "clearCache":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 310
    const-string v4, "UIStateManager"

    const-string v5, "Clearing play cache"

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_1
    :try_start_0
    new-instance v4, Lcom/google/android/play/utils/PlayCommonNetworkStack;

    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    new-instance v6, Lcom/google/android/music/ui/UIStateManager$12;

    invoke-direct {v6, p0}, Lcom/google/android/music/ui/UIStateManager$12;-><init>(Lcom/google/android/music/ui/UIStateManager;)V

    invoke-direct {v4, v5, v6, v0}, Lcom/google/android/play/utils/PlayCommonNetworkStack;-><init>(Landroid/content/Context;Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;Z)V

    iput-object v4, p0, Lcom/google/android/music/ui/UIStateManager;->mPlayCommonNetworkStack:Lcom/google/android/play/utils/PlayCommonNetworkStack;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    if-eqz v0, :cond_2

    .line 335
    iget-object v4, p0, Lcom/google/android/music/ui/UIStateManager;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->setPlayCacheCleaned()V

    .line 338
    :cond_2
    return-void

    .end local v0    # "clearCache":Z
    :cond_3
    move v0, v4

    .line 308
    goto :goto_0

    .line 334
    .restart local v0    # "clearCache":Z
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_4

    .line 335
    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->setPlayCacheCleaned()V

    :cond_4
    throw v4
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 68
    sget-boolean v0, Lcom/google/android/music/ui/UIStateManager;->LOGV:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/UIStateManager;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mRunOnPlaybackServiceConnected:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/music/ui/UIStateManager;Lcom/google/android/music/store/IStoreService;)Lcom/google/android/music/store/IStoreService;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;
    .param p1, "x1"    # Lcom/google/android/music/store/IStoreService;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/music/ui/UIStateManager;->mStoreService:Lcom/google/android/music/store/IStoreService;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/music/ui/UIStateManager;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/music/ui/UIStateManager;->mRunOnPlaybackServiceConnected:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/google/android/music/ui/UIStateManager;Lcom/google/android/music/download/cache/ICacheManager;)Lcom/google/android/music/download/cache/ICacheManager;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;
    .param p1, "x1"    # Lcom/google/android/music/download/cache/ICacheManager;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/music/ui/UIStateManager;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/google/android/music/ui/UIStateManager;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/UIStateManager;->setUIVisibility(Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/music/ui/UIStateManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/music/ui/UIStateManager;->refreshAccountStatus()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/music/ui/UIStateManager;)Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/music/ui/UIStateManager;)Lcom/google/android/music/NautilusStatus;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mCachedNautilusStatus:Lcom/google/android/music/NautilusStatus;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/google/android/music/ui/UIStateManager;Lcom/google/android/music/NautilusStatus;)Lcom/google/android/music/NautilusStatus;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;
    .param p1, "x1"    # Lcom/google/android/music/NautilusStatus;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/music/ui/UIStateManager;->mCachedNautilusStatus:Lcom/google/android/music/NautilusStatus;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/google/android/music/ui/UIStateManager;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mCachedSelectedAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/google/android/music/ui/UIStateManager;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/music/ui/UIStateManager;->mCachedSelectedAccount:Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/google/android/music/ui/UIStateManager;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mStateChangeListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/UIStateManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/android/music/ui/UIStateManager;->mIsStreamingEnabled:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/music/ui/UIStateManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/google/android/music/ui/UIStateManager;->mIsStreamingEnabled:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/UIStateManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/music/ui/UIStateManager;->notifyStreamabilityChanged()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/UIStateManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/UIStateManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/android/music/ui/UIStateManager;->mIsDownloadingEnabled:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/music/ui/UIStateManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/google/android/music/ui/UIStateManager;->mIsDownloadingEnabled:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/UIStateManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/music/ui/UIStateManager;->notifyDownloadabilityChanged()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/UIStateManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/android/music/ui/UIStateManager;->mIsMobileConnected:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/music/ui/UIStateManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/google/android/music/ui/UIStateManager;->mIsMobileConnected:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/music/ui/UIStateManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/android/music/ui/UIStateManager;->mIsWifiOrEthernetConnected:Z

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/music/ui/UIStateManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/google/android/music/ui/UIStateManager;->mIsWifiOrEthernetConnected:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/UIStateManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/UIStateManager;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/music/ui/UIStateManager;->notifyNetworkChanged()V

    return-void
.end method

.method public static getInstance()Lcom/google/android/music/ui/UIStateManager;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 384
    sget-object v0, Lcom/google/android/music/ui/UIStateManager;->sInstance:Lcom/google/android/music/ui/UIStateManager;

    if-nez v0, :cond_0

    .line 385
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "App state is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387
    :cond_0
    sget-object v0, Lcom/google/android/music/ui/UIStateManager;->sInstance:Lcom/google/android/music/ui/UIStateManager;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 371
    sget-object v0, Lcom/google/android/music/ui/UIStateManager;->sInstance:Lcom/google/android/music/ui/UIStateManager;

    if-nez v0, :cond_0

    .line 372
    new-instance v0, Lcom/google/android/music/ui/UIStateManager;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/UIStateManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/music/ui/UIStateManager;->sInstance:Lcom/google/android/music/ui/UIStateManager;

    .line 374
    :cond_0
    sget-object v0, Lcom/google/android/music/ui/UIStateManager;->sInstance:Lcom/google/android/music/ui/UIStateManager;

    return-object v0
.end method

.method public static isInitialized()Z
    .locals 1

    .prologue
    .line 391
    sget-object v0, Lcom/google/android/music/ui/UIStateManager;->sInstance:Lcom/google/android/music/ui/UIStateManager;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyDownloadabilityChanged()V
    .locals 4

    .prologue
    .line 420
    iget-object v3, p0, Lcom/google/android/music/ui/UIStateManager;->mDownloadabilityChangeListeners:Ljava/util/Collection;

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 423
    .local v2, "listenerSnapshot":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/ui/UIStateManager$DownloadabilityChangeListener;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/UIStateManager$DownloadabilityChangeListener;

    .line 424
    .local v1, "listener":Lcom/google/android/music/ui/UIStateManager$DownloadabilityChangeListener;
    iget-boolean v3, p0, Lcom/google/android/music/ui/UIStateManager;->mIsDownloadingEnabled:Z

    invoke-interface {v1, v3}, Lcom/google/android/music/ui/UIStateManager$DownloadabilityChangeListener;->onDownloadabilityChanged(Z)V

    goto :goto_0

    .line 426
    .end local v1    # "listener":Lcom/google/android/music/ui/UIStateManager$DownloadabilityChangeListener;
    :cond_0
    return-void
.end method

.method private notifyNetworkChanged()V
    .locals 5

    .prologue
    .line 534
    iget-object v3, p0, Lcom/google/android/music/ui/UIStateManager;->mNetworkChangeListeners:Ljava/util/Collection;

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 537
    .local v2, "listenerSnapshot":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;

    .line 538
    .local v1, "listener":Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;
    iget-boolean v3, p0, Lcom/google/android/music/ui/UIStateManager;->mIsMobileConnected:Z

    iget-boolean v4, p0, Lcom/google/android/music/ui/UIStateManager;->mIsWifiOrEthernetConnected:Z

    invoke-interface {v1, v3, v4}, Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;->onNetworkChanged(ZZ)V

    goto :goto_0

    .line 540
    .end local v1    # "listener":Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;
    :cond_0
    return-void
.end method

.method private notifyStreamabilityChanged()V
    .locals 4

    .prologue
    .line 408
    iget-object v3, p0, Lcom/google/android/music/ui/UIStateManager;->mStreamabilityChangeListeners:Ljava/util/Collection;

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 411
    .local v2, "listenerSnapshot":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/ui/UIStateManager$StreamabilityChangeListener;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/UIStateManager$StreamabilityChangeListener;

    .line 412
    .local v1, "listener":Lcom/google/android/music/ui/UIStateManager$StreamabilityChangeListener;
    iget-boolean v3, p0, Lcom/google/android/music/ui/UIStateManager;->mIsStreamingEnabled:Z

    invoke-interface {v1, v3}, Lcom/google/android/music/ui/UIStateManager$StreamabilityChangeListener;->onStreamabilityChanged(Z)V

    goto :goto_0

    .line 414
    .end local v1    # "listener":Lcom/google/android/music/ui/UIStateManager$StreamabilityChangeListener;
    :cond_0
    return-void
.end method

.method private refreshAccountStatus()V
    .locals 2

    .prologue
    .line 657
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/music/ui/UIStateManager$13;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/UIStateManager$13;-><init>(Lcom/google/android/music/ui/UIStateManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 679
    return-void
.end method

.method private setUIVisibility(Z)V
    .locals 6
    .param p1, "visible"    # Z

    .prologue
    .line 609
    iget-boolean v1, p0, Lcom/google/android/music/ui/UIStateManager;->mCurrentUIVisibility:Z

    if-ne v1, p1, :cond_0

    .line 622
    :goto_0
    return-void

    .line 611
    :cond_0
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_1

    .line 612
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1, p1}, Lcom/google/android/music/playback/IMusicPlaybackService;->setUIVisible(Z)V

    .line 613
    iput-boolean p1, p0, Lcom/google/android/music/ui/UIStateManager;->mCurrentUIVisibility:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 619
    :catch_0
    move-exception v0

    .line 620
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "UIStateManager"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 616
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/music/ui/UIStateManager;->mHandler:Landroid/os/Handler;

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/ui/UIStateManager;->mMarkUIVisibileRunnable:Ljava/lang/Runnable;

    :goto_1
    const-wide/16 v4, 0x64

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/UIStateManager;->mMarkUIInvisibileRunnable:Ljava/lang/Runnable;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public addRunOnPlaybackServiceConnected(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 682
    sget-object v0, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v0, :cond_0

    .line 683
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 690
    :goto_0
    return-void

    .line 686
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mRunOnPlaybackServiceConnected:Ljava/util/List;

    if-nez v0, :cond_1

    .line 687
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mRunOnPlaybackServiceConnected:Ljava/util/List;

    .line 689
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mRunOnPlaybackServiceConnected:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final getArtMonitor()Lcom/google/android/music/download/artwork/ArtMonitor;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/download/artwork/ArtMonitorFactory;->getArtMonitor(Landroid/content/Context;)Lcom/google/android/music/download/artwork/ArtMonitor;

    move-result-object v0

    return-object v0
.end method

.method public getCacheManager()Lcom/google/android/music/download/cache/ICacheManager;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    return-object v0
.end method

.method public getPlayCommonNetworkStack()Lcom/google/android/play/utils/PlayCommonNetworkStack;
    .locals 1

    .prologue
    .line 705
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertMainThread()V

    .line 706
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mPlayCommonNetworkStack:Lcom/google/android/play/utils/PlayCommonNetworkStack;

    return-object v0
.end method

.method public getPrefs()Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method public getScreenWidth()I
    .locals 4

    .prologue
    .line 573
    iget-object v2, p0, Lcom/google/android/music/ui/UIStateManager;->mApplicationContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 575
    .local v1, "mgr":Landroid/view/WindowManager;
    if-eqz v1, :cond_0

    .line 577
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 578
    .local v0, "display":Landroid/view/Display;
    iget-object v2, p0, Lcom/google/android/music/ui/UIStateManager;->mCurrentScreenSize:Landroid/graphics/Point;

    invoke-static {v0, v2}, Lcom/google/android/music/utils/ViewUtils;->getSize(Landroid/view/Display;Landroid/graphics/Point;)V

    .line 580
    .end local v0    # "display":Landroid/view/Display;
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/UIStateManager;->mCurrentScreenSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    return v2
.end method

.method public getStoreService()Lcom/google/android/music/store/IStoreService;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mStoreService:Lcom/google/android/music/store/IStoreService;

    return-object v0
.end method

.method public isDownloadedOnlyMode()Z
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v0

    return v0
.end method

.method public isMobileConnected()Z
    .locals 1

    .prologue
    .line 523
    iget-boolean v0, p0, Lcom/google/android/music/ui/UIStateManager;->mIsMobileConnected:Z

    return v0
.end method

.method public isNetworkConnected()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 509
    iget-object v3, p0, Lcom/google/android/music/ui/UIStateManager;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    if-eqz v3, :cond_0

    .line 510
    iget-object v3, p0, Lcom/google/android/music/ui/UIStateManager;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v3}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;

    move-result-object v1

    .line 512
    .local v1, "monitor":Lcom/google/android/music/net/INetworkMonitor;
    if-eqz v1, :cond_0

    :try_start_0
    invoke-interface {v1}, Lcom/google/android/music/net/INetworkMonitor;->isConnected()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 518
    .end local v1    # "monitor":Lcom/google/android/music/net/INetworkMonitor;
    :cond_0
    :goto_0
    return v2

    .line 513
    .restart local v1    # "monitor":Lcom/google/android/music/net/INetworkMonitor;
    :catch_0
    move-exception v0

    .line 514
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "UIStateManager"

    const-string v4, ""

    invoke-static {v3, v4, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public isStreamingEnabled()Z
    .locals 1

    .prologue
    .line 402
    iget-boolean v0, p0, Lcom/google/android/music/ui/UIStateManager;->mIsStreamingEnabled:Z

    return v0
.end method

.method public isWifiOrEthernetConnected()Z
    .locals 1

    .prologue
    .line 527
    iget-boolean v0, p0, Lcom/google/android/music/ui/UIStateManager;->mIsWifiOrEthernetConnected:Z

    return v0
.end method

.method public onConfigChange(I)V
    .locals 1
    .param p1, "configType"    # I

    .prologue
    .line 651
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 652
    invoke-direct {p0}, Lcom/google/android/music/ui/UIStateManager;->refreshAccountStatus()V

    .line 654
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/music/ui/UIStateManager;->mMarkUIInvisibileRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 606
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 595
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/music/ui/UIStateManager;->mMarkUIInvisibileRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 596
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/music/ui/UIStateManager;->mMarkUIVisibileRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 597
    return-void
.end method

.method public registerNetworkChangeListener(Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mNetworkChangeListeners:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 551
    return-void
.end method

.method public registerUIStateChangeListener(Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    .prologue
    .line 635
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 636
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mStateChangeListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 637
    return-void
.end method

.method public removeRunOnPlaybackServiceConnected(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 693
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mRunOnPlaybackServiceConnected:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 695
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mRunOnPlaybackServiceConnected:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 698
    :cond_1
    return-void
.end method

.method public unregisterNetworkChangeListener(Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mNetworkChangeListeners:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 559
    return-void
.end method

.method public unregisterUIStateChangeListener(Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    .prologue
    .line 643
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 644
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager;->mStateChangeListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 645
    return-void
.end method
