.class public Lcom/google/android/music/leanback/Item$ConstantStringGetter;
.super Ljava/lang/Object;
.source "Item.java"

# interfaces
.implements Lcom/google/android/music/leanback/Item$StringGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConstantStringGetter"
.end annotation


# instance fields
.field private final mString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/google/android/music/leanback/Item$ConstantStringGetter;->mString:Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method public getString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/music/leanback/Item$ConstantStringGetter;->mString:Ljava/lang/String;

    return-object v0
.end method
