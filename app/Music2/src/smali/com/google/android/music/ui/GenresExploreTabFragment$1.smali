.class Lcom/google/android/music/ui/GenresExploreTabFragment$1;
.super Ljava/lang/Object;
.source "GenresExploreTabFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/GenresExploreTabFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mArtUrls:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/ui/GenresExploreTabFragment;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$genreId:Ljava/lang/String;

.field final synthetic val$genreName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/GenresExploreTabFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/music/ui/GenresExploreTabFragment$1;->this$0:Lcom/google/android/music/ui/GenresExploreTabFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/GenresExploreTabFragment$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/music/ui/GenresExploreTabFragment$1;->val$genreId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/music/ui/GenresExploreTabFragment$1;->val$genreName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/music/ui/GenresExploreTabFragment$1;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/ui/GenresExploreTabFragment$1;->val$genreId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/MusicUtils;->getArtUrlsForGenreRadio(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/GenresExploreTabFragment$1;->mArtUrls:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public taskCompleted()V
    .locals 4

    .prologue
    .line 82
    iget-object v1, p0, Lcom/google/android/music/ui/GenresExploreTabFragment$1;->this$0:Lcom/google/android/music/ui/GenresExploreTabFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/GenresExploreTabFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 83
    .local v0, "activity":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 84
    iget-object v1, p0, Lcom/google/android/music/ui/GenresExploreTabFragment$1;->val$genreName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/ui/GenresExploreTabFragment$1;->val$genreId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/music/ui/GenresExploreTabFragment$1;->mArtUrls:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/MusicUtils;->playGenreRadio(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :cond_0
    return-void
.end method
