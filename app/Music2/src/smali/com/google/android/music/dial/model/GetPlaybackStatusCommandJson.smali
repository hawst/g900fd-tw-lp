.class public Lcom/google/android/music/dial/model/GetPlaybackStatusCommandJson;
.super Lcom/google/android/music/dial/model/GenericDialCommand;
.source "GetPlaybackStatusCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/model/GetPlaybackStatusCommandJson$GetPlaybackStatusCommandBody;
    }
.end annotation


# instance fields
.field public mBody:Lcom/google/android/music/dial/model/GetPlaybackStatusCommandJson$GetPlaybackStatusCommandBody;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "body"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/music/dial/model/GenericDialCommand;-><init>()V

    .line 25
    iget-object v0, p0, Lcom/google/android/music/dial/model/GetPlaybackStatusCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "playback:1"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mNamespace:Ljava/lang/String;

    .line 26
    iget-object v0, p0, Lcom/google/android/music/dial/model/GetPlaybackStatusCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "getPlaybackStatus"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mCommand:Ljava/lang/String;

    .line 28
    new-instance v0, Lcom/google/android/music/dial/model/GetPlaybackStatusCommandJson$GetPlaybackStatusCommandBody;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/GetPlaybackStatusCommandJson$GetPlaybackStatusCommandBody;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/dial/model/GetPlaybackStatusCommandJson;->mBody:Lcom/google/android/music/dial/model/GetPlaybackStatusCommandJson$GetPlaybackStatusCommandBody;

    .line 29
    return-void
.end method


# virtual methods
.method protected getBodyAsJsonString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/dial/model/GetPlaybackStatusCommandJson;->mBody:Lcom/google/android/music/dial/model/GetPlaybackStatusCommandJson$GetPlaybackStatusCommandBody;

    invoke-virtual {v0}, Lcom/google/android/music/dial/model/GetPlaybackStatusCommandJson$GetPlaybackStatusCommandBody;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
