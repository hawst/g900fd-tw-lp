.class Lcom/google/android/music/leanback/LeanbackItemActivity$2;
.super Ljava/lang/Object;
.source "LeanbackItemActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackItemActivity;->loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackItemActivity;

.field final synthetic val$adapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$projection:[Ljava/lang/String;

.field final synthetic val$uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackItemActivity;Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Landroid/support/v17/leanback/widget/CursorObjectAdapter;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackItemActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$2;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$2;->val$uri:Landroid/net/Uri;

    iput-object p4, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$2;->val$projection:[Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$2;->val$adapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 157
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$2;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$2;->val$uri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$2;->val$projection:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 164
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$2;->val$adapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    invoke-virtual {v0, p2}, Landroid/support/v17/leanback/widget/CursorObjectAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 165
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 154
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackItemActivity$2;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 172
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$2;->val$adapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/CursorObjectAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 173
    return-void
.end method
