.class Lcom/google/android/music/download/AbstractSchedulingService$1;
.super Lcom/google/android/music/utils/SafeServiceConnection;
.source "AbstractSchedulingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/AbstractSchedulingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/AbstractSchedulingService;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/AbstractSchedulingService;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/music/download/AbstractSchedulingService$1;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-direct {p0}, Lcom/google/android/music/utils/SafeServiceConnection;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$1;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-static {p2}, Lcom/google/android/music/download/IDownloadQueueManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/download/IDownloadQueueManager;

    move-result-object v1

    # setter for: Lcom/google/android/music/download/AbstractSchedulingService;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;
    invoke-static {v0, v1}, Lcom/google/android/music/download/AbstractSchedulingService;->access$002(Lcom/google/android/music/download/AbstractSchedulingService;Lcom/google/android/music/download/IDownloadQueueManager;)Lcom/google/android/music/download/IDownloadQueueManager;

    .line 119
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$1;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->checkDependentServices()V
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$100(Lcom/google/android/music/download/AbstractSchedulingService;)V

    .line 120
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 125
    return-void
.end method
