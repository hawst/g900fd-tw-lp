.class Lcom/google/android/music/art/ArtKey;
.super Ljava/lang/Object;
.source "ArtKey.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mDescriptor:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mType:Lcom/google/android/music/art/ArtType;


# direct methods
.method constructor <init>(Lcom/google/android/music/art/ArtType;Ljava/lang/Object;)V
    .locals 0
    .param p1, "type"    # Lcom/google/android/music/art/ArtType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtType;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 9
    .local p0, "this":Lcom/google/android/music/art/ArtKey;, "Lcom/google/android/music/art/ArtKey<TT;>;"
    .local p2, "descriptor":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/google/android/music/art/ArtKey;->mType:Lcom/google/android/music/art/ArtType;

    .line 11
    iput-object p2, p0, Lcom/google/android/music/art/ArtKey;->mDescriptor:Ljava/lang/Object;

    .line 12
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/google/android/music/art/ArtKey;, "Lcom/google/android/music/art/ArtKey<TT;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 16
    if-ne p0, p1, :cond_1

    .line 27
    :cond_0
    :goto_0
    return v1

    .line 17
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 19
    check-cast v0, Lcom/google/android/music/art/ArtKey;

    .line 21
    .local v0, "artKey":Lcom/google/android/music/art/ArtKey;
    iget-object v3, p0, Lcom/google/android/music/art/ArtKey;->mDescriptor:Ljava/lang/Object;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/music/art/ArtKey;->mDescriptor:Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/music/art/ArtKey;->mDescriptor:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_4
    move v1, v2

    .line 23
    goto :goto_0

    .line 21
    :cond_5
    iget-object v3, v0, Lcom/google/android/music/art/ArtKey;->mDescriptor:Ljava/lang/Object;

    if-nez v3, :cond_4

    .line 25
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/art/ArtKey;->mType:Lcom/google/android/music/art/ArtType;

    iget-object v4, v0, Lcom/google/android/music/art/ArtKey;->mType:Lcom/google/android/music/art/ArtType;

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .local p0, "this":Lcom/google/android/music/art/ArtKey;, "Lcom/google/android/music/art/ArtKey<TT;>;"
    const/4 v1, 0x0

    .line 32
    iget-object v2, p0, Lcom/google/android/music/art/ArtKey;->mType:Lcom/google/android/music/art/ArtType;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/music/art/ArtKey;->mType:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v2}, Lcom/google/android/music/art/ArtType;->hashCode()I

    move-result v0

    .line 33
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/music/art/ArtKey;->mDescriptor:Ljava/lang/Object;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/google/android/music/art/ArtKey;->mDescriptor:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 34
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 32
    goto :goto_0
.end method
