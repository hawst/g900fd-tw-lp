.class Lcom/google/android/music/xdi/NowPlayingCursor;
.super Landroid/database/MatrixCursor;
.source "NowPlayingCursor.java"


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 18
    sget-object v4, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {p0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 20
    new-instance v2, Lcom/google/android/music/xdi/ProjectionMap;

    sget-object v4, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {v2, v4}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    .line 22
    .local v2, "projectionMap":Lcom/google/android/music/xdi/ProjectionMap;
    sget-object v4, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    array-length v4, v4

    new-array v3, v4, [Ljava/lang/Object;

    .line 25
    .local v3, "values":[Ljava/lang/Object;
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "container"

    const/16 v6, 0x17

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 29
    .local v0, "intent":Landroid/content/Intent;
    const v4, 0x7f0b0095

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 31
    .local v1, "name":Ljava/lang/String;
    const-string v4, "_id"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 32
    const-string v4, "display_name"

    invoke-virtual {v2, v3, v4, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 33
    const-string v4, "display_description"

    invoke-virtual {v2, v3, v4, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 34
    const-string v4, "image_uri"

    invoke-virtual {v2, v3, v4, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 35
    const-string v4, "width"

    invoke-virtual {v2, v3, v4, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 36
    const-string v4, "height"

    invoke-virtual {v2, v3, v4, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 37
    const-string v4, "intent_uri"

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 39
    invoke-virtual {p0, v3}, Lcom/google/android/music/xdi/NowPlayingCursor;->addRow([Ljava/lang/Object;)V

    .line 40
    return-void
.end method
