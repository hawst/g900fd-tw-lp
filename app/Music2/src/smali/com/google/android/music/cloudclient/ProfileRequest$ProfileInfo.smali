.class public Lcom/google/android/music/cloudclient/ProfileRequest$ProfileInfo;
.super Lcom/google/api/client/json/GenericJson;
.source "ProfileRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/ProfileRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProfileInfo"
.end annotation


# instance fields
.field public mFamilyName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "familyName"
    .end annotation
.end field

.field public mGivenName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "givenName"
    .end annotation
.end field

.field public mPhotoUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "photoUrl"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method

.method public static parse([B)Lcom/google/android/music/cloudclient/ProfileRequest$ProfileInfo;
    .locals 6
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    :try_start_0
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p0}, Ljava/lang/String;-><init>([B)V

    .line 44
    .local v2, "txt":Ljava/lang/String;
    const-string v3, "ProfileRequest"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    sget-object v3, Lcom/google/api/client/json/Json;->JSON_FACTORY:Lorg/codehaus/jackson/JsonFactory;

    invoke-virtual {v3, p0}, Lorg/codehaus/jackson/JsonFactory;->createJsonParser([B)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    .line 46
    .local v1, "parser":Lorg/codehaus/jackson/JsonParser;
    invoke-virtual {v1}, Lorg/codehaus/jackson/JsonParser;->nextToken()Lorg/codehaus/jackson/JsonToken;

    .line 47
    const-class v3, Lcom/google/android/music/cloudclient/ProfileRequest$ProfileInfo;

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, Lcom/google/api/client/json/Json;->parse(Lorg/codehaus/jackson/JsonParser;Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/cloudclient/ProfileRequest$ProfileInfo;
    :try_end_0
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 48
    .end local v1    # "parser":Lorg/codehaus/jackson/JsonParser;
    .end local v2    # "txt":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 49
    .local v0, "je":Lorg/codehaus/jackson/JsonParseException;
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to parse preview: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonParseException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
.end method
