.class public Lcom/google/android/music/dial/SonosGroupInfo;
.super Ljava/lang/Object;
.source "SonosGroupInfo.java"


# instance fields
.field private mGroupId:Ljava/lang/String;

.field private mGroupName:Ljava/lang/String;

.field private mIsGroupCoordinator:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/google/android/music/dial/SonosGroupInfo;
    .locals 14
    .param p0, "groupinfoHeader"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x2

    const/4 v7, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 41
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 42
    const-string v8, "SonosGroupInfo"

    const-string v9, "groupinfo should not empty or null."

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    .line 74
    :cond_0
    :goto_0
    return-object v6

    .line 46
    :cond_1
    const-string v8, ";"

    invoke-static {p0, v8}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 48
    .local v2, "attributes":[Ljava/lang/String;
    array-length v8, v2

    const/4 v9, 0x3

    if-ge v8, v9, :cond_2

    .line 49
    const-string v8, "SonosGroupInfo"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "groupinfo should have atleast 3 attributes separated by semicolons.  "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    .line 52
    goto :goto_0

    .line 55
    :cond_2
    new-instance v6, Lcom/google/android/music/dial/SonosGroupInfo;

    invoke-direct {v6}, Lcom/google/android/music/dial/SonosGroupInfo;-><init>()V

    .line 57
    .local v6, "sgi":Lcom/google/android/music/dial/SonosGroupInfo;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_0

    aget-object v1, v0, v3

    .line 58
    .local v1, "attr":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    const-string v9, "="

    invoke-virtual {v8, v9, v13}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v4

    .line 59
    .local v4, "keyValue":[Ljava/lang/String;
    array-length v8, v4

    if-eq v8, v13, :cond_3

    .line 60
    const-string v8, "SonosGroupInfo"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "groupinfo attribute is not of the form key=value.  "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    .line 61
    goto :goto_0

    .line 63
    :cond_3
    aget-object v8, v4, v12

    const-string v9, "gc"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 64
    aget-object v8, v4, v11

    const-string v9, "1"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 65
    iput-boolean v11, v6, Lcom/google/android/music/dial/SonosGroupInfo;->mIsGroupCoordinator:Z

    .line 57
    :cond_4
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 67
    :cond_5
    aget-object v8, v4, v12

    const-string v9, "gid"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 68
    aget-object v8, v4, v11

    iput-object v8, v6, Lcom/google/android/music/dial/SonosGroupInfo;->mGroupId:Ljava/lang/String;

    goto :goto_2

    .line 69
    :cond_6
    aget-object v8, v4, v12

    const-string v9, "gname"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 70
    aget-object v8, v4, v11

    const-string v9, "\""

    const-string v10, ""

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/google/android/music/dial/SonosGroupInfo;->mGroupName:Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method public getGroupId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/music/dial/SonosGroupInfo;->mGroupId:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/music/dial/SonosGroupInfo;->mGroupName:Ljava/lang/String;

    return-object v0
.end method

.method public getIsGroupCoordinator()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/android/music/dial/SonosGroupInfo;->mIsGroupCoordinator:Z

    return v0
.end method
