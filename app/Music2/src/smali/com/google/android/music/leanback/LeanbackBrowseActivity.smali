.class public abstract Lcom/google/android/music/leanback/LeanbackBrowseActivity;
.super Lcom/google/android/music/leanback/LeanbackSearchOrbItemActivity;
.source "LeanbackBrowseActivity.java"


# instance fields
.field private mItemPresenter:Lcom/google/android/music/leanback/ItemPresenter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackSearchOrbItemActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected createFragment()Landroid/app/Fragment;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/music/leanback/LeanbackBrowseFragment;

    invoke-direct {v0}, Lcom/google/android/music/leanback/LeanbackBrowseFragment;-><init>()V

    .line 57
    .local v0, "browseFragment":Lcom/google/android/music/leanback/LeanbackBrowseFragment;
    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->setupFragment(Landroid/app/Fragment;)V

    .line 58
    return-object v0
.end method

.method protected getBrowseTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->mItemPresenter:Lcom/google/android/music/leanback/ItemPresenter;

    return-object v0
.end method

.method protected abstract getListRowsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v1

    .line 43
    .local v1, "f":Landroid/app/Fragment;
    instance-of v2, v1, Landroid/support/v17/leanback/app/BrowseFragment;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 44
    check-cast v0, Landroid/support/v17/leanback/app/BrowseFragment;

    .line 45
    .local v0, "browseFragment":Landroid/support/v17/leanback/app/BrowseFragment;
    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BrowseFragment;->isShowingHeaders()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BrowseFragment;->isInHeadersTransition()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BrowseFragment;->getHeadersState()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    .line 47
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/app/BrowseFragment;->startHeadersTransition(Z)V

    .line 52
    .end local v0    # "browseFragment":Landroid/support/v17/leanback/app/BrowseFragment;
    :goto_0
    return-void

    .line 51
    :cond_0
    invoke-super {p0}, Lcom/google/android/music/leanback/LeanbackSearchOrbItemActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 33
    .local v6, "r":Landroid/content/res/Resources;
    new-instance v0, Lcom/google/android/music/leanback/ItemPresenter;

    const/4 v2, 0x0

    const v1, 0x7f0f016a

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v1, 0x7f0f016d

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const v1, 0x7f0f016e

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/ItemPresenter;-><init>(Landroid/content/Context;ZIII)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->mItemPresenter:Lcom/google/android/music/leanback/ItemPresenter;

    .line 37
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/LeanbackSearchOrbItemActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    return-void
.end method

.method protected setupFragment(Landroid/app/Fragment;)V
    .locals 4
    .param p1, "fragment"    # Landroid/app/Fragment;

    .prologue
    .line 63
    move-object v0, p1

    check-cast v0, Lcom/google/android/music/leanback/LeanbackBrowseFragment;

    .line 65
    .local v0, "browseFragment":Lcom/google/android/music/leanback/LeanbackBrowseFragment;
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->getBrowseTitle()Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, "browseTitle":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/leanback/LeanbackBrowseFragment;->setBadgeDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 73
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->getSearchOrbViewColors(Landroid/content/res/Resources;)Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/leanback/LeanbackBrowseFragment;->setSearchAffordanceColors(Landroid/support/v17/leanback/widget/SearchOrbView$Colors;)V

    .line 74
    invoke-virtual {v0, p0}, Lcom/google/android/music/leanback/LeanbackBrowseFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 75
    invoke-virtual {v0, p0}, Lcom/google/android/music/leanback/LeanbackBrowseFragment;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 76
    invoke-virtual {v0, p0}, Lcom/google/android/music/leanback/LeanbackBrowseFragment;->setOnSearchClickedListener(Landroid/view/View$OnClickListener;)V

    .line 77
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->getListRowsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/leanback/LeanbackBrowseFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 78
    return-void

    .line 70
    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackBrowseFragment;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method
