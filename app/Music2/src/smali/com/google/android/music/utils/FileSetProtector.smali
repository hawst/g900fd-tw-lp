.class public Lcom/google/android/music/utils/FileSetProtector;
.super Ljava/lang/Object;
.source "FileSetProtector.java"


# instance fields
.field private final mAbsolutePaths:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mAbsolutePathsLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/utils/FileSetProtector;->mAbsolutePathsLock:Ljava/lang/Object;

    .line 24
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/music/utils/FileSetProtector;->mAbsolutePaths:Ljava/util/Set;

    .line 25
    return-void
.end method


# virtual methods
.method public deleteFileIfPossible(Ljava/lang/String;)Z
    .locals 5
    .param p1, "absolutePath"    # Ljava/lang/String;

    .prologue
    .line 32
    iget-object v2, p0, Lcom/google/android/music/utils/FileSetProtector;->mAbsolutePathsLock:Ljava/lang/Object;

    monitor-enter v2

    .line 33
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/utils/FileSetProtector;->mAbsolutePaths:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34
    const/4 v0, 0x0

    monitor-exit v2

    .line 40
    :goto_0
    return v0

    .line 36
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    .line 37
    .local v0, "result":Z
    if-nez v0, :cond_1

    .line 38
    const-string v1, "FileSetProtector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not delete file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    :cond_1
    monitor-exit v2

    goto :goto_0

    .line 42
    .end local v0    # "result":Z
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerAbsolutePath(Ljava/lang/String;)V
    .locals 2
    .param p1, "absolutePath"    # Ljava/lang/String;

    .prologue
    .line 49
    iget-object v1, p0, Lcom/google/android/music/utils/FileSetProtector;->mAbsolutePathsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 50
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/utils/FileSetProtector;->mAbsolutePaths:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 51
    monitor-exit v1

    .line 52
    return-void

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unRegisterAbsolutePath(Ljava/lang/String;)V
    .locals 2
    .param p1, "absolutePath"    # Ljava/lang/String;

    .prologue
    .line 59
    iget-object v1, p0, Lcom/google/android/music/utils/FileSetProtector;->mAbsolutePathsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/utils/FileSetProtector;->mAbsolutePaths:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 61
    monitor-exit v1

    .line 62
    return-void

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
