.class public final enum Lcom/google/android/music/mix/MixDescriptor$Type;
.super Ljava/lang/Enum;
.source "MixDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/mix/MixDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/mix/MixDescriptor$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/mix/MixDescriptor$Type;

.field public static final enum ALBUM_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

.field public static final enum ARTIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

.field public static final enum ARTIST_SHUFFLE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

.field public static final enum CURATED_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

.field public static final enum GENRE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

.field public static final enum LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

.field public static final enum PLAYLIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

.field public static final enum RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

.field public static final enum TRACK_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 28
    new-instance v0, Lcom/google/android/music/mix/MixDescriptor$Type;

    const-string v1, "RADIO"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/mix/MixDescriptor$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 29
    new-instance v0, Lcom/google/android/music/mix/MixDescriptor$Type;

    const-string v1, "TRACK_SEED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/mix/MixDescriptor$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->TRACK_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 30
    new-instance v0, Lcom/google/android/music/mix/MixDescriptor$Type;

    const-string v1, "ALBUM_SEED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/mix/MixDescriptor$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->ALBUM_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 31
    new-instance v0, Lcom/google/android/music/mix/MixDescriptor$Type;

    const-string v1, "ARTIST_SEED"

    invoke-direct {v0, v1, v6}, Lcom/google/android/music/mix/MixDescriptor$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->ARTIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 32
    new-instance v0, Lcom/google/android/music/mix/MixDescriptor$Type;

    const-string v1, "GENRE_SEED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/music/mix/MixDescriptor$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->GENRE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 33
    new-instance v0, Lcom/google/android/music/mix/MixDescriptor$Type;

    const-string v1, "LUCKY_RADIO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/mix/MixDescriptor$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 34
    new-instance v0, Lcom/google/android/music/mix/MixDescriptor$Type;

    const-string v1, "ARTIST_SHUFFLE_SEED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/mix/MixDescriptor$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->ARTIST_SHUFFLE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 35
    new-instance v0, Lcom/google/android/music/mix/MixDescriptor$Type;

    const-string v1, "PLAYLIST_SEED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/mix/MixDescriptor$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->PLAYLIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 36
    new-instance v0, Lcom/google/android/music/mix/MixDescriptor$Type;

    const-string v1, "CURATED_SEED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/mix/MixDescriptor$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->CURATED_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 27
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/music/mix/MixDescriptor$Type;

    sget-object v1, Lcom/google/android/music/mix/MixDescriptor$Type;->RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/mix/MixDescriptor$Type;->TRACK_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/mix/MixDescriptor$Type;->ALBUM_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/mix/MixDescriptor$Type;->ARTIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/mix/MixDescriptor$Type;->GENRE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/music/mix/MixDescriptor$Type;->ARTIST_SHUFFLE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/music/mix/MixDescriptor$Type;->PLAYLIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/music/mix/MixDescriptor$Type;->CURATED_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->$VALUES:[Lcom/google/android/music/mix/MixDescriptor$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/mix/MixDescriptor$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 27
    const-class v0, Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/mix/MixDescriptor$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/mix/MixDescriptor$Type;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->$VALUES:[Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v0}, [Lcom/google/android/music/mix/MixDescriptor$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/mix/MixDescriptor$Type;

    return-object v0
.end method


# virtual methods
.method public isPersistentSeed()Z
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->TRACK_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->ALBUM_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->ARTIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->GENRE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->PLAYLIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->CURATED_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
