.class final Lcom/google/android/music/utils/MusicUtils$6;
.super Ljava/lang/Object;
.source "MusicUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/utils/MusicUtils;->performDelete(Ljava/lang/Long;ZLcom/google/android/music/medialist/SongList;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$finalUri:Landroid/net/Uri;

.field final synthetic val$resolver:Landroid/content/ContentResolver;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2393
    iput-object p1, p0, Lcom/google/android/music/utils/MusicUtils$6;->val$resolver:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/google/android/music/utils/MusicUtils$6;->val$finalUri:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2396
    iget-object v0, p0, Lcom/google/android/music/utils/MusicUtils$6;->val$resolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/music/utils/MusicUtils$6;->val$finalUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 2397
    const-string v0, "MusicUtils"

    const-string v1, "Could not remove item from playlist"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2399
    :cond_0
    return-void
.end method
