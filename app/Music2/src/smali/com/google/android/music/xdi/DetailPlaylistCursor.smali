.class Lcom/google/android/music/xdi/DetailPlaylistCursor;
.super Landroid/database/MatrixCursor;
.source "DetailPlaylistCursor.java"


# static fields
.field private static final PROJECTION_PLAYLISTS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mImageHeight:I

.field private final mImageWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "playlist_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->PROJECTION_PLAYLISTS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "playlistId"    # J

    .prologue
    .line 37
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 38
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mContext:Landroid/content/Context;

    .line 39
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 41
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mImageWidth:I

    .line 42
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mImageHeight:I

    .line 44
    invoke-direct {p0, p3, p4}, Lcom/google/android/music/xdi/DetailPlaylistCursor;->addRowForPlaylist(J)V

    .line 45
    return-void
.end method

.method private addRowForPlaylist(J)V
    .locals 11
    .param p1, "playlistId"    # J

    .prologue
    .line 48
    iget-object v6, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mContext:Landroid/content/Context;

    sget-object v7, Lcom/google/android/music/xdi/DetailPlaylistCursor;->PROJECTION_PLAYLISTS:[Ljava/lang/String;

    invoke-static {v6, p1, p2, v7}, Lcom/google/android/music/xdi/XdiUtils;->getPlaylistCursor(Landroid/content/Context;J[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 50
    .local v3, "cursor":Landroid/database/Cursor;
    if-nez v3, :cond_0

    .line 79
    :goto_0
    return-void

    .line 55
    :cond_0
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 56
    iget-object v6, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v6}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v6

    new-array v5, v6, [Ljava/lang/Object;

    .line 57
    .local v5, "values":[Ljava/lang/Object;
    const/4 v6, 0x0

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 59
    .local v4, "name":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/music/xdi/XdiUtils;->getDisplayDimensions(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v2

    .line 60
    .local v2, "backgroundDimensionsPx":Landroid/graphics/Point;
    iget v6, v2, Landroid/graphics/Point;->x:I

    iget v7, v2, Landroid/graphics/Point;->y:I

    invoke-static {p1, p2, v6, v7}, Lcom/google/android/music/store/MusicContent$PlaylistArt;->getPlaylistArtUri(JII)Landroid/net/Uri;

    move-result-object v1

    .line 62
    .local v1, "backgroundArtUri":Landroid/net/Uri;
    iget v6, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mImageWidth:I

    iget v7, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mImageHeight:I

    invoke-static {p1, p2, v6, v7}, Lcom/google/android/music/store/MusicContent$PlaylistArt;->getPlaylistArtUri(JII)Landroid/net/Uri;

    move-result-object v0

    .line 64
    .local v0, "artUri":Landroid/net/Uri;
    iget-object v6, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v7, "_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v5, v7, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    iget-object v6, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v7, "foreground_image_uri"

    invoke-virtual {v6, v5, v7, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 66
    iget-object v6, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v7, "background_image_uri"

    invoke-virtual {v6, v5, v7, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 68
    iget-object v6, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v7, "badge_uri"

    iget-object v8, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultBadgeUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v5, v7, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 70
    iget-object v6, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v7, "color_hint"

    iget-object v8, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0c00a6

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v5, v7, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 72
    iget-object v6, p0, Lcom/google/android/music/xdi/DetailPlaylistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v7, "text_color_hint"

    const/4 v8, 0x0

    invoke-virtual {v6, v5, v7, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 74
    invoke-virtual {p0, v5}, Lcom/google/android/music/xdi/DetailPlaylistCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    .end local v0    # "artUri":Landroid/net/Uri;
    .end local v1    # "backgroundArtUri":Landroid/net/Uri;
    .end local v2    # "backgroundDimensionsPx":Landroid/graphics/Point;
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "values":[Ljava/lang/Object;
    :cond_1
    invoke-static {v3}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v6

    invoke-static {v3}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v6
.end method
