.class public Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
.super Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;
.source "SongListBitmapGettersGetter.java"


# instance fields
.field private final mSongList:Lcom/google/android/music/medialist/SongList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLcom/google/android/music/medialist/SongList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loaderManager"    # Landroid/support/v4/app/LoaderManager;
    .param p3, "loaderId"    # I
    .param p4, "seed"    # J
    .param p6, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 22
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    .line 23
    iput-object p6, p0, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 24
    return-void
.end method


# virtual methods
.method protected addBitmapGetters(Landroid/database/Cursor;Ljava/util/ArrayList;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/leanback/bitmap/BitmapGetter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "bitmapGettersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/leanback/bitmap/BitmapGetter;>;"
    const/4 v1, 0x0

    .line 39
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Lcom/google/android/music/leanback/bitmap/DecodedExternalAlbumArtUriBitmapGetter;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/leanback/bitmap/DecodedExternalAlbumArtUriBitmapGetter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    :cond_0
    return-void
.end method

.method public bridge synthetic getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .locals 1

    .prologue
    .line 16
    invoke-super {p0}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    move-result-object v0

    return-object v0
.end method

.method protected getMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;->mSongList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "AlbumArtLocation"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 16
    invoke-super {p0, p1, p2}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;

    .prologue
    .line 16
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    return-void
.end method

.method public bridge synthetic startLoading()V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->startLoading()V

    return-void
.end method
