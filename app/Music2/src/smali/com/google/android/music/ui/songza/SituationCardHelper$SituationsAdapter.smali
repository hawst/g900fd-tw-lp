.class public Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;
.super Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;
.source "SituationCardHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/songza/SituationCardHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SituationsAdapter"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field private final mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/MaterialMainstageFragment;)V
    .locals 1
    .param p1, "fragment"    # Lcom/google/android/music/ui/MaterialMainstageFragment;

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    .line 88
    invoke-virtual {p1}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mContext:Landroid/content/Context;

    .line 89
    return-void
.end method

.method private setupMatrixAdapterItems()V
    .locals 4

    .prologue
    .line 262
    iget-object v2, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 263
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x5

    if-ge v0, v2, :cond_3

    .line 264
    const-wide/16 v2, -0x65

    invoke-virtual {p0, v2, v3, v0}, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->setItemId(JI)V

    .line 265
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v0}, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->setItemViewType(II)V

    .line 263
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 268
    .end local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v3, -0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 269
    const/4 v1, 0x0

    .line 270
    .local v1, "index":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 271
    const-wide/16 v2, -0x64

    invoke-virtual {p0, v2, v3, v1}, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->setItemId(JI)V

    .line 272
    iget-object v2, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_2

    .line 273
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v1}, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->setItemViewType(II)V

    .line 277
    :goto_2
    iget-object v2, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-virtual {p0, v2, v3, v1}, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->setItemData(Landroid/database/Cursor;II)V

    .line 278
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 275
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->setItemViewType(II)V

    goto :goto_2

    .line 281
    .end local v1    # "index":I
    :cond_3
    return-void
.end method


# virtual methods
.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 12
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 129
    move-object v10, p1

    check-cast v10, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;

    .line 130
    .local v10, "vh":Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v1}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    iput-object v1, v10, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 131
    invoke-virtual {v10}, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;->getItemViewType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 134
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    iget-object v7, v10, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 140
    .local v7, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->SITUATION:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v7, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 141
    invoke-virtual {v7, p2}, Lcom/google/android/music/ui/cardlib/model/Document;->setPosition(I)V

    .line 142
    const/4 v1, 0x7

    invoke-virtual {v7, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setMainstageReason(I)V

    .line 144
    const/4 v6, 0x0

    .line 145
    .local v6, "description":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 146
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 147
    invoke-virtual {v7, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setDescription(Ljava/lang/String;)V

    .line 150
    :cond_1
    const/4 v9, 0x0

    .line 151
    .local v9, "title":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 152
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 153
    invoke-virtual {v7, v9}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 155
    :cond_2
    iget-object v1, v10, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;->mSituationTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v1, v10, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;->mOverlay:Landroid/view/View;

    invoke-virtual {v1, v9}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 158
    const/4 v8, 0x0

    .line 159
    .local v8, "imageUrl":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 160
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 161
    invoke-virtual {v7, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 164
    :cond_3
    const/4 v11, 0x0

    .line 165
    .local v11, "wideImageUrl":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v2, 0x4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 166
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v2, 0x4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 167
    invoke-virtual {v7, v11}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 170
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_5

    .line 171
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setSituationId(Ljava/lang/String;)V

    .line 174
    :cond_5
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v2, 0x5

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v7, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setHasSubSituations(Z)V

    .line 176
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 177
    new-instance v5, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    const/4 v1, 0x1

    invoke-direct {v5, v8, v1}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;-><init>(Ljava/lang/String;Z)V

    .line 178
    .local v5, "url":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    new-instance v0, Lcom/google/android/music/art/SingleUrlArtDescriptor;

    sget-object v1, Lcom/google/android/music/art/ArtType;->SONGZA_SITUATION:Lcom/google/android/music/art/ArtType;

    sget-object v2, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->SLOPPY:Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    const/4 v3, 0x2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/art/SingleUrlArtDescriptor;-><init>(Lcom/google/android/music/art/ArtType;Lcom/google/android/music/art/ArtDescriptor$SizeHandling;IFLcom/google/android/music/art/ArtResolver2$ArtUrl;)V

    .line 181
    .local v0, "artDescriptor":Lcom/google/android/music/art/SingleUrlArtDescriptor;
    iget-object v1, v10, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;->mSituationArt:Lcom/google/android/music/art/SimpleArtView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/art/SimpleArtView;->bind(Lcom/google/android/music/art/ArtDescriptor;Z)V

    .line 185
    .end local v0    # "artDescriptor":Lcom/google/android/music/art/SingleUrlArtDescriptor;
    .end local v5    # "url":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    :goto_2
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/google/android/music/eventlog/MusicEventLogger;->logMainstageDocumentDisplayed(Lcom/google/android/music/ui/cardlib/model/Document;)V

    goto/16 :goto_0

    .line 174
    :cond_6
    const/4 v1, 0x0

    goto :goto_1

    .line 183
    :cond_7
    iget-object v1, v10, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;->mSituationArt:Lcom/google/android/music/art/SimpleArtView;

    const v2, 0x7f02017b

    invoke-virtual {v1, v2}, Lcom/google/android/music/art/SimpleArtView;->setBackgroundResource(I)V

    goto :goto_2

    .line 188
    .end local v6    # "description":Ljava/lang/String;
    .end local v7    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v8    # "imageUrl":Ljava/lang/String;
    .end local v9    # "title":Ljava/lang/String;
    .end local v11    # "wideImageUrl":Ljava/lang/String;
    :pswitch_1
    iget-object v1, v10, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;->mSituationTitleView:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v1, v10, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;->mSituationArt:Lcom/google/android/music/art/SimpleArtView;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/4 v3, -0x1

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/art/SimpleArtView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 196
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;

    .line 197
    .local v6, "vh":Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;
    if-nez v6, :cond_0

    .line 253
    :goto_0
    return-void

    .line 201
    :cond_0
    invoke-virtual {v6}, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;->getItemViewType()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 204
    :pswitch_1
    iget-object v0, v6, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 206
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getSituationId()Ljava/lang/String;

    move-result-object v4

    .line 207
    .local v4, "situationId":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v5

    .line 208
    .local v5, "situationTitle":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getDescription()Ljava/lang/String;

    move-result-object v3

    .line 209
    .local v3, "situationDescription":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v2

    .line 211
    .local v2, "situationArtUrl":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 212
    const-string v7, "SituationsAdapter"

    const-string v8, "Missing top situation id"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 216
    :cond_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 217
    const-string v7, "SituationsAdapter"

    const-string v8, "Missing top situation title"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 221
    :cond_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 222
    const-string v7, "SituationsAdapter"

    const-string v8, "Missing top situation description"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_3
    iget-object v7, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logMainstageCardClicked(Lcom/google/android/music/ui/cardlib/model/Document;)V

    .line 231
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->hasSubSituations()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 232
    new-instance v1, Landroid/content/Intent;

    iget-object v7, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mContext:Landroid/content/Context;

    const-class v8, Lcom/google/android/music/ui/songza/TopSituationActivity;

    invoke-direct {v1, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 233
    .local v1, "intent":Landroid/content/Intent;
    const-string v7, "situationId"

    invoke-virtual {v1, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    const-string v7, "situationTitle"

    invoke-virtual {v1, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    const-string v7, "situationDescription"

    invoke-virtual {v1, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    const-string v7, "situationArtUrl"

    invoke-virtual {v1, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 247
    :goto_1
    iget-object v7, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 240
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_4
    new-instance v1, Landroid/content/Intent;

    iget-object v7, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mContext:Landroid/content/Context;

    const-class v8, Lcom/google/android/music/ui/songza/SubSituationActivity;

    invoke-direct {v1, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 241
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v7, "subSituationId"

    invoke-virtual {v1, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    const-string v7, "subSituationTitle"

    invoke-virtual {v1, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    const-string v7, "subSituationArtUrl"

    invoke-virtual {v1, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 7
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 96
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 97
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v1, -0x1

    .line 98
    .local v1, "layoutId":I
    packed-switch p2, :pswitch_data_0

    .line 107
    const-string v4, "SituationsAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unexpected view type to create: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :goto_0
    const/4 v4, -0x1

    if-ne v1, v4, :cond_0

    .line 111
    const/4 v3, 0x0

    .line 124
    :goto_1
    return-object v3

    .line 101
    :pswitch_0
    const v1, 0x7f0400e1

    .line 102
    goto :goto_0

    .line 104
    :pswitch_1
    const v1, 0x7f0400e3

    .line 105
    goto :goto_0

    .line 113
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v0, v1, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 114
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    const v4, 0x7f0e010a

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v5, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter$1;

    invoke-direct {v5, p0, v2}, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter$1;-><init>(Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;Landroid/view/View;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    new-instance v3, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;

    invoke-direct {v3, v2}, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;-><init>(Landroid/view/View;)V

    .line 124
    .local v3, "vh":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    goto :goto_1

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public swapCursor(Landroid/database/Cursor;)V
    .locals 0
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 256
    iput-object p1, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->mCursor:Landroid/database/Cursor;

    .line 257
    invoke-direct {p0}, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->setupMatrixAdapterItems()V

    .line 258
    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->notifyDataSetChanged()V

    .line 259
    return-void
.end method
