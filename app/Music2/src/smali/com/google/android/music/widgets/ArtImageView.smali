.class public abstract Lcom/google/android/music/widgets/ArtImageView;
.super Landroid/widget/FrameLayout;
.source "ArtImageView.java"

# interfaces
.implements Lcom/google/android/music/art/ArtRequest$Listener;
.implements Lcom/google/android/music/art/UpdateBackgroundTask$Callback;
.implements Lcom/google/android/music/art/UpdateForegroundTask$Callback;


# static fields
.field private static final LOGV:Z


# instance fields
.field protected mArtRequest:Lcom/google/android/music/art/ArtRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/music/art/ArtRequest",
            "<*>;"
        }
    .end annotation
.end field

.field protected mArtType:Lcom/google/android/music/art/ArtType;

.field protected mBackgroundArtColor:I

.field private mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field protected mBackgroundMode:I

.field private mBackgroundTask:Lcom/google/android/music/art/UpdateBackgroundTask;

.field protected mCropToCircle:Z

.field protected mDefaultArtId:I

.field protected mFadeInAnimation:Landroid/view/animation/Animation;

.field private mForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mForegroundTask:Lcom/google/android/music/art/UpdateForegroundTask;

.field private mForegroundView:Lcom/google/android/music/widgets/InternalImageView;

.field protected mIsAttached:Z

.field private mOverlayMode:I

.field private mOverlayView:Landroid/view/View;

.field protected mViewBlanked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/widgets/ArtImageView;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/music/widgets/ArtImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/widgets/ArtImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 104
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    iput-boolean v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mIsAttached:Z

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mViewBlanked:Z

    .line 83
    sget-object v0, Lcom/google/android/music/art/ArtType;->DEFAULT:Lcom/google/android/music/art/ArtType;

    iput-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtType:Lcom/google/android/music/art/ArtType;

    .line 85
    iput-boolean v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mCropToCircle:Z

    .line 86
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundMode:I

    .line 87
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mDefaultArtId:I

    .line 105
    invoke-direct {p0, p2}, Lcom/google/android/music/widgets/ArtImageView;->init(Landroid/util/AttributeSet;)V

    .line 106
    return-void
.end method

.method private bind(Lcom/google/android/music/art/ArtRequest$Token;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest$Token",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 290
    .local p1, "requestToken":Lcom/google/android/music/art/ArtRequest$Token;, "Lcom/google/android/music/art/ArtRequest$Token<*>;"
    sget-boolean v2, Lcom/google/android/music/widgets/ArtImageView;->LOGV:Z

    if-eqz v2, :cond_0

    .line 291
    const-string v2, "MusicArtView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bind(Token) requestToken="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    invoke-virtual {v2, p1}, Lcom/google/android/music/art/ArtRequest;->tokenEqualsRequest(Lcom/google/android/music/art/ArtRequest$Token;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    invoke-virtual {v2}, Lcom/google/android/music/art/ArtRequest;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 310
    :goto_0
    return-void

    .line 298
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->cleanUpArtRequest()V

    .line 299
    iget-object v0, p1, Lcom/google/android/music/art/ArtRequest$Token;->mDescriptor:Ljava/lang/Object;

    .line 302
    .local v0, "descriptor":Ljava/lang/Object;
    instance-of v2, v0, Lcom/google/android/music/cloudclient/QuizGenreJson;

    if-eqz v2, :cond_3

    .line 303
    check-cast v0, Lcom/google/android/music/cloudclient/QuizGenreJson;

    .end local v0    # "descriptor":Ljava/lang/Object;
    iget-object v2, v0, Lcom/google/android/music/cloudclient/QuizGenreJson;->mName:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ArtImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 307
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/art/ArtResolver;->getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver;

    move-result-object v1

    .line 308
    .local v1, "resolver":Lcom/google/android/music/art/ArtResolver;
    invoke-virtual {v1, p1}, Lcom/google/android/music/art/ArtResolver;->getArt(Lcom/google/android/music/art/ArtRequest$Token;)Lcom/google/android/music/art/ArtRequest;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    .line 309
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->setupNewArtRequest()V

    goto :goto_0

    .line 304
    .end local v1    # "resolver":Lcom/google/android/music/art/ArtResolver;
    .restart local v0    # "descriptor":Ljava/lang/Object;
    :cond_3
    instance-of v2, v0, Lcom/google/android/music/cloudclient/QuizArtistJson;

    if-eqz v2, :cond_2

    .line 305
    check-cast v0, Lcom/google/android/music/cloudclient/QuizArtistJson;

    .end local v0    # "descriptor":Ljava/lang/Object;
    iget-object v2, v0, Lcom/google/android/music/cloudclient/QuizArtistJson;->mName:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ArtImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private init(Landroid/util/AttributeSet;)V
    .locals 13
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v12, -0x1

    .line 109
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 110
    .local v2, "context":Landroid/content/Context;
    sget-object v7, Lcom/google/android/music/R$styleable;->ArtImageView:[I

    invoke-virtual {v2, p1, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 111
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v3

    .line 113
    .local v3, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_1

    .line 114
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v1

    .line 115
    .local v1, "attr":I
    packed-switch v1, :pswitch_data_0

    .line 113
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 117
    :pswitch_0
    invoke-virtual {v0, v4, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    .line 118
    .local v6, "value":I
    if-ne v6, v8, :cond_0

    move v7, v8

    :goto_2
    iput-boolean v7, p0, Lcom/google/android/music/widgets/ArtImageView;->mCropToCircle:Z

    goto :goto_1

    :cond_0
    move v7, v9

    goto :goto_2

    .line 122
    .end local v1    # "attr":I
    .end local v6    # "value":I
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 124
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f0c00b5

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundArtColor:I

    .line 126
    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-direct {v7, v9, v10}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v7, p0, Lcom/google/android/music/widgets/ArtImageView;->mFadeInAnimation:Landroid/view/animation/Animation;

    .line 127
    iget-object v7, p0, Lcom/google/android/music/widgets/ArtImageView;->mFadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0d0008

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v7, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 129
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->createForegroundView()Lcom/google/android/music/widgets/InternalImageView;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundView:Lcom/google/android/music/widgets/InternalImageView;

    .line 130
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v5, v12, v12}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 132
    .local v5, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v7, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundView:Lcom/google/android/music/widgets/InternalImageView;

    invoke-virtual {v7, v5}, Lcom/google/android/music/widgets/InternalImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 133
    iget-object v7, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundView:Lcom/google/android/music/widgets/InternalImageView;

    invoke-virtual {v7, v8}, Lcom/google/android/music/widgets/InternalImageView;->setDuplicateParentStateEnabled(Z)V

    .line 134
    iget-object v7, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundView:Lcom/google/android/music/widgets/InternalImageView;

    invoke-virtual {p0, v7}, Lcom/google/android/music/widgets/ArtImageView;->addView(Landroid/view/View;)V

    .line 135
    new-instance v7, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v7, v9}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/google/android/music/widgets/ArtImageView;->mOverlayView:Landroid/view/View;

    .line 136
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    .end local v5    # "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-direct {v5, v12, v12}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 138
    .restart local v5    # "params":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v7, p0, Lcom/google/android/music/widgets/ArtImageView;->mOverlayView:Landroid/view/View;

    invoke-virtual {v7, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 139
    iget-object v7, p0, Lcom/google/android/music/widgets/ArtImageView;->mOverlayView:Landroid/view/View;

    invoke-virtual {v7, v8}, Landroid/view/View;->setDuplicateParentStateEnabled(Z)V

    .line 140
    iget-object v7, p0, Lcom/google/android/music/widgets/ArtImageView;->mOverlayView:Landroid/view/View;

    invoke-virtual {p0, v7}, Lcom/google/android/music/widgets/ArtImageView;->addView(Landroid/view/View;)V

    .line 141
    return-void

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private loadArt(Lcom/google/android/music/art/ArtRequest;Z)V
    .locals 3
    .param p2, "finishSuccess"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 450
    .local p1, "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<*>;"
    sget-boolean v0, Lcom/google/android/music/widgets/ArtImageView;->LOGV:Z

    if-eqz v0, :cond_0

    .line 451
    const-string v0, "MusicArtView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadArt: request="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", finishSuccess="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    invoke-virtual {p1, v0}, Lcom/google/android/music/art/ArtRequest;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 456
    sget-boolean v0, Lcom/google/android/music/widgets/ArtImageView;->LOGV:Z

    if-eqz v0, :cond_1

    .line 457
    const-string v0, "MusicArtView"

    const-string v1, "loadArt: stale request tried to load art, ignored."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :cond_1
    :goto_0
    return-void

    .line 461
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getHeight()I

    move-result v0

    if-eqz v0, :cond_1

    .line 462
    if-eqz p2, :cond_3

    .line 463
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->updateForeground()V

    goto :goto_0

    .line 465
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->cancelForegroundTask()V

    .line 466
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/widgets/ArtImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;Z)V

    goto :goto_0
.end method

.method private updateNonForegroundViews()V
    .locals 0

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->updateBackground()V

    .line 504
    invoke-direct {p0}, Lcom/google/android/music/widgets/ArtImageView;->updateOverlayDrawable()V

    .line 505
    return-void
.end method

.method private updateOverlayDrawable()V
    .locals 5

    .prologue
    .line 231
    iget v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mOverlayMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mOverlayView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getHeight()I

    move-result v3

    iget-boolean v4, p0, Lcom/google/android/music/widgets/ArtImageView;->mCropToCircle:Z

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/music/art/ArtUtils;->getOverlay(Landroid/content/Context;IIZ)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 237
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mOverlayView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public bind(Lcom/google/android/music/cloudclient/QuizArtistJson;)V
    .locals 4
    .param p1, "artistJson"    # Lcom/google/android/music/cloudclient/QuizArtistJson;

    .prologue
    .line 268
    sget-boolean v1, Lcom/google/android/music/widgets/ArtImageView;->LOGV:Z

    if-eqz v1, :cond_0

    .line 269
    const-string v1, "MusicArtView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bind(QuizArtistJson) artistJson="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->cleanUpArtRequest()V

    .line 272
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/art/ArtResolver;->getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver;

    move-result-object v0

    .line 273
    .local v0, "resolver":Lcom/google/android/music/art/ArtResolver;
    iget-object v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtType:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/music/art/ArtResolver;->getArt(Lcom/google/android/music/cloudclient/QuizArtistJson;Lcom/google/android/music/art/ArtType;)Lcom/google/android/music/art/ArtRequest;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    .line 274
    iget-object v1, p1, Lcom/google/android/music/cloudclient/QuizArtistJson;->mName:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ArtImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 275
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->setupNewArtRequest()V

    .line 276
    return-void
.end method

.method public bind(Lcom/google/android/music/cloudclient/QuizGenreJson;)V
    .locals 4
    .param p1, "genreJson"    # Lcom/google/android/music/cloudclient/QuizGenreJson;

    .prologue
    .line 279
    sget-boolean v1, Lcom/google/android/music/widgets/ArtImageView;->LOGV:Z

    if-eqz v1, :cond_0

    .line 280
    const-string v1, "MusicArtView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bind(QuizGenreJson) genreJson="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->cleanUpArtRequest()V

    .line 283
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/art/ArtResolver;->getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver;

    move-result-object v0

    .line 284
    .local v0, "resolver":Lcom/google/android/music/art/ArtResolver;
    iget-object v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtType:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/music/art/ArtResolver;->getArt(Lcom/google/android/music/cloudclient/QuizGenreJson;Lcom/google/android/music/art/ArtType;)Lcom/google/android/music/art/ArtRequest;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    .line 285
    iget-object v1, p1, Lcom/google/android/music/cloudclient/QuizGenreJson;->mName:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ArtImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 286
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->setupNewArtRequest()V

    .line 287
    return-void
.end method

.method protected final cancelForegroundTask()V
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundTask:Lcom/google/android/music/art/UpdateForegroundTask;

    if-eqz v0, :cond_0

    .line 497
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundTask:Lcom/google/android/music/art/UpdateForegroundTask;

    invoke-virtual {v0}, Lcom/google/android/music/art/UpdateForegroundTask;->cancel()V

    .line 498
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundTask:Lcom/google/android/music/art/UpdateForegroundTask;

    .line 500
    :cond_0
    return-void
.end method

.method protected cleanUpArtRequest()V
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    invoke-virtual {v0, p0}, Lcom/google/android/music/art/ArtRequest;->removeListener(Lcom/google/android/music/art/ArtRequest$Listener;)V

    .line 347
    :cond_0
    return-void
.end method

.method protected createForegroundUpdateTask()Lcom/google/android/music/art/UpdateForegroundTask;
    .locals 8

    .prologue
    .line 476
    new-instance v0, Lcom/google/android/music/art/UpdateForegroundTask;

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    iget-boolean v4, p0, Lcom/google/android/music/widgets/ArtImageView;->mCropToCircle:Z

    iget v5, p0, Lcom/google/android/music/widgets/ArtImageView;->mDefaultArtId:I

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getHeight()I

    move-result v7

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/art/UpdateForegroundTask;-><init>(Landroid/content/Context;Lcom/google/android/music/art/UpdateForegroundTask$Callback;Lcom/google/android/music/art/ArtRequest;ZIII)V

    return-object v0
.end method

.method protected createForegroundView()Lcom/google/android/music/widgets/InternalImageView;
    .locals 2

    .prologue
    .line 148
    new-instance v0, Lcom/google/android/music/widgets/InternalImageView;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/widgets/InternalImageView;-><init>(Lcom/google/android/music/widgets/ArtImageView;Z)V

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 406
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 407
    sget-boolean v2, Lcom/google/android/music/widgets/ArtImageView;->LOGV:Z

    if-eqz v2, :cond_0

    .line 408
    const-string v2, "MusicArtView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onAttachedToWindow mArtRequest="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    invoke-virtual {v2}, Lcom/google/android/music/art/ArtRequest;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_3

    .line 411
    iget-object v2, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    invoke-virtual {v2}, Lcom/google/android/music/art/ArtRequest;->isDoneLoading()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 412
    iget-object v2, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    iget-object v3, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    invoke-virtual {v3}, Lcom/google/android/music/art/ArtRequest;->getErrorLoading()Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    :cond_1
    invoke-direct {p0, v2, v0}, Lcom/google/android/music/widgets/ArtImageView;->loadArt(Lcom/google/android/music/art/ArtRequest;Z)V

    .line 419
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/google/android/music/widgets/ArtImageView;->updateNonForegroundViews()V

    .line 420
    iput-boolean v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mIsAttached:Z

    .line 421
    iput-boolean v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mViewBlanked:Z

    .line 422
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mOverlayView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 423
    return-void

    .line 414
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    if-eqz v2, :cond_4

    .line 415
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtRequest;->getToken()Lcom/google/android/music/art/ArtRequest$Token;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/widgets/ArtImageView;->bind(Lcom/google/android/music/art/ArtRequest$Token;)V

    goto :goto_0

    .line 417
    :cond_4
    const/4 v2, 0x0

    invoke-direct {p0, v2, v0}, Lcom/google/android/music/widgets/ArtImageView;->loadArt(Lcom/google/android/music/art/ArtRequest;Z)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 427
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 428
    sget-boolean v2, Lcom/google/android/music/widgets/ArtImageView;->LOGV:Z

    if-eqz v2, :cond_0

    .line 429
    const-string v2, "MusicArtView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDetachedFromWindow mArtRequest="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    :cond_0
    invoke-virtual {p0, v5}, Lcom/google/android/music/widgets/ArtImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 433
    invoke-virtual {p0, v5, v6}, Lcom/google/android/music/widgets/ArtImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;Z)V

    .line 434
    iget-object v2, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    instance-of v2, v2, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_1

    .line 435
    iget-object v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 436
    .local v1, "foreground":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 437
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 438
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 441
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    .end local v1    # "foreground":Landroid/graphics/drawable/BitmapDrawable;
    :cond_1
    iput-object v5, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 442
    iput-boolean v6, p0, Lcom/google/android/music/widgets/ArtImageView;->mIsAttached:Z

    .line 443
    return-void
.end method

.method public onFinishBackgroundUpdate(Lcom/google/android/music/art/UpdateBackgroundTask;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "task"    # Lcom/google/android/music/art/UpdateBackgroundTask;
    .param p2, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 531
    invoke-virtual {p0, p2}, Lcom/google/android/music/widgets/ArtImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 532
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundTask:Lcom/google/android/music/art/UpdateBackgroundTask;

    .line 533
    return-void
.end method

.method public onFinishForegroundUpdate(Lcom/google/android/music/art/UpdateForegroundTask;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "task"    # Lcom/google/android/music/art/UpdateForegroundTask;
    .param p2, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 537
    const/4 v0, 0x1

    invoke-virtual {p0, p2, v0}, Lcom/google/android/music/widgets/ArtImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;Z)V

    .line 538
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundTask:Lcom/google/android/music/art/UpdateForegroundTask;

    .line 539
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 558
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 559
    const-class v0, Lcom/google/android/music/widgets/ArtImageView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 560
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 564
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 565
    const-class v0, Lcom/google/android/music/widgets/ArtImageView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 566
    return-void
.end method

.method public onLoadError(Lcom/google/android/music/art/ArtRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest;

    .prologue
    .line 553
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/widgets/ArtImageView;->loadArt(Lcom/google/android/music/art/ArtRequest;Z)V

    .line 554
    return-void
.end method

.method public onLoadFinished(Lcom/google/android/music/art/ArtRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest;

    .prologue
    .line 543
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/widgets/ArtImageView;->loadArt(Lcom/google/android/music/art/ArtRequest;Z)V

    .line 544
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 388
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 389
    invoke-direct {p0}, Lcom/google/android/music/widgets/ArtImageView;->updateNonForegroundViews()V

    .line 390
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    iget-object v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    invoke-virtual {v1}, Lcom/google/android/music/art/ArtRequest;->isSuccessfullyLoaded()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/widgets/ArtImageView;->loadArt(Lcom/google/android/music/art/ArtRequest;Z)V

    .line 395
    :goto_0
    return-void

    .line 393
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/widgets/ArtImageView;->loadArt(Lcom/google/android/music/art/ArtRequest;Z)V

    goto :goto_0
.end method

.method public onStartTemporaryDetach()V
    .locals 2

    .prologue
    .line 399
    invoke-super {p0}, Landroid/widget/FrameLayout;->onStartTemporaryDetach()V

    .line 400
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mViewBlanked:Z

    .line 401
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mOverlayView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 402
    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 372
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 373
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 374
    .local v0, "old":Landroid/graphics/drawable/Drawable;
    iput-object p1, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 375
    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    .line 376
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "old":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 378
    :cond_0
    return-void
.end method

.method public setBackgroundArtColor(I)V
    .locals 0
    .param p1, "backgroundArtColor"    # I

    .prologue
    .line 198
    iput p1, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundArtColor:I

    .line 199
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->updateBackground()V

    .line 200
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/android/music/widgets/ArtImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 383
    return-void
.end method

.method public setBackgroundMode(I)V
    .locals 1
    .param p1, "backgroundMode"    # I

    .prologue
    .line 178
    iput p1, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundMode:I

    .line 179
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 180
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->updateBackground()V

    .line 182
    :cond_0
    return-void
.end method

.method public setCropToCircle(Z)V
    .locals 1
    .param p1, "cropToCircle"    # Z

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mCropToCircle:Z

    if-eq v0, p1, :cond_0

    .line 158
    iput-boolean p1, p0, Lcom/google/android/music/widgets/ArtImageView;->mCropToCircle:Z

    .line 159
    invoke-direct {p0}, Lcom/google/android/music/widgets/ArtImageView;->updateNonForegroundViews()V

    .line 161
    :cond_0
    return-void
.end method

.method public setDefaultArtId(I)V
    .locals 0
    .param p1, "defaultArtId"    # I

    .prologue
    .line 255
    iput p1, p0, Lcom/google/android/music/widgets/ArtImageView;->mDefaultArtId:I

    .line 256
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->updateBackground()V

    .line 257
    return-void
.end method

.method public setExpectedAspectRatio(F)V
    .locals 1
    .param p1, "expectedAspectRatio"    # F

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundView:Lcom/google/android/music/widgets/InternalImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/music/widgets/InternalImageView;->setExpectedAspectRatio(F)V

    .line 209
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;Z)V
    .locals 4
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "animateIfAllowed"    # Z

    .prologue
    const/4 v3, 0x0

    .line 356
    iget-object v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundView:Lcom/google/android/music/widgets/InternalImageView;

    invoke-virtual {v1, p1}, Lcom/google/android/music/widgets/InternalImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 357
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 358
    .local v0, "old":Landroid/graphics/drawable/Drawable;
    iput-object p1, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 359
    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    .line 360
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "old":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 362
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mOverlayView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 363
    iget-boolean v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mViewBlanked:Z

    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    .line 364
    iget-object v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundView:Lcom/google/android/music/widgets/InternalImageView;

    iget-object v2, p0, Lcom/google/android/music/widgets/ArtImageView;->mFadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Lcom/google/android/music/widgets/InternalImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 365
    iget-object v1, p0, Lcom/google/android/music/widgets/ArtImageView;->mOverlayView:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/music/widgets/ArtImageView;->mFadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 366
    iput-boolean v3, p0, Lcom/google/android/music/widgets/ArtImageView;->mViewBlanked:Z

    .line 368
    :cond_1
    return-void
.end method

.method public setOverlayMode(I)V
    .locals 1
    .param p1, "overlayMode"    # I

    .prologue
    .line 224
    iget v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mOverlayMode:I

    if-eq p1, v0, :cond_0

    .line 225
    iput p1, p0, Lcom/google/android/music/widgets/ArtImageView;->mOverlayMode:I

    .line 226
    invoke-direct {p0}, Lcom/google/android/music/widgets/ArtImageView;->updateOverlayDrawable()V

    .line 228
    :cond_0
    return-void
.end method

.method protected setupNewArtRequest()V
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    if-nez v0, :cond_0

    .line 336
    :goto_0
    return-void

    .line 331
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->cancelForegroundTask()V

    .line 332
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/widgets/ArtImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;Z)V

    .line 333
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mOverlayView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 334
    invoke-direct {p0}, Lcom/google/android/music/widgets/ArtImageView;->updateNonForegroundViews()V

    .line 335
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    invoke-virtual {v0, p0}, Lcom/google/android/music/art/ArtRequest;->addListener(Lcom/google/android/music/art/ArtRequest$Listener;)V

    goto :goto_0
.end method

.method public showDefaultArt(Lcom/google/android/music/art/ArtRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest;

    .prologue
    .line 549
    return-void
.end method

.method protected final updateBackground()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 515
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundTask:Lcom/google/android/music/art/UpdateBackgroundTask;

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundTask:Lcom/google/android/music/art/UpdateBackgroundTask;

    invoke-virtual {v0}, Lcom/google/android/music/art/UpdateBackgroundTask;->cancel()V

    .line 517
    iput-object v2, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundTask:Lcom/google/android/music/art/UpdateBackgroundTask;

    .line 519
    :cond_0
    iget v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 520
    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ArtImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 527
    :cond_1
    :goto_0
    return-void

    .line 523
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getWidth()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getHeight()I

    move-result v0

    if-lez v0, :cond_1

    .line 524
    new-instance v0, Lcom/google/android/music/art/UpdateBackgroundTask;

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v3, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundArtColor:I

    iget v4, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundMode:I

    iget v5, p0, Lcom/google/android/music/widgets/ArtImageView;->mDefaultArtId:I

    iget-boolean v6, p0, Lcom/google/android/music/widgets/ArtImageView;->mCropToCircle:Z

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getHeight()I

    move-result v8

    move-object v2, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/music/art/UpdateBackgroundTask;-><init>(Landroid/content/Context;Lcom/google/android/music/art/UpdateBackgroundTask$Callback;IIIZII)V

    iput-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundTask:Lcom/google/android/music/art/UpdateBackgroundTask;

    .line 526
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mBackgroundTask:Lcom/google/android/music/art/UpdateBackgroundTask;

    invoke-virtual {v0}, Lcom/google/android/music/art/UpdateBackgroundTask;->schedule()V

    goto :goto_0
.end method

.method protected final updateForeground()V
    .locals 1

    .prologue
    .line 486
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->cancelForegroundTask()V

    .line 487
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->getHeight()I

    move-result v0

    if-gtz v0, :cond_1

    .line 490
    :cond_0
    :goto_0
    return-void

    .line 488
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageView;->createForegroundUpdateTask()Lcom/google/android/music/art/UpdateForegroundTask;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundTask:Lcom/google/android/music/art/UpdateForegroundTask;

    .line 489
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageView;->mForegroundTask:Lcom/google/android/music/art/UpdateForegroundTask;

    invoke-virtual {v0}, Lcom/google/android/music/art/UpdateForegroundTask;->schedule()V

    goto :goto_0
.end method
