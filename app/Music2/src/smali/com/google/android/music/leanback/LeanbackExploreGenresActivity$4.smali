.class Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackExploreGenresActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getFeaturedAdapter(Ljava/lang/String;Ljava/lang/String;IJ)Landroid/support/v17/leanback/widget/CursorObjectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

.field final synthetic val$baseLoaderId:I

.field final synthetic val$baseSeed:J

.field final synthetic val$genreId:Ljava/lang/String;

.field final synthetic val$genreName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 1

    .prologue
    .line 232
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->val$genreName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->val$genreId:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->val$baseLoaderId:I

    iput-wide p5, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->val$baseSeed:J

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 18
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 235
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 236
    .local v11, "groupTitle":Ljava/lang/String;
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 238
    .local v10, "groupDescription":Ljava/lang/String;
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 240
    .local v12, "groupId":J
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 243
    .local v9, "groupType":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    const v5, 0x7f0b036d

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->val$genreName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    const v15, 0x7f0b00a6

    invoke-virtual {v8, v15}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->val$genreId:Ljava/lang/String;

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/music/leanback/LeanbackUtils;->newExploreTopChartsIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v14

    .line 248
    .local v14, "intent":Landroid/content/Intent;
    new-instance v2, Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    invoke-virtual {v4}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->val$baseLoaderId:I

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    sub-int/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->val$baseSeed:J

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v8

    int-to-long v0, v8

    move-wide/from16 v16, v0

    add-long v6, v6, v16

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;->val$genreId:Ljava/lang/String;

    invoke-static {v12, v13, v8}, Lcom/google/android/music/store/MusicContent$Explore;->getTopChartsUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct/range {v2 .. v9}, Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLandroid/net/Uri;I)V

    .line 253
    .local v2, "exploreGroupItemsBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;
    invoke-virtual {v2}, Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;->startLoading()V

    .line 254
    new-instance v3, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v3}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v3, v11}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v10}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v14}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v3

    return-object v3
.end method
