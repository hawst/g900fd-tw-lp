.class public Lcom/google/android/music/dial/model/SetGroupRelativeVolumeCommandJson;
.super Lcom/google/android/music/dial/model/GenericDialCommand;
.source "SetGroupRelativeVolumeCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/model/SetGroupRelativeVolumeCommandJson$SetGroupRelativeVolumeCommandBody;
    }
.end annotation


# instance fields
.field public mBody:Lcom/google/android/music/dial/model/SetGroupRelativeVolumeCommandJson$SetGroupRelativeVolumeCommandBody;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "body"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/music/dial/model/GenericDialCommand;-><init>()V

    .line 28
    iget-object v0, p0, Lcom/google/android/music/dial/model/SetGroupRelativeVolumeCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "groupvolume:1"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mNamespace:Ljava/lang/String;

    .line 29
    iget-object v0, p0, Lcom/google/android/music/dial/model/SetGroupRelativeVolumeCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "setRelativeVolume"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mCommand:Ljava/lang/String;

    .line 31
    new-instance v0, Lcom/google/android/music/dial/model/SetGroupRelativeVolumeCommandJson$SetGroupRelativeVolumeCommandBody;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/SetGroupRelativeVolumeCommandJson$SetGroupRelativeVolumeCommandBody;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/dial/model/SetGroupRelativeVolumeCommandJson;->mBody:Lcom/google/android/music/dial/model/SetGroupRelativeVolumeCommandJson$SetGroupRelativeVolumeCommandBody;

    .line 32
    return-void
.end method


# virtual methods
.method protected getBodyAsJsonString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/music/dial/model/SetGroupRelativeVolumeCommandJson;->mBody:Lcom/google/android/music/dial/model/SetGroupRelativeVolumeCommandJson$SetGroupRelativeVolumeCommandBody;

    invoke-virtual {v0}, Lcom/google/android/music/dial/model/SetGroupRelativeVolumeCommandJson$SetGroupRelativeVolumeCommandBody;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
