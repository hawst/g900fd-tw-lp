.class public Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;
.super Ljava/lang/Object;
.source "NowPlayingWidgetUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "Art"
.end annotation


# instance fields
.field public final fromService:Z

.field public final resourceId:I

.field public final url:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Intent;)V
    .locals 2
    .param p1, "musicState"    # Landroid/content/Intent;

    .prologue
    .line 433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 434
    const-string v0, "albumArtFromService"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;->fromService:Z

    .line 436
    const-string v0, "externalAlbumArtUrl"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;->url:Ljava/lang/String;

    .line 437
    const-string v0, "albumArtResourceId"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;->resourceId:I

    .line 439
    return-void
.end method
