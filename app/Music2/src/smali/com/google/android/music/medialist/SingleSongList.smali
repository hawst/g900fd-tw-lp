.class public Lcom/google/android/music/medialist/SingleSongList;
.super Lcom/google/android/music/medialist/SongList;
.source "SingleSongList.java"


# instance fields
.field private mAlbumId:J

.field private mAlbumName:Ljava/lang/String;

.field private mArtUrl:Ljava/lang/String;

.field private mArtistId:J

.field private mArtistName:Ljava/lang/String;

.field private final mAudioId:J

.field private final mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field private mTrackMetajamId:Ljava/lang/String;

.field private final mTrackName:Ljava/lang/String;

.field private mVariablesResolved:Z


# direct methods
.method public constructor <init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V
    .locals 6
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p2, "songId"    # J
    .param p4, "trackName"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 40
    const/4 v0, -0x1

    const/4 v1, 0x1

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/music/medialist/SongList;-><init>(IZZ)V

    .line 30
    iput-boolean v2, p0, Lcom/google/android/music/medialist/SingleSongList;->mVariablesResolved:Z

    .line 31
    iput-wide v4, p0, Lcom/google/android/music/medialist/SingleSongList;->mArtistId:J

    .line 32
    iput-object v3, p0, Lcom/google/android/music/medialist/SingleSongList;->mArtistName:Ljava/lang/String;

    .line 34
    iput-wide v4, p0, Lcom/google/android/music/medialist/SingleSongList;->mAlbumId:J

    .line 35
    iput-object v3, p0, Lcom/google/android/music/medialist/SingleSongList;->mAlbumName:Ljava/lang/String;

    .line 41
    iput-object p1, p0, Lcom/google/android/music/medialist/SingleSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 42
    iput-wide p2, p0, Lcom/google/android/music/medialist/SingleSongList;->mAudioId:J

    .line 43
    iput-object p4, p0, Lcom/google/android/music/medialist/SingleSongList;->mTrackName:Ljava/lang/String;

    .line 45
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid local music id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/medialist/SingleSongList;->mAudioId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    return-void
.end method

.method private declared-synchronized resolveNames(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mVariablesResolved:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 90
    :goto_0
    monitor-exit p0

    return-void

    .line 71
    :cond_0
    const/4 v0, 0x4

    :try_start_1
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "artist_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "artist"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "album_id"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "album"

    aput-object v1, v2, v0

    .line 77
    .local v2, "cols":[Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/SingleSongList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 80
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mArtistId:J

    .line 82
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mArtistName:Ljava/lang/String;

    .line 83
    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mAlbumId:J

    .line 84
    const/4 v0, 0x3

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mAlbumName:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87
    :cond_1
    :try_start_3
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mVariablesResolved:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 67
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 87
    .restart local v2    # "cols":[Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    :catchall_1
    move-exception v0

    :try_start_4
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method


# virtual methods
.method public appendToPlaylist(Landroid/content/Context;J)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "playlistId"    # J

    .prologue
    .line 136
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/medialist/SingleSongList;->mAudioId:J

    invoke-static {v0, p2, p3, v2, v3}, Lcom/google/android/music/store/MusicContent$Playlists;->appendItemToPlayList(Landroid/content/ContentResolver;JJ)I

    move-result v0

    return v0
.end method

.method public getAlbumId(Landroid/content/Context;)J
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/SingleSongList;->resolveNames(Landroid/content/Context;)V

    .line 120
    iget-wide v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mAlbumId:J

    return-wide v0
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 63
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/medialist/SingleSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/medialist/SingleSongList;->mAudioId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/medialist/SingleSongList;->mTrackName:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getArtUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-object v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mArtUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 150
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "AlbumArtLocation"

    aput-object v0, v2, v1

    .line 153
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 155
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/SingleSongList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 157
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 158
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mArtUrl:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    :goto_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 166
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mArtUrl:Ljava/lang/String;

    return-object v0

    .line 160
    .restart local v2    # "cols":[Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_1
    :try_start_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mArtUrl:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    return-object v0
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 125
    iget-wide v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mAudioId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$XAudio;->getAudioUri(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 194
    iget-wide v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mAudioId:J

    return-wide v0
.end method

.method public getImage(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "allowAlias"    # Z

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/SingleSongList;->resolveNames(Landroid/content/Context;)V

    .line 112
    iget-wide v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mAlbumId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 113
    .local v2, "albumid":J
    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/music/medialist/SingleSongList;->mAlbumName:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/music/medialist/SingleSongList;->mArtistName:Ljava/lang/String;

    move-object v1, p1

    move v4, p2

    move v5, p3

    move-object v9, p4

    move/from16 v10, p5

    invoke-static/range {v1 .. v10}, Lcom/google/android/music/utils/AlbumArtUtils;->getArtwork(Landroid/content/Context;JIIZLjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mTrackName:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondaryName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/SingleSongList;->resolveNames(Landroid/content/Context;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mArtistName:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackMetajamId(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 173
    iget-object v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mTrackMetajamId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 174
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "Nid"

    aput-object v0, v2, v1

    .line 177
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 179
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/SingleSongList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 181
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 182
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mTrackMetajamId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    :goto_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 190
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mTrackMetajamId:Ljava/lang/String;

    return-object v0

    .line 184
    .restart local v2    # "cols":[Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_1
    :try_start_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/medialist/SingleSongList;->mTrackMetajamId:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 187
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getValidSortOrders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public populateExternalSearchExtras(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "i"    # Landroid/content/Intent;

    .prologue
    .line 142
    const-string v0, "android.intent.extra.title"

    iget-object v1, p0, Lcom/google/android/music/medialist/SingleSongList;->mTrackName:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    return-void
.end method

.method public storeDefaultSortOrder(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "not supported for SingleSongList"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public supportsAppendToPlaylist()Z
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x1

    return v0
.end method
