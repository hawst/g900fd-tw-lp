.class public Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "TrackListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/TrackListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ViewHolder"
.end annotation


# instance fields
.field album:Lcom/google/android/music/StatefulShadowTextView;

.field artist:Lcom/google/android/music/StatefulShadowTextView;

.field comboColumn:Landroid/view/View;

.field contextMenu:Landroid/view/View;

.field document:Lcom/google/android/music/ui/cardlib/model/Document;

.field duration:Lcom/google/android/music/StatefulShadowTextView;

.field hasRemote:Z

.field icon:Lcom/google/android/music/AsyncAlbumArtImageView;

.field isAvailable:Z

.field play_indicator:Lcom/google/android/music/PlayingIndicator;

.field final synthetic this$0:Lcom/google/android/music/ui/TrackListAdapter;

.field title:Lcom/google/android/music/StatefulShadowTextView;

.field titleBuffer:Landroid/database/CharArrayBuffer;

.field trackNumber:Lcom/google/android/music/StatefulShadowTextView;


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/TrackListAdapter;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->this$0:Lcom/google/android/music/ui/TrackListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
