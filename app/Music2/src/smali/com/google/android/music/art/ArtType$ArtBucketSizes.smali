.class final Lcom/google/android/music/art/ArtType$ArtBucketSizes;
.super Ljava/lang/Object;
.source "ArtType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ArtBucketSizes"
.end annotation


# static fields
.field private static final DEFAULT_ART_BUCKETS:Lcom/google/android/music/art/ArtType$ArtBucketSizes;


# instance fields
.field public final largeWidth:I

.field public final mediumWidth:I

.field public final smallWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    const/16 v1, 0x80

    const/16 v2, 0x200

    const/16 v3, 0x438

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/art/ArtType$ArtBucketSizes;-><init>(III)V

    sput-object v0, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->DEFAULT_ART_BUCKETS:Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    return-void
.end method

.method private constructor <init>(III)V
    .locals 0
    .param p1, "smallWidth"    # I
    .param p2, "mediumWidth"    # I
    .param p3, "largeWidth"    # I

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput p1, p0, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->smallWidth:I

    .line 59
    iput p2, p0, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->mediumWidth:I

    .line 60
    iput p3, p0, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->largeWidth:I

    .line 61
    return-void
.end method

.method synthetic constructor <init>(IIILcom/google/android/music/art/ArtType$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .param p4, "x3"    # Lcom/google/android/music/art/ArtType$1;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/art/ArtType$ArtBucketSizes;-><init>(III)V

    return-void
.end method

.method static synthetic access$000()Lcom/google/android/music/art/ArtType$ArtBucketSizes;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->DEFAULT_ART_BUCKETS:Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    return-object v0
.end method
