.class public Lcom/google/android/music/cast/CastMediaProviderFactory;
.super Ljava/lang/Object;
.source "CastMediaProviderFactory.java"


# instance fields
.field private mCastContext:Lcom/google/cast/CastContext;

.field private final mCastSessionOptions:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isLogFilesEnabled"    # Z

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v2, Lcom/google/cast/CastContext;

    invoke-direct {v2, p1}, Lcom/google/cast/CastContext;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/music/cast/CastMediaProviderFactory;->mCastContext:Lcom/google/cast/CastContext;

    .line 27
    if-eqz p2, :cond_0

    .line 28
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/google/cast/Logger;->setDebugEnabledByDefault(Z)V

    .line 31
    :cond_0
    const/4 v1, 0x3

    .line 34
    .local v1, "sessionOptions":I
    iget-object v2, p0, Lcom/google/android/music/cast/CastMediaProviderFactory;->mCastContext:Lcom/google/cast/CastContext;

    invoke-virtual {v2}, Lcom/google/cast/CastContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "music_enable_chromecast_wake_lock"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 39
    .local v0, "isWakeLockEnabled":Z
    if-eqz v0, :cond_1

    .line 40
    or-int/lit8 v1, v1, 0x4

    .line 43
    :cond_1
    iput v1, p0, Lcom/google/android/music/cast/CastMediaProviderFactory;->mCastSessionOptions:I

    .line 44
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/music/cast/CastMediaProviderFactory;->mCastContext:Lcom/google/cast/CastContext;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/android/music/cast/CastMediaProviderFactory;->mCastContext:Lcom/google/cast/CastContext;

    invoke-virtual {v0}, Lcom/google/cast/CastContext;->dispose()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/cast/CastMediaProviderFactory;->mCastContext:Lcom/google/cast/CastContext;

    .line 51
    :cond_0
    return-void
.end method

.method public registerMediaRouteProvider()V
    .locals 5

    .prologue
    .line 63
    iget-object v2, p0, Lcom/google/android/music/cast/CastMediaProviderFactory;->mCastContext:Lcom/google/cast/CastContext;

    if-nez v2, :cond_0

    .line 64
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "CastMediaProviderFactory is not initialized"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 67
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/cast/CastMediaProviderFactory;->mCastContext:Lcom/google/cast/CastContext;

    invoke-virtual {v2}, Lcom/google/cast/CastContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "music_cast_app_name"

    const-string v4, "GoogleMusic"

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 74
    .local v1, "appName":Ljava/lang/String;
    const-string v2, "http://www.google.com/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/google/cast/MimeData;->createUrlData(Landroid/net/Uri;)Lcom/google/cast/MimeData;

    move-result-object v0

    .line 76
    .local v0, "appArgument":Lcom/google/cast/MimeData;
    iget-object v2, p0, Lcom/google/android/music/cast/CastMediaProviderFactory;->mCastContext:Lcom/google/cast/CastContext;

    iget v3, p0, Lcom/google/android/music/cast/CastMediaProviderFactory;->mCastSessionOptions:I

    const/4 v4, 0x1

    invoke-static {v2, v1, v0, v3, v4}, Lcom/google/cast/MediaRouteHelper;->registerMediaRouteProvider(Lcom/google/cast/CastContext;Ljava/lang/String;Lcom/google/cast/MimeData;IZ)Z

    .line 79
    return-void
.end method

.method public registerMinimalMediaRouteProvider()V
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/cast/CastMediaProviderFactory;->mCastContext:Lcom/google/cast/CastContext;

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CastMediaProviderFactory is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/cast/CastMediaProviderFactory;->mCastContext:Lcom/google/cast/CastContext;

    new-instance v1, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;

    iget-object v2, p0, Lcom/google/android/music/cast/CastMediaProviderFactory;->mCastContext:Lcom/google/cast/CastContext;

    invoke-virtual {v2}, Lcom/google/cast/CastContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/google/cast/MediaRouteHelper;->registerMinimalMediaRouteProvider(Lcom/google/cast/CastContext;Lcom/google/cast/MediaRouteAdapter;)Z

    .line 60
    return-void
.end method
