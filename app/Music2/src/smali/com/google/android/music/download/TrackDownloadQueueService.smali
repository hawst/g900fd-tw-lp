.class public Lcom/google/android/music/download/TrackDownloadQueueService;
.super Landroid/app/Service;
.source "TrackDownloadQueueService.java"


# instance fields
.field private volatile mDownloadQueueManager:Lcom/google/android/music/download/BaseDownloadQueueManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "writer"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 42
    const-string v0, "TrackDownloadQueueService:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 43
    invoke-static {p2}, Lcom/google/android/music/log/Log;->dump(Ljava/io/PrintWriter;)V

    .line 44
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/download/TrackDownloadQueueService;->mDownloadQueueManager:Lcom/google/android/music/download/BaseDownloadQueueManager;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/music/download/TrackDownloadQueueManager;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/TrackDownloadQueueManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/download/TrackDownloadQueueService;->mDownloadQueueManager:Lcom/google/android/music/download/BaseDownloadQueueManager;

    .line 26
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/music/download/TrackDownloadQueueService;->mDownloadQueueManager:Lcom/google/android/music/download/BaseDownloadQueueManager;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/google/android/music/download/TrackDownloadQueueService;->mDownloadQueueManager:Lcom/google/android/music/download/BaseDownloadQueueManager;

    invoke-virtual {v0}, Lcom/google/android/music/download/BaseDownloadQueueManager;->onDestroy()V

    .line 33
    :cond_0
    return-void
.end method
