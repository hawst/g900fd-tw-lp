.class public Lcom/google/android/music/dial/SsdpWriter;
.super Ljava/lang/Object;
.source "SsdpWriter.java"


# instance fields
.field private final mListener:Lcom/google/android/music/dial/SsdpListener;

.field private final mMaxWaitSeconds:I

.field private final mMulticastAddress:Ljava/net/InetAddress;

.field private final mMulticastPort:I

.field private mSearchRequest:[B

.field private final mSearchTarget:Ljava/lang/String;

.field private final mSocket:Ljava/net/MulticastSocket;


# direct methods
.method public constructor <init>(Ljava/net/MulticastSocket;Ljava/lang/String;Ljava/net/InetAddress;IILcom/google/android/music/dial/SsdpListener;)V
    .locals 0
    .param p1, "socket"    # Ljava/net/MulticastSocket;
    .param p2, "searchTarget"    # Ljava/lang/String;
    .param p3, "multicastAddress"    # Ljava/net/InetAddress;
    .param p4, "multicastPort"    # I
    .param p5, "maxWaitSeconds"    # I
    .param p6, "listener"    # Lcom/google/android/music/dial/SsdpListener;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/music/dial/SsdpWriter;->mSocket:Ljava/net/MulticastSocket;

    .line 34
    iput-object p2, p0, Lcom/google/android/music/dial/SsdpWriter;->mSearchTarget:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/google/android/music/dial/SsdpWriter;->mMulticastAddress:Ljava/net/InetAddress;

    .line 36
    iput p4, p0, Lcom/google/android/music/dial/SsdpWriter;->mMulticastPort:I

    .line 37
    iput p5, p0, Lcom/google/android/music/dial/SsdpWriter;->mMaxWaitSeconds:I

    .line 38
    iput-object p6, p0, Lcom/google/android/music/dial/SsdpWriter;->mListener:Lcom/google/android/music/dial/SsdpListener;

    .line 39
    return-void
.end method


# virtual methods
.method public initialize()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 42
    new-instance v1, Lcom/google/android/music/dial/SsdpMessage$Builder;

    sget-object v2, Lcom/google/android/music/dial/SsdpMessage$MessageType;->MSEARCH:Lcom/google/android/music/dial/SsdpMessage$MessageType;

    invoke-direct {v1, v2}, Lcom/google/android/music/dial/SsdpMessage$Builder;-><init>(Lcom/google/android/music/dial/SsdpMessage$MessageType;)V

    const-string v2, "HOST"

    const-string v3, "239.255.255.250:1900"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/dial/SsdpMessage$Builder;->withHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/dial/SsdpMessage$Builder;

    move-result-object v1

    const-string v2, "MAN"

    const-string v3, "\"ssdp:discover\""

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/dial/SsdpMessage$Builder;->withHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/dial/SsdpMessage$Builder;

    move-result-object v1

    const-string v2, "MX"

    iget v3, p0, Lcom/google/android/music/dial/SsdpWriter;->mMaxWaitSeconds:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/dial/SsdpMessage$Builder;->withHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/dial/SsdpMessage$Builder;

    move-result-object v1

    const-string v2, "ST"

    iget-object v3, p0, Lcom/google/android/music/dial/SsdpWriter;->mSearchTarget:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/dial/SsdpMessage$Builder;->withHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/dial/SsdpMessage$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/dial/SsdpMessage$Builder;->build()Lcom/google/android/music/dial/SsdpMessage;

    move-result-object v0

    .line 50
    .local v0, "search":Lcom/google/android/music/dial/SsdpMessage;
    invoke-virtual {v0}, Lcom/google/android/music/dial/SsdpMessage;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/dial/SsdpWriter;->mSearchRequest:[B

    .line 51
    return-void
.end method

.method public sendSearch()V
    .locals 6

    .prologue
    .line 54
    new-instance v1, Ljava/net/DatagramPacket;

    iget-object v2, p0, Lcom/google/android/music/dial/SsdpWriter;->mSearchRequest:[B

    iget-object v3, p0, Lcom/google/android/music/dial/SsdpWriter;->mSearchRequest:[B

    array-length v3, v3

    iget-object v4, p0, Lcom/google/android/music/dial/SsdpWriter;->mMulticastAddress:Ljava/net/InetAddress;

    iget v5, p0, Lcom/google/android/music/dial/SsdpWriter;->mMulticastPort:I

    invoke-direct {v1, v2, v3, v4, v5}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    .line 58
    .local v1, "packet":Ljava/net/DatagramPacket;
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/dial/SsdpWriter;->mSocket:Ljava/net/MulticastSocket;

    invoke-virtual {v2, v1}, Ljava/net/MulticastSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 59
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "SsdpWriter"

    const-string v3, "Error sending M-SEARCH request"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 61
    iget-object v2, p0, Lcom/google/android/music/dial/SsdpWriter;->mListener:Lcom/google/android/music/dial/SsdpListener;

    if-eqz v2, :cond_0

    .line 62
    iget-object v2, p0, Lcom/google/android/music/dial/SsdpWriter;->mListener:Lcom/google/android/music/dial/SsdpListener;

    sget-object v3, Lcom/google/android/music/dial/SsdpListener$ScanError;->ERROR_SENDING_MSEARCH:Lcom/google/android/music/dial/SsdpListener$ScanError;

    invoke-interface {v2, v3}, Lcom/google/android/music/dial/SsdpListener;->onScanError(Lcom/google/android/music/dial/SsdpListener$ScanError;)V

    goto :goto_0
.end method
