.class final Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;
.super Ljava/lang/Object;
.source "MaterialTrackContainerFragment.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/MaterialTrackContainerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ContainerMetadata"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public albumId:J

.field public albumYear:I

.field public artistId:J

.field public artistUrl:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public descriptionAttrLicenseTitle:Ljava/lang/String;

.field public descriptionAttrLicenseUrl:Ljava/lang/String;

.field public descriptionAttrSourceTitle:Ljava/lang/String;

.field public descriptionAttrSourceUrl:Ljava/lang/String;

.field public metajamAlbumId:Ljava/lang/String;

.field public metajamArtistId:Ljava/lang/String;

.field public primaryTitle:Ljava/lang/String;

.field public secondaryTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 315
    new-instance v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata$1;

    invoke-direct {v0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata$1;-><init>()V

    sput-object v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275
    iput-wide v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->albumId:J

    .line 277
    iput-wide v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistId:J

    .line 284
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->albumYear:I

    .line 291
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const-wide/16 v0, -0x1

    .line 326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275
    iput-wide v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->albumId:J

    .line 277
    iput-wide v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistId:J

    .line 284
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->albumYear:I

    .line 327
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->albumId:J

    .line 328
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->metajamAlbumId:Ljava/lang/String;

    .line 329
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistId:J

    .line 330
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->metajamArtistId:Ljava/lang/String;

    .line 331
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistUrl:Ljava/lang/String;

    .line 332
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->primaryTitle:Ljava/lang/String;

    .line 333
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->secondaryTitle:Ljava/lang/String;

    .line 334
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->description:Ljava/lang/String;

    .line 335
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->albumYear:I

    .line 336
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrSourceTitle:Ljava/lang/String;

    .line 337
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrSourceUrl:Ljava/lang/String;

    .line 338
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrLicenseTitle:Ljava/lang/String;

    .line 339
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrLicenseUrl:Ljava/lang/String;

    .line 340
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;

    .prologue
    .line 273
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 295
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 300
    iget-wide v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->albumId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 301
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->metajamAlbumId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 302
    iget-wide v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 303
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->metajamArtistId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 305
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->primaryTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->secondaryTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 307
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 308
    iget v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->albumYear:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 309
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrSourceTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 310
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrSourceUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrLicenseTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 312
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrLicenseUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 313
    return-void
.end method
