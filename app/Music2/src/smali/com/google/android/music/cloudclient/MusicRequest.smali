.class public Lcom/google/android/music/cloudclient/MusicRequest;
.super Ljava/lang/Object;
.source "MusicRequest.java"


# static fields
.field private static final LOGV:Z

.field private static sSharedClient:Lcom/google/android/music/cloudclient/MusicHttpClient;


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mAndroidId:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mLogHttp:Z

.field private final mMusicAuthInfo:Lcom/google/android/music/sync/google/MusicAuthInfo;

.field protected final mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private final mPassthroughCookies:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CLOUD_CLIENT:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/cloudclient/MusicRequest;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 185
    invoke-virtual {p2}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/cloudclient/MusicRequest;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;Landroid/accounts/Account;)V

    .line 186
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;Landroid/accounts/Account;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;
    .param p3, "account"    # Landroid/accounts/Account;

    .prologue
    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    sget-object v5, Lcom/google/android/music/utils/DebugUtils$MusicTag;->HTTP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v5}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/music/cloudclient/MusicRequest;->mLogHttp:Z

    .line 92
    new-instance v5, Ljava/util/TreeSet;

    invoke-direct {v5}, Ljava/util/TreeSet;-><init>()V

    iput-object v5, p0, Lcom/google/android/music/cloudclient/MusicRequest;->mPassthroughCookies:Ljava/util/TreeSet;

    .line 189
    iput-object p2, p0, Lcom/google/android/music/cloudclient/MusicRequest;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 190
    iput-object p1, p0, Lcom/google/android/music/cloudclient/MusicRequest;->mContext:Landroid/content/Context;

    .line 191
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "android_id"

    const-wide/16 v8, 0x0

    invoke-static {v5, v6, v8, v9}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/cloudclient/MusicRequest;->mAndroidId:Ljava/lang/String;

    .line 193
    iput-object p3, p0, Lcom/google/android/music/cloudclient/MusicRequest;->mAccount:Landroid/accounts/Account;

    .line 194
    new-instance v5, Lcom/google/android/music/sync/google/MusicAuthInfo;

    invoke-direct {v5, p1}, Lcom/google/android/music/sync/google/MusicAuthInfo;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/music/cloudclient/MusicRequest;->mMusicAuthInfo:Lcom/google/android/music/sync/google/MusicAuthInfo;

    .line 195
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "music_download_passthrough_cookies"

    const-string v7, "sjsc"

    invoke-static {v5, v6, v7}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 197
    .local v4, "passthroughCookies":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 200
    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 201
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 202
    iget-object v5, p0, Lcom/google/android/music/cloudclient/MusicRequest;->mPassthroughCookies:Ljava/util/TreeSet;

    invoke-virtual {v5, v2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 200
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 206
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "key":Ljava/lang/String;
    .end local v3    # "len$":I
    :cond_1
    return-void
.end method

.method private static createHttpClient(Landroid/content/Context;)Lcom/google/android/music/cloudclient/MusicHttpClient;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 110
    invoke-static {p0}, Lcom/google/android/music/download/DownloadUtils;->getUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/google/android/music/GoogleHttpClientFactory;->createGoogleHttpClient(Landroid/content/Context;Ljava/lang/String;Z)Lcom/google/android/music/cloudclient/MusicHttpClient;

    move-result-object v0

    .line 113
    .local v0, "client":Lcom/google/android/music/cloudclient/MusicHttpClient;
    sget-object v2, Lcom/google/android/music/utils/DebugUtils;->HTTP_TAG:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/music/utils/DebugUtils;->isAutoLogAll()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    :goto_0
    invoke-virtual {v0, v2, v1}, Lcom/google/android/music/cloudclient/MusicHttpClient;->enableCurlLogging(Ljava/lang/String;I)V

    .line 118
    return-object v0

    .line 113
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method

.method private final getAuthAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/android/music/cloudclient/MusicRequest;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method public static declared-synchronized getSharedHttpClient(Landroid/content/Context;)Lcom/google/android/music/cloudclient/MusicHttpClient;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    const-class v1, Lcom/google/android/music/cloudclient/MusicRequest;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/music/cloudclient/MusicRequest;->sSharedClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    if-nez v0, :cond_0

    .line 100
    invoke-static {p0}, Lcom/google/android/music/cloudclient/MusicRequest;->createHttpClient(Landroid/content/Context;)Lcom/google/android/music/cloudclient/MusicHttpClient;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/cloudclient/MusicRequest;->sSharedClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    .line 102
    :cond_0
    sget-object v0, Lcom/google/android/music/cloudclient/MusicRequest;->sSharedClient:Lcom/google/android/music/cloudclient/MusicHttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static logInputStreamContents(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 10
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x4000

    .line 156
    if-nez p0, :cond_0

    .line 181
    .end local p0    # "in":Ljava/io/InputStream;
    :goto_0
    return-object p0

    .line 167
    .restart local p0    # "in":Ljava/io/InputStream;
    :cond_0
    const/16 v2, 0x4000

    .line 168
    .local v2, "bufferSize":I
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, p0, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 169
    .local v0, "bin":Ljava/io/BufferedInputStream;
    invoke-virtual {v0, v6}, Ljava/io/BufferedInputStream;->mark(I)V

    .line 170
    const/16 v5, 0x4000

    .line 171
    .local v5, "wanted":I
    const/4 v4, 0x0

    .line 172
    .local v4, "totalReceived":I
    new-array v1, v5, [B

    .line 173
    .local v1, "buf":[B
    :goto_1
    if-lez v5, :cond_1

    .line 174
    invoke-virtual {v0, v1, v4, v5}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v3

    .line 175
    .local v3, "got":I
    if-gtz v3, :cond_2

    .line 179
    .end local v3    # "got":I
    :cond_1
    sget-object v6, Lcom/google/android/music/utils/DebugUtils;->HTTP_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "UTF-8"

    invoke-direct {v7, v1, v8, v4, v9}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->reset()V

    move-object p0, v0

    .line 181
    goto :goto_0

    .line 176
    .restart local v3    # "got":I
    :cond_2
    sub-int/2addr v5, v3

    .line 177
    add-int/2addr v4, v3

    .line 178
    goto :goto_1
.end method

.method private prepareAuthorizedRequest(Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;)V
    .locals 0
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpRequestBase;
    .param p2, "authToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 427
    invoke-direct {p0, p1}, Lcom/google/android/music/cloudclient/MusicRequest;->prepareRequest(Lorg/apache/http/client/methods/HttpRequestBase;)V

    .line 428
    invoke-static {p1, p2}, Lcom/google/android/music/cloudclient/MusicRequest;->updateAuthorization(Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;)V

    .line 429
    return-void
.end method

.method private prepareRedirectRequestAndUpdateCookies(Lorg/apache/http/HttpResponse;Ljava/util/List;)Lorg/apache/http/client/methods/HttpGet;
    .locals 16
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpResponse;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/http/client/methods/HttpGet;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 449
    .local p2, "cookies":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v13, "Location"

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v8

    .line 450
    .local v8, "location":Lorg/apache/http/Header;
    if-eqz v8, :cond_0

    invoke-interface {v8}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 451
    :cond_0
    const-string v13, "MusicStreaming"

    const-string v14, "Redirect requested but no Location specified."

    invoke-static {v13, v14}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    new-instance v13, Ljava/io/IOException;

    const-string v14, "Redirect requested but no Location specified."

    invoke-direct {v13, v14}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 454
    :cond_1
    const-string v13, "MusicStreaming"

    const/4 v14, 0x3

    invoke-static {v13, v14}, Lcom/google/android/music/log/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 455
    const-string v13, "MusicStreaming"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Following redirect to "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-interface {v8}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    :cond_2
    invoke-interface {v8}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v10

    .line 459
    .local v10, "redirectUrl":Ljava/lang/String;
    new-instance v11, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v11, v10}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 462
    .local v11, "request":Lorg/apache/http/client/methods/HttpGet;
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/music/cloudclient/MusicRequest;->prepareUnauthorizedRequest(Lorg/apache/http/client/methods/HttpRequestBase;)V

    .line 465
    const-string v13, "Set-Cookie"

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v3

    .line 466
    .local v3, "cookieHeaders":[Lorg/apache/http/Header;
    move-object v1, v3

    .local v1, "arr$":[Lorg/apache/http/Header;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_6

    aget-object v2, v1, v6

    .line 467
    .local v2, "cookie":Lorg/apache/http/Header;
    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v12

    .line 468
    .local v12, "setCookieString":Ljava/lang/String;
    if-eqz v12, :cond_3

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v13

    if-nez v13, :cond_4

    .line 466
    :cond_3
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 471
    :cond_4
    const/16 v13, 0x3d

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    .line 472
    .local v9, "nameLength":I
    const/16 v13, 0x3b

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 473
    .local v5, "endOfCookieIndex":I
    const/4 v13, -0x1

    if-eq v9, v13, :cond_5

    const/4 v13, -0x1

    if-ne v5, v13, :cond_7

    .line 474
    :cond_5
    const-string v13, "MusicStreaming"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Invalid cookie format: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    .end local v2    # "cookie":Lorg/apache/http/Header;
    .end local v5    # "endOfCookieIndex":I
    .end local v9    # "nameLength":I
    .end local v12    # "setCookieString":Ljava/lang/String;
    :cond_6
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 484
    .local v2, "cookie":Ljava/lang/String;
    const-string v13, "Cookie"

    invoke-virtual {v11, v13, v2}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 477
    .local v2, "cookie":Lorg/apache/http/Header;
    .restart local v5    # "endOfCookieIndex":I
    .local v6, "i$":I
    .restart local v9    # "nameLength":I
    .restart local v12    # "setCookieString":Ljava/lang/String;
    :cond_7
    const/4 v13, 0x0

    invoke-virtual {v12, v13, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 478
    .local v4, "cookieName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/cloudclient/MusicRequest;->mPassthroughCookies:Ljava/util/TreeSet;

    invoke-virtual {v13, v4}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 479
    const/4 v13, 0x0

    add-int/lit8 v14, v5, 0x1

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 487
    .end local v2    # "cookie":Lorg/apache/http/Header;
    .end local v4    # "cookieName":Ljava/lang/String;
    .end local v5    # "endOfCookieIndex":I
    .end local v9    # "nameLength":I
    .end local v12    # "setCookieString":Ljava/lang/String;
    .local v6, "i$":Ljava/util/Iterator;
    :cond_8
    return-object v11
.end method

.method private prepareRequest(Lorg/apache/http/client/methods/HttpRequestBase;)V
    .locals 6
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpRequestBase;

    .prologue
    .line 388
    invoke-virtual {p1}, Lorg/apache/http/client/methods/HttpRequestBase;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    .line 390
    .local v1, "params":Lorg/apache/http/params/HttpParams;
    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicRequest;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "music_http_connection_timeout_ms"

    const v5, 0xafc8

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 392
    .local v0, "connectionTimeout":I
    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicRequest;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "music_http_socket_timeout_ms"

    const/16 v5, 0x7530

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 395
    .local v2, "socketTimeout":I
    invoke-static {v1, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 396
    invoke-static {v1, v2}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 397
    invoke-virtual {p1, v1}, Lorg/apache/http/client/methods/HttpRequestBase;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 402
    const-string v3, "X-Device-ID"

    iget-object v4, p0, Lcom/google/android/music/cloudclient/MusicRequest;->mAndroidId:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    const-string v3, "X-Device-Logging-ID"

    iget-object v4, p0, Lcom/google/android/music/cloudclient/MusicRequest;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getLoggingId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    return-void
.end method

.method private prepareUnauthorizedRequest(Lorg/apache/http/client/methods/HttpRequestBase;)V
    .locals 1
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpRequestBase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 433
    invoke-direct {p0, p1}, Lcom/google/android/music/cloudclient/MusicRequest;->prepareRequest(Lorg/apache/http/client/methods/HttpRequestBase;)V

    .line 434
    const-string v0, "Authorization"

    invoke-virtual {p1, v0}, Lorg/apache/http/client/methods/HttpRequestBase;->removeHeaders(Ljava/lang/String;)V

    .line 435
    return-void
.end method

.method public static readAndReleaseShortResponse(Lorg/apache/http/client/methods/HttpRequestBase;Lorg/apache/http/HttpResponse;I)[B
    .locals 4
    .param p0, "request"    # Lorg/apache/http/client/methods/HttpRequestBase;
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .param p2, "maxSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 134
    .local v0, "entity":Lorg/apache/http/HttpEntity;
    const/4 v2, 0x0

    .line 136
    .local v2, "responseData":[B
    :try_start_0
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 137
    .local v1, "in":Ljava/io/InputStream;
    sget-object v3, Lcom/google/android/music/utils/DebugUtils$MusicTag;->HTTP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v3}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 138
    invoke-static {v1}, Lcom/google/android/music/cloudclient/MusicRequest;->logInputStreamContents(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v1

    .line 140
    :cond_0
    invoke-static {v1, p2}, Lcom/google/android/music/utils/IOUtils;->readSmallStream(Ljava/io/InputStream;I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 142
    invoke-static {p0, p1}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    .line 144
    return-object v2

    .line 142
    .end local v1    # "in":Ljava/io/InputStream;
    :catchall_0
    move-exception v3

    invoke-static {p0, p1}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    throw v3
.end method

.method public static releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V
    .locals 5
    .param p0, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 491
    if-eqz p1, :cond_1

    .line 492
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 493
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v1, :cond_0

    .line 495
    :try_start_0
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 503
    .end local v1    # "entity":Lorg/apache/http/HttpEntity;
    :cond_0
    :goto_0
    return-void

    .line 496
    .restart local v1    # "entity":Lorg/apache/http/HttpEntity;
    :catch_0
    move-exception v0

    .line 497
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "MusicStreaming"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error consuming entity. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 500
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "entity":Lorg/apache/http/HttpEntity;
    :cond_1
    if-eqz p0, :cond_0

    .line 501
    invoke-interface {p0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    goto :goto_0
.end method

.method public static rewriteURI(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "original"    # Ljava/lang/String;

    .prologue
    .line 537
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/common/http/UrlRules;->getRules(Landroid/content/ContentResolver;)Lcom/google/android/common/http/UrlRules;

    move-result-object v1

    .line 538
    .local v1, "rules":Lcom/google/android/common/http/UrlRules;
    invoke-virtual {v1, p1}, Lcom/google/android/common/http/UrlRules;->matchRule(Ljava/lang/String;)Lcom/google/android/common/http/UrlRules$Rule;

    move-result-object v0

    .line 539
    .local v0, "rule":Lcom/google/android/common/http/UrlRules$Rule;
    invoke-virtual {v0, p1}, Lcom/google/android/common/http/UrlRules$Rule;->apply(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static updateAuthorization(Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;)V
    .locals 3
    .param p0, "request"    # Lorg/apache/http/client/methods/HttpRequestBase;
    .param p1, "authToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 416
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    new-instance v0, Ljava/io/IOException;

    const-string v1, "No auth token available."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 422
    :cond_0
    const-string v0, "Authorization"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GoogleLogin auth="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    return-void
.end method


# virtual methods
.method protected getAuthToken()Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Lorg/apache/http/client/HttpResponseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 517
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/cloudclient/MusicRequest;->mMusicAuthInfo:Lcom/google/android/music/sync/google/MusicAuthInfo;

    invoke-direct {p0}, Lcom/google/android/music/cloudclient/MusicRequest;->getAuthAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/sync/google/MusicAuthInfo;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 518
    :catch_0
    move-exception v0

    .line 519
    .local v0, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v0}, Landroid/accounts/AuthenticatorException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    instance-of v2, v2, Landroid/accounts/OperationCanceledException;

    if-eqz v2, :cond_0

    .line 520
    const-string v2, "MusicStreaming"

    const-string v3, "Getting auth token canceled"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    new-instance v1, Ljava/lang/InterruptedException;

    invoke-direct {v1}, Ljava/lang/InterruptedException;-><init>()V

    .line 522
    .local v1, "ex":Ljava/lang/InterruptedException;
    invoke-virtual {v1, v0}, Ljava/lang/InterruptedException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 523
    throw v1

    .line 525
    .end local v1    # "ex":Ljava/lang/InterruptedException;
    :cond_0
    const-string v2, "MusicStreaming"

    const-string v3, "Failed to get auth token"

    invoke-static {v2, v3, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 526
    new-instance v2, Lorg/apache/http/client/HttpResponseException;

    const/16 v3, 0x191

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to obtain auth token for music streaming: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/accounts/AuthenticatorException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v2
.end method

.method protected onBeforeExecute(Lorg/apache/http/client/methods/HttpRequestBase;)V
    .locals 0
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpRequestBase;

    .prologue
    .line 532
    return-void
.end method

.method public sendRequest(Lorg/apache/http/client/methods/HttpRequestBase;Lcom/google/android/music/cloudclient/MusicHttpClient;)Lorg/apache/http/HttpResponse;
    .locals 29
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpRequestBase;
    .param p2, "httpClient"    # Lcom/google/android/music/cloudclient/MusicHttpClient;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/cloudclient/MusicRequest;->getAuthToken()Ljava/lang/String;

    move-result-object v6

    .line 222
    .local v6, "authToken":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/cloudclient/MusicRequest;->getAuthAccount()Landroid/accounts/Account;

    move-result-object v4

    .line 224
    .local v4, "account":Landroid/accounts/Account;
    const/16 v19, 0x0

    .line 225
    .local v19, "response":Lorg/apache/http/HttpResponse;
    const/16 v23, 0x1f4

    .line 226
    .local v23, "status":I
    const/4 v15, 0x0

    .line 227
    .local v15, "redirectCount":I
    const/16 v22, 0x1

    .line 229
    .local v22, "retryAuthFailure":Z
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 230
    .local v7, "cookies":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    .line 232
    .local v8, "currentThread":Ljava/lang/Thread;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lcom/google/android/music/cloudclient/MusicRequest;->prepareAuthorizedRequest(Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;)V

    .line 233
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/music/cloudclient/MusicRequest;->onBeforeExecute(Lorg/apache/http/client/methods/HttpRequestBase;)V

    .line 235
    const/4 v9, 0x0

    .line 236
    .local v9, "done":Z
    :goto_0
    if-nez v9, :cond_1b

    .line 238
    const/4 v9, 0x1

    .line 241
    invoke-virtual {v8}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v26

    if-eqz v26, :cond_0

    .line 242
    new-instance v26, Ljava/lang/InterruptedException;

    invoke-direct/range {v26 .. v26}, Ljava/lang/InterruptedException;-><init>()V

    throw v26

    .line 245
    :cond_0
    sget-boolean v26, Lcom/google/android/music/cloudclient/MusicRequest;->LOGV:Z

    if-eqz v26, :cond_1

    .line 246
    const-string v26, "MusicStreaming"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Requesting URL: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {p1 .. p1}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :cond_1
    const/16 v25, 0x0

    .line 250
    .local v25, "success":Z
    const/16 v19, 0x0

    .line 252
    :try_start_0
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/music/cloudclient/MusicHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v19

    .line 253
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v24

    .line 254
    .local v24, "statusLine":Lorg/apache/http/StatusLine;
    if-nez v24, :cond_3

    .line 255
    const-string v26, "MusicStreaming"

    const-string v27, "Stream-download response status line is null."

    invoke-static/range {v26 .. v27}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    new-instance v26, Ljava/io/IOException;

    const-string v27, "StatusLine is null -- should not happen."

    invoke-direct/range {v26 .. v27}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v26
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    .end local v24    # "statusLine":Lorg/apache/http/StatusLine;
    :catchall_0
    move-exception v26

    if-nez v25, :cond_2

    .line 279
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    :cond_2
    throw v26

    .line 259
    .restart local v24    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_3
    :try_start_1
    invoke-interface/range {v24 .. v24}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v23

    .line 260
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/cloudclient/MusicRequest;->mLogHttp:Z

    move/from16 v26, v0

    if-eqz v26, :cond_5

    .line 261
    sget-object v26, Lcom/google/android/music/utils/DebugUtils;->HTTP_TAG:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Respone status="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v5

    .local v5, "arr$":[Lorg/apache/http/Header;
    array-length v12, v5

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_1
    if-ge v11, v12, :cond_5

    aget-object v10, v5, v11

    .line 263
    .local v10, "header":Lorg/apache/http/Header;
    invoke-interface {v10}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v26

    const-string v27, "Set-Cookie"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v26

    if-nez v26, :cond_4

    .line 262
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 266
    :cond_4
    sget-object v26, Lcom/google/android/music/utils/DebugUtils;->HTTP_TAG:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Response header: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-interface {v10}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ": "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-interface {v10}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 270
    .end local v5    # "arr$":[Lorg/apache/http/Header;
    .end local v10    # "header":Lorg/apache/http/Header;
    .end local v11    # "i$":I
    .end local v12    # "len$":I
    :cond_5
    const/16 v26, 0xc8

    move/from16 v0, v23

    move/from16 v1, v26

    if-lt v0, v1, :cond_8

    const/16 v26, 0x12c

    move/from16 v0, v23

    move/from16 v1, v26

    if-ge v0, v1, :cond_8

    const/16 v26, 0xcc

    move/from16 v0, v23

    move/from16 v1, v26

    if-eq v0, v1, :cond_6

    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v26

    if-eqz v26, :cond_8

    .line 274
    :cond_6
    const/16 v25, 0x1

    .line 278
    if-nez v25, :cond_7

    .line 279
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    :cond_7
    return-object v19

    .line 278
    :cond_8
    if-nez v25, :cond_9

    .line 279
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    .line 284
    :cond_9
    const/16 v26, 0x12e

    move/from16 v0, v23

    move/from16 v1, v26

    if-ne v0, v1, :cond_b

    .line 286
    const/16 v26, 0xa

    move/from16 v0, v26

    if-lt v15, v0, :cond_a

    .line 287
    new-instance v26, Ljava/io/IOException;

    const-string v27, "Music request failed due to too many redirects."

    invoke-direct/range {v26 .. v27}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v26

    .line 290
    :cond_a
    add-int/lit8 v15, v15, 0x1

    .line 291
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v7}, Lcom/google/android/music/cloudclient/MusicRequest;->prepareRedirectRequestAndUpdateCookies(Lorg/apache/http/HttpResponse;Ljava/util/List;)Lorg/apache/http/client/methods/HttpGet;

    move-result-object p1

    .line 292
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 294
    :cond_b
    const/16 v26, 0x191

    move/from16 v0, v23

    move/from16 v1, v26

    if-ne v0, v1, :cond_f

    .line 295
    const-string v13, "Received 401 Unauthorized from server."

    .line 296
    .local v13, "msg":Ljava/lang/String;
    sget-boolean v26, Lcom/google/android/music/cloudclient/MusicRequest;->LOGV:Z

    if-eqz v26, :cond_c

    .line 297
    const-string v26, "MusicStreaming"

    move-object/from16 v0, v26

    invoke-static {v0, v13}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :cond_c
    if-eqz v4, :cond_e

    .line 301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/cloudclient/MusicRequest;->mMusicAuthInfo:Lcom/google/android/music/sync/google/MusicAuthInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v6}, Lcom/google/android/music/sync/google/MusicAuthInfo;->invalidateAuthToken(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 302
    if-eqz v22, :cond_e

    if-nez v15, :cond_e

    .line 304
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/cloudclient/MusicRequest;->getAuthToken()Ljava/lang/String;

    move-result-object v6

    .line 305
    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/google/android/music/cloudclient/MusicRequest;->updateAuthorization(Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;)V

    .line 306
    const/16 v22, 0x0

    .line 307
    sget-boolean v26, Lcom/google/android/music/cloudclient/MusicRequest;->LOGV:Z

    if-eqz v26, :cond_d

    const-string v26, "MusicStreaming"

    const-string v27, "Will retry with updated token"

    invoke-static/range {v26 .. v27}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    :cond_d
    const/4 v9, 0x0

    .line 309
    goto/16 :goto_0

    .line 312
    :cond_e
    new-instance v26, Lorg/apache/http/client/HttpResponseException;

    move-object/from16 v0, v26

    move/from16 v1, v23

    invoke-direct {v0, v1, v13}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v26

    .line 313
    .end local v13    # "msg":Ljava/lang/String;
    :cond_f
    const/16 v26, 0x193

    move/from16 v0, v23

    move/from16 v1, v26

    if-ne v0, v1, :cond_15

    .line 314
    const-string v26, "X-Rejected-Reason"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getLastHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v18

    .line 316
    .local v18, "rejectionReasonHeader":Lorg/apache/http/Header;
    if-eqz v18, :cond_14

    .line 317
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v16

    .line 318
    .local v16, "rejectionReason":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_14

    .line 319
    const/16 v17, 0x0

    .line 320
    .local v17, "rejectionReasonEnum":Lcom/google/android/music/download/ServerRejectionException$RejectionReason;
    const-string v26, "DEVICE_NOT_AUTHORIZED"

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_11

    .line 322
    sget-object v17, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->DEVICE_NOT_AUTHORIZED:Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    .line 331
    :cond_10
    :goto_3
    if-eqz v17, :cond_13

    .line 334
    new-instance v26, Lcom/google/android/music/download/ServerRejectionException;

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/music/download/ServerRejectionException;-><init>(Lcom/google/android/music/download/ServerRejectionException$RejectionReason;)V

    throw v26

    .line 323
    :cond_11
    const-string v26, "ANOTHER_STREAM_BEING_PLAYED"

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_12

    .line 325
    sget-object v17, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->ANOTHER_STREAM_BEING_PLAYED:Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    goto :goto_3

    .line 326
    :cond_12
    const-string v26, "STREAM_RATE_LIMIT_REACHED"

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_10

    .line 328
    sget-object v17, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->STREAM_RATE_LIMIT_REACHED:Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    goto :goto_3

    .line 336
    :cond_13
    const-string v26, "MusicStreaming"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Server returned an unknown rejection reason: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    .end local v16    # "rejectionReason":Ljava/lang/String;
    .end local v17    # "rejectionReasonEnum":Lcom/google/android/music/download/ServerRejectionException$RejectionReason;
    :cond_14
    new-instance v26, Lorg/apache/http/client/HttpResponseException;

    const-string v27, "Unable to stream due to 403 error"

    move-object/from16 v0, v26

    move/from16 v1, v23

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v26

    .line 343
    .end local v18    # "rejectionReasonHeader":Lorg/apache/http/Header;
    :cond_15
    const/16 v26, 0x194

    move/from16 v0, v23

    move/from16 v1, v26

    if-ne v0, v1, :cond_17

    .line 344
    const-string v13, "Music request failed due to 404 (file not found) error"

    .line 345
    .restart local v13    # "msg":Ljava/lang/String;
    sget-boolean v26, Lcom/google/android/music/cloudclient/MusicRequest;->LOGV:Z

    if-eqz v26, :cond_16

    const-string v26, "MusicStreaming"

    move-object/from16 v0, v26

    invoke-static {v0, v13}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    :cond_16
    new-instance v26, Lorg/apache/http/client/HttpResponseException;

    move-object/from16 v0, v26

    move/from16 v1, v23

    invoke-direct {v0, v1, v13}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v26

    .line 348
    .end local v13    # "msg":Ljava/lang/String;
    :cond_17
    const/16 v26, 0x1f7

    move/from16 v0, v23

    move/from16 v1, v26

    if-ne v0, v1, :cond_19

    .line 349
    const-string v26, "Retry-After"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v26

    if-eqz v26, :cond_18

    .line 351
    :try_start_2
    const-string v26, "Retry-After"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 353
    .local v20, "retryAfterInSeconds":J
    const-string v26, "MusicStreaming"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Server said to retry after "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " seconds"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    new-instance v26, Lcom/google/android/music/download/ServiceUnavailableException;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Music request failed due to 503 (Service Unavailable) error.  Unavailable for "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " seconds."

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v26

    move-wide/from16 v1, v20

    move-object/from16 v3, v27

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/download/ServiceUnavailableException;-><init>(JLjava/lang/String;)V

    throw v26
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    .line 358
    .end local v20    # "retryAfterInSeconds":J
    :catch_0
    move-exception v14

    .line 361
    .local v14, "ne":Ljava/lang/NumberFormatException;
    new-instance v26, Lorg/apache/http/client/HttpResponseException;

    const-string v27, "Music request failed due to 503 error."

    move-object/from16 v0, v26

    move/from16 v1, v23

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v26

    .line 365
    .end local v14    # "ne":Ljava/lang/NumberFormatException;
    :cond_18
    const-string v26, "MusicStreaming"

    const-string v27, "Received 503 with no Retry-After header"

    invoke-static/range {v26 .. v27}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    new-instance v26, Lorg/apache/http/client/HttpResponseException;

    const-string v27, "Music request failed due to 503 with no Retry-After header."

    move-object/from16 v0, v26

    move/from16 v1, v23

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v26

    .line 370
    :cond_19
    sget-boolean v26, Lcom/google/android/music/cloudclient/MusicRequest;->LOGV:Z

    if-eqz v26, :cond_1a

    const-string v26, "MusicStreaming"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Music request failed due to HTTP error "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    :cond_1a
    new-instance v26, Lorg/apache/http/client/HttpResponseException;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Music request failed due to HTTP error "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v26

    move/from16 v1, v23

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v26

    .line 378
    .end local v24    # "statusLine":Lorg/apache/http/StatusLine;
    .end local v25    # "success":Z
    :cond_1b
    new-instance v26, Ljava/io/IOException;

    const-string v27, "Unable to retreive stream"

    invoke-direct/range {v26 .. v27}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v26
.end method
