.class public Lcom/google/android/music/AvailableSpaceTracker;
.super Ljava/lang/Object;
.source "AvailableSpaceTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/AvailableSpaceTracker$AvailableSpaceChangedListener;,
        Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;,
        Lcom/google/android/music/AvailableSpaceTracker$PlaylistChangedListener;,
        Lcom/google/android/music/AvailableSpaceTracker$AlbumChangedListener;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private mAlbumListeners:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/music/AvailableSpaceTracker$AlbumChangedListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mAlbumLock:Ljava/lang/Object;

.field private mAvailabilityTransition:Z

.field private final mDeselectedAlbums:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mDeselectedAutoPlaylists:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mDeselectedPlaylists:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mDeselectedRadioStations:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mFreeSpaceChange:J

.field private mFreeSpaceOnDevice:J

.field private mIsActive:Z

.field private mListeners:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/music/AvailableSpaceTracker$AvailableSpaceChangedListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPlaylistListeners:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/music/AvailableSpaceTracker$PlaylistChangedListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mSelectedAlbums:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mSelectedAutoPlaylists:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mSelectedPlaylists:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mSelectedRadioStations:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mSpaceVariablesLock:Ljava/lang/Object;

.field private mStoreSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

.field private mStoreService:Lcom/google/android/music/store/IStoreService;

.field private mTotalSpace:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/AvailableSpaceTracker;->LOGV:Z

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/AvailableSpaceTracker;)Lcom/google/android/music/store/IStoreService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AvailableSpaceTracker;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mStoreService:Lcom/google/android/music/store/IStoreService;

    return-object v0
.end method

.method static synthetic access$1014(Lcom/google/android/music/AvailableSpaceTracker;J)J
    .locals 3
    .param p0, "x0"    # Lcom/google/android/music/AvailableSpaceTracker;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mFreeSpaceChange:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mFreeSpaceChange:J

    return-wide v0
.end method

.method static synthetic access$1022(Lcom/google/android/music/AvailableSpaceTracker;J)J
    .locals 3
    .param p0, "x0"    # Lcom/google/android/music/AvailableSpaceTracker;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mFreeSpaceChange:J

    sub-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mFreeSpaceChange:J

    return-wide v0
.end method

.method static synthetic access$1102(Lcom/google/android/music/AvailableSpaceTracker;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AvailableSpaceTracker;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/google/android/music/AvailableSpaceTracker;->mAvailabilityTransition:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/google/android/music/AvailableSpaceTracker;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AvailableSpaceTracker;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/music/AvailableSpaceTracker;->notifyAlbumChangedListeners(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/AvailableSpaceTracker;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AvailableSpaceTracker;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSpaceVariablesLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/music/AvailableSpaceTracker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AvailableSpaceTracker;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/music/AvailableSpaceTracker;->notifyAvailableSpaceChangedListener()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/music/AvailableSpaceTracker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AvailableSpaceTracker;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/music/AvailableSpaceTracker;->waitForStoreConnection()V

    return-void
.end method

.method private checkActive()V
    .locals 2

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mIsActive:Z

    if-nez v0, :cond_0

    .line 199
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not make changes once a session is committed or cancelled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_0
    return-void
.end method

.method private clearAlbum(J)Z
    .locals 3
    .param p1, "albumId"    # J

    .prologue
    .line 715
    iget-object v1, p0, Lcom/google/android/music/AvailableSpaceTracker;->mAlbumLock:Ljava/lang/Object;

    monitor-enter v1

    .line 716
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSelectedAlbums:Ljava/util/TreeSet;

    iget-object v2, p0, Lcom/google/android/music/AvailableSpaceTracker;->mDeselectedAlbums:Ljava/util/TreeSet;

    invoke-direct {p0, p1, p2, v0, v2}, Lcom/google/android/music/AvailableSpaceTracker;->moveIdFromTo(JLjava/util/TreeSet;Ljava/util/TreeSet;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 717
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private clearAlbumAndNotify(J)V
    .locals 3
    .param p1, "albumId"    # J

    .prologue
    .line 725
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/AvailableSpaceTracker;->clearAlbum(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 726
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/AvailableSpaceTracker;->notifyAlbumChangedListeners(Ljava/util/List;)V

    .line 727
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/music/AvailableSpaceTracker;->submitUpdateFreespaceTask(JIZ)V

    .line 729
    :cond_0
    return-void
.end method

.method private isAvailableSpaceInitialized()Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 472
    iget-object v1, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSpaceVariablesLock:Ljava/lang/Object;

    monitor-enter v1

    .line 473
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/music/AvailableSpaceTracker;->mFreeSpaceOnDevice:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    iget-wide v2, p0, Lcom/google/android/music/AvailableSpaceTracker;->mTotalSpace:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 474
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private markAlbum(J)Z
    .locals 3
    .param p1, "albumId"    # J

    .prologue
    .line 692
    iget-object v1, p0, Lcom/google/android/music/AvailableSpaceTracker;->mAlbumLock:Ljava/lang/Object;

    monitor-enter v1

    .line 693
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mDeselectedAlbums:Ljava/util/TreeSet;

    iget-object v2, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSelectedAlbums:Ljava/util/TreeSet;

    invoke-direct {p0, p1, p2, v0, v2}, Lcom/google/android/music/AvailableSpaceTracker;->moveIdFromTo(JLjava/util/TreeSet;Ljava/util/TreeSet;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 694
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private markAlbumAndNotify(J)V
    .locals 3
    .param p1, "albumId"    # J

    .prologue
    const/4 v1, 0x1

    .line 702
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/AvailableSpaceTracker;->markAlbum(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 703
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/AvailableSpaceTracker;->notifyAlbumChangedListeners(Ljava/util/List;)V

    .line 704
    invoke-direct {p0, p1, p2, v1, v1}, Lcom/google/android/music/AvailableSpaceTracker;->submitUpdateFreespaceTask(JIZ)V

    .line 706
    :cond_0
    return-void
.end method

.method private moveIdFromTo(JLjava/util/TreeSet;Ljava/util/TreeSet;)Z
    .locals 3
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 734
    .local p3, "fromSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/Long;>;"
    .local p4, "toSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .line 736
    .local v0, "modified":Z
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p4, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 737
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 739
    return v0
.end method

.method private notifyAlbumChangedListeners(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 658
    .local p1, "albumids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker;->mAlbumListeners:Ljava/util/LinkedList;

    monitor-enter v3

    .line 659
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/AvailableSpaceTracker;->mAlbumListeners:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 660
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/AvailableSpaceTracker$AlbumChangedListener;>;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 661
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/AvailableSpaceTracker$AlbumChangedListener;

    .line 662
    .local v1, "listener":Lcom/google/android/music/AvailableSpaceTracker$AlbumChangedListener;
    if-nez v1, :cond_0

    .line 663
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 668
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/AvailableSpaceTracker$AlbumChangedListener;>;>;"
    .end local v1    # "listener":Lcom/google/android/music/AvailableSpaceTracker$AlbumChangedListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 665
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/AvailableSpaceTracker$AlbumChangedListener;>;>;"
    .restart local v1    # "listener":Lcom/google/android/music/AvailableSpaceTracker$AlbumChangedListener;
    :cond_0
    :try_start_1
    invoke-interface {v1, p1}, Lcom/google/android/music/AvailableSpaceTracker$AlbumChangedListener;->onAlbumChanged(Ljava/util/List;)V

    goto :goto_0

    .line 668
    .end local v1    # "listener":Lcom/google/android/music/AvailableSpaceTracker$AlbumChangedListener;
    :cond_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 669
    return-void
.end method

.method private notifyAvailableSpaceChangedListener()V
    .locals 9

    .prologue
    .line 607
    iget-object v8, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSpaceVariablesLock:Ljava/lang/Object;

    monitor-enter v8

    .line 608
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/music/AvailableSpaceTracker;->mTotalSpace:J

    .line 609
    .local v2, "totalSpace":J
    invoke-virtual {p0}, Lcom/google/android/music/AvailableSpaceTracker;->getCombinedFreeSpace()J

    move-result-wide v4

    .line 610
    .local v4, "freeSpace":J
    iget-boolean v6, p0, Lcom/google/android/music/AvailableSpaceTracker;->mAvailabilityTransition:Z

    .line 611
    .local v6, "transition":Z
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/google/android/music/AvailableSpaceTracker;->mAvailabilityTransition:Z

    .line 612
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613
    iget-object v8, p0, Lcom/google/android/music/AvailableSpaceTracker;->mListeners:Ljava/util/LinkedList;

    monitor-enter v8

    .line 614
    :try_start_1
    iget-object v7, p0, Lcom/google/android/music/AvailableSpaceTracker;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 615
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/AvailableSpaceTracker$AvailableSpaceChangedListener;>;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 616
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/AvailableSpaceTracker$AvailableSpaceChangedListener;

    .line 617
    .local v1, "listener":Lcom/google/android/music/AvailableSpaceTracker$AvailableSpaceChangedListener;
    if-nez v1, :cond_0

    .line 618
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 623
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/AvailableSpaceTracker$AvailableSpaceChangedListener;>;>;"
    .end local v1    # "listener":Lcom/google/android/music/AvailableSpaceTracker$AvailableSpaceChangedListener;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    .line 612
    .end local v2    # "totalSpace":J
    .end local v4    # "freeSpace":J
    .end local v6    # "transition":Z
    :catchall_1
    move-exception v7

    :try_start_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v7

    .line 620
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/AvailableSpaceTracker$AvailableSpaceChangedListener;>;>;"
    .restart local v1    # "listener":Lcom/google/android/music/AvailableSpaceTracker$AvailableSpaceChangedListener;
    .restart local v2    # "totalSpace":J
    .restart local v4    # "freeSpace":J
    .restart local v6    # "transition":Z
    :cond_0
    :try_start_3
    invoke-interface/range {v1 .. v6}, Lcom/google/android/music/AvailableSpaceTracker$AvailableSpaceChangedListener;->onAvailableSpaceChanged(JJZ)V

    goto :goto_0

    .line 623
    .end local v1    # "listener":Lcom/google/android/music/AvailableSpaceTracker$AvailableSpaceChangedListener;
    :cond_1
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 624
    return-void
.end method

.method private notifyPlaylistChangedListeners(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 672
    .local p1, "playlistIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker;->mPlaylistListeners:Ljava/util/LinkedList;

    monitor-enter v3

    .line 673
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/AvailableSpaceTracker;->mPlaylistListeners:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 674
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/AvailableSpaceTracker$PlaylistChangedListener;>;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 675
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/AvailableSpaceTracker$PlaylistChangedListener;

    .line 676
    .local v1, "listener":Lcom/google/android/music/AvailableSpaceTracker$PlaylistChangedListener;
    if-nez v1, :cond_0

    .line 677
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 682
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/AvailableSpaceTracker$PlaylistChangedListener;>;>;"
    .end local v1    # "listener":Lcom/google/android/music/AvailableSpaceTracker$PlaylistChangedListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 679
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/AvailableSpaceTracker$PlaylistChangedListener;>;>;"
    .restart local v1    # "listener":Lcom/google/android/music/AvailableSpaceTracker$PlaylistChangedListener;
    :cond_0
    :try_start_1
    invoke-interface {v1, p1}, Lcom/google/android/music/AvailableSpaceTracker$PlaylistChangedListener;->onPlaylistChanged(Ljava/util/List;)V

    goto :goto_0

    .line 682
    .end local v1    # "listener":Lcom/google/android/music/AvailableSpaceTracker$PlaylistChangedListener;
    :cond_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 683
    return-void
.end method

.method private submitUpdateFreespaceTask(JIZ)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "type"    # I
    .param p4, "added"    # Z

    .prologue
    .line 456
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 457
    .local v0, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 458
    invoke-direct {p0, v0, p3, p4}, Lcom/google/android/music/AvailableSpaceTracker;->submitUpdateFreespaceTask(Ljava/util/List;IZ)V

    .line 459
    return-void
.end method

.method private submitUpdateFreespaceTask(Ljava/util/List;IZ)V
    .locals 1
    .param p2, "type"    # I
    .param p3, "added"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 462
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 463
    invoke-direct {p0}, Lcom/google/android/music/AvailableSpaceTracker;->isAvailableSpaceInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464
    new-instance v0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;-><init>(Lcom/google/android/music/AvailableSpaceTracker;Ljava/util/List;IZ)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 466
    :cond_0
    return-void
.end method

.method private waitForStoreConnection()V
    .locals 4

    .prologue
    .line 181
    iget-object v1, p0, Lcom/google/android/music/AvailableSpaceTracker;->mStoreSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    monitor-enter v1

    .line 182
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mStoreService:Lcom/google/android/music/store/IStoreService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 184
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mStoreSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 189
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mStoreService:Lcom/google/android/music/store/IStoreService;

    if-nez v0, :cond_0

    .line 190
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Could not connect to store service"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 195
    return-void

    .line 185
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public deselectAlbum(J)V
    .locals 1
    .param p1, "albumId"    # J

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/google/android/music/AvailableSpaceTracker;->checkActive()V

    .line 211
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/AvailableSpaceTracker;->clearAlbumAndNotify(J)V

    .line 212
    return-void
.end method

.method public deselectAutoPlaylist(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 262
    invoke-direct {p0}, Lcom/google/android/music/AvailableSpaceTracker;->checkActive()V

    .line 263
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSelectedAutoPlaylists:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mDeselectedAutoPlaylists:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 266
    :cond_0
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/music/AvailableSpaceTracker;->submitUpdateFreespaceTask(JIZ)V

    .line 267
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/AvailableSpaceTracker;->notifyPlaylistChangedListeners(Ljava/util/List;)V

    .line 268
    return-void
.end method

.method public deselectPlaylist(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/music/AvailableSpaceTracker;->checkActive()V

    .line 235
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSelectedPlaylists:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mDeselectedPlaylists:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 238
    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/music/AvailableSpaceTracker;->submitUpdateFreespaceTask(JIZ)V

    .line 239
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/AvailableSpaceTracker;->notifyPlaylistChangedListeners(Ljava/util/List;)V

    .line 240
    return-void
.end method

.method public deselectRadioStation(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 290
    invoke-direct {p0}, Lcom/google/android/music/AvailableSpaceTracker;->checkActive()V

    .line 291
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSelectedRadioStations:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mDeselectedRadioStations:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 294
    :cond_0
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/music/AvailableSpaceTracker;->submitUpdateFreespaceTask(JIZ)V

    .line 295
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/AvailableSpaceTracker;->notifyPlaylistChangedListeners(Ljava/util/List;)V

    .line 296
    return-void
.end method

.method public getCombinedFreeSpace()J
    .locals 4

    .prologue
    .line 432
    iget-wide v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mFreeSpaceOnDevice:J

    iget-wide v2, p0, Lcom/google/android/music/AvailableSpaceTracker;->mFreeSpaceChange:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public isAlbumSelected(J)Ljava/lang/Boolean;
    .locals 3
    .param p1, "albumId"    # J

    .prologue
    .line 215
    iget-object v1, p0, Lcom/google/android/music/AvailableSpaceTracker;->mAlbumLock:Ljava/lang/Object;

    monitor-enter v1

    .line 216
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSelectedAlbums:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    monitor-exit v1

    .line 221
    :goto_0
    return-object v0

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mDeselectedAlbums:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 221
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public isAutoPlaylistSelected(J)Ljava/lang/Boolean;
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mDeselectedAutoPlaylists:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 276
    :goto_0
    return-object v0

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSelectedAutoPlaylists:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 276
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaylistSelected(J)Ljava/lang/Boolean;
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mDeselectedPlaylists:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 248
    :goto_0
    return-object v0

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSelectedPlaylists:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 248
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRadioStationSelected(J)Ljava/lang/Boolean;
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mDeselectedRadioStations:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 304
    :goto_0
    return-object v0

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSelectedRadioStations:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 302
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 304
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public selectAlbum(J)V
    .locals 1
    .param p1, "albumId"    # J

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/google/android/music/AvailableSpaceTracker;->checkActive()V

    .line 206
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/AvailableSpaceTracker;->markAlbumAndNotify(J)V

    .line 207
    return-void
.end method

.method public selectAutoPlaylist(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 252
    sget-boolean v0, Lcom/google/android/music/AvailableSpaceTracker;->LOGV:Z

    if-eqz v0, :cond_0

    .line 253
    const-string v0, "AvailableSpaceTracker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "selectAutoPlaylist: id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/AvailableSpaceTracker;->checkActive()V

    .line 256
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSelectedAutoPlaylists:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 257
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/music/AvailableSpaceTracker;->submitUpdateFreespaceTask(JIZ)V

    .line 258
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/AvailableSpaceTracker;->notifyPlaylistChangedListeners(Ljava/util/List;)V

    .line 259
    return-void
.end method

.method public selectPlaylist(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 227
    invoke-direct {p0}, Lcom/google/android/music/AvailableSpaceTracker;->checkActive()V

    .line 228
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSelectedPlaylists:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 229
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/music/AvailableSpaceTracker;->submitUpdateFreespaceTask(JIZ)V

    .line 230
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/AvailableSpaceTracker;->notifyPlaylistChangedListeners(Ljava/util/List;)V

    .line 231
    return-void
.end method

.method public selectRadioStation(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 280
    sget-boolean v0, Lcom/google/android/music/AvailableSpaceTracker;->LOGV:Z

    if-eqz v0, :cond_0

    .line 281
    const-string v0, "AvailableSpaceTracker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "selectAutoPlaylist: id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/AvailableSpaceTracker;->checkActive()V

    .line 284
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker;->mSelectedRadioStations:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 285
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/music/AvailableSpaceTracker;->submitUpdateFreespaceTask(JIZ)V

    .line 286
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/AvailableSpaceTracker;->notifyPlaylistChangedListeners(Ljava/util/List;)V

    .line 287
    return-void
.end method
