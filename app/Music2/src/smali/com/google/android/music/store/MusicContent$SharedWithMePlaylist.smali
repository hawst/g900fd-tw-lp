.class public final Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;
.super Ljava/lang/Object;
.source "MusicContent.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/MusicContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SharedWithMePlaylist"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist$Members;
    }
.end annotation


# static fields
.field public static ART_URL:Ljava/lang/String;

.field public static CREATION_TIMESTAMP:Ljava/lang/String;

.field public static DESCRIPTION:Ljava/lang/String;

.field public static LAST_MODIFIED_TIMESTAMP:Ljava/lang/String;

.field public static NAME:Ljava/lang/String;

.field public static OWNER_NAME:Ljava/lang/String;

.field public static OWNER_PROFILE_PHOTO_URL:Ljava/lang/String;

.field public static SHARE_TOKEN:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3348
    const-string v0, "creationTimestamp"

    sput-object v0, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->CREATION_TIMESTAMP:Ljava/lang/String;

    .line 3350
    const-string v0, "lastModifiedTimestamp"

    sput-object v0, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->LAST_MODIFIED_TIMESTAMP:Ljava/lang/String;

    .line 3352
    const-string v0, "name"

    sput-object v0, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->NAME:Ljava/lang/String;

    .line 3354
    const-string v0, "shareToken"

    sput-object v0, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->SHARE_TOKEN:Ljava/lang/String;

    .line 3356
    const-string v0, "ownerName"

    sput-object v0, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->OWNER_NAME:Ljava/lang/String;

    .line 3358
    const-string v0, "description"

    sput-object v0, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->DESCRIPTION:Ljava/lang/String;

    .line 3360
    const-string v0, "artUrl"

    sput-object v0, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->ART_URL:Ljava/lang/String;

    .line 3362
    const-string v0, "ownerProfilePhotoUrl"

    sput-object v0, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->OWNER_PROFILE_PHOTO_URL:Ljava/lang/String;

    return-void
.end method
