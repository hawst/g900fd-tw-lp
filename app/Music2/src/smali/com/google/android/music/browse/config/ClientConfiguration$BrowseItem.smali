.class public Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
.super Ljava/lang/Object;
.source "ClientConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/browse/config/ClientConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BrowseItem"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;
    }
.end annotation


# instance fields
.field private mContent:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;",
            ">;"
        }
    .end annotation
.end field

.field private mDisplayResourceId:I

.field private mFullId:Ljava/lang/String;

.field private mId:Ljava/lang/String;

.field private mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->mContent:Ljava/util/List;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->mItems:Ljava/util/List;

    .line 145
    return-void
.end method


# virtual methods
.method public addContent(Ljava/lang/String;I)V
    .locals 2
    .param p1, "content"    # Ljava/lang/String;
    .param p2, "max"    # I

    .prologue
    .line 128
    if-eqz p1, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->mContent:Ljava/util/List;

    new-instance v1, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;

    invoke-direct {v1, p1, p2}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_0
    return-void
.end method

.method public addItem(Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;)V
    .locals 1
    .param p1, "item"    # Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    .prologue
    .line 122
    if-eqz p1, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    :cond_0
    return-void
.end method

.method public getContent()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->mContent:Ljava/util/List;

    return-object v0
.end method

.method public getDisplayResourceId()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->mDisplayResourceId:I

    return v0
.end method

.method public getFullId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->mFullId:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getItemById(Ljava/util/Iterator;)Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 94
    .end local p0    # "this":Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
    :goto_0
    return-object p0

    .line 87
    .restart local p0    # "this":Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 89
    .local v1, "id":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    .line 90
    .local v2, "item":Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
    invoke-virtual {v2}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 91
    invoke-virtual {v2, p1}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->getItemById(Ljava/util/Iterator;)Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    move-result-object p0

    goto :goto_0

    .line 94
    .end local v2    # "item":Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->mItems:Ljava/util/List;

    return-object v0
.end method

.method public setDisplay(I)V
    .locals 0
    .param p1, "resourceId"    # I

    .prologue
    .line 110
    iput p1, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->mDisplayResourceId:I

    .line 111
    return-void
.end method

.method public setFullId(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->mFullId:Ljava/lang/String;

    .line 143
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->mId:Ljava/lang/String;

    .line 139
    return-void
.end method
