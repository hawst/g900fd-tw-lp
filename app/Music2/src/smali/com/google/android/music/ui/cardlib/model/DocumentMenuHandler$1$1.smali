.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1$1;
.super Ljava/lang/Object;
.source "DocumentMenuHandler.java"

# interfaces
.implements Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->taskCompleted()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;

.field final synthetic val$entry:Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1$1;->this$1:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;

    iput-object p2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1$1;->val$entry:Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1$1;->val$entry:Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;->shouldRunAsync()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    new-instance v0, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuTask;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1$1;->val$entry:Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;

    invoke-direct {v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuTask;-><init>(Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 260
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1$1;->val$entry:Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;->onActionSelected()V

    goto :goto_0
.end method
