.class public Lcom/google/android/music/store/SearchHandler;
.super Ljava/lang/Object;
.source "SearchHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/SearchHandler$2;
    }
.end annotation


# static fields
.field private static final LIST_ITEM_COUNT_EXPRESSIONS:[Ljava/lang/String;

.field private static final LOGV:Z

.field private static final PLAYLIST_SEARCH_WHERE_CLAUSE:Ljava/lang/String;

.field private static final SEARCH_ALBUM_COUNT_EXPRESSIONS:[Ljava/lang/String;

.field private static final SEARCH_SONG_COUNT_EXPRESSIONS:[Ljava/lang/String;

.field private static sSearchAlbumMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sSearchArtistMaps:[Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sSearchPlaylistMaps:[Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sSearchTrackMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCancellationSignal:Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;

.field private final mContext:Landroid/content/Context;

.field private final mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private final mFilterIndex:I

.field private final mIsGlobalSearch:Z

.field private final mIsNautilusEnabled:Z

.field private final mLimit:Ljava/lang/String;

.field private final mNumSearchResults:I

.field private final mProjection:[Ljava/lang/String;

.field private final mQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;

.field private final mSearchQuery:Ljava/lang/String;

.field private final mStore:Lcom/google/android/music/store/Store;

.field private final mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x5

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v9, 0x2

    const/4 v10, 0x1

    .line 56
    sget-object v6, Lcom/google/android/music/utils/DebugUtils$MusicTag;->SEARCH:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v6}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v6

    sput-boolean v6, Lcom/google/android/music/store/SearchHandler;->LOGV:Z

    .line 80
    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "(SELECT count(distinct(SongId)) FROM MUSIC WHERE (AlbumArtistId=search_artist_id OR ArtistId=search_artist_id))"

    aput-object v8, v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(distinct(SongId)) FROM MUSIC WHERE (AlbumArtistId=search_artist_id OR ArtistId=search_artist_id) AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v10}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(distinct(SongId)) FROM MUSIC WHERE (AlbumArtistId=search_artist_id OR ArtistId=search_artist_id) AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v9}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(distinct(SongId)) FROM MUSIC WHERE (AlbumArtistId=search_artist_id OR ArtistId=search_artist_id) AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v11}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v11

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(distinct(SongId)) FROM MUSIC WHERE (AlbumArtistId=search_artist_id OR ArtistId=search_artist_id) AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v12}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v12

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(distinct(SongId)) FROM MUSIC WHERE (AlbumArtistId=search_artist_id OR ArtistId=search_artist_id) AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v13}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v13

    sput-object v6, Lcom/google/android/music/store/SearchHandler;->SEARCH_SONG_COUNT_EXPRESSIONS:[Ljava/lang/String;

    .line 94
    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "(SELECT count(distinct(AlbumId)) FROM MUSIC WHERE (AlbumArtistId=search_artist_id OR ArtistId=search_artist_id))"

    aput-object v8, v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(distinct(AlbumId)) FROM MUSIC WHERE (AlbumArtistId=search_artist_id OR ArtistId=search_artist_id) AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v10}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(distinct(AlbumId)) FROM MUSIC WHERE (AlbumArtistId=search_artist_id OR ArtistId=search_artist_id) AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v9}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(distinct(AlbumId)) FROM MUSIC WHERE (AlbumArtistId=search_artist_id OR ArtistId=search_artist_id) AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v11}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v11

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(distinct(AlbumId)) FROM MUSIC WHERE (AlbumArtistId=search_artist_id OR ArtistId=search_artist_id) AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v12}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v12

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(distinct(AlbumId)) FROM MUSIC WHERE (AlbumArtistId=search_artist_id OR ArtistId=search_artist_id) AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v13}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v13

    sput-object v6, Lcom/google/android/music/store/SearchHandler;->SEARCH_ALBUM_COUNT_EXPRESSIONS:[Ljava/lang/String;

    .line 107
    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "(SELECT count(1) FROM LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE ListId=LISTS.Id)"

    aput-object v8, v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(1) FROM LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE ListId=LISTS.Id AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v10}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(1) FROM LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE ListId=LISTS.Id AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v9}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(1) FROM LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE ListId=LISTS.Id AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v11}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v11

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(1) FROM LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE ListId=LISTS.Id AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v12}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v12

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(SELECT count(1) FROM LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE ListId=LISTS.Id AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v13}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v13

    sput-object v6, Lcom/google/android/music/store/SearchHandler;->LIST_ITEM_COUNT_EXPRESSIONS:[Ljava/lang/String;

    .line 141
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Name"

    invoke-static {v7}, Lcom/google/android/music/store/SearchHandler;->getLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ListType IN (0, 1, 71)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/google/android/music/store/SearchHandler;->PLAYLIST_SEARCH_WHERE_CLAUSE:Ljava/lang/String;

    .line 880
    sget-object v6, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "albumorfauxart"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 884
    .local v0, "baseAlbumOrFauxArtUri":Landroid/net/Uri;
    sget-object v6, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "largealbumart"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 888
    .local v2, "baseLargeAlbumArtUri":Landroid/net/Uri;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CASE WHEN ( AlbumArtLocation IS NOT NULL AND AlbumArtLocation LIKE \'mediastore:%\' ) OR EXISTS ( SELECT 1 FROM ARTWORK_CACHE AS m WHERE m.RemoteLocation = MUSIC.AlbumArtLocation AND m.LocalLocationStorageType <> 0 LIMIT 1 ) THEN \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/\' || "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "AlbumId"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " END"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 891
    .local v4, "largeAlbumArtUriIfLocalCopyExists":Ljava/lang/String;
    sget-object v6, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "playlistfauxart"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 895
    .local v3, "basePlaylistFauxArtUri":Landroid/net/Uri;
    sget-object v6, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "artistfauxart"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 900
    .local v1, "baseArtistFauxArtUri":Landroid/net/Uri;
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 901
    .local v5, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "searchType"

    const-string v7, "search_type"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 903
    invoke-static {v5}, Lcom/google/android/music/store/SearchHandler;->addNullPlaylistMappings(Ljava/util/HashMap;)V

    .line 905
    const-string v6, "_id"

    const-string v7, "search_artist_id"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    const-string v6, "searchName"

    const-string v7, "search_artist"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 907
    const-string v6, "searchSortName"

    const-string v7, "search_canonical_artist"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    const-string v6, "Artist"

    const-string v7, "search_artist"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 909
    const-string v6, "AlbumArtist"

    const-string v7, "search_artist"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    const-string v6, "AlbumArtistId"

    const-string v7, "search_artist_id"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    const-string v6, "Album"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    const-string v6, "duration"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    const-string v6, "year"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    const-string v6, "AlbumId"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    const-string v6, "SongId"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    const-string v6, "Vid"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    const-string v6, "suggest_text_1"

    const-string v7, "search_artist"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    const-string v6, "suggest_text_2"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 919
    const-string v6, "suggest_icon_1"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/\' || "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "search_artist_id"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    const-string v6, "suggest_icon_large"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 922
    const-string v6, "suggest_intent_data"

    const-string v7, "search_intent"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    const-string v6, "suggest_intent_data_id"

    const-string v7, "search_artist_id"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    const-string v6, "suggest_last_access_hint"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    const-string v6, "suggest_shortcut_id"

    const-string v7, "\'_-1\'"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    const-string v6, "hasRemote"

    const-string v7, "0"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    const-string v6, "StoreAlbumId"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 929
    const-string v6, "Nid"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    invoke-static {v5}, Lcom/google/android/music/store/SearchHandler;->addNullGenreMappings(Ljava/util/HashMap;)V

    .line 931
    invoke-static {v5}, Lcom/google/android/music/store/SearchHandler;->addNullRadioSearchMappings(Ljava/util/HashMap;)V

    .line 933
    invoke-static {v5}, Lcom/google/android/music/store/SearchHandler;->createMapsForFilters(Ljava/util/HashMap;)[Ljava/util/HashMap;

    move-result-object v6

    sput-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchArtistMaps:[Ljava/util/HashMap;

    .line 934
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchArtistMaps:[Ljava/util/HashMap;

    const-string v7, "itemCount"

    sget-object v8, Lcom/google/android/music/store/SearchHandler;->SEARCH_SONG_COUNT_EXPRESSIONS:[Ljava/lang/String;

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMappings([Ljava/util/HashMap;Ljava/lang/String;[Ljava/lang/String;)V

    .line 935
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchArtistMaps:[Ljava/util/HashMap;

    const-string v7, "albumCount"

    sget-object v8, Lcom/google/android/music/store/SearchHandler;->SEARCH_ALBUM_COUNT_EXPRESSIONS:[Ljava/lang/String;

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMappings([Ljava/util/HashMap;Ljava/lang/String;[Ljava/lang/String;)V

    .line 937
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    sput-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    .line 938
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "searchType"

    const-string v8, "\'3\'"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "_id"

    const-string v8, "AlbumId"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    invoke-static {v6}, Lcom/google/android/music/store/SearchHandler;->addNullPlaylistMappings(Ljava/util/HashMap;)V

    .line 943
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "searchName"

    const-string v8, "Album"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "searchSortName"

    const-string v8, "CanonicalAlbum"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "Artist"

    const-string v8, "Artist"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 946
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "AlbumArtist"

    const-string v8, "AlbumArtist"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 947
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "AlbumArtistId"

    const-string v8, "AlbumArtistId"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "Album"

    const-string v8, "Album"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "itemCount"

    const-string v8, "count(distinct songid)"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "albumCount"

    const-string v8, "\'\'"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "duration"

    const-string v8, "null"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "year"

    const-string v8, "null"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "AlbumId"

    const-string v8, "AlbumId"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "SongId"

    const-string v8, "null"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 955
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "Vid"

    const-string v8, "null"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 956
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "suggest_text_1"

    const-string v8, "Album"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 957
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "suggest_text_2"

    const-string v8, "AlbumArtist"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 958
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "suggest_icon_1"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/\' || "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "AlbumId"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "suggest_icon_large"

    invoke-static {v6, v7, v4}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "suggest_intent_data"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_ALBUM:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 964
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "suggest_intent_data_id"

    const-string v8, "AlbumId"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 965
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "suggest_last_access_hint"

    const-string v8, "null"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "suggest_shortcut_id"

    const-string v8, "\'_-1\'"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "hasLocal"

    const-string v8, "(MAX(LocalCopyType)  IN (100,200,300))"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "hasRemote"

    const-string v8, "0"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "artworkUrl"

    const-string v8, "null"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "ArtistMetajamId"

    const-string v8, "max(ArtistMetajamId)"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "StoreAlbumId"

    const-string v8, "max(StoreAlbumId)"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 976
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    const-string v7, "Nid"

    const-string v8, "null"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    invoke-static {v6}, Lcom/google/android/music/store/SearchHandler;->addNullGenreMappings(Ljava/util/HashMap;)V

    .line 978
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    invoke-static {v6}, Lcom/google/android/music/store/SearchHandler;->addNullRadioSearchMappings(Ljava/util/HashMap;)V

    .line 980
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    sput-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    .line 981
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "searchType"

    const-string v8, "\'5\'"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 984
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    invoke-static {v6}, Lcom/google/android/music/store/SearchHandler;->addNullPlaylistMappings(Ljava/util/HashMap;)V

    .line 986
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "_id"

    const-string v8, "Id"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 987
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "searchName"

    const-string v8, "Title"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 988
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "searchSortName"

    const-string v8, "CanonicalName"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "Artist"

    const-string v8, "Artist"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 990
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "AlbumArtist"

    const-string v8, "AlbumArtist"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 991
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "AlbumArtistId"

    const-string v8, "AlbumArtistId"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 993
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "Album"

    const-string v8, "Album"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "itemCount"

    const-string v8, "\'\'"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "albumCount"

    const-string v8, "\'\'"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "duration"

    const-string v8, "Duration"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "year"

    const-string v8, "Year"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "AlbumId"

    const-string v8, "AlbumId"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "SongId"

    const-string v8, "SongId"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1000
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "Vid"

    const-string v8, "Vid"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1001
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "suggest_text_1"

    const-string v8, "Title"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "suggest_text_2"

    const-string v8, "Artist"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "suggest_icon_1"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/\' || "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "AlbumId"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "suggest_icon_large"

    invoke-static {v6, v7, v4}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "suggest_intent_data"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_TRACK:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1009
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "suggest_intent_data_id"

    const-string v8, "Id"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "suggest_last_access_hint"

    const-string v8, "null"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "suggest_shortcut_id"

    const-string v8, "\'_-1\'"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const/4 v7, 0x0

    invoke-static {v6, v7, v10}, Lcom/google/android/music/store/MusicContentProvider;->addCategoryMappings(Ljava/util/HashMap;ZZ)V

    .line 1014
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "artworkUrl"

    const-string v8, "null"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1015
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "ArtistMetajamId"

    const-string v8, "max(ArtistMetajamId)"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "StoreAlbumId"

    const-string v8, "max(StoreAlbumId)"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1019
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    const-string v7, "Nid"

    const-string v8, "max(Nid)"

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    invoke-static {v6}, Lcom/google/android/music/store/SearchHandler;->addNullGenreMappings(Ljava/util/HashMap;)V

    .line 1022
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    invoke-static {v6}, Lcom/google/android/music/store/SearchHandler;->addNullRadioSearchMappings(Ljava/util/HashMap;)V

    .line 1024
    new-instance v5, Ljava/util/HashMap;

    .end local v5    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1025
    .restart local v5    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "searchType"

    const-string v7, "\'4\'"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    const-string v6, "ListType"

    const-string v7, "ListType"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    const-string v6, "Name"

    const-string v7, "Name"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1029
    const-string v6, "ShareToken"

    const-string v7, "ShareToken"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    const-string v6, "OwnerName"

    const-string v7, "OwnerName"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1031
    const-string v6, "Description"

    const-string v7, "Description"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    const-string v6, "OwnerProfilePhotoUrl"

    const-string v7, "OwnerProfilePhotoUrl"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    const-string v6, "_id"

    const-string v7, "LISTS.Id"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1037
    const-string v6, "searchName"

    const-string v7, "Name"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1039
    const-string v6, "searchSortName"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    const-string v6, "Artist"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1041
    const-string v6, "AlbumArtist"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1042
    const-string v6, "AlbumArtistId"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1043
    const-string v6, "Album"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1044
    const-string v6, "albumCount"

    const-string v7, "\'\'"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1045
    const-string v6, "duration"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    const-string v6, "year"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1047
    const-string v6, "AlbumId"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    const-string v6, "SongId"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    const-string v6, "Vid"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    const-string v6, "suggest_text_1"

    const-string v7, "Name"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1051
    const-string v6, "suggest_text_2"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    const-string v6, "suggest_icon_1"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/\' || "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "LISTS.Id"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    const-string v6, "suggest_icon_large"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    const-string v6, "suggest_intent_data"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_PLAYLIST:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1057
    const-string v6, "suggest_intent_data_id"

    const-string v7, "LISTS.Id"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    const-string v6, "suggest_last_access_hint"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    const-string v6, "suggest_shortcut_id"

    const-string v7, "\'_-1\'"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1061
    const-string v6, "hasLocal"

    const-string v7, "EXISTS (SELECT 1 FROM LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE (ListId=LISTS.Id) AND LocalCopyType IN (100,200,300) LIMIT 1)"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1063
    const-string v6, "hasRemote"

    const-string v7, "0"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1064
    const-string v6, "artworkUrl"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1065
    const-string v6, "ArtistMetajamId"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    const-string v6, "StoreAlbumId"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    const-string v6, "Nid"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1068
    invoke-static {v5}, Lcom/google/android/music/store/SearchHandler;->addNullGenreMappings(Ljava/util/HashMap;)V

    .line 1069
    invoke-static {v5}, Lcom/google/android/music/store/SearchHandler;->addNullRadioSearchMappings(Ljava/util/HashMap;)V

    .line 1071
    invoke-static {v5}, Lcom/google/android/music/store/SearchHandler;->createMapsForFilters(Ljava/util/HashMap;)[Ljava/util/HashMap;

    move-result-object v6

    sput-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchPlaylistMaps:[Ljava/util/HashMap;

    .line 1072
    sget-object v6, Lcom/google/android/music/store/SearchHandler;->sSearchPlaylistMaps:[Ljava/util/HashMap;

    const-string v7, "itemCount"

    sget-object v8, Lcom/google/android/music/store/SearchHandler;->LIST_ITEM_COUNT_EXPRESSIONS:[Ljava/lang/String;

    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/SearchHandler;->addMappings([Ljava/util/HashMap;Ljava/lang/String;[Ljava/lang/String;)V

    .line 1073
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/store/Store;Ljava/util/concurrent/ThreadPoolExecutor;Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ILcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;ZZ)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "store"    # Lcom/google/android/music/store/Store;
    .param p3, "executor"    # Ljava/util/concurrent/ThreadPoolExecutor;
    .param p4, "qb"    # Landroid/database/sqlite/SQLiteQueryBuilder;
    .param p5, "uri"    # Landroid/net/Uri;
    .param p6, "searchQuery"    # Ljava/lang/String;
    .param p7, "limit"    # Ljava/lang/String;
    .param p8, "projection"    # [Ljava/lang/String;
    .param p9, "filterIndex"    # I
    .param p10, "cancellationSignal"    # Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;
    .param p11, "isGlobalSearch"    # Z
    .param p12, "nautilusEnabled"    # Z

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    iput-object p1, p0, Lcom/google/android/music/store/SearchHandler;->mContext:Landroid/content/Context;

    .line 164
    iput-object p2, p0, Lcom/google/android/music/store/SearchHandler;->mStore:Lcom/google/android/music/store/Store;

    .line 165
    iput-object p3, p0, Lcom/google/android/music/store/SearchHandler;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 166
    iput-object p4, p0, Lcom/google/android/music/store/SearchHandler;->mQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;

    .line 167
    iput-object p5, p0, Lcom/google/android/music/store/SearchHandler;->mUri:Landroid/net/Uri;

    .line 168
    iput-object p6, p0, Lcom/google/android/music/store/SearchHandler;->mSearchQuery:Ljava/lang/String;

    .line 169
    iput-object p8, p0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    .line 170
    iput p9, p0, Lcom/google/android/music/store/SearchHandler;->mFilterIndex:I

    .line 171
    iput-object p10, p0, Lcom/google/android/music/store/SearchHandler;->mCancellationSignal:Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;

    .line 172
    iput-boolean p11, p0, Lcom/google/android/music/store/SearchHandler;->mIsGlobalSearch:Z

    .line 173
    iput-boolean p12, p0, Lcom/google/android/music/store/SearchHandler;->mIsNautilusEnabled:Z

    .line 174
    iget-object v0, p0, Lcom/google/android/music/store/SearchHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_nautilus_search_num_results"

    const/16 v2, 0xa

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/SearchHandler;->mNumSearchResults:I

    .line 180
    if-eqz p7, :cond_0

    invoke-virtual {p7}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 181
    :cond_0
    const-string p7, "1000"

    .line 183
    :cond_1
    iput-object p7, p0, Lcom/google/android/music/store/SearchHandler;->mLimit:Ljava/lang/String;

    .line 184
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/store/SearchHandler;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/store/SearchHandler;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/music/store/SearchHandler;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 53
    sget-boolean v0, Lcom/google/android/music/store/SearchHandler;->LOGV:Z

    return v0
.end method

.method private static addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<-TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 601
    .local p0, "collection":Ljava/util/Collection;, "Ljava/util/Collection<-TT;>;"
    .local p1, "itemToAdd":Ljava/lang/Object;, "TT;"
    if-eqz p1, :cond_0

    .line 602
    invoke-interface {p0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 604
    :cond_0
    return-void
.end method

.method private static addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "sql"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 820
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    return-void
.end method

.method private static addMappings([Ljava/util/HashMap;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "sqls"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 825
    .local p0, "maps":[Ljava/util/HashMap;, "[Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 826
    aget-object v1, p0, v0

    aget-object v2, p2, v0

    invoke-static {v1, p1, v2}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 828
    :cond_0
    return-void
.end method

.method private static addNullGenreMappings(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 835
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "name"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    const-string v0, "genreArtUris"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    const-string v0, "genreServerId"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    const-string v0, "parentGenreId"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    const-string v0, "subgenreCount"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 840
    return-void
.end method

.method private static addNullPlaylistMappings(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 843
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "ListType"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 844
    const-string v0, "Name"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    const-string v0, "ShareToken"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    const-string v0, "OwnerName"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    const-string v0, "Description"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    const-string v0, "OwnerProfilePhotoUrl"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    return-void
.end method

.method private static addNullRadioSearchMappings(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 852
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "radio_name"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    const-string v0, "radio_description"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    const-string v0, "radio_seed_source_id"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    const-string v0, "radio_seed_source_type"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/SearchHandler;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    return-void
.end method

.method private static createMapsForFilters(Ljava/util/HashMap;)[Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "baseMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x6

    .line 864
    new-array v1, v3, [Ljava/util/HashMap;

    .line 865
    .local v1, "maps":[Ljava/util/HashMap;, "[Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v2, 0x0

    aput-object p0, v1, v2

    .line 866
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 867
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, p0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    aput-object v2, v1, v0

    .line 866
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 869
    :cond_0
    return-object v1
.end method

.method private dedupeNautilusPlaylists(Ljava/util/List;Landroid/database/Cursor;)Landroid/database/MatrixCursor;
    .locals 11
    .param p2, "playlistCursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[",
            "Ljava/lang/Object;",
            ">;",
            "Landroid/database/Cursor;",
            ")",
            "Landroid/database/MatrixCursor;"
        }
    .end annotation

    .prologue
    .local p1, "nautilusPlaylistRows":Ljava/util/List;, "Ljava/util/List<[Ljava/lang/Object;>;"
    const/4 v10, -0x1

    .line 615
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 616
    .local v7, "shareTokens":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz p2, :cond_1

    .line 618
    :try_start_0
    const-string v8, "ShareToken"

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 620
    .local v6, "shareTokenIndex":I
    if-eq v6, v10, :cond_0

    .line 621
    const/4 v8, -0x1

    invoke-interface {p2, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 622
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 623
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 628
    .end local v6    # "shareTokenIndex":I
    :catchall_0
    move-exception v8

    invoke-interface {p2, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    throw v8

    .restart local v6    # "shareTokenIndex":I
    :cond_0
    invoke-interface {p2, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 632
    .end local v6    # "shareTokenIndex":I
    :cond_1
    const/4 v3, -0x1

    .line 633
    .local v3, "nautilusShareTokenIndex":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v8, p0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    array-length v8, v8

    if-ge v0, v8, :cond_2

    .line 634
    iget-object v8, p0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    aget-object v8, v8, v0

    const-string v9, "ShareToken"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 635
    move v3, v0

    .line 640
    :cond_2
    new-instance v2, Landroid/database/MatrixCursor;

    iget-object v8, p0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    invoke-direct {v2, v8}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 642
    .local v2, "nautilusPlaylistCursor":Landroid/database/MatrixCursor;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/Object;

    .line 643
    .local v5, "row":[Ljava/lang/Object;
    if-eq v3, v10, :cond_5

    .line 644
    aget-object v4, v5, v3

    check-cast v4, Ljava/lang/String;

    .line 645
    .local v4, "nautuilusShareToken":Ljava/lang/String;
    invoke-virtual {v7, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 646
    invoke-virtual {v2, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_2

    .line 633
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "nautilusPlaylistCursor":Landroid/database/MatrixCursor;
    .end local v4    # "nautuilusShareToken":Ljava/lang/String;
    .end local v5    # "row":[Ljava/lang/Object;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 649
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "nautilusPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v5    # "row":[Ljava/lang/Object;
    :cond_5
    invoke-virtual {v2, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_2

    .line 654
    .end local v5    # "row":[Ljava/lang/Object;
    :cond_6
    return-object v2
.end method

.method private doLocalAlbumQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 13
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "canonicalSearchValue"    # Ljava/lang/String;
    .param p3, "andFilter"    # Ljava/lang/String;
    .param p4, "albumExclude"    # Ljava/lang/String;
    .param p5, "canonicalSearch"    # Z

    .prologue
    .line 734
    iget-object v1, p0, Lcom/google/android/music/store/SearchHandler;->mQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;

    const-string v2, "MUSIC"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 735
    iget-object v1, p0, Lcom/google/android/music/store/SearchHandler;->mQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;

    sget-object v2, Lcom/google/android/music/store/SearchHandler;->sSearchAlbumMap:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 736
    if-eqz p5, :cond_1

    .line 737
    const-string v12, "CanonicalAlbum"

    .line 746
    .local v12, "searchBy":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/store/SearchHandler;->mQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;

    iget-object v3, p0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " IN (SELECT "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " FROM "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "MUSIC"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " WHERE "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v12}, Lcom/google/android/music/store/SearchHandler;->getLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " GROUP BY "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ")"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x1

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v5, v2

    const-string v6, "AlbumId"

    const/4 v7, 0x0

    iget-boolean v2, p0, Lcom/google/android/music/store/SearchHandler;->mIsGlobalSearch:Z

    if-eqz v2, :cond_0

    const/4 v8, 0x0

    :goto_0
    iget-object v9, p0, Lcom/google/android/music/store/SearchHandler;->mLimit:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/music/store/SearchHandler;->mCancellationSignal:Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;

    move-object v2, p1

    invoke-static/range {v1 .. v10}, Lcom/google/android/music/utils/PostFroyoUtils$SQLiteDatabaseComp;->query(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v11

    .line 768
    .local v11, "albumCursor":Landroid/database/Cursor;
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    .line 769
    return-object v11

    .line 746
    .end local v11    # "albumCursor":Landroid/database/Cursor;
    :cond_0
    const-string v8, "CanonicalAlbum"

    goto :goto_0

    .line 759
    .end local v12    # "searchBy":Ljava/lang/String;
    :cond_1
    const-string v12, "Album"

    .line 760
    .restart local v12    # "searchBy":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/store/SearchHandler;->mQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;

    iget-object v3, p0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v12}, Lcom/google/android/music/store/SearchHandler;->getLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x1

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v5, v2

    const-string v6, "AlbumId"

    const/4 v7, 0x0

    iget-boolean v2, p0, Lcom/google/android/music/store/SearchHandler;->mIsGlobalSearch:Z

    if-eqz v2, :cond_2

    const/4 v8, 0x0

    :goto_2
    iget-object v9, p0, Lcom/google/android/music/store/SearchHandler;->mLimit:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/music/store/SearchHandler;->mCancellationSignal:Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;

    move-object v2, p1

    invoke-static/range {v1 .. v10}, Lcom/google/android/music/utils/PostFroyoUtils$SQLiteDatabaseComp;->query(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v11

    .restart local v11    # "albumCursor":Landroid/database/Cursor;
    goto :goto_1

    .end local v11    # "albumCursor":Landroid/database/Cursor;
    :cond_2
    const-string v8, "CanonicalAlbum"

    goto :goto_2
.end method

.method private doLocalArtistQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "canonicalSearchValue"    # Ljava/lang/String;
    .param p3, "andFilter"    # Ljava/lang/String;
    .param p4, "artistExclude"    # Ljava/lang/String;
    .param p5, "artistMetajamExclude"    # Ljava/lang/String;
    .param p6, "canonicalSearch"    # Z

    .prologue
    const/4 v7, 0x1

    .line 660
    iget-object v4, p0, Lcom/google/android/music/store/SearchHandler;->mQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;

    const-string v5, "MUSIC"

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 661
    if-eqz p6, :cond_0

    const-string v3, "CanonicalAlbumArtist"

    .line 664
    .local v3, "searchBy":Ljava/lang/String;
    :goto_0
    if-eqz p6, :cond_1

    const-string v2, "CanonicalArtist"

    .line 666
    .local v2, "artistSearchBy":Ljava/lang/String;
    :goto_1
    iget-object v4, p0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    sget-object v5, Lcom/google/android/music/store/SearchHandler;->sSearchArtistMaps:[Ljava/util/HashMap;

    iget v6, p0, Lcom/google/android/music/store/SearchHandler;->mFilterIndex:I

    aget-object v5, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/music/utils/DbUtils;->formatProjection([Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 668
    .local v1, "artistProj":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "SELECT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_type"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "AlbumArtistId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_artist_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "AlbumArtist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_artist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "CanonicalAlbumArtist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_canonical_artist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_ALBUM_ARTIST:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_intent"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "(MAX(LocalCopyType)  IN (100,200,300))"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "hasLocal"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "max("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ArtistArtLocation"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "artworkUrl"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "max("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ArtistMetajamId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ArtistMetajamId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MUSIC"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Lcom/google/android/music/store/SearchHandler;->getLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " GROUP BY "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "AlbumArtistId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " UNION "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "SELECT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_type"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ArtistId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_artist_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Artist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_artist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "CanonicalArtist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_canonical_artist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_ARTIST:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_intent"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "(MAX(LocalCopyType)  IN (100,200,300))"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "hasLocal"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "max("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ArtistArtLocation"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "artworkUrl"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "max("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ArtistMetajamId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ArtistMetajamId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MUSIC"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Lcom/google/android/music/store/SearchHandler;->getLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ArtistId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "<>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "AlbumArtistId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND NOT EXISTS( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "SELECT 1 FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MUSIC"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " as m "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "WHERE m."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "AlbumArtistId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MUSIC"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ArtistId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " GROUP BY "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "CanonicalArtist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v4, p0, Lcom/google/android/music/store/SearchHandler;->mIsGlobalSearch:Z

    if-eqz v4, :cond_2

    const-string v4, ""

    :goto_2
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " LIMIT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/store/SearchHandler;->mLimit:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    iget-object v6, p0, Lcom/google/android/music/store/SearchHandler;->mCancellationSignal:Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;

    invoke-static {p1, v4, v5, v6}, Lcom/google/android/music/utils/PostFroyoUtils$SQLiteDatabaseComp;->rawQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v0

    .line 727
    .local v0, "artistCursor":Landroid/database/Cursor;
    return-object v0

    .line 661
    .end local v0    # "artistCursor":Landroid/database/Cursor;
    .end local v1    # "artistProj":Ljava/lang/String;
    .end local v2    # "artistSearchBy":Ljava/lang/String;
    .end local v3    # "searchBy":Ljava/lang/String;
    :cond_0
    const-string v3, "AlbumArtist"

    goto/16 :goto_0

    .line 664
    .restart local v3    # "searchBy":Ljava/lang/String;
    :cond_1
    const-string v2, "Artist"

    goto/16 :goto_1

    .line 668
    .restart local v1    # "artistProj":Ljava/lang/String;
    .restart local v2    # "artistSearchBy":Ljava/lang/String;
    :cond_2
    const-string v4, " ORDER BY search_canonical_artist"

    goto :goto_2
.end method

.method private doLocalTrackQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 13
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "canonicalSearchValue"    # Ljava/lang/String;
    .param p3, "andFilter"    # Ljava/lang/String;
    .param p4, "trackExclude"    # Ljava/lang/String;
    .param p5, "canonicalSearch"    # Z

    .prologue
    .line 792
    iget-object v1, p0, Lcom/google/android/music/store/SearchHandler;->mQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;

    const-string v2, "MUSIC"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 793
    iget-object v1, p0, Lcom/google/android/music/store/SearchHandler;->mQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;

    sget-object v2, Lcom/google/android/music/store/SearchHandler;->sSearchTrackMap:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 794
    if-eqz p5, :cond_0

    const-string v11, "CanonicalName"

    .line 795
    .local v11, "searchBy":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/store/SearchHandler;->mQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;

    iget-object v3, p0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v11}, Lcom/google/android/music/store/SearchHandler;->getLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x1

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v5, v2

    const-string v6, "CanonicalName,SongId"

    const/4 v7, 0x0

    iget-boolean v2, p0, Lcom/google/android/music/store/SearchHandler;->mIsGlobalSearch:Z

    if-eqz v2, :cond_1

    const/4 v8, 0x0

    :goto_1
    iget-object v9, p0, Lcom/google/android/music/store/SearchHandler;->mLimit:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/music/store/SearchHandler;->mCancellationSignal:Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;

    move-object v2, p1

    invoke-static/range {v1 .. v10}, Lcom/google/android/music/utils/PostFroyoUtils$SQLiteDatabaseComp;->query(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v12

    .line 809
    .local v12, "trackCursor":Landroid/database/Cursor;
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    .line 810
    return-object v12

    .line 794
    .end local v11    # "searchBy":Ljava/lang/String;
    .end local v12    # "trackCursor":Landroid/database/Cursor;
    :cond_0
    const-string v11, "Title"

    goto :goto_0

    .line 795
    .restart local v11    # "searchBy":Ljava/lang/String;
    :cond_1
    const-string v8, "CanonicalName"

    goto :goto_1
.end method

.method private static getLikeClause(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "searchBy"    # Ljava/lang/String;

    .prologue
    .line 873
    const-string v0, " (%1$s LIKE ?1||\'%%\' ESCAPE \'!\' OR %1$s LIKE \'%% \'||?1||\'%%\' ESCAPE \'!\') "

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getServerResults(Ljava/lang/String;I)Ljava/util/concurrent/Future;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "numSearchResults"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/android/music/cloudclient/SearchClientResponseJson;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    iget-object v1, p0, Lcom/google/android/music/store/SearchHandler;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v2, Lcom/google/android/music/store/SearchHandler$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/music/store/SearchHandler$1;-><init>(Lcom/google/android/music/store/SearchHandler;Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ThreadPoolExecutor;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 217
    .local v0, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/music/cloudclient/SearchClientResponseJson;>;"
    return-object v0
.end method


# virtual methods
.method doLocalPlaylistQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "searchValue"    # Ljava/lang/String;
    .param p3, "andFilter"    # Ljava/lang/String;

    .prologue
    .line 776
    iget-object v0, p0, Lcom/google/android/music/store/SearchHandler;->mQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;

    const-string v1, "LISTS LEFT OUTER JOIN LISTITEMS ON LISTS.Id=LISTITEMS.ListId LEFT OUTER JOIN MUSIC ON LISTITEMS.MusicId=MUSIC.Id"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 777
    iget-object v0, p0, Lcom/google/android/music/store/SearchHandler;->mQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;

    sget-object v1, Lcom/google/android/music/store/SearchHandler;->sSearchPlaylistMaps:[Ljava/util/HashMap;

    iget v2, p0, Lcom/google/android/music/store/SearchHandler;->mFilterIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 778
    iget-object v0, p0, Lcom/google/android/music/store/SearchHandler;->mQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;

    iget-object v2, p0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/google/android/music/store/SearchHandler;->PLAYLIST_SEARCH_WHERE_CLAUSE:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v4, v1

    const-string v5, "LISTS.Id"

    const-string v6, "count(LISTITEMS.ListId) > 0"

    iget-boolean v1, p0, Lcom/google/android/music/store/SearchHandler;->mIsGlobalSearch:Z

    if-eqz v1, :cond_0

    const/4 v7, 0x0

    :goto_0
    iget-object v8, p0, Lcom/google/android/music/store/SearchHandler;->mLimit:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/music/store/SearchHandler;->mCancellationSignal:Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;

    move-object v1, p1

    invoke-static/range {v0 .. v9}, Lcom/google/android/music/utils/PostFroyoUtils$SQLiteDatabaseComp;->query(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v10

    .line 786
    .local v10, "playlistCursor":Landroid/database/Cursor;
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    .line 787
    return-object v10

    .line 778
    .end local v10    # "playlistCursor":Landroid/database/Cursor;
    :cond_0
    const-string v7, "Name"

    goto :goto_0
.end method

.method public performQuery()Landroid/database/Cursor;
    .locals 87

    .prologue
    .line 249
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    array-length v4, v4

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mSearchQuery:Ljava/lang/String;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mSearchQuery:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v11, 0x2

    if-ge v4, v11, :cond_2

    .line 251
    :cond_0
    const/16 v32, 0x0

    .line 596
    :cond_1
    :goto_0
    return-object v32

    .line 254
    :cond_2
    new-instance v67, Lcom/google/android/music/store/TagNormalizer;

    invoke-direct/range {v67 .. v67}, Lcom/google/android/music/store/TagNormalizer;-><init>()V

    .line 257
    .local v67, "normalizer":Lcom/google/android/music/store/TagNormalizer;
    const/16 v57, 0x0

    .line 258
    .local v57, "navArtistCursor":Landroid/database/MatrixCursor;
    const/16 v55, 0x0

    .line 259
    .local v55, "navAlbumCursor":Landroid/database/MatrixCursor;
    const/16 v65, 0x0

    .line 260
    .local v65, "navTrackCursor":Landroid/database/MatrixCursor;
    const/16 v61, 0x0

    .line 261
    .local v61, "navPlaylistCursor":Landroid/database/MatrixCursor;
    const/16 v59, 0x0

    .line 262
    .local v59, "navGenreCursor":Landroid/database/MatrixCursor;
    const/16 v63, 0x0

    .line 264
    .local v63, "navRadioCursor":Landroid/database/MatrixCursor;
    const/16 v45, 0x0

    .line 265
    .local v45, "nautilusArtistCursor":Landroid/database/MatrixCursor;
    const/16 v43, 0x0

    .line 266
    .local v43, "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    const/16 v53, 0x0

    .line 267
    .local v53, "nautilusTrackCursor":Landroid/database/MatrixCursor;
    const/16 v49, 0x0

    .line 268
    .local v49, "nautilusPlaylistCursor":Landroid/database/MatrixCursor;
    const/16 v47, 0x0

    .line 269
    .local v47, "nautilusGenreCursor":Landroid/database/MatrixCursor;
    const/16 v51, 0x0

    .line 271
    .local v51, "nautilusRadioCursor":Landroid/database/MatrixCursor;
    const/16 v28, 0x0

    .line 272
    .local v28, "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/16 v30, 0x0

    .line 273
    .local v30, "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v24, 0x0

    .line 274
    .local v24, "albumExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/16 v83, 0x0

    .line 278
    .local v83, "trackExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v50, Ljava/util/LinkedList;

    invoke-direct/range {v50 .. v50}, Ljava/util/LinkedList;-><init>()V

    .line 280
    .local v50, "nautilusPlaylistRows":Ljava/util/List;, "Ljava/util/List<[Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mUri:Landroid/net/Uri;

    const-string v11, "type"

    invoke-virtual {v4, v11}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v86

    .line 282
    .local v86, "typeParam":Ljava/lang/String;
    const-string v4, "bestmatch"

    move-object/from16 v0, v86

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    .line 286
    .local v39, "isBestMatchSearch":Z
    if-eqz v86, :cond_3

    if-eqz v39, :cond_b

    :cond_3
    const/16 v74, 0x1

    .line 288
    .local v74, "shouldDoAll":Z
    :goto_1
    if-nez v74, :cond_4

    const-string v4, "artist"

    move-object/from16 v0, v86

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    :cond_4
    const/16 v75, 0x1

    .line 290
    .local v75, "shouldDoArtistSearch":Z
    :goto_2
    if-nez v74, :cond_5

    const-string v4, "album"

    move-object/from16 v0, v86

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    :cond_5
    const/16 v73, 0x1

    .line 292
    .local v73, "shouldDoAlbumSearch":Z
    :goto_3
    if-nez v74, :cond_6

    const-string v4, "track"

    move-object/from16 v0, v86

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    :cond_6
    const/16 v79, 0x1

    .line 294
    .local v79, "shouldDoTrackSearch":Z
    :goto_4
    if-nez v74, :cond_7

    const-string v4, "playlist"

    move-object/from16 v0, v86

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    :cond_7
    const/16 v77, 0x1

    .line 296
    .local v77, "shouldDoPlaylistSearch":Z
    :goto_5
    if-nez v74, :cond_8

    const-string v4, "genre"

    move-object/from16 v0, v86

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    :cond_8
    const/16 v76, 0x1

    .line 300
    .local v76, "shouldDoGenreSearch":Z
    :goto_6
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isRadioSearchEnabled()Z

    move-result v4

    if-nez v4, :cond_11

    .line 301
    const/16 v78, 0x0

    .line 308
    .local v78, "shouldDoRadioSearch":Z
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/music/store/SearchHandler;->mIsNautilusEnabled:Z

    if-eqz v4, :cond_27

    .line 310
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mSearchQuery:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/music/store/SearchHandler;->mNumSearchResults:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v11}, Lcom/google/android/music/store/SearchHandler;->getServerResults(Ljava/lang/String;I)Ljava/util/concurrent/Future;

    move-result-object v36

    .line 313
    .local v36, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/music/cloudclient/SearchClientResponseJson;>;"
    const-wide/16 v12, 0x1e

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v36

    invoke-interface {v0, v12, v13, v4}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v71

    check-cast v71, Lcom/google/android/music/cloudclient/SearchClientResponseJson;

    .line 316
    .local v71, "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    if-eqz v71, :cond_26

    move-object/from16 v0, v71

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson;->mEntries:Ljava/util/List;

    if-eqz v4, :cond_26

    .line 317
    sget-boolean v4, Lcom/google/android/music/store/SearchHandler;->LOGV:Z

    if-eqz v4, :cond_9

    .line 318
    const-string v4, "Search"

    const-string v11, "Result count: %d"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, v71

    iget-object v14, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson;->mEntries:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    :cond_9
    if-eqz v39, :cond_1d

    .line 322
    const/16 v41, 0x0

    .line 323
    .local v41, "maxScore":F
    const/16 v42, 0x0

    .line 324
    .local v42, "maxScoreEntry":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
    move-object/from16 v0, v71

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson;->mEntries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    .local v38, "i$":Ljava/util/Iterator;
    :cond_a
    :goto_8
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_14

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;

    .line 325
    .local v35, "entry":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
    invoke-virtual/range {v35 .. v35}, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->getType()Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    move-result-object v85

    .line 326
    .local v85, "type":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;
    move-object/from16 v0, v35

    iget-boolean v0, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mNavigationalResult:Z

    move/from16 v40, v0

    .line 327
    .local v40, "isNavigational":Z
    if-eqz v40, :cond_a

    .line 328
    move-object/from16 v0, v35

    iget v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mNavigationalConfidence:F

    cmpl-float v4, v4, v41

    if-lez v4, :cond_a

    .line 329
    move-object/from16 v0, v35

    iget v0, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mNavigationalConfidence:F

    move/from16 v41, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    move-object/from16 v42, v35

    goto :goto_8

    .line 286
    .end local v35    # "entry":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
    .end local v36    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/music/cloudclient/SearchClientResponseJson;>;"
    .end local v38    # "i$":Ljava/util/Iterator;
    .end local v40    # "isNavigational":Z
    .end local v41    # "maxScore":F
    .end local v42    # "maxScoreEntry":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
    .end local v71    # "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    .end local v73    # "shouldDoAlbumSearch":Z
    .end local v74    # "shouldDoAll":Z
    .end local v75    # "shouldDoArtistSearch":Z
    .end local v76    # "shouldDoGenreSearch":Z
    .end local v77    # "shouldDoPlaylistSearch":Z
    .end local v78    # "shouldDoRadioSearch":Z
    .end local v79    # "shouldDoTrackSearch":Z
    .end local v85    # "type":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;
    :cond_b
    const/16 v74, 0x0

    goto/16 :goto_1

    .line 288
    .restart local v74    # "shouldDoAll":Z
    :cond_c
    const/16 v75, 0x0

    goto/16 :goto_2

    .line 290
    .restart local v75    # "shouldDoArtistSearch":Z
    :cond_d
    const/16 v73, 0x0

    goto/16 :goto_3

    .line 292
    .restart local v73    # "shouldDoAlbumSearch":Z
    :cond_e
    const/16 v79, 0x0

    goto/16 :goto_4

    .line 294
    .restart local v79    # "shouldDoTrackSearch":Z
    :cond_f
    const/16 v77, 0x0

    goto/16 :goto_5

    .line 296
    .restart local v77    # "shouldDoPlaylistSearch":Z
    :cond_10
    const/16 v76, 0x0

    goto/16 :goto_6

    .line 303
    .restart local v76    # "shouldDoGenreSearch":Z
    :cond_11
    if-nez v74, :cond_12

    const-string v4, "radio"

    move-object/from16 v0, v86

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    :cond_12
    const/16 v78, 0x1

    .restart local v78    # "shouldDoRadioSearch":Z
    :goto_9
    goto/16 :goto_7

    .end local v78    # "shouldDoRadioSearch":Z
    :cond_13
    const/16 v78, 0x0

    goto :goto_9

    .line 334
    .restart local v36    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/music/cloudclient/SearchClientResponseJson;>;"
    .restart local v38    # "i$":Ljava/util/Iterator;
    .restart local v41    # "maxScore":F
    .restart local v42    # "maxScoreEntry":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
    .restart local v71    # "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    .restart local v78    # "shouldDoRadioSearch":Z
    :cond_14
    :try_start_1
    new-instance v32, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v32

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 335
    .local v32, "c":Landroid/database/MatrixCursor;
    if-eqz v42, :cond_1

    .line 336
    sget-object v4, Lcom/google/android/music/store/SearchHandler$2;->$SwitchMap$com$google$android$music$cloudclient$SearchClientResponseJson$SearchClientResponseEntry$Type:[I

    invoke-virtual/range {v42 .. v42}, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->getType()Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->ordinal()I

    move-result v11

    aget v4, v4, v11

    packed-switch v4, :pswitch_data_0

    goto/16 :goto_0

    .line 338
    :pswitch_0
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mArtist:Lcom/google/android/music/cloudclient/ArtistJson;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    invoke-static {v4, v11}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/ArtistJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 483
    .end local v32    # "c":Landroid/database/MatrixCursor;
    .end local v36    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/music/cloudclient/SearchClientResponseJson;>;"
    .end local v38    # "i$":Ljava/util/Iterator;
    .end local v41    # "maxScore":F
    .end local v42    # "maxScoreEntry":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
    .end local v71    # "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    :catch_0
    move-exception v34

    .line 484
    .local v34, "e":Ljava/lang/Exception;
    :goto_a
    const-string v4, "Search"

    invoke-virtual/range {v34 .. v34}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v34

    invoke-static {v4, v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 492
    .end local v34    # "e":Ljava/lang/Exception;
    :cond_15
    :goto_b
    if-eqz v28, :cond_16

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_28

    :cond_16
    const-string v8, ""

    .line 501
    .local v8, "artistExclude":Ljava/lang/String;
    :goto_c
    if-eqz v30, :cond_17

    invoke-interface/range {v30 .. v30}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_29

    :cond_17
    const-string v9, ""

    .line 508
    .local v9, "artistMetajamExclude":Ljava/lang/String;
    :goto_d
    if-eqz v24, :cond_18

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2a

    :cond_18
    const-string v15, ""

    .line 513
    .local v15, "albumExclude":Ljava/lang/String;
    :goto_e
    if-eqz v83, :cond_19

    invoke-interface/range {v83 .. v83}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2b

    :cond_19
    const-string v20, ""

    .line 524
    .local v20, "trackExclude":Ljava/lang/String;
    :goto_f
    const/4 v10, 0x1

    .line 525
    .local v10, "canonicalSearch":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/SearchHandler;->mSearchQuery:Ljava/lang/String;

    move-object/from16 v72, v0

    .line 526
    .local v72, "searchValue":Ljava/lang/String;
    move-object/from16 v0, v67

    move-object/from16 v1, v72

    invoke-virtual {v0, v1}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 527
    .local v6, "canonicalSearchValue":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v11, 0x2

    if-ge v4, v11, :cond_1a

    .line 530
    const/4 v10, 0x0

    .line 531
    move-object/from16 v6, v72

    .line 534
    :cond_1a
    const/16 v4, 0x21

    move-object/from16 v0, v72

    invoke-static {v0, v4}, Lcom/google/android/music/utils/DbUtils;->escapeForLikeOperator(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v72

    .line 535
    const/16 v4, 0x21

    invoke-static {v6, v4}, Lcom/google/android/music/utils/DbUtils;->escapeForLikeOperator(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v6

    .line 537
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 539
    .local v5, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_2
    const-string v7, ""

    .line 540
    .local v7, "andFilter":Ljava/lang/String;
    const-string v19, ""

    .line 541
    .local v19, "trackAndFilter":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/store/SearchHandler;->mFilterIndex:I

    if-eqz v4, :cond_1c

    .line 545
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " AND "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/music/store/SearchHandler;->mFilterIndex:I

    invoke-static {v11}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 550
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/store/SearchHandler;->mFilterIndex:I

    move/from16 v81, v0

    .line 551
    .local v81, "trackAndFilterIndex":I
    const/4 v4, 0x4

    move/from16 v0, v81

    if-ne v0, v4, :cond_2c

    .line 552
    const/16 v81, 0x1

    .line 556
    :cond_1b
    :goto_10
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " AND "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v81 .. v81}, Lcom/google/android/music/store/Filters;->getFilterWithoutVolumeCondition(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 560
    .end local v81    # "trackAndFilterIndex":I
    :cond_1c
    if-eqz v75, :cond_2d

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v10}, Lcom/google/android/music/store/SearchHandler;->doLocalArtistQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v27

    .line 563
    .local v27, "artistCursor":Landroid/database/Cursor;
    :goto_11
    if-eqz v73, :cond_2e

    move-object/from16 v11, p0

    move-object v12, v5

    move-object v13, v6

    move-object v14, v7

    move/from16 v16, v10

    invoke-direct/range {v11 .. v16}, Lcom/google/android/music/store/SearchHandler;->doLocalAlbumQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v23

    .line 566
    .local v23, "albumCursor":Landroid/database/Cursor;
    :goto_12
    if-eqz v77, :cond_2f

    move-object/from16 v0, p0

    move-object/from16 v1, v72

    invoke-virtual {v0, v5, v1, v7}, Lcom/google/android/music/store/SearchHandler;->doLocalPlaylistQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v69

    .line 568
    .local v69, "playlistCursor":Landroid/database/Cursor;
    :goto_13
    if-eqz v79, :cond_30

    move-object/from16 v16, p0

    move-object/from16 v17, v5

    move-object/from16 v18, v6

    move/from16 v21, v10

    invoke-direct/range {v16 .. v21}, Lcom/google/android/music/store/SearchHandler;->doLocalTrackQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v82

    .line 572
    .local v82, "trackCursor":Landroid/database/Cursor;
    :goto_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v5}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 575
    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v69

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/store/SearchHandler;->dedupeNautilusPlaylists(Ljava/util/List;Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v49

    .line 578
    new-instance v33, Ljava/util/ArrayList;

    const/16 v4, 0xa

    move-object/from16 v0, v33

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 579
    .local v33, "cursors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/database/Cursor;>;"
    move-object/from16 v0, v33

    move-object/from16 v1, v57

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 580
    move-object/from16 v0, v33

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 581
    move-object/from16 v0, v33

    move-object/from16 v1, v45

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 582
    move-object/from16 v0, v33

    move-object/from16 v1, v55

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 583
    move-object/from16 v0, v33

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 584
    move-object/from16 v0, v33

    move-object/from16 v1, v43

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 585
    move-object/from16 v0, v33

    move-object/from16 v1, v65

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 586
    move-object/from16 v0, v33

    move-object/from16 v1, v82

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 587
    move-object/from16 v0, v33

    move-object/from16 v1, v53

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 588
    move-object/from16 v0, v33

    move-object/from16 v1, v61

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 589
    move-object/from16 v0, v33

    move-object/from16 v1, v69

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 590
    move-object/from16 v0, v33

    move-object/from16 v1, v49

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 591
    move-object/from16 v0, v33

    move-object/from16 v1, v59

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 592
    move-object/from16 v0, v33

    move-object/from16 v1, v47

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 593
    move-object/from16 v0, v33

    move-object/from16 v1, v63

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 594
    move-object/from16 v0, v33

    move-object/from16 v1, v51

    invoke-static {v0, v1}, Lcom/google/android/music/store/SearchHandler;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 596
    new-instance v32, Lcom/google/android/music/store/CustomMergeCursor;

    const/4 v4, 0x0

    new-array v4, v4, [Landroid/database/Cursor;

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/database/Cursor;

    move-object/from16 v0, v32

    invoke-direct {v0, v4}, Lcom/google/android/music/store/CustomMergeCursor;-><init>([Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 342
    .end local v5    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v6    # "canonicalSearchValue":Ljava/lang/String;
    .end local v7    # "andFilter":Ljava/lang/String;
    .end local v8    # "artistExclude":Ljava/lang/String;
    .end local v9    # "artistMetajamExclude":Ljava/lang/String;
    .end local v10    # "canonicalSearch":Z
    .end local v15    # "albumExclude":Ljava/lang/String;
    .end local v19    # "trackAndFilter":Ljava/lang/String;
    .end local v20    # "trackExclude":Ljava/lang/String;
    .end local v23    # "albumCursor":Landroid/database/Cursor;
    .end local v27    # "artistCursor":Landroid/database/Cursor;
    .end local v33    # "cursors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/database/Cursor;>;"
    .end local v69    # "playlistCursor":Landroid/database/Cursor;
    .end local v72    # "searchValue":Ljava/lang/String;
    .end local v82    # "trackCursor":Landroid/database/Cursor;
    .restart local v32    # "c":Landroid/database/MatrixCursor;
    .restart local v36    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/music/cloudclient/SearchClientResponseJson;>;"
    .restart local v38    # "i$":Ljava/util/Iterator;
    .restart local v41    # "maxScore":F
    .restart local v42    # "maxScoreEntry":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
    .restart local v71    # "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    :pswitch_1
    :try_start_3
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mAlbum:Lcom/google/android/music/cloudclient/AlbumJson;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    invoke-static {v4, v11}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/AlbumJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 346
    :pswitch_2
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    invoke-static {v4, v11}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/TrackJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 350
    :pswitch_3
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mPlaylist:Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    invoke-static {v4, v11}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/sync/google/model/SyncablePlaylist;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 354
    :pswitch_4
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mGenre:Lcom/google/android/music/cloudclient/MusicGenreJson;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    invoke-static {v4, v11}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/MusicGenreJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 358
    :pswitch_5
    if-eqz v78, :cond_1

    .line 359
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mStation:Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    invoke-static {v4, v11}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/sync/google/model/SyncableRadioStation;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 368
    .end local v32    # "c":Landroid/database/MatrixCursor;
    .end local v38    # "i$":Ljava/util/Iterator;
    .end local v41    # "maxScore":F
    .end local v42    # "maxScoreEntry":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
    :cond_1d
    new-instance v58, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v58

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 369
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .local v58, "navArtistCursor":Landroid/database/MatrixCursor;
    :try_start_4
    new-instance v56, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v56

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 370
    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .local v56, "navAlbumCursor":Landroid/database/MatrixCursor;
    :try_start_5
    new-instance v66, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v66

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 371
    .end local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    .local v66, "navTrackCursor":Landroid/database/MatrixCursor;
    :try_start_6
    new-instance v62, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v62

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 372
    .end local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .local v62, "navPlaylistCursor":Landroid/database/MatrixCursor;
    :try_start_7
    new-instance v60, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v60

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 373
    .end local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    .local v60, "navGenreCursor":Landroid/database/MatrixCursor;
    :try_start_8
    new-instance v64, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v64

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    .line 375
    .end local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    .local v64, "navRadioCursor":Landroid/database/MatrixCursor;
    :try_start_9
    new-instance v46, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v46

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7

    .line 376
    .end local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .local v46, "nautilusArtistCursor":Landroid/database/MatrixCursor;
    :try_start_a
    new-instance v44, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v44

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_8

    .line 377
    .end local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .local v44, "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    :try_start_b
    new-instance v54, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v54

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_9

    .line 378
    .end local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .local v54, "nautilusTrackCursor":Landroid/database/MatrixCursor;
    :try_start_c
    new-instance v48, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v48

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_a

    .line 379
    .end local v47    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .local v48, "nautilusGenreCursor":Landroid/database/MatrixCursor;
    :try_start_d
    new-instance v52, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v52

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_b

    .line 381
    .end local v51    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .local v52, "nautilusRadioCursor":Landroid/database/MatrixCursor;
    :try_start_e
    new-instance v29, Ljava/util/LinkedList;

    invoke-direct/range {v29 .. v29}, Ljava/util/LinkedList;-><init>()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_c

    .line 382
    .end local v28    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local v29, "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :try_start_f
    new-instance v31, Ljava/util/LinkedList;

    invoke-direct/range {v31 .. v31}, Ljava/util/LinkedList;-><init>()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_d

    .line 383
    .end local v30    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v31, "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_10
    new-instance v25, Ljava/util/LinkedList;

    invoke-direct/range {v25 .. v25}, Ljava/util/LinkedList;-><init>()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_e

    .line 384
    .end local v24    # "albumExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local v25, "albumExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :try_start_11
    new-instance v84, Ljava/util/LinkedList;

    invoke-direct/range {v84 .. v84}, Ljava/util/LinkedList;-><init>()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_f

    .line 386
    .end local v83    # "trackExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local v84, "trackExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :try_start_12
    move-object/from16 v0, v71

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson;->mEntries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    .restart local v38    # "i$":Ljava/util/Iterator;
    :cond_1e
    :goto_15
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_25

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;

    .line 387
    .restart local v35    # "entry":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
    invoke-virtual/range {v35 .. v35}, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->getType()Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    move-result-object v85

    .line 388
    .restart local v85    # "type":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;
    move-object/from16 v0, v35

    iget-boolean v0, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mNavigationalResult:Z

    move/from16 v40, v0

    .line 389
    .restart local v40    # "isNavigational":Z
    sget-object v4, Lcom/google/android/music/store/SearchHandler$2;->$SwitchMap$com$google$android$music$cloudclient$SearchClientResponseJson$SearchClientResponseEntry$Type:[I

    invoke-virtual/range {v85 .. v85}, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->ordinal()I

    move-result v11

    aget v4, v4, v11

    packed-switch v4, :pswitch_data_1

    goto :goto_15

    .line 391
    :pswitch_6
    if-eqz v75, :cond_1e

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mArtist:Lcom/google/android/music/cloudclient/ArtistJson;

    if-eqz v4, :cond_1e

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mArtist:Lcom/google/android/music/cloudclient/ArtistJson;

    iget-object v4, v4, Lcom/google/android/music/cloudclient/ArtistJson;->mArtistId:Ljava/lang/String;

    if-eqz v4, :cond_1e

    .line 393
    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mArtist:Lcom/google/android/music/cloudclient/ArtistJson;

    move-object/from16 v26, v0

    .line 394
    .local v26, "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    move-object/from16 v0, v67

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/cloudclient/ArtistJson;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 396
    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/google/android/music/cloudclient/ArtistJson;->mArtistId:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 397
    if-eqz v40, :cond_1f

    invoke-virtual/range {v58 .. v58}, Landroid/database/MatrixCursor;->getCount()I

    move-result v4

    if-nez v4, :cond_1f

    .line 398
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/ArtistJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_15

    .line 483
    .end local v26    # "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    .end local v35    # "entry":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
    .end local v38    # "i$":Ljava/util/Iterator;
    .end local v40    # "isNavigational":Z
    .end local v85    # "type":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;
    :catch_1
    move-exception v34

    move-object/from16 v83, v84

    .end local v84    # "trackExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v83    # "trackExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    move-object/from16 v24, v25

    .end local v25    # "albumExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v24    # "albumExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    move-object/from16 v30, v31

    .end local v31    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v30    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v28, v29

    .end local v29    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v28    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    move-object/from16 v51, v52

    .end local v52    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .restart local v51    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v47, v48

    .end local v48    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .restart local v47    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v53, v54

    .end local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v43, v44

    .end local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v45, v46

    .end local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v63, v64

    .end local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v59, v60

    .end local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v61, v62

    .end local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v65, v66

    .end local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .line 401
    .end local v24    # "albumExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v28    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v30    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .end local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .end local v47    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .end local v51    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .end local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .end local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    .end local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .end local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    .end local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    .end local v83    # "trackExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v25    # "albumExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v26    # "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    .restart local v29    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v31    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v35    # "entry":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
    .restart local v38    # "i$":Ljava/util/Iterator;
    .restart local v40    # "isNavigational":Z
    .restart local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v48    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .restart local v52    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .restart local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v84    # "trackExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v85    # "type":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;
    :cond_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/ArtistJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_15

    .line 407
    .end local v26    # "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    :pswitch_7
    if-eqz v73, :cond_1e

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mAlbum:Lcom/google/android/music/cloudclient/AlbumJson;

    if-eqz v4, :cond_1e

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mAlbum:Lcom/google/android/music/cloudclient/AlbumJson;

    iget-object v4, v4, Lcom/google/android/music/cloudclient/AlbumJson;->mAlbumId:Ljava/lang/String;

    if-eqz v4, :cond_1e

    .line 409
    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mAlbum:Lcom/google/android/music/cloudclient/AlbumJson;

    move-object/from16 v22, v0

    .line 410
    .local v22, "album":Lcom/google/android/music/cloudclient/AlbumJson;
    move-object/from16 v0, v67

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/cloudclient/AlbumJson;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 412
    if-eqz v40, :cond_20

    invoke-virtual/range {v56 .. v56}, Landroid/database/MatrixCursor;->getCount()I

    move-result v4

    if-nez v4, :cond_20

    .line 413
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-static {v0, v4}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/AlbumJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v56

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_15

    .line 416
    :cond_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-static {v0, v4}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/AlbumJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_15

    .line 422
    .end local v22    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    :pswitch_8
    if-eqz v79, :cond_1e

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    if-eqz v4, :cond_1e

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    iget-object v4, v4, Lcom/google/android/music/sync/google/model/Track;->mNautilusId:Ljava/lang/String;

    if-eqz v4, :cond_1e

    .line 424
    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    move-object/from16 v80, v0

    .line 425
    .local v80, "track":Lcom/google/android/music/sync/google/model/Track;
    move-object/from16 v0, v67

    move-object/from16 v1, v80

    invoke-static {v0, v1}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/sync/google/model/Track;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v84

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 427
    if-eqz v40, :cond_21

    invoke-virtual/range {v66 .. v66}, Landroid/database/MatrixCursor;->getCount()I

    move-result v4

    if-nez v4, :cond_21

    .line 428
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v80

    invoke-static {v0, v4}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/TrackJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v66

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_15

    .line 431
    :cond_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v80

    invoke-static {v0, v4}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/TrackJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v54

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_15

    .line 438
    .end local v80    # "track":Lcom/google/android/music/sync/google/model/Track;
    :pswitch_9
    if-eqz v77, :cond_1e

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mPlaylist:Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    if-eqz v4, :cond_1e

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mPlaylist:Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    iget-object v4, v4, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mShareToken:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1e

    .line 440
    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mPlaylist:Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-object/from16 v68, v0

    .line 441
    .local v68, "playlist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    if-eqz v40, :cond_22

    invoke-virtual/range {v62 .. v62}, Landroid/database/MatrixCursor;->getCount()I

    move-result v4

    if-nez v4, :cond_22

    .line 442
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v68

    invoke-static {v0, v4}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/sync/google/model/SyncablePlaylist;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v62

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_15

    .line 445
    :cond_22
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v68

    invoke-static {v0, v4}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/sync/google/model/SyncablePlaylist;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v50

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_15

    .line 451
    .end local v68    # "playlist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :pswitch_a
    if-eqz v76, :cond_1e

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mGenre:Lcom/google/android/music/cloudclient/MusicGenreJson;

    if-eqz v4, :cond_1e

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mGenre:Lcom/google/android/music/cloudclient/MusicGenreJson;

    iget-object v4, v4, Lcom/google/android/music/cloudclient/MusicGenreJson;->mId:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1e

    .line 453
    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mGenre:Lcom/google/android/music/cloudclient/MusicGenreJson;

    move-object/from16 v37, v0

    .line 454
    .local v37, "genre":Lcom/google/android/music/cloudclient/MusicGenreJson;
    if-eqz v40, :cond_23

    invoke-virtual/range {v60 .. v60}, Landroid/database/MatrixCursor;->getCount()I

    move-result v4

    if-nez v4, :cond_23

    .line 455
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v37

    invoke-static {v0, v4}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/MusicGenreJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_15

    .line 458
    :cond_23
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v37

    invoke-static {v0, v4}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/MusicGenreJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_15

    .line 465
    .end local v37    # "genre":Lcom/google/android/music/cloudclient/MusicGenreJson;
    :pswitch_b
    if-eqz v78, :cond_1e

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mStation:Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    if-eqz v4, :cond_1e

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mStation:Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    iget-object v4, v4, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mSeed:Lcom/google/android/music/sync/google/model/RadioSeed;

    if-eqz v4, :cond_1e

    .line 467
    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mStation:Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move-object/from16 v70, v0

    .line 468
    .local v70, "radio":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    if-eqz v40, :cond_24

    invoke-virtual/range {v64 .. v64}, Landroid/database/MatrixCursor;->getCount()I

    move-result v4

    if-nez v4, :cond_24

    .line 469
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v70

    invoke-static {v0, v4}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/sync/google/model/SyncableRadioStation;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_15

    .line 472
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v70

    invoke-static {v0, v4}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/sync/google/model/SyncableRadioStation;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v52

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_1

    goto/16 :goto_15

    .end local v35    # "entry":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
    .end local v40    # "isNavigational":Z
    .end local v70    # "radio":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .end local v85    # "type":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;
    :cond_25
    move-object/from16 v83, v84

    .end local v84    # "trackExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v83    # "trackExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    move-object/from16 v24, v25

    .end local v25    # "albumExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v24    # "albumExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    move-object/from16 v30, v31

    .end local v31    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v30    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v28, v29

    .end local v29    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v28    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    move-object/from16 v51, v52

    .end local v52    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .restart local v51    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v47, v48

    .end local v48    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .restart local v47    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v53, v54

    .end local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v43, v44

    .end local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v45, v46

    .end local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v63, v64

    .end local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v59, v60

    .end local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v61, v62

    .end local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v65, v66

    .end local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .line 478
    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_b

    .line 480
    .end local v38    # "i$":Ljava/util/Iterator;
    :cond_26
    :try_start_13
    const-string v4, "Search"

    const-string v11, "null search response."

    invoke-static {v4, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_0

    goto/16 :goto_b

    .line 486
    .end local v36    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/music/cloudclient/SearchClientResponseJson;>;"
    .end local v71    # "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    :cond_27
    if-eqz v39, :cond_15

    .line 488
    new-instance v32, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/SearchHandler;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v32

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 492
    :cond_28
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " AND "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, "MUSIC.AlbumArtistId"

    move-object/from16 v0, v28

    invoke-static {v11, v0}, Lcom/google/android/music/utils/DbUtils;->getNotInClause(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, " AND "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, "MUSIC.ArtistId"

    move-object/from16 v0, v28

    invoke-static {v11, v0}, Lcom/google/android/music/utils/DbUtils;->getNotInClause(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_c

    .line 501
    .restart local v8    # "artistExclude":Ljava/lang/String;
    :cond_29
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " AND "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, "MUSIC.ArtistMetajamId"

    move-object/from16 v0, v30

    invoke-static {v11, v0}, Lcom/google/android/music/utils/DbUtils;->getStringNotInClause(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_d

    .line 508
    .restart local v9    # "artistMetajamExclude":Ljava/lang/String;
    :cond_2a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " AND "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, "MUSIC.AlbumId"

    move-object/from16 v0, v24

    invoke-static {v11, v0}, Lcom/google/android/music/utils/DbUtils;->getNotInClause(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_e

    .line 513
    .restart local v15    # "albumExclude":Ljava/lang/String;
    :cond_2b
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " AND "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, "MUSIC.SongId"

    move-object/from16 v0, v83

    invoke-static {v11, v0}, Lcom/google/android/music/utils/DbUtils;->getNotInClause(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    goto/16 :goto_f

    .line 553
    .restart local v5    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v6    # "canonicalSearchValue":Ljava/lang/String;
    .restart local v7    # "andFilter":Ljava/lang/String;
    .restart local v10    # "canonicalSearch":Z
    .restart local v19    # "trackAndFilter":Ljava/lang/String;
    .restart local v20    # "trackExclude":Ljava/lang/String;
    .restart local v72    # "searchValue":Ljava/lang/String;
    .restart local v81    # "trackAndFilterIndex":I
    :cond_2c
    const/4 v4, 0x5

    move/from16 v0, v81

    if-ne v0, v4, :cond_1b

    .line 554
    const/16 v81, 0x2

    goto/16 :goto_10

    .line 560
    .end local v81    # "trackAndFilterIndex":I
    :cond_2d
    const/16 v27, 0x0

    goto/16 :goto_11

    .line 563
    .restart local v27    # "artistCursor":Landroid/database/Cursor;
    :cond_2e
    const/16 v23, 0x0

    goto/16 :goto_12

    .line 566
    .restart local v23    # "albumCursor":Landroid/database/Cursor;
    :cond_2f
    const/16 v69, 0x0

    goto/16 :goto_13

    .line 568
    .restart local v69    # "playlistCursor":Landroid/database/Cursor;
    :cond_30
    const/16 v82, 0x0

    goto/16 :goto_14

    .line 572
    .end local v7    # "andFilter":Ljava/lang/String;
    .end local v19    # "trackAndFilter":Ljava/lang/String;
    .end local v23    # "albumCursor":Landroid/database/Cursor;
    .end local v27    # "artistCursor":Landroid/database/Cursor;
    .end local v69    # "playlistCursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/store/SearchHandler;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v11, v5}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v4

    .line 483
    .end local v5    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v6    # "canonicalSearchValue":Ljava/lang/String;
    .end local v8    # "artistExclude":Ljava/lang/String;
    .end local v9    # "artistMetajamExclude":Ljava/lang/String;
    .end local v10    # "canonicalSearch":Z
    .end local v15    # "albumExclude":Ljava/lang/String;
    .end local v20    # "trackExclude":Ljava/lang/String;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .end local v72    # "searchValue":Ljava/lang/String;
    .restart local v36    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/music/cloudclient/SearchClientResponseJson;>;"
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v71    # "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    :catch_2
    move-exception v34

    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    :catch_3
    move-exception v34

    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .end local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    :catch_4
    move-exception v34

    move-object/from16 v65, v66

    .end local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .end local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .end local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    :catch_5
    move-exception v34

    move-object/from16 v61, v62

    .end local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v65, v66

    .end local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .end local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    .end local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .end local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    :catch_6
    move-exception v34

    move-object/from16 v59, v60

    .end local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v61, v62

    .end local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v65, v66

    .end local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .end local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    .end local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .end local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    .end local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    :catch_7
    move-exception v34

    move-object/from16 v63, v64

    .end local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v59, v60

    .end local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v61, v62

    .end local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v65, v66

    .end local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .end local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    .end local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .end local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    .end local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    :catch_8
    move-exception v34

    move-object/from16 v45, v46

    .end local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v63, v64

    .end local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v59, v60

    .end local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v61, v62

    .end local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v65, v66

    .end local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .end local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .end local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    .end local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .end local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    .end local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    :catch_9
    move-exception v34

    move-object/from16 v43, v44

    .end local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v45, v46

    .end local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v63, v64

    .end local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v59, v60

    .end local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v61, v62

    .end local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v65, v66

    .end local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .end local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .end local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .end local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    .end local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .end local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    .end local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    :catch_a
    move-exception v34

    move-object/from16 v53, v54

    .end local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v43, v44

    .end local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v45, v46

    .end local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v63, v64

    .end local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v59, v60

    .end local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v61, v62

    .end local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v65, v66

    .end local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .end local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .end local v47    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .end local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .end local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    .end local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .end local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    .end local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v48    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .restart local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    :catch_b
    move-exception v34

    move-object/from16 v47, v48

    .end local v48    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .restart local v47    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v53, v54

    .end local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v43, v44

    .end local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v45, v46

    .end local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v63, v64

    .end local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v59, v60

    .end local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v61, v62

    .end local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v65, v66

    .end local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .end local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .end local v47    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .end local v51    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .end local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .end local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    .end local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .end local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    .end local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v48    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .restart local v52    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .restart local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    :catch_c
    move-exception v34

    move-object/from16 v51, v52

    .end local v52    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .restart local v51    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v47, v48

    .end local v48    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .restart local v47    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v53, v54

    .end local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v43, v44

    .end local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v45, v46

    .end local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v63, v64

    .end local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v59, v60

    .end local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v61, v62

    .end local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v65, v66

    .end local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v28    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .end local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .end local v47    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .end local v51    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .end local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .end local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    .end local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .end local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    .end local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v29    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v48    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .restart local v52    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .restart local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    :catch_d
    move-exception v34

    move-object/from16 v28, v29

    .end local v29    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v28    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    move-object/from16 v51, v52

    .end local v52    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .restart local v51    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v47, v48

    .end local v48    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .restart local v47    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v53, v54

    .end local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v43, v44

    .end local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v45, v46

    .end local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v63, v64

    .end local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v59, v60

    .end local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v61, v62

    .end local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v65, v66

    .end local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v28    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v30    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .end local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .end local v47    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .end local v51    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .end local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .end local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    .end local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .end local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    .end local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v29    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v31    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v48    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .restart local v52    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .restart local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    :catch_e
    move-exception v34

    move-object/from16 v30, v31

    .end local v31    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v30    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v28, v29

    .end local v29    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v28    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    move-object/from16 v51, v52

    .end local v52    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .restart local v51    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v47, v48

    .end local v48    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .restart local v47    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v53, v54

    .end local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v43, v44

    .end local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v45, v46

    .end local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v63, v64

    .end local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v59, v60

    .end local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v61, v62

    .end local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v65, v66

    .end local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v24    # "albumExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v28    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v30    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .end local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .end local v47    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .end local v51    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .end local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .end local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .end local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    .end local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    .end local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .end local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    .end local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v25    # "albumExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v29    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v31    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v48    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .restart local v52    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .restart local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    :catch_f
    move-exception v34

    move-object/from16 v24, v25

    .end local v25    # "albumExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v24    # "albumExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    move-object/from16 v30, v31

    .end local v31    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v30    # "artistMetajamExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v28, v29

    .end local v29    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v28    # "artistExcludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    move-object/from16 v51, v52

    .end local v52    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    .restart local v51    # "nautilusRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v47, v48

    .end local v48    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    .restart local v47    # "nautilusGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v53, v54

    .end local v54    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    .restart local v53    # "nautilusTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v43, v44

    .end local v44    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v43    # "nautilusAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v45, v46

    .end local v46    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    .restart local v45    # "nautilusArtistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v63, v64

    .end local v64    # "navRadioCursor":Landroid/database/MatrixCursor;
    .restart local v63    # "navRadioCursor":Landroid/database/MatrixCursor;
    move-object/from16 v59, v60

    .end local v60    # "navGenreCursor":Landroid/database/MatrixCursor;
    .restart local v59    # "navGenreCursor":Landroid/database/MatrixCursor;
    move-object/from16 v61, v62

    .end local v62    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    .restart local v61    # "navPlaylistCursor":Landroid/database/MatrixCursor;
    move-object/from16 v65, v66

    .end local v66    # "navTrackCursor":Landroid/database/MatrixCursor;
    .restart local v65    # "navTrackCursor":Landroid/database/MatrixCursor;
    move-object/from16 v55, v56

    .end local v56    # "navAlbumCursor":Landroid/database/MatrixCursor;
    .restart local v55    # "navAlbumCursor":Landroid/database/MatrixCursor;
    move-object/from16 v57, v58

    .end local v58    # "navArtistCursor":Landroid/database/MatrixCursor;
    .restart local v57    # "navArtistCursor":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .line 336
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 389
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public performSuggestedQueryLookup()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 221
    new-array v0, v6, [Ljava/lang/String;

    const-string v5, "suggestedQuery"

    aput-object v5, v0, v7

    .line 224
    .local v0, "columns":[Ljava/lang/String;
    new-instance v3, Landroid/database/MatrixCursor;

    invoke-direct {v3, v0, v6}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 227
    .local v3, "nautilusSuggestedQueryCursor":Landroid/database/MatrixCursor;
    iget-boolean v5, p0, Lcom/google/android/music/store/SearchHandler;->mIsNautilusEnabled:Z

    if-eqz v5, :cond_0

    .line 228
    iget-object v5, p0, Lcom/google/android/music/store/SearchHandler;->mSearchQuery:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/music/store/SearchHandler;->mNumSearchResults:I

    invoke-direct {p0, v5, v6}, Lcom/google/android/music/store/SearchHandler;->getServerResults(Ljava/lang/String;I)Ljava/util/concurrent/Future;

    move-result-object v2

    .line 233
    .local v2, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/music/cloudclient/SearchClientResponseJson;>;"
    const-wide/16 v6, 0x1e

    :try_start_0
    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v6, v7, v5}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/cloudclient/SearchClientResponseJson;

    .line 236
    .local v4, "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    iget-object v5, v4, Lcom/google/android/music/cloudclient/SearchClientResponseJson;->mSuggestedQuery:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 237
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v4, Lcom/google/android/music/cloudclient/SearchClientResponseJson;->mSuggestedQuery:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    .end local v2    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/music/cloudclient/SearchClientResponseJson;>;"
    .end local v4    # "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    :cond_0
    :goto_0
    return-object v3

    .line 240
    .restart local v2    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/music/cloudclient/SearchClientResponseJson;>;"
    :catch_0
    move-exception v1

    .line 241
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "Search"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
