.class final Lcom/google/android/music/wear/DefaultImageResizer;
.super Ljava/lang/Object;
.source "DefaultImageResizer.java"

# interfaces
.implements Lcom/google/android/music/wear/ImageResizer;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mSmallestDimensionSizePx:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/music/wear/DefaultImageResizer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/wear/DefaultImageResizer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "smallestDimensionSizePx"    # I

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "smallestDimensionSizePx must be non-negative"

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 32
    iput p1, p0, Lcom/google/android/music/wear/DefaultImageResizer;->mSmallestDimensionSizePx:I

    .line 33
    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getResizedImageInputStream(Ljava/io/File;)Ljava/io/InputStream;
    .locals 20
    .param p1, "imageFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 37
    const-string v15, "imageFile cannot be null"

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 41
    .local v2, "boundsOptions":Landroid/graphics/BitmapFactory$Options;
    const/4 v15, 0x1

    iput-boolean v15, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 42
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 43
    iget v10, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 44
    .local v10, "originalWidth":I
    iget v9, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 46
    .local v9, "originalHeight":I
    if-eqz v10, :cond_0

    if-nez v9, :cond_1

    .line 47
    :cond_0
    sget-object v15, Lcom/google/android/music/wear/DefaultImageResizer;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "One of the image dimensions were zero: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " x "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    new-instance v15, Ljava/io/FileNotFoundException;

    invoke-direct {v15}, Ljava/io/FileNotFoundException;-><init>()V

    throw v15

    .line 53
    :cond_1
    int-to-double v0, v10

    move-wide/from16 v16, v0

    int-to-double v0, v9

    move-wide/from16 v18, v0

    div-double v6, v16, v18

    .line 56
    .local v6, "imageRatio":D
    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    cmpl-double v15, v6, v16

    if-ltz v15, :cond_2

    .line 57
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/music/wear/DefaultImageResizer;->mSmallestDimensionSizePx:I

    invoke-static {v15, v9}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 58
    .local v5, "newHeight":I
    int-to-double v0, v5

    move-wide/from16 v16, v0

    mul-double v16, v16, v6

    move-wide/from16 v0, v16

    double-to-int v8, v0

    .line 64
    .local v8, "newWidth":I
    :goto_0
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 67
    .local v4, "decodingOptions":Landroid/graphics/BitmapFactory$Options;
    int-to-double v0, v9

    move-wide/from16 v16, v0

    int-to-double v0, v5

    move-wide/from16 v18, v0

    div-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->floor(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-int v14, v0

    .line 68
    .local v14, "scalingFactor":I
    invoke-static {v14}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v15

    iput v15, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 69
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 71
    .local v13, "scaledOriginalBitmap":Landroid/graphics/Bitmap;
    const/4 v15, 0x1

    invoke-static {v13, v8, v5, v15}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 74
    .local v12, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v11, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v11}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 75
    .local v11, "outputStream":Ljava/io/ByteArrayOutputStream;
    sget-object v15, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v16, 0x5a

    move/from16 v0, v16

    invoke-virtual {v12, v15, v0, v11}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 76
    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 77
    .local v3, "compressedBitmapBytes":[B
    invoke-static {v11}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 78
    new-instance v15, Ljava/io/ByteArrayInputStream;

    invoke-direct {v15, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v15

    .line 60
    .end local v3    # "compressedBitmapBytes":[B
    .end local v4    # "decodingOptions":Landroid/graphics/BitmapFactory$Options;
    .end local v5    # "newHeight":I
    .end local v8    # "newWidth":I
    .end local v11    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .end local v12    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v13    # "scaledOriginalBitmap":Landroid/graphics/Bitmap;
    .end local v14    # "scalingFactor":I
    :cond_2
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/music/wear/DefaultImageResizer;->mSmallestDimensionSizePx:I

    invoke-static {v15, v10}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 61
    .restart local v8    # "newWidth":I
    int-to-double v0, v8

    move-wide/from16 v16, v0

    div-double v16, v16, v6

    move-wide/from16 v0, v16

    double-to-int v5, v0

    .restart local v5    # "newHeight":I
    goto :goto_0
.end method
