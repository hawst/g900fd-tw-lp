.class final enum Lcom/google/android/music/RatingSelector$Asset;
.super Ljava/lang/Enum;
.source "RatingSelector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/RatingSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Asset"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/RatingSelector$Asset;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/RatingSelector$Asset;

.field public static final enum THUMBS_DOWN_DEFAULT:Lcom/google/android/music/RatingSelector$Asset;

.field public static final enum THUMBS_DOWN_SELECTED:Lcom/google/android/music/RatingSelector$Asset;

.field public static final enum THUMBS_UP_DEFAULT:Lcom/google/android/music/RatingSelector$Asset;

.field public static final enum THUMBS_UP_SELECTED:Lcom/google/android/music/RatingSelector$Asset;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-instance v0, Lcom/google/android/music/RatingSelector$Asset;

    const-string v1, "THUMBS_DOWN_DEFAULT"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/RatingSelector$Asset;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_DOWN_DEFAULT:Lcom/google/android/music/RatingSelector$Asset;

    .line 43
    new-instance v0, Lcom/google/android/music/RatingSelector$Asset;

    const-string v1, "THUMBS_DOWN_SELECTED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/RatingSelector$Asset;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_DOWN_SELECTED:Lcom/google/android/music/RatingSelector$Asset;

    .line 44
    new-instance v0, Lcom/google/android/music/RatingSelector$Asset;

    const-string v1, "THUMBS_UP_DEFAULT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/RatingSelector$Asset;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_UP_DEFAULT:Lcom/google/android/music/RatingSelector$Asset;

    .line 45
    new-instance v0, Lcom/google/android/music/RatingSelector$Asset;

    const-string v1, "THUMBS_UP_SELECTED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/RatingSelector$Asset;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_UP_SELECTED:Lcom/google/android/music/RatingSelector$Asset;

    .line 41
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/music/RatingSelector$Asset;

    sget-object v1, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_DOWN_DEFAULT:Lcom/google/android/music/RatingSelector$Asset;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_DOWN_SELECTED:Lcom/google/android/music/RatingSelector$Asset;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_UP_DEFAULT:Lcom/google/android/music/RatingSelector$Asset;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_UP_SELECTED:Lcom/google/android/music/RatingSelector$Asset;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/RatingSelector$Asset;->$VALUES:[Lcom/google/android/music/RatingSelector$Asset;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/RatingSelector$Asset;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 41
    const-class v0, Lcom/google/android/music/RatingSelector$Asset;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/RatingSelector$Asset;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/RatingSelector$Asset;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/music/RatingSelector$Asset;->$VALUES:[Lcom/google/android/music/RatingSelector$Asset;

    invoke-virtual {v0}, [Lcom/google/android/music/RatingSelector$Asset;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/RatingSelector$Asset;

    return-object v0
.end method
