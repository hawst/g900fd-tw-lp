.class Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CastListener;
.super Lcom/google/android/gms/cast/Cast$Listener;
.source "CastRemotePlaybackClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CastListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;


# direct methods
.method private constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V
    .locals 0

    .prologue
    .line 611
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CastListener;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    invoke-direct {p0}, Lcom/google/android/gms/cast/Cast$Listener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
    .param p2, "x1"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;

    .prologue
    .line 611
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CastListener;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V

    return-void
.end method


# virtual methods
.method public onApplicationDisconnected(I)V
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 614
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CastListener;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 615
    const-string v0, "MusicCastRemote"

    const-string v1, "onApplicationDisconnected"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CastListener;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyMediaRouteInvalidated()V

    .line 618
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CastListener;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # invokes: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->tearDown()V
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1100(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V

    .line 619
    return-void
.end method

.method public onApplicationStatusChanged()V
    .locals 2

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CastListener;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 624
    const-string v0, "MusicCastRemote"

    const-string v1, "onApplicationStatusChanged"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    :cond_0
    return-void
.end method

.method public onVolumeChanged()V
    .locals 2

    .prologue
    .line 631
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CastListener;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 632
    const-string v0, "MusicCastRemote"

    const-string v1, "onVolumeChanged"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    :cond_0
    return-void
.end method
