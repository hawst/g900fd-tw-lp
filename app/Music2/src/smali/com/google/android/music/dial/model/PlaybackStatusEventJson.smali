.class public Lcom/google/android/music/dial/model/PlaybackStatusEventJson;
.super Lcom/google/api/client/json/GenericJson;
.source "PlaybackStatusEventJson.java"


# instance fields
.field public mItemId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "itemId"
    .end annotation
.end field

.field public mPlaybackState:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "playbackState"
    .end annotation
.end field

.field public mPositionMillis:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "positionMillis"
    .end annotation
.end field

.field public mQueueVersion:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "queueVersion"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
