.class Lcom/google/android/music/ui/NowPlayingScreenFragment$14;
.super Ljava/lang/Object;
.source "NowPlayingScreenFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/NowPlayingScreenFragment;->setupPlayQueue()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0

    .prologue
    .line 1456
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$14;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1459
    .local p1, "lv":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    instance-of v1, p1, Lcom/google/android/music/ui/BaseTrackListView;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 1460
    check-cast v0, Lcom/google/android/music/ui/BaseTrackListView;

    .local v0, "btlv":Lcom/google/android/music/ui/BaseTrackListView;
    move-object v1, v0

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    .line 1461
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/music/ui/BaseTrackListView;->handleItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 1465
    .end local v0    # "btlv":Lcom/google/android/music/ui/BaseTrackListView;
    :goto_0
    return-void

    .line 1463
    :cond_0
    const-string v1, "NowPlayingFragment"

    const-string v2, "Unknown listview type"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
