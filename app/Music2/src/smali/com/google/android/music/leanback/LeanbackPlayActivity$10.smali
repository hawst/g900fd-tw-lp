.class final Lcom/google/android/music/leanback/LeanbackPlayActivity$10;
.super Ljava/lang/Object;
.source "LeanbackPlayActivity.java"

# interfaces
.implements Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity;->loadArtistArtwork(Lcom/google/android/music/leanback/LeanbackItemActivity;Lcom/google/android/music/medialist/SongList;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$leanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

.field final synthetic val$loaderId:I

.field final synthetic val$songList:Lcom/google/android/music/medialist/SongList;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackItemActivity;Lcom/google/android/music/medialist/SongList;I)V
    .locals 0

    .prologue
    .line 934
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10;->val$leanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10;->val$songList:Lcom/google/android/music/medialist/SongList;

    iput p3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10;->val$loaderId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getItem(Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;)V
    .locals 5
    .param p1, "itemGetterListener"    # Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;

    .prologue
    .line 937
    new-instance v0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$1;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity$10;)V

    .line 952
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    new-instance v1, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$2;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$2;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity$10;Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 963
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10;->val$leanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    invoke-virtual {v1}, Lcom/google/android/music/leanback/LeanbackItemActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10;->val$loaderId:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 964
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10;->val$leanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    iget v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10;->val$loaderId:I

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10;->val$songList:Lcom/google/android/music/medialist/SongList;

    sget-object v4, Lcom/google/android/music/xdi/TrackAdapter;->TRACK_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/music/leanback/LeanbackItemActivity;->loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 966
    return-void
.end method
