.class public Lcom/google/android/music/net/NetworkMonitor;
.super Landroid/app/Service;
.source "NetworkMonitor.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;,
        Lcom/google/android/music/net/NetworkMonitor$NotifyListenersDownloadabilityChangedRunnable;,
        Lcom/google/android/music/net/NetworkMonitor$NotifyListenersStreamabilityChangedRunnable;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static sActiveNetworkMonitor:Lcom/google/android/music/net/NetworkMonitor;

.field private static sCurrentBitrate:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private mConnectedMobileDataType:I

.field private final mConnectionBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mConnectivityType:I

.field private mDownloadabilityListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/google/android/music/net/IDownloadabilityChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mIsDownloadingAvailable:Z

.field private mIsStreamingAvailable:Z

.field private mMobileOrMeteredConnected:Z

.field private mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private mNetworkChangeListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/google/android/music/net/INetworkChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mNetworkMonitorBinder:Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;

.field private mNotifiyListenersNetworkChangedRunnable:Ljava/lang/Runnable;

.field private final mStateRecievedLock:Ljava/lang/Object;

.field private mStreamabilityListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/google/android/music/net/IStreamabilityChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mUnmeteredWifiOrEthernetConnected:Z

.field private final mUpdateDownloadingAvailable:Ljava/lang/Runnable;

.field private final mUpdateStreamingAvailable:Ljava/lang/Runnable;

.field private mWifiConnected:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/net/NetworkMonitor;->LOGV:Z

    .line 49
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/16 v1, 0x60

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/android/music/net/NetworkMonitor;->sCurrentBitrate:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 92
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/music/net/NetworkMonitor;->sActiveNetworkMonitor:Lcom/google/android/music/net/NetworkMonitor;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 51
    iput-object v3, p0, Lcom/google/android/music/net/NetworkMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 53
    new-instance v0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;

    invoke-direct {v0, p0, v3}, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;-><init>(Lcom/google/android/music/net/NetworkMonitor;Lcom/google/android/music/net/NetworkMonitor$1;)V

    iput-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mNetworkMonitorBinder:Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;

    .line 55
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mStreamabilityListeners:Landroid/os/RemoteCallbackList;

    .line 58
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mDownloadabilityListeners:Landroid/os/RemoteCallbackList;

    .line 61
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mNetworkChangeListeners:Landroid/os/RemoteCallbackList;

    .line 69
    iput-boolean v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mMobileOrMeteredConnected:Z

    .line 70
    iput v2, p0, Lcom/google/android/music/net/NetworkMonitor;->mConnectivityType:I

    .line 71
    iput v2, p0, Lcom/google/android/music/net/NetworkMonitor;->mConnectedMobileDataType:I

    .line 76
    iput-boolean v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mUnmeteredWifiOrEthernetConnected:Z

    .line 82
    iput-boolean v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mWifiConnected:Z

    .line 87
    iput-boolean v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mIsStreamingAvailable:Z

    .line 88
    iput-boolean v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mIsDownloadingAvailable:Z

    .line 90
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mStateRecievedLock:Ljava/lang/Object;

    .line 274
    new-instance v0, Lcom/google/android/music/net/NetworkMonitor$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/net/NetworkMonitor$1;-><init>(Lcom/google/android/music/net/NetworkMonitor;)V

    iput-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mConnectionBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 340
    new-instance v0, Lcom/google/android/music/net/NetworkMonitor$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/net/NetworkMonitor$2;-><init>(Lcom/google/android/music/net/NetworkMonitor;)V

    iput-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mUpdateStreamingAvailable:Ljava/lang/Runnable;

    .line 365
    new-instance v0, Lcom/google/android/music/net/NetworkMonitor$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/net/NetworkMonitor$3;-><init>(Lcom/google/android/music/net/NetworkMonitor;)V

    iput-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mUpdateDownloadingAvailable:Ljava/lang/Runnable;

    .line 458
    new-instance v0, Lcom/google/android/music/net/NetworkMonitor$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/net/NetworkMonitor$4;-><init>(Lcom/google/android/music/net/NetworkMonitor;)V

    iput-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mNotifiyListenersNetworkChangedRunnable:Ljava/lang/Runnable;

    .line 490
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/net/NetworkMonitor;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mStateRecievedLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/net/NetworkMonitor;)Landroid/telephony/TelephonyManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/music/net/NetworkMonitor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/music/net/NetworkMonitor;->getIsStreamingAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/net/NetworkMonitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/music/net/NetworkMonitor;->notifyListenersNetworkChanged()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/music/net/NetworkMonitor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mIsStreamingAvailable:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/music/net/NetworkMonitor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/google/android/music/net/NetworkMonitor;->mIsStreamingAvailable:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/music/net/NetworkMonitor;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/music/net/NetworkMonitor;->notifyListenersStreamabilityChanged(Z)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/music/net/NetworkMonitor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/music/net/NetworkMonitor;->getIsDownloadingAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/music/net/NetworkMonitor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mIsDownloadingAvailable:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/google/android/music/net/NetworkMonitor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/google/android/music/net/NetworkMonitor;->mIsDownloadingAvailable:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/google/android/music/net/NetworkMonitor;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/music/net/NetworkMonitor;->notifyListenersDownloadabilityChanged(Z)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mStreamabilityListeners:Landroid/os/RemoteCallbackList;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mDownloadabilityListeners:Landroid/os/RemoteCallbackList;

    return-object v0
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 33
    sget-boolean v0, Lcom/google/android/music/net/NetworkMonitor;->LOGV:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mNetworkChangeListeners:Landroid/os/RemoteCallbackList;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/music/net/NetworkMonitor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mWifiConnected:Z

    return v0
.end method

.method static synthetic access$300()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/music/net/NetworkMonitor;->sCurrentBitrate:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/net/NetworkMonitor;Landroid/net/NetworkInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;
    .param p1, "x1"    # Landroid/net/NetworkInfo;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/music/net/NetworkMonitor;->updateInternalNetworkState(Landroid/net/NetworkInfo;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/net/NetworkMonitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/music/net/NetworkMonitor;->processConnectivityChange()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/net/NetworkMonitor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mMobileOrMeteredConnected:Z

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/music/net/NetworkMonitor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/google/android/music/net/NetworkMonitor;->mMobileOrMeteredConnected:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/music/net/NetworkMonitor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mUnmeteredWifiOrEthernetConnected:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/music/net/NetworkMonitor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/google/android/music/net/NetworkMonitor;->mUnmeteredWifiOrEthernetConnected:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/android/music/net/NetworkMonitor;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/google/android/music/net/NetworkMonitor;->mConnectivityType:I

    return p1
.end method

.method static synthetic access$902(Lcom/google/android/music/net/NetworkMonitor;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitor;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/google/android/music/net/NetworkMonitor;->mConnectedMobileDataType:I

    return p1
.end method

.method private getIsDownloadingAvailable()Z
    .locals 2

    .prologue
    .line 130
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isOfflineDLOnlyOnWifi()Z

    move-result v0

    .line 131
    .local v0, "downloadOnlyOnWifi":Z
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mNetworkMonitorBinder:Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;

    invoke-virtual {v1}, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->hasHighSpeedConnection()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mNetworkMonitorBinder:Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;

    invoke-virtual {v1}, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->isConnected()Z

    move-result v1

    goto :goto_0
.end method

.method private getIsStreamingAvailable()Z
    .locals 2

    .prologue
    .line 124
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isStreamOnlyOnWifi()Z

    move-result v0

    .line 125
    .local v0, "streamOnlyOnWifi":Z
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mNetworkMonitorBinder:Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;

    invoke-virtual {v1}, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->hasHighSpeedConnection()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mNetworkMonitorBinder:Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;

    invoke-virtual {v1}, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->isConnected()Z

    move-result v1

    goto :goto_0
.end method

.method public static getRecommendedBitrate()I
    .locals 1

    .prologue
    .line 201
    sget-object v0, Lcom/google/android/music/net/NetworkMonitor;->sCurrentBitrate:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public static initIsStreamingEnabled(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 228
    invoke-static {p0}, Lcom/google/android/music/download/DownloadUtils;->isStreamingAvailable(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private notifyListenersDownloadabilityChanged(Z)V
    .locals 2
    .param p1, "isDownloadable"    # Z

    .prologue
    .line 454
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/net/NetworkMonitor$NotifyListenersDownloadabilityChangedRunnable;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/net/NetworkMonitor$NotifyListenersDownloadabilityChangedRunnable;-><init>(Lcom/google/android/music/net/NetworkMonitor;Z)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 456
    return-void
.end method

.method private notifyListenersNetworkChanged()V
    .locals 2

    .prologue
    .line 481
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mNotifiyListenersNetworkChangedRunnable:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 483
    return-void
.end method

.method private notifyListenersStreamabilityChanged(Z)V
    .locals 2
    .param p1, "isStreamable"    # Z

    .prologue
    .line 449
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/net/NetworkMonitor$NotifyListenersStreamabilityChangedRunnable;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/net/NetworkMonitor$NotifyListenersStreamabilityChangedRunnable;-><init>(Lcom/google/android/music/net/NetworkMonitor;Z)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 451
    return-void
.end method

.method private processConnectivityChange()V
    .locals 2

    .prologue
    .line 331
    invoke-direct {p0}, Lcom/google/android/music/net/NetworkMonitor;->resetBitrate()V

    .line 333
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mUpdateStreamingAvailable:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 336
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mUpdateDownloadingAvailable:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 338
    return-void
.end method

.method public static reportBitrate(Landroid/content/Context;JJ)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "totalLengthDownloaded"    # J
    .param p3, "timeToDownload"    # J

    .prologue
    const-wide v6, 0x408f400000000000L    # 1000.0

    .line 207
    const-wide/32 v2, 0x80000

    cmp-long v1, p1, v2

    if-ltz v1, :cond_0

    const-wide/16 v2, 0xbb8

    cmp-long v1, p3, v2

    if-gez v1, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 211
    :cond_1
    const-wide/16 v2, 0x8

    mul-long/2addr v2, p1

    long-to-float v1, v2

    const/high16 v2, 0x3f400000    # 0.75f

    mul-float/2addr v1, v2

    float-to-double v2, v1

    div-double/2addr v2, v6

    long-to-double v4, p3

    div-double/2addr v4, v6

    div-double/2addr v2, v4

    double-to-int v0, v2

    .line 214
    .local v0, "newBitrate":I
    const/16 v1, 0x30

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 216
    sget-object v1, Lcom/google/android/music/net/NetworkMonitor;->sCurrentBitrate:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_0
.end method

.method private resetBitrate()V
    .locals 4

    .prologue
    const/16 v3, 0x100

    const/16 v2, 0x60

    .line 161
    iget-boolean v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mUnmeteredWifiOrEthernetConnected:Z

    if-eqz v0, :cond_0

    .line 162
    sget-object v0, Lcom/google/android/music/net/NetworkMonitor;->sCurrentBitrate:Ljava/util/concurrent/atomic/AtomicInteger;

    const/16 v1, 0x200

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 198
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mMobileOrMeteredConnected:Z

    if-eqz v0, :cond_2

    .line 166
    iget v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mConnectivityType:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 167
    sget-object v0, Lcom/google/android/music/net/NetworkMonitor;->sCurrentBitrate:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_0

    .line 169
    :cond_1
    iget v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mConnectedMobileDataType:I

    packed-switch v0, :pswitch_data_0

    .line 191
    sget-object v0, Lcom/google/android/music/net/NetworkMonitor;->sCurrentBitrate:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_0

    .line 173
    :pswitch_0
    sget-object v0, Lcom/google/android/music/net/NetworkMonitor;->sCurrentBitrate:Ljava/util/concurrent/atomic/AtomicInteger;

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_0

    .line 185
    :pswitch_1
    sget-object v0, Lcom/google/android/music/net/NetworkMonitor;->sCurrentBitrate:Ljava/util/concurrent/atomic/AtomicInteger;

    const/16 v1, 0xc0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_0

    .line 188
    :pswitch_2
    sget-object v0, Lcom/google/android/music/net/NetworkMonitor;->sCurrentBitrate:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_0

    .line 196
    :cond_2
    sget-object v0, Lcom/google/android/music/net/NetworkMonitor;->sCurrentBitrate:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private updateInternalNetworkState(Landroid/net/NetworkInfo;)V
    .locals 9
    .param p1, "netInfo"    # Landroid/net/NetworkInfo;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 138
    if-eqz p1, :cond_0

    .line 139
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    .line 140
    .local v1, "type":I
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    .line 141
    .local v0, "isConnected":Z
    const-string v2, "NetworkMonitor"

    const-string v5, "type=%s subType=%s reason=%s isConnected=%b"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v7, 0x2

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-static {v1}, Lcom/google/android/music/download/DownloadUtils;->isSupportedNetworkType(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 145
    if-eqz v0, :cond_1

    invoke-static {p1, p0}, Lcom/google/android/music/download/DownloadUtils;->isMobileOrMeteredNetworkType(Landroid/net/NetworkInfo;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/music/net/NetworkMonitor;->mMobileOrMeteredConnected:Z

    .line 147
    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/google/android/music/download/DownloadUtils;->isWifiNetworkType(Landroid/net/NetworkInfo;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/music/net/NetworkMonitor;->mWifiConnected:Z

    .line 148
    if-eqz v0, :cond_3

    invoke-static {p1, p0}, Lcom/google/android/music/download/DownloadUtils;->isUnmeteredWifiOrEthernetConnection(Landroid/net/NetworkInfo;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_2
    iput-boolean v3, p0, Lcom/google/android/music/net/NetworkMonitor;->mUnmeteredWifiOrEthernetConnected:Z

    .line 150
    iput v1, p0, Lcom/google/android/music/net/NetworkMonitor;->mConnectivityType:I

    .line 151
    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v2

    iput v2, p0, Lcom/google/android/music/net/NetworkMonitor;->mConnectedMobileDataType:I

    .line 152
    invoke-direct {p0}, Lcom/google/android/music/net/NetworkMonitor;->resetBitrate()V

    .line 155
    .end local v0    # "isConnected":Z
    .end local v1    # "type":I
    :cond_0
    return-void

    .restart local v0    # "isConnected":Z
    .restart local v1    # "type":I
    :cond_1
    move v2, v4

    .line 145
    goto :goto_0

    :cond_2
    move v2, v4

    .line 147
    goto :goto_1

    :cond_3
    move v3, v4

    .line 148
    goto :goto_2
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 487
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mNetworkMonitorBinder:Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 95
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 96
    sput-object p0, Lcom/google/android/music/net/NetworkMonitor;->sActiveNetworkMonitor:Lcom/google/android/music/net/NetworkMonitor;

    .line 97
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Lcom/google/android/music/net/NetworkMonitor;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    iput-object v2, p0, Lcom/google/android/music/net/NetworkMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 100
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Lcom/google/android/music/net/NetworkMonitor;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 101
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/net/NetworkMonitor;->updateInternalNetworkState(Landroid/net/NetworkInfo;)V

    .line 103
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/net/NetworkMonitor;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 104
    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitor;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v2, p0}, Lcom/google/android/music/preferences/MusicPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 107
    invoke-virtual {p0, v3, v3}, Lcom/google/android/music/net/NetworkMonitor;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 109
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 110
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 112
    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitor;->mConnectionBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/google/android/music/net/NetworkMonitor;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 113
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/music/net/NetworkMonitor;->sActiveNetworkMonitor:Lcom/google/android/music/net/NetworkMonitor;

    .line 118
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 119
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor;->mConnectionBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/music/net/NetworkMonitor;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 121
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 5
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 239
    const/4 v1, 0x1

    .line 243
    .local v1, "interestedInNetworkState":Z
    iget-object v4, p0, Lcom/google/android/music/net/NetworkMonitor;->mStateRecievedLock:Ljava/lang/Object;

    monitor-enter v4

    .line 244
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/net/NetworkMonitor;->getIsStreamingAvailable()Z

    move-result v2

    .line 246
    .local v2, "newIsAvail":Z
    iget-boolean v3, p0, Lcom/google/android/music/net/NetworkMonitor;->mIsStreamingAvailable:Z

    if-ne v2, v3, :cond_0

    .line 248
    monitor-exit v4

    .line 272
    :goto_0
    return-void

    .line 250
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/music/net/NetworkMonitor;->mIsStreamingAvailable:Z

    .line 252
    invoke-direct {p0, v2}, Lcom/google/android/music/net/NetworkMonitor;->notifyListenersStreamabilityChanged(Z)V

    .line 253
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 257
    const/4 v0, 0x1

    .line 260
    .local v0, "interestedInDownloadState":Z
    iget-object v4, p0, Lcom/google/android/music/net/NetworkMonitor;->mStateRecievedLock:Ljava/lang/Object;

    monitor-enter v4

    .line 261
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/music/net/NetworkMonitor;->getIsDownloadingAvailable()Z

    move-result v2

    .line 263
    iget-boolean v3, p0, Lcom/google/android/music/net/NetworkMonitor;->mIsDownloadingAvailable:Z

    if-ne v2, v3, :cond_1

    .line 265
    monitor-exit v4

    goto :goto_0

    .line 270
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 253
    .end local v0    # "interestedInDownloadState":Z
    .end local v2    # "newIsAvail":Z
    :catchall_1
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3

    .line 267
    .restart local v0    # "interestedInDownloadState":Z
    .restart local v2    # "newIsAvail":Z
    :cond_1
    :try_start_3
    iput-boolean v2, p0, Lcom/google/android/music/net/NetworkMonitor;->mIsDownloadingAvailable:Z

    .line 269
    invoke-direct {p0, v2}, Lcom/google/android/music/net/NetworkMonitor;->notifyListenersDownloadabilityChanged(Z)V

    .line 270
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
