.class public Lcom/google/android/music/leanback/DailyWindow$Builder;
.super Ljava/lang/Object;
.source "DailyWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/DailyWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mDurationInMillisec:I

.field private mFrequencyInMillisec:J

.field private final mStartInMillisecSinceMidnight:I

.field private mVariableTriggerPercentage:I


# direct methods
.method private constructor <init>(II)V
    .locals 4
    .param p1, "startInMillisecSinceMidnight"    # I
    .param p2, "durationInMillisec"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const v3, 0x5265c00

    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/leanback/DailyWindow$Builder;->mFrequencyInMillisec:J

    .line 212
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/leanback/DailyWindow$Builder;->mVariableTriggerPercentage:I

    .line 258
    if-ltz p1, :cond_0

    if-le p1, v3, :cond_1

    .line 260
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid startInMillisecSinceMidnight:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 263
    :cond_1
    if-ltz p2, :cond_2

    if-le p2, v3, :cond_3

    .line 264
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid durationInMillisec:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 267
    :cond_3
    add-int v0, p1, p2

    if-le v0, v3, :cond_4

    .line 270
    const-string v0, "MusicLeanback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Combined start and duration are greater than a day. startInMillisecSinceMidnight: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". durationInMillisec: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 275
    sub-int p2, v3, p1

    .line 278
    :cond_4
    iput p1, p0, Lcom/google/android/music/leanback/DailyWindow$Builder;->mStartInMillisecSinceMidnight:I

    .line 279
    iput p2, p0, Lcom/google/android/music/leanback/DailyWindow$Builder;->mDurationInMillisec:I

    .line 280
    return-void
.end method

.method public static startBuildingInMinutes(II)Lcom/google/android/music/leanback/DailyWindow$Builder;
    .locals 3
    .param p0, "startInMinutesSinceMidnight"    # I
    .param p1, "durationInMinutes"    # I

    .prologue
    const v2, 0xea60

    const/16 v0, 0x5a0

    .line 238
    if-ltz p0, :cond_0

    if-le p0, v0, :cond_1

    .line 239
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid startInMinutesSinceMidnight:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :cond_1
    if-ltz p1, :cond_2

    if-le p1, v0, :cond_3

    .line 243
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid durationInMinutes:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 246
    :cond_3
    new-instance v0, Lcom/google/android/music/leanback/DailyWindow$Builder;

    mul-int v1, p0, v2

    mul-int/2addr v2, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/leanback/DailyWindow$Builder;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/music/leanback/DailyWindow;
    .locals 8

    .prologue
    .line 285
    iget-wide v0, p0, Lcom/google/android/music/leanback/DailyWindow$Builder;->mFrequencyInMillisec:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 286
    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lcom/google/android/music/leanback/DailyWindow$Builder;->mFrequencyInMillisec:J

    .line 290
    :cond_0
    new-instance v1, Lcom/google/android/music/leanback/DailyWindow;

    iget v2, p0, Lcom/google/android/music/leanback/DailyWindow$Builder;->mStartInMillisecSinceMidnight:I

    iget v3, p0, Lcom/google/android/music/leanback/DailyWindow$Builder;->mDurationInMillisec:I

    iget-wide v4, p0, Lcom/google/android/music/leanback/DailyWindow$Builder;->mFrequencyInMillisec:J

    iget v6, p0, Lcom/google/android/music/leanback/DailyWindow$Builder;->mVariableTriggerPercentage:I

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/leanback/DailyWindow;-><init>(IIJILcom/google/android/music/leanback/DailyWindow$1;)V

    return-object v1
.end method

.method public setFrequencyInMinutes(I)Lcom/google/android/music/leanback/DailyWindow$Builder;
    .locals 4
    .param p1, "frequencyInMinutes"    # I

    .prologue
    .line 315
    int-to-long v0, p1

    const-wide/32 v2, 0xea60

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/music/leanback/DailyWindow$Builder;->mFrequencyInMillisec:J

    .line 316
    return-object p0
.end method

.method public setVariableTriggerPercentage(I)Lcom/google/android/music/leanback/DailyWindow$Builder;
    .locals 3
    .param p1, "triggerPercentage"    # I

    .prologue
    .line 337
    if-ltz p1, :cond_0

    const/16 v0, 0x64

    if-le p1, v0, :cond_1

    .line 338
    :cond_0
    const-string v0, "MusicLeanback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid trigger percentage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 340
    :cond_1
    iput p1, p0, Lcom/google/android/music/leanback/DailyWindow$Builder;->mVariableTriggerPercentage:I

    .line 341
    return-object p0
.end method
