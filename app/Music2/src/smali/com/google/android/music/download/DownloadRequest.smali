.class public interface abstract Lcom/google/android/music/download/DownloadRequest;
.super Ljava/lang/Object;
.source "DownloadRequest.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/DownloadRequest$Owner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ProgressType::",
        "Lcom/google/android/music/download/DownloadProgress;",
        "OwnerType::",
        "Lcom/google/android/music/download/DownloadRequest$Owner;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/download/DownloadRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/music/download/DownloadRequest$1;

    invoke-direct {v0}, Lcom/google/android/music/download/DownloadRequest$1;-><init>()V

    sput-object v0, Lcom/google/android/music/download/DownloadRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method


# virtual methods
.method public abstract getFileLocation()Lcom/google/android/music/download/cache/FileLocation;
.end method

.method public abstract getId()Lcom/google/android/music/download/ContentIdentifier;
.end method

.method public abstract getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TOwnerType;"
        }
    .end annotation
.end method

.method public abstract getPriority()I
.end method

.method public abstract isRetryAllowed()Z
.end method

.method public abstract upgrade(Lcom/google/android/music/download/DownloadRequest;)Z
.end method
