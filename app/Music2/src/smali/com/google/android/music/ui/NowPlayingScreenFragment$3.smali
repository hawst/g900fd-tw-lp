.class Lcom/google/android/music/ui/NowPlayingScreenFragment$3;
.super Landroid/content/BroadcastReceiver;
.source "NowPlayingScreenFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingScreenFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 276
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 277
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.google.android.music.nowplaying.HEADER_CLICKED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 278
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;

    move-result-object v1

    .line 279
    .local v1, "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    if-eqz v1, :cond_0

    .line 281
    invoke-virtual {v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExpandingState()Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v2, v3, :cond_1

    .line 282
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {v1, v2}, Lcom/google/android/music/widgets/ExpandingScrollView;->moveToExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    .line 311
    .end local v1    # "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    :cond_0
    :goto_0
    return-void

    .line 284
    .restart local v1    # "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    :cond_1
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {v1, v2}, Lcom/google/android/music/widgets/ExpandingScrollView;->moveToExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    goto :goto_0

    .line 287
    .end local v1    # "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    :cond_2
    const-string v2, "com.google.android.music.nowplaying.HEADER_ART_CLICKED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 288
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$100(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v2

    sget-object v4, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v2, v4, :cond_4

    .line 290
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueShown:Z
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$200(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v3

    :goto_1
    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->showQueue(ZZ)V
    invoke-static {v4, v2, v3}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$300(Lcom/google/android/music/ui/NowPlayingScreenFragment;ZZ)V

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 292
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;

    move-result-object v1

    .line 293
    .restart local v1    # "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    if-eqz v1, :cond_0

    .line 294
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {v1, v2}, Lcom/google/android/music/widgets/ExpandingScrollView;->moveToExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    goto :goto_0

    .line 297
    .end local v1    # "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    :cond_5
    const-string v2, "com.google.android.music.OPEN_DRAWER"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsStarted:Z
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$400(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 300
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$600(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/google/android/music/ui/NowPlayingScreenFragment$3$1;

    invoke-direct {v3, p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment$3$1;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment$3;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
