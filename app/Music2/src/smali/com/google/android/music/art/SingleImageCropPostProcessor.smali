.class public Lcom/google/android/music/art/SingleImageCropPostProcessor;
.super Ljava/lang/Object;
.source "SingleImageCropPostProcessor.java"

# interfaces
.implements Lcom/google/android/music/art/ArtPostProcessor;


# static fields
.field private static final sInitializationLock:Ljava/lang/Object;

.field private static sInstance:Lcom/google/android/music/art/SingleImageCropPostProcessor;


# instance fields
.field private final mAntiAliasPaint:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/music/art/SingleImageCropPostProcessor;->sInitializationLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/music/art/SingleImageCropPostProcessor;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 23
    return-void
.end method

.method private checkRequestAndImages(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "images"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .prologue
    const/4 v0, 0x0

    .line 103
    const-string v1, "images must not be null"

    invoke-static {p2, v1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    array-length v1, p2

    if-lez v1, :cond_0

    aget-object v1, p2, v0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "images must contain at least one image."

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 106
    const-string v0, "request must not be null"

    invoke-static {p1, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    return-void
.end method

.method public static getInstance()Lcom/google/android/music/art/SingleImageCropPostProcessor;
    .locals 2

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/music/art/SingleImageCropPostProcessor;->sInstance:Lcom/google/android/music/art/SingleImageCropPostProcessor;

    if-nez v0, :cond_1

    .line 27
    sget-object v1, Lcom/google/android/music/art/SingleImageCropPostProcessor;->sInitializationLock:Ljava/lang/Object;

    monitor-enter v1

    .line 28
    :try_start_0
    sget-object v0, Lcom/google/android/music/art/SingleImageCropPostProcessor;->sInstance:Lcom/google/android/music/art/SingleImageCropPostProcessor;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/google/android/music/art/SingleImageCropPostProcessor;

    invoke-direct {v0}, Lcom/google/android/music/art/SingleImageCropPostProcessor;-><init>()V

    sput-object v0, Lcom/google/android/music/art/SingleImageCropPostProcessor;->sInstance:Lcom/google/android/music/art/SingleImageCropPostProcessor;

    .line 31
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :cond_1
    sget-object v0, Lcom/google/android/music/art/SingleImageCropPostProcessor;->sInstance:Lcom/google/android/music/art/SingleImageCropPostProcessor;

    return-object v0

    .line 31
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public aliasPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .locals 2
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "images"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .prologue
    .line 95
    sget-boolean v0, Lcom/google/android/music/art/ArtResolver2;->LOGV:Z

    if-eqz v0, :cond_0

    .line 96
    const-string v0, "ArtResolver"

    const-string v1, "Single Image Processor: Using alias mode"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/SingleImageCropPostProcessor;->checkRequestAndImages(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V

    .line 99
    const/4 v0, 0x0

    aget-object v0, p2, v0

    return-object v0
.end method

.method public getConfig(Lcom/google/android/music/art/ArtDescriptor;)Landroid/graphics/Bitmap$Config;
    .locals 1
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 121
    sget-object v0, Lcom/google/android/music/art/ArtResolver2;->DEFAULT_CONFIG:Landroid/graphics/Bitmap$Config;

    return-object v0
.end method

.method public getMaxNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I
    .locals 1
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 111
    const/4 v0, 0x1

    return v0
.end method

.method public getMinNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I
    .locals 1
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 116
    const/4 v0, 0x1

    return v0
.end method

.method public getPostProcessingMode(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)I
    .locals 8
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "images"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .prologue
    const/4 v7, 0x0

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/SingleImageCropPostProcessor;->checkRequestAndImages(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V

    .line 39
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->getDescriptor()Lcom/google/android/music/art/ArtDescriptor;

    move-result-object v0

    .line 40
    .local v0, "descriptor":Lcom/google/android/music/art/ArtDescriptor;
    invoke-virtual {v0}, Lcom/google/android/music/art/ArtDescriptor;->getBucketSize()I

    move-result v4

    .line 41
    .local v4, "requestedWidth":I
    iget v5, v0, Lcom/google/android/music/art/ArtDescriptor;->aspectRatio:F

    int-to-float v6, v4

    mul-float/2addr v5, v6

    float-to-int v3, v5

    .line 42
    .local v3, "requestedHeight":I
    aget-object v5, p2, v7

    iget-object v5, v5, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 43
    .local v2, "givenWidth":I
    aget-object v5, p2, v7

    iget-object v5, v5, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 44
    .local v1, "givenHeight":I
    sget-boolean v5, Lcom/google/android/music/art/ArtResolver2;->LOGV:Z

    if-eqz v5, :cond_0

    .line 45
    const-string v5, "ArtResolver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Single Image Processor: dimensions requested="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", given="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_0
    int-to-float v5, v4

    int-to-float v6, v2

    invoke-virtual {v0, v5, v6}, Lcom/google/android/music/art/ArtDescriptor;->checkSizeAcceptable(FF)Z

    move-result v5

    if-eqz v5, :cond_1

    int-to-float v5, v3

    int-to-float v6, v1

    invoke-virtual {v0, v5, v6}, Lcom/google/android/music/art/ArtDescriptor;->checkSizeAcceptable(FF)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 51
    const/4 v5, 0x2

    .line 53
    :goto_0
    return v5

    :cond_1
    const/4 v5, 0x1

    goto :goto_0
.end method

.method public renderPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;Landroid/graphics/Bitmap;)Z
    .locals 12
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "images"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .param p3, "result"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v11, 0x0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/SingleImageCropPostProcessor;->checkRequestAndImages(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V

    .line 61
    const-string v9, "result must not be null"

    invoke-static {p3, v9}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v9

    const-string v10, "result must be a mutable bitmap."

    invoke-static {v9, v10}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 64
    sget-boolean v9, Lcom/google/android/music/art/ArtResolver2;->LOGV:Z

    if-eqz v9, :cond_0

    .line 65
    const-string v9, "ArtResolver"

    const-string v10, "Single Image Processor: Using render mode"

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_0
    aget-object v0, p2, v11

    .line 71
    .local v0, "bitmap":Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    iget-object v5, v0, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    .line 73
    .local v5, "src":Landroid/graphics/Bitmap;
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 74
    .local v4, "destWidth":I
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 75
    .local v2, "destHeight":I
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    .line 76
    .local v8, "srcWidth":I
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 78
    .local v6, "srcHeight":I
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 79
    .local v7, "srcRect":Landroid/graphics/Rect;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 81
    .local v3, "destRect":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, p3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 84
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-static {v7, v8, v6, v4, v2}, Lcom/google/android/music/art/ArtRenderingUtils;->calculateSourceSlice(Landroid/graphics/Rect;IIII)V

    .line 86
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-virtual {v3, v11, v11, v9, v10}, Landroid/graphics/Rect;->set(IIII)V

    .line 88
    iget-object v9, p0, Lcom/google/android/music/art/SingleImageCropPostProcessor;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5, v7, v3, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 89
    const/4 v9, 0x1

    return v9
.end method
