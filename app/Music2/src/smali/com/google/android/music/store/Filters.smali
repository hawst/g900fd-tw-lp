.class Lcom/google/android/music/store/Filters;
.super Ljava/lang/Object;
.source "Filters.java"


# static fields
.field private static final FILTERS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 57
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "LocalCopyType IN (200,300)"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "LocalCopyType IN (100,200,300)"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Domain=0"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "+Domain=0 AND LocalCopyType IN (200,300)"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "+Domain=0 AND LocalCopyType IN (100,200,300)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/Filters;->FILTERS:[Ljava/lang/String;

    return-void
.end method

.method static appendUserAllFilter(Z)Ljava/lang/String;
    .locals 1
    .param p0, "suppressDomainIndex"    # Z

    .prologue
    .line 261
    if-eqz p0, :cond_0

    .line 262
    const-string v0, "+Domain=0"

    .line 264
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Domain=0"

    goto :goto_0
.end method

.method static doesExcludeOnlineMusic(I)Z
    .locals 3
    .param p0, "filterIndex"    # I

    .prologue
    .line 222
    packed-switch p0, :pswitch_data_0

    .line 232
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown filter value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225
    :pswitch_0
    const/4 v0, 0x0

    .line 230
    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 222
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static getCurrentVolumesFilter()Ljava/lang/String;
    .locals 8

    .prologue
    .line 237
    invoke-static {}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstanceNoContext()Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v3

    .line 238
    .local v3, "manager":Lcom/google/android/music/download/cache/CacheLocationManager;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 239
    .local v4, "result":Ljava/lang/StringBuilder;
    if-eqz v3, :cond_2

    .line 240
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 241
    .local v1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v3}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownUsableLocations()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/download/cache/CacheLocation;

    .line 242
    .local v2, "location":Lcom/google/android/music/download/cache/CacheLocation;
    invoke-virtual {v2}, Lcom/google/android/music/download/cache/CacheLocation;->getVolumeId()Ljava/util/UUID;

    move-result-object v5

    .line 243
    .local v5, "volumeId":Ljava/util/UUID;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 245
    .end local v2    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    .end local v5    # "volumeId":Ljava/util/UUID;
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 246
    const-string v6, "LocalCopyStorageVolumeId"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    invoke-static {v4, v1}, Lcom/google/android/music/utils/DbUtils;->stringAppendIN(Ljava/lang/StringBuilder;Ljava/util/Collection;)V

    .line 252
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    :goto_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 249
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    const-string v6, "MusicFilters"

    const-string v7, "Volume list is empty. This shouldn\'t ever happen."

    invoke-static {v6, v7}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getFilter(I)Ljava/lang/String;
    .locals 6
    .param p0, "index"    # I

    .prologue
    .line 74
    sget-object v4, Lcom/google/android/music/store/Filters;->FILTERS:[Ljava/lang/String;

    aget-object v0, v4, p0

    .line 75
    .local v0, "baseFilter":Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/music/store/Filters;->shouldApplyVolumeFilter(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 76
    invoke-static {}, Lcom/google/android/music/store/Filters;->getCurrentVolumesFilter()Ljava/lang/String;

    move-result-object v3

    .line 77
    .local v3, "volumeFilter":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 78
    .local v1, "builder":Ljava/lang/StringBuilder;
    const/16 v4, 0x28

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 79
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "LocalCopyType=300"

    invoke-static {v4, v5}, Lcom/google/android/music/utils/DbUtils;->addOrCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, "orCondition":Ljava/lang/String;
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    const/16 v4, 0x29

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 85
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/utils/DbUtils;->addAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 88
    .end local v0    # "baseFilter":Ljava/lang/String;
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "orCondition":Ljava/lang/String;
    .end local v3    # "volumeFilter":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static getFilterWithoutVolumeCondition(I)Ljava/lang/String;
    .locals 1
    .param p0, "index"    # I

    .prologue
    .line 99
    sget-object v0, Lcom/google/android/music/store/Filters;->FILTERS:[Ljava/lang/String;

    aget-object v0, v0, p0

    return-object v0
.end method

.method static getMusicFilterIndex(Landroid/content/Context;ZZ)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shouldFilter"    # Z
    .param p2, "includeExternal"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 145
    const/4 v4, 0x1

    .line 146
    .local v4, "showRemote":Z
    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    .line 147
    .local v3, "prefsHolder":Ljava/lang/Object;
    invoke-static {p0, v3}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    .line 151
    .local v2, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isCachedStreamingMusicEnabled()Z

    move-result v0

    .line 152
    .local v0, "cacheStreamedMusic":Z
    if-eqz p1, :cond_0

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-nez v7, :cond_2

    :cond_0
    move v4, v6

    .line 154
    :goto_0
    invoke-static {v3}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 157
    if-nez p2, :cond_3

    move v1, v6

    .line 159
    .local v1, "filterExternal":Z
    :goto_1
    if-nez v4, :cond_7

    .line 160
    if-eqz v0, :cond_5

    .line 161
    if-eqz v1, :cond_4

    const/4 v5, 0x5

    .line 167
    :cond_1
    :goto_2
    return v5

    .end local v1    # "filterExternal":Z
    :cond_2
    move v4, v5

    .line 152
    goto :goto_0

    .line 154
    .end local v0    # "cacheStreamedMusic":Z
    :catchall_0
    move-exception v5

    invoke-static {v3}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v5

    .restart local v0    # "cacheStreamedMusic":Z
    :cond_3
    move v1, v5

    .line 157
    goto :goto_1

    .line 161
    .restart local v1    # "filterExternal":Z
    :cond_4
    const/4 v5, 0x2

    goto :goto_2

    .line 164
    :cond_5
    if-eqz v1, :cond_6

    const/4 v6, 0x4

    :cond_6
    move v5, v6

    goto :goto_2

    .line 167
    :cond_7
    if-eqz v1, :cond_1

    const/4 v5, 0x3

    goto :goto_2
.end method

.method static setExternalFiltering(IZ)I
    .locals 3
    .param p0, "filterIndex"    # I
    .param p1, "allowExternal"    # Z

    .prologue
    .line 186
    if-eqz p1, :cond_0

    .line 187
    packed-switch p0, :pswitch_data_0

    .line 214
    :goto_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown filter value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :pswitch_0
    const/4 p0, 0x0

    .line 211
    .end local p0    # "filterIndex":I
    :goto_1
    :pswitch_1
    return p0

    .line 196
    .restart local p0    # "filterIndex":I
    :pswitch_2
    const/4 p0, 0x1

    goto :goto_1

    .line 198
    :pswitch_3
    const/4 p0, 0x2

    goto :goto_1

    .line 201
    :cond_0
    packed-switch p0, :pswitch_data_1

    goto :goto_0

    .line 203
    :pswitch_4
    const/4 p0, 0x3

    goto :goto_1

    .line 205
    :pswitch_5
    const/4 p0, 0x4

    goto :goto_1

    .line 207
    :pswitch_6
    const/4 p0, 0x5

    goto :goto_1

    .line 187
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 201
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private static shouldApplyVolumeFilter(I)Z
    .locals 1
    .param p0, "index"    # I

    .prologue
    .line 106
    packed-switch p0, :pswitch_data_0

    .line 112
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 111
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
