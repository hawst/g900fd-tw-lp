.class Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;
.super Landroid/support/v17/leanback/widget/ObjectAdapter;
.source "LeanbackExploreGenresActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "GenreItemsObjectAdapter"
.end annotation


# instance fields
.field private final mFeaturedAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mFeaturedAdapterStartIndex:I

.field private final mNewReleasesAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mNewReleasesAdapterStartIndex:I

.field private final mRadioItem:Lcom/google/android/music/leanback/Item;

.field private mSize:I

.field private mSubgenreItem:Lcom/google/android/music/leanback/Item;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/ItemPresenter;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/ObjectAdapter;)V
    .locals 2
    .param p1, "itemPresenter"    # Lcom/google/android/music/leanback/ItemPresenter;
    .param p2, "radioItem"    # Lcom/google/android/music/leanback/Item;
    .param p3, "featuredAdapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;
    .param p4, "newReleasesAdapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 299
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 300
    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mRadioItem:Lcom/google/android/music/leanback/Item;

    .line 301
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mSubgenreItem:Lcom/google/android/music/leanback/Item;

    .line 302
    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mFeaturedAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 303
    iput-object p4, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mNewReleasesAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 304
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mFeaturedAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    new-instance v1, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$1;-><init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)V

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 328
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mNewReleasesAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    new-instance v1, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$2;

    invoke-direct {v1, p0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$2;-><init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)V

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 355
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->recalculateSizeAndIndices()V

    .line 356
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyItemRangeInserted(II)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyItemRangeRemoved(II)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyChanged()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    .prologue
    .line 287
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mNewReleasesAdapterStartIndex:I

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyItemRangeInserted(II)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyItemRangeRemoved(II)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    .prologue
    .line 287
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->recalculateSizeAndIndices()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyChanged()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    .prologue
    .line 287
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mFeaturedAdapterStartIndex:I

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method private recalculateSizeAndIndices()V
    .locals 2

    .prologue
    .line 403
    const/4 v0, 0x0

    .line 404
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mRadioItem:Lcom/google/android/music/leanback/Item;

    if-eqz v1, :cond_0

    .line 405
    add-int/lit8 v0, v0, 0x1

    .line 407
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mSubgenreItem:Lcom/google/android/music/leanback/Item;

    if-eqz v1, :cond_1

    .line 408
    add-int/lit8 v0, v0, 0x1

    .line 410
    :cond_1
    iput v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mFeaturedAdapterStartIndex:I

    .line 411
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mFeaturedAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 412
    iput v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mNewReleasesAdapterStartIndex:I

    .line 413
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mNewReleasesAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 414
    iput v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mSize:I

    .line 415
    return-void
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 382
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mSize:I

    if-ge p1, v0, :cond_3

    .line 383
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mNewReleasesAdapterStartIndex:I

    if-lt p1, v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mNewReleasesAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mNewReleasesAdapterStartIndex:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 395
    :goto_0
    return-object v0

    .line 387
    :cond_0
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mFeaturedAdapterStartIndex:I

    if-lt p1, v0, :cond_1

    .line 388
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mFeaturedAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mFeaturedAdapterStartIndex:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 391
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 392
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mSubgenreItem:Lcom/google/android/music/leanback/Item;

    goto :goto_0

    .line 394
    :cond_2
    if-nez p1, :cond_3

    .line 395
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mRadioItem:Lcom/google/android/music/leanback/Item;

    goto :goto_0

    .line 398
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tried to get unexpected position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setSubgenreItem(Lcom/google/android/music/leanback/Item;)V
    .locals 2
    .param p1, "item"    # Lcom/google/android/music/leanback/Item;

    .prologue
    const/4 v1, 0x1

    .line 359
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mSubgenreItem:Lcom/google/android/music/leanback/Item;

    .line 360
    .local v0, "previousSubgenreItem":Lcom/google/android/music/leanback/Item;
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mSubgenreItem:Lcom/google/android/music/leanback/Item;

    .line 361
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->recalculateSizeAndIndices()V

    .line 362
    if-eqz p1, :cond_2

    .line 363
    if-eqz v0, :cond_1

    .line 364
    invoke-virtual {p0, v1, v1}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyItemRangeChanged(II)V

    .line 373
    :cond_0
    :goto_0
    return-void

    .line 366
    :cond_1
    invoke-virtual {p0, v1, v1}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyItemRangeInserted(II)V

    goto :goto_0

    .line 369
    :cond_2
    if-eqz v0, :cond_0

    .line 370
    invoke-virtual {p0, v1, v1}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyItemRangeRemoved(II)V

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 377
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mSize:I

    return v0
.end method
