.class Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackMyGenresActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackMyGenresActivity;->getListRowsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackMyGenresActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackMyGenresActivity;Landroid/support/v17/leanback/widget/Presenter;)V
    .locals 0
    .param p2, "x0"    # Landroid/support/v17/leanback/widget/Presenter;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackMyGenresActivity;

    invoke-direct {p0, p2}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    return-void
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v7, 0x0

    .line 31
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 32
    .local v2, "id":J
    const/4 v4, 0x1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 34
    .local v1, "name":Ljava/lang/String;
    new-instance v0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1;

    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackMyGenresActivity;

    invoke-virtual {v4}, Lcom/google/android/music/leanback/LeanbackMyGenresActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v4

    invoke-direct {v0, p0, v4}, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1;-><init>(Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 71
    .local v0, "genreItemsAdapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackMyGenresActivity;

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    new-instance v6, Lcom/google/android/music/medialist/GenreAlbumList;

    invoke-direct {v6, v2, v3, v7}, Lcom/google/android/music/medialist/GenreAlbumList;-><init>(JZ)V

    sget-object v7, Lcom/google/android/music/ui/AlbumsAdapter;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {v4, v5, v0, v6, v7}, Lcom/google/android/music/leanback/LeanbackMyGenresActivity;->loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 74
    invoke-static {v1, v0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->createListRow(Ljava/lang/String;Landroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v4

    return-object v4
.end method
