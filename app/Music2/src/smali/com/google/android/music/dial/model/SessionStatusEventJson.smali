.class public Lcom/google/android/music/dial/model/SessionStatusEventJson;
.super Lcom/google/api/client/json/GenericJson;
.source "SessionStatusEventJson.java"


# instance fields
.field public mCustomData:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "customData"
    .end annotation
.end field

.field public mSessionCreated:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "sessionCreated"
    .end annotation
.end field

.field public mSessionId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "sessionId"
    .end annotation
.end field

.field public mSessionState:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "sessionState"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
