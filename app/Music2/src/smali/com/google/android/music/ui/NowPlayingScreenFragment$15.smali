.class Lcom/google/android/music/ui/NowPlayingScreenFragment$15;
.super Ljava/lang/Object;
.source "NowPlayingScreenFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/NowPlayingScreenFragment;->handlePageSelected(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;I)V
    .locals 0

    .prologue
    .line 1833
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$15;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    iput p2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$15;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1837
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$15;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1700(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$15;->val$position:I

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$15;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1700(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->getQueuePosition()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 1838
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$15;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1700(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$15;->val$position:I

    invoke-interface {v1, v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->setQueuePosition(I)V

    .line 1845
    :cond_0
    :goto_0
    return-void

    .line 1839
    :cond_1
    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->DEBUG_QUEUE:Z
    invoke-static {}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1800()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1840
    const-string v1, "NowPlayingFragment"

    const-string v2, "Setting to the same position. No-op."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1842
    :catch_0
    move-exception v0

    .line 1843
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "NowPlayingFragment"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
