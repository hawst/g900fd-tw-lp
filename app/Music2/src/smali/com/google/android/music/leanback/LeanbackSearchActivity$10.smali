.class Lcom/google/android/music/leanback/LeanbackSearchActivity$10;
.super Ljava/lang/Object;
.source "LeanbackSearchActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackSearchActivity;->handleSearchResult(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final cols:[Ljava/lang/String;

.field mPlaylistArtUrl:Ljava/lang/String;

.field mPlaylistName:Ljava/lang/String;

.field mPlaylistType:I

.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

.field final synthetic val$playlistId:J

.field final synthetic val$shouldAutoPlay:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;JZ)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 558
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    iput-wide p2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->val$playlistId:J

    iput-boolean p4, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->val$shouldAutoPlay:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "playlist_name"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "playlist_type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "playlist_art_url"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->cols:[Ljava/lang/String;

    .line 566
    iput-object v4, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->mPlaylistName:Ljava/lang/String;

    .line 567
    iput v3, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->mPlaylistType:I

    .line 568
    iput-object v4, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->mPlaylistArtUrl:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 572
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    iget-wide v4, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->val$playlistId:J

    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->cols:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 576
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 577
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->mPlaylistName:Ljava/lang/String;

    .line 578
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->mPlaylistType:I

    .line 579
    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->mPlaylistArtUrl:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 582
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 584
    return-void

    .line 582
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public taskCompleted()V
    .locals 12

    .prologue
    const/4 v6, 0x0

    .line 588
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    iget-wide v4, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->val$playlistId:J

    invoke-static {v2, v4, v5}, Lcom/google/android/music/leanback/LeanbackUtils;->getPlaylistDetailsIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 592
    iget-boolean v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->val$shouldAutoPlay:Z

    if-eqz v0, :cond_0

    .line 593
    new-instance v1, Lcom/google/android/music/medialist/PlaylistSongList;

    iget-wide v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->val$playlistId:J

    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->mPlaylistName:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->mPlaylistType:I

    iget-object v10, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->mPlaylistArtUrl:Ljava/lang/String;

    const/4 v11, 0x0

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    invoke-direct/range {v1 .. v11}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 596
    .local v1, "songlist":Lcom/google/android/music/medialist/SongList;
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V

    .line 598
    .end local v1    # "songlist":Lcom/google/android/music/medialist/SongList;
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->finish()V

    .line 599
    return-void
.end method
