.class public Lcom/google/android/music/ui/cardlib/layout/accessible/AccessibleListView;
.super Landroid/widget/ListView;
.source "AccessibleListView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    .line 41
    .local v1, "populated":Z
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/accessible/AccessibleListView;->getSelectedView()Landroid/view/View;

    move-result-object v3

    .line 42
    .local v3, "selectedView":Landroid/view/View;
    if-eqz v3, :cond_1

    .line 44
    invoke-virtual {v3}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 45
    .local v0, "focusOwner":Landroid/view/View;
    if-nez v0, :cond_0

    move v2, v1

    .line 55
    .end local v0    # "focusOwner":Landroid/view/View;
    .end local v1    # "populated":Z
    .local v2, "populated":Z
    :goto_0
    return v2

    .line 50
    .end local v2    # "populated":Z
    .restart local v0    # "focusOwner":Landroid/view/View;
    .restart local v1    # "populated":Z
    :cond_0
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 52
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    .end local v0    # "focusOwner":Landroid/view/View;
    :cond_1
    move v2, v1

    .line 55
    .end local v1    # "populated":Z
    .restart local v2    # "populated":Z
    goto :goto_0
.end method
