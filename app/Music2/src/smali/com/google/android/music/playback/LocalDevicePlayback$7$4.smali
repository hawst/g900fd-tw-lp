.class Lcom/google/android/music/playback/LocalDevicePlayback$7$4;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback$7;->onCloudQueueSessionInitialized(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

.field final synthetic val$shouldSuppressPlaybackRequests:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback$7;Z)V
    .locals 0

    .prologue
    .line 822
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iput-boolean p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->val$shouldSuppressPlaybackRequests:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 825
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mCloudQueueSessionLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3200(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9

    .line 826
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 827
    const-string v0, "LocalDevicePlayback"

    const-string v1, "Moving local playback to the connected receiver"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 832
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->val$shouldSuppressPlaybackRequests:Z

    if-eqz v0, :cond_1

    .line 833
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->stop()V

    .line 834
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->resetCurrentSongMetaDataCursor()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3300(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 844
    :try_start_2
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mWasCloudQueuePlaybackRequested:Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3402(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    .line 845
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSwappingToCloudQueue:Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3602(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 848
    :goto_0
    return-void

    .line 838
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->position()J

    move-result-wide v6

    .line 839
    .local v6, "position":J
    :goto_1
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v1, v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mWasCloudQueuePlaybackRequested:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3400(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v3

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mWasCloudQueuePlaybackRequested:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3400(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v4

    const/4 v5, 0x0

    const/4 v8, 0x0

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndNext(ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    invoke-static/range {v1 .. v8}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3500(Lcom/google/android/music/playback/LocalDevicePlayback;ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 844
    :try_start_4
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mWasCloudQueuePlaybackRequested:Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3402(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    .line 845
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSwappingToCloudQueue:Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3602(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    .line 847
    monitor-exit v9

    goto :goto_0

    .end local v6    # "position":J
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 838
    :cond_2
    :try_start_5
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->position()J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-wide v6

    goto :goto_1

    .line 844
    :catchall_1
    move-exception v0

    :try_start_6
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mWasCloudQueuePlaybackRequested:Z
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3402(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    .line 845
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSwappingToCloudQueue:Z
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3602(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method
