.class Lcom/google/android/music/utils/RouteChecker$PackageEntry;
.super Ljava/lang/Object;
.source "RouteChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/RouteChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PackageEntry"
.end annotation


# instance fields
.field private final mDigest:Lcom/google/android/music/utils/RouteChecker$SHA1;

.field private final mHashCode:I

.field private final mName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/utils/RouteChecker;


# direct methods
.method public constructor <init>(Lcom/google/android/music/utils/RouteChecker;Ljava/lang/String;Lcom/google/android/music/utils/RouteChecker$SHA1;)V
    .locals 2
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "digest"    # Lcom/google/android/music/utils/RouteChecker$SHA1;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->this$0:Lcom/google/android/music/utils/RouteChecker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-object p2, p0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->mName:Ljava/lang/String;

    .line 120
    iput-object p3, p0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->mDigest:Lcom/google/android/music/utils/RouteChecker$SHA1;

    .line 121
    iget-object v0, p0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->mName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->mDigest:Lcom/google/android/music/utils/RouteChecker$SHA1;

    invoke-virtual {v1}, Lcom/google/android/music/utils/RouteChecker$SHA1;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->mHashCode:I

    .line 122
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 126
    instance-of v2, p1, Lcom/google/android/music/utils/RouteChecker$PackageEntry;

    if-nez v2, :cond_1

    .line 133
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 129
    check-cast v0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;

    .line 130
    .local v0, "otherPackageEntry":Lcom/google/android/music/utils/RouteChecker$PackageEntry;
    iget v2, p0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->mHashCode:I

    iget v3, v0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->mHashCode:I

    if-ne v2, v3, :cond_0

    .line 133
    iget-object v2, p0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->mName:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->mDigest:Lcom/google/android/music/utils/RouteChecker$SHA1;

    iget-object v3, v0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->mDigest:Lcom/google/android/music/utils/RouteChecker$SHA1;

    invoke-virtual {v2, v3}, Lcom/google/android/music/utils/RouteChecker$SHA1;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->mHashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", digest: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/utils/RouteChecker$PackageEntry;->mDigest:Lcom/google/android/music/utils/RouteChecker$SHA1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
