.class Lcom/google/android/music/ui/ContainerHeaderView$2$2;
.super Ljava/lang/Object;
.source "ContainerHeaderView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ContainerHeaderView$2;->taskCompleted()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ContainerHeaderView$2;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$2;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$2;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    iget-object v0, v0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistId:J
    invoke-static {v0}, Lcom/google/android/music/ui/ContainerHeaderView;->access$400(Lcom/google/android/music/ui/ContainerHeaderView;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 415
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$2;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    iget-object v0, v0, Lcom/google/android/music/ui/ContainerHeaderView$2;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$2;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    iget-object v1, v1, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistId:J
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$400(Lcom/google/android/music/ui/ContainerHeaderView;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$2;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    iget-object v1, v1, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSecondaryTitle:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$300(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v1, v4}, Lcom/google/android/music/ui/ArtistPageActivity;->showArtist(Landroid/content/Context;JLjava/lang/String;Z)V

    .line 421
    :cond_0
    :goto_0
    return-void

    .line 417
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$2;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    iget-object v0, v0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistMetajamId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/ui/ContainerHeaderView;->access$500(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$2;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    iget-object v0, v0, Lcom/google/android/music/ui/ContainerHeaderView$2;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$2;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    iget-object v1, v1, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistMetajamId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$500(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$2;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    iget-object v2, v2, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSecondaryTitle:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$300(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/music/ui/ArtistPageActivity;->showNautilusArtist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
