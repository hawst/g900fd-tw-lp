.class public Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;
.super Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
.source "MaterialMainstagePlayCard.java"

# interfaces
.implements Lcom/google/android/music/ui/ShareableElement;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard$2;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field protected mAvatarView:Lcom/google/android/music/art/SimpleArtView;

.field protected mImageOverlay:Landroid/view/View;

.field protected mImageView:Lcom/google/android/music/art/MainstageArtView;

.field protected mPlayButton:Landroid/widget/ImageView;

.field protected mRadioIcon:Landroid/widget/ImageView;

.field protected mTitleBackground:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    return-void
.end method

.method private getAspectRatio(Lcom/google/android/music/ui/cardlib/model/Document;)F
    .locals 5
    .param p1, "document"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 85
    if-nez p1, :cond_1

    .line 98
    :cond_0
    :goto_0
    return v1

    .line 86
    :cond_1
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard$2;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 88
    :pswitch_0
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioSeedType()I

    move-result v0

    .line 89
    .local v0, "seedType":I
    const/4 v3, 0x4

    if-eq v0, v3, :cond_2

    const/4 v3, 0x7

    if-ne v0, v3, :cond_0

    :cond_2
    move v1, v2

    .line 91
    goto :goto_0

    .end local v0    # "seedType":I
    :pswitch_1
    move v1, v2

    .line 96
    goto :goto_0

    .line 86
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getBackgroundColor()I
    .locals 2

    .prologue
    .line 412
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method private getRadioIconResId()I
    .locals 2

    .prologue
    .line 302
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 303
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f02004e

    :goto_0
    return v1

    :cond_0
    const v1, 0x7f02004d

    goto :goto_0
.end method

.method private getSharedElementPadding()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 420
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 421
    .local v0, "padding":Landroid/graphics/RectF;
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 422
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 423
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingRight()I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 424
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingBottom()I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 425
    return-object v0
.end method


# virtual methods
.method public bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V
    .locals 9
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p2, "contextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 310
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 311
    if-nez p1, :cond_0

    .line 361
    :goto_0
    return-void

    .line 315
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getAspectRatio(Lcom/google/android/music/ui/cardlib/model/Document;)F

    move-result v1

    .line 316
    .local v1, "aspect":F
    iget v4, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mThumbnailAspectRatio:F

    sub-float v4, v1, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const v5, 0x3c23d70a    # 0.01f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    .line 317
    iput v1, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mThumbnailAspectRatio:F

    .line 318
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageView:Lcom/google/android/music/art/MainstageArtView;

    invoke-virtual {v4, v1}, Lcom/google/android/music/art/MainstageArtView;->setAspectRatio(F)V

    .line 321
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageView:Lcom/google/android/music/art/MainstageArtView;

    sget-object v5, Lcom/google/android/music/art/ArtType;->MAINSTAGE_CARD:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v4, p1, v5}, Lcom/google/android/music/art/MainstageArtView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/art/ArtType;)V

    .line 323
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v4, v5, :cond_2

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v4

    const/16 v5, 0x32

    if-ne v4, v5, :cond_3

    .line 325
    :cond_2
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageOverlay:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 326
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mRadioIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 334
    :goto_1
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mPlayButton:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 336
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mAvatarView:Lcom/google/android/music/art/SimpleArtView;

    invoke-virtual {v4, v6}, Lcom/google/android/music/art/SimpleArtView;->setVisibility(I)V

    .line 337
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getProfilePhotoUrl()Ljava/lang/String;

    move-result-object v3

    .line 338
    .local v3, "url":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 339
    new-instance v0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    invoke-direct {v0, v3, v7}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;-><init>(Ljava/lang/String;Z)V

    .line 340
    .local v0, "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    new-instance v2, Lcom/google/android/music/art/SingleUrlArtDescriptor;

    sget-object v4, Lcom/google/android/music/art/ArtType;->AVATAR:Lcom/google/android/music/art/ArtType;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v2, v4, v7, v5, v0}, Lcom/google/android/music/art/SingleUrlArtDescriptor;-><init>(Lcom/google/android/music/art/ArtType;IFLcom/google/android/music/art/ArtResolver2$ArtUrl;)V

    .line 342
    .local v2, "descriptor":Lcom/google/android/music/art/SingleUrlArtDescriptor;
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mAvatarView:Lcom/google/android/music/art/SimpleArtView;

    invoke-virtual {v4, v2, v7}, Lcom/google/android/music/art/SimpleArtView;->bind(Lcom/google/android/music/art/ArtDescriptor;Z)V

    goto :goto_0

    .line 329
    .end local v0    # "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    .end local v2    # "descriptor":Lcom/google/android/music/art/SingleUrlArtDescriptor;
    .end local v3    # "url":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageOverlay:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 330
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mRadioIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 348
    .restart local v3    # "url":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mAvatarView:Lcom/google/android/music/art/SimpleArtView;

    new-instance v5, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard$1;

    invoke-direct {v5, p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard$1;-><init>(Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;)V

    invoke-virtual {v4, v5}, Lcom/google/android/music/art/SimpleArtView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public getSnapshotElement()Landroid/view/View;
    .locals 1

    .prologue
    .line 394
    const v0, 0x7f0e017f

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getTransitionInfo()Lcom/google/android/music/ui/TransitionInfo;
    .locals 2

    .prologue
    .line 401
    new-instance v0, Lcom/google/android/music/ui/AlbumTransitionInfo;

    invoke-direct {v0}, Lcom/google/android/music/ui/AlbumTransitionInfo;-><init>()V

    .line 402
    .local v0, "info":Lcom/google/android/music/ui/AlbumTransitionInfo;
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getBackgroundColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/AlbumTransitionInfo;->setBackgroundColor(I)V

    .line 403
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getSharedElementPadding()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/AlbumTransitionInfo;->setSharedElementPadding(Landroid/graphics/RectF;)V

    .line 405
    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 67
    invoke-super {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->onFinishInflate()V

    .line 68
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mDescription:Landroid/widget/TextView;

    const-string v1, "Description view must not be null"

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    const v0, 0x7f0e0106

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageOverlay:Landroid/view/View;

    .line 70
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageOverlay:Landroid/view/View;

    const v1, 0x3f19999a    # 0.6f

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 72
    const v0, 0x7f0e0181

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mTitleBackground:Landroid/view/View;

    .line 73
    const v0, 0x7f0e017f

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/art/MainstageArtView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageView:Lcom/google/android/music/art/MainstageArtView;

    .line 74
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageView:Lcom/google/android/music/art/MainstageArtView;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageOverlay:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mTitleBackground:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/art/MainstageArtView;->setViews(Landroid/view/View;Landroid/view/View;)V

    .line 76
    const v0, 0x7f0e0107

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mRadioIcon:Landroid/widget/ImageView;

    .line 77
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mRadioIcon:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getRadioIconResId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 79
    const v0, 0x7f0e0180

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mPlayButton:Landroid/widget/ImageView;

    .line 81
    const v0, 0x7f0e017d

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/art/SimpleArtView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mAvatarView:Lcom/google/android/music/art/SimpleArtView;

    .line 82
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 46
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 192
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingLeft()I

    move-result v21

    .line 193
    .local v21, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingRight()I

    move-result v22

    .line 194
    .local v22, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingTop()I

    move-result v23

    .line 195
    .local v23, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingBottom()I

    move-result v20

    .line 197
    .local v20, "paddingBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageView:Lcom/google/android/music/art/MainstageArtView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/art/MainstageArtView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 198
    .local v8, "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v33

    check-cast v33, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 199
    .local v33, "reasonLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/KeepOnViewSmall;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 200
    .local v12, "keeponLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mTitleBackground:Landroid/view/View;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v35

    check-cast v35, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 202
    .local v35, "titleAreaLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageView:Lcom/google/android/music/art/MainstageArtView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/art/MainstageArtView;->getMeasuredHeight()I

    move-result v6

    .line 203
    .local v6, "imageHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageView:Lcom/google/android/music/art/MainstageArtView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/art/MainstageArtView;->getMeasuredWidth()I

    move-result v10

    .line 204
    .local v10, "imageWidth":I
    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v40, v0

    add-int v7, v21, v40

    .line 205
    .local v7, "imageLeft":I
    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v40, v0

    add-int v9, v23, v40

    .line 206
    .local v9, "imageTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageView:Lcom/google/android/music/art/MainstageArtView;

    move-object/from16 v40, v0

    add-int v41, v7, v10

    add-int v42, v9, v6

    move-object/from16 v0, v40

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-virtual {v0, v7, v9, v1, v2}, Lcom/google/android/music/art/MainstageArtView;->layout(IIII)V

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageOverlay:Landroid/view/View;

    move-object/from16 v40, v0

    add-int v41, v7, v10

    add-int v42, v9, v6

    move-object/from16 v0, v40

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-virtual {v0, v7, v9, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 212
    add-int v14, v6, v9

    .line 213
    .local v14, "offsetHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mTitleBackground:Landroid/view/View;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/view/View;->getMeasuredHeight()I

    move-result v36

    .line 214
    .local v36, "titleHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mTitleBackground:Landroid/view/View;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/view/View;->getMeasuredWidth()I

    move-result v39

    .line 215
    .local v39, "titleWidth":I
    move-object/from16 v0, v35

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v40, v0

    add-int v37, v21, v40

    .line 216
    .local v37, "titleLeft":I
    move/from16 v38, v14

    .line 217
    .local v38, "titleTop":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mThumbnailAspectRatio:F

    move/from16 v40, v0

    const/high16 v41, 0x3f000000    # 0.5f

    sub-float v40, v40, v41

    invoke-static/range {v40 .. v40}, Ljava/lang/Math;->abs(F)F

    move-result v40

    const v41, 0x3c23d70a    # 0.01f

    cmpl-float v40, v40, v41

    if-ltz v40, :cond_4

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mTitleBackground:Landroid/view/View;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/view/View;->getMeasuredHeight()I

    move-result v40

    sub-int v38, v14, v40

    .line 226
    :goto_0
    if-lez v38, :cond_0

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mTitleBackground:Landroid/view/View;

    move-object/from16 v40, v0

    add-int v41, v37, v39

    add-int v42, v38, v36

    move-object/from16 v0, v40

    move/from16 v1, v37

    move/from16 v2, v38

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 231
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    check-cast v17, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 232
    .local v17, "overflowLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->getMeasuredHeight()I

    move-result v15

    .line 233
    .local v15, "overflowHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->getMeasuredWidth()I

    move-result v19

    .line 234
    .local v19, "overflowWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mTitle:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/TextView;->getTop()I

    move-result v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mTitleBackground:Landroid/view/View;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->getTop()I

    move-result v41

    add-int v40, v40, v41

    move-object/from16 v0, v17

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v41, v0

    add-int v18, v40, v41

    .line 235
    .local v18, "overflowTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getMeasuredWidth()I

    move-result v40

    move-object/from16 v0, v17

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v41, v0

    sub-int v40, v40, v41

    sub-int v16, v40, v19

    .line 237
    .local v16, "overflowLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v40, v0

    add-int v41, v16, v19

    add-int v42, v18, v15

    move-object/from16 v0, v40

    move/from16 v1, v16

    move/from16 v2, v18

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->layout(IIII)V

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mPlayButton:Landroid/widget/ImageView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/ImageView;->getVisibility()I

    move-result v40

    const/16 v41, 0x8

    move/from16 v0, v40

    move/from16 v1, v41

    if-eq v0, v1, :cond_1

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mPlayButton:Landroid/widget/ImageView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v25

    check-cast v25, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 242
    .local v25, "playLp":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getMeasuredWidth()I

    move-result v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mPlayButton:Landroid/widget/ImageView;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v41

    sub-int v40, v40, v41

    sub-int v40, v40, v22

    move-object/from16 v0, v25

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v41, v0

    sub-int v24, v40, v41

    .line 244
    .local v24, "playLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mPlayButton:Landroid/widget/ImageView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v40

    sub-int v40, v38, v40

    move-object/from16 v0, v25

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v41, v0

    sub-int v26, v40, v41

    .line 245
    .local v26, "playTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mPlayButton:Landroid/widget/ImageView;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mPlayButton:Landroid/widget/ImageView;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v41

    add-int v41, v41, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mPlayButton:Landroid/widget/ImageView;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v42

    add-int v42, v42, v26

    move-object/from16 v0, v40

    move/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    .line 249
    .end local v24    # "playLeft":I
    .end local v25    # "playLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v26    # "playTop":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mRadioIcon:Landroid/widget/ImageView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/ImageView;->getVisibility()I

    move-result v40

    const/16 v41, 0x8

    move/from16 v0, v40

    move/from16 v1, v41

    if-eq v0, v1, :cond_2

    .line 250
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mRadioIcon:Landroid/widget/ImageView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v27

    .line 251
    .local v27, "radioHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mRadioIcon:Landroid/widget/ImageView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v30

    .line 252
    .local v30, "radioWidth":I
    sub-int v40, v6, v27

    div-int/lit8 v29, v40, 0x2

    .line 253
    .local v29, "radioTop":I
    sub-int v40, v10, v30

    div-int/lit8 v28, v40, 0x2

    .line 254
    .local v28, "radioLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mRadioIcon:Landroid/widget/ImageView;

    move-object/from16 v40, v0

    add-int v41, v28, v30

    add-int v42, v29, v27

    move-object/from16 v0, v40

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    .line 260
    .end local v27    # "radioHeight":I
    .end local v28    # "radioLeft":I
    .end local v29    # "radioTop":I
    .end local v30    # "radioWidth":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getVisibility()I

    move-result v40

    const/16 v41, 0x8

    move/from16 v0, v40

    move/from16 v1, v41

    if-eq v0, v1, :cond_3

    .line 261
    move-object/from16 v0, v33

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v40, v0

    add-int v34, v14, v40

    .line 262
    .local v34, "reasonTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getMeasuredHeight()I

    move-result v40

    add-int v31, v34, v40

    .line 263
    .local v31, "reasonBottom":I
    move-object/from16 v0, v33

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v40, v0

    add-int v32, v21, v40

    .line 264
    .local v32, "reasonLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getMeasuredWidth()I

    move-result v41

    add-int v41, v41, v32

    move-object/from16 v0, v40

    move/from16 v1, v32

    move/from16 v2, v34

    move/from16 v3, v41

    move/from16 v4, v31

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->layout(IIII)V

    .line 267
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getMeasuredHeight()I

    move-result v40

    sub-int v40, v40, v20

    iget v0, v12, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v41, v0

    sub-int v11, v40, v41

    .line 268
    .local v11, "keeponBottom":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getMeasuredWidth()I

    move-result v40

    sub-int v40, v40, v22

    iget v0, v12, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v41, v0

    sub-int v13, v40, v41

    .line 269
    .local v13, "keeponRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lcom/google/android/music/KeepOnViewSmall;->getMeasuredWidth()I

    move-result v41

    sub-int v41, v13, v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/google/android/music/KeepOnViewSmall;->getMeasuredHeight()I

    move-result v42

    sub-int v42, v11, v42

    move-object/from16 v0, v40

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2, v13, v11}, Lcom/google/android/music/KeepOnViewSmall;->layout(IIII)V

    .line 273
    .end local v11    # "keeponBottom":I
    .end local v13    # "keeponRight":I
    .end local v31    # "reasonBottom":I
    .end local v32    # "reasonLeft":I
    .end local v34    # "reasonTop":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 275
    .local v5, "accessibilityLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v40, v0

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v41, v0

    add-int v41, v41, v21

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v42, v0

    add-int v42, v42, v23

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v43, v0

    add-int v43, v43, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Landroid/view/View;->getMeasuredWidth()I

    move-result v44

    add-int v43, v43, v44

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v44, v0

    add-int v44, v44, v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/view/View;->getMeasuredHeight()I

    move-result v45

    add-int v44, v44, v45

    invoke-virtual/range {v40 .. v44}, Landroid/view/View;->layout(IIII)V

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v41, v0

    invoke-virtual/range {v40 .. v41}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->getHitRect(Landroid/graphics/Rect;)V

    .line 286
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowTouchExtend:I

    move/from16 v42, v0

    sub-int v41, v41, v42

    move/from16 v0, v41

    move-object/from16 v1, v40

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowTouchExtend:I

    move/from16 v42, v0

    add-int v41, v41, v42

    move/from16 v0, v41

    move-object/from16 v1, v40

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 288
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowTouchExtend:I

    move/from16 v42, v0

    sub-int v41, v41, v42

    move/from16 v0, v41

    move-object/from16 v1, v40

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowTouchExtend:I

    move/from16 v42, v0

    add-int v41, v41, v42

    move/from16 v0, v41

    move-object/from16 v1, v40

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOldOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v41, v0

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOldOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v41, v0

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOldOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v41, v0

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOldOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v41, v0

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_5

    .line 299
    :goto_1
    return-void

    .line 220
    .end local v5    # "accessibilityLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v15    # "overflowHeight":I
    .end local v16    # "overflowLeft":I
    .end local v17    # "overflowLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v18    # "overflowTop":I
    .end local v19    # "overflowWidth":I
    :cond_4
    add-int v14, v14, v36

    goto/16 :goto_0

    .line 297
    .restart local v5    # "accessibilityLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v15    # "overflowHeight":I
    .restart local v16    # "overflowLeft":I
    .restart local v17    # "overflowLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v18    # "overflowTop":I
    .restart local v19    # "overflowWidth":I
    :cond_5
    new-instance v40, Landroid/view/TouchDelegate;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v42, v0

    invoke-direct/range {v40 .. v42}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOldOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v41, v0

    invoke-virtual/range {v40 .. v41}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 27
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 104
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingLeft()I

    move-result v14

    .line 105
    .local v14, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingRight()I

    move-result v15

    .line 106
    .local v15, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingTop()I

    move-result v16

    .line 107
    .local v16, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingBottom()I

    move-result v13

    .line 110
    .local v13, "paddingBottom":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v22

    .line 113
    .local v22, "resultWidth":I
    add-int v21, v13, v16

    .line 115
    .local v21, "resultHeight":I
    sub-int v23, v22, v14

    sub-int v10, v23, v15

    .line 117
    .local v10, "innerWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageView:Lcom/google/android/music/art/MainstageArtView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/music/art/MainstageArtView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 118
    .local v7, "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    check-cast v20, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 119
    .local v20, "reasonLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/music/KeepOnViewSmall;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 121
    .local v11, "keepOnLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v23, v0

    sub-int v23, v10, v23

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v24, v0

    sub-int v8, v23, v24

    .line 122
    .local v8, "imageWidth":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mThumbnailAspectRatio:F

    move/from16 v23, v0

    int-to-float v0, v8

    move/from16 v24, v0

    mul-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v6, v0

    .line 123
    .local v6, "imageHeight":I
    iput v6, v7, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageView:Lcom/google/android/music/art/MainstageArtView;

    move-object/from16 v23, v0

    const/high16 v24, 0x40000000    # 2.0f

    move/from16 v0, v24

    invoke-static {v8, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v24

    const/high16 v25, 0x40000000    # 2.0f

    move/from16 v0, v25

    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v25

    invoke-virtual/range {v23 .. v25}, Lcom/google/android/music/art/MainstageArtView;->measure(II)V

    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mImageView:Lcom/google/android/music/art/MainstageArtView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/music/art/MainstageArtView;->getMeasuredHeight()I

    move-result v23

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v24, v0

    add-int v23, v23, v24

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v24, v0

    add-int v23, v23, v24

    add-int v21, v21, v23

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mPlayButton:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    move/from16 v17, v0

    .line 133
    .local v17, "playButtonSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mPlayButton:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const/high16 v24, 0x40000000    # 2.0f

    move/from16 v0, v17

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v24

    const/high16 v25, 0x40000000    # 2.0f

    move/from16 v0, v17

    move/from16 v1, v25

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v25

    invoke-virtual/range {v23 .. v25}, Landroid/widget/ImageView;->measure(II)V

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mTitleBackground:Landroid/view/View;

    move-object/from16 v23, v0

    const/high16 v24, 0x40000000    # 2.0f

    move/from16 v0, v24

    invoke-static {v8, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v24

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 140
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mThumbnailAspectRatio:F

    move/from16 v23, v0

    const/high16 v24, 0x3f000000    # 0.5f

    sub-float v23, v23, v24

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(F)F

    move-result v23

    const v24, 0x3c23d70a    # 0.01f

    cmpg-float v23, v23, v24

    if-gez v23, :cond_0

    .line 142
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mTitleBackground:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredHeight()I

    move-result v23

    add-int v21, v21, v23

    .line 145
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-virtual/range {v23 .. v25}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->measure(II)V

    .line 147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mRadioIcon:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-virtual/range {v23 .. v25}, Landroid/widget/ImageView;->measure(II)V

    .line 149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-virtual/range {v23 .. v25}, Lcom/google/android/music/KeepOnViewSmall;->measure(II)V

    .line 152
    const/16 v18, 0x0

    .line 153
    .local v18, "reason1Height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getVisibility()I

    move-result v23

    const/16 v24, 0x8

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_1

    .line 154
    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v23, v0

    sub-int v23, v10, v23

    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v24, v0

    sub-int v19, v23, v24

    .line 157
    .local v19, "reason1Width":I
    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v23, v0

    if-lez v23, :cond_2

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v23, v0

    const/high16 v24, 0x40000000    # 2.0f

    move/from16 v0, v19

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v24

    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v25, v0

    const/high16 v26, 0x40000000    # 2.0f

    invoke-static/range {v25 .. v26}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v25

    invoke-virtual/range {v23 .. v25}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->measure(II)V

    .line 166
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getMeasuredHeight()I

    move-result v23

    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v24, v0

    add-int v23, v23, v24

    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v24, v0

    add-int v18, v23, v24

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/music/KeepOnViewSmall;->getMeasuredHeight()I

    move-result v23

    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v24, v0

    add-int v12, v23, v24

    .line 170
    .local v12, "keeponHeightRow3":I
    move/from16 v0, v18

    invoke-static {v12, v0}, Ljava/lang/Math;->max(II)I

    move-result v23

    add-int v21, v21, v23

    .line 173
    .end local v12    # "keeponHeightRow3":I
    .end local v19    # "reason1Width":I
    :cond_1
    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->setMeasuredDimension(II)V

    .line 176
    sub-int v23, v21, v16

    sub-int v9, v23, v13

    .line 179
    .local v9, "innerHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 181
    .local v4, "accessibilityLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v23, v0

    sub-int v23, v10, v23

    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v24, v0

    sub-int v5, v23, v24

    .line 183
    .local v5, "accessibilityWidth":I
    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v23, v0

    sub-int v23, v9, v23

    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v24, v0

    sub-int v3, v23, v24

    .line 185
    .local v3, "accessibilityHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v23, v0

    const/high16 v24, 0x40000000    # 2.0f

    move/from16 v0, v24

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v24

    const/high16 v25, 0x40000000    # 2.0f

    move/from16 v0, v25

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v25

    invoke-virtual/range {v23 .. v25}, Landroid/view/View;->measure(II)V

    .line 188
    return-void

    .line 162
    .end local v3    # "accessibilityHeight":I
    .end local v4    # "accessibilityLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v5    # "accessibilityWidth":I
    .end local v9    # "innerHeight":I
    .restart local v19    # "reason1Width":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v23, v0

    const/high16 v24, 0x40000000    # 2.0f

    move/from16 v0, v19

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v24

    const/16 v25, 0x0

    invoke-virtual/range {v23 .. v25}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->measure(II)V

    goto/16 :goto_0
.end method

.method public setSharedElementSnapshotInfo(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V
    .locals 3
    .param p1, "viewToGlobalMatrix"    # Landroid/graphics/Matrix;
    .param p2, "screenBounds"    # Landroid/graphics/RectF;

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getSnapshotElement()Landroid/view/View;

    move-result-object v0

    .line 374
    .local v0, "view":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 375
    invoke-virtual {v0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 377
    if-eqz p2, :cond_0

    .line 383
    iget v1, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, p2, Landroid/graphics/RectF;->top:F

    .line 384
    iget v1, p2, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingRight()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, p2, Landroid/graphics/RectF;->right:F

    .line 385
    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingBottom()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, p2, Landroid/graphics/RectF;->bottom:F

    .line 386
    iget v1, p2, Landroid/graphics/RectF;->left:F

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, p2, Landroid/graphics/RectF;->left:F

    .line 388
    :cond_0
    return-void
.end method

.method public setThumbnailAspectRatio(F)V
    .locals 0
    .param p1, "thumbnailAspectRatio"    # F

    .prologue
    .line 366
    return-void
.end method
