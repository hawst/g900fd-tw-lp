.class public Lcom/google/android/music/store/AutoCacheManager;
.super Ljava/lang/Object;
.source "AutoCacheManager.java"


# instance fields
.field private mStore:Lcom/google/android/music/store/Store;


# direct methods
.method public constructor <init>(Lcom/google/android/music/store/Store;)V
    .locals 0
    .param p1, "store"    # Lcom/google/android/music/store/Store;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/music/store/AutoCacheManager;->mStore:Lcom/google/android/music/store/Store;

    .line 35
    return-void
.end method

.method private doAutoCacheQuery([Ljava/lang/String;ILjava/util/Collection;)Landroid/database/Cursor;
    .locals 12
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .local p3, "blacklistIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const/4 v2, 0x0

    .line 76
    iget-object v4, p0, Lcom/google/android/music/store/AutoCacheManager;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4}, Lcom/google/android/music/store/Store;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "music_autocache_num_suggested_mixes"

    const/4 v6, 0x4

    invoke-static {v4, v5, v6}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v9

    .line 81
    .local v9, "autoCacheNumSuggestedMixes":I
    iget-object v4, p0, Lcom/google/android/music/store/AutoCacheManager;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4}, Lcom/google/android/music/store/Store;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "music_autocache_suggested_mix_size"

    const/16 v6, 0xa

    invoke-static {v4, v5, v6}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    .line 86
    .local v10, "autoCacheNumTracksPerMix":I
    if-lez v9, :cond_0

    const/16 v4, 0x64

    if-gt v9, v4, :cond_0

    if-lez v10, :cond_0

    const/16 v4, 0x3e8

    if-le v10, v4, :cond_1

    .line 90
    :cond_0
    const-string v4, "AutoCacheManager"

    const-string v5, "Invalid limit(s): numSuggestedMixes=%d, numTracksPerMix=%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v11, v2

    .line 136
    :goto_0
    return-object v11

    .line 96
    :cond_1
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 97
    .local v0, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "(SELECT SeedListId,SeedOrder FROM SUGGESTED_SEEDS ORDER BY SeedOrder LIMIT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " JOIN LISTITEMS ON SeedListId=LISTITEMS.ListId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id) "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 107
    const/4 v11, 0x0

    .line 108
    .local v11, "c":Landroid/database/Cursor;
    iget-object v4, p0, Lcom/google/android/music/store/AutoCacheManager;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 117
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "LocalCopyType IN (0, 300) AND ClientPosition < "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 124
    .local v3, "selection":Ljava/lang/String;
    if-eqz p3, :cond_2

    invoke-interface {p3}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 125
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MUSIC.Id"

    invoke-static {v5, p3}, Lcom/google/android/music/utils/DbUtils;->getNotInClause(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 128
    :cond_2
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "SeedOrder,ClientPosition"

    const/4 v8, -0x1

    if-eq p2, v8, :cond_3

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    :goto_1
    move-object v2, p1

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 133
    iget-object v2, p0, Lcom/google/android/music/store/AutoCacheManager;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v2, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_0

    :cond_3
    move-object v8, v2

    .line 128
    goto :goto_1

    .line 133
    .end local v3    # "selection":Ljava/lang/String;
    :catchall_0
    move-exception v2

    iget-object v4, p0, Lcom/google/android/music/store/AutoCacheManager;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v2
.end method


# virtual methods
.method public getNextAutoCacheDownloads(ILjava/util/Collection;)[Lcom/google/android/music/download/ContentIdentifier;
    .locals 9
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)[",
            "Lcom/google/android/music/download/ContentIdentifier;"
        }
    .end annotation

    .prologue
    .local p2, "blacklistIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 38
    const/4 v5, 0x2

    new-array v4, v5, [Ljava/lang/String;

    const-string v5, "MUSIC.Id"

    aput-object v5, v4, v6

    const-string v5, "Domain"

    aput-object v5, v4, v7

    .line 42
    .local v4, "projection":[Ljava/lang/String;
    invoke-direct {p0, v4, p1, p2}, Lcom/google/android/music/store/AutoCacheManager;->doAutoCacheQuery([Ljava/lang/String;ILjava/util/Collection;)Landroid/database/Cursor;

    move-result-object v0

    .line 43
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 45
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    new-array v3, v5, [Lcom/google/android/music/download/ContentIdentifier;

    .line 46
    .local v3, "ids":[Lcom/google/android/music/download/ContentIdentifier;
    const/4 v1, 0x0

    .local v1, "i":I
    move v2, v1

    .line 47
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 48
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    new-instance v5, Lcom/google/android/music/download/ContentIdentifier;

    const/4 v6, 0x0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-static {v8}, Lcom/google/android/music/download/ContentIdentifier$Domain;->fromDBValue(I)Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v8

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/music/download/ContentIdentifier;-><init>(JLcom/google/android/music/download/ContentIdentifier$Domain;)V

    aput-object v5, v3, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 52
    :cond_0
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 55
    .end local v2    # "i":I
    .end local v3    # "ids":[Lcom/google/android/music/download/ContentIdentifier;
    :goto_1
    return-object v3

    .line 52
    :catchall_0
    move-exception v5

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v5

    .line 55
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getTotalSizeToAutoCache()J
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 59
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/String;

    const-string v2, "SUM(Size)"

    aput-object v2, v1, v3

    .line 62
    .local v1, "projection":[Ljava/lang/String;
    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/music/store/AutoCacheManager;->doAutoCacheQuery([Ljava/lang/String;ILjava/util/Collection;)Landroid/database/Cursor;

    move-result-object v0

    .line 63
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 65
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 66
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 69
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 72
    :goto_0
    return-wide v2

    .line 69
    :cond_0
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 72
    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_0

    .line 69
    :catchall_0
    move-exception v2

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2
.end method
