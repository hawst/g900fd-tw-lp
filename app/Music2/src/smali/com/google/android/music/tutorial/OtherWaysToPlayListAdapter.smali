.class public Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "OtherWaysToPlayListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;,
        Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mLayoutResouceId:I

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private final mPrefs:Lcom/google/android/music/preferences/MusicPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;ILjava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/music/preferences/MusicPreferences;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p4, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;>;"
    invoke-direct {p0, p1, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 110
    new-instance v0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;-><init>(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)V

    iput-object v0, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 56
    iput-object p1, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mContext:Landroid/content/Context;

    .line 57
    iput p3, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mLayoutResouceId:I

    .line 58
    iput-object p4, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mData:Ljava/util/List;

    .line 59
    iput-object p2, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;I[Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;
    .param p3, "textViewResourceId"    # I
    .param p4, "data"    # [Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;

    .prologue
    .line 65
    invoke-direct {p0, p1, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 110
    new-instance v0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;-><init>(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)V

    iput-object v0, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 66
    iput-object p1, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mContext:Landroid/content/Context;

    .line 67
    iput p3, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mLayoutResouceId:I

    .line 68
    invoke-static {p4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mData:Ljava/util/List;

    .line 69
    iput-object p2, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 75
    move-object v3, p2

    .line 76
    .local v3, "row":Landroid/view/View;
    const/4 v1, 0x0

    .line 77
    .local v1, "holder":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;
    if-nez p2, :cond_0

    .line 78
    iget-object v4, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 79
    .local v2, "inflater":Landroid/view/LayoutInflater;
    iget v4, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mLayoutResouceId:I

    invoke-virtual {v2, v4, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 80
    new-instance v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;

    .end local v1    # "holder":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;
    invoke-direct {v1, p0}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;-><init>(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)V

    .line 81
    .restart local v1    # "holder":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;
    const v4, 0x7f0e01c7

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;->mIcon:Landroid/widget/ImageView;

    .line 82
    const v4, 0x7f0e01c8

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;->mTextView_1:Landroid/widget/TextView;

    .line 83
    const v4, 0x7f0e01c9

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;->mTextView_2:Landroid/widget/TextView;

    .line 84
    iget-object v4, v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;->mTextView_1:Landroid/widget/TextView;

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 85
    iget-object v4, v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;->mTextView_2:Landroid/widget/TextView;

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 86
    invoke-virtual {v3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 91
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v4, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mData:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;

    .line 92
    .local v0, "entry":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;
    iget-object v4, v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;->mIcon:Landroid/widget/ImageView;

    iget v5, v0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;->mIconId:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 93
    iget-object v4, v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;->mIcon:Landroid/widget/ImageView;

    iget v5, v0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;->mIconId:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 94
    iget-object v4, v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;->mTextView_1:Landroid/widget/TextView;

    iget v5, v0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;->mFirstTextId:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 95
    iget-object v4, v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;->mTextView_2:Landroid/widget/TextView;

    iget v5, v0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;->mSecondTextId:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 98
    iget v4, v0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;->mSecondTextId:I

    const v5, 0x7f0b0145

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-static {v4, v5}, Lcom/google/android/music/purchase/Finsky;->isPlayStoreAvailable(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 100
    iget-object v4, v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;->mTextView_2:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    :goto_1
    iget-object v4, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    return-object v3

    .line 88
    .end local v0    # "entry":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;
    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "holder":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;
    check-cast v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;

    .restart local v1    # "holder":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;
    goto :goto_0

    .line 102
    .restart local v0    # "entry":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;
    :cond_1
    iget-object v4, v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;->mTextView_2:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
