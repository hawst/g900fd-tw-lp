.class Lcom/google/android/music/cast/CastSessionManager$SessionStatusReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CastSessionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cast/CastSessionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SessionStatusReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/CastSessionManager;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/CastSessionManager;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/music/cast/CastSessionManager$SessionStatusReceiver;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 72
    iget-object v1, p0, Lcom/google/android/music/cast/CastSessionManager$SessionStatusReceiver;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    # invokes: Lcom/google/android/music/cast/CastSessionManager;->isLogVerbose()Z
    invoke-static {v1}, Lcom/google/android/music/cast/CastSessionManager;->access$200(Lcom/google/android/music/cast/CastSessionManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    const-string v1, "MusicCastSession"

    const-string v2, "onReceive intent=%s extras=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/cast/CastSessionManager$SessionStatusReceiver;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    const-string v2, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/cast/CastSessionManager;->mSessionId:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/cast/CastSessionManager;->access$302(Lcom/google/android/music/cast/CastSessionManager;Ljava/lang/String;)Ljava/lang/String;

    .line 78
    const-string v1, "android.media.intent.extra.SESSION_STATUS"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 79
    const-string v1, "android.media.intent.extra.SESSION_STATUS"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v7/media/MediaSessionStatus;->fromBundle(Landroid/os/Bundle;)Landroid/support/v7/media/MediaSessionStatus;

    move-result-object v0

    .line 81
    .local v0, "status":Landroid/support/v7/media/MediaSessionStatus;
    iget-object v1, p0, Lcom/google/android/music/cast/CastSessionManager$SessionStatusReceiver;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    # invokes: Lcom/google/android/music/cast/CastSessionManager;->processMediaSessionStatus(Landroid/support/v7/media/MediaSessionStatus;)V
    invoke-static {v1, v0}, Lcom/google/android/music/cast/CastSessionManager;->access$400(Lcom/google/android/music/cast/CastSessionManager;Landroid/support/v7/media/MediaSessionStatus;)V

    .line 85
    .end local v0    # "status":Landroid/support/v7/media/MediaSessionStatus;
    :goto_0
    return-void

    .line 83
    :cond_1
    const-string v1, "MusicCastSession"

    const-string v2, "Received update with no status!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
