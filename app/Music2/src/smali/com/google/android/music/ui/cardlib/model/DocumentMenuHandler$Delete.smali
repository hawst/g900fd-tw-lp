.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$AsyncMenuEntry;
.source "DocumentMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Delete"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;I)V
    .locals 1
    .param p2, "res"    # Landroid/content/res/Resources;
    .param p3, "titleId"    # I

    .prologue
    .line 1284
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 1285
    const v0, 0x7f0e002c

    invoke-direct {p0, p2, v0, p3}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$AsyncMenuEntry;-><init>(Landroid/content/res/Resources;II)V

    .line 1286
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 15

    .prologue
    .line 1290
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v10}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v10

    sget-object v11, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v10, v11, :cond_0

    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v10}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v10

    sget-object v11, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v10, v11, :cond_2

    .line 1292
    :cond_0
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v10}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1293
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_1

    .line 1294
    new-instance v4, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog;

    invoke-direct {v4}, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog;-><init>()V

    .line 1295
    .local v4, "fragment":Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog;
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v4, v0, v10}, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog;->createArgs(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/os/Bundle;

    move-result-object v1

    .line 1296
    .local v1, "args":Landroid/os/Bundle;
    invoke-static {v0, v4, v1}, Lcom/google/android/music/ui/FragmentUtils;->addFragment(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/Fragment;Landroid/os/Bundle;)V

    .line 1346
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .end local v1    # "args":Landroid/os/Bundle;
    .end local v4    # "fragment":Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog;
    :cond_1
    :goto_0
    return-void

    .line 1298
    :cond_2
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v10}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v10

    sget-object v11, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v10, v11, :cond_9

    .line 1299
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v10}, Lcom/google/android/music/ui/MusicFragment;->getFragmentMediaList()Lcom/google/android/music/medialist/MediaList;

    move-result-object v3

    .line 1300
    .local v3, "contextList":Lcom/google/android/music/medialist/MediaList;
    instance-of v10, v3, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v10, :cond_4

    .line 1301
    check-cast v3, Lcom/google/android/music/medialist/PlaylistSongList;

    .end local v3    # "contextList":Lcom/google/android/music/medialist/MediaList;
    invoke-virtual {v3}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistId()J

    move-result-wide v10

    iget-object v12, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v12, v12, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v12}, Lcom/google/android/music/ui/cardlib/model/Document;->getIdInParent()J

    move-result-wide v12

    invoke-static {v10, v11, v12, v13}, Lcom/google/android/music/store/MusicContent$Playlists$Members;->getPlaylistItemUri(JJ)Landroid/net/Uri;

    move-result-object v9

    .line 1303
    .local v9, "uri":Landroid/net/Uri;
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v10}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 1304
    .local v6, "resolver":Landroid/content/ContentResolver;
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v6, v9, v10, v11}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    if-eqz v10, :cond_3

    .line 1305
    const v8, 0x7f0b00d2

    .line 1306
    .local v8, "stringId":I
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    const v11, 0x7f0b00d2

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v14, v14, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1308
    .local v5, "msg":Ljava/lang/String;
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    const/4 v11, 0x0

    invoke-static {v10, v5, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1310
    .end local v5    # "msg":Ljava/lang/String;
    .end local v8    # "stringId":I
    :cond_3
    const-string v10, "DocumentMenuHandler"

    const-string v11, "Could not remove item from playlist"

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1312
    .end local v6    # "resolver":Landroid/content/ContentResolver;
    .end local v9    # "uri":Landroid/net/Uri;
    .restart local v3    # "contextList":Lcom/google/android/music/medialist/MediaList;
    :cond_4
    instance-of v10, v3, Lcom/google/android/music/medialist/CaqPlayQueueSongList;

    if-eqz v10, :cond_6

    .line 1313
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v10}, Lcom/google/android/music/ui/cardlib/model/Document;->getQueueItemId()J

    move-result-wide v10

    iget-object v12, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v12, v12, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-static {v12}, Lcom/google/android/music/utils/MusicUtils;->isInCloudQueueMode(Landroid/content/Context;)Z

    move-result v12

    invoke-static {v10, v11, v12}, Lcom/google/android/music/store/MusicContent$Queue;->getItemUri(JZ)Landroid/net/Uri;

    move-result-object v9

    .line 1315
    .restart local v9    # "uri":Landroid/net/Uri;
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v10}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 1316
    .restart local v6    # "resolver":Landroid/content/ContentResolver;
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v6, v9, v10, v11}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    if-eqz v10, :cond_5

    .line 1317
    const v8, 0x7f0b00d3

    .line 1318
    .restart local v8    # "stringId":I
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    const v11, 0x7f0b00d3

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v14, v14, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1320
    .restart local v5    # "msg":Ljava/lang/String;
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    const/4 v11, 0x0

    invoke-static {v10, v5, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1322
    .end local v5    # "msg":Ljava/lang/String;
    .end local v8    # "stringId":I
    :cond_5
    const-string v10, "DocumentMenuHandler"

    const-string v11, "Could not remove item from playlist"

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1325
    .end local v6    # "resolver":Landroid/content/ContentResolver;
    .end local v9    # "uri":Landroid/net/Uri;
    :cond_6
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # invokes: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->getIsAllNautilus()Z
    invoke-static {v10}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$100(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Z

    move-result v10

    if-nez v10, :cond_8

    .line 1326
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1327
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v10, "deleteName"

    iget-object v11, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v11, v11, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v11}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1328
    const-string v10, "deleteId"

    iget-object v11, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v11, v11, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v11}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v12

    invoke-virtual {v2, v10, v12, v13}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1329
    const-string v10, "deleteType"

    sget-object v11, Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;->SONG:Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;

    invoke-virtual {v11}, Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;->ordinal()I

    move-result v11

    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1333
    const-string v11, "deleteHasRemote"

    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # invokes: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->getIsSideloaded()Z
    invoke-static {v10}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$900(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Z

    move-result v10

    if-nez v10, :cond_7

    const/4 v10, 0x1

    :goto_1
    invoke-virtual {v2, v11, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1334
    const-string v10, "deleteArtistName"

    iget-object v11, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v11, v11, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v11}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v10}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1337
    .restart local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    const/16 v10, 0x6d

    invoke-virtual {v0, v10, v2}, Landroid/support/v4/app/FragmentActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto/16 :goto_0

    .line 1333
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    :cond_7
    const/4 v10, 0x0

    goto :goto_1

    .line 1339
    .end local v2    # "bundle":Landroid/os/Bundle;
    :cond_8
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v10}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/google/android/music/store/MusicContent$XAudio;->getAudioUri(J)Landroid/net/Uri;

    move-result-object v7

    .line 1340
    .local v7, "songUri":Landroid/net/Uri;
    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v10, v10, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v10}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v10, v7, v11, v12}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1344
    .end local v3    # "contextList":Lcom/google/android/music/medialist/MediaList;
    .end local v7    # "songUri":Landroid/net/Uri;
    :cond_9
    const-string v10, "DocumentMenuHandler"

    const-string v11, "Unexpected doc type"

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
