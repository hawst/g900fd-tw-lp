.class public Lcom/google/android/music/cloudclient/RadioEditStationsResponse;
.super Ljava/lang/Object;
.source "RadioEditStationsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;
    }
.end annotation


# instance fields
.field public mMutateResponses:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "mutate_response"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse;->mMutateResponses:Ljava/util/List;

    .line 16
    return-void
.end method

.method public static validateResponse(Lcom/google/android/music/cloudclient/RadioEditStationsResponse;)Z
    .locals 4
    .param p0, "result"    # Lcom/google/android/music/cloudclient/RadioEditStationsResponse;

    .prologue
    const/4 v1, 0x0

    .line 38
    iget-object v2, p0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse;->mMutateResponses:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    iget-object v2, p0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse;->mMutateResponses:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;

    .line 41
    .local v0, "mutateResponse":Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;
    iget-object v2, v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;->mRadioStation:Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;->mRadioStation:Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    iget-object v2, v2, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mTracks:Ljava/util/List;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;->OK:Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    invoke-virtual {v2}, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;->mResponseCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 44
    const/4 v1, 0x1

    .line 48
    .end local v0    # "mutateResponse":Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;
    :cond_0
    return v1
.end method
