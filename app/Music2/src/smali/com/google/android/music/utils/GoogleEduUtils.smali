.class public Lcom/google/android/music/utils/GoogleEduUtils;
.super Ljava/lang/Object;
.source "GoogleEduUtils.java"


# direct methods
.method public static isEduDevice(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 35
    const-string v4, "device_policy"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 37
    .local v0, "dpm":Landroid/app/admin/DevicePolicyManager;
    const/4 v2, 0x0

    .line 39
    .local v2, "isDeviceOwner":Ljava/lang/Object;
    :try_start_0
    const-class v4, Landroid/app/admin/DevicePolicyManager;

    const-string v6, "isDeviceOwnerApp"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 41
    .local v3, "isDeviceOwnerMethod":Ljava/lang/reflect/Method;
    if-eqz v3, :cond_0

    .line 42
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "com.google.android.apps.enterprise.dmagent"

    aput-object v7, v4, v6

    invoke-virtual {v3, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .end local v2    # "isDeviceOwner":Ljava/lang/Object;
    :cond_0
    move-object v4, v2

    .line 48
    .end local v3    # "isDeviceOwnerMethod":Ljava/lang/reflect/Method;
    :goto_0
    instance-of v6, v4, Ljava/lang/Boolean;

    if-eqz v6, :cond_1

    .line 49
    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 51
    :goto_1
    return v4

    .line 44
    .restart local v2    # "isDeviceOwner":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 45
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "GoogleEduUtils"

    const-string v6, "isEduDevice failed: "

    invoke-static {v4, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v4, v2

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "isDeviceOwner":Ljava/lang/Object;
    :cond_1
    move v4, v5

    .line 51
    goto :goto_1
.end method
