.class Lcom/google/android/music/ui/ArtistHeaderView$2;
.super Ljava/lang/Object;
.source "ArtistHeaderView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ArtistHeaderView;->getBitmap(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/ArtistHeaderView;

.field final synthetic val$artistUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ArtistHeaderView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistHeaderView$2;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    iput-object p2, p0, Lcom/google/android/music/ui/ArtistHeaderView$2;->val$artistUrl:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 146
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView$2;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    invoke-virtual {v1}, Lcom/google/android/music/ui/ArtistHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$2;->val$artistUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmapFromDisk(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 148
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    # getter for: Lcom/google/android/music/ui/ArtistHeaderView;->sMainHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/google/android/music/ui/ArtistHeaderView;->access$700()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/ui/ArtistHeaderView$2$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/ui/ArtistHeaderView$2$1;-><init>(Lcom/google/android/music/ui/ArtistHeaderView$2;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 154
    return-void
.end method
