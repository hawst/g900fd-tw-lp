.class public Lcom/google/android/music/medialist/RecommendedRadioList;
.super Lcom/google/android/music/medialist/RadioList;
.source "RecommendedRadioList.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/medialist/RecommendedRadioList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/music/medialist/RecommendedRadioList$1;

    invoke-direct {v0}, Lcom/google/android/music/medialist/RecommendedRadioList$1;-><init>()V

    sput-object v0, Lcom/google/android/music/medialist/RecommendedRadioList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/music/medialist/RadioList;-><init>()V

    .line 18
    return-void
.end method


# virtual methods
.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-static {}, Lcom/google/android/music/store/MusicContent$Playlists;->getSuggestedMixesUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
