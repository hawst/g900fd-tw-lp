.class Lcom/google/android/music/leanback/bitmap/DecodedNonAlbumArtUriBitmapGetter;
.super Ljava/lang/Object;
.source "DecodedNonAlbumArtUriBitmapGetter.java"

# interfaces
.implements Lcom/google/android/music/leanback/bitmap/BitmapGetter;


# instance fields
.field private final mDecodedArtUri:Ljava/lang/String;

.field private final mOverrideCropToSquare:Z

.field private final mUseOverrideCropToSquare:Z


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "decodedArtUri"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/music/leanback/bitmap/DecodedNonAlbumArtUriBitmapGetter;-><init>(Ljava/lang/String;ZZ)V

    .line 20
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "decodedArtUri"    # Ljava/lang/String;
    .param p2, "overrideCropToSquare"    # Z

    .prologue
    .line 23
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/music/leanback/bitmap/DecodedNonAlbumArtUriBitmapGetter;-><init>(Ljava/lang/String;ZZ)V

    .line 24
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ZZ)V
    .locals 0
    .param p1, "decodedArtUri"    # Ljava/lang/String;
    .param p2, "useOverrideCropToSquare"    # Z
    .param p3, "overrideCropToSquare"    # Z

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/music/leanback/bitmap/DecodedNonAlbumArtUriBitmapGetter;->mDecodedArtUri:Ljava/lang/String;

    .line 29
    iput-boolean p2, p0, Lcom/google/android/music/leanback/bitmap/DecodedNonAlbumArtUriBitmapGetter;->mUseOverrideCropToSquare:Z

    .line 30
    iput-boolean p3, p0, Lcom/google/android/music/leanback/bitmap/DecodedNonAlbumArtUriBitmapGetter;->mOverrideCropToSquare:Z

    .line 31
    return-void
.end method


# virtual methods
.method public getBitmap(Landroid/content/Context;IIZ)Landroid/util/Pair;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "targetWidth"    # I
    .param p3, "targetHeight"    # I
    .param p4, "cropToSquare"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IIZ)",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v3, p0, Lcom/google/android/music/leanback/bitmap/DecodedNonAlbumArtUriBitmapGetter;->mDecodedArtUri:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 37
    .local v1, "uri":Landroid/net/Uri;
    const-string v3, "content"

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 38
    new-instance v3, Landroid/util/Pair;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 51
    .end local p4    # "cropToSquare":Z
    :goto_0
    return-object v3

    .line 41
    .restart local p4    # "cropToSquare":Z
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 42
    .local v2, "url":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/utils/FifeImageUrlUtil;->isFifeHostedUrl(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 43
    if-le p2, p3, :cond_3

    move v0, p2

    .line 45
    .local v0, "size":I
    :goto_1
    :try_start_0
    invoke-static {v0, v1}, Lcom/google/android/music/utils/FifeImageUrlUtil;->setImageUrlSize(ILandroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/music/utils/FifeImageUrlUtil$InvalidUrlException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 51
    .end local v0    # "size":I
    :cond_1
    :goto_2
    new-instance v3, Landroid/util/Pair;

    iget-boolean v4, p0, Lcom/google/android/music/leanback/bitmap/DecodedNonAlbumArtUriBitmapGetter;->mUseOverrideCropToSquare:Z

    if-eqz v4, :cond_2

    iget-boolean p4, p0, Lcom/google/android/music/leanback/bitmap/DecodedNonAlbumArtUriBitmapGetter;->mOverrideCropToSquare:Z

    .end local p4    # "cropToSquare":Z
    :cond_2
    invoke-static {p1, v2, p2, p3, p4}, Lcom/google/android/music/utils/AlbumArtUtils;->getRealNonAlbumArt(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .restart local p4    # "cropToSquare":Z
    :cond_3
    move v0, p3

    .line 43
    goto :goto_1

    .line 46
    .restart local v0    # "size":I
    :catch_0
    move-exception v3

    goto :goto_2
.end method
