.class public Lcom/google/android/music/utils/ViewUtils;
.super Ljava/lang/Object;
.source "ViewUtils.java"


# direct methods
.method public static announceTextForAccessibility(Landroid/view/accessibility/AccessibilityManager;Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "manager"    # Landroid/view/accessibility/AccessibilityManager;
    .param p1, "source"    # Landroid/view/View;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "className"    # Ljava/lang/String;
    .param p4, "packageName"    # Ljava/lang/String;

    .prologue
    .line 300
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 326
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-ge v3, v4, :cond_2

    .line 306
    const/16 v1, 0x8

    .line 311
    .local v1, "eventType":I
    :goto_1
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 312
    .local v0, "event":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313
    invoke-virtual {v0, p3}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 314
    invoke-virtual {v0, p4}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 316
    if-eqz p1, :cond_3

    .line 318
    invoke-static {v0}, Landroid/support/v4/view/accessibility/AccessibilityEventCompat;->asRecord(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    move-result-object v2

    .line 319
    .local v2, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v2, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setSource(Landroid/view/View;)V

    .line 325
    .end local v2    # "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    :goto_2
    invoke-virtual {p0, v0}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    .line 308
    .end local v0    # "event":Landroid/view/accessibility/AccessibilityEvent;
    .end local v1    # "eventType":I
    :cond_2
    const/16 v1, 0x4000

    .restart local v1    # "eventType":I
    goto :goto_1

    .line 321
    .restart local v0    # "event":Landroid/view/accessibility/AccessibilityEvent;
    :cond_3
    const-string v3, "ViewUtils"

    const-string v4, "Source view cannot be null for announceTextForAccessibility"

    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v3, v4, v5}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public static canScrollVertically(Landroid/view/View;ZIII)Z
    .locals 10
    .param p0, "v"    # Landroid/view/View;
    .param p1, "checkV"    # Z
    .param p2, "dy"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    const/4 v6, 0x1

    .line 169
    instance-of v7, p0, Landroid/view/ViewGroup;

    if-eqz v7, :cond_3

    move-object v2, p0

    .line 170
    check-cast v2, Landroid/view/ViewGroup;

    .line 171
    .local v2, "group":Landroid/view/ViewGroup;
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v4

    .line 172
    .local v4, "scrollX":I
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v5

    .line 173
    .local v5, "scrollY":I
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 175
    .local v1, "count":I
    add-int/lit8 v3, v1, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_3

    .line 178
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 181
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_1

    .line 175
    :cond_0
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 183
    :cond_1
    add-int v7, p3, v4

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v8

    if-lt v7, v8, :cond_0

    add-int v7, p3, v4

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v8

    if-ge v7, v8, :cond_0

    add-int v7, p4, v5

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v8

    if-lt v7, v8, :cond_0

    add-int v7, p4, v5

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v8

    if-ge v7, v8, :cond_0

    add-int v7, p3, v4

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v8

    sub-int/2addr v7, v8

    add-int v8, p4, v5

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-static {v0, v6, p2, v7, v8}, Lcom/google/android/music/utils/ViewUtils;->canScrollVertically(Landroid/view/View;ZIII)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 194
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "count":I
    .end local v2    # "group":Landroid/view/ViewGroup;
    .end local v3    # "i":I
    .end local v4    # "scrollX":I
    .end local v5    # "scrollY":I
    :cond_2
    :goto_1
    return v6

    :cond_3
    if-eqz p1, :cond_4

    instance-of v7, p0, Landroid/widget/ListView;

    if-nez v7, :cond_2

    neg-int v7, p2

    invoke-static {p0, v7}, Landroid/support/v4/view/ViewCompat;->canScrollVertically(Landroid/view/View;I)Z

    move-result v7

    if-nez v7, :cond_2

    :cond_4
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static createOrReuseView(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 2
    .param p0, "convertView"    # Landroid/view/View;
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "layoutId"    # I

    .prologue
    .line 57
    if-nez p0, :cond_0

    .line 58
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    .line 60
    .end local p0    # "convertView":Landroid/view/View;
    :cond_0
    return-object p0
.end method

.method public static dipToPixels(Landroid/content/Context;F)F
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dipValue"    # F

    .prologue
    .line 373
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 374
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    const/4 v1, 0x1

    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    return v1
.end method

.method public static fadeViewForPosition(Landroid/view/View;III)V
    .locals 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "size"    # I
    .param p2, "viewPos"    # I
    .param p3, "numberOfFadedItems"    # I

    .prologue
    const/4 v5, -0x1

    const/high16 v4, 0x3f800000    # 1.0f

    .line 81
    sub-int v2, p1, p2

    .line 82
    .local v2, "posFromEnd":I
    int-to-float v3, p3

    add-float/2addr v3, v4

    div-float v1, v4, v3

    .line 83
    .local v1, "fadeFactor":F
    if-eq p2, v5, :cond_1

    if-eq p1, v5, :cond_1

    if-gt v2, p3, :cond_1

    .line 84
    if-lez v2, :cond_0

    .line 85
    int-to-float v3, v2

    mul-float v0, v3, v1

    .line 86
    .local v0, "alpha":F
    invoke-static {p0, v0}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 92
    .end local v0    # "alpha":F
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    invoke-static {p0, v4}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    goto :goto_0
.end method

.method public static getActionBarHeight(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 335
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 336
    .local v0, "tv":Landroid/util/TypedValue;
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010125

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 337
    iget v1, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v1

    .line 341
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_action_bar_height"

    const/16 v3, 0x30

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    int-to-float v1, v1

    invoke-static {p0, v1}, Lcom/google/android/music/utils/ViewUtils;->dipToPixels(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    goto :goto_0
.end method

.method public static getCurrentSizeRange(Landroid/view/Display;Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 2
    .param p0, "display"    # Landroid/view/Display;
    .param p1, "smallestSize"    # Landroid/graphics/Point;
    .param p2, "largestSize"    # Landroid/graphics/Point;

    .prologue
    .line 115
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 116
    invoke-static {p0, p1, p2}, Lcom/google/android/music/utils/ViewUtils;->getCurrentSizeRangeJb(Landroid/view/Display;Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 120
    :goto_0
    return-void

    .line 118
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/music/utils/ViewUtils;->getCurrentSizeRangePreJb(Landroid/view/Display;Landroid/graphics/Point;Landroid/graphics/Point;)V

    goto :goto_0
.end method

.method private static getCurrentSizeRangeJb(Landroid/view/Display;Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 0
    .param p0, "display"    # Landroid/view/Display;
    .param p1, "smallestSize"    # Landroid/graphics/Point;
    .param p2, "largestSize"    # Landroid/graphics/Point;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 125
    invoke-virtual {p0, p1, p2}, Landroid/view/Display;->getCurrentSizeRange(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 126
    return-void
.end method

.method private static getCurrentSizeRangePreJb(Landroid/view/Display;Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 4
    .param p0, "display"    # Landroid/view/Display;
    .param p1, "smallestSize"    # Landroid/graphics/Point;
    .param p2, "largestSize"    # Landroid/graphics/Point;

    .prologue
    .line 130
    invoke-static {p0, p1}, Lcom/google/android/music/utils/ViewUtils;->getSize(Landroid/view/Display;Landroid/graphics/Point;)V

    .line 131
    iget v2, p1, Landroid/graphics/Point;->x:I

    iget v3, p1, Landroid/graphics/Point;->y:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 132
    .local v1, "min":I
    iget v2, p1, Landroid/graphics/Point;->x:I

    iget v3, p1, Landroid/graphics/Point;->y:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 133
    .local v0, "max":I
    invoke-virtual {p1, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 134
    invoke-virtual {p2, v0, v0}, Landroid/graphics/Point;->set(II)V

    .line 135
    return-void
.end method

.method public static getNotificationBarHeight(Landroid/content/Context;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 354
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "status_bar_height"

    const-string v3, "dimen"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 356
    .local v0, "resourceId":I
    if-lez v0, :cond_0

    .line 357
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 360
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_notification_bar_height"

    const/16 v3, 0x19

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    int-to-float v1, v1

    invoke-static {p0, v1}, Lcom/google/android/music/utils/ViewUtils;->dipToPixels(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    goto :goto_0
.end method

.method public static getRawSize(Landroid/view/Display;)Landroid/graphics/Point;
    .locals 8
    .param p0, "display"    # Landroid/view/Display;

    .prologue
    .line 267
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-lt v5, v6, :cond_0

    .line 268
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 269
    .local v4, "p":Landroid/graphics/Point;
    invoke-virtual {p0, v4}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 284
    .end local v4    # "p":Landroid/graphics/Point;
    :goto_0
    return-object v4

    .line 274
    :cond_0
    :try_start_0
    const-class v5, Landroid/view/Display;

    const-string v6, "getRawWidth"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 275
    .local v1, "getRawW":Ljava/lang/reflect/Method;
    const-class v5, Landroid/view/Display;

    const-string v6, "getRawHeight"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 276
    .local v0, "getRawH":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, p0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 277
    .local v3, "nW":I
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 278
    .local v2, "nH":I
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4, v3, v2}, Landroid/graphics/Point;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 279
    .end local v0    # "getRawH":Ljava/lang/reflect/Method;
    .end local v1    # "getRawW":Ljava/lang/reflect/Method;
    .end local v2    # "nH":I
    .end local v3    # "nW":I
    :catch_0
    move-exception v5

    .line 282
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 283
    .restart local v4    # "p":Landroid/graphics/Point;
    invoke-static {p0, v4}, Lcom/google/android/music/utils/ViewUtils;->getSize(Landroid/view/Display;Landroid/graphics/Point;)V

    goto :goto_0
.end method

.method public static getScreenColumnCount(Landroid/content/res/Resources;Lcom/google/android/music/preferences/MusicPreferences;)I
    .locals 2
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 203
    const v1, 0x7f0d000b

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 204
    .local v0, "numColumns":I
    invoke-virtual {p1}, Lcom/google/android/music/preferences/MusicPreferences;->isTvMusic()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 205
    const v1, 0x7f0d000c

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 207
    :cond_0
    return v0
.end method

.method public static getScreenSortedDimensions(Landroid/view/Display;)Landroid/util/Pair;
    .locals 6
    .param p0, "display"    # Landroid/view/Display;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Display;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260
    invoke-static {p0}, Lcom/google/android/music/utils/ViewUtils;->getRawSize(Landroid/view/Display;)Landroid/graphics/Point;

    move-result-object v1

    .line 261
    .local v1, "p":Landroid/graphics/Point;
    iget v3, v1, Landroid/graphics/Point;->x:I

    iget v4, v1, Landroid/graphics/Point;->y:I

    if-ge v3, v4, :cond_0

    iget v2, v1, Landroid/graphics/Point;->x:I

    .line 262
    .local v2, "shortEdge":I
    :goto_0
    iget v3, v1, Landroid/graphics/Point;->x:I

    iget v4, v1, Landroid/graphics/Point;->y:I

    if-lt v3, v4, :cond_1

    iget v0, v1, Landroid/graphics/Point;->x:I

    .line 263
    .local v0, "longEdge":I
    :goto_1
    new-instance v3, Landroid/util/Pair;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3

    .line 261
    .end local v0    # "longEdge":I
    .end local v2    # "shortEdge":I
    :cond_0
    iget v2, v1, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 262
    .restart local v2    # "shortEdge":I
    :cond_1
    iget v0, v1, Landroid/graphics/Point;->y:I

    goto :goto_1
.end method

.method public static getShortestEdge(Landroid/content/Context;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 225
    const-string v3, "window"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 226
    .local v2, "wm":Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 227
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 228
    .local v1, "p":Landroid/graphics/Point;
    invoke-static {v0, v1}, Lcom/google/android/music/utils/ViewUtils;->getSize(Landroid/view/Display;Landroid/graphics/Point;)V

    .line 229
    iget v3, v1, Landroid/graphics/Point;->x:I

    iget v4, v1, Landroid/graphics/Point;->y:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    return v3
.end method

.method public static getSize(Landroid/view/Display;Landroid/graphics/Point;)V
    .locals 2
    .param p0, "display"    # Landroid/view/Display;
    .param p1, "size"    # Landroid/graphics/Point;

    .prologue
    .line 138
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_0

    .line 139
    invoke-static {p0, p1}, Lcom/google/android/music/utils/ViewUtils;->getSizeHCMR2(Landroid/view/Display;Landroid/graphics/Point;)V

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/music/utils/ViewUtils;->getSizePreHCMR2(Landroid/view/Display;Landroid/graphics/Point;)V

    goto :goto_0
.end method

.method private static getSizeHCMR2(Landroid/view/Display;Landroid/graphics/Point;)V
    .locals 0
    .param p0, "display"    # Landroid/view/Display;
    .param p1, "size"    # Landroid/graphics/Point;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xd
    .end annotation

    .prologue
    .line 147
    invoke-virtual {p0, p1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 148
    return-void
.end method

.method private static getSizePreHCMR2(Landroid/view/Display;Landroid/graphics/Point;)V
    .locals 2
    .param p0, "display"    # Landroid/view/Display;
    .param p1, "size"    # Landroid/graphics/Point;

    .prologue
    .line 152
    invoke-virtual {p0}, Landroid/view/Display;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/Display;->getHeight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 153
    return-void
.end method

.method public static removeViewFromParent(Landroid/view/View;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 65
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 66
    .local v0, "parent":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 69
    :cond_0
    return-void
.end method

.method public static setAlpha(Landroid/view/View;F)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "alpha"    # F

    .prologue
    const/4 v3, 0x1

    .line 95
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 96
    invoke-virtual {p0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 105
    :goto_0
    return-void

    .line 99
    :cond_0
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p1, p1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 100
    .local v0, "alphaAnim":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v0, v3}, Landroid/view/animation/AlphaAnimation;->setFillEnabled(Z)V

    .line 101
    invoke-virtual {v0, v3}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 102
    invoke-virtual {p0, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 103
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public static setEnabledAll(Landroid/view/View;Z)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "enabled"    # Z

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 44
    instance-of v3, p0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    move-object v1, p0

    .line 45
    check-cast v1, Landroid/view/ViewGroup;

    .line 46
    .local v1, "group":Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 47
    .local v0, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 48
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/google/android/music/utils/ViewUtils;->setEnabledAll(Landroid/view/View;Z)V

    .line 47
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 51
    .end local v0    # "childCount":I
    .end local v1    # "group":Landroid/view/ViewGroup;
    .end local v2    # "i":I
    :cond_0
    return-void
.end method

.method public static setWidthToShortestEdge(Landroid/content/Context;Landroid/view/ViewGroup;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/ViewGroup;

    .prologue
    .line 216
    invoke-static {p0}, Lcom/google/android/music/utils/ViewUtils;->getShortestEdge(Landroid/content/Context;)I

    move-result v0

    .line 217
    .local v0, "calculatedWidth":I
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 218
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    if-eqz v1, :cond_0

    .line 219
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 221
    :cond_0
    return v0
.end method

.method public static setWidthToThirdOfScreen(Landroid/content/Context;Landroid/view/ViewGroup;I)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/ViewGroup;
    .param p2, "minSize"    # I

    .prologue
    .line 243
    const-string v5, "window"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    .line 244
    .local v4, "wm":Landroid/view/WindowManager;
    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 245
    .local v1, "display":Landroid/view/Display;
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 246
    .local v3, "p":Landroid/graphics/Point;
    invoke-static {v1, v3}, Lcom/google/android/music/utils/ViewUtils;->getSize(Landroid/view/Display;Landroid/graphics/Point;)V

    .line 247
    iget v5, v3, Landroid/graphics/Point;->x:I

    div-int/lit8 v5, v5, 0x3

    invoke-static {p2, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 248
    .local v0, "calculatedWidth":I
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 249
    .local v2, "lp":Landroid/view/ViewGroup$LayoutParams;
    if-eqz v2, :cond_0

    .line 250
    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 252
    :cond_0
    return v0
.end method
