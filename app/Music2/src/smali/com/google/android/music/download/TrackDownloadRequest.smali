.class public Lcom/google/android/music/download/TrackDownloadRequest;
.super Lcom/google/android/music/download/BaseDownloadRequest;
.source "TrackDownloadRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/music/download/BaseDownloadRequest",
        "<",
        "Lcom/google/android/music/download/TrackDownloadProgress;",
        "Lcom/google/android/music/download/TrackOwner;",
        ">;"
    }
.end annotation


# static fields
.field public static PRIORITY_AUTOCACHE:I

.field public static PRIORITY_KEEPON:I

.field public static PRIORITY_PREFETCH1:I

.field public static PRIORITY_PREFETCH2:I

.field public static PRIORITY_PREFETCH3:I

.field public static PRIORITY_PREFETCH4:I

.field public static PRIORITY_STREAM:I


# instance fields
.field private final mDomainParam:Ljava/lang/String;

.field private final mRemoteId:Ljava/lang/String;

.field private final mSeekMs:J

.field private final mSourceAccount:I

.field private final mTrackTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput v0, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_STREAM:I

    .line 21
    const/4 v0, 0x1

    sput v0, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_PREFETCH1:I

    .line 26
    const/4 v0, 0x2

    sput v0, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_PREFETCH2:I

    .line 31
    const/4 v0, 0x3

    sput v0, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_PREFETCH3:I

    .line 36
    const/4 v0, 0x4

    sput v0, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_PREFETCH4:I

    .line 41
    const/16 v0, 0x64

    sput v0, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_KEEPON:I

    .line 46
    const/16 v0, 0xc8

    sput v0, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_AUTOCACHE:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/google/android/music/download/BaseDownloadRequest;-><init>(Landroid/os/Parcel;)V

    .line 95
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mRemoteId:Ljava/lang/String;

    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mSourceAccount:I

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mDomainParam:Ljava/lang/String;

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mTrackTitle:Ljava/lang/String;

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mSeekMs:J

    .line 100
    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/music/download/TrackOwner;JZLcom/google/android/music/download/cache/FileLocation;Ljava/lang/String;Z)V
    .locals 9
    .param p1, "id"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "trackTitle"    # Ljava/lang/String;
    .param p3, "remoteId"    # Ljava/lang/String;
    .param p4, "sourceAccount"    # I
    .param p5, "priority"    # I
    .param p6, "owner"    # Lcom/google/android/music/download/TrackOwner;
    .param p7, "seekMs"    # J
    .param p9, "retryAllowed"    # Z
    .param p10, "fileLocation"    # Lcom/google/android/music/download/cache/FileLocation;
    .param p11, "domainParam"    # Ljava/lang/String;
    .param p12, "explicit"    # Z

    .prologue
    .line 73
    move-object v2, p0

    move-object v3, p1

    move v4, p5

    move-object v5, p6

    move/from16 v6, p9

    move-object/from16 v7, p10

    move/from16 v8, p12

    invoke-direct/range {v2 .. v8}, Lcom/google/android/music/download/BaseDownloadRequest;-><init>(Lcom/google/android/music/download/ContentIdentifier;ILcom/google/android/music/download/DownloadRequest$Owner;ZLcom/google/android/music/download/cache/FileLocation;Z)V

    .line 75
    if-nez p3, :cond_0

    .line 76
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "The remote id is required"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 79
    :cond_0
    if-nez p2, :cond_1

    .line 80
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "The track title is required"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 82
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v2, p7, v2

    if-gez v2, :cond_2

    .line 83
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Negative seek time"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 86
    :cond_2
    iput-object p3, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mRemoteId:Ljava/lang/String;

    .line 87
    iput p4, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mSourceAccount:I

    .line 88
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mDomainParam:Ljava/lang/String;

    .line 89
    iput-object p2, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mTrackTitle:Ljava/lang/String;

    .line 90
    move-wide/from16 v0, p7

    iput-wide v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mSeekMs:J

    .line 91
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 169
    instance-of v2, p1, Lcom/google/android/music/download/TrackDownloadRequest;

    if-nez v2, :cond_1

    .line 174
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 173
    check-cast v0, Lcom/google/android/music/download/TrackDownloadRequest;

    .line 174
    .local v0, "downloadRequest":Lcom/google/android/music/download/TrackDownloadRequest;
    invoke-super {p0, v0}, Lcom/google/android/music/download/BaseDownloadRequest;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mSeekMs:J

    invoke-virtual {v0}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getDomainParam()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mDomainParam:Ljava/lang/String;

    return-object v0
.end method

.method protected getMaxPriority()I
    .locals 1

    .prologue
    .line 211
    sget v0, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_STREAM:I

    return v0
.end method

.method protected getMinPriority()I
    .locals 1

    .prologue
    .line 206
    sget v0, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_AUTOCACHE:I

    return v0
.end method

.method protected bridge synthetic getOwnerFromInt(I)Lcom/google/android/music/download/DownloadRequest$Owner;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwnerFromInt(I)Lcom/google/android/music/download/TrackOwner;

    move-result-object v0

    return-object v0
.end method

.method protected getOwnerFromInt(I)Lcom/google/android/music/download/TrackOwner;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 104
    invoke-static {}, Lcom/google/android/music/download/TrackOwner;->values()[Lcom/google/android/music/download/TrackOwner;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getRemoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mRemoteId:Ljava/lang/String;

    return-object v0
.end method

.method public getSeekMs()J
    .locals 2

    .prologue
    .line 146
    iget-wide v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mSeekMs:J

    return-wide v0
.end method

.method public getSourceAccount()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mSourceAccount:I

    return v0
.end method

.method public getTrackTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mTrackTitle:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 161
    invoke-super {p0}, Lcom/google/android/music/download/BaseDownloadRequest;->hashCode()I

    move-result v0

    .line 162
    .local v0, "h":I
    int-to-long v2, v0

    mul-int/lit8 v1, v0, 0x1f

    int-to-long v4, v1

    iget-wide v6, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mSeekMs:J

    add-long/2addr v4, v6

    add-long/2addr v2, v4

    long-to-int v0, v2

    .line 164
    return v0
.end method

.method public bridge synthetic isMyProgress(Lcom/google/android/music/download/DownloadProgress;)Z
    .locals 1
    .param p1, "x0"    # Lcom/google/android/music/download/DownloadProgress;

    .prologue
    .line 12
    check-cast p1, Lcom/google/android/music/download/TrackDownloadProgress;

    .end local p1    # "x0":Lcom/google/android/music/download/DownloadProgress;
    invoke-virtual {p0, p1}, Lcom/google/android/music/download/TrackDownloadRequest;->isMyProgress(Lcom/google/android/music/download/TrackDownloadProgress;)Z

    move-result v0

    return v0
.end method

.method public isMyProgress(Lcom/google/android/music/download/TrackDownloadProgress;)Z
    .locals 4
    .param p1, "progress"    # Lcom/google/android/music/download/TrackDownloadProgress;

    .prologue
    .line 155
    invoke-super {p0, p1}, Lcom/google/android/music/download/BaseDownloadRequest;->isMyProgress(Lcom/google/android/music/download/DownloadProgress;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mSeekMs:J

    invoke-virtual {p1}, Lcom/google/android/music/download/TrackDownloadProgress;->getSeekMs()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected shouldUpgrade(Lcom/google/android/music/download/BaseDownloadRequest;)Z
    .locals 4
    .param p1, "newRequest"    # Lcom/google/android/music/download/BaseDownloadRequest;

    .prologue
    .line 130
    invoke-super {p0, p1}, Lcom/google/android/music/download/BaseDownloadRequest;->shouldUpgrade(Lcom/google/android/music/download/BaseDownloadRequest;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mSeekMs:J

    check-cast p1, Lcom/google/android/music/download/TrackDownloadRequest;

    .end local p1    # "newRequest":Lcom/google/android/music/download/BaseDownloadRequest;
    invoke-virtual {p1}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lcom/google/android/music/download/BaseDownloadRequest;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 181
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    const-string v1, " mRemoteId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    const-string v1, " mSourceAccount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mSourceAccount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 184
    const-string v1, " mTrackTitle=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mTrackTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    const-string v1, " mSeekMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mSeekMs:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 190
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 196
    invoke-super {p0, p1, p2}, Lcom/google/android/music/download/BaseDownloadRequest;->writeToParcel(Landroid/os/Parcel;I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mRemoteId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 198
    iget v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mSourceAccount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 199
    iget-object v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mDomainParam:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mTrackTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 201
    iget-wide v0, p0, Lcom/google/android/music/download/TrackDownloadRequest;->mSeekMs:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 202
    return-void
.end method
