.class Lcom/google/android/music/playback/RemoteControlClientCompat$1;
.super Ljava/lang/Object;
.source "RemoteControlClientCompat.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/RemoteControlClientCompat;->setPlaybackPositionUpdateListener(Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/RemoteControlClientCompat;

.field final synthetic val$l:Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/RemoteControlClientCompat;Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;)V
    .locals 0

    .prologue
    .line 396
    iput-object p1, p0, Lcom/google/android/music/playback/RemoteControlClientCompat$1;->this$0:Lcom/google/android/music/playback/RemoteControlClientCompat;

    iput-object p2, p0, Lcom/google/android/music/playback/RemoteControlClientCompat$1;->val$l:Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "method"    # Ljava/lang/reflect/Method;
    .param p3, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 400
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPlaybackPositionUpdate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 401
    iget-object v0, p0, Lcom/google/android/music/playback/RemoteControlClientCompat$1;->val$l:Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;

    if-eqz v0, :cond_0

    .line 402
    iget-object v1, p0, Lcom/google/android/music/playback/RemoteControlClientCompat$1;->val$l:Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;

    const/4 v0, 0x0

    aget-object v0, p3, v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;->onPlaybackPositionUpdate(J)V

    .line 408
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 405
    :cond_1
    const-string v0, "RemoteControlCompat"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected interface method call: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
