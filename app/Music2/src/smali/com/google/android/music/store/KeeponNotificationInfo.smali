.class public Lcom/google/android/music/store/KeeponNotificationInfo;
.super Ljava/lang/Object;
.source "KeeponNotificationInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;
    }
.end annotation


# instance fields
.field private mHasError:Z

.field private mIsSingleContainer:Z

.field private mSingleContainerArtistName:Ljava/lang/String;

.field private mSingleContainerId:J

.field private mSingleContainerName:Ljava/lang/String;

.field private mSingleContainerType:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-direct {p0}, Lcom/google/android/music/store/KeeponNotificationInfo;->reset()V

    .line 60
    return-void
.end method

.method private reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mHasError:Z

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mIsSingleContainer:Z

    .line 68
    iput-object v1, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mSingleContainerType:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    .line 69
    iput-object v1, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mSingleContainerName:Ljava/lang/String;

    .line 70
    iput-object v1, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mSingleContainerArtistName:Ljava/lang/String;

    .line 71
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mSingleContainerId:J

    .line 72
    return-void
.end method


# virtual methods
.method public getArtistName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/music/store/KeeponNotificationInfo;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Error populating this object, artist name field is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/store/KeeponNotificationInfo;->isSingleContainer()Z

    move-result v0

    if-nez v0, :cond_1

    .line 149
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Multiple containers are being downloaded, single container metadata is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mSingleContainerArtistName:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/android/music/store/KeeponNotificationInfo;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Error populating this object, name field is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/store/KeeponNotificationInfo;->isSingleContainer()Z

    move-result v0

    if-nez v0, :cond_1

    .line 138
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Multiple containers are being downloaded, single container metadata is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mSingleContainerName:Ljava/lang/String;

    return-object v0
.end method

.method public hasError()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mHasError:Z

    return v0
.end method

.method public isSingleContainer()Z
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/android/music/store/KeeponNotificationInfo;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Error populating this object, isSingle field is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mIsSingleContainer:Z

    return v0
.end method

.method public setError()V
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/google/android/music/store/KeeponNotificationInfo;->reset()V

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mHasError:Z

    .line 108
    return-void
.end method

.method public setMultipleContainerInfo()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 97
    invoke-direct {p0}, Lcom/google/android/music/store/KeeponNotificationInfo;->reset()V

    .line 98
    iput-boolean v0, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mIsSingleContainer:Z

    .line 99
    iput-boolean v0, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mHasError:Z

    .line 100
    return-void
.end method

.method public setSingleContainerInfo(Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2
    .param p1, "type"    # Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "artistName"    # Ljava/lang/String;
    .param p4, "id"    # J

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/google/android/music/store/KeeponNotificationInfo;->reset()V

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mIsSingleContainer:Z

    .line 86
    iput-object p1, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mSingleContainerType:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    .line 87
    iput-object p2, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mSingleContainerName:Ljava/lang/String;

    .line 88
    iput-object p3, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mSingleContainerArtistName:Ljava/lang/String;

    .line 89
    iput-wide p4, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mSingleContainerId:J

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mHasError:Z

    .line 91
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 159
    .local v0, "out":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " hasError="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mHasError:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " isSingleContainer="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mIsSingleContainer:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mSingleContainerType:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " containerId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mSingleContainerId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " containerName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mSingleContainerName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " containerArtistName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/KeeponNotificationInfo;->mSingleContainerArtistName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
