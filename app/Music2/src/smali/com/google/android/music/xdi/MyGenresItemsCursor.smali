.class Lcom/google/android/music/xdi/MyGenresItemsCursor;
.super Landroid/database/MatrixCursor;
.source "MyGenresItemsCursor.java"


# static fields
.field private static final PROJECTION_ALBUMS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mImageHeight:I

.field private final mImageWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "album_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_artist"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "genreId"    # J

    .prologue
    .line 36
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 37
    iput-object p1, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mContext:Landroid/content/Context;

    .line 38
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 39
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mImageWidth:I

    .line 40
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mImageHeight:I

    .line 42
    invoke-direct {p0, p3, p4}, Lcom/google/android/music/xdi/MyGenresItemsCursor;->addRowsForGenre(J)V

    .line 43
    return-void
.end method

.method private addRowsForGenre(J)V
    .locals 15
    .param p1, "genreId"    # J

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mContext:Landroid/content/Context;

    invoke-static/range {p1 .. p2}, Lcom/google/android/music/store/MusicContent$Genres;->getAlbumsOfGenreUri(J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/MyGenresItemsCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 50
    .local v10, "cursor":Landroid/database/Cursor;
    if-nez v10, :cond_0

    .line 79
    :goto_0
    return-void

    .line 55
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 57
    .local v6, "albumId":J
    const/4 v0, 0x1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 58
    .local v12, "name":Ljava/lang/String;
    const/4 v0, 0x2

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 59
    .local v9, "artist":Ljava/lang/String;
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mImageWidth:I

    iget v2, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mImageHeight:I

    invoke-static {v6, v7, v0, v1, v2}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v8

    .line 61
    .local v8, "artUri":Landroid/net/Uri;
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumDetailsIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v11

    .line 63
    .local v11, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v0}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v0

    new-array v13, v0, [Ljava/lang/Object;

    .line 64
    .local v13, "values":[Ljava/lang/Object;
    iget-object v0, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "parent_id"

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "display_name"

    invoke-virtual {v0, v13, v1, v12}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "display_description"

    invoke-virtual {v0, v13, v1, v9}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "image_uri"

    invoke-virtual {v0, v13, v1, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "width"

    const/4 v2, 0x0

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "height"

    const/4 v2, 0x0

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/music/xdi/MyGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "intent_uri"

    const/4 v2, 0x1

    invoke-virtual {v11, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 74
    invoke-virtual {p0, v13}, Lcom/google/android/music/xdi/MyGenresItemsCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 77
    .end local v6    # "albumId":J
    .end local v8    # "artUri":Landroid/net/Uri;
    .end local v9    # "artist":Ljava/lang/String;
    .end local v11    # "intent":Landroid/content/Intent;
    .end local v12    # "name":Ljava/lang/String;
    .end local v13    # "values":[Ljava/lang/Object;
    :catchall_0
    move-exception v0

    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    :cond_1
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method
