.class Lcom/google/android/music/MusicUserContentNotifier$6;
.super Ljava/lang/Object;
.source "MusicUserContentNotifier.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/MusicUserContentNotifier;->registerArtListeners(Ljava/util/Set;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/MusicUserContentNotifier;

.field final synthetic val$urlsToWatchForChanges:Ljava/util/Set;


# direct methods
.method constructor <init>(Lcom/google/android/music/MusicUserContentNotifier;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/google/android/music/MusicUserContentNotifier$6;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    iput-object p2, p0, Lcom/google/android/music/MusicUserContentNotifier$6;->val$urlsToWatchForChanges:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 154
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$6;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mWatchedUrls:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$600(Lcom/google/android/music/MusicUserContentNotifier;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 156
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$6;->val$urlsToWatchForChanges:Ljava/util/Set;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$6;->val$urlsToWatchForChanges:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 167
    :cond_0
    return-void

    .line 160
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$6;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mWatchedUrls:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$600(Lcom/google/android/music/MusicUserContentNotifier;)Ljava/util/Set;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/MusicUserContentNotifier$6;->val$urlsToWatchForChanges:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 162
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$6;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mWatchedUrls:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$600(Lcom/google/android/music/MusicUserContentNotifier;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 163
    .local v1, "remoteUrl":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$6;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mArtLoader:Lcom/google/android/music/art/ArtLoader;
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$1000(Lcom/google/android/music/MusicUserContentNotifier;)Lcom/google/android/music/art/ArtLoader;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/art/ArtLoader$DownloadMode;->ASYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    iget-object v4, p0, Lcom/google/android/music/MusicUserContentNotifier$6;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mArtLoaderListener:Lcom/google/android/music/art/ArtLoader$Listener;
    invoke-static {v4}, Lcom/google/android/music/MusicUserContentNotifier;->access$800(Lcom/google/android/music/MusicUserContentNotifier;)Lcom/google/android/music/art/ArtLoader$Listener;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/MusicUserContentNotifier$6;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/google/android/music/MusicUserContentNotifier;->access$900(Lcom/google/android/music/MusicUserContentNotifier;)Landroid/os/Handler;

    move-result-object v5

    invoke-interface {v2, v1, v3, v4, v5}, Lcom/google/android/music/art/ArtLoader;->getArtFileDescriptorAsync(Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;Landroid/os/Handler;)V

    goto :goto_0
.end method
