.class public Lcom/google/android/music/playback/TrackInfo;
.super Ljava/lang/Object;
.source "TrackInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/playback/TrackInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAlbumId:J

.field private final mAlbumName:Ljava/lang/String;

.field private final mArtistName:Ljava/lang/String;

.field private final mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field private final mDuration:J

.field private final mExternalAlbumArtUrl:Ljava/lang/String;

.field private final mIsAlbumArtInService:Z

.field private final mPreviewPlayType:I

.field private final mRating:I

.field private final mSongId:Lcom/google/android/music/download/ContentIdentifier;

.field private final mSupportRating:Z

.field private mTrackMetajamId:Ljava/lang/String;

.field private final mTrackName:Ljava/lang/String;

.field private final mVideoId:Ljava/lang/String;

.field private final mVideoThumbnailUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 204
    new-instance v0, Lcom/google/android/music/playback/TrackInfo$1;

    invoke-direct {v0}, Lcom/google/android/music/playback/TrackInfo$1;-><init>()V

    sput-object v0, Lcom/google/android/music/playback/TrackInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    const-class v0, Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/ContentIdentifier;

    iput-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mSongId:Lcom/google/android/music/download/ContentIdentifier;

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mTrackName:Ljava/lang/String;

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mArtistName:Ljava/lang/String;

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mAlbumName:Ljava/lang/String;

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/playback/TrackInfo;->mAlbumId:J

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/music/playback/TrackInfo;->mIsAlbumArtInService:Z

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/playback/TrackInfo;->mDuration:J

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mExternalAlbumArtUrl:Ljava/lang/String;

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/music/playback/TrackInfo;->mSupportRating:Z

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/playback/TrackInfo;->mRating:I

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/playback/TrackInfo;->mPreviewPlayType:I

    .line 88
    const-class v0, Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/store/ContainerDescriptor;

    iput-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mVideoId:Ljava/lang/String;

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mTrackMetajamId:Ljava/lang/String;

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mVideoThumbnailUrl:Ljava/lang/String;

    .line 92
    return-void

    :cond_0
    move v0, v2

    .line 82
    goto :goto_0

    :cond_1
    move v1, v2

    .line 85
    goto :goto_1
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/music/playback/TrackInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/music/playback/TrackInfo$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/TrackInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZJLjava/lang/String;ZIILcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "songId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "trackName"    # Ljava/lang/String;
    .param p3, "artistName"    # Ljava/lang/String;
    .param p4, "albumName"    # Ljava/lang/String;
    .param p5, "albumId"    # J
    .param p7, "isAlbumArtInService"    # Z
    .param p8, "duration"    # J
    .param p10, "externalAlbumArtUrl"    # Ljava/lang/String;
    .param p11, "supportRating"    # Z
    .param p12, "rating"    # I
    .param p13, "previewPlayType"    # I
    .param p14, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p15, "videoId"    # Ljava/lang/String;
    .param p16, "trackMetajamId"    # Ljava/lang/String;
    .param p17, "videoThumbnailUrl"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    if-nez p1, :cond_0

    .line 55
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "songId is Null, which is an invalid value"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 58
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/playback/TrackInfo;->mSongId:Lcom/google/android/music/download/ContentIdentifier;

    .line 59
    iput-object p2, p0, Lcom/google/android/music/playback/TrackInfo;->mTrackName:Ljava/lang/String;

    .line 60
    iput-object p3, p0, Lcom/google/android/music/playback/TrackInfo;->mArtistName:Ljava/lang/String;

    .line 61
    iput-object p4, p0, Lcom/google/android/music/playback/TrackInfo;->mAlbumName:Ljava/lang/String;

    .line 62
    iput-wide p5, p0, Lcom/google/android/music/playback/TrackInfo;->mAlbumId:J

    .line 63
    iput-boolean p7, p0, Lcom/google/android/music/playback/TrackInfo;->mIsAlbumArtInService:Z

    .line 64
    iput-wide p8, p0, Lcom/google/android/music/playback/TrackInfo;->mDuration:J

    .line 65
    iput-object p10, p0, Lcom/google/android/music/playback/TrackInfo;->mExternalAlbumArtUrl:Ljava/lang/String;

    .line 66
    iput-boolean p11, p0, Lcom/google/android/music/playback/TrackInfo;->mSupportRating:Z

    .line 67
    iput p12, p0, Lcom/google/android/music/playback/TrackInfo;->mRating:I

    .line 68
    move/from16 v0, p13

    iput v0, p0, Lcom/google/android/music/playback/TrackInfo;->mPreviewPlayType:I

    .line 69
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 70
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mVideoId:Ljava/lang/String;

    .line 71
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mTrackMetajamId:Ljava/lang/String;

    .line 72
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mVideoThumbnailUrl:Ljava/lang/String;

    .line 73
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x0

    return v0
.end method

.method public getAlbumArtIsFromService()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/google/android/music/playback/TrackInfo;->mIsAlbumArtInService:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getAlbumId()J
    .locals 2

    .prologue
    .line 128
    iget-wide v0, p0, Lcom/google/android/music/playback/TrackInfo;->mAlbumId:J

    return-wide v0
.end method

.method public getAlbumName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mAlbumName:Ljava/lang/String;

    return-object v0
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mArtistName:Ljava/lang/String;

    return-object v0
.end method

.method public getContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 152
    iget-wide v0, p0, Lcom/google/android/music/playback/TrackInfo;->mDuration:J

    return-wide v0
.end method

.method public getPreviewPlayType()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/google/android/music/playback/TrackInfo;->mPreviewPlayType:I

    return v0
.end method

.method public getRating()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lcom/google/android/music/playback/TrackInfo;->mRating:I

    return v0
.end method

.method public getSongId()Lcom/google/android/music/download/ContentIdentifier;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mSongId:Lcom/google/android/music/download/ContentIdentifier;

    return-object v0
.end method

.method public getSupportRating()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/google/android/music/playback/TrackInfo;->mSupportRating:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getTrackMetajamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mTrackMetajamId:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mTrackName:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mVideoId:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoThumbnailUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mVideoThumbnailUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    const-string v1, "mSongId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/TrackInfo;->mSongId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 186
    const-string v1, " mTrackName=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/TrackInfo;->mTrackName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    const-string v1, " mArtistName=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/TrackInfo;->mArtistName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    const-string v1, " mAlbumName=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/TrackInfo;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    const-string v1, " mAlbumId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/playback/TrackInfo;->mAlbumId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 190
    const-string v1, " mIsAlbumArtInService="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/playback/TrackInfo;->mIsAlbumArtInService:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 191
    const-string v1, " mDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/playback/TrackInfo;->mDuration:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 192
    const-string v1, " mExternalAlbumArtUrl"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/TrackInfo;->mExternalAlbumArtUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    const-string v1, " mSupportRating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/playback/TrackInfo;->mSupportRating:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 194
    const-string v1, " mRating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/playback/TrackInfo;->mRating:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 195
    const-string v1, " mPreviewPlayType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/playback/TrackInfo;->mPreviewPlayType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 196
    const-string v1, " mContainerDescriptor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/TrackInfo;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 197
    const-string v1, " mVideoId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/TrackInfo;->mVideoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    const-string v1, " mTrackMetajamId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/TrackInfo;->mTrackMetajamId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    const-string v1, " mVideoThumbnailUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/TrackInfo;->mVideoThumbnailUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 97
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mSongId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 98
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mTrackName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mArtistName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mAlbumName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    iget-wide v4, p0, Lcom/google/android/music/playback/TrackInfo;->mAlbumId:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 102
    iget-boolean v0, p0, Lcom/google/android/music/playback/TrackInfo;->mIsAlbumArtInService:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 103
    iget-wide v4, p0, Lcom/google/android/music/playback/TrackInfo;->mDuration:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 104
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mExternalAlbumArtUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 105
    iget-boolean v0, p0, Lcom/google/android/music/playback/TrackInfo;->mSupportRating:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 106
    iget v0, p0, Lcom/google/android/music/playback/TrackInfo;->mRating:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 107
    iget v0, p0, Lcom/google/android/music/playback/TrackInfo;->mPreviewPlayType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 108
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 109
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mVideoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mTrackMetajamId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/music/playback/TrackInfo;->mVideoThumbnailUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112
    return-void

    :cond_0
    move v0, v2

    .line 102
    goto :goto_0

    :cond_1
    move v1, v2

    .line 105
    goto :goto_1
.end method
