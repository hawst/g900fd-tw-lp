.class public Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
.super Ljava/lang/Object;
.source "AppNavigationMetajamHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/AppNavigationMetajamHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OpenMetajamItemInfo"
.end annotation


# instance fields
.field private mAutoPlay:Z

.field private mCanSendToStore:Z

.field private mLink:Ljava/lang/String;

.field private mMetajamId:Ljava/lang/String;

.field private mNautilusEnabled:Z

.field private mSignUpForced:Z

.field private mSignUpIfNeeded:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-boolean v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mCanSendToStore:Z

    .line 46
    iput-boolean v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mSignUpForced:Z

    .line 47
    iput-boolean v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mSignUpIfNeeded:Z

    .line 48
    iput-boolean v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mAutoPlay:Z

    .line 49
    iput-boolean v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mNautilusEnabled:Z

    return-void
.end method


# virtual methods
.method public canSendToStore()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mCanSendToStore:Z

    return v0
.end method

.method public getMetajamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mMetajamId:Ljava/lang/String;

    return-object v0
.end method

.method public isAutoPlay()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mAutoPlay:Z

    return v0
.end method

.method public isNautilusEnabled()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mNautilusEnabled:Z

    return v0
.end method

.method public isSignUpForced()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mSignUpForced:Z

    return v0
.end method

.method public isSignUpIfNeeded()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mSignUpIfNeeded:Z

    return v0
.end method

.method public setAutoPlay(Z)V
    .locals 0
    .param p1, "autoPlay"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mAutoPlay:Z

    .line 86
    return-void
.end method

.method public setCanSendToStore(Z)V
    .locals 0
    .param p1, "canSendToStore"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mCanSendToStore:Z

    .line 68
    return-void
.end method

.method public setIsNautilusEnabled(Z)V
    .locals 0
    .param p1, "nautilusEnabled"    # Z

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mNautilusEnabled:Z

    .line 92
    return-void
.end method

.method public setLink(Ljava/lang/String;)V
    .locals 0
    .param p1, "link"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mLink:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setMetajamId(Ljava/lang/String;)V
    .locals 0
    .param p1, "metajamId"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mMetajamId:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public setSignUpForced(Z)V
    .locals 0
    .param p1, "signUpForced"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mSignUpForced:Z

    .line 74
    return-void
.end method

.method public setSignUpIfNeeded(Z)V
    .locals 0
    .param p1, "signUpIfNeeded"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->mSignUpIfNeeded:Z

    .line 80
    return-void
.end method
