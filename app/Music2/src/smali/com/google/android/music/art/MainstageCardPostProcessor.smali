.class public Lcom/google/android/music/art/MainstageCardPostProcessor;
.super Ljava/lang/Object;
.source "MainstageCardPostProcessor.java"

# interfaces
.implements Lcom/google/android/music/art/ArtPostProcessor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/MainstageCardPostProcessor$1;
    }
.end annotation


# static fields
.field private static final sInitializationLock:Ljava/lang/Object;

.field private static sInstance:Lcom/google/android/music/art/MainstageCardPostProcessor;


# instance fields
.field private mRadioCardPostProcessor:Lcom/google/android/music/art/RadioCardPostProcessor;

.field private mSingleImageCropProcessor:Lcom/google/android/music/art/SingleImageCropPostProcessor;

.field private mTwoByTwoCompositor:Lcom/google/android/music/art/MultiImageCompositePostProcessor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/music/art/MainstageCardPostProcessor;->sInitializationLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {}, Lcom/google/android/music/art/SingleImageCropPostProcessor;->getInstance()Lcom/google/android/music/art/SingleImageCropPostProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/art/MainstageCardPostProcessor;->mSingleImageCropProcessor:Lcom/google/android/music/art/SingleImageCropPostProcessor;

    .line 24
    invoke-static {v1, v1}, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->getInstance(II)Lcom/google/android/music/art/MultiImageCompositePostProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/art/MainstageCardPostProcessor;->mTwoByTwoCompositor:Lcom/google/android/music/art/MultiImageCompositePostProcessor;

    .line 27
    invoke-static {}, Lcom/google/android/music/art/RadioCardPostProcessor;->getInstance()Lcom/google/android/music/art/RadioCardPostProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/art/MainstageCardPostProcessor;->mRadioCardPostProcessor:Lcom/google/android/music/art/RadioCardPostProcessor;

    .line 30
    return-void
.end method

.method public static getInstance()Lcom/google/android/music/art/MainstageCardPostProcessor;
    .locals 2

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/music/art/MainstageCardPostProcessor;->sInstance:Lcom/google/android/music/art/MainstageCardPostProcessor;

    if-nez v0, :cond_1

    .line 34
    sget-object v1, Lcom/google/android/music/art/MainstageCardPostProcessor;->sInitializationLock:Ljava/lang/Object;

    monitor-enter v1

    .line 35
    :try_start_0
    sget-object v0, Lcom/google/android/music/art/MainstageCardPostProcessor;->sInstance:Lcom/google/android/music/art/MainstageCardPostProcessor;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/google/android/music/art/MainstageCardPostProcessor;

    invoke-direct {v0}, Lcom/google/android/music/art/MainstageCardPostProcessor;-><init>()V

    sput-object v0, Lcom/google/android/music/art/MainstageCardPostProcessor;->sInstance:Lcom/google/android/music/art/MainstageCardPostProcessor;

    .line 38
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    :cond_1
    sget-object v0, Lcom/google/android/music/art/MainstageCardPostProcessor;->sInstance:Lcom/google/android/music/art/MainstageCardPostProcessor;

    return-object v0

    .line 38
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private getProcessor(Lcom/google/android/music/art/ArtDescriptor;)Lcom/google/android/music/art/ArtPostProcessor;
    .locals 3
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 44
    instance-of v1, p1, Lcom/google/android/music/art/DocumentArtDescriptor;

    const-string v2, "descriptor must be a DocumentArtDescriptor"

    invoke-static {v1, v2}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 46
    check-cast p1, Lcom/google/android/music/art/DocumentArtDescriptor;

    .end local p1    # "descriptor":Lcom/google/android/music/art/ArtDescriptor;
    iget-object v0, p1, Lcom/google/android/music/art/DocumentArtDescriptor;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 47
    .local v0, "document":Lcom/google/android/music/ui/cardlib/model/Document;
    sget-object v1, Lcom/google/android/music/art/MainstageCardPostProcessor$1;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 54
    iget-object v1, p0, Lcom/google/android/music/art/MainstageCardPostProcessor;->mSingleImageCropProcessor:Lcom/google/android/music/art/SingleImageCropPostProcessor;

    :goto_0
    return-object v1

    .line 49
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/music/art/MainstageCardPostProcessor;->mRadioCardPostProcessor:Lcom/google/android/music/art/RadioCardPostProcessor;

    goto :goto_0

    .line 52
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/music/art/MainstageCardPostProcessor;->mTwoByTwoCompositor:Lcom/google/android/music/art/MultiImageCompositePostProcessor;

    goto :goto_0

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public aliasPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .locals 3
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "images"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .prologue
    .line 78
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->getDescriptor()Lcom/google/android/music/art/ArtDescriptor;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/art/MainstageCardPostProcessor;->getProcessor(Lcom/google/android/music/art/ArtDescriptor;)Lcom/google/android/music/art/ArtPostProcessor;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lcom/google/android/music/art/ArtPostProcessor;->aliasPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    move-result-object v0

    .line 80
    .local v0, "bitmap":Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    if-eqz v0, :cond_0

    .line 81
    iget-object v2, v0, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    invoke-static {v2}, Landroid/support/v7/graphics/Palette;->generate(Landroid/graphics/Bitmap;)Landroid/support/v7/graphics/Palette;

    move-result-object v1

    .line 82
    .local v1, "palette":Landroid/support/v7/graphics/Palette;
    invoke-virtual {p1, v1}, Lcom/google/android/music/art/ArtRequest2;->setPalette(Landroid/support/v7/graphics/Palette;)V

    .line 84
    .end local v1    # "palette":Landroid/support/v7/graphics/Palette;
    :cond_0
    return-object v0
.end method

.method public getConfig(Lcom/google/android/music/art/ArtDescriptor;)Landroid/graphics/Bitmap$Config;
    .locals 1
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 99
    sget-object v0, Lcom/google/android/music/art/ArtResolver2;->DEFAULT_CONFIG:Landroid/graphics/Bitmap$Config;

    return-object v0
.end method

.method public getMaxNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I
    .locals 1
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/google/android/music/art/MainstageCardPostProcessor;->getProcessor(Lcom/google/android/music/art/ArtDescriptor;)Lcom/google/android/music/art/ArtPostProcessor;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/music/art/ArtPostProcessor;->getMaxNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I

    move-result v0

    return v0
.end method

.method public getMinNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I
    .locals 1
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/google/android/music/art/MainstageCardPostProcessor;->getProcessor(Lcom/google/android/music/art/ArtDescriptor;)Lcom/google/android/music/art/ArtPostProcessor;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/music/art/ArtPostProcessor;->getMinNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I

    move-result v0

    return v0
.end method

.method public getPostProcessingMode(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)I
    .locals 1
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "images"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .prologue
    .line 60
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->getDescriptor()Lcom/google/android/music/art/ArtDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/art/MainstageCardPostProcessor;->getProcessor(Lcom/google/android/music/art/ArtDescriptor;)Lcom/google/android/music/art/ArtPostProcessor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/music/art/ArtPostProcessor;->getPostProcessingMode(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)I

    move-result v0

    return v0
.end method

.method public renderPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;Landroid/graphics/Bitmap;)Z
    .locals 3
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "images"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .param p3, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 66
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->getDescriptor()Lcom/google/android/music/art/ArtDescriptor;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/art/MainstageCardPostProcessor;->getProcessor(Lcom/google/android/music/art/ArtDescriptor;)Lcom/google/android/music/art/ArtPostProcessor;

    move-result-object v2

    invoke-interface {v2, p1, p2, p3}, Lcom/google/android/music/art/ArtPostProcessor;->renderPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;Landroid/graphics/Bitmap;)Z

    move-result v1

    .line 68
    .local v1, "success":Z
    if-eqz v1, :cond_0

    .line 69
    invoke-static {p3}, Landroid/support/v7/graphics/Palette;->generate(Landroid/graphics/Bitmap;)Landroid/support/v7/graphics/Palette;

    move-result-object v0

    .line 70
    .local v0, "palette":Landroid/support/v7/graphics/Palette;
    invoke-virtual {p1, v0}, Lcom/google/android/music/art/ArtRequest2;->setPalette(Landroid/support/v7/graphics/Palette;)V

    .line 72
    .end local v0    # "palette":Landroid/support/v7/graphics/Palette;
    :cond_0
    return v1
.end method
