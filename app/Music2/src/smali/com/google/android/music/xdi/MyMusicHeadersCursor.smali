.class Lcom/google/android/music/xdi/MyMusicHeadersCursor;
.super Landroid/database/MatrixCursor;
.source "MyMusicHeadersCursor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mItemHeight:I

.field private final mItemWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 23
    iput-object p1, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mContext:Landroid/content/Context;

    .line 24
    new-instance v2, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v2, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 25
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthDp(Landroid/content/Context;)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mItemWidth:I

    .line 26
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightDp(Landroid/content/Context;)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mItemHeight:I

    .line 28
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_MY_MUSIC_HEADER_NAMES:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 29
    invoke-direct {p0, v0}, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->getColumnValuesForBrowseHeader(I)[Ljava/lang/Object;

    move-result-object v1

    .line 30
    .local v1, "values":[Ljava/lang/Object;
    invoke-virtual {p0, v1}, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->addRow([Ljava/lang/Object;)V

    .line 28
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    .end local v1    # "values":[Ljava/lang/Object;
    :cond_0
    return-void
.end method

.method private getColumnValuesForBrowseHeader(I)[Ljava/lang/Object;
    .locals 9
    .param p1, "row"    # I

    .prologue
    const/4 v8, 0x0

    .line 35
    iget-object v4, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v4}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v4

    new-array v3, v4, [Ljava/lang/Object;

    .line 36
    .local v3, "values":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_MY_MUSIC_HEADER_NAMES:[I

    aget v5, v5, p1

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 37
    .local v2, "name":Ljava/lang/String;
    sget-object v4, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_MY_MUSIC_HEADER_IDS:[I

    aget v4, v4, p1

    int-to-long v0, v4

    .line 39
    .local v0, "headerId":J
    iget-object v4, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 40
    iget-object v4, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "name"

    invoke-virtual {v4, v3, v5, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 41
    iget-object v4, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "display_name"

    invoke-virtual {v4, v3, v5, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 42
    iget-object v4, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "background_image_uri"

    invoke-virtual {v4, v3, v5, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 43
    iget-object v4, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "bg_image_uri"

    invoke-virtual {v4, v3, v5, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 44
    iget-object v4, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "icon_uri"

    invoke-virtual {v4, v3, v5, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 45
    iget-object v4, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "default_item_width"

    iget v6, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mItemWidth:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 46
    iget-object v4, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "default_item_height"

    iget v6, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mItemHeight:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 47
    iget-object v4, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "color_hint"

    iget-object v6, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c00a6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 49
    iget-object v4, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "badge_uri"

    iget-object v6, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultBadgeUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 51
    iget-object v4, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "items_uri"

    invoke-virtual {v4, v3, v5, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 52
    iget-object v4, p0, Lcom/google/android/music/xdi/MyMusicHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "items_per_page"

    const/16 v6, 0x19

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 55
    return-object v3
.end method
