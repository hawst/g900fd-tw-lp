.class public final enum Lcom/google/android/music/mix/MixGenerationState$State;
.super Ljava/lang/Enum;
.source "MixGenerationState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/mix/MixGenerationState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/mix/MixGenerationState$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/mix/MixGenerationState$State;

.field public static final enum CANCELED:Lcom/google/android/music/mix/MixGenerationState$State;

.field public static final enum FAILED:Lcom/google/android/music/mix/MixGenerationState$State;

.field public static final enum FINISHED:Lcom/google/android/music/mix/MixGenerationState$State;

.field public static final enum NOT_STARTED:Lcom/google/android/music/mix/MixGenerationState$State;

.field public static final enum RUNNING:Lcom/google/android/music/mix/MixGenerationState$State;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, Lcom/google/android/music/mix/MixGenerationState$State;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/mix/MixGenerationState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/MixGenerationState$State;->NOT_STARTED:Lcom/google/android/music/mix/MixGenerationState$State;

    .line 17
    new-instance v0, Lcom/google/android/music/mix/MixGenerationState$State;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/mix/MixGenerationState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/MixGenerationState$State;->RUNNING:Lcom/google/android/music/mix/MixGenerationState$State;

    .line 18
    new-instance v0, Lcom/google/android/music/mix/MixGenerationState$State;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/mix/MixGenerationState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/MixGenerationState$State;->FINISHED:Lcom/google/android/music/mix/MixGenerationState$State;

    .line 19
    new-instance v0, Lcom/google/android/music/mix/MixGenerationState$State;

    const-string v1, "CANCELED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/mix/MixGenerationState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/MixGenerationState$State;->CANCELED:Lcom/google/android/music/mix/MixGenerationState$State;

    .line 20
    new-instance v0, Lcom/google/android/music/mix/MixGenerationState$State;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v6}, Lcom/google/android/music/mix/MixGenerationState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/MixGenerationState$State;->FAILED:Lcom/google/android/music/mix/MixGenerationState$State;

    .line 15
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/music/mix/MixGenerationState$State;

    sget-object v1, Lcom/google/android/music/mix/MixGenerationState$State;->NOT_STARTED:Lcom/google/android/music/mix/MixGenerationState$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/mix/MixGenerationState$State;->RUNNING:Lcom/google/android/music/mix/MixGenerationState$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/mix/MixGenerationState$State;->FINISHED:Lcom/google/android/music/mix/MixGenerationState$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/mix/MixGenerationState$State;->CANCELED:Lcom/google/android/music/mix/MixGenerationState$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/mix/MixGenerationState$State;->FAILED:Lcom/google/android/music/mix/MixGenerationState$State;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/music/mix/MixGenerationState$State;->$VALUES:[Lcom/google/android/music/mix/MixGenerationState$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/mix/MixGenerationState$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/google/android/music/mix/MixGenerationState$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/mix/MixGenerationState$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/mix/MixGenerationState$State;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/android/music/mix/MixGenerationState$State;->$VALUES:[Lcom/google/android/music/mix/MixGenerationState$State;

    invoke-virtual {v0}, [Lcom/google/android/music/mix/MixGenerationState$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/mix/MixGenerationState$State;

    return-object v0
.end method
