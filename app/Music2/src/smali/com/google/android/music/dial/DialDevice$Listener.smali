.class public interface abstract Lcom/google/android/music/dial/DialDevice$Listener;
.super Ljava/lang/Object;
.source "DialDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/DialDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/DialDevice$Listener$Error;
    }
.end annotation


# virtual methods
.method public abstract onError(Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Listener$Error;)V
.end method

.method public abstract onMessageReceived(Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)V
.end method
