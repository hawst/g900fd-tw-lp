.class final enum Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;
.super Ljava/lang/Enum;
.source "ModifyPlaylistFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/ModifyPlaylistFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ShareSwitchState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

.field public static final enum DISABLED:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

.field public static final enum ENABLED:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

.field public static final enum HIDDEN:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    new-instance v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    const-string v1, "ENABLED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->ENABLED:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    .line 60
    new-instance v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->DISABLED:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    .line 61
    new-instance v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->HIDDEN:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    .line 58
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    sget-object v1, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->ENABLED:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->DISABLED:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->HIDDEN:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->$VALUES:[Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    const-class v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->$VALUES:[Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    invoke-virtual {v0}, [Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    return-object v0
.end method
