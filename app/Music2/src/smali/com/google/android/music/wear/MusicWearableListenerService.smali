.class public Lcom/google/android/music/wear/MusicWearableListenerService;
.super Lcom/google/android/wearable/datatransfer/WearableDataListenerService;
.source "MusicWearableListenerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/wear/MusicWearableListenerService$1;,
        Lcom/google/android/music/wear/MusicWearableListenerService$MyGoogleApiClientCallbacks;
    }
.end annotation


# instance fields
.field private mCapabilityRefresher:Lcom/google/android/music/wear/NodeCapabilityRefresher;

.field private mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private mIntentReceiver:Lcom/google/android/wear/intentstarter/IntentReceiver;

.field private final mMyGoogleApiClientCallbacks:Lcom/google/android/music/wear/MusicWearableListenerService$MyGoogleApiClientCallbacks;

.field private mPlaybackLogsConsumer:Lcom/google/android/music/wear/PlaybackLogsConsumer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/WearableDataListenerService;-><init>()V

    .line 30
    new-instance v0, Lcom/google/android/music/wear/MusicWearableListenerService$MyGoogleApiClientCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/wear/MusicWearableListenerService$MyGoogleApiClientCallbacks;-><init>(Lcom/google/android/music/wear/MusicWearableListenerService;Lcom/google/android/music/wear/MusicWearableListenerService$1;)V

    iput-object v0, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mMyGoogleApiClientCallbacks:Lcom/google/android/music/wear/MusicWearableListenerService$MyGoogleApiClientCallbacks;

    .line 95
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 5

    .prologue
    .line 42
    invoke-super {p0}, Lcom/google/android/wearable/datatransfer/WearableDataListenerService;->onCreate()V

    .line 43
    new-instance v0, Lcom/google/android/wear/intentstarter/IntentReceiver;

    invoke-direct {v0, p0}, Lcom/google/android/wear/intentstarter/IntentReceiver;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mIntentReceiver:Lcom/google/android/wear/intentstarter/IntentReceiver;

    .line 44
    invoke-static {p0}, Lcom/google/android/music/wear/GmsUtils;->newClientBuilderWithErrorNotification(Landroid/content/Context;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mMyGoogleApiClientCallbacks:Lcom/google/android/music/wear/MusicWearableListenerService$MyGoogleApiClientCallbacks;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wearable/Wearable;->API:Lcom/google/android/gms/common/api/Api;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 48
    iget-object v0, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    .line 50
    new-instance v0, Lcom/google/android/music/wear/PlaybackLogsConsumer;

    new-instance v1, Lcom/google/android/music/wear/util/DefaultDataApiWrapper;

    iget-object v2, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-direct {v1, v2}, Lcom/google/android/music/wear/util/DefaultDataApiWrapper;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    new-instance v2, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;

    invoke-virtual {p0}, Lcom/google/android/music/wear/MusicWearableListenerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    new-instance v4, Lcom/google/android/music/wear/util/DefaultClock;

    invoke-direct {v4}, Lcom/google/android/music/wear/util/DefaultClock;-><init>()V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/music/wear/PlaybackLogsConsumer;-><init>(Lcom/google/android/music/wear/util/DataApiWrapper;Lcom/google/android/music/wear/WearMusicDataProvider;Ljava/util/concurrent/Executor;Lcom/google/android/music/wear/util/Clock;)V

    iput-object v0, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mPlaybackLogsConsumer:Lcom/google/android/music/wear/PlaybackLogsConsumer;

    .line 55
    invoke-static {p0}, Lcom/google/android/music/wear/NodeCapabilityRefresher;->createFromContext(Landroid/content/Context;)Lcom/google/android/music/wear/NodeCapabilityRefresher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mCapabilityRefresher:Lcom/google/android/music/wear/NodeCapabilityRefresher;

    .line 56
    return-void
.end method

.method public onDataChanged(Lcom/google/android/gms/wearable/DataEventBuffer;)V
    .locals 3
    .param p1, "dataEvents"    # Lcom/google/android/gms/wearable/DataEventBuffer;

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/google/android/wearable/datatransfer/WearableDataListenerService;->onDataChanged(Lcom/google/android/gms/wearable/DataEventBuffer;)V

    .line 82
    iget-object v2, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mPlaybackLogsConsumer:Lcom/google/android/music/wear/PlaybackLogsConsumer;

    invoke-virtual {v2}, Lcom/google/android/music/wear/PlaybackLogsConsumer;->consumePlaybackLogs()V

    .line 83
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/wearable/DataEventBuffer;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 84
    invoke-virtual {p1, v1}, Lcom/google/android/gms/wearable/DataEventBuffer;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/wearable/DataEvent;

    invoke-interface {v2}, Lcom/google/android/gms/wearable/DataEvent;->getDataItem()Lcom/google/android/gms/wearable/DataItem;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/wearable/DataItem;->getUri()Landroid/net/Uri;

    move-result-object v0

    .line 87
    .local v0, "dataItemUri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mCapabilityRefresher:Lcom/google/android/music/wear/NodeCapabilityRefresher;

    invoke-virtual {v2, v0}, Lcom/google/android/music/wear/NodeCapabilityRefresher;->shouldRefreshForDataItemUri(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 88
    iget-object v2, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mCapabilityRefresher:Lcom/google/android/music/wear/NodeCapabilityRefresher;

    invoke-virtual {v2}, Lcom/google/android/music/wear/NodeCapabilityRefresher;->refreshNodeCapabilities()V

    .line 92
    .end local v0    # "dataItemUri":Landroid/net/Uri;
    :cond_0
    return-void

    .line 83
    .restart local v0    # "dataItemUri":Landroid/net/Uri;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    .line 61
    invoke-super {p0}, Lcom/google/android/wearable/datatransfer/WearableDataListenerService;->onDestroy()V

    .line 62
    return-void
.end method

.method public onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)V
    .locals 1
    .param p1, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mIntentReceiver:Lcom/google/android/wear/intentstarter/IntentReceiver;

    invoke-virtual {v0, p1}, Lcom/google/android/wear/intentstarter/IntentReceiver;->handleIncomingIntentRequest(Lcom/google/android/gms/wearable/MessageEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    :goto_0
    return-void

    .line 69
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/wearable/datatransfer/WearableDataListenerService;->onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)V

    goto :goto_0
.end method

.method public onPeerConnected(Lcom/google/android/gms/wearable/Node;)V
    .locals 1
    .param p1, "peer"    # Lcom/google/android/gms/wearable/Node;

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/google/android/wearable/datatransfer/WearableDataListenerService;->onPeerConnected(Lcom/google/android/gms/wearable/Node;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mPlaybackLogsConsumer:Lcom/google/android/music/wear/PlaybackLogsConsumer;

    invoke-virtual {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer;->consumePlaybackLogs()V

    .line 76
    iget-object v0, p0, Lcom/google/android/music/wear/MusicWearableListenerService;->mCapabilityRefresher:Lcom/google/android/music/wear/NodeCapabilityRefresher;

    invoke-virtual {v0}, Lcom/google/android/music/wear/NodeCapabilityRefresher;->refreshNodeCapabilities()V

    .line 77
    return-void
.end method
