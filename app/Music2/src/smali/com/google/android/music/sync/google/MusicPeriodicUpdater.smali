.class public Lcom/google/android/music/sync/google/MusicPeriodicUpdater;
.super Ljava/lang/Object;
.source "MusicPeriodicUpdater.java"


# direct methods
.method public static checkAndEnablePeriodicUpdate(Landroid/accounts/Account;Landroid/content/Context;)Z
    .locals 14
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "music_periodic_sync_frequency_in_seconds"

    const-wide/32 v12, 0x15180

    invoke-static {v10, v11, v12, v13}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v8

    .line 69
    .local v8, "period":J
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-nez v10, :cond_0

    .line 70
    const-string v10, "MusicSyncAdapter"

    const-string v11, "Periodic sync is disabled"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-static {p0}, Lcom/google/android/music/sync/google/MusicPeriodicUpdater;->disablePeriodicSync(Landroid/accounts/Account;)V

    .line 73
    const/4 v10, 0x0

    .line 118
    :goto_0
    return v10

    .line 74
    :cond_0
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-gez v10, :cond_1

    .line 75
    const-string v10, "MusicSyncAdapter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Periodic sync frequency is invalid: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    const/4 v10, 0x0

    goto :goto_0

    .line 80
    :cond_1
    invoke-static {p0, v8, v9}, Lcom/google/android/music/sync/google/MusicPeriodicUpdater;->enablePeriodicSync(Landroid/accounts/Account;J)V

    .line 82
    const-string v10, "periodicUpdate.prefs"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 85
    .local v3, "prefs":Landroid/content/SharedPreferences;
    const-string v10, "lastRunAccountName"

    const/4 v11, 0x0

    invoke-interface {v3, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 86
    .local v1, "lastAccountName":Ljava/lang/String;
    const-string v10, "lastRunAccountType"

    const/4 v11, 0x0

    invoke-interface {v3, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 87
    .local v2, "lastAccountType":Ljava/lang/String;
    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    .line 88
    new-instance v0, Landroid/accounts/Account;

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .local v0, "lastAccount":Landroid/accounts/Account;
    invoke-virtual {v0, p0}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 90
    const-string v10, "MusicSyncAdapter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Account changed from "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " to "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-static {v0}, Lcom/google/android/music/sync/google/MusicPeriodicUpdater;->disablePeriodicSync(Landroid/accounts/Account;)V

    .line 93
    const/4 v10, 0x1

    goto :goto_0

    .line 96
    .end local v0    # "lastAccount":Landroid/accounts/Account;
    :cond_2
    const-string v10, "MusicSyncAdapter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "First periodic run for account: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 100
    .restart local v0    # "lastAccount":Landroid/accounts/Account;
    :cond_3
    const-string v10, "lastRunTime"

    const-wide/16 v12, 0x0

    invoke-interface {v3, v10, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long v4, v10, v12

    .line 101
    .local v4, "lastRunTimeInSec":J
    const-wide/16 v10, 0x0

    cmp-long v10, v4, v10

    if-nez v10, :cond_4

    .line 102
    const-string v10, "MusicSyncAdapter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "First periodic run for account: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 105
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long v6, v10, v12

    .line 106
    .local v6, "nowInSec":J
    add-long v10, v4, v8

    cmp-long v10, v6, v10

    if-ltz v10, :cond_5

    .line 107
    const-string v10, "MusicSyncAdapter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Will attempt periodic sync for account: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 109
    :cond_5
    sub-long v10, v4, v6

    const-wide/32 v12, 0x15180

    cmp-long v10, v10, v12

    if-lez v10, :cond_6

    .line 111
    const-string v10, "MusicSyncAdapter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Periodic sync last run time is too far in the future: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 115
    :cond_6
    const-string v10, "MusicSyncAdapter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Skipping periodic sync. Last sync was "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sub-long v12, v6, v4

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " seconds ago. Frequency: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    const/4 v10, 0x0

    goto/16 :goto_0
.end method

.method private static createPeriodicSyncExtras()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 143
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 144
    .local v0, "extras":Landroid/os/Bundle;
    const-string v1, "com.google.android.music.PERIODIC_SYNC"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    return-object v0
.end method

.method private static disablePeriodicSync(Landroid/accounts/Account;)V
    .locals 2
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 138
    const-string v0, "com.google.android.music.MusicContent"

    invoke-static {}, Lcom/google/android/music/sync/google/MusicPeriodicUpdater;->createPeriodicSyncExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {p0, v0, v1}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 140
    return-void
.end method

.method private static doPeriodicUpdate(Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 5
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 159
    const-string v3, "MusicSyncAdapter"

    const-string v4, "Periodic update"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    const/4 v0, 0x0

    .line 164
    .local v0, "isNautilus":Z
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 165
    .local v2, "ref":Ljava/lang/Object;
    invoke-static {p1, v2}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 167
    .local v1, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 169
    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 172
    if-eqz v0, :cond_0

    .line 173
    invoke-static {p0, p1}, Lcom/google/android/music/sync/google/MusicPeriodicUpdater;->populateEphemeralTop(Landroid/accounts/Account;Landroid/content/Context;)V

    .line 174
    invoke-static {p0, p1}, Lcom/google/android/music/sync/google/MusicPeriodicUpdater;->populateListenNowRecommendations(Landroid/accounts/Account;Landroid/content/Context;)V

    .line 176
    :cond_0
    return-void

    .line 169
    :catchall_0
    move-exception v3

    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v3
.end method

.method private static enablePeriodicSync(Landroid/accounts/Account;J)V
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "periodInSeconds"    # J

    .prologue
    .line 131
    const-wide/32 v0, 0x15180

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 132
    const-string v0, "com.google.android.music.MusicContent"

    invoke-static {}, Lcom/google/android/music/sync/google/MusicPeriodicUpdater;->createPeriodicSyncExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {p0, v0, v1, p1, p2}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 135
    :cond_0
    return-void
.end method

.method public static performPeriodicUpdate(Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/music/sync/google/MusicPeriodicUpdater;->doPeriodicUpdate(Landroid/accounts/Account;Landroid/content/Context;)V

    .line 51
    invoke-static {p0, p1}, Lcom/google/android/music/sync/google/MusicPeriodicUpdater;->recordSuccessfulRun(Landroid/accounts/Account;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    :goto_0
    return-void

    .line 52
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MusicSyncAdapter"

    const-string v2, "Periodic update failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static populateEphemeralTop(Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 9
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 180
    :try_start_0
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p1}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 181
    .local v0, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "music_max_ephemeral_top_size"

    const/16 v8, 0x3e8

    invoke-static {v6, v7, v8}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 186
    .local v3, "maxSize":I
    if-nez v3, :cond_0

    .line 208
    .end local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    .end local v3    # "maxSize":I
    :goto_0
    return-void

    .line 189
    .restart local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    .restart local v3    # "maxSize":I
    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface {v0, v3, v6, v7}, Lcom/google/android/music/cloudclient/MusicCloud;->getEphemeralTopTracks(ILjava/lang/String;I)Lcom/google/android/music/sync/google/model/TrackFeed;

    move-result-object v4

    .line 191
    .local v4, "response":Lcom/google/android/music/sync/google/model/TrackFeed;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/google/android/music/sync/google/model/TrackFeed;->getItemList()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v4}, Lcom/google/android/music/sync/google/model/TrackFeed;->getItemList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 193
    :cond_1
    const-string v6, "MusicSyncAdapter"

    const-string v7, "No ephemeral top trcks"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 203
    .end local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    .end local v3    # "maxSize":I
    .end local v4    # "response":Lcom/google/android/music/sync/google/model/TrackFeed;
    :catch_0
    move-exception v1

    .line 204
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v6, "MusicSyncAdapter"

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 199
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    .restart local v3    # "maxSize":I
    .restart local v4    # "response":Lcom/google/android/music/sync/google/model/TrackFeed;
    :cond_2
    :try_start_1
    invoke-virtual {v4}, Lcom/google/android/music/sync/google/model/TrackFeed;->getItemList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/sync/google/model/Track;

    .line 200
    .local v5, "track":Lcom/google/android/music/sync/google/model/Track;
    const/4 v6, 0x0

    iput-object v6, v5, Lcom/google/android/music/sync/google/model/Track;->mRemoteId:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 205
    .end local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "maxSize":I
    .end local v4    # "response":Lcom/google/android/music/sync/google/model/TrackFeed;
    .end local v5    # "track":Lcom/google/android/music/sync/google/model/Track;
    :catch_1
    move-exception v1

    .line 206
    .local v1, "e":Ljava/io/IOException;
    const-string v6, "MusicSyncAdapter"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 202
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "maxSize":I
    .restart local v4    # "response":Lcom/google/android/music/sync/google/model/TrackFeed;
    :cond_3
    :try_start_2
    invoke-static {p1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/android/music/sync/google/model/TrackFeed;->getItemList()Ljava/util/List;

    move-result-object v7

    invoke-virtual {v6, p0, v7}, Lcom/google/android/music/store/Store;->populateEphemeralTop(Landroid/accounts/Account;Ljava/util/List;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0
.end method

.method private static populateListenNowRecommendations(Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 5
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 217
    :try_start_0
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p1}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 218
    .local v0, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    invoke-interface {v0}, Lcom/google/android/music/cloudclient/MusicCloud;->getListenNowRecommendations()Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

    move-result-object v2

    .line 219
    .local v2, "response":Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;
    invoke-static {p1, v2}, Lcom/google/android/music/store/RecentItemsManager;->insertLockerRecommendations(Landroid/content/Context;Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 225
    .end local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    .end local v2    # "response":Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;
    :goto_0
    return-void

    .line 220
    :catch_0
    move-exception v1

    .line 221
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v3, "MusicSyncAdapter"

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 222
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 223
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "MusicSyncAdapter"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static recordSuccessfulRun(Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 6
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 149
    const-string v2, "periodicUpdate.prefs"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 151
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 152
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "lastRunTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 153
    const-string v2, "lastRunAccountName"

    iget-object v3, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 154
    const-string v2, "lastRunAccountType"

    iget-object v3, p0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 155
    invoke-static {v0}, Lcom/google/android/music/SharedPreferencesCompat;->apply(Landroid/content/SharedPreferences$Editor;)V

    .line 156
    return-void
.end method

.method public static resetLastRunTime(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 233
    const-string v2, "periodicUpdate.prefs"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 235
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 236
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "lastRunTime"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 237
    invoke-static {v0}, Lcom/google/android/music/SharedPreferencesCompat;->apply(Landroid/content/SharedPreferences$Editor;)V

    .line 238
    return-void
.end method
