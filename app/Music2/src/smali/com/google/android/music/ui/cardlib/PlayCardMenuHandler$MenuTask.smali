.class public Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuTask;
.super Ljava/lang/Object;
.source "PlayCardMenuHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MenuTask"
.end annotation


# instance fields
.field private final mEntry:Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;)V
    .locals 0
    .param p1, "entry"    # Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuTask;->mEntry:Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;

    .line 106
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuTask;->mEntry:Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;->onActionSelected()V

    .line 111
    return-void
.end method
