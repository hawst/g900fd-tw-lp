.class public abstract Lcom/google/android/music/ui/MediaListCursorAdapter;
.super Landroid/support/v4/widget/SimpleCursorAdapter;
.source "MediaListCursorAdapter.java"


# instance fields
.field private final mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

.field private mRequiredIdColumnIndex:I


# direct methods
.method protected constructor <init>(Lcom/google/android/music/ui/MusicFragment;I)V
    .locals 7
    .param p1, "musicFragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p2, "layout"    # I

    .prologue
    const/4 v6, 0x0

    .line 31
    invoke-interface {p1}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v3, 0x0

    new-array v4, v6, [Ljava/lang/String;

    new-array v5, v6, [I

    move-object v0, p0

    move v2, p2

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/ui/MediaListCursorAdapter;->mRequiredIdColumnIndex:I

    .line 33
    iput-object p1, p0, Lcom/google/android/music/ui/MediaListCursorAdapter;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    .line 34
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/music/ui/MusicFragment;ILandroid/database/Cursor;)V
    .locals 6
    .param p1, "musicFragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p2, "layout"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-interface {p1}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-array v4, v2, [Ljava/lang/String;

    new-array v5, v2, [I

    move-object v0, p0

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/ui/MediaListCursorAdapter;->mRequiredIdColumnIndex:I

    .line 48
    invoke-direct {p0, p3}, Lcom/google/android/music/ui/MediaListCursorAdapter;->setRequiredIdColumnIndex(Landroid/database/Cursor;)V

    .line 49
    iput-object p1, p0, Lcom/google/android/music/ui/MediaListCursorAdapter;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    .line 50
    return-void
.end method

.method private setRequiredIdColumnIndex(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 121
    if-eqz p1, :cond_0

    .line 122
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/MediaListCursorAdapter;->mRequiredIdColumnIndex:I

    .line 124
    :cond_0
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 58
    invoke-virtual {p0, p3}, Lcom/google/android/music/ui/MediaListCursorAdapter;->hasRowData(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    iget v0, p0, Lcom/google/android/music/ui/MediaListCursorAdapter;->mRequiredIdColumnIndex:I

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .local v4, "id":J
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 60
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/music/ui/MediaListCursorAdapter;->bindViewToMediaListItem(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;J)V

    .line 64
    .end local v4    # "id":J
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/MediaListCursorAdapter;->bindViewToLoadingItem(Landroid/view/View;Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected abstract bindViewToLoadingItem(Landroid/view/View;Landroid/content/Context;)V
.end method

.method protected abstract bindViewToMediaListItem(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;J)V
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 106
    iget-object v1, p0, Lcom/google/android/music/ui/MediaListCursorAdapter;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v1}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 107
    .local v0, "activity":Landroid/app/Activity;
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 109
    const/4 p1, 0x0

    .line 111
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/SimpleCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 112
    return-void
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListCursorAdapter;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v0}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method protected getFragment()Lcom/google/android/music/ui/MusicFragment;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListCursorAdapter;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    return-object v0
.end method

.method protected hasRowData(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 83
    if-nez p1, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/music/ui/MediaListCursorAdapter;->mRequiredIdColumnIndex:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected isActivityValid()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 130
    iget-object v2, p0, Lcom/google/android/music/ui/MediaListCursorAdapter;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v2}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 131
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_1

    .line 143
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_0
    :goto_0
    return v1

    .line 135
    .restart local v0    # "activity":Landroid/app/Activity;
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 138
    instance-of v2, v0, Lcom/google/android/music/ui/BaseActivity;

    if-eqz v2, :cond_2

    .line 139
    check-cast v0, Lcom/google/android/music/ui/BaseActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isActivityDestroyed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 143
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/MediaListCursorAdapter;->setRequiredIdColumnIndex(Landroid/database/Cursor;)V

    .line 117
    invoke-super {p0, p1}, Landroid/support/v4/widget/SimpleCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
