.class public Lcom/google/android/music/sharedpreview/SharedPreviewClient;
.super Ljava/lang/Object;
.source "SharedPreviewClient.java"


# instance fields
.field private final LOGV:Z

.field private final mContext:Landroid/content/Context;

.field private final mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

.field private final mLogHttp:Z

.field private final mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->LOGV:Z

    .line 27
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->HTTP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->mLogHttp:Z

    .line 40
    iput-object p1, p0, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->mContext:Landroid/content/Context;

    .line 41
    iget-object v0, p0, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/cloudclient/MusicRequest;->getSharedHttpClient(Landroid/content/Context;)Lcom/google/android/music/cloudclient/MusicHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    .line 42
    invoke-static {p1, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 43
    return-void
.end method


# virtual methods
.method public getMetaDataResponse(Ljava/lang/String;)Lcom/google/android/music/sharedpreview/JsonResponse;
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->getResponse(Ljava/lang/String;Z)Lcom/google/android/music/sharedpreview/JsonResponse;

    move-result-object v0

    return-object v0
.end method

.method public getPreviewResponse(Ljava/lang/String;)Lcom/google/android/music/sharedpreview/PreviewResponse;
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 51
    const/4 v2, 0x1

    invoke-virtual {p0, p1, v2}, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->getResponse(Ljava/lang/String;Z)Lcom/google/android/music/sharedpreview/JsonResponse;

    move-result-object v1

    .line 52
    .local v1, "res":Lcom/google/android/music/sharedpreview/JsonResponse;
    if-nez v1, :cond_0

    .line 53
    const-string v2, "SharedPreviewClient"

    const-string v3, "Failed to gret preview response"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :goto_0
    return-object v0

    .line 56
    :cond_0
    instance-of v2, v1, Lcom/google/android/music/sharedpreview/PreviewResponse;

    if-nez v2, :cond_1

    .line 57
    const-string v2, "SharedPreviewClient"

    const-string v3, "Received wrong response"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 60
    check-cast v0, Lcom/google/android/music/sharedpreview/PreviewResponse;

    .line 62
    .local v0, "preview":Lcom/google/android/music/sharedpreview/PreviewResponse;
    goto :goto_0
.end method

.method public getResponse(Ljava/lang/String;Z)Lcom/google/android/music/sharedpreview/JsonResponse;
    .locals 13
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "isStreaming"    # Z

    .prologue
    .line 80
    const/4 v9, 0x0

    .line 82
    .local v9, "response":Lorg/apache/http/HttpResponse;
    :try_start_0
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "&o=json&u=0"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v4, v10}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 83
    .local v4, "httpRequest":Lorg/apache/http/client/methods/HttpGet;
    new-instance v8, Lcom/google/android/music/cloudclient/MusicRequest;

    iget-object v10, p0, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->mContext:Landroid/content/Context;

    iget-object v11, p0, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-direct {v8, v10, v11}, Lcom/google/android/music/cloudclient/MusicRequest;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 84
    .local v8, "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    iget-object v10, p0, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    invoke-virtual {v8, v4, v10}, Lcom/google/android/music/cloudclient/MusicRequest;->sendRequest(Lorg/apache/http/client/methods/HttpRequestBase;Lcom/google/android/music/cloudclient/MusicHttpClient;)Lorg/apache/http/HttpResponse;

    move-result-object v9

    .line 86
    const/4 v7, 0x0

    .line 88
    .local v7, "jsonResponse":Lcom/google/android/music/sharedpreview/JsonResponse;
    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    .line 89
    .local v3, "entity":Lorg/apache/http/HttpEntity;
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 92
    .local v5, "inputStream":Ljava/io/InputStream;
    if-eqz p2, :cond_1

    .line 93
    :try_start_1
    invoke-static {v5}, Lcom/google/android/music/sharedpreview/PreviewResponse;->parseFromJsonInputStream(Ljava/io/InputStream;)Lcom/google/android/music/sharedpreview/PreviewResponse;

    move-result-object v7

    .line 125
    :goto_0
    iget-boolean v10, p0, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->LOGV:Z

    if-eqz v10, :cond_0

    .line 126
    const-string v10, "SharedPreviewClient"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "jsonResponse="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 129
    :cond_0
    :try_start_2
    invoke-static {v5}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 130
    invoke-static {v4, v9}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 139
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v4    # "httpRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v5    # "inputStream":Ljava/io/InputStream;
    .end local v7    # "jsonResponse":Lcom/google/android/music/sharedpreview/JsonResponse;
    .end local v8    # "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    :goto_1
    return-object v7

    .line 96
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v4    # "httpRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v5    # "inputStream":Ljava/io/InputStream;
    .restart local v7    # "jsonResponse":Lcom/google/android/music/sharedpreview/JsonResponse;
    .restart local v8    # "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    :cond_1
    :try_start_3
    invoke-static {v5}, Lcom/google/common/io/ByteStreams;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v1

    .line 98
    .local v1, "byteResponse":[B
    iget-boolean v10, p0, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->mLogHttp:Z

    if-eqz v10, :cond_2

    .line 99
    sget-object v10, Lcom/google/android/music/utils/DebugUtils;->HTTP_TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Response: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :cond_2
    invoke-static {v5}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 104
    const/4 v5, 0x0

    .line 107
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 108
    .end local v5    # "inputStream":Ljava/io/InputStream;
    .local v6, "inputStream":Ljava/io/InputStream;
    :try_start_4
    invoke-static {v6}, Lcom/google/android/music/sharedpreview/SharedAlbumResponse;->parseFromJsonInputStream(Ljava/io/InputStream;)Lcom/google/android/music/sharedpreview/SharedAlbumResponse;

    move-result-object v7

    .line 112
    move-object v0, v7

    check-cast v0, Lcom/google/android/music/sharedpreview/SharedAlbumResponse;

    move-object v10, v0

    iget-object v10, v10, Lcom/google/android/music/sharedpreview/SharedAlbumResponse;->mAlbumArtist:Ljava/lang/String;

    if-nez v10, :cond_4

    .line 113
    iget-boolean v10, p0, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->LOGV:Z

    if-eqz v10, :cond_3

    .line 114
    const-string v10, "SharedPreviewClient"

    const-string v11, "Try to parse as shared song"

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_3
    invoke-static {v6}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 118
    const/4 v5, 0x0

    .line 121
    .end local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v5    # "inputStream":Ljava/io/InputStream;
    :try_start_5
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 122
    .end local v5    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    :try_start_6
    invoke-static {v6}, Lcom/google/android/music/sharedpreview/SharedSongResponse;->parseFromJsonInputStream(Ljava/io/InputStream;)Lcom/google/android/music/sharedpreview/SharedSongResponse;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v7

    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v5    # "inputStream":Ljava/io/InputStream;
    goto :goto_0

    .line 129
    .end local v1    # "byteResponse":[B
    :catchall_0
    move-exception v10

    :goto_2
    :try_start_7
    invoke-static {v5}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 130
    invoke-static {v4, v9}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    throw v10
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    .line 134
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v4    # "httpRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v5    # "inputStream":Ljava/io/InputStream;
    .end local v7    # "jsonResponse":Lcom/google/android/music/sharedpreview/JsonResponse;
    .end local v8    # "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    :catch_0
    move-exception v2

    .line 135
    .local v2, "e":Ljava/lang/InterruptedException;
    const-string v10, "SharedPreviewClient"

    const-string v11, "Exception: "

    invoke-static {v10, v11, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 139
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :goto_3
    const/4 v7, 0x0

    goto :goto_1

    .line 136
    :catch_1
    move-exception v2

    .line 137
    .local v2, "e":Ljava/io/IOException;
    const-string v10, "SharedPreviewClient"

    const-string v11, "Exception: "

    invoke-static {v10, v11, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 129
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "byteResponse":[B
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v4    # "httpRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v7    # "jsonResponse":Lcom/google/android/music/sharedpreview/JsonResponse;
    .restart local v8    # "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    :catchall_1
    move-exception v10

    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v5    # "inputStream":Ljava/io/InputStream;
    goto :goto_2

    .end local v5    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    :cond_4
    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v5    # "inputStream":Ljava/io/InputStream;
    goto/16 :goto_0
.end method
