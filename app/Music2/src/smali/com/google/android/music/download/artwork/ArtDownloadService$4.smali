.class Lcom/google/android/music/download/artwork/ArtDownloadService$4;
.super Lcom/google/android/music/net/INetworkChangeListener$Stub;
.source "ArtDownloadService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/artwork/ArtDownloadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/artwork/ArtDownloadService;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/artwork/ArtDownloadService;)V
    .locals 0

    .prologue
    .line 678
    iput-object p1, p0, Lcom/google/android/music/download/artwork/ArtDownloadService$4;->this$0:Lcom/google/android/music/download/artwork/ArtDownloadService;

    invoke-direct {p0}, Lcom/google/android/music/net/INetworkChangeListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onNetworkChanged(ZZ)V
    .locals 2
    .param p1, "mobileConnected"    # Z
    .param p2, "wifiOrEthernetConnected"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 683
    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    .line 684
    :cond_0
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/download/artwork/ArtDownloadService$4$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/download/artwork/ArtDownloadService$4$1;-><init>(Lcom/google/android/music/download/artwork/ArtDownloadService$4;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 693
    :goto_0
    return-void

    .line 691
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService$4;->this$0:Lcom/google/android/music/download/artwork/ArtDownloadService;

    # invokes: Lcom/google/android/music/download/artwork/ArtDownloadService;->shutdownArtExecutorNow()V
    invoke-static {v0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->access$300(Lcom/google/android/music/download/artwork/ArtDownloadService;)V

    goto :goto_0
.end method
