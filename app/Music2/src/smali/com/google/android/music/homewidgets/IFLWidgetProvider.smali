.class public Lcom/google/android/music/homewidgets/IFLWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "IFLWidgetProvider.java"


# static fields
.field private static sTimeoutHandler:Landroid/os/Handler;

.field private static final sToken:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->sToken:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method public static canUseRadio(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 129
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "action":Ljava/lang/String;
    const-string v5, "com.google.android.music.STREAM_ONLY_ON_WIFI"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 135
    const-string v5, "stream_on_wifi_only"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 151
    .local v1, "isStreamOnWifiOnly":Z
    :goto_0
    const-string v5, "com.google.android.music.DOWNLOADED_ONLY_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 153
    const-string v5, "download_only"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 168
    .local v2, "localOnly":Z
    :goto_1
    invoke-static {p0, v1, v2}, Lcom/google/android/music/utils/MusicUtils;->canStartImFeelingLucky(Landroid/content/Context;ZZ)Z

    move-result v5

    return v5

    .line 139
    .end local v1    # "isStreamOnWifiOnly":Z
    .end local v2    # "localOnly":Z
    :cond_0
    new-instance v4, Ljava/lang/Object;

    invoke-direct {v4}, Ljava/lang/Object;-><init>()V

    .line 140
    .local v4, "refObject":Ljava/lang/Object;
    invoke-static {p0, v4}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    .line 143
    .local v3, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->isStreamOnlyOnWifi()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 145
    .restart local v1    # "isStreamOnWifiOnly":Z
    invoke-static {v4}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    .end local v1    # "isStreamOnWifiOnly":Z
    :catchall_0
    move-exception v5

    invoke-static {v4}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v5

    .line 157
    .end local v3    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v4    # "refObject":Ljava/lang/Object;
    .restart local v1    # "isStreamOnWifiOnly":Z
    :cond_1
    new-instance v4, Ljava/lang/Object;

    invoke-direct {v4}, Ljava/lang/Object;-><init>()V

    .line 158
    .restart local v4    # "refObject":Ljava/lang/Object;
    invoke-static {p0, v4}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    .line 161
    .restart local v3    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_1
    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    .line 163
    .restart local v2    # "localOnly":Z
    invoke-static {v4}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_1

    .end local v2    # "localOnly":Z
    :catchall_1
    move-exception v5

    invoke-static {v4}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v5
.end method

.method public static notifyChange(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nowPlaying"    # Landroid/content/Intent;
    .param p2, "what"    # Ljava/lang/String;

    .prologue
    .line 177
    const-string v1, "com.android.music.asyncopencomplete"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 179
    :cond_0
    const-string v1, "playing"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 182
    .local v0, "isPlaying":Z
    if-eqz v0, :cond_1

    .line 183
    new-instance v1, Lcom/google/android/music/homewidgets/IFLWidgetProvider$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/homewidgets/IFLWidgetProvider$1;-><init>(Landroid/content/Context;)V

    invoke-static {v1, p0}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 192
    .end local v0    # "isPlaying":Z
    :cond_1
    return-void
.end method

.method protected static setLoadingIndicatorVisibility(Landroid/content/Context;Z)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "visible"    # Z

    .prologue
    .line 204
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 206
    new-instance v0, Landroid/content/Intent;

    const-string v1, "update_widget_state"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0, p1}, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->updateWidget(Landroid/content/Context;Landroid/content/Intent;Z)V

    .line 208
    sget-object v0, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->sTimeoutHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 209
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->sTimeoutHandler:Landroid/os/Handler;

    .line 213
    :cond_0
    sget-object v0, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->sTimeoutHandler:Landroid/os/Handler;

    sget-object v1, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->sToken:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 214
    if-eqz p1, :cond_1

    .line 216
    sget-object v0, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->sTimeoutHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/music/homewidgets/IFLWidgetProvider$2;

    invoke-direct {v1, p0}, Lcom/google/android/music/homewidgets/IFLWidgetProvider$2;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->sToken:Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3a98

    add-long/2addr v4, v6

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;Ljava/lang/Object;J)Z

    .line 224
    :cond_1
    return-void
.end method

.method private static updateWidget(Landroid/content/Context;Landroid/content/Intent;Z)V
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "showLoadingIndicator"    # Z

    .prologue
    .line 73
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 74
    .local v2, "awm":Landroid/appwidget/AppWidgetManager;
    new-instance v9, Landroid/content/ComponentName;

    const-class v12, Lcom/google/android/music/homewidgets/IFLWidgetProvider;

    invoke-direct {v9, p0, v12}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 75
    .local v9, "widgetComponent":Landroid/content/ComponentName;
    invoke-virtual {v2, v9}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v11

    .line 78
    .local v11, "widgetIds":[I
    new-instance v8, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v12

    const v13, 0x7f04010b

    invoke-direct {v8, v12, v13}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 81
    .local v8, "views":Landroid/widget/RemoteViews;
    array-length v12, v11

    if-lez v12, :cond_3

    .line 83
    invoke-static/range {p0 .. p1}, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->canUseRadio(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v7

    .line 86
    .local v7, "showRadio":Z
    const v13, 0x7f0e0296

    if-eqz v7, :cond_0

    const/4 v12, 0x0

    :goto_0
    invoke-virtual {v8, v13, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 88
    const v13, 0x7f0e0297

    if-nez v7, :cond_1

    if-nez p2, :cond_1

    const/4 v12, 0x0

    :goto_1
    invoke-virtual {v8, v13, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 90
    const v13, 0x7f0e0298

    if-eqz p2, :cond_2

    const/4 v12, 0x0

    :goto_2
    invoke-virtual {v8, v13, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 94
    new-instance v0, Landroid/content/Intent;

    const-class v12, Lcom/google/android/music/homewidgets/IFLWidgetActivity;

    invoke-direct {v0, p0, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 95
    .local v0, "activity_intent":Landroid/content/Intent;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    const/high16 v12, 0x10000000

    invoke-virtual {v0, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 97
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {p0, v12, v0, v13}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 101
    .local v6, "pendingIntent":Landroid/app/PendingIntent;
    const v12, 0x7f0e0296

    invoke-virtual {v8, v12, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 102
    const v12, 0x7f0e0297

    invoke-virtual {v8, v12, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 105
    const/4 v12, 0x0

    new-instance v13, Landroid/content/Intent;

    const-string v14, "com.google.android.music.IFL_WIDGET_CANCEL"

    invoke-direct {v13, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v14, 0x8000000

    invoke-static {p0, v12, v13, v14}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 109
    .local v3, "cancelIntent":Landroid/app/PendingIntent;
    const v12, 0x7f0e0298

    invoke-virtual {v8, v12, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 112
    move-object v1, v11

    .local v1, "arr$":[I
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_3
    if-ge v4, v5, :cond_3

    aget v10, v1, v4

    .line 113
    .local v10, "widgetId":I
    invoke-virtual {v2, v10, v8}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 112
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 86
    .end local v0    # "activity_intent":Landroid/content/Intent;
    .end local v1    # "arr$":[I
    .end local v3    # "cancelIntent":Landroid/app/PendingIntent;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v10    # "widgetId":I
    :cond_0
    const/16 v12, 0x8

    goto :goto_0

    .line 88
    :cond_1
    const/16 v12, 0x8

    goto :goto_1

    .line 90
    :cond_2
    const/16 v12, 0x8

    goto :goto_2

    .line 116
    .end local v7    # "showRadio":Z
    :cond_3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 51
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.google.android.music.DOWNLOADED_ONLY_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.music.STREAM_ONLY_ON_WIFI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "update_widget_state"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 61
    :cond_0
    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->updateWidget(Landroid/content/Context;Landroid/content/Intent;Z)V

    .line 63
    :cond_1
    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 43
    new-instance v0, Landroid/content/Intent;

    const-string v1, "update_widget_state"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 44
    return-void
.end method
