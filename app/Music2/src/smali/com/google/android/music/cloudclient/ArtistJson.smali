.class public Lcom/google/android/music/cloudclient/ArtistJson;
.super Lcom/google/api/client/json/GenericJson;
.source "ArtistJson.java"


# instance fields
.field public mAlbums:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albums"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/AlbumJson;",
            ">;"
        }
    .end annotation
.end field

.field public mArtistArtRef:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artistArtRef"
    .end annotation
.end field

.field public mArtistBio:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artistBio"
    .end annotation
.end field

.field public mArtistId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artistId"
    .end annotation
.end field

.field public mName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "name"
    .end annotation
.end field

.field public mRelatedArtists:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "related_artists"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/ArtistJson;",
            ">;"
        }
    .end annotation
.end field

.field public mTopTracks:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "topTracks"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;"
        }
    .end annotation
.end field

.field public mTotalTrackCount:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "totalTrackCount"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
