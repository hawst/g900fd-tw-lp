.class public Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "BaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AccountSwitchWarningDialogFragment"
.end annotation


# instance fields
.field private mParentActivity:Lcom/google/android/music/ui/BaseActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1558
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1562
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 1563
    instance-of v0, p1, Lcom/google/android/music/ui/BaseActivity;

    if-eqz v0, :cond_0

    .line 1564
    check-cast p1, Lcom/google/android/music/ui/BaseActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;->mParentActivity:Lcom/google/android/music/ui/BaseActivity;

    .line 1569
    return-void

    .line 1566
    .restart local p1    # "activity":Landroid/app/Activity;
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This dialog can only be attached to a BaseActivity derivative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 1618
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 1619
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;->mParentActivity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->updateMusicDrawer()V

    .line 1620
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 1573
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/utils/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1574
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1575
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0b00e9

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1576
    const v2, 0x7f0b020e

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1577
    const v2, 0x7f0b0211

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1578
    const v2, 0x7f0b020d

    new-instance v3, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment$1;-><init>(Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1599
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 1606
    :goto_0
    return-object v1

    .line 1601
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1602
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0b0216

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1603
    const v2, 0x7f0b020f

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1604
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 1605
    .local v1, "d":Landroid/app/Dialog;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 1612
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1613
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;->mParentActivity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->updateMusicDrawer()V

    .line 1614
    return-void
.end method
