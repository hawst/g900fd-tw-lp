.class public Lcom/google/android/music/ui/ExploreFragment;
.super Lcom/google/android/music/ui/SubFragmentsPagerFragment;
.source "ExploreFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 15

    .prologue
    const/4 v6, 0x1

    .line 14
    invoke-direct {p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;-><init>()V

    .line 16
    invoke-virtual {p0}, Lcom/google/android/music/ui/ExploreFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v13

    .line 17
    .local v13, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v13}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v5, 0x3f000000    # 0.5f

    .line 19
    .local v5, "genreWidth":F
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/music/ui/GenresExploreFragment;->buildArguments(I)Landroid/os/Bundle;

    move-result-object v4

    .line 21
    .local v4, "bundle":Landroid/os/Bundle;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 22
    .local v14, "tabs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/FragmentTabInfo;>;"
    new-instance v0, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v1, "genres"

    const v2, 0x7f0b00a7

    const-class v3, Lcom/google/android/music/ui/GenresExploreFragment;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;FZ)V

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    new-instance v7, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v8, "top_charts"

    const v9, 0x7f0b00a6

    const-class v10, Lcom/google/android/music/ui/TopChartsExploreFragment;

    move-object v11, v4

    move v12, v6

    invoke-direct/range {v7 .. v12}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;Z)V

    invoke-virtual {v14, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    new-instance v7, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v8, "new_releases"

    const v9, 0x7f0b00a5

    const-class v10, Lcom/google/android/music/ui/NewReleasesExploreFragment;

    move-object v11, v4

    move v12, v6

    invoke-direct/range {v7 .. v12}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;Z)V

    invoke-virtual {v14, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    new-instance v0, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v1, "recommended"

    const v2, 0x7f0b00a4

    const-class v3, Lcom/google/android/music/ui/RecommendedExploreFragment;

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Z)V

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    const-string v0, "top_charts"

    invoke-virtual {p0, v14, v0}, Lcom/google/android/music/ui/ExploreFragment;->init(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 31
    return-void

    .line 17
    .end local v4    # "bundle":Landroid/os/Bundle;
    .end local v5    # "genreWidth":F
    .end local v14    # "tabs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/FragmentTabInfo;>;"
    :cond_0
    const/high16 v5, 0x3f800000    # 1.0f

    goto :goto_0
.end method
