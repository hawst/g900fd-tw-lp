.class Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;
.super Ljava/lang/Object;
.source "AlbumArtUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/AlbumArtUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MutableBitmapPool"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;,
        Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;
    }
.end annotation


# instance fields
.field mCapacityBytes:I

.field mCapacityItems:I

.field private mHead:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

.field mLargestItemBytes:I

.field private mMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;",
            ">;>;"
        }
    .end annotation
.end field

.field mSizeBytes:I

.field mSizeItems:I

.field private mTail:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;


# direct methods
.method public constructor <init>(III)V
    .locals 1
    .param p1, "capacityItems"    # I
    .param p2, "capacityBytes"    # I
    .param p3, "largestItemBytes"    # I

    .prologue
    .line 317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 318
    iput p1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mCapacityItems:I

    .line 319
    iput p2, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mCapacityBytes:I

    .line 320
    iget v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mCapacityBytes:I

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mLargestItemBytes:I

    .line 321
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mMap:Ljava/util/Map;

    .line 322
    return-void
.end method

.method static synthetic access$000(Landroid/graphics/Bitmap;)I
    .locals 1
    .param p0, "x0"    # Landroid/graphics/Bitmap;

    .prologue
    .line 240
    invoke-static {p0}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->getBitmapSizeBytes(Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method

.method private addToList(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;)V
    .locals 2
    .param p1, "e"    # Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .prologue
    .line 412
    iget v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mSizeItems:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mSizeItems:I

    .line 413
    iget v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mSizeBytes:I

    invoke-virtual {p1}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->getSizeBytes()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mSizeBytes:I

    .line 418
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mHead:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    if-nez v0, :cond_0

    .line 419
    iput-object p1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mHead:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .line 420
    iput-object p1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mTail:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .line 426
    :goto_0
    return-void

    .line 422
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mHead:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    # setter for: Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mPrevious:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    invoke-static {v0, p1}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->access$202(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;)Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .line 423
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mHead:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    # setter for: Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mNext:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    invoke-static {p1, v0}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->access$302(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;)Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .line 424
    iput-object p1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mHead:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    goto :goto_0
.end method

.method private static bytesPerPixel(Landroid/graphics/Bitmap$Config;)I
    .locals 4
    .param p0, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    const/4 v1, 0x4

    const/4 v0, 0x2

    .line 465
    if-nez p0, :cond_0

    .line 466
    const-string v0, "AlbumArtUtils"

    const-string v1, "Null bitmap config, returning something huge to prevent caching"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    const/16 v0, 0x400

    .line 479
    :goto_0
    :pswitch_0
    return v0

    .line 469
    :cond_0
    sget-object v2, Lcom/google/android/music/utils/AlbumArtUtils$4;->$SwitchMap$android$graphics$Bitmap$Config:[I

    invoke-virtual {p0}, Landroid/graphics/Bitmap$Config;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 479
    goto :goto_0

    .line 471
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 475
    goto :goto_0

    .line 469
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public static estimatedSize(IILandroid/graphics/Bitmap$Config;)I
    .locals 2
    .param p0, "width"    # I
    .param p1, "height"    # I
    .param p2, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 461
    add-int/lit8 v0, p0, 0x3

    and-int/lit8 v0, v0, -0x4

    invoke-static {p2}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->bytesPerPixel(Landroid/graphics/Bitmap$Config;)I

    move-result v1

    mul-int/2addr v0, v1

    mul-int/2addr v0, p1

    return v0
.end method

.method private findFirstUsableBitmap(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "key"    # Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;

    .prologue
    .line 385
    iget-object v3, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/LinkedList;

    .line 386
    .local v2, "entries":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;>;"
    if-eqz v2, :cond_4

    .line 387
    const/4 v1, 0x0

    .line 388
    .local v1, "e":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    :goto_0
    if-nez v1, :cond_2

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 389
    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .line 390
    .local v0, "candidate":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    invoke-direct {p0, v0}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->removeFromList(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;)V

    .line 391
    iget-object v3, v0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 394
    :cond_0
    const-string v3, "AlbumArtUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "findFirstUsableBitmap candidate is recycled or null: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 397
    :cond_1
    move-object v1, v0

    goto :goto_0

    .line 400
    .end local v0    # "candidate":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    :cond_2
    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 401
    iget-object v3, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    :cond_3
    if-eqz v1, :cond_4

    .line 404
    iget-object v3, v1, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mBitmap:Landroid/graphics/Bitmap;

    # invokes: Lcom/google/android/music/utils/AlbumArtUtils;->throwIfRecycled(Landroid/graphics/Bitmap;)V
    invoke-static {v3}, Lcom/google/android/music/utils/AlbumArtUtils;->access$100(Landroid/graphics/Bitmap;)V

    .line 405
    iget-object v3, v1, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mBitmap:Landroid/graphics/Bitmap;

    .line 408
    .end local v1    # "e":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    :goto_1
    return-object v3

    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static getBitmapSizeBytes(Landroid/graphics/Bitmap;)I
    .locals 2
    .param p0, "b"    # Landroid/graphics/Bitmap;

    .prologue
    .line 452
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method private removeFromList(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;)V
    .locals 4
    .param p1, "e"    # Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .prologue
    .line 429
    iget v2, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mSizeItems:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mSizeItems:I

    .line 430
    iget v2, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mSizeBytes:I

    invoke-virtual {p1}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->getSizeBytes()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mSizeBytes:I

    .line 435
    # getter for: Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mPrevious:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    invoke-static {p1}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->access$200(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;)Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    move-result-object v1

    .line 436
    .local v1, "prev":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    # getter for: Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mNext:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    invoke-static {p1}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->access$300(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;)Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    move-result-object v0

    .line 437
    .local v0, "next":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    iget-object v2, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mHead:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    if-ne v2, p1, :cond_0

    .line 438
    iput-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mHead:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .line 440
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mTail:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    if-ne v2, p1, :cond_1

    .line 441
    iput-object v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mTail:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .line 443
    :cond_1
    if-eqz v1, :cond_2

    .line 444
    # setter for: Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mNext:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    invoke-static {v1, v0}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->access$302(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;)Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .line 446
    :cond_2
    if-eqz v0, :cond_3

    .line 447
    # setter for: Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mPrevious:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    invoke-static {v0, v1}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->access$202(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;)Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .line 449
    :cond_3
    return-void
.end method


# virtual methods
.method public declared-synchronized createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 325
    monitor-enter p0

    :try_start_0
    new-instance v2, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;-><init>(IILandroid/graphics/Bitmap$Config;)V

    .line 326
    .local v2, "key":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;
    invoke-direct {p0, v2}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->findFirstUsableBitmap(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 327
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 328
    iget v3, v2, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mWidth:I

    iget v4, v2, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mHeight:I

    iget-object v5, v2, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mConfig:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 333
    :goto_0
    monitor-exit p0

    return-object v0

    .line 330
    :cond_0
    :try_start_1
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 331
    .local v1, "c":Landroid/graphics/Canvas;
    const/4 v3, 0x0

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 325
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "c":Landroid/graphics/Canvas;
    .end local v2    # "key":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized recycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 9
    .param p1, "b"    # Landroid/graphics/Bitmap;

    .prologue
    .line 337
    monitor-enter p0

    :try_start_0
    # invokes: Lcom/google/android/music/utils/AlbumArtUtils;->throwIfRecycled(Landroid/graphics/Bitmap;)V
    invoke-static {p1}, Lcom/google/android/music/utils/AlbumArtUtils;->access$100(Landroid/graphics/Bitmap;)V

    .line 338
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    .line 339
    .local v1, "config":Landroid/graphics/Bitmap$Config;
    invoke-static {p1}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->getBitmapSizeBytes(Landroid/graphics/Bitmap;)I

    move-result v0

    .line 347
    .local v0, "bitmapSize":I
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v7

    if-eqz v7, :cond_0

    iget v7, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mLargestItemBytes:I

    if-le v0, v7, :cond_1

    .line 348
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 379
    :goto_0
    monitor-exit p0

    return-void

    .line 355
    :cond_1
    :try_start_1
    new-instance v4, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-direct {v4, v7, v8, v1}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;-><init>(IILandroid/graphics/Bitmap$Config;)V

    .line 356
    .local v4, "key":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;
    :cond_2
    :goto_1
    iget v7, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mCapacityItems:I

    iget v8, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mSizeItems:I

    if-eq v7, v8, :cond_3

    iget v7, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mSizeBytes:I

    add-int/2addr v7, v0

    iget v8, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mCapacityBytes:I

    if-le v7, v8, :cond_5

    .line 357
    :cond_3
    iget-object v7, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mTail:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    iget-object v5, v7, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mKey:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;

    .line 358
    .local v5, "lru":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;
    invoke-virtual {v5, v4}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 361
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 337
    .end local v0    # "bitmapSize":I
    .end local v1    # "config":Landroid/graphics/Bitmap$Config;
    .end local v4    # "key":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;
    .end local v5    # "lru":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 366
    .restart local v0    # "bitmapSize":I
    .restart local v1    # "config":Landroid/graphics/Bitmap$Config;
    .restart local v4    # "key":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;
    .restart local v5    # "lru":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;
    :cond_4
    :try_start_2
    invoke-direct {p0, v5}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->findFirstUsableBitmap(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 367
    .local v6, "victim":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_2

    .line 368
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 371
    .end local v5    # "lru":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;
    .end local v6    # "victim":Landroid/graphics/Bitmap;
    :cond_5
    new-instance v3, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    invoke-direct {v3, v4, p1}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;-><init>(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;Landroid/graphics/Bitmap;)V

    .line 372
    .local v3, "entry":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    invoke-direct {p0, v3}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->addToList(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;)V

    .line 373
    iget-object v7, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mMap:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/LinkedList;

    .line 374
    .local v2, "entries":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;>;"
    if-nez v2, :cond_6

    .line 375
    new-instance v2, Ljava/util/LinkedList;

    .end local v2    # "entries":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;>;"
    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 376
    .restart local v2    # "entries":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;>;"
    iget-object v7, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mMap:Ljava/util/Map;

    invoke-interface {v7, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    :cond_6
    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public willCacheBitmap(IILandroid/graphics/Bitmap$Config;)Z
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 457
    invoke-static {p1, p2, p3}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->estimatedSize(IILandroid/graphics/Bitmap$Config;)I

    move-result v0

    iget v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->mLargestItemBytes:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
