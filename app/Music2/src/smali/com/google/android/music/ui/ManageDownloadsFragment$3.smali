.class Lcom/google/android/music/ui/ManageDownloadsFragment$3;
.super Ljava/lang/Object;
.source "ManageDownloadsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ManageDownloadsFragment;->applyDifferences(Landroid/util/Pair;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

.field final synthetic val$toBeRemoved:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ManageDownloadsFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$3;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$3;->val$toBeRemoved:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 531
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$3;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mProcessedData:Ljava/util/LinkedHashSet;
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$1100(Lcom/google/android/music/ui/ManageDownloadsFragment;)Ljava/util/LinkedHashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$3;->val$toBeRemoved:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->removeAll(Ljava/util/Collection;)Z

    .line 532
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$3;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$3;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mProcessedData:Ljava/util/LinkedHashSet;
    invoke-static {v2}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$1100(Lcom/google/android/music/ui/ManageDownloadsFragment;)Ljava/util/LinkedHashSet;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    # setter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mListBackingData:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$1202(Lcom/google/android/music/ui/ManageDownloadsFragment;Ljava/util/List;)Ljava/util/List;

    .line 533
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$3;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mAdapter:Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$1300(Lcom/google/android/music/ui/ManageDownloadsFragment;)Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->notifyDataSetChanged()V

    .line 534
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$3;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # invokes: Lcom/google/android/music/ui/ManageDownloadsFragment;->updateEmptyScreenVisibility()V
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$1400(Lcom/google/android/music/ui/ManageDownloadsFragment;)V

    .line 535
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$3;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # invokes: Lcom/google/android/music/ui/ManageDownloadsFragment;->updateProgress()V
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$1500(Lcom/google/android/music/ui/ManageDownloadsFragment;)V

    .line 536
    return-void
.end method
