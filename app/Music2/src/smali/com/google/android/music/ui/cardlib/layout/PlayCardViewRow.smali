.class public Lcom/google/android/music/ui/cardlib/layout/PlayCardViewRow;
.super Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
.source "PlayCardViewRow.java"


# instance fields
.field private mRowBackground:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->onFinishInflate()V

    .line 28
    const v0, 0x7f0e01fc

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewRow;->mRowBackground:Landroid/view/View;

    .line 29
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 48
    invoke-super/range {p0 .. p5}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->onLayout(ZIIII)V

    .line 49
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 34
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewRow;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 36
    .local v1, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewRow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0095

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 37
    .local v0, "thumbnailHeight":I
    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int v3, v0, v3

    iget v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v0, v3, v4

    .line 38
    iget v3, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewRow;->mThumbnailAspectRatio:F

    int-to-float v4, v0

    mul-float/2addr v3, v4

    float-to-int v2, v3

    .line 39
    .local v2, "thumbnailWidth":I
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewRow;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput v0, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 41
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewRow;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v3, v2, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->setThumbnailMetrics(II)V

    .line 43
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->onMeasure(II)V

    .line 44
    return-void
.end method
