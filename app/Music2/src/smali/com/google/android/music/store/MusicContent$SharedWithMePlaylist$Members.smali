.class public final Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist$Members;
.super Ljava/lang/Object;
.source "MusicContent.java"

# interfaces
.implements Lcom/google/android/music/store/MusicContent$PlaylistMemberColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Members"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3369
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "shared_with_me_playlist"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist$Members;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public static getUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "shareToken"    # Ljava/lang/String;

    .prologue
    .line 3391
    sget-object v0, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist$Members;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
