.class final Lcom/google/android/music/mix/MixDescriptor$1;
.super Ljava/lang/Object;
.source "MixDescriptor.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/mix/MixDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/music/mix/MixDescriptor;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/mix/MixDescriptor;
    .locals 4
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    .line 300
    :try_start_0
    new-instance v1, Lcom/google/android/music/mix/MixDescriptor;

    const/4 v3, 0x0

    invoke-direct {v1, p1, v3}, Lcom/google/android/music/mix/MixDescriptor;-><init>(Landroid/os/Parcel;Lcom/google/android/music/mix/MixDescriptor$1;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 302
    :goto_0
    return-object v1

    .line 301
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/IllegalArgumentException;
    move-object v1, v2

    .line 302
    goto :goto_0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 296
    invoke-virtual {p0, p1}, Lcom/google/android/music/mix/MixDescriptor$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/music/mix/MixDescriptor;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 307
    new-array v0, p1, [Lcom/google/android/music/mix/MixDescriptor;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 296
    invoke-virtual {p0, p1}, Lcom/google/android/music/mix/MixDescriptor$1;->newArray(I)[Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v0

    return-object v0
.end method
