.class public Lcom/google/android/music/download/cache/TrackCacheService;
.super Lcom/google/android/music/download/cache/BaseCacheService;
.source "TrackCacheService.java"


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/cache/TrackCacheService;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/music/download/cache/BaseCacheService;-><init>()V

    return-void
.end method


# virtual methods
.method protected createCacheManager(Lcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/download/cache/BaseCacheManager;
    .locals 1
    .param p1, "musicPreferences"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 18
    new-instance v0, Lcom/google/android/music/download/cache/TrackCacheManager;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/download/cache/TrackCacheManager;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V

    return-object v0
.end method

.method public bridge synthetic onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "x0"    # Landroid/content/Intent;

    .prologue
    .line 12
    invoke-super {p0, p1}, Lcom/google/android/music/download/cache/BaseCacheService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic onCreate()V
    .locals 0

    .prologue
    .line 12
    invoke-super {p0}, Lcom/google/android/music/download/cache/BaseCacheService;->onCreate()V

    return-void
.end method

.method public bridge synthetic onDestroy()V
    .locals 0

    .prologue
    .line 12
    invoke-super {p0}, Lcom/google/android/music/download/cache/BaseCacheService;->onDestroy()V

    return-void
.end method

.method public bridge synthetic onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "x0"    # Landroid/content/Intent;
    .param p2, "x1"    # I
    .param p3, "x2"    # I

    .prologue
    .line 12
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/download/cache/BaseCacheService;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method
