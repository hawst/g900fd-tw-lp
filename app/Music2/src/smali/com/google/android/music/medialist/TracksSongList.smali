.class public Lcom/google/android/music/medialist/TracksSongList;
.super Lcom/google/android/music/medialist/ExternalSongList;
.source "TracksSongList.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TracksSongList"


# instance fields
.field private final mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field private final mTracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/music/store/ContainerDescriptor;)V
    .locals 4
    .param p1, "json"    # Ljava/lang/String;
    .param p2, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;

    .prologue
    .line 41
    sget-object v2, Lcom/google/android/music/download/ContentIdentifier$Domain;->NAUTILUS:Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-direct {p0, v2}, Lcom/google/android/music/medialist/ExternalSongList;-><init>(Lcom/google/android/music/download/ContentIdentifier$Domain;)V

    .line 44
    :try_start_0
    const-class v2, Lcom/google/android/music/cloudclient/MixJson;

    invoke-static {v2, p1}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonString(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/MixJson;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    .local v1, "mix":Lcom/google/android/music/cloudclient/MixJson;
    iget-object v2, v1, Lcom/google/android/music/cloudclient/MixJson;->mTracks:Ljava/util/List;

    iput-object v2, p0, Lcom/google/android/music/medialist/TracksSongList;->mTracks:Ljava/util/List;

    .line 49
    iput-object p2, p0, Lcom/google/android/music/medialist/TracksSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 50
    return-void

    .line 45
    .end local v1    # "mix":Lcom/google/android/music/cloudclient/MixJson;
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Failed to parse json"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static createList(Ljava/util/List;Lcom/google/android/music/store/ContainerDescriptor;)Lcom/google/android/music/medialist/TracksSongList;
    .locals 2
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;",
            "Lcom/google/android/music/store/ContainerDescriptor;",
            ")",
            "Lcom/google/android/music/medialist/TracksSongList;"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    new-instance v0, Lcom/google/android/music/medialist/TracksSongList;

    invoke-static {p0}, Lcom/google/android/music/medialist/TracksSongList;->encodeAsString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/music/medialist/TracksSongList;-><init>(Ljava/lang/String;Lcom/google/android/music/store/ContainerDescriptor;)V

    return-object v0
.end method

.method private static encodeAsString(Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    new-instance v0, Lcom/google/android/music/cloudclient/MixJson;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/MixJson;-><init>()V

    .line 34
    .local v0, "mix":Lcom/google/android/music/cloudclient/MixJson;
    iput-object p0, v0, Lcom/google/android/music/cloudclient/MixJson;->mTracks:Ljava/util/List;

    .line 35
    invoke-static {v0}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonString(Lcom/google/api/client/json/GenericJson;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public addToStore(Landroid/content/Context;Z)[J
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "addToLibrary"    # Z

    .prologue
    .line 74
    const/4 v9, 0x0

    .line 75
    .local v9, "streamingAccount":Landroid/accounts/Account;
    invoke-static {p1, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v7

    .line 77
    .local v7, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 79
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 82
    if-nez v9, :cond_1

    .line 83
    const/4 v6, 0x0

    .line 99
    :cond_0
    :goto_0
    return-object v6

    .line 79
    :catchall_0
    move-exception v10

    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v10

    .line 86
    :cond_1
    invoke-static {p1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v8

    .line 87
    .local v8, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v8, p0, v9, p2}, Lcom/google/android/music/store/Store;->insertSongs(Lcom/google/android/music/medialist/ExternalSongList;Landroid/accounts/Account;Z)Ljava/util/List;

    move-result-object v3

    .line 88
    .local v3, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v6, 0x0

    .line 89
    .local v6, "longLocalIds":[J
    if-eqz v3, :cond_2

    .line 90
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    new-array v6, v10, [J

    .line 91
    const/4 v0, 0x0

    .line 92
    .local v0, "i":I
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 93
    .local v4, "id":J
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aput-wide v4, v6, v0

    move v0, v1

    .line 94
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_1

    .line 96
    .end local v0    # "i":I
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "id":J
    :cond_2
    const-string v10, "TracksSongList"

    const-string v11, "No songs were inserted"

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 62
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/medialist/TracksSongList;->mTracks:Ljava/util/List;

    invoke-static {v2}, Lcom/google/android/music/medialist/TracksSongList;->encodeAsString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/medialist/TracksSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/music/medialist/TracksSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    return-object v0
.end method

.method public getTracks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/music/medialist/TracksSongList;->mTracks:Ljava/util/List;

    return-object v0
.end method

.method public isAddToStoreSupported()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    return v0
.end method
