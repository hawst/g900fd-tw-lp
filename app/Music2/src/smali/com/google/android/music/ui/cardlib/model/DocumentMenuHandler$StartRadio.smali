.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartRadio;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$AsyncMenuEntry;
.source "DocumentMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StartRadio"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V
    .locals 3
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 865
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartRadio;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 866
    const v1, 0x7f0e001f

    const v2, 0x7f0b023a

    # invokes: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->isDownloadedOnlyMode()Z
    invoke-static {p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$300(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p2, v1, v2, v0}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$AsyncMenuEntry;-><init>(Landroid/content/res/Resources;IIZ)V

    .line 867
    return-void

    .line 866
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;II)V
    .locals 1
    .param p2, "res"    # Landroid/content/res/Resources;
    .param p3, "menu"    # I
    .param p4, "text"    # I

    .prologue
    .line 869
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartRadio;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 870
    # invokes: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->isDownloadedOnlyMode()Z
    invoke-static {p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$300(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p2, p3, p4, v0}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$AsyncMenuEntry;-><init>(Landroid/content/res/Resources;IIZ)V

    .line 871
    return-void

    .line 870
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onActionSelected()V
    .locals 3

    .prologue
    .line 875
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartRadio;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # invokes: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->isDownloadedOnlyMode()Z
    invoke-static {v1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$300(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 876
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartRadio;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # invokes: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->showNotAvailableToast()V
    invoke-static {v1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$400(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)V

    .line 881
    :goto_0
    return-void

    .line 879
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartRadio;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartRadio;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    .line 880
    .local v0, "songlist":Lcom/google/android/music/medialist/SongList;
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartRadio;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/google/android/music/utils/MusicUtils;->playRadio(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0
.end method
