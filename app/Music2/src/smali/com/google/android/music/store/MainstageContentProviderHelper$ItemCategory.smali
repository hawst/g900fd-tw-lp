.class final enum Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
.super Ljava/lang/Enum;
.source "MainstageContentProviderHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/MainstageContentProviderHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ItemCategory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

.field public static final enum RECOMMENDATIONS:Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

.field public static final enum SUGGESTED_MIX:Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

.field private static sRangeMap:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Float;",
            "Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;",
            ">;"
        }
    .end annotation
.end field

.field private static final sRangeMapLock:Ljava/lang/Object;


# instance fields
.field private mMaxNumItems:I

.field private mProbability:F


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 660
    new-instance v0, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    const-string v1, "SUGGESTED_MIX"

    const/high16 v2, 0x3e800000    # 0.25f

    const/4 v3, 0x4

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;-><init>(Ljava/lang/String;IFI)V

    sput-object v0, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->SUGGESTED_MIX:Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    .line 661
    new-instance v0, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    const-string v1, "RECOMMENDATIONS"

    const/high16 v2, 0x3f400000    # 0.75f

    const v3, 0x7fffffff

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;-><init>(Ljava/lang/String;IFI)V

    sput-object v0, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->RECOMMENDATIONS:Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    .line 659
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    sget-object v1, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->SUGGESTED_MIX:Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->RECOMMENDATIONS:Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->$VALUES:[Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    .line 664
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->sRangeMapLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IFI)V
    .locals 0
    .param p3, "probability"    # F
    .param p4, "max"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FI)V"
        }
    .end annotation

    .prologue
    .line 676
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 677
    iput p3, p0, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->mProbability:F

    .line 678
    iput p4, p0, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->mMaxNumItems:I

    .line 679
    return-void
.end method

.method static synthetic access$000()Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    .locals 1

    .prologue
    .line 659
    invoke-static {}, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->getRandomCategory()Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    .prologue
    .line 659
    iget v0, p0, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->mMaxNumItems:I

    return v0
.end method

.method private static generateRangeMap()Ljava/util/TreeMap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Float;",
            "Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 711
    new-instance v6, Ljava/util/TreeMap;

    invoke-direct {v6}, Ljava/util/TreeMap;-><init>()V

    .line 712
    .local v6, "map":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Float;Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;>;"
    const/4 v0, 0x0

    .line 713
    .local v0, "accumProbability":F
    invoke-static {}, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->values()[Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    move-result-object v2

    .line 715
    .local v2, "categories":[Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    move-object v1, v2

    .local v1, "arr$":[Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v3, v1, v4

    .line 716
    .local v3, "category":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v6, v7, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 717
    iget v7, v3, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->mProbability:F

    add-float/2addr v0, v7

    .line 715
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 722
    .end local v3    # "category":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    :cond_0
    const/high16 v7, 0x3f800000    # 1.0f

    sub-float v7, v0, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    const v8, 0x3a83126f    # 0.001f

    cmpl-float v7, v7, v8

    if-lez v7, :cond_1

    .line 723
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "The probabilities do not add up to 1: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 727
    :cond_1
    return-object v6
.end method

.method private static getRandomCategory()Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    .locals 6

    .prologue
    .line 686
    sget-object v3, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->sRangeMapLock:Ljava/lang/Object;

    monitor-enter v3

    .line 687
    :try_start_0
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->sRangeMap:Ljava/util/TreeMap;

    if-nez v2, :cond_0

    .line 688
    invoke-static {}, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->generateRangeMap()Ljava/util/TreeMap;

    move-result-object v2

    sput-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->sRangeMap:Ljava/util/TreeMap;

    .line 690
    :cond_0
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    double-to-float v1, v4

    .line 691
    .local v1, "random":F
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->sRangeMap:Ljava/util/TreeMap;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 692
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Float;Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;>;"
    if-nez v0, :cond_2

    .line 693
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->sRangeMap:Ljava/util/TreeMap;

    invoke-virtual {v2}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 694
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v4, "Range map is empty?"

    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 702
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Float;Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 696
    .restart local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Float;Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;>;"
    :cond_1
    :try_start_1
    const-string v2, "MainstageContentProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to find floor entry for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/Exception;

    invoke-direct {v5}, Ljava/lang/Exception;-><init>()V

    invoke-static {v2, v4, v5}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 697
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->sRangeMap:Ljava/util/TreeMap;

    invoke-virtual {v2}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    monitor-exit v3

    .line 700
    :goto_0
    return-object v2

    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 659
    const-class v0, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    .locals 1

    .prologue
    .line 659
    sget-object v0, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->$VALUES:[Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    invoke-virtual {v0}, [Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    return-object v0
.end method
