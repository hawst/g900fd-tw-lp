.class public Lcom/google/android/music/sync/google/MusicUpstreamReader;
.super Lcom/google/android/music/sync/common/UpstreamReader;
.source "MusicUpstreamReader.java"


# instance fields
.field private final mAccountId:I

.field private final mEnableTrackDeletes:Z

.field private final mStore:Lcom/google/android/music/store/Store;

.field private final mTag:Ljava/lang/String;

.field private final mUseVerboseLogging:Z


# direct methods
.method public constructor <init>(Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "queue"    # Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;
    .param p2, "context"    # Landroid/content/Context;
    .param p4, "logTag"    # Ljava/lang/String;
    .param p5, "enableTrackDeletes"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p3, "protocolState":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p0, p1, p4}, Lcom/google/android/music/sync/common/UpstreamReader;-><init>(Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;Ljava/lang/String;)V

    .line 48
    iput-object p4, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    .line 49
    invoke-static {p2}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    .line 50
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->SYNC:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mUseVerboseLogging:Z

    .line 51
    const-class v0, Ljava/lang/Integer;

    const-string v1, "remote_account"

    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mAccountId:I

    .line 52
    iput-boolean p5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mEnableTrackDeletes:Z

    .line 53
    return-void
.end method

.method private fillQueueWithBlacklistItemChanges()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 424
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v5}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 425
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 427
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {v1}, Lcom/google/android/music/store/BlacklistItem;->getBlacklistItemsToSync(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v0

    .line 428
    const/4 v3, 0x0

    .line 429
    .local v3, "recordsRead":I
    if-eqz v0, :cond_0

    .line 430
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 431
    add-int/lit8 v3, v3, 0x1

    .line 432
    new-instance v2, Lcom/google/android/music/store/BlacklistItem;

    invoke-direct {v2}, Lcom/google/android/music/store/BlacklistItem;-><init>()V

    .line 433
    .local v2, "dismissedItem":Lcom/google/android/music/store/BlacklistItem;
    invoke-virtual {v2, v0}, Lcom/google/android/music/store/BlacklistItem;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V

    .line 434
    invoke-static {v2}, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->parse(Lcom/google/android/music/store/BlacklistItem;)Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    move-result-object v4

    .line 435
    .local v4, "syncable":Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;
    invoke-direct {p0, v4}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->putItemInQueue(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 440
    .end local v2    # "dismissedItem":Lcom/google/android/music/store/BlacklistItem;
    .end local v3    # "recordsRead":I
    .end local v4    # "syncable":Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;
    :catchall_0
    move-exception v5

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 441
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v6, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v5

    .line 440
    .restart local v3    # "recordsRead":I
    :cond_0
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 441
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v5, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    return v3
.end method

.method private fillQueueWithEntryChanges()I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 363
    const/4 v1, 0x0

    .line 364
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 366
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v10}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 367
    invoke-static {v1}, Lcom/google/android/music/store/PlayList$Item;->getItemsToSync(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v0

    .line 368
    const/4 v6, 0x0

    .line 369
    .local v6, "recordsRead":I
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 371
    :cond_0
    add-int/lit8 v6, v6, 0x1

    .line 372
    new-instance v4, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct {v4}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .line 373
    .local v4, "item":Lcom/google/android/music/store/PlayList$Item;
    new-instance v5, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct {v5}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .line 374
    .local v5, "precedingItem":Lcom/google/android/music/store/PlayList$Item;
    new-instance v3, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct {v3}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .line 375
    .local v3, "followingItem":Lcom/google/android/music/store/PlayList$Item;
    invoke-virtual {v4, v0}, Lcom/google/android/music/store/PlayList$Item;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376
    const/4 v8, 0x0

    .line 380
    .local v8, "sourceIdAndType":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    :try_start_1
    invoke-virtual {v4}, Lcom/google/android/music/store/PlayList$Item;->getMusicId()J

    move-result-wide v10

    invoke-static {v1, v10, v11}, Lcom/google/android/music/store/Store;->getSourceIdAndTypeForMusicId(Landroid/database/sqlite/SQLiteDatabase;J)Landroid/util/Pair;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 385
    :try_start_2
    iget-object v10, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    iget-object v11, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-static {v10, v11, v4}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->parse(Ljava/lang/String;ILcom/google/android/music/store/PlayList$Item;)Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    move-result-object v9

    .line 396
    .local v9, "syncable":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    iget v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mAccountId:I

    invoke-virtual {v4, v1, v5, v10}, Lcom/google/android/music/store/PlayList$Item;->findPrecedingItem(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/music/store/PlayList$Item;I)Lcom/google/android/music/store/PlayList$Item;

    move-result-object v10

    if-eqz v10, :cond_2

    .line 398
    invoke-virtual {v5}, Lcom/google/android/music/store/PlayList$Item;->getSourceId()Ljava/lang/String;

    move-result-object v7

    .line 399
    .local v7, "sourceId":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v5}, Lcom/google/android/music/store/PlayList$Item;->getClientId()Ljava/lang/String;

    move-result-object v7

    .end local v7    # "sourceId":Ljava/lang/String;
    :cond_1
    iput-object v7, v9, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mPrecedingEntryId:Ljava/lang/String;

    .line 402
    :cond_2
    iget v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mAccountId:I

    invoke-virtual {v4, v1, v3, v10}, Lcom/google/android/music/store/PlayList$Item;->findFollowingItem(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/music/store/PlayList$Item;I)Lcom/google/android/music/store/PlayList$Item;

    move-result-object v10

    if-eqz v10, :cond_4

    .line 404
    invoke-virtual {v3}, Lcom/google/android/music/store/PlayList$Item;->getSourceId()Ljava/lang/String;

    move-result-object v7

    .line 405
    .restart local v7    # "sourceId":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-virtual {v3}, Lcom/google/android/music/store/PlayList$Item;->getClientId()Ljava/lang/String;

    move-result-object v7

    .end local v7    # "sourceId":Ljava/lang/String;
    :cond_3
    iput-object v7, v9, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mFollowingEntryId:Ljava/lang/String;

    .line 408
    :cond_4
    invoke-direct {p0, v9}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->putItemInQueue(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Z

    .line 409
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v10

    if-nez v10, :cond_0

    .line 413
    .end local v3    # "followingItem":Lcom/google/android/music/store/PlayList$Item;
    .end local v4    # "item":Lcom/google/android/music/store/PlayList$Item;
    .end local v5    # "precedingItem":Lcom/google/android/music/store/PlayList$Item;
    .end local v8    # "sourceIdAndType":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v9    # "syncable":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    :cond_5
    if-eqz v0, :cond_6

    .line 414
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 415
    const/4 v0, 0x0

    .line 417
    :cond_6
    if-eqz v1, :cond_7

    .line 418
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v10, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_7
    return v6

    .line 381
    .restart local v3    # "followingItem":Lcom/google/android/music/store/PlayList$Item;
    .restart local v4    # "item":Lcom/google/android/music/store/PlayList$Item;
    .restart local v5    # "precedingItem":Lcom/google/android/music/store/PlayList$Item;
    .restart local v8    # "sourceIdAndType":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    :catch_0
    move-exception v2

    .line 382
    .local v2, "e":Ljava/io/FileNotFoundException;
    :try_start_3
    new-instance v10, Lcom/google/android/music/sync/common/SoftSyncException;

    invoke-direct {v10, v2}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/Throwable;)V

    throw v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 413
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    .end local v3    # "followingItem":Lcom/google/android/music/store/PlayList$Item;
    .end local v4    # "item":Lcom/google/android/music/store/PlayList$Item;
    .end local v5    # "precedingItem":Lcom/google/android/music/store/PlayList$Item;
    .end local v6    # "recordsRead":I
    .end local v8    # "sourceIdAndType":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v10

    if-eqz v0, :cond_8

    .line 414
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 415
    const/4 v0, 0x0

    .line 417
    :cond_8
    if-eqz v1, :cond_9

    .line 418
    iget-object v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v11, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_9
    throw v10
.end method

.method private fillQueueWithEntryTombstones()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 245
    const/4 v1, 0x0

    .line 246
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 248
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v5}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 249
    invoke-static {v1}, Lcom/google/android/music/store/PlayList$Item;->getItemTombstones(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v0

    .line 250
    const/4 v3, 0x0

    .line 251
    .local v3, "recordsRead":I
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 253
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 254
    new-instance v2, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct {v2}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .line 255
    .local v2, "item":Lcom/google/android/music/store/PlayList$Item;
    invoke-virtual {v2, v0}, Lcom/google/android/music/store/PlayList$Item;->populateFromTombstoneProjectionCursor(Landroid/database/Cursor;)V

    .line 256
    invoke-static {v2}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->parse(Lcom/google/android/music/store/PlayList$Item;)Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    move-result-object v4

    .line 257
    .local v4, "syncable":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mIsDeleted:Z

    .line 258
    invoke-direct {p0, v4}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->putItemInQueue(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Z

    .line 259
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_0

    .line 263
    .end local v2    # "item":Lcom/google/android/music/store/PlayList$Item;
    .end local v4    # "syncable":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    :cond_1
    if-eqz v0, :cond_2

    .line 264
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 265
    const/4 v0, 0x0

    .line 267
    :cond_2
    if-eqz v1, :cond_3

    .line 268
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v5, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_3
    return v3

    .line 263
    .end local v3    # "recordsRead":I
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_4

    .line 264
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 265
    const/4 v0, 0x0

    .line 267
    :cond_4
    if-eqz v1, :cond_5

    .line 268
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v6, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v5
.end method

.method private fillQueueWithPlaylistChanges()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 330
    const/4 v1, 0x0

    .line 331
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 333
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v5}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 334
    invoke-static {v1}, Lcom/google/android/music/store/PlayList;->getPlaylistsToSync(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v0

    .line 335
    const/4 v3, 0x0

    .line 336
    .local v3, "recordsRead":I
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 338
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 339
    new-instance v2, Lcom/google/android/music/store/PlayList;

    invoke-direct {v2}, Lcom/google/android/music/store/PlayList;-><init>()V

    .line 340
    .local v2, "playList":Lcom/google/android/music/store/PlayList;
    invoke-virtual {v2, v0}, Lcom/google/android/music/store/PlayList;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V

    .line 341
    invoke-static {v2}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->parse(Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-result-object v4

    .line 342
    .local v4, "syncable":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    iget-object v5, v4, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mName:Ljava/lang/String;

    if-nez v5, :cond_4

    .line 343
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    const-string v6, "Found a playlist with no name. Not syncing this change upstream."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_0

    .line 352
    .end local v2    # "playList":Lcom/google/android/music/store/PlayList;
    .end local v4    # "syncable":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :cond_1
    if-eqz v0, :cond_2

    .line 353
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 354
    const/4 v0, 0x0

    .line 356
    :cond_2
    if-eqz v1, :cond_3

    .line 357
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v5, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_3
    return v3

    .line 347
    .restart local v2    # "playList":Lcom/google/android/music/store/PlayList;
    .restart local v4    # "syncable":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :cond_4
    :try_start_1
    invoke-direct {p0, v4}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->putItemInQueue(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 352
    .end local v2    # "playList":Lcom/google/android/music/store/PlayList;
    .end local v3    # "recordsRead":I
    .end local v4    # "syncable":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_5

    .line 353
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 354
    const/4 v0, 0x0

    .line 356
    :cond_5
    if-eqz v1, :cond_6

    .line 357
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v6, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_6
    throw v5
.end method

.method private fillQueueWithPlaylistTombstones()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 207
    const/4 v1, 0x0

    .line 208
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 210
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v5}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 211
    invoke-static {v1}, Lcom/google/android/music/store/PlayList;->getPlaylistTombstones(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v0

    .line 212
    const/4 v3, 0x0

    .line 213
    .local v3, "recordsRead":I
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 215
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 216
    new-instance v2, Lcom/google/android/music/store/PlayList;

    invoke-direct {v2}, Lcom/google/android/music/store/PlayList;-><init>()V

    .line 217
    .local v2, "playList":Lcom/google/android/music/store/PlayList;
    invoke-virtual {v2, v0}, Lcom/google/android/music/store/PlayList;->populateFromTombstoneProjectionCursor(Landroid/database/Cursor;)V

    .line 218
    invoke-static {v2}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->parse(Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-result-object v4

    .line 219
    .local v4, "syncable":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mIsDeleted:Z

    .line 220
    invoke-direct {p0, v4}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->putItemInQueue(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Z

    .line 221
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_0

    .line 225
    .end local v2    # "playList":Lcom/google/android/music/store/PlayList;
    .end local v4    # "syncable":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :cond_1
    if-eqz v0, :cond_2

    .line 226
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 227
    const/4 v0, 0x0

    .line 229
    :cond_2
    if-eqz v1, :cond_3

    .line 230
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v5, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_3
    return v3

    .line 225
    .end local v3    # "recordsRead":I
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_4

    .line 226
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 227
    const/4 v0, 0x0

    .line 229
    :cond_4
    if-eqz v1, :cond_5

    .line 230
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v6, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v5
.end method

.method private fillQueueWithRadioStationChanges()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 274
    const/4 v1, 0x0

    .line 275
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 277
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v5}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 278
    invoke-static {v1}, Lcom/google/android/music/store/RadioStation;->getRadioStationsToSync(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v0

    .line 279
    const/4 v3, 0x0

    .line 280
    .local v3, "recordsRead":I
    if-eqz v0, :cond_2

    .line 281
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 282
    add-int/lit8 v3, v3, 0x1

    .line 283
    new-instance v2, Lcom/google/android/music/store/RadioStation;

    invoke-direct {v2}, Lcom/google/android/music/store/RadioStation;-><init>()V

    .line 284
    .local v2, "radioStation":Lcom/google/android/music/store/RadioStation;
    invoke-virtual {v2, v0}, Lcom/google/android/music/store/RadioStation;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V

    .line 285
    invoke-static {v2}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->parse(Lcom/google/android/music/store/RadioStation;)Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move-result-object v4

    .line 286
    .local v4, "syncable":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    iget-object v5, v4, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mName:Ljava/lang/String;

    if-nez v5, :cond_1

    .line 287
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    const-string v6, "Found a radio station with no name. Not syncing this change upstream."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 296
    .end local v2    # "radioStation":Lcom/google/android/music/store/RadioStation;
    .end local v3    # "recordsRead":I
    .end local v4    # "syncable":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    :catchall_0
    move-exception v5

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 297
    if-eqz v1, :cond_0

    .line 298
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v6, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    throw v5

    .line 291
    .restart local v2    # "radioStation":Lcom/google/android/music/store/RadioStation;
    .restart local v3    # "recordsRead":I
    .restart local v4    # "syncable":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    :cond_1
    :try_start_1
    invoke-direct {p0, v4}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->putItemInQueue(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 296
    .end local v2    # "radioStation":Lcom/google/android/music/store/RadioStation;
    .end local v4    # "syncable":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    :cond_2
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 297
    if-eqz v1, :cond_3

    .line 298
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v5, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_3
    return v3
.end method

.method private fillQueueWithRadioStationTombstones()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 304
    const/4 v1, 0x0

    .line 305
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 307
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v5}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 308
    invoke-static {v1}, Lcom/google/android/music/store/RadioStation;->getRadioStationTombstones(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v0

    .line 309
    const/4 v3, 0x0

    .line 310
    .local v3, "recordsRead":I
    if-eqz v0, :cond_1

    .line 311
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 312
    add-int/lit8 v3, v3, 0x1

    .line 313
    new-instance v2, Lcom/google/android/music/store/RadioStation;

    invoke-direct {v2}, Lcom/google/android/music/store/RadioStation;-><init>()V

    .line 314
    .local v2, "radioStation":Lcom/google/android/music/store/RadioStation;
    invoke-virtual {v2, v0}, Lcom/google/android/music/store/RadioStation;->populateFromTombstoneProjectionCursor(Landroid/database/Cursor;)V

    .line 315
    invoke-static {v2}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->parseForTombstone(Lcom/google/android/music/store/RadioStation;)Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move-result-object v4

    .line 317
    .local v4, "syncable":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    invoke-direct {p0, v4}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->putItemInQueue(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 322
    .end local v2    # "radioStation":Lcom/google/android/music/store/RadioStation;
    .end local v3    # "recordsRead":I
    .end local v4    # "syncable":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    :catchall_0
    move-exception v5

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 323
    if-eqz v1, :cond_0

    .line 324
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v6, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    throw v5

    .line 322
    .restart local v3    # "recordsRead":I
    :cond_1
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 323
    if-eqz v1, :cond_2

    .line 324
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v5, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_2
    return v3
.end method

.method private fillQueueWithTrackChanges()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 119
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v5}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 120
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 122
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v3, 0x0

    .line 123
    .local v3, "recordsRead":I
    :try_start_0
    iget v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mAccountId:I

    int-to-long v6, v5

    invoke-static {v1, v6, v7}, Lcom/google/android/music/store/MusicFile;->getMusicFilesToSync(Landroid/database/sqlite/SQLiteDatabase;J)Landroid/database/Cursor;

    move-result-object v0

    .line 124
    if-eqz v0, :cond_0

    .line 125
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 126
    new-instance v2, Lcom/google/android/music/store/MusicFile;

    invoke-direct {v2}, Lcom/google/android/music/store/MusicFile;-><init>()V

    .line 127
    .local v2, "musicFile":Lcom/google/android/music/store/MusicFile;
    invoke-virtual {v2, v0}, Lcom/google/android/music/store/MusicFile;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V

    .line 128
    invoke-static {v2}, Lcom/google/android/music/sync/google/model/Track;->parse(Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/sync/google/model/Track;

    move-result-object v4

    .line 129
    .local v4, "track":Lcom/google/android/music/sync/google/model/Track;
    invoke-direct {p0, v4}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->putItemInQueue(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    add-int/lit8 v3, v3, 0x1

    .line 131
    goto :goto_0

    .line 135
    .end local v2    # "musicFile":Lcom/google/android/music/store/MusicFile;
    .end local v4    # "track":Lcom/google/android/music/sync/google/model/Track;
    :cond_0
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 136
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v5, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    return v3

    .line 135
    :catchall_0
    move-exception v5

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 136
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v6, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v5
.end method

.method private fillQueueWithTrackStats()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 141
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v6}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 142
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 144
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v4, 0x0

    .line 145
    .local v4, "recordsRead":I
    :try_start_0
    iget v6, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mAccountId:I

    int-to-long v6, v6

    invoke-static {v1, v6, v7}, Lcom/google/android/music/store/MusicFile;->getPlaycountsToSync(Landroid/database/sqlite/SQLiteDatabase;J)Landroid/database/Cursor;

    move-result-object v0

    .line 146
    if-eqz v0, :cond_3

    .line 147
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 148
    invoke-static {v0}, Lcom/google/android/music/sync/google/model/TrackStat;->parse(Landroid/database/Cursor;)Lcom/google/android/music/sync/google/model/TrackStat;

    move-result-object v5

    .line 149
    .local v5, "stat":Lcom/google/android/music/sync/google/model/TrackStat;
    iget-object v6, v5, Lcom/google/android/music/sync/google/model/TrackStat;->mRemoteId:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    iget v6, v5, Lcom/google/android/music/sync/google/model/TrackStat;->mIncrementalPlays:I

    if-gtz v6, :cond_1

    .line 151
    :cond_0
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    const-string v7, "Found a trackstat with no valid track id or zero play count. Not syncing this change upstream."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 176
    .end local v5    # "stat":Lcom/google/android/music/sync/google/model/TrackStat;
    :catchall_0
    move-exception v6

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 177
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v7, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v6

    .line 155
    .restart local v5    # "stat":Lcom/google/android/music/sync/google/model/TrackStat;
    :cond_1
    const/4 v3, 0x0

    .line 157
    .local v3, "eventCursor":Landroid/database/Cursor;
    :try_start_1
    iget-object v6, v5, Lcom/google/android/music/sync/google/model/TrackStat;->mRemoteId:Ljava/lang/String;

    invoke-static {v1, v6}, Lcom/google/android/music/store/Store;->getStoredEvents(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 159
    if-eqz v3, :cond_2

    .line 160
    :goto_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 161
    invoke-static {v3}, Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;->parse(Landroid/database/Cursor;)Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;

    move-result-object v2

    .line 163
    .local v2, "event":Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;
    iget-object v6, v5, Lcom/google/android/music/sync/google/model/TrackStat;->mTrackEvents:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 167
    .end local v2    # "event":Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;
    :catchall_1
    move-exception v6

    :try_start_2
    invoke-static {v3}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v6

    :cond_2
    invoke-static {v3}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 170
    invoke-direct {p0, v5}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->putItemInQueue(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 171
    add-int/lit8 v4, v4, 0x1

    .line 172
    goto :goto_0

    .line 176
    .end local v3    # "eventCursor":Landroid/database/Cursor;
    .end local v5    # "stat":Lcom/google/android/music/sync/google/model/TrackStat;
    :cond_3
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 177
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v6, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    return v4
.end method

.method private fillQueueWithTrackTombstones()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 236
    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mStore:Lcom/google/android/music/store/Store;

    invoke-static {v4}, Lcom/google/android/music/store/MusicFileTombstone;->getMusicTombstones(Lcom/google/android/music/store/Store;)Ljava/util/List;

    move-result-object v2

    .line 237
    .local v2, "tombstones":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/store/MusicFileTombstone;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/store/MusicFileTombstone;

    .line 238
    .local v1, "musicFileTombstone":Lcom/google/android/music/store/MusicFileTombstone;
    invoke-static {v1}, Lcom/google/android/music/sync/google/model/TrackTombstone;->parse(Lcom/google/android/music/store/MusicFileTombstone;)Lcom/google/android/music/sync/google/model/TrackTombstone;

    move-result-object v3

    .line 239
    .local v3, "trackTombstone":Lcom/google/android/music/sync/google/model/TrackTombstone;
    invoke-direct {p0, v3}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->putItemInQueue(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Z

    goto :goto_0

    .line 241
    .end local v1    # "musicFileTombstone":Lcom/google/android/music/store/MusicFileTombstone;
    .end local v3    # "trackTombstone":Lcom/google/android/music/sync/google/model/TrackTombstone;
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    return v4
.end method

.method private putItemInQueue(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Z
    .locals 5
    .param p1, "item"    # Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 187
    iget-boolean v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mUseVerboseLogging:Z

    if-eqz v2, :cond_0

    .line 188
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Found item: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mQueue:Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;

    invoke-virtual {v2, p1}, Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/android/music/sync/common/ClosableBlockingQueue$QueueClosedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 203
    const/4 v2, 0x1

    return v2

    .line 192
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Lcom/google/android/music/sync/common/ClosableBlockingQueue$QueueClosedException;
    const-string v1, "The upstream sender has killed the upstream queue, so there\'s no point in having the reader continue."

    .line 195
    .local v1, "msg":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mUseVerboseLogging:Z

    if-eqz v2, :cond_1

    .line 196
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_1
    new-instance v2, Lcom/google/android/music/sync/common/SoftSyncException;

    invoke-direct {v2, v1, v0}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 199
    .end local v0    # "e":Lcom/google/android/music/sync/common/ClosableBlockingQueue$QueueClosedException;
    .end local v1    # "msg":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 200
    .local v0, "e":Ljava/lang/InterruptedException;
    new-instance v2, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v3, "Interrupted while putting item into upload queue: "

    invoke-direct {v2, v3, v0}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method


# virtual methods
.method public fillQueue()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->fillQueueWithEntryTombstones()I

    move-result v2

    .line 58
    .local v2, "entryTombstoneCount":I
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mUseVerboseLogging:Z

    if-eqz v10, :cond_0

    .line 59
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Upstream reader: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " playlist entry tombstone(s) found."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->fillQueueWithPlaylistTombstones()I

    move-result v4

    .line 63
    .local v4, "playlistTombstoneCount":I
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mUseVerboseLogging:Z

    if-eqz v10, :cond_1

    .line 64
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Upstream reader: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " playlist tombstone(s) found."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->fillQueueWithTrackChanges()I

    move-result v7

    .line 69
    .local v7, "trackCount":I
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mUseVerboseLogging:Z

    if-eqz v10, :cond_2

    .line 70
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Upstream reader: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " track change(s) found."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_2
    invoke-direct {p0}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->fillQueueWithPlaylistChanges()I

    move-result v3

    .line 74
    .local v3, "playlistCount":I
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mUseVerboseLogging:Z

    if-eqz v10, :cond_3

    .line 75
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Upstream reader: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " playlist change(s) found."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :cond_3
    invoke-direct {p0}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->fillQueueWithEntryChanges()I

    move-result v1

    .line 80
    .local v1, "entryCount":I
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mUseVerboseLogging:Z

    if-eqz v10, :cond_4

    .line 81
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Upstream reader: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " playlist entry change(s) found."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :cond_4
    invoke-direct {p0}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->fillQueueWithTrackStats()I

    move-result v8

    .line 86
    .local v8, "trackStatCount":I
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mUseVerboseLogging:Z

    if-eqz v10, :cond_5

    .line 87
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Upstream reader: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " track stat change(s) found."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_5
    invoke-direct {p0}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->fillQueueWithRadioStationTombstones()I

    move-result v6

    .line 92
    .local v6, "radioStationTombstoneCount":I
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mUseVerboseLogging:Z

    if-eqz v10, :cond_6

    .line 93
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Upstream reader: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " radio station tombstone(s) found."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_6
    invoke-direct {p0}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->fillQueueWithRadioStationChanges()I

    move-result v5

    .line 98
    .local v5, "radioStationCount":I
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mUseVerboseLogging:Z

    if-eqz v10, :cond_7

    .line 99
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Upstream reader: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " radio station found."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_7
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mEnableTrackDeletes:Z

    if-eqz v10, :cond_8

    .line 104
    invoke-direct {p0}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->fillQueueWithTrackTombstones()I

    move-result v9

    .line 105
    .local v9, "trackTombstoneCount":I
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mUseVerboseLogging:Z

    if-eqz v10, :cond_8

    .line 106
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Upstream reader: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " track tombstone(s) found."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    .end local v9    # "trackTombstoneCount":I
    :cond_8
    invoke-direct {p0}, Lcom/google/android/music/sync/google/MusicUpstreamReader;->fillQueueWithBlacklistItemChanges()I

    move-result v0

    .line 112
    .local v0, "blacklistItemCount":I
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mUseVerboseLogging:Z

    if-eqz v10, :cond_9

    .line 113
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicUpstreamReader;->mTag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Upstream reader: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " blacklist item found."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :cond_9
    return-void
.end method
