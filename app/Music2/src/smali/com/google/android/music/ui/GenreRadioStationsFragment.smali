.class public Lcom/google/android/music/ui/GenreRadioStationsFragment;
.super Lcom/google/android/music/ui/MediaListGridFragment;
.source "GenreRadioStationsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;
    }
.end annotation


# static fields
.field static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAdapter:Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;

.field private mParentArt:Ljava/lang/String;

.field private mParentGenre:Ljava/lang/String;

.field private mParentName:Ljava/lang/String;

.field private mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "genreServerId"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "genreArtUris"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/music/ui/MediaListGridFragment;-><init>()V

    .line 31
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/GenreRadioStationsFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/GenreRadioStationsFragment;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/GenreRadioStationsFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/GenreRadioStationsFragment;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mParentGenre:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/GenreRadioStationsFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/GenreRadioStationsFragment;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mParentName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/GenreRadioStationsFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/GenreRadioStationsFragment;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mParentArt:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected initEmptyScreen()V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->initEmptyScreen()V

    .line 40
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->setEmptyImageView(I)V

    .line 41
    return-void
.end method

.method newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 1
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 69
    new-instance v0, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;

    invoke-direct {v0, p0, p0, p1}, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;-><init>(Lcom/google/android/music/ui/GenreRadioStationsFragment;Lcom/google/android/music/ui/GenreRadioStationsFragment;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mAdapter:Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;

    .line 70
    iget-object v0, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mAdapter:Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;

    return-object v0
.end method

.method newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;

    invoke-direct {v0, p0, p0}, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;-><init>(Lcom/google/android/music/ui/GenreRadioStationsFragment;Lcom/google/android/music/ui/GenreRadioStationsFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mAdapter:Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;

    .line 64
    iget-object v0, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mAdapter:Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 46
    .local v0, "extra":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 47
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Genre must be provided to show genre radio stations."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 50
    :cond_0
    const-string v1, "nautilusId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mParentGenre:Ljava/lang/String;

    .line 51
    const-string v1, "name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mParentName:Ljava/lang/String;

    .line 52
    const-string v1, "artUrls"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mParentArt:Ljava/lang/String;

    .line 53
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 54
    new-instance v1, Lcom/google/android/music/ui/GenreExploreList;

    iget-object v2, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mParentGenre:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/music/ui/GenreExploreList;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/music/ui/GenreRadioStationsFragment;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V

    .line 58
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListGridFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 59
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mAdapter:Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;->setShowFakeEvenIfEmpty(Z)V

    .line 80
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/MediaListGridFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    .line 81
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 19
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mAdapter:Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;->setShowFakeEvenIfEmpty(Z)V

    .line 90
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListGridFragment;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    .line 91
    return-void
.end method
