.class Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$5;
.super Ljava/lang/Object;
.source "TutorialSelectAccountActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->showAccountSwitchWarning()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;)V
    .locals 0

    .prologue
    .line 515
    iput-object p1, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$5;->this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$5;->this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    # getter for: Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mSelectedAccount:Landroid/accounts/Account;
    invoke-static {v0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->access$700(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    .line 520
    :goto_0
    return-void

    .line 519
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$5;->this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    # invokes: Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->selectAccount()V
    invoke-static {v0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->access$800(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;)V

    goto :goto_0
.end method
