.class final Lcom/google/android/music/ui/AppNavigationMetajamHelper$7;
.super Ljava/lang/Object;
.source "AppNavigationMetajamHelper.java"

# interfaces
.implements Lcom/google/android/music/utils/MusicUtils$QueryCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/AppNavigationMetajamHelper;->openTrackInStore(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Lcom/google/android/music/ui/BaseActivity;

.field final synthetic val$trackMetajamId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 634
    iput-object p1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$7;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iput-object p2, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$7;->val$trackMetajamId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryComplete(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 639
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$7;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$7;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isActivityDestroyed()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 659
    :cond_0
    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 662
    :goto_0
    return-void

    .line 642
    :cond_1
    if-eqz p1, :cond_4

    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 643
    # getter for: Lcom/google/android/music/ui/AppNavigationMetajamHelper;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->access$000()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 644
    const-string v0, "MusicNavigationHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Found track: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    :cond_2
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 647
    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$7;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$7;->val$trackMetajamId:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/music/purchase/Finsky;->startBuyTrackActivity(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 659
    :goto_1
    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 661
    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$7;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->finish()V

    goto :goto_0

    .line 650
    :cond_3
    :try_start_2
    const-string v0, "MusicNavigationHelper"

    const-string v1, "No album natilus id found for track."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 659
    :catchall_0
    move-exception v0

    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    .line 653
    :cond_4
    :try_start_3
    const-string v0, "MusicNavigationHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Track not found for metajam id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$7;->val$trackMetajamId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$7;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$7;->val$trackMetajamId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/music/purchase/Finsky;->startBuyTrackActivity(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method
