.class public Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;
.super Ljava/lang/Object;
.source "NowPlayingBitmapGetter.java"

# interfaces
.implements Lcom/google/android/music/leanback/bitmap/BitmapGetter;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAlbumArtIsFromService:Z

.field private mAlbumArtResourceId:I

.field private mAlbumId:J

.field private mAlbumName:Ljava/lang/String;

.field private mArtistName:Ljava/lang/String;

.field private mExternalAlbumArtUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBitmap(Landroid/content/Context;IIZ)Landroid/util/Pair;
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "targetWidth"    # I
    .param p3, "targetHeight"    # I
    .param p4, "cropToSquare"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IIZ)",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    const/4 v11, 0x0

    .line 40
    .local v11, "allowDefault":Z
    const/4 v12, 0x0

    .line 41
    .local v12, "artBitmap":Landroid/graphics/Bitmap;
    const/4 v13, -0x1

    .line 42
    .local v13, "artResourceId":I
    iget-boolean v0, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mAlbumArtIsFromService:Z

    if-eqz v0, :cond_0

    .line 43
    iget-wide v2, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mAlbumId:J

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mAlbumName:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mArtistName:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object v1, p1

    move/from16 v4, p2

    move/from16 v5, p2

    invoke-static/range {v1 .. v10}, Lcom/google/android/music/utils/AlbumArtUtils;->getArtwork(Landroid/content/Context;JIIZLjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 57
    :goto_0
    if-nez v12, :cond_4

    if-lez v13, :cond_4

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    .line 60
    new-instance v0, Landroid/util/Pair;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v12, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 63
    :goto_1
    return-object v0

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mExternalAlbumArtUrl:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 46
    iget-object v1, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mExternalAlbumArtUrl:Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p1

    move/from16 v2, p2

    move/from16 v3, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/music/utils/AlbumArtUtils;->getExternalAlbumArtBitmap(Landroid/content/Context;Ljava/lang/String;IIZZZ)Landroid/graphics/Bitmap;

    move-result-object v12

    goto :goto_0

    .line 48
    :cond_1
    iget v0, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mAlbumArtResourceId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 49
    iget v13, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mAlbumArtResourceId:I

    goto :goto_0

    .line 50
    :cond_2
    iget-wide v0, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mAlbumId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3

    .line 51
    iget-wide v2, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mAlbumId:J

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object v1, p1

    move/from16 v4, p2

    move/from16 v5, p2

    invoke-static/range {v1 .. v10}, Lcom/google/android/music/utils/AlbumArtUtils;->getArtwork(Landroid/content/Context;JIIZLjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v12

    goto :goto_0

    .line 54
    :cond_3
    const v13, 0x7f02003a

    goto :goto_0

    .line 63
    :cond_4
    new-instance v0, Landroid/util/Pair;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v12, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public setData(ZLjava/lang/String;ILjava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p1, "albumArtIsFromService"    # Z
    .param p2, "externalAlbumArtUrl"    # Ljava/lang/String;
    .param p3, "albumArtResourceId"    # I
    .param p4, "artistName"    # Ljava/lang/String;
    .param p5, "albumName"    # Ljava/lang/String;
    .param p6, "albumId"    # J

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mAlbumArtIsFromService:Z

    .line 29
    iput-object p2, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mExternalAlbumArtUrl:Ljava/lang/String;

    .line 30
    iput p3, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mAlbumArtResourceId:I

    .line 31
    iput-object p4, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mArtistName:Ljava/lang/String;

    .line 32
    iput-object p5, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mAlbumName:Ljava/lang/String;

    .line 33
    iput-wide p6, p0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->mAlbumId:J

    .line 34
    return-void
.end method
