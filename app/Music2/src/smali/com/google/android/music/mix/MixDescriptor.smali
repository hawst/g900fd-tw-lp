.class public Lcom/google/android/music/mix/MixDescriptor;
.super Ljava/lang/Object;
.source "MixDescriptor.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/mix/MixDescriptor$Type;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/mix/MixDescriptor;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mArtLocation:Ljava/lang/String;

.field private final mIsRecommendation:Z

.field private final mLocalRadioId:J

.field private final mLocalSeedId:J

.field private final mName:Ljava/lang/String;

.field private final mRemoteSeedId:Ljava/lang/String;

.field private final mType:Lcom/google/android/music/mix/MixDescriptor$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 295
    new-instance v0, Lcom/google/android/music/mix/MixDescriptor$1;

    invoke-direct {v0}, Lcom/google/android/music/mix/MixDescriptor$1;-><init>()V

    sput-object v0, Lcom/google/android/music/mix/MixDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "id"    # J
    .param p3, "type"    # Lcom/google/android/music/mix/MixDescriptor$Type;
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "artLocation"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, -0x1

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 63
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid seed id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mRemoteSeedId:Ljava/lang/String;

    .line 66
    if-nez p3, :cond_1

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_1
    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    if-ne p3, v0, :cond_2

    .line 70
    iput-wide v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalSeedId:J

    .line 71
    iput-wide p1, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalRadioId:J

    .line 77
    :goto_0
    iput-object p3, p0, Lcom/google/android/music/mix/MixDescriptor;->mType:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 78
    iput-object p4, p0, Lcom/google/android/music/mix/MixDescriptor;->mName:Ljava/lang/String;

    .line 79
    iput-object p5, p0, Lcom/google/android/music/mix/MixDescriptor;->mArtLocation:Ljava/lang/String;

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mIsRecommendation:Z

    .line 81
    return-void

    .line 73
    :cond_2
    iput-wide p1, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalSeedId:J

    .line 74
    iput-wide v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalRadioId:J

    goto :goto_0
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalSeedId:J

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mRemoteSeedId:Ljava/lang/String;

    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalRadioId:J

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 116
    .local v0, "typeVal":I
    invoke-static {}, Lcom/google/android/music/mix/MixDescriptor$Type;->values()[Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_0

    if-gez v0, :cond_1

    .line 118
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid type"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 120
    :cond_1
    invoke-static {}, Lcom/google/android/music/mix/MixDescriptor$Type;->values()[Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v2

    aget-object v2, v2, v0

    iput-object v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mType:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mName:Ljava/lang/String;

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mArtLocation:Ljava/lang/String;

    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v1, :cond_2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/music/mix/MixDescriptor;->mIsRecommendation:Z

    .line 124
    return-void

    .line 123
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/music/mix/MixDescriptor$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/music/mix/MixDescriptor$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/google/android/music/mix/MixDescriptor;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, -0x1

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    iput-object v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mType:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 129
    iput-wide v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalRadioId:J

    .line 130
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mRemoteSeedId:Ljava/lang/String;

    .line 131
    iput-wide v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalSeedId:J

    .line 132
    iput-object p1, p0, Lcom/google/android/music/mix/MixDescriptor;->mName:Ljava/lang/String;

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mArtLocation:Ljava/lang/String;

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mIsRecommendation:Z

    .line 135
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "remoteSeedId"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/google/android/music/mix/MixDescriptor$Type;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "artLocation"    # Ljava/lang/String;
    .param p5, "isRecommendation"    # Z

    .prologue
    const-wide/16 v0, -0x1

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    if-nez p1, :cond_0

    .line 95
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing remote seed id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_0
    iput-wide v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalSeedId:J

    .line 98
    iput-object p1, p0, Lcom/google/android/music/mix/MixDescriptor;->mRemoteSeedId:Ljava/lang/String;

    .line 99
    iput-wide v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalRadioId:J

    .line 100
    if-nez p2, :cond_1

    .line 101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_1
    iput-object p2, p0, Lcom/google/android/music/mix/MixDescriptor;->mType:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 105
    iput-object p3, p0, Lcom/google/android/music/mix/MixDescriptor;->mName:Ljava/lang/String;

    .line 107
    iput-object p4, p0, Lcom/google/android/music/mix/MixDescriptor;->mArtLocation:Ljava/lang/String;

    .line 108
    iput-boolean p5, p0, Lcom/google/android/music/mix/MixDescriptor;->mIsRecommendation:Z

    .line 109
    return-void
.end method

.method public static getLuckyRadio(Landroid/content/Context;)Lcom/google/android/music/mix/MixDescriptor;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 138
    const v1, 0x7f0b02ed

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "name":Ljava/lang/String;
    new-instance v1, Lcom/google/android/music/mix/MixDescriptor;

    invoke-direct {v1, v0}, Lcom/google/android/music/mix/MixDescriptor;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method public static unmarshall([B)Lcom/google/android/music/mix/MixDescriptor;
    .locals 4
    .param p0, "bytes"    # [B

    .prologue
    const/4 v3, 0x0

    .line 284
    if-nez p0, :cond_0

    .line 285
    const/4 v0, 0x0

    .line 292
    :goto_0
    return-object v0

    .line 287
    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 288
    .local v1, "parcel":Landroid/os/Parcel;
    array-length v2, p0

    invoke-virtual {v1, p0, v3, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 289
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 290
    sget-object v2, Lcom/google/android/music/mix/MixDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v2, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/mix/MixDescriptor;

    .line 291
    .local v0, "descriptor":Lcom/google/android/music/mix/MixDescriptor;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 235
    instance-of v2, p1, Lcom/google/android/music/mix/MixDescriptor;

    if-nez v2, :cond_1

    .line 239
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 238
    check-cast v0, Lcom/google/android/music/mix/MixDescriptor;

    .line 239
    .local v0, "other":Lcom/google/android/music/mix/MixDescriptor;
    iget-wide v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalSeedId:J

    invoke-virtual {v0}, Lcom/google/android/music/mix/MixDescriptor;->getLocalSeedId()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mRemoteSeedId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/music/mix/MixDescriptor;->getRemoteSeedId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalRadioId:J

    iget-wide v4, v0, Lcom/google/android/music/mix/MixDescriptor;->mLocalRadioId:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mType:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v0}, Lcom/google/android/music/mix/MixDescriptor;->getType()Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/mix/MixDescriptor$Type;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/music/mix/MixDescriptor;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getArtLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mArtLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getIsRecommendation()Z
    .locals 1

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mIsRecommendation:Z

    return v0
.end method

.method public getLocalRadioId()J
    .locals 2

    .prologue
    .line 161
    iget-wide v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalRadioId:J

    return-wide v0
.end method

.method public getLocalSeedId()J
    .locals 2

    .prologue
    .line 146
    iget-wide v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalSeedId:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getRemoteSeedId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mRemoteSeedId:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/google/android/music/mix/MixDescriptor$Type;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mType:Lcom/google/android/music/mix/MixDescriptor$Type;

    return-object v0
.end method

.method public hasLocalSeedId()Z
    .locals 4

    .prologue
    .line 184
    iget-wide v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalSeedId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRemoteSeedId()Z
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mRemoteSeedId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 248
    const/4 v0, 0x0

    .line 249
    .local v0, "h":I
    int-to-long v2, v0

    const/4 v1, 0x0

    int-to-long v4, v1

    iget-wide v6, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalSeedId:J

    add-long/2addr v4, v6

    add-long/2addr v2, v4

    long-to-int v0, v2

    .line 250
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mRemoteSeedId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 251
    int-to-long v2, v0

    mul-int/lit8 v1, v0, 0x1f

    int-to-long v4, v1

    iget-wide v6, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalRadioId:J

    add-long/2addr v4, v6

    add-long/2addr v2, v4

    long-to-int v0, v2

    .line 252
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mType:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v2}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 253
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 254
    return v0
.end method

.method public marshall()[B
    .locals 3

    .prologue
    .line 275
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 276
    .local v1, "parcel":Landroid/os/Parcel;
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/mix/MixDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    .line 277
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0

    .line 278
    .local v0, "bytes":[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 279
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 215
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    iget-wide v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalSeedId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 217
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    iget-object v1, p0, Lcom/google/android/music/mix/MixDescriptor;->mRemoteSeedId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    iget-wide v2, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalRadioId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 221
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    iget-object v1, p0, Lcom/google/android/music/mix/MixDescriptor;->mType:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 223
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    iget-object v1, p0, Lcom/google/android/music/mix/MixDescriptor;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    iget-object v1, p0, Lcom/google/android/music/mix/MixDescriptor;->mArtLocation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    iget-boolean v1, p0, Lcom/google/android/music/mix/MixDescriptor;->mIsRecommendation:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 229
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 264
    iget-wide v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalSeedId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 265
    iget-object v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mRemoteSeedId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 266
    iget-wide v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mLocalRadioId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 267
    iget-object v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mType:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v0}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 268
    iget-object v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mArtLocation:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 270
    iget-boolean v0, p0, Lcom/google/android/music/mix/MixDescriptor;->mIsRecommendation:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 271
    return-void

    .line 270
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
