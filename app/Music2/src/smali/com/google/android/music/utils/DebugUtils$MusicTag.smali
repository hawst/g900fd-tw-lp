.class public final enum Lcom/google/android/music/utils/DebugUtils$MusicTag;
.super Ljava/lang/Enum;
.source "DebugUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/DebugUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MusicTag"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/utils/DebugUtils$MusicTag;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum AAH:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum ALBUM_ART:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum ART_RESOLVER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum ASYNC:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum CACHE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum CAST:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum CAST_REMOTE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum CLOUD_CLIENT:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum DIAL_MRP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum DIAL_MRP_COMMUNICATION:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum DIAL_MRP_DISCOVERY:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum DIAL_MRP_MANAGEMENT:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum DIAL_MRP_PLAYBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum EVENT_LOG:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum HTTP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum LOG_FILE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum MEDIA_LIST:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum PLAYBACK_SERVICE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum PREFERENCES:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum SEARCH:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum STORE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum STORE_IMPORTER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum SYNC:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

.field public static final enum XDI:Lcom/google/android/music/utils/DebugUtils$MusicTag;


# instance fields
.field private final mText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 35
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "PLAYBACK_SERVICE"

    const-string v2, "MusicPlaybackService"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->PLAYBACK_SERVICE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 36
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "DOWNLOAD"

    const-string v2, "MusicDownload"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 37
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "CONTENT_PROVIDER"

    const-string v2, "MusicContentProvider"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 38
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "STORE_IMPORTER"

    const-string v2, "MusicStoreImporter"

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE_IMPORTER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 39
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "SYNC"

    const-string v2, "MusicSync"

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->SYNC:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 40
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "HTTP"

    const/4 v2, 0x5

    const-string v3, "MusicHttp"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->HTTP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 41
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "LOG_FILE"

    const/4 v2, 0x6

    const-string v3, "MusicLogFile"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LOG_FILE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 42
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "MEDIA_LIST"

    const/4 v2, 0x7

    const-string v3, "MusicMediaList"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->MEDIA_LIST:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 43
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "AAH"

    const/16 v2, 0x8

    const-string v3, "aah.Music"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->AAH:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 44
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "STORE"

    const/16 v2, 0x9

    const-string v3, "MusicStore"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 45
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "PREFERENCES"

    const/16 v2, 0xa

    const-string v3, "MusicPreferences"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->PREFERENCES:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 46
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "CLOUD_CLIENT"

    const/16 v2, 0xb

    const-string v3, "MusicCloudClient"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CLOUD_CLIENT:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 47
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "ALBUM_ART"

    const/16 v2, 0xc

    const-string v3, "MusicAlbumArt"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ALBUM_ART:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 48
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "ART_RESOLVER"

    const/16 v2, 0xd

    const-string v3, "MusicArtResolver"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ART_RESOLVER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 49
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "ASYNC"

    const/16 v2, 0xe

    const-string v3, "MusicAsync"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ASYNC:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 50
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "UI"

    const/16 v2, 0xf

    const-string v3, "MusicUI"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 51
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "CALLS"

    const/16 v2, 0x10

    const-string v3, "MusicCalls"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 52
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "LEANBACK"

    const/16 v2, 0x11

    const-string v3, "MusicLeanback"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 53
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "SEARCH"

    const/16 v2, 0x12

    const-string v3, "Search"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->SEARCH:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 54
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "CAST"

    const/16 v2, 0x13

    const-string v3, "MusicCast"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CAST:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 55
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "CAST_REMOTE"

    const/16 v2, 0x14

    const-string v3, "MusicCastRemote"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CAST_REMOTE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 56
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "XDI"

    const/16 v2, 0x15

    const-string v3, "MusicXdi"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->XDI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 57
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "CACHE"

    const/16 v2, 0x16

    const-string v3, "MusicCache"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CACHE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 58
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "EVENT_LOG"

    const/16 v2, 0x17

    const-string v3, "MusicEventLog"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->EVENT_LOG:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 59
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "DIAL_MRP"

    const/16 v2, 0x18

    const-string v3, "DialMRP"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 60
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "DIAL_MRP_DISCOVERY"

    const/16 v2, 0x19

    const-string v3, "DialMRPDiscovery"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_DISCOVERY:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 61
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "DIAL_MRP_MANAGEMENT"

    const/16 v2, 0x1a

    const-string v3, "DialMRPManagement"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_MANAGEMENT:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 62
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "DIAL_MRP_PLAYBACK"

    const/16 v2, 0x1b

    const-string v3, "DialMRPPlayback"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_PLAYBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 63
    new-instance v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    const-string v1, "DIAL_MRP_COMMUNICATION"

    const/16 v2, 0x1c

    const-string v3, "DialMRPCommunication"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/utils/DebugUtils$MusicTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_COMMUNICATION:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .line 34
    const/16 v0, 0x1d

    new-array v0, v0, [Lcom/google/android/music/utils/DebugUtils$MusicTag;

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->PLAYBACK_SERVICE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE_IMPORTER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->SYNC:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->HTTP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LOG_FILE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->MEDIA_LIST:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->AAH:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->PREFERENCES:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CLOUD_CLIENT:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ALBUM_ART:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ART_RESOLVER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ASYNC:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->SEARCH:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CAST:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CAST_REMOTE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->XDI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CACHE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->EVENT_LOG:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_DISCOVERY:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_MANAGEMENT:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_PLAYBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_COMMUNICATION:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->$VALUES:[Lcom/google/android/music/utils/DebugUtils$MusicTag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 68
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x17

    if-le v0, v1, :cond_0

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tag length is longer than the max 23"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    iput-object p3, p0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->mText:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/utils/DebugUtils$MusicTag;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 34
    const-class v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/utils/DebugUtils$MusicTag;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->$VALUES:[Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, [Lcom/google/android/music/utils/DebugUtils$MusicTag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/utils/DebugUtils$MusicTag;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->mText:Ljava/lang/String;

    return-object v0
.end method
