.class public final Lcom/google/android/music/wear/util/DefaultDataApiWrapper;
.super Ljava/lang/Object;
.source "DefaultDataApiWrapper.java"

# interfaces
.implements Lcom/google/android/music/wear/util/DataApiWrapper;


# instance fields
.field private final mClient:Lcom/google/android/gms/common/api/GoogleApiClient;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/GoogleApiClient;)V
    .locals 1
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/GoogleApiClient;

    iput-object v0, p0, Lcom/google/android/music/wear/util/DefaultDataApiWrapper;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 34
    return-void
.end method


# virtual methods
.method public deleteDataItems(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 70
    sget-object v0, Lcom/google/android/gms/wearable/Wearable;->DataApi:Lcom/google/android/gms/wearable/DataApi;

    iget-object v1, p0, Lcom/google/android/music/wear/util/DefaultDataApiWrapper;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/wearable/DataApi;->deleteDataItems(Lcom/google/android/gms/common/api/GoogleApiClient;Landroid/net/Uri;)Lcom/google/android/gms/common/api/PendingResult;

    .line 71
    return-void
.end method

.method public getAllDataItems(JLjava/util/concurrent/TimeUnit;)Ljava/util/Map;
    .locals 7
    .param p1, "timeout"    # J
    .param p3, "unit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/gms/wearable/DataMap;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;,
            Lcom/google/android/music/wear/util/GmsApiException;
        }
    .end annotation

    .prologue
    .line 39
    sget-object v5, Lcom/google/android/gms/wearable/Wearable;->DataApi:Lcom/google/android/gms/wearable/DataApi;

    iget-object v6, p0, Lcom/google/android/music/wear/util/DefaultDataApiWrapper;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v5, v6}, Lcom/google/android/gms/wearable/DataApi;->getDataItems(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v5

    invoke-interface {v5, p1, p2, p3}, Lcom/google/android/gms/common/api/PendingResult;->await(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/Result;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/DataItemBuffer;

    .line 42
    .local v1, "dataItemBuffer":Lcom/google/android/gms/wearable/DataItemBuffer;
    invoke-virtual {v1}, Lcom/google/android/gms/wearable/DataItemBuffer;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v4

    .line 43
    .local v4, "status":Lcom/google/android/gms/common/api/Status;
    invoke-virtual {v4}, Lcom/google/android/gms/common/api/Status;->isInterrupted()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 44
    new-instance v5, Ljava/lang/InterruptedException;

    invoke-direct {v5}, Ljava/lang/InterruptedException;-><init>()V

    throw v5

    .line 46
    :cond_0
    invoke-virtual {v4}, Lcom/google/android/gms/common/api/Status;->getStatusCode()I

    move-result v5

    const/16 v6, 0xf

    if-ne v5, v6, :cond_1

    .line 47
    new-instance v5, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v5}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v5

    .line 49
    :cond_1
    invoke-virtual {v4}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v5

    if-nez v5, :cond_2

    .line 50
    new-instance v5, Lcom/google/android/music/wear/util/GmsApiException;

    invoke-direct {v5, v4}, Lcom/google/android/music/wear/util/GmsApiException;-><init>(Lcom/google/android/gms/common/api/Status;)V

    throw v5

    .line 53
    :cond_2
    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->builder()Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    .line 54
    .local v0, "builder":Lcom/google/common/collect/ImmutableMap$Builder;, "Lcom/google/common/collect/ImmutableMap$Builder<Landroid/net/Uri;Lcom/google/android/gms/wearable/DataMap;>;"
    invoke-virtual {v1}, Lcom/google/android/gms/wearable/DataItemBuffer;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/wearable/DataItem;

    .line 55
    .local v3, "item":Lcom/google/android/gms/wearable/DataItem;
    invoke-interface {v3}, Lcom/google/android/gms/wearable/DataItem;->getUri()Landroid/net/Uri;

    move-result-object v6

    invoke-interface {v3}, Lcom/google/android/gms/wearable/DataItem;->freeze()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/wearable/DataItem;

    invoke-static {v5}, Lcom/google/android/gms/wearable/DataMapItem;->fromDataItem(Lcom/google/android/gms/wearable/DataItem;)Lcom/google/android/gms/wearable/DataMapItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/wearable/DataMapItem;->getDataMap()Lcom/google/android/gms/wearable/DataMap;

    move-result-object v5

    invoke-virtual {v0, v6, v5}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    goto :goto_0

    .line 58
    .end local v3    # "item":Lcom/google/android/gms/wearable/DataItem;
    :cond_3
    invoke-virtual {v1}, Lcom/google/android/gms/wearable/DataItemBuffer;->close()V

    .line 60
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v5

    return-object v5
.end method

.method public putDataItem(Lcom/google/android/gms/wearable/PutDataRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/gms/wearable/PutDataRequest;

    .prologue
    .line 65
    sget-object v0, Lcom/google/android/gms/wearable/Wearable;->DataApi:Lcom/google/android/gms/wearable/DataApi;

    iget-object v1, p0, Lcom/google/android/music/wear/util/DefaultDataApiWrapper;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/wearable/DataApi;->putDataItem(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/wearable/PutDataRequest;)Lcom/google/android/gms/common/api/PendingResult;

    .line 66
    return-void
.end method
