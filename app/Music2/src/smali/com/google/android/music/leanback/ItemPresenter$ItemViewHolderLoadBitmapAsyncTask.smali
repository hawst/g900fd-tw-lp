.class Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;
.super Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;
.source "ItemPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/ItemPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ItemViewHolderLoadBitmapAsyncTask"
.end annotation


# instance fields
.field private final mImageCardView:Lcom/google/android/music/leanback/ImageCardView;

.field private final mItem:Lcom/google/android/music/leanback/Item;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/ImageCardView;Lcom/google/android/music/leanback/Item;IIZ)V
    .locals 6
    .param p1, "imageCardView"    # Lcom/google/android/music/leanback/ImageCardView;
    .param p2, "item"    # Lcom/google/android/music/leanback/Item;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "cropToSquare"    # Z

    .prologue
    .line 40
    invoke-virtual {p1}, Lcom/google/android/music/leanback/ImageCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/music/leanback/Item;->getBitmapGettersGetter()Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    move-result-object v2

    move-object v0, p0

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;-><init>(Landroid/content/Context;Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;IIZ)V

    .line 42
    iput-object p1, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;->mImageCardView:Lcom/google/android/music/leanback/ImageCardView;

    .line 43
    iput-object p2, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;->mItem:Lcom/google/android/music/leanback/Item;

    .line 44
    return-void
.end method


# virtual methods
.method public onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    .line 49
    iget-object v2, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;->mImageCardView:Lcom/google/android/music/leanback/ImageCardView;

    const v3, 0x7f0e015f

    invoke-virtual {v2, v3}, Lcom/google/android/music/leanback/ImageCardView;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;->mItem:Lcom/google/android/music/leanback/Item;

    if-eq v2, v3, :cond_0

    .line 56
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;->mImageCardView:Lcom/google/android/music/leanback/ImageCardView;

    invoke-virtual {v2}, Lcom/google/android/music/leanback/ImageCardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 53
    .local v1, "resources":Landroid/content/res/Resources;
    if-eqz p1, :cond_1

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 55
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_1
    iget-object v2, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;->mImageCardView:Lcom/google/android/music/leanback/ImageCardView;

    # invokes: Lcom/google/android/music/leanback/ItemPresenter;->setImageView(Lcom/google/android/music/leanback/ImageCardView;Landroid/graphics/drawable/Drawable;)V
    invoke-static {v2, v0}, Lcom/google/android/music/leanback/ItemPresenter;->access$000(Lcom/google/android/music/leanback/ImageCardView;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 53
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    const v2, 0x7f02003a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 33
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
