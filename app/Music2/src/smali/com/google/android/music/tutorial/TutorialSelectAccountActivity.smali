.class public Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;
.super Lcom/google/android/music/tutorial/TutorialListActivity;
.source "TutorialSelectAccountActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;,
        Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$GetAuthTokenCallback;
    }
.end annotation


# instance fields
.field private mAccountSwitchFromDrawer:Z

.field private mAccounts:[Landroid/accounts/Account;

.field private mAdapter:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;

.field private mAddAccountButton:Landroid/widget/Button;

.field private mAuthTask:Landroid/accounts/AccountManagerFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private mContainer:Landroid/view/View;

.field private mCoupon:Ljava/lang/String;

.field private mCouponType:Ljava/lang/String;

.field private mCreatedAccountName:Ljava/lang/String;

.field private mCreatedAccountType:Ljava/lang/String;

.field private mGetOfferSpinner:Landroid/widget/ProgressBar;

.field private mGetSelectedAccountOffers:Lcom/google/android/music/tutorial/GetAccountOffersTask;

.field private mList:Landroid/widget/ListView;

.field private mNotNowButton:Landroid/widget/Button;

.field private mSelectedAccount:Landroid/accounts/Account;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialListActivity;-><init>()V

    .line 400
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mCreatedAccountName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mCreatedAccountType:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;Landroid/accounts/Account;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->authenticationSuccess(Landroid/accounts/Account;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->authenticationFailed(II)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAccountSwitchFromDrawer:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mSelectedAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->selectAccount()V

    return-void
.end method

.method private addAccountClick()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 224
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 225
    .local v4, "options":Landroid/os/Bundle;
    const-string v0, "introMessage"

    const v1, 0x7f0b012c

    invoke-virtual {p0, v1}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 226
    const-string v0, "allowSkip"

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 228
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    const-string v2, "sj"

    new-instance v6, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$1;

    invoke-direct {v6, p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$1;-><init>(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;)V

    move-object v5, p0

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 264
    return-void
.end method

.method private authenticationFailed(II)V
    .locals 2
    .param p1, "title"    # I
    .param p2, "message"    # I

    .prologue
    .line 341
    const-string v0, "MusicAccountSelect"

    const-string v1, "authenticationFailed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    new-instance v0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$2;-><init>(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;II)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 362
    return-void
.end method

.method private authenticationSuccess(Landroid/accounts/Account;)V
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 317
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->cancelGetOffers()V

    .line 319
    new-instance v0, Lcom/google/android/music/tutorial/GetAccountOffersTask;

    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mCoupon:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mCouponType:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;-><init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mGetSelectedAccountOffers:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .line 321
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mGetSelectedAccountOffers:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    invoke-virtual {v0}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->run()V

    .line 322
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->updateSpinner(ZZ)V

    .line 323
    return-void
.end method

.method private cancelAuthTask()V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAuthTask:Landroid/accounts/AccountManagerFuture;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAuthTask:Landroid/accounts/AccountManagerFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/accounts/AccountManagerFuture;->cancel(Z)Z

    .line 335
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAuthTask:Landroid/accounts/AccountManagerFuture;

    .line 337
    :cond_0
    return-void
.end method

.method private cancelGetOffers()V
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mGetSelectedAccountOffers:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mGetSelectedAccountOffers:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    invoke-virtual {v0}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->cancel()V

    .line 328
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mGetSelectedAccountOffers:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .line 330
    :cond_0
    return-void
.end method

.method private isFirstTimeTutorial()Z
    .locals 3

    .prologue
    .line 500
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "firstTimeTutorial"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private onAccountsChanged()V
    .locals 6

    .prologue
    .line 369
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAdapter:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;

    invoke-virtual {v4}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;->clear()V

    .line 370
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAccounts:[Landroid/accounts/Account;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAccounts:[Landroid/accounts/Account;

    array-length v4, v4

    if-eqz v4, :cond_1

    .line 371
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAccounts:[Landroid/accounts/Account;

    array-length v4, v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 372
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAccounts:[Landroid/accounts/Account;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iput-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mSelectedAccount:Landroid/accounts/Account;

    .line 374
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAccounts:[Landroid/accounts/Account;

    .local v1, "arr$":[Landroid/accounts/Account;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 375
    .local v0, "a":Landroid/accounts/Account;
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAdapter:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;

    invoke-virtual {v4, v0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;->add(Ljava/lang/Object;)V

    .line 374
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 378
    .end local v0    # "a":Landroid/accounts/Account;
    .end local v1    # "arr$":[Landroid/accounts/Account;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_1
    return-void
.end method

.method private refreshAccounts()V
    .locals 6

    .prologue
    .line 182
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->getAvailableAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAccounts:[Landroid/accounts/Account;

    .line 184
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mSelectedAccount:Landroid/accounts/Account;

    .line 186
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAdapter:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;

    if-nez v4, :cond_1

    .line 207
    :cond_0
    return-void

    .line 197
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->onAccountsChanged()V

    .line 198
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mCreatedAccountName:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mCreatedAccountType:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAccounts:[Landroid/accounts/Account;

    if-eqz v4, :cond_0

    .line 199
    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAccounts:[Landroid/accounts/Account;

    .local v1, "arr$":[Landroid/accounts/Account;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v0, v1, v2

    .line 200
    .local v0, "a":Landroid/accounts/Account;
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mCreatedAccountName:Ljava/lang/String;

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mCreatedAccountType:Ljava/lang/String;

    iget-object v5, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 201
    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mSelectedAccount:Landroid/accounts/Account;

    .line 202
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAdapter:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;

    invoke-virtual {v4}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;->notifyDataSetChanged()V

    .line 203
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->selectAccount()V

    .line 199
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private selectAccount()V
    .locals 5

    .prologue
    .line 267
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->cancelGetOffers()V

    .line 268
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->cancelAuthTask()V

    .line 269
    new-instance v0, Lcom/google/android/music/sync/google/MusicAuthInfo;

    invoke-direct {v0, p0}, Lcom/google/android/music/sync/google/MusicAuthInfo;-><init>(Landroid/content/Context;)V

    .line 271
    .local v0, "authInfo":Lcom/google/android/music/sync/google/MusicAuthInfo;
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mSelectedAccount:Landroid/accounts/Account;

    new-instance v3, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$GetAuthTokenCallback;

    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mSelectedAccount:Landroid/accounts/Account;

    invoke-direct {v3, p0, v4}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$GetAuthTokenCallback;-><init>(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;Landroid/accounts/Account;)V

    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    invoke-virtual {v0, p0, v2, v3, v4}, Lcom/google/android/music/sync/google/MusicAuthInfo;->getAuthTokenForeground(Landroid/app/Activity;Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAuthTask:Landroid/accounts/AccountManagerFuture;
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    :goto_0
    return-void

    .line 274
    :catch_0
    move-exception v1

    .line 275
    .local v1, "e":Landroid/accounts/AuthenticatorException;
    const-string v2, "MusicAccountSelect"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAuthToken failed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateSpinner(ZZ)V
    .locals 3
    .param p1, "syncStart"    # Z
    .param p2, "switchScreen"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 527
    if-eqz p1, :cond_1

    .line 528
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 529
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mGetOfferSpinner:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 538
    :cond_0
    :goto_0
    return-void

    .line 530
    :cond_1
    if-nez p2, :cond_0

    .line 531
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 532
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mGetOfferSpinner:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 43
    invoke-super {p0}, Lcom/google/android/music/tutorial/TutorialListActivity;->isActivityDestroyed()Z

    move-result v0

    return v0
.end method

.method public onAccountDisabled(Landroid/accounts/Account;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v1, 0x0

    .line 493
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->updateSpinner(ZZ)V

    .line 494
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->setResult(I)V

    .line 495
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->setInvalidStreamingAccount(Landroid/accounts/Account;)V

    .line 496
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-static {p0, v1, v1, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->finishTutorialPermanently(Landroid/app/Activity;ZZLcom/google/android/music/preferences/MusicPreferences;)V

    .line 497
    return-void
.end method

.method public onAccountInvalid(Landroid/accounts/Account;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v1, 0x0

    .line 485
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->updateSpinner(ZZ)V

    .line 486
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->setResult(I)V

    .line 487
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->setInvalidStreamingAccount(Landroid/accounts/Account;)V

    .line 488
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-static {p0, v1, v1, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->finishTutorialPermanently(Landroid/app/Activity;ZZLcom/google/android/music/preferences/MusicPreferences;)V

    .line 489
    return-void
.end method

.method public onAccountOffersError(Landroid/accounts/Account;)V
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v1, 0x0

    .line 464
    invoke-direct {p0, v1, v1}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->updateSpinner(ZZ)V

    .line 466
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b0045

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b0046

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b004e

    new-instance v3, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$3;

    invoke-direct {v3, p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$3;-><init>(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    .line 481
    .local v0, "dialog":Landroid/app/AlertDialog;
    return-void
.end method

.method public onAccountOffersSuccess(Landroid/accounts/Account;Lcom/google/android/music/cloudclient/OffersResponseJson;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "availableOffers"    # Lcom/google/android/music/cloudclient/OffersResponseJson;

    .prologue
    const/4 v2, 0x0

    .line 451
    const/4 v0, 0x1

    invoke-direct {p0, v2, v0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->updateSpinner(ZZ)V

    .line 452
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->setResult(I)V

    .line 453
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->setStreamingAccount(Landroid/accounts/Account;)V

    .line 454
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "changeAccountOnly"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    invoke-static {p0}, Lcom/google/android/music/tutorial/TutorialUtils;->finishTutorialTemporarily(Landroid/app/Activity;)V

    .line 460
    :goto_0
    return-void

    .line 458
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-static {p2, p0, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->openTryNautilusOrFinishTutorial(Lcom/google/android/music/cloudclient/OffersResponseJson;Landroid/app/Activity;Lcom/google/android/music/preferences/MusicPreferences;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 211
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 221
    :goto_0
    return-void

    .line 213
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->addAccountClick()V

    goto :goto_0

    .line 216
    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->setResult(I)V

    .line 217
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-static {p0, v1, v1, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->finishTutorialPermanently(Landroid/app/Activity;ZZLcom/google/android/music/preferences/MusicPreferences;)V

    goto :goto_0

    .line 211
    :pswitch_data_0
    .packed-switch 0x7f0e0252
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 105
    invoke-super {p0, p1}, Lcom/google/android/music/tutorial/TutorialListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 107
    const v4, 0x7f040102

    invoke-virtual {p0, v4}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->setContentView(I)V

    .line 109
    const v4, 0x7f0e01c5

    invoke-virtual {p0, v4}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 110
    .local v3, "titleView":Landroid/widget/TextView;
    invoke-static {v3, v7}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 112
    const v4, 0x7f0e0251

    invoke-virtual {p0, v4}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 113
    .local v1, "description":Landroid/widget/TextView;
    invoke-static {v1, v7}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getListView()Landroid/widget/ListView;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mList:Landroid/widget/ListView;

    .line 116
    new-instance v4, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;

    invoke-direct {v4, p0, p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;-><init>(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAdapter:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;

    .line 117
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mList:Landroid/widget/ListView;

    iget-object v7, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAdapter:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;

    invoke-virtual {v4, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 118
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mList:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 120
    const v4, 0x7f0e00e3

    invoke-virtual {p0, v4}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mContainer:Landroid/view/View;

    .line 121
    const v4, 0x7f0e0284

    invoke-virtual {p0, v4}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    iput-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mGetOfferSpinner:Landroid/widget/ProgressBar;

    .line 123
    const v4, 0x7f0e0252

    invoke-virtual {p0, v4}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAddAccountButton:Landroid/widget/Button;

    .line 124
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAddAccountButton:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAddAccountButton:Landroid/widget/Button;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 126
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAddAccountButton:Landroid/widget/Button;

    invoke-static {v4, v5}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 128
    const v4, 0x7f0e0253

    invoke-virtual {p0, v4}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mNotNowButton:Landroid/widget/Button;

    .line 129
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mNotNowButton:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mNotNowButton:Landroid/widget/Button;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 131
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mNotNowButton:Landroid/widget/Button;

    invoke-static {v4, v5}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v7, "coupon"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mCoupon:Ljava/lang/String;

    .line 134
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v7, "couponType"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mCouponType:Ljava/lang/String;

    .line 138
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->disableAutoSelect()V

    .line 141
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v7, "accountToChangeTo"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 142
    .local v0, "accountIn":Landroid/accounts/Account;
    if-eqz v0, :cond_1

    move v4, v5

    :goto_0
    iput-boolean v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAccountSwitchFromDrawer:Z

    .line 143
    iget-boolean v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAccountSwitchFromDrawer:Z

    if-eqz v4, :cond_0

    .line 144
    const v4, 0x7f0e027d

    invoke-virtual {p0, v4}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 145
    .local v2, "header":Landroid/view/View;
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 146
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mContainer:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 150
    .end local v2    # "header":Landroid/view/View;
    :cond_0
    invoke-virtual {p0, v6}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->setResult(I)V

    .line 151
    return-void

    :cond_1
    move v4, v6

    .line 142
    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->cancelGetOffers()V

    .line 156
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->cancelAuthTask()V

    .line 157
    invoke-super {p0}, Lcom/google/android/music/tutorial/TutorialListActivity;->onDestroy()V

    .line 158
    return-void
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 382
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 386
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 387
    .local v0, "currentAccount":Landroid/accounts/Account;
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;

    # getter for: Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;->account:Landroid/accounts/Account;
    invoke-static {v1}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;->access$500(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;)Landroid/accounts/Account;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mSelectedAccount:Landroid/accounts/Account;

    .line 388
    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mAdapter:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;

    invoke-virtual {v1}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;->notifyDataSetChanged()V

    .line 389
    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mSelectedAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    .line 392
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->isFirstTimeTutorial()Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mSelectedAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v0}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 394
    :cond_2
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->selectAccount()V

    goto :goto_0

    .line 396
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->showAccountSwitchWarning()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 176
    invoke-super {p0}, Lcom/google/android/music/tutorial/TutorialListActivity;->onPause()V

    .line 177
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 162
    invoke-super {p0}, Lcom/google/android/music/tutorial/TutorialListActivity;->onResume()V

    .line 163
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "accountToChangeTo"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 164
    .local v0, "accountIn":Landroid/accounts/Account;
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->refreshAccounts()V

    .line 165
    if-eqz v0, :cond_0

    .line 169
    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mSelectedAccount:Landroid/accounts/Account;

    .line 170
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->selectAccount()V

    .line 172
    :cond_0
    return-void
.end method

.method public showAccountSwitchWarning()V
    .locals 4

    .prologue
    .line 504
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 505
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0b020e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b004f

    new-instance v3, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$4;

    invoke-direct {v3, p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$4;-><init>(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 514
    const v1, 0x7f0b0210

    new-instance v2, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$5;

    invoke-direct {v2, p0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$5;-><init>(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 523
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 524
    return-void
.end method
