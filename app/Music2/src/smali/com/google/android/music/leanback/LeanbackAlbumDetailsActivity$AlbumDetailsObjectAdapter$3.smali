.class Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$3;
.super Ljava/lang/Object;
.source "LeanbackAlbumDetailsActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->recalculateSize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mAction:Landroid/support/v17/leanback/widget/Action;

.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

.field final synthetic val$detailsOverviewRow:Landroid/support/v17/leanback/widget/DetailsOverviewRow;

.field final synthetic val$item:Lcom/google/android/music/leanback/Item;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/DetailsOverviewRow;)V
    .locals 0

    .prologue
    .line 378
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$3;->this$1:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$3;->val$item:Lcom/google/android/music/leanback/Item;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$3;->val$detailsOverviewRow:Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 4

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$3;->this$1:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$3;->val$item:Lcom/google/android/music/leanback/Item;

    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$3;->val$item:Lcom/google/android/music/leanback/Item;

    invoke-virtual {v2}, Lcom/google/android/music/leanback/Item;->getTitle()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    # invokes: Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->createStartRadioAction(Ljava/lang/String;Ljava/lang/String;I)Landroid/support/v17/leanback/widget/Action;
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->access$1000(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Ljava/lang/String;Ljava/lang/String;I)Landroid/support/v17/leanback/widget/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$3;->mAction:Landroid/support/v17/leanback/widget/Action;

    .line 385
    return-void
.end method

.method public taskCompleted()V
    .locals 3

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$3;->val$detailsOverviewRow:Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$3;->mAction:Landroid/support/v17/leanback/widget/Action;

    invoke-virtual {v0, v1, v2}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->addAction(ILandroid/support/v17/leanback/widget/Action;)V

    .line 390
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$3;->this$1:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    const/4 v1, 0x0

    const/4 v2, 0x1

    # invokes: Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->notifyItemRangeChanged(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->access$1100(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;II)V

    .line 391
    return-void
.end method
