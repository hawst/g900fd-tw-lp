.class public Lcom/google/android/music/leanback/DailyWindow;
.super Ljava/lang/Object;
.source "DailyWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/DailyWindow$1;,
        Lcom/google/android/music/leanback/DailyWindow$Builder;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mDurationInMillisec:I

.field private final mFrequencyInMillisec:J

.field private final mStartInMillisecSinceMidnight:I

.field private final mVariableTriggerPercentage:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-boolean v0, Lcom/google/android/music/leanback/LeanbackLog;->LOGV:Z

    sput-boolean v0, Lcom/google/android/music/leanback/DailyWindow;->LOGV:Z

    return-void
.end method

.method private constructor <init>(IIJI)V
    .locals 1
    .param p1, "startInMillisecSinceMidnight"    # I
    .param p2, "durationInMillisec"    # I
    .param p3, "periodInMillisec"    # J
    .param p5, "variableTriggerPercentage"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput p1, p0, Lcom/google/android/music/leanback/DailyWindow;->mStartInMillisecSinceMidnight:I

    .line 54
    iput p2, p0, Lcom/google/android/music/leanback/DailyWindow;->mDurationInMillisec:I

    .line 55
    iput-wide p3, p0, Lcom/google/android/music/leanback/DailyWindow;->mFrequencyInMillisec:J

    .line 56
    iput p5, p0, Lcom/google/android/music/leanback/DailyWindow;->mVariableTriggerPercentage:I

    .line 57
    return-void
.end method

.method synthetic constructor <init>(IIJILcom/google/android/music/leanback/DailyWindow$1;)V
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # J
    .param p5, "x3"    # I
    .param p6, "x4"    # Lcom/google/android/music/leanback/DailyWindow$1;

    .prologue
    .line 13
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/leanback/DailyWindow;-><init>(IIJI)V

    return-void
.end method

.method private static getMillsecSinceMidnight(JI)Ljava/util/Calendar;
    .locals 4
    .param p0, "date"    # J
    .param p2, "millisec"    # I

    .prologue
    const/4 v2, 0x0

    .line 175
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 176
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 177
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 178
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 179
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 180
    const/16 v1, 0xe

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 181
    return-object v0
.end method

.method private static moveByDaysAndMillisec(Ljava/util/Calendar;J)J
    .locals 7
    .param p0, "cal"    # Ljava/util/Calendar;
    .param p1, "millisec"    # J

    .prologue
    const-wide/32 v4, 0x5265c00

    .line 194
    div-long v2, p1, v4

    long-to-int v0, v2

    .line 195
    .local v0, "days":I
    if-lez v0, :cond_0

    .line 196
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v0}, Ljava/util/Calendar;->add(II)V

    .line 198
    :cond_0
    rem-long v2, p1, v4

    long-to-int v1, v2

    .line 199
    .local v1, "remainingMillisec":I
    if-lez v1, :cond_1

    .line 200
    const/16 v2, 0xe

    invoke-virtual {p0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 202
    :cond_1
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method private moveByDuration(Ljava/util/Calendar;)J
    .locals 2
    .param p1, "cal"    # Ljava/util/Calendar;

    .prologue
    .line 185
    iget v0, p0, Lcom/google/android/music/leanback/DailyWindow;->mDurationInMillisec:I

    int-to-long v0, v0

    invoke-static {p1, v0, v1}, Lcom/google/android/music/leanback/DailyWindow;->moveByDaysAndMillisec(Ljava/util/Calendar;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private moveByFrequency(Ljava/util/Calendar;)J
    .locals 2
    .param p1, "cal"    # Ljava/util/Calendar;

    .prologue
    .line 189
    iget-wide v0, p0, Lcom/google/android/music/leanback/DailyWindow;->mFrequencyInMillisec:J

    invoke-static {p1, v0, v1}, Lcom/google/android/music/leanback/DailyWindow;->moveByDaysAndMillisec(Ljava/util/Calendar;J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public geFrequencyInMillisec()J
    .locals 2

    .prologue
    .line 164
    iget-wide v0, p0, Lcom/google/android/music/leanback/DailyWindow;->mFrequencyInMillisec:J

    return-wide v0
.end method

.method getTriggerTime(J)J
    .locals 21
    .param p1, "lastTriggerTime"    # J

    .prologue
    .line 80
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 81
    .local v6, "now":J
    move-wide v8, v6

    .line 82
    .local v8, "triggerTime":J
    const-wide/16 v16, 0x0

    cmp-long v11, p1, v16

    if-lez v11, :cond_0

    .line 83
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 84
    .local v2, "c":Ljava/util/Calendar;
    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 85
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/leanback/DailyWindow;->moveByFrequency(Ljava/util/Calendar;)J

    move-result-wide v8

    .line 86
    cmp-long v11, v8, v6

    if-gez v11, :cond_0

    .line 87
    move-wide v8, v6

    .line 92
    .end local v2    # "c":Ljava/util/Calendar;
    :cond_0
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/music/leanback/DailyWindow;->mStartInMillisecSinceMidnight:I

    invoke-static {v8, v9, v11}, Lcom/google/android/music/leanback/DailyWindow;->getMillsecSinceMidnight(JI)Ljava/util/Calendar;

    move-result-object v2

    .line 93
    .restart local v2    # "c":Ljava/util/Calendar;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v14

    .line 94
    .local v14, "windowStart":J
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/leanback/DailyWindow;->moveByDuration(Ljava/util/Calendar;)J

    move-result-wide v12

    .line 95
    .local v12, "windowEnd":J
    cmp-long v11, v8, v12

    if-lez v11, :cond_1

    .line 97
    invoke-virtual {v2, v14, v15}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 98
    const/4 v11, 0x6

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v2, v11, v0}, Ljava/util/Calendar;->add(II)V

    .line 99
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v14

    .line 100
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/leanback/DailyWindow;->moveByDuration(Ljava/util/Calendar;)J

    move-result-wide v12

    .line 103
    :cond_1
    cmp-long v11, v8, v12

    if-lez v11, :cond_2

    .line 104
    const-string v11, "MusicLeanback"

    const-string v16, "Error in window calculation"

    new-instance v17, Ljava/lang/Exception;

    invoke-direct/range {v17 .. v17}, Ljava/lang/Exception;-><init>()V

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v11, v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 107
    :cond_2
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/music/leanback/DailyWindow;->mVariableTriggerPercentage:I

    if-lez v11, :cond_5

    .line 109
    sub-long v4, v12, v14

    .line 110
    .local v4, "effectiveDuration":J
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/music/leanback/DailyWindow;->mVariableTriggerPercentage:I

    int-to-long v0, v11

    move-wide/from16 v16, v0

    mul-long v16, v16, v4

    const-wide/16 v18, 0x64

    div-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v10, v0

    .line 113
    .local v10, "variableTriggerOffsetInMillisec":I
    new-instance v11, Ljava/util/Random;

    invoke-direct {v11}, Ljava/util/Random;-><init>()V

    invoke-virtual {v11, v10}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    .line 114
    .local v3, "randomOffSet":I
    int-to-long v0, v3

    move-wide/from16 v16, v0

    add-long v8, v14, v16

    .line 120
    .end local v3    # "randomOffSet":I
    .end local v4    # "effectiveDuration":J
    .end local v10    # "variableTriggerOffsetInMillisec":I
    :cond_3
    :goto_0
    cmp-long v11, v8, v6

    if-gez v11, :cond_4

    .line 121
    move-wide v8, v6

    .line 124
    :cond_4
    return-wide v8

    .line 115
    :cond_5
    cmp-long v11, v8, v14

    if-gez v11, :cond_3

    .line 117
    move-wide v8, v14

    goto :goto_0
.end method

.method public isInSameWindow(JJ)Z
    .locals 11
    .param p1, "time1"    # J
    .param p3, "time2"    # J

    .prologue
    .line 134
    cmp-long v1, p1, p3

    if-lez v1, :cond_0

    .line 135
    move-wide v6, p3

    .line 136
    .local v6, "earlierTime":J
    move-wide v8, p1

    .line 142
    .local v8, "laterTime":J
    :goto_0
    iget v1, p0, Lcom/google/android/music/leanback/DailyWindow;->mStartInMillisecSinceMidnight:I

    invoke-static {v6, v7, v1}, Lcom/google/android/music/leanback/DailyWindow;->getMillsecSinceMidnight(JI)Ljava/util/Calendar;

    move-result-object v0

    .line 143
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 144
    .local v4, "dailyWindowStart":J
    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/DailyWindow;->moveByDuration(Ljava/util/Calendar;)J

    move-result-wide v2

    .line 146
    .local v2, "dailyWindowEnd":J
    cmp-long v1, v6, v4

    if-ltz v1, :cond_1

    cmp-long v1, v6, v2

    if-gtz v1, :cond_1

    cmp-long v1, v8, v4

    if-ltz v1, :cond_1

    cmp-long v1, v8, v2

    if-gtz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 138
    .end local v0    # "c":Ljava/util/Calendar;
    .end local v2    # "dailyWindowEnd":J
    .end local v4    # "dailyWindowStart":J
    .end local v6    # "earlierTime":J
    .end local v8    # "laterTime":J
    :cond_0
    move-wide v6, p1

    .line 139
    .restart local v6    # "earlierTime":J
    move-wide v8, p3

    .restart local v8    # "laterTime":J
    goto :goto_0

    .line 146
    .restart local v0    # "c":Ljava/util/Calendar;
    .restart local v2    # "dailyWindowEnd":J
    .restart local v4    # "dailyWindowStart":J
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isInWindow(J)Z
    .locals 7
    .param p1, "time"    # J

    .prologue
    .line 154
    iget v1, p0, Lcom/google/android/music/leanback/DailyWindow;->mStartInMillisecSinceMidnight:I

    invoke-static {p1, p2, v1}, Lcom/google/android/music/leanback/DailyWindow;->getMillsecSinceMidnight(JI)Ljava/util/Calendar;

    move-result-object v0

    .line 155
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 156
    .local v4, "dailyWindowStart":J
    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/DailyWindow;->moveByDuration(Ljava/util/Calendar;)J

    move-result-wide v2

    .line 157
    .local v2, "dailyWindowEnd":J
    cmp-long v1, p1, v4

    if-ltz v1, :cond_0

    cmp-long v1, p1, v2

    if-gtz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "Start: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/leanback/DailyWindow;->mStartInMillisecSinceMidnight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    const-string v1, "Duration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/leanback/DailyWindow;->mDurationInMillisec:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    const-string v1, "Frequency: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/leanback/DailyWindow;->mFrequencyInMillisec:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    const-string v1, "Trigger %: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/leanback/DailyWindow;->mVariableTriggerPercentage:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
