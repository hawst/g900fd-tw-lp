.class public interface abstract Lcom/google/android/music/download/artwork/ArtMonitor;
.super Ljava/lang/Object;
.source "ArtMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;,
        Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;
    }
.end annotation


# virtual methods
.method public abstract getWatchedArtUrls()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract registerArtChangeListener(Ljava/lang/String;Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;)V
.end method

.method public abstract registerArtServiceIdleListener(Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;)V
.end method

.method public abstract unregisterArtChangeListener(Ljava/lang/String;Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;)V
.end method

.method public abstract unregisterArtServiceIdleListener(Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;)V
.end method
