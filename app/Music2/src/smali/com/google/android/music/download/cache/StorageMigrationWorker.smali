.class public Lcom/google/android/music/download/cache/StorageMigrationWorker;
.super Ljava/lang/Object;
.source "StorageMigrationWorker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;,
        Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;,
        Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private mBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

.field private mCallbacks:Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;

.field private mContext:Landroid/content/Context;

.field private final mFileIdsToCopy:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

.field private mNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mScheduledRunnable:Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;

.field private mTargetLocation:Lcom/google/android/music/download/cache/CacheLocation;

.field private mTargetLocationId:Ljava/util/UUID;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CACHE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mFileIdsToCopy:Ljava/util/LinkedList;

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mContext:Landroid/content/Context;

    .line 67
    invoke-static {p1}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    .line 68
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationManager:Landroid/app/NotificationManager;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/download/cache/StorageMigrationWorker;Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;Ljava/util/UUID;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationWorker;
    .param p1, "x1"    # Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;
    .param p2, "x2"    # Ljava/util/UUID;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->initialize(Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;Ljava/util/UUID;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/download/cache/StorageMigrationWorker;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationWorker;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mFileIdsToCopy:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/download/cache/StorageMigrationWorker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationWorker;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->moveToNextFileOrEnd()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/download/cache/StorageMigrationWorker;)Landroid/app/NotificationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationWorker;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationManager:Landroid/app/NotificationManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/download/cache/StorageMigrationWorker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationWorker;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->shutdownBackgroundWorker()V

    return-void
.end method

.method static synthetic access$402(Lcom/google/android/music/download/cache/StorageMigrationWorker;Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;)Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationWorker;
    .param p1, "x1"    # Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mCallbacks:Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/music/download/cache/StorageMigrationWorker;Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationWorker;
    .param p1, "x1"    # Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->doCopy(Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/music/download/cache/StorageMigrationWorker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationWorker;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->updateRemainingString()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/music/download/cache/StorageMigrationWorker;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationWorker;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    return-object v0
.end method

.method private doCopy(Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;)Z
    .locals 20
    .param p1, "details"    # Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;

    .prologue
    .line 386
    sget-boolean v13, Lcom/google/android/music/download/cache/StorageMigrationWorker;->LOGV:Z

    if-eqz v13, :cond_0

    .line 387
    const-string v13, "StorageMigrationWkr"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "doCopy="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->currentVolume:Ljava/util/UUID;

    invoke-virtual {v13, v14}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownLocationByID(Ljava/util/UUID;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v9

    .line 391
    .local v9, "sourceLocation":Lcom/google/android/music/download/cache/CacheLocation;
    const/4 v8, 0x0

    .line 392
    .local v8, "sourceFile":Ljava/io/File;
    const/4 v3, 0x1

    .line 393
    .local v3, "copyDone":Z
    if-eqz v9, :cond_4

    .line 394
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    invoke-virtual {v13, v9}, Lcom/google/android/music/download/cache/CacheLocationManager;->asMusicCacheLocation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v7

    .line 395
    .local v7, "location":Lcom/google/android/music/download/cache/CacheLocation;
    if-eqz v7, :cond_1

    move-object v9, v7

    .line 396
    :cond_1
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->path:Ljava/lang/String;

    invoke-virtual {v9, v13}, Lcom/google/android/music/download/cache/CacheLocation;->getCacheFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v8

    .line 406
    .end local v7    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    :goto_0
    if-eqz v8, :cond_2

    .line 407
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_6

    .line 408
    sget-boolean v13, Lcom/google/android/music/download/cache/StorageMigrationWorker;->LOGV:Z

    if-eqz v13, :cond_2

    .line 409
    const-string v13, "StorageMigrationWkr"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Source file at "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " does not exist"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const/4 v3, 0x0

    .line 428
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v10

    .line 429
    .local v10, "store":Lcom/google/android/music/store/Store;
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 430
    .local v12, "values":Landroid/content/ContentValues;
    if-eqz v3, :cond_7

    .line 431
    const-string v13, "LocalCopyStorageVolumeId"

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mTargetLocationId:Ljava/util/UUID;

    invoke-virtual {v14}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    const-string v13, "LocalCopyPath"

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->path:Ljava/lang/String;

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    const-string v13, "LocalCopyType"

    move-object/from16 v0, p1

    iget v14, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->type:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 434
    const-string v13, "LocalCopyStorageType"

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mTargetLocation:Lcom/google/android/music/download/cache/CacheLocation;

    invoke-virtual {v14}, Lcom/google/android/music/download/cache/CacheLocation;->getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->ordinal()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 436
    const-string v13, "LocalCopyStreamFidelity"

    move-object/from16 v0, p1

    iget v14, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->streamFidelity:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 446
    :goto_2
    invoke-virtual {v10}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 447
    .local v4, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, -0x1

    .line 448
    .local v2, "affected":I
    const/4 v5, 0x0

    .line 450
    .local v5, "dbUpdateDone":Z
    :try_start_0
    const-string v13, "MUSIC"

    const-string v14, "Id=?"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->id:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v4, v13, v12, v14, v15}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 452
    const/4 v5, 0x1

    .line 454
    invoke-virtual {v10, v4, v5}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 456
    sget-boolean v13, Lcom/google/android/music/download/cache/StorageMigrationWorker;->LOGV:Z

    if-eqz v13, :cond_3

    .line 457
    const-string v13, "StorageMigrationWkr"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Updated DB for track "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->id:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", rows affected: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    :cond_3
    if-eqz v3, :cond_8

    if-eqz v5, :cond_8

    const/4 v13, 0x1

    :goto_3
    return v13

    .line 399
    .end local v2    # "affected":I
    .end local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v5    # "dbUpdateDone":Z
    .end local v10    # "store":Lcom/google/android/music/store/Store;
    .end local v12    # "values":Landroid/content/ContentValues;
    :cond_4
    sget-boolean v13, Lcom/google/android/music/download/cache/StorageMigrationWorker;->LOGV:Z

    if-eqz v13, :cond_5

    .line 400
    const-string v13, "StorageMigrationWkr"

    const-string v14, "Source volume %s is missing, file will not be copied."

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->currentVolume:Ljava/util/UUID;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 414
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mTargetLocation:Lcom/google/android/music/download/cache/CacheLocation;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->path:Ljava/lang/String;

    invoke-virtual {v13, v14}, Lcom/google/android/music/download/cache/CacheLocation;->getCacheFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v11

    .line 416
    .local v11, "targetFile":Ljava/io/File;
    :try_start_1
    invoke-static {v8, v11}, Lcom/google/common/io/Files;->copy(Ljava/io/File;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 417
    :catch_0
    move-exception v6

    .line 418
    .local v6, "e":Ljava/io/IOException;
    sget-boolean v13, Lcom/google/android/music/download/cache/StorageMigrationWorker;->LOGV:Z

    if-eqz v13, :cond_2

    .line 419
    const-string v13, "StorageMigrationWkr"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Failed to copy file from "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " to "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v6}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 421
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 438
    .end local v6    # "e":Ljava/io/IOException;
    .end local v11    # "targetFile":Ljava/io/File;
    .restart local v10    # "store":Lcom/google/android/music/store/Store;
    .restart local v12    # "values":Landroid/content/ContentValues;
    :cond_7
    const-string v13, "LocalCopyStorageVolumeId"

    invoke-virtual {v12, v13}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 439
    const-string v13, "LocalCopyPath"

    invoke-virtual {v12, v13}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 440
    const-string v13, "LocalCopyType"

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 441
    const-string v13, "LocalCopyStorageType"

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 443
    const-string v13, "LocalCopyStreamFidelity"

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_2

    .line 454
    .restart local v2    # "affected":I
    .restart local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v5    # "dbUpdateDone":Z
    :catchall_0
    move-exception v13

    invoke-virtual {v10, v4, v5}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v13

    .line 459
    :cond_8
    const/4 v13, 0x0

    goto/16 :goto_3
.end method

.method private endMigration()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 181
    sget-boolean v2, Lcom/google/android/music/download/cache/StorageMigrationWorker;->LOGV:Z

    if-eqz v2, :cond_0

    .line 182
    const-string v2, "StorageMigrationWkr"

    const-string v3, "endMigration"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mContext:Landroid/content/Context;

    const-string v3, "migration.preferences"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 186
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 187
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v2, "storageMigrationInProgress"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 188
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 189
    iget-object v2, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v3, 0x425

    invoke-virtual {v2, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 190
    invoke-direct {p0}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->shutdownBackgroundWorker()V

    .line 191
    invoke-direct {p0}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->notifyServiceMigrationDone()V

    .line 192
    return-void
.end method

.method private initialize(Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;Ljava/util/UUID;)V
    .locals 21
    .param p1, "callbacks"    # Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;
    .param p2, "selectedVolumeId"    # Ljava/util/UUID;

    .prologue
    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mFileIdsToCopy:Ljava/util/LinkedList;

    move-object/from16 v20, v0

    monitor-enter v20

    .line 222
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->shutdownBackgroundWorker()V

    .line 223
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mCallbacks:Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mCallbacks:Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;

    move-object/from16 v0, p1

    if-eq v0, v3, :cond_0

    .line 224
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->notifyServiceMigrationDone()V

    .line 226
    :cond_0
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mCallbacks:Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;

    .line 227
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v4, 0x425

    invoke-virtual {v3, v4}, Landroid/app/NotificationManager;->cancel(I)V

    .line 229
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->isMainProcess(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 230
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " must be run on main process"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 318
    :catchall_0
    move-exception v3

    monitor-exit v20
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 233
    :cond_1
    :try_start_1
    sget-boolean v3, Lcom/google/android/music/download/cache/StorageMigrationWorker;->LOGV:Z

    if-eqz v3, :cond_2

    .line 234
    const-string v3, "StorageMigrationWkr"

    const-string v4, "Starting initialization"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_2
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mTargetLocation:Lcom/google/android/music/download/cache/CacheLocation;

    .line 238
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mFileIdsToCopy:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->clear()V

    .line 241
    const/16 v18, 0x0

    .line 242
    .local v18, "targetMeta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    if-nez p2, :cond_7

    .line 244
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInternal(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v13

    .line 245
    .local v13, "cacheLocation":Lcom/google/android/music/download/cache/CacheLocation;
    if-nez v13, :cond_4

    .line 246
    sget-boolean v3, Lcom/google/android/music/download/cache/StorageMigrationWorker;->LOGV:Z

    if-eqz v3, :cond_3

    .line 247
    const-string v3, "StorageMigrationWkr"

    const-string v4, "Failed to get default internal storage location."

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :cond_3
    monitor-exit v20

    .line 319
    :goto_0
    return-void

    .line 251
    :cond_4
    invoke-virtual {v13}, Lcom/google/android/music/download/cache/CacheLocation;->getVolumeId()Ljava/util/UUID;

    move-result-object p2

    .line 263
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    invoke-virtual {v3, v13}, Lcom/google/android/music/download/cache/CacheLocationManager;->getStorageMetadata(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    move-result-object v18

    .line 264
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    invoke-virtual {v3, v13}, Lcom/google/android/music/download/cache/CacheLocationManager;->asMusicCacheLocation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v17

    .line 265
    .local v17, "musicLocation":Lcom/google/android/music/download/cache/CacheLocation;
    if-nez v17, :cond_6

    move-object/from16 v17, v13

    .line 266
    :cond_6
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v15

    .line 267
    .local v15, "f":Ljava/io/File;
    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_8

    invoke-virtual {v15}, Ljava/io/File;->mkdirs()Z

    move-result v3

    if-nez v3, :cond_8

    .line 268
    const-string v3, "StorageMigrationWkr"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to prepare target location "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", file migration paused."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    monitor-exit v20

    goto :goto_0

    .line 253
    .end local v13    # "cacheLocation":Lcom/google/android/music/download/cache/CacheLocation;
    .end local v15    # "f":Ljava/io/File;
    .end local v17    # "musicLocation":Lcom/google/android/music/download/cache/CacheLocation;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownLocationByID(Ljava/util/UUID;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v13

    .line 254
    .restart local v13    # "cacheLocation":Lcom/google/android/music/download/cache/CacheLocation;
    if-nez v13, :cond_5

    .line 256
    const-string v3, "StorageMigrationWkr"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Selected volume for migration "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " is not "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "currently available. Pausing migration until it becomes available"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    monitor-exit v20

    goto :goto_0

    .line 272
    .restart local v15    # "f":Ljava/io/File;
    .restart local v17    # "musicLocation":Lcom/google/android/music/download/cache/CacheLocation;
    :cond_8
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mTargetLocation:Lcom/google/android/music/download/cache/CacheLocation;

    .line 273
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/music/download/cache/CacheLocation;->getVolumeId()Ljava/util/UUID;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mTargetLocationId:Ljava/util/UUID;

    .line 274
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v2

    .line 275
    .local v2, "store":Lcom/google/android/music/store/Store;
    const/4 v3, 0x6

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Id"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "LocalCopyType"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "LocalCopyPath"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string v4, "LocalCopyStorageVolumeId"

    aput-object v4, v5, v3

    const/4 v3, 0x4

    const-string v4, "LocalCopySize"

    aput-object v4, v5, v3

    const/4 v3, 0x5

    const-string v4, "LocalCopyStreamFidelity"

    aput-object v4, v5, v3

    .line 283
    .local v5, "columns":[Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 284
    .local v19, "whereBuilder":Ljava/lang/StringBuilder;
    const-string v3, "LocalCopyType"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    const/4 v3, 0x2

    new-array v3, v3, [J

    fill-array-data v3, :array_0

    move-object/from16 v0, v19

    invoke-static {v0, v3}, Lcom/google/android/music/utils/DbUtils;->appendIN(Ljava/lang/StringBuilder;[J)V

    .line 287
    const-string v3, "LocalCopyStorageVolumeId!=?"

    move-object/from16 v0, v19

    invoke-static {v0, v3}, Lcom/google/android/music/utils/DbUtils;->addAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    const/4 v3, 0x1

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual/range {p2 .. p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v3

    .line 291
    .local v7, "whereParams":[Ljava/lang/String;
    const/4 v3, 0x0

    const-string v4, "MUSIC"

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, "LocalCopyType DESC"

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Lcom/google/android/music/store/Store;->executeQuery(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v12

    .line 295
    .local v12, "c":Landroid/database/Cursor;
    :cond_9
    :goto_1
    if-eqz v12, :cond_a

    :try_start_2
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 297
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_9

    const/4 v3, 0x3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_9

    .line 298
    new-instance v16, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;

    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;-><init>(Lcom/google/android/music/download/cache/StorageMigrationWorker$1;)V

    .line 299
    .local v16, "fcd":Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    move-object/from16 v0, v16

    iput-wide v8, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->id:J

    .line 300
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v16

    iput v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->type:I

    .line 301
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    iput-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->path:Ljava/lang/String;

    .line 302
    const/4 v3, 0x3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->getUUIDFromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    move-object/from16 v0, v16

    iput-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->currentVolume:Ljava/util/UUID;

    .line 303
    const/4 v3, 0x4

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    move-object/from16 v0, v16

    iput-wide v8, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->fileSize:J

    .line 304
    const/4 v3, 0x5

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v16

    iput v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->streamFidelity:I

    .line 305
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->isValid()Z

    move-result v3

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mFileIdsToCopy:Ljava/util/LinkedList;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 308
    .end local v16    # "fcd":Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;
    :catchall_1
    move-exception v3

    :try_start_3
    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v3

    :cond_a
    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 310
    sget-boolean v3, Lcom/google/android/music/download/cache/StorageMigrationWorker;->LOGV:Z

    if-eqz v3, :cond_b

    .line 311
    const-string v3, "StorageMigrationWkr"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initialization complete: mFileIdsToCopy.size="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mFileIdsToCopy:Ljava/util/LinkedList;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "mTargetLocation="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mTargetLocation:Lcom/google/android/music/download/cache/CacheLocation;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    :cond_b
    new-instance v3, Lcom/google/android/music/utils/LoggableHandler;

    const-class v4, Lcom/google/android/music/download/cache/StorageMigrationWorker;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    .line 315
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v13}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->getDescription(ZLcom/google/android/music/download/cache/CacheLocation;)Ljava/lang/String;

    move-result-object v14

    .line 316
    .local v14, "description":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->setupNotification(Ljava/lang/String;)V

    .line 317
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->moveToNextFileOrEnd()V

    .line 318
    monitor-exit v20
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 285
    :array_0
    .array-data 8
        0xc8
        0x64
    .end array-data
.end method

.method private moveToNextFileOrEnd()V
    .locals 3

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mFileIdsToCopy:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    invoke-direct {p0}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->endMigration()V

    .line 353
    :goto_0
    return-void

    .line 350
    :cond_0
    new-instance v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;-><init>(Lcom/google/android/music/download/cache/StorageMigrationWorker;Lcom/google/android/music/download/cache/StorageMigrationWorker$1;)V

    iput-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mScheduledRunnable:Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;

    .line 351
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    iget-object v1, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mScheduledRunnable:Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;

    const/16 v2, 0x3e8

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsyncDelayed(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method private notifyServiceMigrationDone()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mCallbacks:Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mCallbacks:Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;

    invoke-interface {v0}, Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;->onMigrationFinished()V

    .line 208
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mCallbacks:Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;

    .line 210
    :cond_0
    return-void
.end method

.method private setupNotification(Ljava/lang/String;)V
    .locals 6
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 322
    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v1, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    .line 323
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v1, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mContext:Landroid/content/Context;

    const v2, 0x7f0b02c4

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 325
    invoke-direct {p0}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->updateRemainingString()V

    .line 326
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 327
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    const v1, 0x7f02025c

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 328
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 329
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v1, 0x425

    iget-object v2, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 330
    return-void
.end method

.method private shutdownBackgroundWorker()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 195
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mScheduledRunnable:Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mScheduledRunnable:Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;->mAbort:Z

    .line 197
    iput-object v2, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mScheduledRunnable:Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-virtual {v0}, Lcom/google/android/music/utils/LoggableHandler;->quit()V

    .line 201
    iput-object v2, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    .line 203
    :cond_1
    return-void
.end method

.method private updateRemainingString()V
    .locals 6

    .prologue
    .line 336
    iget-object v1, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mContext:Landroid/content/Context;

    const v2, 0x7f0b02c3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mFileIdsToCopy:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 338
    .local v0, "s":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 339
    return-void
.end method


# virtual methods
.method abortMigration()V
    .locals 2

    .prologue
    .line 160
    sget-boolean v0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->LOGV:Z

    if-eqz v0, :cond_0

    .line 161
    const-string v0, "StorageMigrationWkr"

    const-string v1, "abortMigration"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    :cond_0
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/download/cache/StorageMigrationWorker$3;

    invoke-direct {v1, p0}, Lcom/google/android/music/download/cache/StorageMigrationWorker$3;-><init>(Lcom/google/android/music/download/cache/StorageMigrationWorker;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 175
    return-void
.end method

.method beginMigration(Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;Ljava/util/UUID;)V
    .locals 7
    .param p1, "callbacks"    # Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;
    .param p2, "volumeId"    # Ljava/util/UUID;

    .prologue
    const/4 v6, 0x1

    .line 101
    sget-boolean v3, Lcom/google/android/music/download/cache/StorageMigrationWorker;->LOGV:Z

    if-eqz v3, :cond_0

    .line 102
    const-string v3, "StorageMigrationWkr"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "beginMigration "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "music_enable_storage_migrations"

    invoke-static {v3, v4, v6}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    .line 107
    .local v1, "migrationsEnabled":Z
    if-nez v1, :cond_1

    .line 119
    :goto_0
    return-void

    .line 108
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mContext:Landroid/content/Context;

    const-string v4, "migration.preferences"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 110
    .local v2, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 111
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v3, "storageMigrationInProgress"

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 112
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 113
    sget-object v3, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v4, Lcom/google/android/music/download/cache/StorageMigrationWorker$1;

    invoke-direct {v4, p0, p1, p2}, Lcom/google/android/music/download/cache/StorageMigrationWorker$1;-><init>(Lcom/google/android/music/download/cache/StorageMigrationWorker;Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;Ljava/util/UUID;)V

    invoke-static {v3, v4}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method resumeMigration()Z
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mCallbacks:Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;

    iget-object v1, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mTargetLocationId:Ljava/util/UUID;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->resumeMigration(Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;Ljava/util/UUID;)Z

    move-result v0

    return v0
.end method

.method resumeMigration(Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;Ljava/util/UUID;)Z
    .locals 5
    .param p1, "callbacks"    # Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;
    .param p2, "volumeId"    # Ljava/util/UUID;

    .prologue
    const/4 v2, 0x0

    .line 137
    sget-boolean v3, Lcom/google/android/music/download/cache/StorageMigrationWorker;->LOGV:Z

    if-eqz v3, :cond_0

    .line 138
    const-string v3, "StorageMigrationWkr"

    const-string v4, "resumeMigration"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker;->mContext:Landroid/content/Context;

    const-string v4, "migration.preferences"

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 142
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v3, "storageMigrationInProgress"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 143
    .local v0, "inProgress":Z
    if-eqz v0, :cond_1

    .line 144
    sget-object v2, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v3, Lcom/google/android/music/download/cache/StorageMigrationWorker$2;

    invoke-direct {v3, p0, p1, p2}, Lcom/google/android/music/download/cache/StorageMigrationWorker$2;-><init>(Lcom/google/android/music/download/cache/StorageMigrationWorker;Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;Ljava/util/UUID;)V

    invoke-static {v2, v3}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 150
    const/4 v2, 0x1

    .line 152
    :cond_1
    return v2
.end method
