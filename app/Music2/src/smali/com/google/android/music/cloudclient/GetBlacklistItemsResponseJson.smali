.class public Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;
.super Lcom/google/api/client/json/GenericJson;
.source "GetBlacklistItemsResponseJson.java"

# interfaces
.implements Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity$Feed;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/client/json/GenericJson;",
        "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity$Feed",
        "<",
        "Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;",
        ">;"
    }
.end annotation


# instance fields
.field public mMinLastModifiedIgnore:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "minLastModifiedIgnored"
    .end annotation
.end field

.field public mSyncableBlacklistItems:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;->mSyncableBlacklistItems:Ljava/util/List;

    return-void
.end method

.method public static parseFromJsonInputStream(Ljava/io/InputStream;)Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;
    .locals 1
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34
    const-class v0, Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;

    invoke-static {v0, p0}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonInputStream(Ljava/lang/Class;Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;

    return-object v0
.end method


# virtual methods
.method public getItemList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;->mSyncableBlacklistItems:Ljava/util/List;

    return-object v0
.end method

.method public getNextPageToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forGetBlacklistItems()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method
