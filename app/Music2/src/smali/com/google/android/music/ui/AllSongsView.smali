.class public Lcom/google/android/music/ui/AllSongsView;
.super Landroid/widget/LinearLayout;
.source "AllSongsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mArt:Lcom/google/android/music/AsyncAlbumArtImageView;

.field private mContent:Landroid/view/View;

.field private mSongList:Lcom/google/android/music/medialist/SongList;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/music/ui/AllSongsView;->mContent:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 82
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 53
    iget-object v2, p0, Lcom/google/android/music/ui/AllSongsView;->mSongList:Lcom/google/android/music/medialist/SongList;

    if-nez v2, :cond_1

    .line 54
    const-string v2, "AllSongsView"

    const-string v3, "Song list not yet initialized"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    if-ne p1, p0, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/google/android/music/ui/AllSongsView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 61
    .local v0, "context":Landroid/content/Context;
    iget-object v2, p0, Lcom/google/android/music/ui/AllSongsView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-static {v0, v2}, Lcom/google/android/music/ui/TrackContainerActivity;->buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v1

    .line 62
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 37
    const v0, 0x7f0e00d0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AllSongsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/AllSongsView;->mContent:Landroid/view/View;

    .line 38
    const v0, 0x7f0e00d1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AllSongsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/AllSongsView;->mArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 39
    const v0, 0x7f0e00d3

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AllSongsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/AllSongsView;->mTitle:Landroid/widget/TextView;

    .line 41
    iget-object v0, p0, Lcom/google/android/music/ui/AllSongsView;->mTitle:Landroid/widget/TextView;

    const v1, 0x7f0b0055

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 43
    invoke-virtual {p0, p0}, Lcom/google/android/music/ui/AllSongsView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    return-void
.end method

.method public setSongList(Lcom/google/android/music/medialist/SongList;)V
    .locals 1
    .param p1, "songlist"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/AllSongsView;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 48
    iget-object v0, p0, Lcom/google/android/music/ui/AllSongsView;->mArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setArtForSonglist(Lcom/google/android/music/medialist/SongList;)V

    .line 49
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/music/ui/AllSongsView;->mContent:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 71
    return-void
.end method
