.class Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApiPreL;
.super Ljava/lang/Object;
.source "MediaSessionCompat.java"

# interfaces
.implements Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StubApiPreL"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;


# direct methods
.method private constructor <init>(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApiPreL;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;Lcom/google/android/music/playback/session/MediaSessionCompat$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;
    .param p2, "x1"    # Lcom/google/android/music/playback/session/MediaSessionCompat$1;

    .prologue
    .line 490
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApiPreL;-><init>(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;)V

    return-void
.end method


# virtual methods
.method public onGetPlaybackPosition()J
    .locals 2

    .prologue
    .line 498
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApiPreL;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onGetPlaybackPosition()J

    move-result-wide v0

    return-wide v0
.end method

.method public onMetadataUpdate(ILjava/lang/Object;)V
    .locals 2
    .param p1, "key"    # I
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 503
    const v0, 0x10000001

    if-ne p1, v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApiPreL;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-static {p2}, Lcom/google/android/music/playback/session/RatingCompat;->fromRating(Ljava/lang/Object;)Lcom/google/android/music/playback/session/RatingCompat;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onSetRating(Lcom/google/android/music/playback/session/RatingCompat;)V

    .line 506
    :cond_0
    return-void
.end method

.method public onPlaybackPositionUpdate(J)V
    .locals 1
    .param p1, "newPositionMs"    # J

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApiPreL;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onSeekTo(J)V

    .line 494
    return-void
.end method
