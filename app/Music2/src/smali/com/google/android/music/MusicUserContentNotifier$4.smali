.class Lcom/google/android/music/MusicUserContentNotifier$4;
.super Ljava/lang/Object;
.source "MusicUserContentNotifier.java"

# interfaces
.implements Lcom/google/android/music/art/ArtLoader$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/MusicUserContentNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/MusicUserContentNotifier;


# direct methods
.method constructor <init>(Lcom/google/android/music/MusicUserContentNotifier;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/music/MusicUserContentNotifier$4;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailed(Ljava/lang/String;)V
    .locals 3
    .param p1, "remoteArtUrl"    # Ljava/lang/String;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/music/MusicUserContentNotifier$4;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mWatchedUrls:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/music/MusicUserContentNotifier;->access$600(Lcom/google/android/music/MusicUserContentNotifier;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 105
    const-string v0, "MusicNotifier"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to download art for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    return-void
.end method

.method public onSuccess(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V
    .locals 2
    .param p1, "remoteArtUrl"    # Ljava/lang/String;
    .param p2, "pfd"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/music/MusicUserContentNotifier$4;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mWatchedUrls:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/music/MusicUserContentNotifier;->access$600(Lcom/google/android/music/MusicUserContentNotifier;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/music/MusicUserContentNotifier$4;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/music/MusicUserContentNotifier;->notifyContentChanged(I)V
    invoke-static {v0, v1}, Lcom/google/android/music/MusicUserContentNotifier;->access$000(Lcom/google/android/music/MusicUserContentNotifier;I)V

    .line 100
    :cond_0
    return-void
.end method
