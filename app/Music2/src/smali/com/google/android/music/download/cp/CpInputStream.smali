.class public Lcom/google/android/music/download/cp/CpInputStream;
.super Ljava/lang/Object;
.source "CpInputStream.java"

# interfaces
.implements Lcom/google/android/music/io/ChunkedInputStream;


# instance fields
.field private final mBuffer:[B

.field private final mCipher:Ljavax/crypto/Cipher;

.field private mEOF:Z

.field private final mIvBuffer:[B

.field private mReadShortBlock:Z

.field private final mSecretKeySpec:Ljavax/crypto/spec/SecretKeySpec;

.field private final mSource:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;[B)V
    .locals 2
    .param p1, "source"    # Ljava/io/InputStream;
    .param p2, "secretKey"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/music/download/cp/UnrecognizedDataCpException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x10

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/google/android/music/download/cp/CpInputStream;->mIvBuffer:[B

    .line 30
    array-length v0, p2

    if-eq v0, v1, :cond_0

    .line 31
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "secretKey length must be 16"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/download/cp/CpInputStream;->mSource:Ljava/io/InputStream;

    .line 34
    invoke-static {}, Lcom/google/android/music/download/cp/CpUtils;->getCipher()Ljavax/crypto/Cipher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/cp/CpInputStream;->mCipher:Ljavax/crypto/Cipher;

    .line 35
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v1, "AES"

    invoke-direct {v0, p2, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/download/cp/CpInputStream;->mSecretKeySpec:Ljavax/crypto/spec/SecretKeySpec;

    .line 36
    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/music/download/cp/CpInputStream;->mBuffer:[B

    .line 38
    invoke-static {p1}, Lcom/google/android/music/download/cp/CpUtils;->readAndCheckMagicNumber(Ljava/io/InputStream;)V

    .line 39
    return-void
.end method


# virtual methods
.method public availableBytes()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v1, p0, Lcom/google/android/music/download/cp/CpInputStream;->mSource:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v0

    .line 49
    .local v0, "sourceAvailable":I
    int-to-long v2, v0

    invoke-static {v2, v3}, Lcom/google/android/music/download/cp/CpUtils;->getDecryptedSize(J)J

    move-result-wide v2

    long-to-int v1, v2

    return v1
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/music/download/cp/CpInputStream;->mSource:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 112
    return-void
.end method

.method public getChunkSize()I
    .locals 1

    .prologue
    .line 43
    const/16 v0, 0x3f0

    return v0
.end method

.method public read([BII)I
    .locals 12
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/music/download/cp/CpInputStream;->mEOF:Z

    if-eqz v0, :cond_0

    .line 63
    const/4 v3, -0x1

    .line 105
    :goto_0
    return v3

    .line 65
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/download/cp/CpInputStream;->mReadShortBlock:Z

    if-eqz v0, :cond_1

    .line 66
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already read short block."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_1
    if-lez p3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/music/download/cp/CpInputStream;->getChunkSize()I

    move-result v0

    if-le p3, v0, :cond_3

    .line 69
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "length out of range 0 < length <= chunkSize: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_3
    add-int/lit8 v6, p3, 0x10

    .line 73
    .local v6, "bufferLength":I
    const/4 v7, 0x0

    .line 74
    .local v7, "bufferOffset":I
    :goto_1
    if-lez v6, :cond_6

    .line 75
    iget-object v0, p0, Lcom/google/android/music/download/cp/CpInputStream;->mSource:Ljava/io/InputStream;

    iget-object v1, p0, Lcom/google/android/music/download/cp/CpInputStream;->mBuffer:[B

    invoke-virtual {v0, v1, v7, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v9

    .line 76
    .local v9, "bytesRead":I
    const/4 v0, -0x1

    if-ne v9, v0, :cond_5

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/download/cp/CpInputStream;->mEOF:Z

    .line 79
    if-nez v7, :cond_4

    .line 80
    const/4 v3, -0x1

    goto :goto_0

    .line 82
    :cond_4
    const/16 v0, 0x10

    if-gt v7, v0, :cond_6

    .line 84
    const/4 v3, -0x1

    goto :goto_0

    .line 88
    :cond_5
    add-int/2addr v7, v9

    .line 89
    sub-int/2addr v6, v9

    .line 90
    goto :goto_1

    .line 91
    .end local v9    # "bytesRead":I
    :cond_6
    iget-object v0, p0, Lcom/google/android/music/download/cp/CpInputStream;->mBuffer:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/download/cp/CpInputStream;->mIvBuffer:[B

    const/4 v4, 0x0

    const/16 v5, 0x10

    invoke-static {v0, v1, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 92
    new-instance v11, Ljavax/crypto/spec/IvParameterSpec;

    iget-object v0, p0, Lcom/google/android/music/download/cp/CpInputStream;->mIvBuffer:[B

    invoke-direct {v11, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 93
    .local v11, "ivParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    add-int/lit8 v3, v7, -0x10

    .line 95
    .local v3, "payloadLength":I
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/cp/CpInputStream;->mCipher:Ljavax/crypto/Cipher;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/download/cp/CpInputStream;->mSecretKeySpec:Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v0, v1, v2, v11}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/music/download/cp/CpInputStream;->mCipher:Ljavax/crypto/Cipher;

    iget-object v1, p0, Lcom/google/android/music/download/cp/CpInputStream;->mBuffer:[B

    const/16 v2, 0x10

    move-object v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Ljavax/crypto/Cipher;->doFinal([BII[BI)I

    move-result v8

    .line 98
    .local v8, "bytesDecrypted":I
    if-eq v8, v3, :cond_7

    .line 99
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "wrong size decrypted block."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    .end local v8    # "bytesDecrypted":I
    :catch_0
    move-exception v10

    .line 102
    .local v10, "e":Ljava/lang/Exception;
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Problem with cipher"

    invoke-direct {v0, v1, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 104
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v8    # "bytesDecrypted":I
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/music/download/cp/CpInputStream;->getChunkSize()I

    move-result v0

    if-ge v3, v0, :cond_8

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/music/download/cp/CpInputStream;->mReadShortBlock:Z

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public skipChunks(J)J
    .locals 11
    .param p1, "chunkCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x400

    .line 54
    mul-long v2, p1, v8

    .line 55
    .local v2, "bytesToSkip":J
    iget-object v6, p0, Lcom/google/android/music/download/cp/CpInputStream;->mSource:Ljava/io/InputStream;

    invoke-virtual {v6, v2, v3}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 56
    .local v0, "bytesSkipped":J
    div-long v4, v0, v8

    .line 57
    .local v4, "chunksSkipped":J
    return-wide v4
.end method
