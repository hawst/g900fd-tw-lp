.class public Lcom/google/android/music/ui/BaseTrackListScrollTransition;
.super Landroid/transition/Transition;
.source "BaseTrackListScrollTransition.java"


# static fields
.field private static final SCROLL_Y_PROPERTY:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final TRANSITION_PROPERTIES:[Ljava/lang/String;


# instance fields
.field private mInterpolator:Landroid/view/animation/Interpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android:music:scrollListView:scrollY"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/BaseTrackListScrollTransition;->TRANSITION_PROPERTIES:[Ljava/lang/String;

    .line 36
    new-instance v0, Lcom/google/android/music/ui/BaseTrackListScrollTransition$1;

    const-class v1, Ljava/lang/Integer;

    const-string v2, "scrollY"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/BaseTrackListScrollTransition$1;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/music/ui/BaseTrackListScrollTransition;->SCROLL_Y_PROPERTY:Landroid/util/Property;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 60
    const v0, 0x10c000d

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/BaseTrackListScrollTransition;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 61
    return-void
.end method

.method static synthetic access$000(Landroid/view/View;)I
    .locals 1
    .param p0, "x0"    # Landroid/view/View;

    .prologue
    .line 21
    invoke-static {p0}, Lcom/google/android/music/ui/BaseTrackListScrollTransition;->getScrollY(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private captureValues(Landroid/transition/TransitionValues;)V
    .locals 4
    .param p1, "values"    # Landroid/transition/TransitionValues;

    .prologue
    .line 79
    if-eqz p1, :cond_0

    iget-object v1, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 80
    iget-object v1, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-static {v1}, Lcom/google/android/music/ui/BaseTrackListScrollTransition;->getScrollY(Landroid/view/View;)I

    move-result v0

    .line 83
    .local v0, "y":I
    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    .line 84
    iget-object v1, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "android:music:scrollListView:scrollY"

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    .end local v0    # "y":I
    :cond_0
    return-void
.end method

.method private getScrollAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 9
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 125
    iget-object v4, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v5, "android:music:scrollListView:scrollY"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 126
    .local v2, "startScrollY":I
    iget-object v4, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v5, "android:music:scrollListView:scrollY"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 128
    .local v0, "endScrollY":I
    iget-object v3, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    check-cast v3, Lcom/google/android/music/ui/BaseTrackListView;

    .line 129
    .local v3, "view":Lcom/google/android/music/ui/BaseTrackListView;
    if-nez v3, :cond_0

    .line 130
    const/4 v1, 0x0

    .line 144
    :goto_0
    return-object v1

    .line 139
    :cond_0
    invoke-virtual {v3, v2}, Lcom/google/android/music/ui/BaseTrackListView;->setAbsoluteY(I)V

    .line 141
    sget-object v4, Lcom/google/android/music/ui/BaseTrackListScrollTransition;->SCROLL_Y_PROPERTY:Landroid/util/Property;

    new-instance v5, Landroid/animation/IntEvaluator;

    invoke-direct {v5}, Landroid/animation/IntEvaluator;-><init>()V

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Integer;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v3, v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Landroid/util/Property;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 143
    .local v1, "scrollYAnimator":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/google/android/music/ui/BaseTrackListScrollTransition;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    goto :goto_0
.end method

.method private static getScrollY(Landroid/view/View;)I
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 69
    const/4 v1, -0x1

    .local v1, "y":I
    move-object v0, p0

    .line 70
    check-cast v0, Lcom/google/android/music/ui/BaseTrackListView;

    .line 71
    .local v0, "listView":Lcom/google/android/music/ui/BaseTrackListView;
    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseTrackListView;->getAbsoluteY()I

    move-result v1

    .line 75
    :cond_0
    return v1
.end method


# virtual methods
.method public captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/BaseTrackListScrollTransition;->captureValues(Landroid/transition/TransitionValues;)V

    .line 97
    return-void
.end method

.method public captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/BaseTrackListScrollTransition;->captureValues(Landroid/transition/TransitionValues;)V

    .line 92
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 2
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 102
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    iget-object v0, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:music:scrollListView:scrollY"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:music:scrollListView:scrollY"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 107
    :cond_0
    const/4 v0, 0x0

    .line 110
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/ui/BaseTrackListScrollTransition;->getScrollAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;

    move-result-object v0

    goto :goto_0
.end method

.method public getTransitionProperties()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/google/android/music/ui/BaseTrackListScrollTransition;->TRANSITION_PROPERTIES:[Ljava/lang/String;

    return-object v0
.end method
