.class Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$7;
.super Ljava/lang/Object;
.source "RemoteAsyncMediaPlayer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyIfSongPlayed()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

.field final synthetic val$songId:Lcom/google/android/music/download/ContentIdentifier;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Lcom/google/android/music/download/ContentIdentifier;)V
    .locals 0

    .prologue
    .line 1362
    iput-object p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$7;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    iput-object p2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$7;->val$songId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1366
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$7;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$3200(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$7;->val$songId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-interface {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->markSongPlayed(Lcom/google/android/music/download/ContentIdentifier;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1370
    :goto_0
    return-void

    .line 1367
    :catch_0
    move-exception v0

    .line 1368
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$7;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const-string v2, "Could not mark song as played"

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;Ljava/lang/Exception;)V
    invoke-static {v1, v2, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$3300(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
