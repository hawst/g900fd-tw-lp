.class public Lcom/google/android/music/dial/DialDeviceFactory;
.super Ljava/lang/Object;
.source "DialDeviceFactory.java"


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_COMMUNICATION:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/dial/DialDeviceFactory;->LOGV:Z

    return-void
.end method

.method public static fromRoute(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)Lcom/google/android/music/dial/DialDevice;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "routeInfo"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 34
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 35
    const-string v3, "WebSocket"

    const-string v4, "Invalid RouteInfo - no extras found."

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    :cond_0
    :goto_0
    return-object v1

    .line 38
    :cond_1
    const-string v3, "com.google.android.music.dial.RemoteDevice"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/dial/RemoteDeviceInfo;

    .line 40
    .local v2, "remoteDeviceInfo":Lcom/google/android/music/dial/RemoteDeviceInfo;
    if-nez v2, :cond_2

    .line 41
    const-string v3, "WebSocket"

    const-string v4, "Invalid RouteInfo bundle - RemoteDevice not found."

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 45
    :cond_2
    const/4 v1, 0x0

    .line 47
    .local v1, "dialDevice":Lcom/google/android/music/dial/DialDevice;
    invoke-static {v2}, Lcom/google/android/music/dial/SonosDialDevice;->supportsRemoteDevice(Lcom/google/android/music/dial/RemoteDeviceInfo;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 48
    sget-boolean v3, Lcom/google/android/music/dial/DialDeviceFactory;->LOGV:Z

    if-eqz v3, :cond_3

    .line 49
    const-string v3, "WebSocket"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Created SonosDialDevice for route "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :cond_3
    new-instance v1, Lcom/google/android/music/dial/SonosDialDevice;

    .end local v1    # "dialDevice":Lcom/google/android/music/dial/DialDevice;
    invoke-direct {v1, p0, p1}, Lcom/google/android/music/dial/SonosDialDevice;-><init>(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 54
    .restart local v1    # "dialDevice":Lcom/google/android/music/dial/DialDevice;
    :cond_4
    if-nez v1, :cond_0

    .line 55
    sget-boolean v3, Lcom/google/android/music/dial/DialDeviceFactory;->LOGV:Z

    if-eqz v3, :cond_0

    .line 56
    const-string v3, "WebSocket"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No DialDevice found for route "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
