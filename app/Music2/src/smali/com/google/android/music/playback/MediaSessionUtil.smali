.class public Lcom/google/android/music/playback/MediaSessionUtil;
.super Ljava/lang/Object;
.source "MediaSessionUtil.java"


# static fields
.field private static final QUEUE_PROJECTION:[Ljava/lang/String;

.field private static sInstance:Lcom/google/android/music/playback/session/MediaSessionCompat;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 53
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "SongId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/playback/MediaSessionUtil;->QUEUE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public static getCurrentCustomAction(Landroid/content/Context;Lcom/google/android/music/playback/TrackInfo;IZZ)Ljava/util/List;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "info"    # Lcom/google/android/music/playback/TrackInfo;
    .param p2, "rating"    # I
    .param p3, "isDownloadedOnly"    # Z
    .param p4, "canShuffle"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/music/playback/TrackInfo;",
            "IZZ)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;>;"
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    if-nez p1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-object v0

    .line 203
    :cond_1
    invoke-static {p0, p2}, Lcom/google/android/music/playback/MediaSessionUtil;->getThumbsUpAction(Landroid/content/Context;I)Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    invoke-static {p0, p2}, Lcom/google/android/music/playback/MediaSessionUtil;->getThumbsDownAction(Landroid/content/Context;I)Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    if-nez p3, :cond_2

    .line 206
    invoke-static {p0, p1}, Lcom/google/android/music/playback/MediaSessionUtil;->getStartRadioAction(Landroid/content/Context;Lcom/google/android/music/playback/TrackInfo;)Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    :cond_2
    if-eqz p4, :cond_0

    .line 209
    invoke-static {p0}, Lcom/google/android/music/playback/MediaSessionUtil;->getShuffleAction(Landroid/content/Context;)Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getCurrentPlayQueue(Landroid/content/Context;Z)Ljava/util/List;
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isInCloudQueueMode"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_0

    .line 126
    const/4 v14, 0x0

    .line 180
    :goto_0
    return-object v14

    .line 128
    :cond_0
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v14, "queue":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;>;"
    const/4 v12, 0x0

    .line 132
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v2, Lcom/google/android/music/store/MusicContent$Queue;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v11

    .line 134
    .local v11, "builder":Landroid/net/Uri$Builder;
    const-string v2, "isInCloudQueueMode"

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 138
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v11}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/playback/MediaSessionUtil;->QUEUE_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 140
    if-eqz v12, :cond_5

    .line 141
    const-string v2, "title"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 142
    .local v21, "titleIndex":I
    const-string v2, "artist"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 144
    .local v10, "artistIndex":I
    const-string v2, "album_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 145
    .local v8, "albumIdIndex":I
    const-string v2, "SongId"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 147
    .local v18, "songIdIndex":I
    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 148
    const/4 v9, 0x0

    .line 149
    .local v9, "artUri":Landroid/net/Uri;
    const/4 v2, -0x1

    if-eq v8, v2, :cond_1

    .line 150
    invoke-interface {v12, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v4, 0x0

    const/16 v5, 0x100

    const/16 v6, 0x100

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v9

    .line 153
    :cond_1
    const-wide/16 v16, -0x1

    .line 154
    .local v16, "songId":J
    const/4 v2, -0x1

    move/from16 v0, v18

    if-eq v0, v2, :cond_2

    .line 155
    move/from16 v0, v18

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 158
    :cond_2
    const/16 v20, 0x0

    .line 159
    .local v20, "title":Ljava/lang/String;
    const/4 v2, -0x1

    move/from16 v0, v21

    if-eq v0, v2, :cond_3

    .line 160
    move/from16 v0, v21

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 163
    :cond_3
    const/16 v19, 0x0

    .line 164
    .local v19, "subtitle":Ljava/lang/String;
    const/4 v2, -0x1

    if-eq v10, v2, :cond_4

    .line 165
    invoke-interface {v12, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 168
    :cond_4
    new-instance v2, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;

    invoke-direct {v2}, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;->setSubtitle(Ljava/lang/CharSequence;)Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;->setIconUri(Landroid/net/Uri;)Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;->build()Lcom/google/android/music/playback/session/MediaDescriptionCompat;

    move-result-object v13

    .line 172
    .local v13, "description":Lcom/google/android/music/playback/session/MediaDescriptionCompat;
    new-instance v15, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;

    move-wide/from16 v0, v16

    invoke-direct {v15, v13, v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;-><init>(Lcom/google/android/music/playback/session/MediaDescriptionCompat;J)V

    .line 174
    .local v15, "queueItem":Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;
    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 178
    .end local v8    # "albumIdIndex":I
    .end local v9    # "artUri":Landroid/net/Uri;
    .end local v10    # "artistIndex":I
    .end local v11    # "builder":Landroid/net/Uri$Builder;
    .end local v13    # "description":Lcom/google/android/music/playback/session/MediaDescriptionCompat;
    .end local v15    # "queueItem":Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;
    .end local v16    # "songId":J
    .end local v18    # "songIdIndex":I
    .end local v19    # "subtitle":Ljava/lang/String;
    .end local v20    # "title":Ljava/lang/String;
    .end local v21    # "titleIndex":I
    :catchall_0
    move-exception v2

    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    .restart local v11    # "builder":Landroid/net/Uri$Builder;
    :cond_5
    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method

.method public static getQueueActiveId(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/download/ContentIdentifier;)J
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "id"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    const-wide/16 v2, -0x1

    .line 104
    const/4 v0, 0x0

    .line 105
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez p1, :cond_0

    .line 119
    :goto_0
    return-wide v2

    .line 109
    :cond_0
    :try_start_0
    sget-object v4, Lcom/google/android/music/playback/MediaSessionUtil;->QUEUE_PROJECTION:[Ljava/lang/String;

    invoke-virtual {p1, p0, p2, v4}, Lcom/google/android/music/medialist/SongList;->getSongCursor(Landroid/content/Context;Lcom/google/android/music/download/ContentIdentifier;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 110
    if-eqz v0, :cond_1

    .line 111
    const-string v4, "SongId"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 112
    .local v1, "songIdIndex":I
    const/4 v4, -0x1

    if-eq v1, v4, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 113
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 117
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v1    # "songIdIndex":I
    :cond_1
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2
.end method

.method public static getSessionExtras()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 90
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 93
    .local v0, "extras":Landroid/os/Bundle;
    const-string v1, "com.google.android.gms.car.media.ALWAYS_RESERVE_SPACE_FOR.ACTION_QUEUE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 95
    return-object v0
.end method

.method public static getSessionInstance(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/os/Looper;)Lcom/google/android/music/playback/session/MediaSessionCompat;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/app/PendingIntent;
    .param p3, "looper"    # Landroid/os/Looper;

    .prologue
    .line 67
    const-string v0, "getSessionInstance must be called from main thread"

    invoke-static {p0, v0}, Lcom/google/android/music/utils/MusicUtils;->assertMainProcess(Landroid/content/Context;Ljava/lang/String;)V

    .line 68
    sget-object v0, Lcom/google/android/music/playback/MediaSessionUtil;->sInstance:Lcom/google/android/music/playback/session/MediaSessionCompat;

    if-nez v0, :cond_0

    .line 69
    new-instance v0, Lcom/google/android/music/playback/session/MediaSessionCompat;

    new-instance v5, Landroid/content/ComponentName;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/music/playback/MediaButtonIntentReceiver;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/playback/session/MediaSessionCompat;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/os/Looper;Landroid/content/ComponentName;)V

    sput-object v0, Lcom/google/android/music/playback/MediaSessionUtil;->sInstance:Lcom/google/android/music/playback/session/MediaSessionCompat;

    .line 72
    :cond_0
    sget-object v0, Lcom/google/android/music/playback/MediaSessionUtil;->sInstance:Lcom/google/android/music/playback/session/MediaSessionCompat;

    return-object v0
.end method

.method public static getShuffleAction(Landroid/content/Context;)Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 265
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getCurrentShuffleMode()I

    move-result v1

    if-nez v1, :cond_0

    .line 266
    const v0, 0x7f020157

    .line 270
    .local v0, "iconId":I
    :goto_0
    new-instance v1, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;

    const-string v2, "shuffle"

    const v3, 0x7f0b0056

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    invoke-virtual {v1}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;->build()Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;

    move-result-object v1

    return-object v1

    .line 268
    .end local v0    # "iconId":I
    :cond_0
    const v0, 0x7f020155

    .restart local v0    # "iconId":I
    goto :goto_0
.end method

.method public static getStartRadioAction(Landroid/content/Context;Lcom/google/android/music/playback/TrackInfo;)Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "info"    # Lcom/google/android/music/playback/TrackInfo;

    .prologue
    .line 216
    if-nez p1, :cond_0

    .line 217
    const/4 v11, 0x0

    .line 259
    :goto_0
    return-object v11

    .line 219
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/playback/TrackInfo;->getSongId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v8

    .line 220
    .local v8, "trackId":J
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/playback/TrackInfo;->getSongId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    .line 221
    .local v7, "trackIdStr":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/playback/TrackInfo;->getTrackName()Ljava/lang/String;

    move-result-object v10

    .line 223
    .local v10, "trackName":Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 224
    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newUnknownContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v1

    .line 225
    .local v1, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v6, Lcom/google/android/music/medialist/NautilusSingleSongList;

    invoke-direct {v6, v1, v7, v10}, Lcom/google/android/music/medialist/NautilusSingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    .local v6, "songList":Lcom/google/android/music/medialist/SongList;
    :goto_1
    :try_start_0
    invoke-static {p0, v6}, Lcom/google/android/music/utils/MusicUtils;->getSongListRadioMixDescriptor(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/mix/MixDescriptor;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 239
    .local v1, "descriptor":Lcom/google/android/music/mix/MixDescriptor;
    :goto_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 240
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/music/mix/MixDescriptor;->marshall()[B

    move-result-object v4

    .line 242
    .local v4, "mix_descriptor_bytes":[B
    :goto_3
    const-string v11, "mix_description"

    invoke-virtual {v0, v11, v4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 244
    new-instance v5, Ljava/lang/Object;

    invoke-direct {v5}, Ljava/lang/Object;-><init>()V

    .line 246
    .local v5, "refObject":Ljava/lang/Object;
    :try_start_1
    invoke-static {p0, v5}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 247
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0b01d2

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 253
    .local v3, "mixFeedbackMessage":Ljava/lang/String;
    :goto_4
    const-string v11, "media_custom_action_status"

    invoke-virtual {v0, v11, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    new-instance v11, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;

    const-string v12, "start_radio"

    const v13, 0x7f0b023a

    invoke-virtual {p0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const v14, 0x7f02013f

    invoke-direct {v11, v12, v13, v14}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    invoke-virtual {v11, v0}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;->setExtras(Landroid/os/Bundle;)Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;->build()Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v11

    .line 259
    invoke-static {v5}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    .line 227
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "descriptor":Lcom/google/android/music/mix/MixDescriptor;
    .end local v3    # "mixFeedbackMessage":Ljava/lang/String;
    .end local v4    # "mix_descriptor_bytes":[B
    .end local v5    # "refObject":Ljava/lang/Object;
    .end local v6    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_1
    invoke-static {v8, v9, v10}, Lcom/google/android/music/store/ContainerDescriptor;->newSingleSongDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v1

    .line 229
    .local v1, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v6, Lcom/google/android/music/medialist/SingleSongList;

    invoke-direct {v6, v1, v8, v9, v10}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    .restart local v6    # "songList":Lcom/google/android/music/medialist/SongList;
    goto :goto_1

    .line 234
    :catch_0
    move-exception v2

    .line 235
    .local v2, "e":Ljava/lang/RuntimeException;
    :goto_5
    const-string v11, "MediaSessionUtil"

    const-string v12, "Failed getting media descriptor for song"

    invoke-static {v11, v12, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 236
    const/4 v1, 0x0

    .local v1, "descriptor":Lcom/google/android/music/mix/MixDescriptor;
    goto :goto_2

    .line 240
    .end local v2    # "e":Ljava/lang/RuntimeException;
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_2
    const/4 v4, 0x0

    goto :goto_3

    .line 250
    .restart local v4    # "mix_descriptor_bytes":[B
    .restart local v5    # "refObject":Ljava/lang/Object;
    :cond_3
    :try_start_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0b01d4

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    .restart local v3    # "mixFeedbackMessage":Ljava/lang/String;
    goto :goto_4

    .line 259
    .end local v3    # "mixFeedbackMessage":Ljava/lang/String;
    :catchall_0
    move-exception v11

    invoke-static {v5}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v11

    .line 234
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v4    # "mix_descriptor_bytes":[B
    .end local v5    # "refObject":Ljava/lang/Object;
    .local v1, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :catch_1
    move-exception v2

    goto :goto_5
.end method

.method public static getThumbsDownAction(Landroid/content/Context;I)Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "currentRating"    # I

    .prologue
    const/4 v4, 0x1

    .line 295
    invoke-static {p1}, Lcom/google/android/music/RatingSelector;->convertRatingToThumbs(I)I

    move-result v2

    .line 296
    .local v2, "thumbs":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 298
    .local v0, "bundle":Landroid/os/Bundle;
    if-ne v2, v4, :cond_0

    .line 299
    const v1, 0x7f02016a

    .line 300
    .local v1, "iconId":I
    const-string v3, "thumbed_down"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 305
    :goto_0
    new-instance v3, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;

    const-string v4, "thumbs_down"

    const v5, 0x7f0b01f2

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5, v1}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    invoke-virtual {v3, v0}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;->setExtras(Landroid/os/Bundle;)Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;->build()Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;

    move-result-object v3

    return-object v3

    .line 302
    .end local v1    # "iconId":I
    :cond_0
    const v1, 0x7f02016b

    .line 303
    .restart local v1    # "iconId":I
    const-string v3, "thumbed_down"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public static getThumbsUpAction(Landroid/content/Context;I)Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "currentRating"    # I

    .prologue
    .line 277
    invoke-static {p1}, Lcom/google/android/music/RatingSelector;->convertRatingToThumbs(I)I

    move-result v2

    .line 278
    .local v2, "thumbs":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 280
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    .line 281
    const v1, 0x7f02016c

    .line 282
    .local v1, "iconId":I
    const-string v3, "thumbed_up"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 287
    :goto_0
    new-instance v3, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;

    const-string v4, "thumbs_up"

    const v5, 0x7f0b01f1

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5, v1}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    invoke-virtual {v3, v0}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;->setExtras(Landroid/os/Bundle;)Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction$Builder;->build()Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;

    move-result-object v3

    return-object v3

    .line 284
    .end local v1    # "iconId":I
    :cond_0
    const v1, 0x7f02016d

    .line 285
    .restart local v1    # "iconId":I
    const-string v3, "thumbed_up"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public static releaseSessionInstance(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    const-string v0, "getSessionInstance must be called from main thread"

    invoke-static {p0, v0}, Lcom/google/android/music/utils/MusicUtils;->assertMainProcess(Landroid/content/Context;Ljava/lang/String;)V

    .line 80
    sget-object v0, Lcom/google/android/music/playback/MediaSessionUtil;->sInstance:Lcom/google/android/music/playback/session/MediaSessionCompat;

    if-eqz v0, :cond_0

    .line 81
    sget-object v0, Lcom/google/android/music/playback/MediaSessionUtil;->sInstance:Lcom/google/android/music/playback/session/MediaSessionCompat;

    invoke-virtual {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat;->release()V

    .line 83
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/music/playback/MediaSessionUtil;->sInstance:Lcom/google/android/music/playback/session/MediaSessionCompat;

    .line 84
    return-void
.end method
