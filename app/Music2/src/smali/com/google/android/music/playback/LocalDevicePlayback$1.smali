.class Lcom/google/android/music/playback/LocalDevicePlayback$1;
.super Landroid/support/v7/media/MediaRouter$Callback;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$1;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0}, Landroid/support/v7/media/MediaRouter$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public onRouteVolumeChanged(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 8
    .param p1, "router"    # Landroid/support/v7/media/MediaRouter;
    .param p2, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 319
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$1;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 320
    const-string v4, "LocalDevicePlayback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " onRouteVolumeChanged: route="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :cond_0
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$1;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$100(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 323
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getVolume()I

    move-result v4

    int-to-double v4, v4

    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getVolumeMax()I

    move-result v6

    int-to-double v6, v6

    div-double v2, v4, v6

    .line 324
    .local v2, "volumeRatio":D
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$1;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$200(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    move-result-object v4

    invoke-interface {v4, v2, v3}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->setVolume(D)V

    .line 338
    .end local v2    # "volumeRatio":D
    :goto_0
    return-void

    .line 326
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$1;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMusicCastMediaRouterCallback:Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$300(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;

    move-result-object v0

    .line 327
    .local v0, "callback":Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;
    if-eqz v0, :cond_2

    .line 329
    :try_start_0
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getVolume()I

    move-result v4

    int-to-double v4, v4

    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getVolumeMax()I

    move-result v6

    int-to-double v6, v6

    div-double v2, v4, v6

    .line 330
    .restart local v2    # "volumeRatio":D
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4, v2, v3}, Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;->onRouteVolumeChanged(Ljava/lang/String;D)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 331
    .end local v2    # "volumeRatio":D
    :catch_0
    move-exception v1

    .line 332
    .local v1, "e":Landroid/os/RemoteException;
    const-string v4, "LocalDevicePlayback"

    const-string v5, "Remote call onRouteVolumeChanged failed"

    invoke-static {v4, v5, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 335
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    const-string v4, "LocalDevicePlayback"

    const-string v5, "No IMusicCastMediaRouterCallback is registered"

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
