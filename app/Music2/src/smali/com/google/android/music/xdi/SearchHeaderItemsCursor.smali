.class Lcom/google/android/music/xdi/SearchHeaderItemsCursor;
.super Lcom/google/android/music/xdi/XdiCursor;
.source "SearchHeaderItemsCursor.java"


# static fields
.field private static final PROJECTION_SEARCH:[Ljava/lang/String;


# instance fields
.field private final mArtistItemHeight:I

.field private final mArtistItemWidth:I

.field private final mContext:Landroid/content/Context;

.field private final mHeaderId:J

.field private final mImageHeight:I

.field private final mImageWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "searchType"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ListType"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "searchName"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Album"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "AlbumArtist"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "AlbumArtistId"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Artist"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "itemCount"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "albumCount"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "year"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "AlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "searchSortName"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "hasLocal"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "hasRemote"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Nid"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->PROJECTION_SEARCH:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "headerId"    # J

    .prologue
    .line 75
    invoke-static {p1, p3, p4, p5}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->getCursorForQuery(Landroid/content/Context;Ljava/lang/String;J)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/xdi/XdiCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/database/Cursor;)V

    .line 76
    iput-object p1, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mContext:Landroid/content/Context;

    .line 77
    iput-wide p4, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mHeaderId:J

    .line 78
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 79
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mImageWidth:I

    .line 80
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mImageHeight:I

    .line 81
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistItemWidthDp(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mArtistItemWidth:I

    .line 82
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistItemHeightDp(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mArtistItemHeight:I

    .line 83
    return-void
.end method

.method private extractDataForAlbum([Ljava/lang/Object;)V
    .locals 13
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v5

    .line 177
    .local v5, "cursor":Landroid/database/Cursor;
    const/4 v10, 0x1

    invoke-interface {v5, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 179
    .local v7, "itemType":I
    const/16 v10, 0x10

    invoke-interface {v5, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 183
    .local v4, "artworkUrl":Ljava/lang/String;
    const/4 v9, 0x0

    .line 184
    .local v9, "trackCount":I
    const/4 v10, 0x7

    if-ne v7, v10, :cond_1

    .line 185
    const/16 v10, 0x12

    invoke-interface {v5, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 186
    .local v1, "albumId":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 187
    iget-object v10, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultAlbumArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 197
    :cond_0
    :goto_0
    const/4 v10, 0x3

    invoke-interface {v5, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 198
    .local v8, "name":Ljava/lang/String;
    const/4 v10, 0x5

    invoke-interface {v5, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "albumArtist":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumDetailsIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 201
    .local v6, "intent":Landroid/content/Intent;
    iget-object v10, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v11, "_id"

    invoke-interface {v5}, Landroid/database/Cursor;->getPosition()I

    move-result v12

    add-int/lit8 v12, v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, p1, v11, v12}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 202
    iget-object v10, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v11, "parent_id"

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, p1, v11, v12}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 204
    iget-object v10, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v11, "display_name"

    invoke-virtual {v10, p1, v11, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 205
    iget-object v10, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v11, "display_description"

    invoke-virtual {v10, p1, v11, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 206
    iget-object v10, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v11, "image_uri"

    invoke-virtual {v10, p1, v11, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 207
    iget-object v10, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v11, "width"

    const/4 v12, 0x0

    invoke-virtual {v10, p1, v11, v12}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 208
    iget-object v10, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v11, "height"

    const/4 v12, 0x0

    invoke-virtual {v10, p1, v11, v12}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 209
    iget-object v10, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v11, "intent_uri"

    const/4 v12, 0x1

    invoke-virtual {v6, v12}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, p1, v11, v12}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 211
    iget-object v10, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v11, "music_album"

    invoke-virtual {v10, p1, v11, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 212
    iget-object v10, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v11, "music_albumArtist"

    invoke-virtual {v10, p1, v11, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 213
    iget-object v10, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v11, "music_trackCount"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, p1, v11, v12}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 215
    return-void

    .line 190
    .end local v0    # "albumArtist":Ljava/lang/String;
    .end local v1    # "albumId":Ljava/lang/String;
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v8    # "name":Ljava/lang/String;
    :cond_1
    const/16 v10, 0xc

    invoke-interface {v5, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 191
    .local v2, "albumIdLong":J
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 192
    .restart local v1    # "albumId":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 193
    const/4 v10, 0x1

    iget v11, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mImageWidth:I

    iget v12, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mImageHeight:I

    invoke-static {v2, v3, v10, v11, v12}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0
.end method

.method private extractDataForArtist([Ljava/lang/Object;)V
    .locals 17
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 218
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v8

    .line 219
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v13, 0x1

    invoke-interface {v8, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 220
    .local v10, "itemType":I
    const/16 v13, 0x10

    invoke-interface {v8, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 225
    .local v5, "artworkUrl":Ljava/lang/String;
    const/4 v2, 0x0

    .line 226
    .local v2, "albumCount":I
    const/4 v12, 0x0

    .line 227
    .local v12, "trackCount":I
    const/4 v13, 0x6

    if-ne v10, v13, :cond_1

    .line 228
    const/16 v13, 0x13

    invoke-interface {v8, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 229
    .local v4, "artistId":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 230
    invoke-static {v4}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistArtUrlUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    invoke-virtual {v13}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 239
    :cond_0
    :goto_0
    const/4 v13, 0x3

    invoke-interface {v8, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 240
    .local v11, "name":Ljava/lang/String;
    new-instance v9, Landroid/content/Intent;

    const-string v13, "com.google.android.xdi.action.DETAIL"

    sget-object v14, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "details/artists/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    invoke-direct {v9, v13, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 244
    .local v9, "intent":Landroid/content/Intent;
    const/4 v3, 0x0

    .line 248
    .local v3, "albumCountText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "_id"

    invoke-interface {v8}, Landroid/database/Cursor;->getPosition()I

    move-result v15

    add-int/lit8 v15, v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 249
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "parent_id"

    const/4 v15, 0x3

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 251
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "display_name"

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 252
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "display_description"

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 253
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "image_uri"

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 254
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "width"

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mArtistItemWidth:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 255
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "height"

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mArtistItemHeight:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 256
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "intent_uri"

    const/4 v15, 0x1

    invoke-virtual {v9, v15}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 258
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "music_album"

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 259
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "music_albumArtist"

    const/4 v15, 0x5

    invoke-interface {v8, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 261
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "music_albumCount"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 262
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "music_trackCount"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 263
    return-void

    .line 233
    .end local v3    # "albumCountText":Ljava/lang/String;
    .end local v4    # "artistId":Ljava/lang/String;
    .end local v9    # "intent":Landroid/content/Intent;
    .end local v11    # "name":Ljava/lang/String;
    :cond_1
    const/4 v13, 0x6

    invoke-interface {v8, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 234
    .local v6, "artistIdLong":J
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 235
    .restart local v4    # "artistId":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 236
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0
.end method

.method private extractDataForBestMatch([Ljava/lang/Object;)Z
    .locals 6
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v5, 0x5

    const/4 v2, 0x1

    .line 139
    invoke-virtual {p0}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 140
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 141
    .local v1, "itemType":I
    packed-switch v1, :pswitch_data_0

    .line 169
    :pswitch_0
    const-string v2, "MusicXdi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported best match type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const/4 v2, 0x0

    .line 172
    :goto_0
    return v2

    .line 144
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->extractDataForAlbum([Ljava/lang/Object;)V

    .line 145
    iget-object v3, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "parent_id"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 147
    iget-object v3, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "music_bestMatchType"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 153
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->extractDataForArtist([Ljava/lang/Object;)V

    .line 154
    iget-object v3, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "parent_id"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 156
    iget-object v3, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "music_bestMatchType"

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 161
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->extractDataForTrack([Ljava/lang/Object;)V

    .line 162
    iget-object v3, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "parent_id"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 164
    iget-object v3, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "music_bestMatchType"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 141
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private extractDataForPlaylist([Ljava/lang/Object;)V
    .locals 12
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v11, 0x0

    const/4 v9, -0x1

    .line 315
    invoke-virtual {p0}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 316
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 317
    .local v4, "playlistId":J
    const/4 v8, 0x3

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 320
    .local v3, "name":Ljava/lang/String;
    const/4 v6, 0x0

    .line 321
    .local v6, "songCount":I
    const/16 v8, 0x10

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 322
    .local v0, "artworkUrl":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 323
    invoke-static {v4, v5, v9, v9}, Lcom/google/android/music/store/MusicContent$PlaylistArt;->getPlaylistArtUri(JII)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 327
    :cond_0
    invoke-static {v4, v5}, Lcom/google/android/music/xdi/XdiUtils;->getPlaylistDetailsIntent(J)Landroid/content/Intent;

    move-result-object v2

    .line 329
    .local v2, "intent":Landroid/content/Intent;
    const/4 v7, 0x0

    .line 334
    .local v7, "songCountDesc":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "_id"

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, p1, v9, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 335
    iget-object v8, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "parent_id"

    const/4 v10, 0x4

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, p1, v9, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 337
    iget-object v8, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "display_name"

    invoke-virtual {v8, p1, v9, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 338
    iget-object v8, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "display_description"

    invoke-virtual {v8, p1, v9, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 339
    iget-object v8, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "image_uri"

    invoke-virtual {v8, p1, v9, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 340
    iget-object v8, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "width"

    invoke-virtual {v8, p1, v9, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 341
    iget-object v8, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "height"

    invoke-virtual {v8, p1, v9, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 342
    iget-object v8, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "intent_uri"

    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, p1, v9, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 345
    iget-object v8, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "music_trackCount"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, p1, v9, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 346
    return-void
.end method

.method private extractDataForTrack([Ljava/lang/Object;)V
    .locals 18
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 266
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v5

    .line 267
    .local v5, "cursor":Landroid/database/Cursor;
    const/4 v11, 0x1

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 269
    .local v7, "itemType":I
    const/16 v11, 0x10

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 270
    .local v4, "artworkUrl":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 271
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 274
    :cond_0
    const/4 v11, 0x3

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 275
    .local v8, "name":Ljava/lang/String;
    const/4 v11, 0x4

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 276
    .local v3, "albumName":Ljava/lang/String;
    const/4 v11, 0x5

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 277
    .local v2, "albumArtist":Ljava/lang/String;
    const/4 v11, 0x7

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 279
    .local v9, "trackArtist":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v6

    .line 282
    .local v6, "intent":Landroid/content/Intent;
    const/16 v11, 0x8

    if-ne v7, v11, :cond_1

    .line 283
    const/16 v11, 0x11

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 284
    .local v10, "trackId":Ljava/lang/String;
    const-string v11, "container"

    const/4 v14, 0x7

    invoke-virtual {v6, v11, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 286
    const-string v11, "id_string"

    invoke-virtual {v6, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 293
    :goto_0
    const-string v11, "name"

    invoke-virtual {v6, v11, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 294
    const-string v11, "art_uri"

    invoke-virtual {v6, v11, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 296
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "_id"

    invoke-interface {v5}, Landroid/database/Cursor;->getPosition()I

    move-result v15

    add-int/lit8 v15, v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 297
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "parent_id"

    const/4 v15, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 299
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "display_name"

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v14, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 300
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "display_description"

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v14, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 301
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "image_uri"

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v14, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 302
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "width"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 303
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "height"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 304
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "intent_uri"

    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 306
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "music_album"

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v14, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 307
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "music_trackname"

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v14, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 308
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "music_albumArtist"

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v14, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 309
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "music_trackArtist"

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v14, v9}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 310
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "music_duration"

    const/16 v15, 0xb

    invoke-interface {v5, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 312
    return-void

    .line 288
    .end local v10    # "trackId":Ljava/lang/String;
    :cond_1
    const/4 v11, 0x0

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 289
    .local v12, "trackIdLong":J
    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    .line 290
    .restart local v10    # "trackId":Ljava/lang/String;
    const-string v11, "container"

    const/4 v14, 0x6

    invoke-virtual {v6, v11, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 291
    const-string v11, "id"

    invoke-virtual {v6, v11, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto/16 :goto_0
.end method

.method private static getCursorForQuery(Landroid/content/Context;Ljava/lang/String;J)Landroid/database/Cursor;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "headerId"    # J

    .prologue
    const/4 v3, 0x0

    .line 86
    const/4 v6, 0x0

    .line 88
    .local v6, "type":Ljava/lang/String;
    long-to-int v0, p2

    packed-switch v0, :pswitch_data_0

    .line 105
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected header id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :goto_0
    return-object v3

    .line 90
    :pswitch_0
    const-string v6, "album"

    .line 108
    :goto_1
    invoke-static {p1, v6}, Lcom/google/android/music/store/MusicContent$Search;->getSearchUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->PROJECTION_SEARCH:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_0

    .line 93
    :pswitch_1
    const-string v6, "artist"

    .line 94
    goto :goto_1

    .line 96
    :pswitch_2
    const-string v6, "track"

    .line 97
    goto :goto_1

    .line 99
    :pswitch_3
    const-string v6, "playlist"

    .line 100
    goto :goto_1

    .line 102
    :pswitch_4
    const-string v6, "bestmatch"

    .line 103
    goto :goto_1

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method protected extractDataForCurrentRow([Ljava/lang/Object;)Z
    .locals 4
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 115
    iget-wide v0, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mHeaderId:J

    long-to-int v0, v0

    packed-switch v0, :pswitch_data_0

    .line 131
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected header id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->mHeaderId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const/4 v0, 0x0

    .line 135
    :goto_0
    return v0

    .line 117
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->extractDataForAlbum([Ljava/lang/Object;)V

    .line 135
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 120
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->extractDataForArtist([Ljava/lang/Object;)V

    goto :goto_1

    .line 123
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->extractDataForTrack([Ljava/lang/Object;)V

    goto :goto_1

    .line 126
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->extractDataForPlaylist([Ljava/lang/Object;)V

    goto :goto_1

    .line 129
    :pswitch_4
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;->extractDataForBestMatch([Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 115
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
