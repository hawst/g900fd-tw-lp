.class public final Lcom/google/android/music/download/cache/BaseCacheService$StorageLowBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BaseCacheService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/cache/BaseCacheService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StorageLowBroadcastReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 115
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isStorageLowHandlingEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 116
    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117
    # getter for: Lcom/google/android/music/download/cache/BaseCacheService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/cache/BaseCacheService;->access$700()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    const-string v1, "CacheService"

    const-string v2, "Received ACTION_DEVICE_STORAGE_LOW broadcast, need to handle low storage"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/download/cache/TrackCacheService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 125
    .local v0, "serviceIntent":Landroid/content/Intent;
    const-string v1, "com.google.android.music.download.cache.CacheService.HANDLE_LOW_STORAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 129
    .end local v0    # "serviceIntent":Landroid/content/Intent;
    :cond_1
    return-void
.end method
