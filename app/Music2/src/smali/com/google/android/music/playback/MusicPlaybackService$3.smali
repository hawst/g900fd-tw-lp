.class Lcom/google/android/music/playback/MusicPlaybackService$3;
.super Landroid/content/BroadcastReceiver;
.source "MusicPlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/MusicPlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/MusicPlaybackService;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/google/android/music/playback/MusicPlaybackService$3;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 232
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 233
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$3;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v1}, Lcom/google/android/music/playback/MusicPlaybackService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    .line 235
    .local v0, "landscape":Z
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$3;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mIsLandscape:Z
    invoke-static {v1}, Lcom/google/android/music/playback/MusicPlaybackService;->access$300(Lcom/google/android/music/playback/MusicPlaybackService;)Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 236
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$3;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # setter for: Lcom/google/android/music/playback/MusicPlaybackService;->mIsLandscape:Z
    invoke-static {v1, v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$302(Lcom/google/android/music/playback/MusicPlaybackService;Z)Z

    .line 237
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/playback/MusicPlaybackService$3$1;

    invoke-direct {v2, p0}, Lcom/google/android/music/playback/MusicPlaybackService$3$1;-><init>(Lcom/google/android/music/playback/MusicPlaybackService$3;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 244
    .end local v0    # "landscape":Z
    :cond_0
    return-void

    .line 233
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
