.class public Lcom/google/android/music/medialist/SelectedSongList;
.super Lcom/google/android/music/medialist/SongList;
.source "SelectedSongList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/medialist/SelectedSongList$SelectedSongListAlbumIdIterator;
    }
.end annotation


# static fields
.field private static final SONGLISTCOLS_ALBUM_ID_INDEX:I

.field private static final sSongListCols:[Ljava/lang/String;


# instance fields
.field private final mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field protected final mMusicIds:[J

.field protected final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 43
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "album_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/medialist/SelectedSongList;->sSongListCols:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/store/ContainerDescriptor;[J)V
    .locals 3
    .param p1, "descriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p2, "musicIds"    # [J

    .prologue
    .line 87
    const/4 v0, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/medialist/SongList;-><init>(IZZ)V

    .line 88
    iput-object p2, p0, Lcom/google/android/music/medialist/SelectedSongList;->mMusicIds:[J

    .line 89
    iput-object p1, p0, Lcom/google/android/music/medialist/SelectedSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 90
    invoke-virtual {p1}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/SelectedSongList;->mName:Ljava/lang/String;

    .line 91
    return-void
.end method

.method static synthetic access$000()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/music/medialist/SelectedSongList;->sSongListCols:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public appendToPlaylist(Landroid/content/Context;J)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "playlistId"    # J

    .prologue
    .line 142
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/medialist/SelectedSongList;->mMusicIds:[J

    invoke-static {v0, p2, p3, v1}, Lcom/google/android/music/store/MusicContent$Playlists;->appendItemsToPlayList(Landroid/content/ContentResolver;J[J)I

    move-result v0

    return v0
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 38
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/medialist/SelectedSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/medialist/SelectedSongList;->mMusicIds:[J

    invoke-static {v2}, Lcom/google/android/music/medialist/SelectedSongList;->encodeArg([J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/medialist/SelectedSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    return-object v0
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/music/medialist/SelectedSongList;->mMusicIds:[J

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$XAudio;->getSelectedAudioUri([J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getImage(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "allowAlias"    # Z

    .prologue
    .line 106
    const/4 v2, 0x2

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/music/medialist/SelectedSongList;->mMusicIds:[J

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/medialist/SelectedSongList;->mMusicIds:[J

    const/4 v1, 0x0

    aget-wide v4, v0, v1

    :goto_0
    iget-object v8, p0, Lcom/google/android/music/medialist/SelectedSongList;->mName:Ljava/lang/String;

    const/4 v9, 0x0

    new-instance v10, Lcom/google/android/music/medialist/SelectedSongList$1;

    invoke-direct {v10, p0, p1}, Lcom/google/android/music/medialist/SelectedSongList$1;-><init>(Lcom/google/android/music/medialist/SelectedSongList;Landroid/content/Context;)V

    move-object v1, p1

    move v6, p2

    move/from16 v7, p3

    move-object/from16 v11, p4

    move/from16 v12, p5

    invoke-static/range {v1 .. v12}, Lcom/google/android/music/utils/AlbumArtUtils;->getFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_0
    const-wide/16 v4, 0x1

    goto :goto_0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/music/medialist/SelectedSongList;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondaryName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    const-string v0, ""

    return-object v0
.end method

.method public getValidSortOrders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    const/4 v0, 0x0

    return-object v0
.end method

.method public storeDefaultSortOrder(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 126
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "not supported for SelectedSongList"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public supportsAppendToPlaylist()Z
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x1

    return v0
.end method
