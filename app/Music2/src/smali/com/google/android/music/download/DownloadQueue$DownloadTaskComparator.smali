.class final Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;
.super Ljava/lang/Object;
.source "DownloadQueue.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/DownloadQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DownloadTaskComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<TTaskType;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/DownloadQueue;


# direct methods
.method private constructor <init>(Lcom/google/android/music/download/DownloadQueue;)V
    .locals 0

    .prologue
    .line 51
    .local p0, "this":Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;, "Lcom/google/android/music/download/DownloadQueue<TTaskType;>.DownloadTaskComparator;"
    iput-object p1, p0, Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;->this$0:Lcom/google/android/music/download/DownloadQueue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/download/DownloadQueue;Lcom/google/android/music/download/DownloadQueue$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/download/DownloadQueue;
    .param p2, "x1"    # Lcom/google/android/music/download/DownloadQueue$1;

    .prologue
    .line 51
    .local p0, "this":Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;, "Lcom/google/android/music/download/DownloadQueue<TTaskType;>.DownloadTaskComparator;"
    invoke-direct {p0, p1}, Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;-><init>(Lcom/google/android/music/download/DownloadQueue;)V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/music/download/DownloadTask;Lcom/google/android/music/download/DownloadTask;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTaskType;TTaskType;)I"
        }
    .end annotation

    .prologue
    .line 54
    .local p0, "this":Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;, "Lcom/google/android/music/download/DownloadQueue<TTaskType;>.DownloadTaskComparator;"
    .local p1, "lhs":Lcom/google/android/music/download/DownloadTask;, "TTaskType;"
    .local p2, "rhs":Lcom/google/android/music/download/DownloadTask;, "TTaskType;"
    invoke-interface {p1}, Lcom/google/android/music/download/DownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/music/download/DownloadRequest;->getPriority()I

    move-result v3

    invoke-interface {p2}, Lcom/google/android/music/download/DownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/music/download/DownloadRequest;->getPriority()I

    move-result v4

    sub-int v1, v3, v4

    .line 55
    .local v1, "priorityDiff":I
    if-eqz v1, :cond_0

    .line 70
    .end local v1    # "priorityDiff":I
    :goto_0
    return v1

    .line 59
    .restart local v1    # "priorityDiff":I
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;->this$0:Lcom/google/android/music/download/DownloadQueue;

    # getter for: Lcom/google/android/music/download/DownloadQueue;->mTaskSubmissionOrderMap:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/android/music/download/DownloadQueue;->access$000(Lcom/google/android/music/download/DownloadQueue;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 61
    .local v0, "lhsSubmissionOrder":Ljava/lang/Long;
    if-nez v0, :cond_1

    .line 62
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Failed to find submission order for the lhs task"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 65
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;->this$0:Lcom/google/android/music/download/DownloadQueue;

    # getter for: Lcom/google/android/music/download/DownloadQueue;->mTaskSubmissionOrderMap:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/android/music/download/DownloadQueue;->access$000(Lcom/google/android/music/download/DownloadQueue;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 66
    .local v2, "rhsSubmissionOrder":Ljava/lang/Long;
    if-nez v2, :cond_2

    .line 67
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Failed to find submission order for the rhs task"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 70
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    long-to-int v1, v4

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 51
    .local p0, "this":Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;, "Lcom/google/android/music/download/DownloadQueue<TTaskType;>.DownloadTaskComparator;"
    check-cast p1, Lcom/google/android/music/download/DownloadTask;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/music/download/DownloadTask;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;->compare(Lcom/google/android/music/download/DownloadTask;Lcom/google/android/music/download/DownloadTask;)I

    move-result v0

    return v0
.end method
