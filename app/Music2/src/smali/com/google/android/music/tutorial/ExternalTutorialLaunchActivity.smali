.class public Lcom/google/android/music/tutorial/ExternalTutorialLaunchActivity;
.super Landroid/app/Activity;
.source "ExternalTutorialLaunchActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 79
    const/16 v1, 0x2a

    if-eq p1, v1, :cond_0

    .line 80
    const-string v1, "ExternalTutorial"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid request code "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :goto_0
    return-void

    .line 83
    :cond_0
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 85
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 86
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/google/android/music/tutorial/ExternalTutorialLaunchActivity;->setResult(I)V

    .line 90
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/ExternalTutorialLaunchActivity;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    .line 88
    :cond_1
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p0, v1}, Lcom/google/android/music/tutorial/ExternalTutorialLaunchActivity;->setResult(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 92
    :catchall_0
    move-exception v1

    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 41
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/ExternalTutorialLaunchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/tutorial/ExternalTutorialLaunchActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/tutorial/TutorialUtils;->isPackageGoogleSigned(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 44
    const-string v3, "ExternalTutorial"

    const-string v4, "Can only be called by a google signed package."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/ExternalTutorialLaunchActivity;->finish()V

    .line 74
    :goto_0
    return-void

    .line 49
    :cond_0
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 52
    .local v1, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 53
    const-string v3, "ExternalTutorial"

    const-string v4, "User already has an account set up."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/google/android/music/tutorial/ExternalTutorialLaunchActivity;->setResult(I)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/ExternalTutorialLaunchActivity;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    .line 59
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/ExternalTutorialLaunchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 60
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "source"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 61
    .local v2, "source":Ljava/lang/String;
    if-eqz v2, :cond_2

    const-string v3, "GEARHEAD"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 64
    const/16 v3, 0x2a

    invoke-static {p0, v3}, Lcom/google/android/music/tutorial/TutorialUtils;->externalLaunchTutorial(Landroid/app/Activity;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 65
    const-string v3, "ExternalTutorial"

    const-string v4, "Launched tutorial"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 72
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    .line 69
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {p0, v3}, Lcom/google/android/music/tutorial/ExternalTutorialLaunchActivity;->setResult(I)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/ExternalTutorialLaunchActivity;->finish()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 72
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "source":Ljava/lang/String;
    :catchall_0
    move-exception v3

    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v3
.end method
