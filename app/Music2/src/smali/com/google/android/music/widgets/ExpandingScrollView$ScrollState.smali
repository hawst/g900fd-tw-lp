.class public final enum Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;
.super Ljava/lang/Enum;
.source "ExpandingScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/widgets/ExpandingScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScrollState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

.field public static final enum HORIZONTAL_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

.field public static final enum NO_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

.field public static final enum START_TOUCH:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

.field public static final enum VERTICAL_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 101
    new-instance v0, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    const-string v1, "NO_SCROLL"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->NO_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    .line 102
    new-instance v0, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    const-string v1, "START_TOUCH"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->START_TOUCH:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    .line 103
    new-instance v0, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    const-string v1, "HORIZONTAL_SCROLL"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->HORIZONTAL_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    .line 104
    new-instance v0, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    const-string v1, "VERTICAL_SCROLL"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->VERTICAL_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    .line 100
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->NO_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->START_TOUCH:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->HORIZONTAL_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->VERTICAL_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->$VALUES:[Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 100
    const-class v0, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->$VALUES:[Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    invoke-virtual {v0}, [Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    return-object v0
.end method
