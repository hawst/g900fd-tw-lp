.class final Lcom/google/android/music/utils/MusicUtils$4;
.super Ljava/lang/Object;
.source "MusicUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/utils/MusicUtils;->movePlaylistItem(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/medialist/PlaylistSongList;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$desiredPreviousItemId:J

.field final synthetic val$itemToMoveId:J

.field final synthetic val$playlist:Lcom/google/android/music/medialist/PlaylistSongList;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/music/medialist/PlaylistSongList;JJ)V
    .locals 1

    .prologue
    .line 1609
    iput-object p1, p0, Lcom/google/android/music/utils/MusicUtils$4;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/music/utils/MusicUtils$4;->val$playlist:Lcom/google/android/music/medialist/PlaylistSongList;

    iput-wide p3, p0, Lcom/google/android/music/utils/MusicUtils$4;->val$itemToMoveId:J

    iput-wide p5, p0, Lcom/google/android/music/utils/MusicUtils$4;->val$desiredPreviousItemId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 1611
    iget-object v0, p0, Lcom/google/android/music/utils/MusicUtils$4;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/music/utils/MusicUtils$4;->val$playlist:Lcom/google/android/music/medialist/PlaylistSongList;

    invoke-virtual {v0}, Lcom/google/android/music/medialist/PlaylistSongList;->getId()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/music/utils/MusicUtils$4;->val$itemToMoveId:J

    iget-wide v6, p0, Lcom/google/android/music/utils/MusicUtils$4;->val$desiredPreviousItemId:J

    invoke-static/range {v1 .. v7}, Lcom/google/android/music/store/MusicContent$Playlists;->movePlaylistItem(Landroid/content/ContentResolver;JJJ)V

    .line 1614
    return-void
.end method
