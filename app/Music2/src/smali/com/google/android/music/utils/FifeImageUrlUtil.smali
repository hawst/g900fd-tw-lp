.class public Lcom/google/android/music/utils/FifeImageUrlUtil;
.super Ljava/lang/Object;
.source "FifeImageUrlUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/utils/FifeImageUrlUtil$InvalidUrlException;
    }
.end annotation


# static fields
.field private static final CROPPING_RELATED_URL_OPTIONS:Ljava/util/regex/Pattern;

.field private static final FIFE_HOSTED_IMAGE_URL_RE:Ljava/util/regex/Pattern;

.field private static final JOIN_ON_SLASH:Lcom/google/common/base/Joiner;

.field private static final SIZE_RELATED_URL_OPTIONS:Ljava/util/regex/Pattern;

.field private static final SPLIT_ON_OPTION_DELIMITER:Lcom/google/common/base/Splitter;

.field private static final SPLIT_ON_SLASH:Lcom/google/common/base/Splitter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "[swh]\\d+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/utils/FifeImageUrlUtil;->SIZE_RELATED_URL_OPTIONS:Ljava/util/regex/Pattern;

    .line 42
    const-string v0, "([cnp])\\1?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/utils/FifeImageUrlUtil;->CROPPING_RELATED_URL_OPTIONS:Ljava/util/regex/Pattern;

    .line 50
    const-string v0, "-"

    invoke-static {v0}, Lcom/google/common/base/Splitter;->on(Ljava/lang/String;)Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->omitEmptyStrings()Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/utils/FifeImageUrlUtil;->SPLIT_ON_OPTION_DELIMITER:Lcom/google/common/base/Splitter;

    .line 53
    const/16 v0, 0x2f

    invoke-static {v0}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->omitEmptyStrings()Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/utils/FifeImageUrlUtil;->SPLIT_ON_SLASH:Lcom/google/common/base/Splitter;

    .line 56
    const-string v0, "/"

    invoke-static {v0}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/utils/FifeImageUrlUtil;->JOIN_ON_SLASH:Lcom/google/common/base/Joiner;

    .line 58
    const-string v0, "^((http(s)?):)?\\/\\/((((lh[3-6]\\.((ggpht)|(googleusercontent)|(google)))|(gp[3-6]\\.((ggpht)|(googleusercontent)|(google)))|([1-4]\\.bp\\.blogspot)|(bp[0-3]\\.blogger))\\.com)|(dp[3-6]\\.googleusercontent\\.cn)|((dev|dev2|dev3|qa|qa2|qa3|qa-red|qa-blue|canary)[-.]lighthouse\\.sandbox\\.google\\.com/image)|(www\\.google\\.com\\/visualsearch\\/lh))\\/"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/utils/FifeImageUrlUtil;->FIFE_HOSTED_IMAGE_URL_RE:Ljava/util/regex/Pattern;

    return-void
.end method

.method private static addUnrelatedOptions(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/StringBuilder;)V
    .locals 4
    .param p0, "allOptions"    # Ljava/lang/String;
    .param p1, "relatedOptions"    # Ljava/util/regex/Pattern;
    .param p2, "builder"    # Ljava/lang/StringBuilder;

    .prologue
    .line 255
    sget-object v3, Lcom/google/android/music/utils/FifeImageUrlUtil;->SPLIT_ON_OPTION_DELIMITER:Lcom/google/common/base/Splitter;

    invoke-virtual {v3, p0}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v2

    .line 256
    .local v2, "splitOptions":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 257
    .local v1, "option":Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-nez v3, :cond_0

    .line 258
    const-string v3, "-"

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 261
    .end local v1    # "option":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public static isFifeHostedUrl(Ljava/lang/String;)Z
    .locals 2
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 207
    invoke-static {p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v1, Lcom/google/android/music/utils/FifeImageUrlUtil;->FIFE_HOSTED_IMAGE_URL_RE:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 209
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    return v1
.end method

.method private static isLegacyUrl(Landroid/net/Uri;)Z
    .locals 6
    .param p0, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/utils/FifeImageUrlUtil$InvalidUrlException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 264
    sget-object v2, Lcom/google/android/music/utils/FifeImageUrlUtil;->SPLIT_ON_SLASH:Lcom/google/common/base/Splitter;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 270
    .local v0, "components":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 271
    .local v1, "numParts":I
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v3, :cond_1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v5, "image"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v5, "public"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v5, "proxy"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 275
    :cond_0
    add-int/lit8 v1, v1, -0x1

    .line 278
    :cond_1
    const/4 v2, 0x4

    if-lt v1, v2, :cond_2

    const/4 v2, 0x6

    if-gt v1, v2, :cond_2

    move v2, v3

    .line 281
    :goto_0
    return v2

    .line 280
    :cond_2
    if-ne v1, v3, :cond_3

    move v2, v4

    .line 281
    goto :goto_0

    .line 283
    :cond_3
    new-instance v2, Lcom/google/android/music/utils/FifeImageUrlUtil$InvalidUrlException;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/music/utils/FifeImageUrlUtil$InvalidUrlException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private static setContentImageUrlOption(Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 8
    .param p0, "option"    # Ljava/lang/String;
    .param p1, "param"    # Ljava/lang/String;
    .param p2, "relatedOptions"    # Ljava/util/regex/Pattern;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    .line 236
    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    invoke-virtual {p3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    .line 238
    .local v5, "uriPath":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 239
    .local v1, "builder":Ljava/lang/StringBuilder;
    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 240
    .local v2, "firstPathDelimiterIdx":I
    if-ltz v2, :cond_0

    .line 241
    const/4 v6, 0x0

    invoke-virtual {v5, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 242
    .local v0, "baseContentUrl":Ljava/lang/String;
    add-int/lit8 v6, v2, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 243
    .local v3, "imageOptions":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    invoke-static {v3, p2, v1}, Lcom/google/android/music/utils/FifeImageUrlUtil;->addUnrelatedOptions(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/StringBuilder;)V

    .line 248
    .end local v0    # "baseContentUrl":Ljava/lang/String;
    .end local v3    # "imageOptions":Ljava/lang/String;
    :goto_0
    invoke-virtual {p3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 249
    .local v4, "uriBuilder":Landroid/net/Uri$Builder;
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 250
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    return-object v6

    .line 246
    .end local v4    # "uriBuilder":Landroid/net/Uri$Builder;
    :cond_0
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static setImageUrlOption(Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1
    .param p0, "option"    # Ljava/lang/String;
    .param p1, "param"    # Ljava/lang/String;
    .param p2, "relatedOptions"    # Ljava/util/regex/Pattern;
    .param p3, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/utils/FifeImageUrlUtil$InvalidUrlException;
        }
    .end annotation

    .prologue
    .line 141
    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    invoke-static {p3}, Lcom/google/android/music/utils/FifeImageUrlUtil;->isLegacyUrl(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/music/utils/FifeImageUrlUtil;->setLegacyImageUrlOption(Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 145
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/music/utils/FifeImageUrlUtil;->setContentImageUrlOption(Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static setImageUrlSize(ILandroid/net/Uri;)Landroid/net/Uri;
    .locals 3
    .param p0, "size"    # I
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/utils/FifeImageUrlUtil$InvalidUrlException;
        }
    .end annotation

    .prologue
    .line 113
    const-string v0, "s"

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/utils/FifeImageUrlUtil;->SIZE_RELATED_URL_OPTIONS:Ljava/util/regex/Pattern;

    invoke-static {v0, v1, v2, p1}, Lcom/google/android/music/utils/FifeImageUrlUtil;->setImageUrlOption(Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static setLegacyImageUrlOption(Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 9
    .param p0, "option"    # Ljava/lang/String;
    .param p1, "param"    # Ljava/lang/String;
    .param p2, "relatedOptions"    # Ljava/util/regex/Pattern;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x4

    .line 161
    invoke-virtual {p3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 162
    .local v3, "path":Ljava/lang/String;
    sget-object v5, Lcom/google/android/music/utils/FifeImageUrlUtil;->SPLIT_ON_SLASH:Lcom/google/common/base/Splitter;

    invoke-virtual {v5, v3}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v5

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 164
    .local v1, "components":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 165
    .local v2, "hasImagePrefix":Z
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, "image"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 166
    invoke-interface {v1, v8}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 167
    const/4 v2, 0x1

    .line 170
    :cond_0
    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 172
    const-string v5, ""

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ne v5, v7, :cond_4

    .line 178
    const-string v5, ""

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    :cond_2
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5, p2, v0}, Lcom/google/android/music/utils/FifeImageUrlUtil;->addUnrelatedOptions(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/StringBuilder;)V

    .line 187
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v7, v5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 190
    if-eqz v2, :cond_3

    .line 191
    const-string v5, "image"

    invoke-interface {v1, v8, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 194
    :cond_3
    invoke-virtual {p3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 195
    .local v4, "uriBuilder":Landroid/net/Uri$Builder;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/utils/FifeImageUrlUtil;->JOIN_ON_SLASH:Lcom/google/common/base/Joiner;

    invoke-virtual {v6, v1}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 196
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    return-object v5

    .line 179
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v4    # "uriBuilder":Landroid/net/Uri$Builder;
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x5

    if-ne v5, v6, :cond_2

    .line 182
    const-string v5, ""

    invoke-interface {v1, v7, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method
