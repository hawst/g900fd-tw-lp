.class public Lcom/google/android/music/store/ExploreContentProviderHelper;
.super Ljava/lang/Object;
.source "ExploreContentProviderHelper.java"


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/ExploreContentProviderHelper;->LOGV:Z

    return-void
.end method

.method public static getGenres(Landroid/content/Context;Lcom/google/android/music/cloudclient/MusicCloud;Ljava/lang/String;)Lcom/google/android/music/cloudclient/MusicGenresResponseJson;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "client"    # Lcom/google/android/music/cloudclient/MusicCloud;
    .param p2, "parentGenreId"    # Ljava/lang/String;

    .prologue
    .line 185
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentCache;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/NautilusContentCache;

    move-result-object v0

    .line 186
    .local v0, "cache":Lcom/google/android/music/store/NautilusContentCache;
    invoke-virtual {v0, p2}, Lcom/google/android/music/store/NautilusContentCache;->getMusicGenresResponse(Ljava/lang/String;)Lcom/google/android/music/cloudclient/MusicGenresResponseJson;

    move-result-object v2

    .line 187
    .local v2, "response":Lcom/google/android/music/cloudclient/MusicGenresResponseJson;
    if-nez v2, :cond_2

    .line 189
    :try_start_0
    invoke-interface {p1, p2}, Lcom/google/android/music/cloudclient/MusicCloud;->getGenres(Ljava/lang/String;)Lcom/google/android/music/cloudclient/MusicGenresResponseJson;

    move-result-object v2

    .line 190
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/google/android/music/cloudclient/MusicGenresResponseJson;->mGenres:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/google/android/music/cloudclient/MusicGenresResponseJson;->mGenres:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v2, Lcom/google/android/music/cloudclient/MusicGenresResponseJson;->mGenres:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/cloudclient/MusicGenreJson;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/MusicGenreJson;->mId:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 193
    :cond_0
    const-string v3, "ExploreContentProviderHelper"

    const-string v4, "Incomplete data"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const/4 v3, 0x0

    .line 205
    :goto_0
    return-object v3

    .line 196
    :cond_1
    invoke-virtual {v0, p2, v2}, Lcom/google/android/music/store/NautilusContentCache;->putMusicGenresResponse(Ljava/lang/String;Lcom/google/android/music/cloudclient/MusicGenresResponseJson;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v3, v2

    .line 198
    goto :goto_0

    .line 199
    :catch_0
    move-exception v1

    .line 200
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v3, "ExploreContentProviderHelper"

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_2
    :goto_1
    move-object v3, v2

    .line 205
    goto :goto_0

    .line 201
    :catch_1
    move-exception v1

    .line 202
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "ExploreContentProviderHelper"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private static getTab(Landroid/content/Context;Lcom/google/android/music/cloudclient/MusicCloud;Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)Lcom/google/android/music/cloudclient/TabJson;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "client"    # Lcom/google/android/music/cloudclient/MusicCloud;
    .param p2, "tabType"    # Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;
    .param p3, "genreId"    # Ljava/lang/String;

    .prologue
    .line 210
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "music_explore_num_entities"

    const/16 v5, 0x19

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 213
    .local v1, "exploreNumEntites":I
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentCache;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/NautilusContentCache;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Lcom/google/android/music/store/NautilusContentCache;->getExploreResponse(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)Lcom/google/android/music/cloudclient/TabJson;

    move-result-object v2

    .line 215
    .local v2, "response":Lcom/google/android/music/cloudclient/TabJson;
    if-nez v2, :cond_0

    .line 217
    :try_start_0
    invoke-interface {p1, p2, v1, p3}, Lcom/google/android/music/cloudclient/MusicCloud;->getTab(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;ILjava/lang/String;)Lcom/google/android/music/cloudclient/TabJson;

    move-result-object v2

    .line 218
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentCache;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/NautilusContentCache;

    move-result-object v3

    invoke-virtual {v3, p2, p3, v2}, Lcom/google/android/music/store/NautilusContentCache;->putExploreResponse(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;Lcom/google/android/music/cloudclient/TabJson;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 226
    :cond_0
    :goto_0
    return-object v2

    .line 220
    :catch_0
    move-exception v0

    .line 221
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v3, "ExploreContentProviderHelper"

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 222
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 223
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "ExploreContentProviderHelper"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static getTabType(I)Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;
    .locals 3
    .param p0, "matchUri"    # I

    .prologue
    .line 168
    sparse-switch p0, :sswitch_data_0

    .line 179
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid Uri type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :sswitch_0
    sget-object v0, Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;->NEW_RELEASES:Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;

    .line 177
    :goto_0
    return-object v0

    .line 174
    :sswitch_1
    sget-object v0, Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;->TOP_CHARTS:Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;

    goto :goto_0

    .line 177
    :sswitch_2
    sget-object v0, Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;->RECOMMENDED:Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;

    goto :goto_0

    .line 168
    nop

    :sswitch_data_0
    .sparse-switch
        0x640 -> :sswitch_2
        0x641 -> :sswitch_2
        0x64a -> :sswitch_1
        0x64b -> :sswitch_1
        0x654 -> :sswitch_0
        0x655 -> :sswitch_0
    .end sparse-switch
.end method

.method private static invalidateCache(Landroid/content/Context;Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "tabType"    # Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;
    .param p2, "genreId"    # Ljava/lang/String;

    .prologue
    .line 230
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentCache;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/NautilusContentCache;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/store/NautilusContentCache;->removeExploreResponse(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method public static query(Landroid/content/Context;Landroid/net/Uri;I[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "matchUri"    # I
    .param p3, "projection"    # [Ljava/lang/String;

    .prologue
    .line 40
    new-instance v2, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 41
    .local v2, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    new-instance v3, Landroid/database/MatrixCursor;

    move-object/from16 v0, p3

    invoke-direct {v3, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 43
    .local v3, "cursor":Landroid/database/MatrixCursor;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    const-string v19, "music_enable_shared_playlists"

    const/16 v20, 0x1

    invoke-static/range {v18 .. v20}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v16

    .line 47
    .local v16, "sharedPlaylistsEnabled":Z
    const/4 v8, 0x0

    .line 48
    .local v8, "genreId":Ljava/lang/String;
    sparse-switch p2, :sswitch_data_0

    .line 164
    .end local v3    # "cursor":Landroid/database/MatrixCursor;
    :cond_0
    :goto_0
    return-object v3

    .line 52
    .restart local v3    # "cursor":Landroid/database/MatrixCursor;
    :sswitch_0
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/ExploreContentProviderHelper;->getTabType(I)Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;

    move-result-object v17

    .line 53
    .local v17, "tabType":Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;
    const-string v18, "genreid"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 54
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v2, v1, v8}, Lcom/google/android/music/store/ExploreContentProviderHelper;->getTab(Landroid/content/Context;Lcom/google/android/music/cloudclient/MusicCloud;Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)Lcom/google/android/music/cloudclient/TabJson;

    move-result-object v15

    .line 55
    .local v15, "response":Lcom/google/android/music/cloudclient/TabJson;
    if-eqz v15, :cond_1

    iget-object v0, v15, Lcom/google/android/music/cloudclient/TabJson;->mGroups:Ljava/util/List;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    iget-object v0, v15, Lcom/google/android/music/cloudclient/TabJson;->mGroups:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-eqz v18, :cond_2

    .line 56
    :cond_1
    const-string v18, "ExploreContentProviderHelper"

    const-string v19, "Incomplete data"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1, v8}, Lcom/google/android/music/store/ExploreContentProviderHelper;->invalidateCache(Landroid/content/Context;Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)V

    .line 58
    const/4 v3, 0x0

    goto :goto_0

    .line 61
    :cond_2
    new-instance v11, Ljava/util/LinkedList;

    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    .line 62
    .local v11, "groups":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;>;"
    iget-object v0, v15, Lcom/google/android/music/cloudclient/TabJson;->mGroups:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;

    .line 63
    .local v10, "group":Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;
    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 66
    .end local v10    # "group":Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;
    :cond_3
    invoke-static/range {p3 .. p3}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 68
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    :cond_4
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_2
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v12, v0, :cond_0

    .line 73
    invoke-interface {v11, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;

    .line 74
    .restart local v10    # "group":Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;
    int-to-long v0, v12

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    iput-wide v0, v10, Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;->mId:J

    .line 75
    move-object/from16 v0, p3

    invoke-static {v10, v0}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 72
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 86
    .end local v10    # "group":Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;
    .end local v11    # "groups":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;>;"
    .end local v12    # "i":I
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v15    # "response":Lcom/google/android/music/cloudclient/TabJson;
    .end local v17    # "tabType":Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;
    :sswitch_1
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v14, v0

    .line 87
    .local v14, "id":I
    if-gez v14, :cond_5

    .line 88
    const-string v18, "ExploreContentProviderHelper"

    const-string v19, "id out of range"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 91
    :cond_5
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/ExploreContentProviderHelper;->getTabType(I)Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;

    move-result-object v17

    .line 92
    .restart local v17    # "tabType":Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;
    const-string v18, "genreid"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 93
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v2, v1, v8}, Lcom/google/android/music/store/ExploreContentProviderHelper;->getTab(Landroid/content/Context;Lcom/google/android/music/cloudclient/MusicCloud;Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)Lcom/google/android/music/cloudclient/TabJson;

    move-result-object v15

    .line 94
    .restart local v15    # "response":Lcom/google/android/music/cloudclient/TabJson;
    if-eqz v15, :cond_6

    iget-object v0, v15, Lcom/google/android/music/cloudclient/TabJson;->mGroups:Ljava/util/List;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    iget-object v0, v15, Lcom/google/android/music/cloudclient/TabJson;->mGroups:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-eqz v18, :cond_7

    .line 96
    :cond_6
    const-string v18, "ExploreContentProviderHelper"

    const-string v19, "Incomplete data"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1, v8}, Lcom/google/android/music/store/ExploreContentProviderHelper;->invalidateCache(Landroid/content/Context;Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)V

    .line 98
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 101
    :cond_7
    iget-object v0, v15, Lcom/google/android/music/cloudclient/TabJson;->mGroups:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    if-le v14, v0, :cond_8

    .line 103
    const-string v18, "ExploreContentProviderHelper"

    const-string v19, "id out of range"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 107
    :cond_8
    iget-object v0, v15, Lcom/google/android/music/cloudclient/TabJson;->mGroups:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;->mEntities:Ljava/util/List;

    .line 108
    .local v4, "entities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/ExploreEntityJson;>;"
    if-nez v4, :cond_9

    .line 110
    const-string v18, "ExploreContentProviderHelper"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Group has empty entities:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1, v8}, Lcom/google/android/music/store/ExploreContentProviderHelper;->invalidateCache(Landroid/content/Context;Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)V

    .line 112
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 114
    :cond_9
    invoke-static/range {p3 .. p3}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 116
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 120
    :cond_a
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .restart local v13    # "i$":Ljava/util/Iterator;
    :cond_b
    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/cloudclient/ExploreEntityJson;

    .line 121
    .local v5, "entity":Lcom/google/android/music/cloudclient/ExploreEntityJson;
    iget-object v0, v5, Lcom/google/android/music/cloudclient/ExploreEntityJson;->mAlbum:Lcom/google/android/music/cloudclient/AlbumJson;

    move-object/from16 v18, v0

    if-eqz v18, :cond_c

    .line 122
    iget-object v0, v5, Lcom/google/android/music/cloudclient/ExploreEntityJson;->mAlbum:Lcom/google/android/music/cloudclient/AlbumJson;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/AlbumJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_3

    .line 124
    :cond_c
    iget-object v0, v5, Lcom/google/android/music/cloudclient/ExploreEntityJson;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    move-object/from16 v18, v0

    if-eqz v18, :cond_d

    .line 125
    iget-object v0, v5, Lcom/google/android/music/cloudclient/ExploreEntityJson;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/TrackJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_3

    .line 127
    :cond_d
    if-eqz v16, :cond_b

    iget-object v0, v5, Lcom/google/android/music/cloudclient/ExploreEntityJson;->mPlaylist:Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-object/from16 v18, v0

    if-eqz v18, :cond_b

    .line 128
    iget-object v0, v5, Lcom/google/android/music/cloudclient/ExploreEntityJson;->mPlaylist:Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/sync/google/model/SyncablePlaylist;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_3

    .line 137
    .end local v4    # "entities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/ExploreEntityJson;>;"
    .end local v5    # "entity":Lcom/google/android/music/cloudclient/ExploreEntityJson;
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "id":I
    .end local v15    # "response":Lcom/google/android/music/cloudclient/TabJson;
    .end local v17    # "tabType":Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    .line 140
    :sswitch_3
    const-string v18, "subgenreId"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 142
    .local v6, "filterSubgenreId":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-static {v0, v2, v8}, Lcom/google/android/music/store/ExploreContentProviderHelper;->getGenres(Landroid/content/Context;Lcom/google/android/music/cloudclient/MusicCloud;Ljava/lang/String;)Lcom/google/android/music/cloudclient/MusicGenresResponseJson;

    move-result-object v15

    .line 143
    .local v15, "response":Lcom/google/android/music/cloudclient/MusicGenresResponseJson;
    if-nez v15, :cond_e

    .line 144
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 146
    :cond_e
    iget-object v9, v15, Lcom/google/android/music/cloudclient/MusicGenresResponseJson;->mGenres:Ljava/util/List;

    .line 147
    .local v9, "genres":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MusicGenreJson;>;"
    invoke-static/range {p3 .. p3}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_f

    .line 149
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 151
    :cond_f
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .restart local v13    # "i$":Ljava/util/Iterator;
    :cond_10
    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/music/cloudclient/MusicGenreJson;

    .line 153
    .local v7, "genre":Lcom/google/android/music/cloudclient/MusicGenreJson;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_11

    iget-object v0, v7, Lcom/google/android/music/cloudclient/MusicGenreJson;->mId:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_10

    .line 157
    :cond_11
    move-object/from16 v0, p3

    invoke-static {v7, v0}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/MusicGenreJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_4

    .line 48
    nop

    :sswitch_data_0
    .sparse-switch
        0x640 -> :sswitch_0
        0x641 -> :sswitch_1
        0x64a -> :sswitch_0
        0x64b -> :sswitch_1
        0x654 -> :sswitch_0
        0x655 -> :sswitch_1
        0x65e -> :sswitch_3
        0x65f -> :sswitch_2
    .end sparse-switch
.end method
