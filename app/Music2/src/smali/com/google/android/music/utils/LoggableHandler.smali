.class public Lcom/google/android/music/utils/LoggableHandler;
.super Landroid/os/Handler;
.source "LoggableHandler.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const/16 v0, 0xa

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;I)V

    .line 32
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "priority"    # I

    .prologue
    .line 35
    invoke-static {p1, p2}, Lcom/google/android/music/utils/LoggableHandler;->startHandlerThread(Ljava/lang/String;I)Landroid/os/HandlerThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 36
    return-void
.end method

.method private static startHandlerThread(Ljava/lang/String;I)Landroid/os/HandlerThread;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "priority"    # I

    .prologue
    .line 42
    new-instance v1, Ljava/util/concurrent/Semaphore;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 43
    .local v1, "semaphore":Ljava/util/concurrent/Semaphore;
    new-instance v0, Lcom/google/android/music/utils/LoggableHandler$1;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/music/utils/LoggableHandler$1;-><init>(Ljava/lang/String;ILjava/util/concurrent/Semaphore;)V

    .line 50
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 52
    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 53
    return-object v0
.end method


# virtual methods
.method public dispatchMessage(Landroid/os/Message;)V
    .locals 0
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 84
    return-void
.end method

.method public quit()V
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/music/utils/LoggableHandler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 61
    return-void
.end method

.method public sendMessageAtTime(Landroid/os/Message;J)Z
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;
    .param p2, "uptimeMillis"    # J

    .prologue
    .line 91
    invoke-super {p0, p1, p2, p3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    move-result v0

    return v0
.end method
