.class public interface abstract Lcom/google/android/music/ui/ShareableElement;
.super Ljava/lang/Object;
.source "ShareableElement.java"


# virtual methods
.method public abstract getSnapshotElement()Landroid/view/View;
.end method

.method public abstract getTransitionInfo()Lcom/google/android/music/ui/TransitionInfo;
.end method

.method public abstract setSharedElementSnapshotInfo(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V
.end method
