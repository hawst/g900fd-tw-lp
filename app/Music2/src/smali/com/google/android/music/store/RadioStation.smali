.class public Lcom/google/android/music/store/RadioStation;
.super Lcom/google/android/music/store/Syncable;
.source "RadioStation.java"


# static fields
.field private static final PROJECTION:[Ljava/lang/String;

.field private static final TOMBSTONE_PROJECTION:[Ljava/lang/String;

.field private static TOMBSTONE_PROJECTION_INDEX_ID:I

.field private static TOMBSTONE_PROJECTION_SOURCE_ID:I

.field private static TOMBSTONE_PROJECTION_SOURCE_VERSION:I


# instance fields
.field private mArtworkLocation:Ljava/lang/String;

.field private mArtworkType:I

.field private mClientId:Ljava/lang/String;

.field private mDescription:Ljava/lang/String;

.field private mHighlightColor:Ljava/lang/String;

.field private mId:J

.field private mName:Ljava/lang/String;

.field private mProfileImage:Ljava/lang/String;

.field private mRecentTimestampMicrosec:J

.field private mSeedSourceId:Ljava/lang/String;

.field private mSeedSourceType:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 81
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "RADIO_STATIONS.Id"

    aput-object v1, v0, v3

    const-string v1, "RADIO_STATIONS.ClientId"

    aput-object v1, v0, v4

    const-string v1, "RADIO_STATIONS.Name"

    aput-object v1, v0, v5

    const-string v1, "RADIO_STATIONS.Description"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "RADIO_STATIONS.RecentTimestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "RADIO_STATIONS.ArtworkLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "RADIO_STATIONS.ArtworkType"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "RADIO_STATIONS.SeedSourceId"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "RADIO_STATIONS.SeedSourceType"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "RADIO_STATIONS._sync_dirty"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "RADIO_STATIONS._sync_version"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "RADIO_STATIONS.SourceAccount"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "RADIO_STATIONS.SourceId"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "RADIO_STATIONS.HighlightColor"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "RADIO_STATIONS.ProfileImage"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/RadioStation;->PROJECTION:[Ljava/lang/String;

    .line 116
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "Id"

    aput-object v1, v0, v3

    const-string v1, "SourceId"

    aput-object v1, v0, v4

    const-string v1, "_sync_version"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/store/RadioStation;->TOMBSTONE_PROJECTION:[Ljava/lang/String;

    .line 122
    sput v3, Lcom/google/android/music/store/RadioStation;->TOMBSTONE_PROJECTION_INDEX_ID:I

    .line 123
    sput v4, Lcom/google/android/music/store/RadioStation;->TOMBSTONE_PROJECTION_SOURCE_ID:I

    .line 124
    sput v5, Lcom/google/android/music/store/RadioStation;->TOMBSTONE_PROJECTION_SOURCE_VERSION:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/google/android/music/store/Syncable;-><init>()V

    .line 139
    return-void
.end method

.method static addRadioSongs(Landroid/database/sqlite/SQLiteDatabase;JLjava/util/List;)I
    .locals 7
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "radioStationId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 535
    .local p3, "musicIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .line 536
    .local v0, "added":I
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 537
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "RadioStationId"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 538
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 539
    .local v2, "musicId":Ljava/lang/Long;
    if-eqz v2, :cond_0

    .line 540
    const-string v4, "MusicId"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 541
    const-string v4, "RADIO_SONGS"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 542
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 545
    .end local v2    # "musicId":Ljava/lang/Long;
    :cond_1
    return v0
.end method

.method public static addRecommendedRadio(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 6
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "seedId"    # Ljava/lang/String;
    .param p3, "seedType"    # I
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "urls"    # Ljava/lang/String;
    .param p6, "description"    # Ljava/lang/String;
    .param p7, "highlightColor"    # Ljava/lang/String;
    .param p8, "profileImage"    # Ljava/lang/String;

    .prologue
    .line 842
    new-instance v1, Lcom/google/android/music/store/RadioStation;

    invoke-direct {v1}, Lcom/google/android/music/store/RadioStation;-><init>()V

    .line 843
    .local v1, "radioStation":Lcom/google/android/music/store/RadioStation;
    invoke-static {}, Lcom/google/android/music/store/Store;->generateClientId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/music/store/RadioStation;->setClientId(Ljava/lang/String;)V

    .line 844
    invoke-virtual {v1, p2}, Lcom/google/android/music/store/RadioStation;->setSeedSourceId(Ljava/lang/String;)V

    .line 845
    invoke-virtual {v1, p3}, Lcom/google/android/music/store/RadioStation;->setSeedSourceType(I)V

    .line 846
    invoke-virtual {v1, p4}, Lcom/google/android/music/store/RadioStation;->setName(Ljava/lang/String;)V

    .line 847
    invoke-virtual {v1, p5}, Lcom/google/android/music/store/RadioStation;->setArtworkLocation(Ljava/lang/String;)V

    .line 848
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lcom/google/android/music/store/RadioStation;->setArtworkType(I)V

    .line 849
    invoke-virtual {v1, p6}, Lcom/google/android/music/store/RadioStation;->setDescription(Ljava/lang/String;)V

    .line 850
    invoke-virtual {v1, p7}, Lcom/google/android/music/store/RadioStation;->setHighlightColor(Ljava/lang/String;)V

    .line 851
    invoke-virtual {v1, p8}, Lcom/google/android/music/store/RadioStation;->setProfileImage(Ljava/lang/String;)V

    .line 852
    invoke-static {p1}, Lcom/google/android/music/store/Store;->computeAccountHash(Landroid/accounts/Account;)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/android/music/store/RadioStation;->setSourceAccount(I)V

    .line 854
    const-wide/16 v2, -0x1

    .line 856
    .local v2, "insertId":J
    :try_start_0
    invoke-static {p0}, Lcom/google/android/music/store/RadioStation;->compileInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/music/store/RadioStation;->insert(Landroid/database/sqlite/SQLiteStatement;)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    move-wide v4, v2

    .line 863
    :goto_0
    return-wide v4

    .line 857
    :catch_0
    move-exception v0

    .line 860
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v4, "RadioStation"

    const-string v5, "Failed to insert recommended radio into radio station table, skipping"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    const-wide/16 v4, -0x1

    goto :goto_0
.end method

.method public static compileInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 269
    const-string v0, "insert into RADIO_STATIONS ( ClientId, Name, Description, RecentTimestamp, ArtworkLocation, ArtworkType, SeedSourceId, SeedSourceType, _sync_dirty, _sync_version, SourceAccount, SourceId, HighlightColor, ProfileImage) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method public static compileUpdateStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 273
    const-string v0, "update RADIO_STATIONS set ClientId=?, Name=?, Description=?, RecentTimestamp=?, ArtworkLocation=?, ArtworkType=?, SeedSourceId=?, SeedSourceType=?, _sync_dirty=?, _sync_version=?, SourceAccount=?, SourceId=?, HighlightColor=?, ProfileImage=?  where Id=?"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method public static deleteById(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "radioStationId"    # J

    .prologue
    .line 468
    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v1}, Lcom/google/android/music/store/RadioStation;->read(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/android/music/store/RadioStation;)Lcom/google/android/music/store/RadioStation;

    move-result-object v0

    .line 469
    .local v0, "station":Lcom/google/android/music/store/RadioStation;
    if-eqz v0, :cond_0

    .line 470
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/music/store/RadioStation;->delete(Landroid/database/sqlite/SQLiteDatabase;Z)Z

    .line 472
    :cond_0
    return-void
.end method

.method public static deleteBySourceInfo(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "sourceAccount"    # I
    .param p2, "sourceId"    # Ljava/lang/String;

    .prologue
    .line 482
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, p2, v2}, Lcom/google/android/music/store/RadioStation;->read(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/RadioStation;)Lcom/google/android/music/store/RadioStation;

    move-result-object v0

    .line 483
    .local v0, "station":Lcom/google/android/music/store/RadioStation;
    if-eqz v0, :cond_0

    .line 484
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/music/store/RadioStation;->delete(Landroid/database/sqlite/SQLiteDatabase;Z)Z

    .line 486
    :cond_0
    return-void
.end method

.method static deleteNewSongs(Landroid/database/sqlite/SQLiteDatabase;J)I
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "radioStationId"    # J

    .prologue
    const/4 v4, 0x0

    .line 642
    const-string v0, "RADIO_SONGS"

    const-string v1, "Id=? AND State=?"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static deleteOrphanedRadioSongs(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 618
    const-string v0, "RADIO_SONGS"

    const-string v1, "RADIO_SONGS.Id IN (SELECT rs.Id FROM RADIO_SONGS as rs  LEFT JOIN MUSIC ON ( rs.MusicId=MUSIC.Id ) WHERE MUSIC.Id IS NULL )"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static deleteRadioSongs(Landroid/database/sqlite/SQLiteDatabase;J)I
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "radioStationId"    # J

    .prologue
    .line 496
    const-string v0, "RADIO_SONGS"

    const-string v1, "RadioStationId=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static deleteSubmittedRadioSongs(Landroid/database/sqlite/SQLiteDatabase;J)I
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "radioStationId"    # J

    .prologue
    .line 502
    const-string v0, "RADIO_SONGS"

    const-string v1, "RadioStationId=? AND State=?"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/16 v4, 0x3e8

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static getRadioSongIds(Landroid/database/sqlite/SQLiteDatabase;JI)Ljava/util/List;
    .locals 15
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "radioStationId"    # J
    .param p3, "state"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "JI)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/ContentIdentifier;",
            ">;"
        }
    .end annotation

    .prologue
    .line 580
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 581
    .local v13, "ids":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    const/4 v8, 0x0

    .line 584
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    const-string v1, "RADIO_SONGS JOIN MUSIC ON (RADIO_SONGS.MusicId=MUSIC.Id) "

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "MUSIC.Id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "Domain"

    aput-object v3, v2, v0

    const-string v3, "RadioStationId=? AND State=?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x1

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "RADIO_SONGS.Id"

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 596
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 598
    .local v10, "fileId":J
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 599
    .local v9, "domain":I
    new-instance v12, Lcom/google/android/music/download/ContentIdentifier;

    invoke-static {v9}, Lcom/google/android/music/download/ContentIdentifier$Domain;->fromDBValue(I)Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v0

    invoke-direct {v12, v10, v11, v0}, Lcom/google/android/music/download/ContentIdentifier;-><init>(JLcom/google/android/music/download/ContentIdentifier$Domain;)V

    .line 601
    .local v12, "id":Lcom/google/android/music/download/ContentIdentifier;
    invoke-interface {v13, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 605
    .end local v9    # "domain":I
    .end local v10    # "fileId":J
    .end local v12    # "id":Lcom/google/android/music/download/ContentIdentifier;
    :cond_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 608
    return-object v13

    .line 605
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static getRadioStationTombstones(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;
    .locals 8
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x0

    .line 801
    const-string v1, "RADIO_STATION_TOMBSTONES"

    sget-object v2, Lcom/google/android/music/store/RadioStation;->TOMBSTONE_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getRadioStationsToSync(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;
    .locals 8
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v4, 0x0

    .line 773
    const-string v1, "RADIO_STATIONS"

    sget-object v2, Lcom/google/android/music/store/RadioStation;->PROJECTION:[Ljava/lang/String;

    const-string v3, "RADIO_STATIONS._sync_dirty=1"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static getSubmittedRadioSongIds(Lcom/google/android/music/store/Store;J)Ljava/util/List;
    .locals 3
    .param p0, "store"    # Lcom/google/android/music/store/Store;
    .param p1, "radioStationId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/store/Store;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/ContentIdentifier;",
            ">;"
        }
    .end annotation

    .prologue
    .line 558
    invoke-virtual {p0}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 560
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v1, 0x3e8

    :try_start_0
    invoke-static {v0, p1, p2, v1}, Lcom/google/android/music/store/RadioStation;->getRadioSongIds(Landroid/database/sqlite/SQLiteDatabase;JI)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 563
    invoke-virtual {p0, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {p0, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v1
.end method

.method public static getSuggestedRadioSeedType(Ljava/lang/String;)I
    .locals 2
    .param p0, "seedId"    # Ljava/lang/String;

    .prologue
    .line 814
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 815
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "empty seed id to check"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 817
    :cond_0
    const-string v0, "T"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 818
    const/4 v0, 0x2

    .line 820
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static markNewRadioSongsAsSubmitted(Landroid/database/sqlite/SQLiteDatabase;J)I
    .locals 7
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "radioStationId"    # J

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 512
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, v6}, Landroid/content/ContentValues;-><init>(I)V

    .line 513
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "State"

    const/16 v2, 0x3e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 514
    const-string v1, "RADIO_SONGS"

    const-string v2, "RadioStationId=? AND State=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method private prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 8
    .param p1, "statement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    const/16 v7, 0xd

    const/16 v6, 0xc

    const/16 v5, 0xa

    const/4 v4, 0x5

    const/4 v2, 0x3

    .line 277
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 279
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mClientId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 280
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Client id must be set before storing"

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 282
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/music/store/RadioStation;->mClientId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 284
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mName:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 285
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Name must be set before storing"

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 287
    :cond_1
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/music/store/RadioStation;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 289
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mDescription:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 290
    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 295
    :goto_0
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/music/store/RadioStation;->mRecentTimestampMicrosec:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 297
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mArtworkLocation:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 298
    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 303
    :goto_1
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/music/store/RadioStation;->mArtworkType:I

    int-to-long v2, v1

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 305
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mSeedSourceId:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 306
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Seed source id must be set before storing"

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 292
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_0

    .line 300
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mArtworkLocation:Ljava/lang/String;

    invoke-virtual {p1, v4, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_1

    .line 308
    :cond_4
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/music/store/RadioStation;->mSeedSourceId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 310
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/music/store/RadioStation;->mSeedSourceType:I

    int-to-long v2, v1

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 312
    const/16 v2, 0x9

    iget-boolean v0, p0, Lcom/google/android/music/store/RadioStation;->mNeedsSync:Z

    if-eqz v0, :cond_5

    const-wide/16 v0, 0x1

    :goto_2
    invoke-virtual {p1, v2, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 315
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mSourceVersion:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 316
    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 321
    :goto_3
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/android/music/store/RadioStation;->mSourceAccount:I

    int-to-long v2, v1

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 323
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mSourceId:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 324
    invoke-virtual {p1, v6}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 329
    :goto_4
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mHighlightColor:Ljava/lang/String;

    if-nez v0, :cond_8

    .line 330
    invoke-virtual {p1, v7}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 335
    :goto_5
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mProfileImage:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 336
    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 342
    :goto_6
    return-void

    .line 312
    :cond_5
    const-wide/16 v0, 0x0

    goto :goto_2

    .line 318
    :cond_6
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mSourceVersion:Ljava/lang/String;

    invoke-virtual {p1, v5, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_3

    .line 326
    :cond_7
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mSourceId:Ljava/lang/String;

    invoke-virtual {p1, v6, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_4

    .line 332
    :cond_8
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mHighlightColor:Ljava/lang/String;

    invoke-virtual {p1, v7, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_5

    .line 338
    :cond_9
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/music/store/RadioStation;->mProfileImage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_6
.end method

.method public static read(Landroid/content/Context;JLcom/google/android/music/store/RadioStation;)Lcom/google/android/music/store/RadioStation;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "localId"    # J
    .param p3, "radioStation"    # Lcom/google/android/music/store/RadioStation;

    .prologue
    .line 664
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    .line 665
    .local v1, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 667
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/music/store/RadioStation;->read(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/android/music/store/RadioStation;)Lcom/google/android/music/store/RadioStation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 669
    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v2
.end method

.method public static read(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/android/music/store/RadioStation;)Lcom/google/android/music/store/RadioStation;
    .locals 11
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "localId"    # J
    .param p3, "radioStation"    # Lcom/google/android/music/store/RadioStation;

    .prologue
    const/4 v5, 0x0

    .line 683
    const-string v1, "RADIO_STATIONS"

    sget-object v2, Lcom/google/android/music/store/RadioStation;->PROJECTION:[Ljava/lang/String;

    const-string v3, "Id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 689
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 690
    if-nez p3, :cond_0

    .line 691
    new-instance v9, Lcom/google/android/music/store/RadioStation;

    invoke-direct {v9}, Lcom/google/android/music/store/RadioStation;-><init>()V

    .end local p3    # "radioStation":Lcom/google/android/music/store/RadioStation;
    .local v9, "radioStation":Lcom/google/android/music/store/RadioStation;
    move-object p3, v9

    .line 693
    .end local v9    # "radioStation":Lcom/google/android/music/store/RadioStation;
    .restart local p3    # "radioStation":Lcom/google/android/music/store/RadioStation;
    :cond_0
    invoke-virtual {p3, v8}, Lcom/google/android/music/store/RadioStation;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 699
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move-object v5, p3

    :goto_0
    return-object v5

    :cond_1
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static read(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/RadioStation;)Lcom/google/android/music/store/RadioStation;
    .locals 10
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "sourceAccount"    # Ljava/lang/String;
    .param p2, "sourceId"    # Ljava/lang/String;
    .param p3, "item"    # Lcom/google/android/music/store/RadioStation;

    .prologue
    const/4 v5, 0x0

    .line 713
    const-string v1, "RADIO_STATIONS"

    sget-object v2, Lcom/google/android/music/store/RadioStation;->PROJECTION:[Ljava/lang/String;

    const-string v3, "SourceAccount=? AND SourceId=?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v0, 0x1

    aput-object p2, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 720
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 721
    if-nez p3, :cond_0

    .line 722
    new-instance v9, Lcom/google/android/music/store/RadioStation;

    invoke-direct {v9}, Lcom/google/android/music/store/RadioStation;-><init>()V

    .end local p3    # "item":Lcom/google/android/music/store/RadioStation;
    .local v9, "item":Lcom/google/android/music/store/RadioStation;
    move-object p3, v9

    .line 724
    .end local v9    # "item":Lcom/google/android/music/store/RadioStation;
    .restart local p3    # "item":Lcom/google/android/music/store/RadioStation;
    :cond_0
    invoke-virtual {p3, v8}, Lcom/google/android/music/store/RadioStation;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 730
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move-object v5, p3

    :goto_0
    return-object v5

    :cond_1
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static readRecommendedStation(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;ILcom/google/android/music/store/RadioStation;)Lcom/google/android/music/store/RadioStation;
    .locals 10
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "seedId"    # Ljava/lang/String;
    .param p2, "seedType"    # I
    .param p3, "item"    # Lcom/google/android/music/store/RadioStation;

    .prologue
    const/4 v5, 0x0

    .line 747
    const-string v1, "RADIO_STATIONS"

    sget-object v2, Lcom/google/android/music/store/RadioStation;->PROJECTION:[Ljava/lang/String;

    const-string v3, "SeedSourceId=? AND SeedSourceType=? AND SourceId IS NULL"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v0, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 755
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 756
    if-nez p3, :cond_0

    .line 757
    new-instance v9, Lcom/google/android/music/store/RadioStation;

    invoke-direct {v9}, Lcom/google/android/music/store/RadioStation;-><init>()V

    .end local p3    # "item":Lcom/google/android/music/store/RadioStation;
    .local v9, "item":Lcom/google/android/music/store/RadioStation;
    move-object p3, v9

    .line 759
    .end local v9    # "item":Lcom/google/android/music/store/RadioStation;
    .restart local p3    # "item":Lcom/google/android/music/store/RadioStation;
    :cond_0
    invoke-virtual {p3, v8}, Lcom/google/android/music/store/RadioStation;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 765
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move-object v5, p3

    :goto_0
    return-object v5

    :cond_1
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method


# virtual methods
.method delete(Landroid/database/sqlite/SQLiteDatabase;Z)Z
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "sync"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 440
    iget-wide v4, p0, Lcom/google/android/music/store/RadioStation;->mId:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 441
    new-instance v2, Lcom/google/android/music/store/InvalidDataException;

    const-string v3, "Cannot delete object that was not loaded or created"

    invoke-direct {v2, v3}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 445
    :cond_0
    const-string v3, "RADIO_STATIONS"

    const-string v4, "Id=?"

    new-array v5, v0, [Ljava/lang/String;

    iget-wide v6, p0, Lcom/google/android/music/store/RadioStation;->mId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 449
    .local v1, "numberOfDeletedLists":I
    if-lez v1, :cond_3

    .line 450
    .local v0, "deleted":Z
    :goto_0
    if-eqz p2, :cond_1

    if-eqz v0, :cond_1

    .line 451
    const-string v2, "RADIO_STATION_TOMBSTONES"

    invoke-virtual {p0, p1, v2}, Lcom/google/android/music/store/RadioStation;->createTombstoneIfNeeded(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    .line 454
    :cond_1
    if-eqz v0, :cond_2

    .line 455
    iget-wide v2, p0, Lcom/google/android/music/store/RadioStation;->mId:J

    invoke-static {p1, v2, v3}, Lcom/google/android/music/store/RadioStation;->deleteRadioSongs(Landroid/database/sqlite/SQLiteDatabase;J)I

    .line 458
    :cond_2
    return v0

    .end local v0    # "deleted":Z
    :cond_3
    move v0, v2

    .line 449
    goto :goto_0
.end method

.method public final getArtworkLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mArtworkLocation:Ljava/lang/String;

    return-object v0
.end method

.method public final getArtworkType()I
    .locals 1

    .prologue
    .line 233
    iget v0, p0, Lcom/google/android/music/store/RadioStation;->mArtworkType:I

    return v0
.end method

.method public final getClientId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mClientId:Ljava/lang/String;

    return-object v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public final getId()J
    .locals 2

    .prologue
    .line 181
    iget-wide v0, p0, Lcom/google/android/music/store/RadioStation;->mId:J

    return-wide v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getRecentTimestampMicrosec()J
    .locals 2

    .prologue
    .line 217
    iget-wide v0, p0, Lcom/google/android/music/store/RadioStation;->mRecentTimestampMicrosec:J

    return-wide v0
.end method

.method public final getSeedSourceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/music/store/RadioStation;->mSeedSourceId:Ljava/lang/String;

    return-object v0
.end method

.method public final getSeedSourceType()I
    .locals 1

    .prologue
    .line 249
    iget v0, p0, Lcom/google/android/music/store/RadioStation;->mSeedSourceType:I

    return v0
.end method

.method public insert(Landroid/database/sqlite/SQLiteStatement;)J
    .locals 6
    .param p1, "insertStatement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    .line 402
    iget-wide v2, p0, Lcom/google/android/music/store/RadioStation;->mId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 403
    new-instance v2, Lcom/google/android/music/store/InvalidDataException;

    const-string v3, "The local id of a radio station must not be set for an insert."

    invoke-direct {v2, v3}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 407
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/store/RadioStation;->mClientId:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 408
    invoke-static {}, Lcom/google/android/music/store/Store;->generateClientId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/RadioStation;->mClientId:Ljava/lang/String;

    .line 411
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/music/store/RadioStation;->prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V

    .line 412
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v0

    .line 413
    .local v0, "insertedId":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 414
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Failed to insert into radio stations"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 416
    :cond_2
    iput-wide v0, p0, Lcom/google/android/music/store/RadioStation;->mId:J

    .line 419
    iget-wide v2, p0, Lcom/google/android/music/store/RadioStation;->mId:J

    return-wide v2
.end method

.method public populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    .locals 7
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v6, 0x3

    const/4 v1, 0x0

    const/16 v5, 0xa

    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 348
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/store/RadioStation;->mId:J

    .line 349
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/RadioStation;->mClientId:Ljava/lang/String;

    .line 350
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/RadioStation;->mName:Ljava/lang/String;

    .line 352
    invoke-interface {p1, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 353
    iput-object v4, p0, Lcom/google/android/music/store/RadioStation;->mDescription:Ljava/lang/String;

    .line 358
    :goto_0
    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/store/RadioStation;->mRecentTimestampMicrosec:J

    .line 360
    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 361
    iput-object v4, p0, Lcom/google/android/music/store/RadioStation;->mArtworkLocation:Ljava/lang/String;

    .line 366
    :goto_1
    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/store/RadioStation;->mArtworkType:I

    .line 368
    const/16 v2, 0x8

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/store/RadioStation;->mSeedSourceType:I

    .line 369
    const/4 v2, 0x7

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/RadioStation;->mSeedSourceId:Ljava/lang/String;

    .line 371
    invoke-interface {p1, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 372
    iput-object v4, p0, Lcom/google/android/music/store/RadioStation;->mSourceVersion:Ljava/lang/String;

    .line 377
    :goto_2
    const/16 v2, 0x9

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v0, :cond_3

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/music/store/RadioStation;->mNeedsSync:Z

    .line 378
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/RadioStation;->mSourceAccount:I

    .line 379
    invoke-interface {p1, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 380
    iput-object v4, p0, Lcom/google/android/music/store/RadioStation;->mSourceId:Ljava/lang/String;

    .line 385
    :goto_4
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 386
    iput-object v4, p0, Lcom/google/android/music/store/RadioStation;->mHighlightColor:Ljava/lang/String;

    .line 391
    :goto_5
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 392
    iput-object v4, p0, Lcom/google/android/music/store/RadioStation;->mProfileImage:Ljava/lang/String;

    .line 396
    :goto_6
    return-void

    .line 355
    :cond_0
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/RadioStation;->mDescription:Ljava/lang/String;

    goto :goto_0

    .line 363
    :cond_1
    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/RadioStation;->mArtworkLocation:Ljava/lang/String;

    goto :goto_1

    .line 374
    :cond_2
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/RadioStation;->mSourceVersion:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move v0, v1

    .line 377
    goto :goto_3

    .line 382
    :cond_4
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/RadioStation;->mSourceId:Ljava/lang/String;

    goto :goto_4

    .line 388
    :cond_5
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/RadioStation;->mHighlightColor:Ljava/lang/String;

    goto :goto_5

    .line 394
    :cond_6
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/RadioStation;->mProfileImage:Ljava/lang/String;

    goto :goto_6
.end method

.method public populateFromTombstoneProjectionCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 786
    sget v0, Lcom/google/android/music/store/RadioStation;->TOMBSTONE_PROJECTION_INDEX_ID:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/store/RadioStation;->mId:J

    .line 787
    sget v0, Lcom/google/android/music/store/RadioStation;->TOMBSTONE_PROJECTION_SOURCE_ID:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 788
    sget v0, Lcom/google/android/music/store/RadioStation;->TOMBSTONE_PROJECTION_SOURCE_ID:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/RadioStation;->mSourceId:Ljava/lang/String;

    .line 790
    :cond_0
    sget v0, Lcom/google/android/music/store/RadioStation;->TOMBSTONE_PROJECTION_SOURCE_VERSION:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 791
    sget v0, Lcom/google/android/music/store/RadioStation;->TOMBSTONE_PROJECTION_SOURCE_VERSION:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/RadioStation;->mSourceVersion:Ljava/lang/String;

    .line 793
    :cond_1
    return-void
.end method

.method public reset()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 142
    invoke-super {p0}, Lcom/google/android/music/store/Syncable;->reset()V

    .line 143
    iput-wide v2, p0, Lcom/google/android/music/store/RadioStation;->mId:J

    .line 144
    iput-object v0, p0, Lcom/google/android/music/store/RadioStation;->mClientId:Ljava/lang/String;

    .line 145
    iput-object v0, p0, Lcom/google/android/music/store/RadioStation;->mName:Ljava/lang/String;

    .line 146
    iput-object v0, p0, Lcom/google/android/music/store/RadioStation;->mDescription:Ljava/lang/String;

    .line 147
    iput-wide v2, p0, Lcom/google/android/music/store/RadioStation;->mRecentTimestampMicrosec:J

    .line 148
    iput-object v0, p0, Lcom/google/android/music/store/RadioStation;->mArtworkLocation:Ljava/lang/String;

    .line 149
    iput v1, p0, Lcom/google/android/music/store/RadioStation;->mArtworkType:I

    .line 150
    iput-object v0, p0, Lcom/google/android/music/store/RadioStation;->mSeedSourceId:Ljava/lang/String;

    .line 151
    iput v1, p0, Lcom/google/android/music/store/RadioStation;->mSeedSourceType:I

    .line 152
    iput-object v0, p0, Lcom/google/android/music/store/RadioStation;->mHighlightColor:Ljava/lang/String;

    .line 153
    iput-object v0, p0, Lcom/google/android/music/store/RadioStation;->mProfileImage:Ljava/lang/String;

    .line 154
    return-void
.end method

.method public final setArtworkLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/music/store/RadioStation;->mArtworkLocation:Ljava/lang/String;

    .line 222
    return-void
.end method

.method public final setArtworkType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 229
    iput p1, p0, Lcom/google/android/music/store/RadioStation;->mArtworkType:I

    .line 230
    return-void
.end method

.method public final setClientId(Ljava/lang/String;)V
    .locals 0
    .param p1, "clientId"    # Ljava/lang/String;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/music/store/RadioStation;->mClientId:Ljava/lang/String;

    .line 190
    return-void
.end method

.method public final setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/google/android/music/store/RadioStation;->mDescription:Ljava/lang/String;

    .line 210
    return-void
.end method

.method public final setHighlightColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "highlightColor"    # Ljava/lang/String;

    .prologue
    .line 253
    iput-object p1, p0, Lcom/google/android/music/store/RadioStation;->mHighlightColor:Ljava/lang/String;

    .line 254
    return-void
.end method

.method public final setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/google/android/music/store/RadioStation;->mName:Ljava/lang/String;

    .line 202
    return-void
.end method

.method public final setProfileImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "profileImage"    # Ljava/lang/String;

    .prologue
    .line 261
    iput-object p1, p0, Lcom/google/android/music/store/RadioStation;->mProfileImage:Ljava/lang/String;

    .line 262
    return-void
.end method

.method public final setRecentTimestampMicrosec(J)V
    .locals 1
    .param p1, "recentTimestamp"    # J

    .prologue
    .line 213
    iput-wide p1, p0, Lcom/google/android/music/store/RadioStation;->mRecentTimestampMicrosec:J

    .line 214
    return-void
.end method

.method public final setSeedSourceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "seedSourceId"    # Ljava/lang/String;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/google/android/music/store/RadioStation;->mSeedSourceId:Ljava/lang/String;

    .line 238
    return-void
.end method

.method public final setSeedSourceType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 245
    iput p1, p0, Lcom/google/android/music/store/RadioStation;->mSeedSourceType:I

    .line 246
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 159
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    const-string v1, "mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/RadioStation;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    const-string v1, "mClientId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/RadioStation;->mClientId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    const-string v1, "mName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/RadioStation;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    const-string v1, "mDescription="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/RadioStation;->mDescription:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    const-string v1, "mRecentTimestampMicrosec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/RadioStation;->mRecentTimestampMicrosec:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    const-string v1, "mArtworkLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/RadioStation;->mArtworkLocation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    const-string v1, "mArtworkType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/RadioStation;->mArtworkType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    const-string v1, "mSeedSourceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/RadioStation;->mSeedSourceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    const-string v1, "mSeedSourceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/RadioStation;->mSeedSourceType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    const-string v1, "mSourceAccount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/RadioStation;->mSourceAccount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    const-string v1, "mSourceVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/RadioStation;->mSourceVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    const-string v1, "mSourceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/RadioStation;->mSourceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    const-string v1, "mNeedsSync="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/store/RadioStation;->mNeedsSync:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    const-string v1, "mHighlightColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/RadioStation;->mHighlightColor:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    const-string v1, "mProfileImage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/RadioStation;->mProfileImage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public update(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 4
    .param p1, "updateStatement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    .line 426
    iget-wide v0, p0, Lcom/google/android/music/store/RadioStation;->mId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 427
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Object cannot be updated before it\'s created"

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 430
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/store/RadioStation;->prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V

    .line 431
    const/16 v0, 0xf

    iget-wide v2, p0, Lcom/google/android/music/store/RadioStation;->mId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 432
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 433
    return-void
.end method
