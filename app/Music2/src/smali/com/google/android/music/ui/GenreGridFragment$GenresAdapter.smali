.class final Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter;
.super Lcom/google/android/music/ui/MediaListCardAdapter;
.source "GenreGridFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/GenreGridFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "GenresAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/GenreGridFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/GenreGridFragment;Lcom/google/android/music/ui/GenreGridFragment;)V
    .locals 1
    .param p2, "fragment"    # Lcom/google/android/music/ui/GenreGridFragment;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter;->this$0:Lcom/google/android/music/ui/GenreGridFragment;

    .line 81
    # getter for: Lcom/google/android/music/ui/GenreGridFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {p1}, Lcom/google/android/music/ui/GenreGridFragment;->access$200(Lcom/google/android/music/ui/GenreGridFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/music/ui/MediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;I)V

    .line 82
    new-instance v0, Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter$1;-><init>(Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter;Lcom/google/android/music/ui/GenreGridFragment;)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 90
    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/ui/GenreGridFragment;Lcom/google/android/music/ui/GenreGridFragment;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "fragment"    # Lcom/google/android/music/ui/GenreGridFragment;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter;->this$0:Lcom/google/android/music/ui/GenreGridFragment;

    .line 93
    # getter for: Lcom/google/android/music/ui/GenreGridFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {p1}, Lcom/google/android/music/ui/GenreGridFragment;->access$200(Lcom/google/android/music/ui/GenreGridFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v0

    invoke-direct {p0, p2, v0, p3}, Lcom/google/android/music/ui/MediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ILandroid/database/Cursor;)V

    .line 94
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/GenreGridFragment;Lcom/google/android/music/ui/GenreGridFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/GenreGridFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/GenreGridFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/GenreGridFragment;
    .param p3, "x2"    # Landroid/database/Cursor;
    .param p4, "x3"    # Lcom/google/android/music/ui/GenreGridFragment$1;

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter;-><init>(Lcom/google/android/music/ui/GenreGridFragment;Lcom/google/android/music/ui/GenreGridFragment;Landroid/database/Cursor;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/GenreGridFragment;Lcom/google/android/music/ui/GenreGridFragment;Lcom/google/android/music/ui/GenreGridFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/GenreGridFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/GenreGridFragment;
    .param p3, "x2"    # Lcom/google/android/music/ui/GenreGridFragment$1;

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter;-><init>(Lcom/google/android/music/ui/GenreGridFragment;Lcom/google/android/music/ui/GenreGridFragment;)V

    return-void
.end method


# virtual methods
.method protected bindViewToLoadingItem(Landroid/view/View;Landroid/content/Context;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 128
    instance-of v1, p1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 129
    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 130
    .local v0, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bindLoading()V

    .line 132
    .end local v0    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_0
    return-void
.end method

.method protected bindViewToMediaListItem(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;J)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "itemId"    # J

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 109
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->getDocument(Landroid/view/View;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 111
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 112
    .local v4, "id":J
    invoke-virtual {v0, v4, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 114
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 115
    .local v1, "genreName":Ljava/lang/String;
    const/4 v7, 0x2

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_1

    .line 116
    .local v2, "hasLocal":Z
    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 117
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v0, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 118
    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setHasLocal(Z)V

    .line 120
    instance-of v6, p1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v6, :cond_0

    move-object v3, p1

    .line 121
    check-cast v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 122
    .local v3, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    iget-object v6, p0, Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter;->this$0:Lcom/google/android/music/ui/GenreGridFragment;

    iget-object v6, v6, Lcom/google/android/music/ui/GenreGridFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-virtual {v3, v0, v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 124
    .end local v3    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_0
    return-void

    .end local v2    # "hasLocal":Z
    :cond_1
    move v2, v6

    .line 115
    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 98
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/MediaListCardAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 99
    .local v0, "view":Landroid/view/View;
    instance-of v1, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 100
    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    iget-object v2, p0, Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter;->this$0:Lcom/google/android/music/ui/GenreGridFragment;

    # getter for: Lcom/google/android/music/ui/GenreGridFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {v2}, Lcom/google/android/music/ui/GenreGridFragment;->access$200(Lcom/google/android/music/ui/GenreGridFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 103
    :cond_0
    return-object v0
.end method
