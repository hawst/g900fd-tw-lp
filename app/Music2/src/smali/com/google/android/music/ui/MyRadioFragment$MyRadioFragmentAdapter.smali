.class final Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;
.super Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;
.source "MyRadioFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/MyRadioFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "MyRadioFragmentAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/MyRadioFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/MyRadioFragment;Lcom/google/android/music/ui/MyRadioFragment;)V
    .locals 3
    .param p2, "fragment"    # Lcom/google/android/music/ui/MyRadioFragment;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;->this$0:Lcom/google/android/music/ui/MyRadioFragment;

    .line 185
    # getter for: Lcom/google/android/music/ui/MyRadioFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {p1}, Lcom/google/android/music/ui/MyRadioFragment;->access$200(Lcom/google/android/music/ui/MyRadioFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v0

    # getter for: Lcom/google/android/music/ui/MyRadioFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {p1}, Lcom/google/android/music/ui/MyRadioFragment;->access$200(Lcom/google/android/music/ui/MyRadioFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, p2, v0, v1, v2}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;III)V

    .line 186
    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/ui/MyRadioFragment;Lcom/google/android/music/ui/MyRadioFragment;Landroid/database/Cursor;)V
    .locals 6
    .param p2, "fragment"    # Lcom/google/android/music/ui/MyRadioFragment;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;->this$0:Lcom/google/android/music/ui/MyRadioFragment;

    .line 189
    # getter for: Lcom/google/android/music/ui/MyRadioFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {p1}, Lcom/google/android/music/ui/MyRadioFragment;->access$200(Lcom/google/android/music/ui/MyRadioFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v2

    # getter for: Lcom/google/android/music/ui/MyRadioFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {p1}, Lcom/google/android/music/ui/MyRadioFragment;->access$200(Lcom/google/android/music/ui/MyRadioFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v3

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;IIILandroid/database/Cursor;)V

    .line 191
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/MyRadioFragment;Lcom/google/android/music/ui/MyRadioFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/MyRadioFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/MyRadioFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/MyRadioFragment;
    .param p3, "x2"    # Landroid/database/Cursor;
    .param p4, "x3"    # Lcom/google/android/music/ui/MyRadioFragment$1;

    .prologue
    .line 182
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;-><init>(Lcom/google/android/music/ui/MyRadioFragment;Lcom/google/android/music/ui/MyRadioFragment;Landroid/database/Cursor;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/MyRadioFragment;Lcom/google/android/music/ui/MyRadioFragment;Lcom/google/android/music/ui/MyRadioFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/MyRadioFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/MyRadioFragment;
    .param p3, "x2"    # Lcom/google/android/music/ui/MyRadioFragment$1;

    .prologue
    .line 182
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;-><init>(Lcom/google/android/music/ui/MyRadioFragment;Lcom/google/android/music/ui/MyRadioFragment;)V

    return-void
.end method


# virtual methods
.method protected bindViewToLoadingItem(Landroid/view/View;Landroid/content/Context;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 263
    instance-of v1, p1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 264
    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 265
    .local v0, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bindLoading()V

    .line 267
    .end local v0    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_0
    return-void
.end method

.method protected bindViewToMediaListItem(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;J)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "itemId"    # J

    .prologue
    .line 226
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->getDocument(Landroid/view/View;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v2

    .line 228
    .local v2, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    const/4 v11, 0x0

    invoke-interface {p3, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 229
    .local v4, "id":J
    const/4 v11, 0x1

    invoke-interface {p3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 230
    .local v10, "title":Ljava/lang/String;
    const/4 v11, 0x2

    invoke-interface {p3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 231
    .local v1, "description":Ljava/lang/String;
    const/4 v11, 0x3

    invoke-interface {p3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 232
    .local v0, "artUrl":Ljava/lang/String;
    const/4 v11, 0x4

    invoke-interface {p3, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    if-eqz v11, :cond_2

    const/4 v3, 0x1

    .line 233
    .local v3, "hasLocal":Z
    :goto_0
    const/4 v11, 0x5

    invoke-interface {p3, v11}, Landroid/database/Cursor;->isNull(I)Z

    move-result v11

    if-eqz v11, :cond_3

    const/4 v8, 0x0

    .line 235
    .local v8, "sourceId":Ljava/lang/String;
    :goto_1
    const/4 v11, 0x6

    invoke-interface {p3, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 236
    .local v9, "sourceType":I
    const/4 v11, 0x7

    invoke-interface {p3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 238
    .local v7, "remoteId":Ljava/lang/String;
    sget-object v11, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v2, v11}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 240
    invoke-virtual {v2, v4, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 241
    invoke-virtual {v2, v10}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 242
    invoke-virtual {v2, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setDescription(Ljava/lang/String;)V

    .line 243
    invoke-virtual {v2, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 244
    invoke-virtual {v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setHasLocal(Z)V

    .line 245
    invoke-virtual {v2, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioSeedId(Ljava/lang/String;)V

    .line 246
    invoke-virtual {v2, v9}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioSeedType(I)V

    .line 247
    invoke-virtual {v2, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioRemoteId(Ljava/lang/String;)V

    .line 249
    iget-object v11, p0, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;->this$0:Lcom/google/android/music/ui/MyRadioFragment;

    # getter for: Lcom/google/android/music/ui/MyRadioFragment;->mShowKeepOnView:Z
    invoke-static {v11}, Lcom/google/android/music/ui/MyRadioFragment;->access$300(Lcom/google/android/music/ui/MyRadioFragment;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 252
    const-string v11, ""

    invoke-virtual {v2, v11}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 255
    :cond_0
    instance-of v11, p1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v11, :cond_1

    move-object v6, p1

    .line 256
    check-cast v6, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 257
    .local v6, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    iget-object v11, p0, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;->this$0:Lcom/google/android/music/ui/MyRadioFragment;

    iget-object v11, v11, Lcom/google/android/music/ui/MyRadioFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-virtual {v6, v2, v11}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 259
    .end local v6    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_1
    return-void

    .line 232
    .end local v3    # "hasLocal":Z
    .end local v7    # "remoteId":Ljava/lang/String;
    .end local v8    # "sourceId":Ljava/lang/String;
    .end local v9    # "sourceType":I
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 233
    .restart local v3    # "hasLocal":Z
    :cond_3
    const/4 v11, 0x5

    invoke-interface {p3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_1
.end method

.method public getFakeView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 213
    iget-object v3, p0, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;->this$0:Lcom/google/android/music/ui/MyRadioFragment;

    # getter for: Lcom/google/android/music/ui/MyRadioFragment;->mShowKeepOnView:Z
    invoke-static {v3}, Lcom/google/android/music/ui/MyRadioFragment;->access$300(Lcom/google/android/music/ui/MyRadioFragment;)Z

    move-result v2

    .line 214
    .local v2, "showSubtitle":Z
    iget-object v3, p0, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3, v2, v4, v4, v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getImFeelingLuckyDocument(Landroid/content/Context;ZZZZ)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .local v0, "d":Lcom/google/android/music/ui/cardlib/model/Document;
    move-object v1, p2

    .line 216
    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 217
    .local v1, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    iget-object v3, p0, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;->this$0:Lcom/google/android/music/ui/MyRadioFragment;

    iget-object v3, v3, Lcom/google/android/music/ui/MyRadioFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-virtual {v1, v0, v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 218
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setTag(Ljava/lang/Object;)V

    .line 219
    new-instance v3, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;

    iget-object v4, p0, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4, v0}, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;-><init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V

    invoke-virtual {v1, v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    return-object v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 195
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 196
    .local v0, "view":Landroid/view/View;
    instance-of v1, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 197
    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    iget-object v2, p0, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;->this$0:Lcom/google/android/music/ui/MyRadioFragment;

    # getter for: Lcom/google/android/music/ui/MyRadioFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {v2}, Lcom/google/android/music/ui/MyRadioFragment;->access$200(Lcom/google/android/music/ui/MyRadioFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 200
    :cond_0
    return-object v0
.end method

.method public setShowFakeEvenIfEmpty(Z)V
    .locals 2
    .param p1, "showFakeEvenIfEmpty"    # Z

    .prologue
    .line 271
    invoke-super {p0, p1}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->setShowFakeEvenIfEmpty(Z)V

    .line 272
    iget-object v1, p0, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;->this$0:Lcom/google/android/music/ui/MyRadioFragment;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/MyRadioFragment;->setEmptyScreenVisible(Z)V

    .line 273
    return-void

    .line 272
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
