.class Lcom/google/android/music/ui/NowPlayingScreenFragment$9;
.super Ljava/lang/Object;
.source "NowPlayingScreenFragment.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingScreenFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mCurrentProgress:I

.field private mFromTouch:Z

.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 1

    .prologue
    .line 926
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 928
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->mFromTouch:Z

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6
    .param p1, "bar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromuser"    # Z

    .prologue
    .line 937
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 940
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    .line 942
    :cond_0
    if-nez p3, :cond_2

    .line 951
    :cond_1
    :goto_0
    return-void

    .line 945
    :cond_2
    iput p2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->mCurrentProgress:I

    .line 946
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mDuration:J
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1600(Lcom/google/android/music/ui/NowPlayingScreenFragment;)J

    move-result-wide v2

    iget v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->mCurrentProgress:I

    int-to-long v4, v1

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    # setter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPosOverride:J
    invoke-static {v0, v2, v3}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1502(Lcom/google/android/music/ui/NowPlayingScreenFragment;J)J

    .line 947
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->mFromTouch:Z

    if-nez v0, :cond_1

    if-eqz p3, :cond_1

    .line 949
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->onStopTrackingTouch(Landroid/widget/SeekBar;)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "bar"    # Landroid/widget/SeekBar;

    .prologue
    .line 931
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->accessibilityAnnounceSeekTime()V
    invoke-static {v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1400(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    .line 932
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->mFromTouch:Z

    .line 933
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4
    .param p1, "bar"    # Landroid/widget/SeekBar;

    .prologue
    .line 953
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1700(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    if-nez v1, :cond_0

    .line 964
    :goto_0
    return-void

    .line 956
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1700(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPosOverride:J
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1500(Lcom/google/android/music/ui/NowPlayingScreenFragment;)J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/google/android/music/playback/IMusicPlaybackService;->seek(J)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 961
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->accessibilityAnnounceSeekTime()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1400(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    .line 962
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->mFromTouch:Z

    .line 963
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    const-wide/16 v2, -0x1

    # setter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPosOverride:J
    invoke-static {v1, v2, v3}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1502(Lcom/google/android/music/ui/NowPlayingScreenFragment;J)J

    goto :goto_0

    .line 957
    :catch_0
    move-exception v0

    .line 958
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "NowPlayingFragment"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
