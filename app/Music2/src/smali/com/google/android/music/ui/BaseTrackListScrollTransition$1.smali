.class final Lcom/google/android/music/ui/BaseTrackListScrollTransition$1;
.super Landroid/util/Property;
.source "BaseTrackListScrollTransition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/BaseTrackListScrollTransition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/Property",
        "<",
        "Landroid/view/View;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0
    .param p2, "x1"    # Ljava/lang/String;

    .prologue
    .line 37
    .local p1, "x0":Ljava/lang/Class;, "Ljava/lang/Class<Ljava/lang/Integer;>;"
    invoke-direct {p0, p1, p2}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public get(Landroid/view/View;)Ljava/lang/Integer;
    .locals 2
    .param p1, "object"    # Landroid/view/View;

    .prologue
    .line 55
    new-instance v0, Ljava/lang/Integer;

    # invokes: Lcom/google/android/music/ui/BaseTrackListScrollTransition;->getScrollY(Landroid/view/View;)I
    invoke-static {p1}, Lcom/google/android/music/ui/BaseTrackListScrollTransition;->access$000(Landroid/view/View;)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 37
    check-cast p1, Landroid/view/View;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/BaseTrackListScrollTransition$1;->get(Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public set(Landroid/view/View;Ljava/lang/Integer;)V
    .locals 5
    .param p1, "object"    # Landroid/view/View;
    .param p2, "value"    # Ljava/lang/Integer;

    .prologue
    .line 41
    move-object v2, p1

    check-cast v2, Lcom/google/android/music/ui/BaseTrackListView;

    .line 44
    .local v2, "list":Lcom/google/android/music/ui/BaseTrackListView;
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/BaseTrackListScrollTransition$1;->get(Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 45
    .local v0, "currValue":I
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sub-int v1, v3, v0

    .line 47
    .local v1, "delta":I
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, -0x1

    if-le v3, v4, :cond_0

    if-eqz v2, :cond_0

    .line 49
    invoke-virtual {v2, v1}, Lcom/google/android/music/ui/BaseTrackListView;->scrollListBy(I)V

    .line 51
    :cond_0
    return-void
.end method

.method public bridge synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 37
    check-cast p1, Landroid/view/View;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/BaseTrackListScrollTransition$1;->set(Landroid/view/View;Ljava/lang/Integer;)V

    return-void
.end method
