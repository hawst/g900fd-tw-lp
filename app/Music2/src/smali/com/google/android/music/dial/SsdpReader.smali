.class public Lcom/google/android/music/dial/SsdpReader;
.super Ljava/lang/Object;
.source "SsdpReader.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private volatile mDone:Z

.field private final mListener:Lcom/google/android/music/dial/SsdpListener;

.field private final mSocket:Ljava/net/MulticastSocket;


# direct methods
.method public constructor <init>(Ljava/net/MulticastSocket;Lcom/google/android/music/dial/SsdpListener;)V
    .locals 1
    .param p1, "socket"    # Ljava/net/MulticastSocket;
    .param p2, "listener"    # Lcom/google/android/music/dial/SsdpListener;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/dial/SsdpReader;->mDone:Z

    .line 34
    iput-object p1, p0, Lcom/google/android/music/dial/SsdpReader;->mSocket:Ljava/net/MulticastSocket;

    .line 35
    iput-object p2, p0, Lcom/google/android/music/dial/SsdpReader;->mListener:Lcom/google/android/music/dial/SsdpListener;

    .line 36
    return-void
.end method

.method private isDone()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/music/dial/SsdpReader;->mDone:Z

    return v0
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 48
    const/high16 v5, 0x10000

    new-array v0, v5, [B

    .line 49
    .local v0, "buffer":[B
    new-instance v4, Ljava/net/DatagramPacket;

    array-length v5, v0

    invoke-direct {v4, v0, v5}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 51
    .local v4, "packet":Ljava/net/DatagramPacket;
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/music/dial/SsdpReader;->isDone()Z

    move-result v5

    if-nez v5, :cond_1

    .line 53
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/dial/SsdpReader;->mSocket:Ljava/net/MulticastSocket;

    invoke-virtual {v5, v4}, Ljava/net/MulticastSocket;->receive(Ljava/net/DatagramPacket;)V
    :try_end_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 68
    invoke-direct {p0}, Lcom/google/android/music/dial/SsdpReader;->isDone()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 96
    :cond_1
    return-void

    .line 54
    :catch_0
    move-exception v1

    .line 55
    .local v1, "e":Ljava/io/InterruptedIOException;
    const-string v5, "SsdpReader"

    const-string v6, "Receive interrupted"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 56
    iget-object v5, p0, Lcom/google/android/music/dial/SsdpReader;->mListener:Lcom/google/android/music/dial/SsdpListener;

    if-eqz v5, :cond_0

    .line 57
    iget-object v5, p0, Lcom/google/android/music/dial/SsdpReader;->mListener:Lcom/google/android/music/dial/SsdpListener;

    sget-object v6, Lcom/google/android/music/dial/SsdpListener$ScanError;->RECEIVE_INTERRUPTED:Lcom/google/android/music/dial/SsdpListener$ScanError;

    invoke-interface {v5, v6}, Lcom/google/android/music/dial/SsdpListener;->onScanError(Lcom/google/android/music/dial/SsdpListener$ScanError;)V

    goto :goto_0

    .line 60
    .end local v1    # "e":Ljava/io/InterruptedIOException;
    :catch_1
    move-exception v1

    .line 61
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "SsdpReader"

    const-string v6, "Error on receiving datagram"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 62
    iget-object v5, p0, Lcom/google/android/music/dial/SsdpReader;->mListener:Lcom/google/android/music/dial/SsdpListener;

    if-eqz v5, :cond_0

    .line 63
    iget-object v5, p0, Lcom/google/android/music/dial/SsdpReader;->mListener:Lcom/google/android/music/dial/SsdpListener;

    sget-object v6, Lcom/google/android/music/dial/SsdpListener$ScanError;->ERROR_RECEIVING_MESSAGE:Lcom/google/android/music/dial/SsdpListener$ScanError;

    invoke-interface {v5, v6}, Lcom/google/android/music/dial/SsdpListener;->onScanError(Lcom/google/android/music/dial/SsdpListener$ScanError;)V

    goto :goto_0

    .line 72
    .end local v1    # "e":Ljava/io/IOException;
    :cond_2
    const/4 v3, 0x0

    .line 74
    .local v3, "messageString":Ljava/lang/String;
    :try_start_1
    new-instance v3, Ljava/lang/String;

    .end local v3    # "messageString":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4}, Ljava/net/DatagramPacket;->getLength()I

    move-result v7

    const-string v8, "UTF-8"

    invoke-direct {v3, v5, v6, v7, v8}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_2

    .line 81
    .restart local v3    # "messageString":Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/music/dial/SsdpReader;->isDone()Z

    move-result v5

    if-nez v5, :cond_1

    .line 85
    invoke-static {v3}, Lcom/google/android/music/dial/SsdpMessage;->fromString(Ljava/lang/String;)Lcom/google/android/music/dial/SsdpMessage;

    move-result-object v2

    .line 86
    .local v2, "message":Lcom/google/android/music/dial/SsdpMessage;
    if-eqz v2, :cond_3

    .line 87
    iget-object v5, p0, Lcom/google/android/music/dial/SsdpReader;->mListener:Lcom/google/android/music/dial/SsdpListener;

    if-eqz v5, :cond_0

    .line 88
    iget-object v5, p0, Lcom/google/android/music/dial/SsdpReader;->mListener:Lcom/google/android/music/dial/SsdpListener;

    invoke-interface {v5, v2}, Lcom/google/android/music/dial/SsdpListener;->onSsdpMessage(Lcom/google/android/music/dial/SsdpMessage;)V

    goto :goto_0

    .line 75
    .end local v2    # "message":Lcom/google/android/music/dial/SsdpMessage;
    .end local v3    # "messageString":Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 76
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    const-string v5, "SsdpReader"

    const-string v6, "Error on converting received datagram to a string encoded in UTF-8.  Ignoring."

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 91
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v2    # "message":Lcom/google/android/music/dial/SsdpMessage;
    .restart local v3    # "messageString":Ljava/lang/String;
    :cond_3
    const-string v5, "SsdpReader"

    const-string v6, "Could not create SsdpMessage from the received string.  Ignoring."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDone()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/dial/SsdpReader;->mDone:Z

    .line 40
    return-void
.end method
