.class public Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;
.super Ljava/lang/Object;
.source "AlbumIdBitmapGetter.java"

# interfaces
.implements Lcom/google/android/music/leanback/bitmap/BitmapGetter;


# static fields
.field private static DEBUG:Z


# instance fields
.field private final mAlbumId:J

.field private mRemoteUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "albumId"    # J

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide p1, p0, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;->mAlbumId:J

    .line 24
    return-void
.end method


# virtual methods
.method public getBitmap(Landroid/content/Context;IIZ)Landroid/util/Pair;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "targetWidth"    # I
    .param p3, "targetHeight"    # I
    .param p4, "cropToSquare"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IIZ)",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 29
    iget-object v2, p0, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;->mRemoteUrl:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 30
    iget-wide v2, p0, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;->mAlbumId:J

    invoke-static {p1, v2, v3}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getRemoteArtLocationForAlbum(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;->mRemoteUrl:Ljava/lang/String;

    .line 31
    sget-boolean v2, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;->DEBUG:Z

    if-eqz v2, :cond_0

    const-string v2, "AlbumIdBitmapGetter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "got remoteUrl: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;->mRemoteUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;->mRemoteUrl:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 33
    const-string v2, ""

    iput-object v2, p0, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;->mRemoteUrl:Ljava/lang/String;

    .line 36
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;->mRemoteUrl:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 37
    new-instance v2, Landroid/util/Pair;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 52
    :goto_0
    return-object v2

    .line 40
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;->mRemoteUrl:Ljava/lang/String;

    .line 41
    .local v1, "url":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/utils/FifeImageUrlUtil;->isFifeHostedUrl(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 42
    if-le p2, p3, :cond_5

    move v0, p2

    .line 44
    .local v0, "size":I
    :goto_1
    :try_start_0
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/music/utils/FifeImageUrlUtil;->setImageUrlSize(ILandroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 45
    sget-boolean v2, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;->DEBUG:Z

    if-eqz v2, :cond_3

    const-string v2, "AlbumIdBitmapGetter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Convert fife url from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;->mRemoteUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/music/utils/FifeImageUrlUtil$InvalidUrlException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    .end local v0    # "size":I
    :cond_3
    :goto_2
    sget-boolean v2, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;->DEBUG:Z

    if-eqz v2, :cond_4

    const-string v2, "AlbumIdBitmapGetter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fetching album art from url: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    :cond_4
    new-instance v2, Landroid/util/Pair;

    invoke-static {p1, v1, p2, p3, v5}, Lcom/google/android/music/utils/AlbumArtUtils;->getRealNonAlbumArt(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_5
    move v0, p3

    .line 42
    goto :goto_1

    .line 46
    .restart local v0    # "size":I
    :catch_0
    move-exception v2

    goto :goto_2
.end method
