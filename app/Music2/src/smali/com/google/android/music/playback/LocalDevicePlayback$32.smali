.class Lcom/google/android/music/playback/LocalDevicePlayback$32;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->handleMediaButtonSeek(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

.field final synthetic val$force:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V
    .locals 0

    .prologue
    .line 3952
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iput-boolean p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->val$force:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 3956
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPendingMediaButtonSeekCount:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9900(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    move-result v6

    .line 3957
    .local v6, "count":I
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3958
    const-string v1, "LocalDevicePlayback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "apply pending media button seek events. count="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3961
    :cond_0
    if-nez v6, :cond_2

    .line 3962
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3963
    const-string v0, "LocalDevicePlayback"

    const-string v1, "All pending seek events are processed. This is a no-op."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 4018
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 4020
    :goto_0
    return-void

    .line 3967
    :cond_2
    if-gez v6, :cond_a

    .line 3968
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/SongList;->isFlagSet(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3971
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->position()J

    move-result-wide v2

    const-wide/16 v4, 0xbb8

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    const/4 v1, -0x1

    if-ne v6, v1, :cond_3

    .line 3972
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->seek(J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 4018
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 3976
    :cond_3
    :try_start_2
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->shouldPlayInRandomOrder()Z
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8300(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 3979
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8100(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;

    move-result-object v1

    monitor-enter v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3980
    :try_start_3
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mHistory:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8200(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v7

    .line 3981
    .local v7, "histsize":I
    if-nez v7, :cond_5

    .line 3983
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->fillShuffleList()V
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8500(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 3984
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8100(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-nez v2, :cond_4

    .line 3985
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4018
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 3987
    :cond_4
    :try_start_4
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mHistory:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8200(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8100(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 3988
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mHistory:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8200(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v7

    .line 3989
    if-nez v7, :cond_5

    .line 3990
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4018
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    .line 3993
    :cond_5
    :try_start_5
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v2

    if-ltz v2, :cond_6

    .line 3994
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8100(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 3996
    :cond_6
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mHistory:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8200(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;

    move-result-object v2

    add-int v3, v7, v6

    if-ltz v3, :cond_7

    add-int v0, v7, v6

    :cond_7
    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 3998
    .local v9, "pos":I
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v0, v9}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 3999
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->dumpPastPresentAndFuture()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8600(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 4000
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 4005
    .end local v7    # "histsize":I
    .end local v9    # "pos":I
    :goto_1
    :try_start_6
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->saveBookmarkIfNeeded()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8700(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 4006
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->stop(Z)V
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$10000(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    .line 4007
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndPrepareToPlaySync(ZZZZLcom/google/android/music/download/ContentIdentifier;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8800(Lcom/google/android/music/playback/LocalDevicePlayback;ZZZZLcom/google/android/music/download/ContentIdentifier;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 4018
    :cond_8
    :goto_2
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    .line 4000
    :catchall_0
    move-exception v0

    :try_start_7
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 4018
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    .line 4002
    :cond_9
    :try_start_9
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x1

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v4

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->getNextPlayPosition(ZZII)I
    invoke-static {v1, v2, v3, v4, v6}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9000(Lcom/google/android/music/playback/LocalDevicePlayback;ZZII)I

    move-result v1

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    goto :goto_1

    .line 4010
    :cond_a
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->val$force:Z

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v3

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->getNextPlayPosition(ZZII)I
    invoke-static {v0, v1, v2, v3, v6}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9000(Lcom/google/android/music/playback/LocalDevicePlayback;ZZII)I

    move-result v8

    .line 4013
    .local v8, "nextPlayPos":I
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    sget-object v1, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->NEXT:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->feedQueueIfNeeded(Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;I)Z
    invoke-static {v0, v1, v6}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$10100(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 4014
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$32;->val$force:Z

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->nextImplSync(ZI)V
    invoke-static {v0, v1, v8}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$10200(Lcom/google/android/music/playback/LocalDevicePlayback;ZI)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_2
.end method
