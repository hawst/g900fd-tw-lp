.class public Lcom/google/android/music/youtube/VideoSearchActivity;
.super Landroid/app/Activity;
.source "VideoSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;,
        Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;
    }
.end annotation


# instance fields
.field mAsyncTask:Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;

.field private mContext:Landroid/content/Context;

.field private mList:Landroid/widget/ListView;

.field mListContainer:Landroid/view/View;

.field mListShown:Z

.field mProgressContainer:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 108
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/youtube/VideoSearchActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/youtube/VideoSearchActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/youtube/VideoSearchActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/youtube/VideoSearchActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mList:Landroid/widget/ListView;

    return-object v0
.end method

.method private runQuery()V
    .locals 6

    .prologue
    .line 228
    invoke-direct {p0}, Lcom/google/android/music/youtube/VideoSearchActivity;->stopQueryIfRunning()V

    .line 230
    invoke-virtual {p0}, Lcom/google/android/music/youtube/VideoSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 231
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "trackName"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 232
    .local v2, "trackName":Ljava/lang/String;
    const-string v3, "artistName"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 233
    .local v0, "artistName":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00c4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 234
    const-string v0, ""

    .line 237
    :cond_0
    new-instance v3, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;-><init>(Lcom/google/android/music/youtube/VideoSearchActivity;Lcom/google/android/music/youtube/VideoSearchActivity$1;)V

    iput-object v3, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mAsyncTask:Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;

    .line 238
    iget-object v3, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mAsyncTask:Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-virtual {v3, v4}, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 239
    return-void
.end method

.method private stopQueryIfRunning()V
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mAsyncTask:Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mAsyncTask:Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;

    invoke-virtual {v0}, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mAsyncTask:Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->cancel(Z)Z

    .line 225
    :cond_0
    return-void
.end method


# virtual methods
.method makeListShown()V
    .locals 2

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mListShown:Z

    if-nez v0, :cond_0

    .line 208
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mListShown:Z

    .line 209
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mProgressContainer:Landroid/view/View;

    const v1, 0x10a0001

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 211
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mProgressContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 212
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mListContainer:Landroid/view/View;

    const/high16 v1, 0x10a0000

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mListContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 216
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 163
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 165
    iput-object p0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mContext:Landroid/content/Context;

    .line 167
    const v0, 0x7f040108

    invoke-virtual {p0, v0}, Lcom/google/android/music/youtube/VideoSearchActivity;->setContentView(I)V

    .line 169
    const v0, 0x7f0e01ab

    invoke-virtual {p0, v0}, Lcom/google/android/music/youtube/VideoSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mProgressContainer:Landroid/view/View;

    .line 170
    const v0, 0x7f0e01ac

    invoke-virtual {p0, v0}, Lcom/google/android/music/youtube/VideoSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mListContainer:Landroid/view/View;

    .line 172
    const v0, 0x7f0e0250

    invoke-virtual {p0, v0}, Lcom/google/android/music/youtube/VideoSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mList:Landroid/widget/ListView;

    .line 173
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity;->mList:Landroid/widget/ListView;

    new-instance v1, Lcom/google/android/music/youtube/VideoSearchActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/youtube/VideoSearchActivity$1;-><init>(Lcom/google/android/music/youtube/VideoSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 183
    invoke-virtual {p0}, Lcom/google/android/music/youtube/VideoSearchActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 185
    invoke-direct {p0}, Lcom/google/android/music/youtube/VideoSearchActivity;->runQuery()V

    .line 186
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 197
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 198
    invoke-direct {p0}, Lcom/google/android/music/youtube/VideoSearchActivity;->stopQueryIfRunning()V

    .line 199
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->onPause()V

    .line 200
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 190
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 191
    invoke-direct {p0}, Lcom/google/android/music/youtube/VideoSearchActivity;->runQuery()V

    .line 192
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->onResume()V

    .line 193
    return-void
.end method
