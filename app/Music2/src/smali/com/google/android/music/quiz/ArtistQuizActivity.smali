.class public Lcom/google/android/music/quiz/ArtistQuizActivity;
.super Lcom/google/android/music/quiz/QuizActivity;
.source "ArtistQuizActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;,
        Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;,
        Lcom/google/android/music/quiz/ArtistQuizActivity$OnArtistSelectedListner;,
        Lcom/google/android/music/quiz/ArtistQuizActivity$FinishQuizOnClickListener;,
        Lcom/google/android/music/quiz/ArtistQuizActivity$MoreArtistOnClickListener;
    }
.end annotation


# instance fields
.field private mAllArtistsResponseList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/cloudclient/QuizArtistJson;",
            ">;"
        }
    .end annotation
.end field

.field private mClientImpl:Lcom/google/android/music/cloudclient/MusicCloudImpl;

.field private mDefaultViewBgColor:I

.field private mOnCheckedChangeListener:Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mSelectedArtistIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedGenreIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mShowMoreButton:Landroid/widget/ImageButton;

.field private mShowMoreButtonContainer:Landroid/view/View;

.field private mShowMoreClicked:Z

.field private mSourceAdapter:Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;

.field private mUnselectedArtistIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mWrappedAdapter:Landroid/widget/ListAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/music/quiz/QuizActivity;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mShowMoreClicked:Z

    .line 431
    new-instance v0, Lcom/google/android/music/quiz/ArtistQuizActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/quiz/ArtistQuizActivity$3;-><init>(Lcom/google/android/music/quiz/ArtistQuizActivity;)V

    iput-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mOnCheckedChangeListener:Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mSelectedGenreIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/music/quiz/ArtistQuizActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mShowMoreClicked:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/music/quiz/ArtistQuizActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->fetchArtistList()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/music/quiz/ArtistQuizActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->getUserQuizResults()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mAllArtistsResponseList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/music/quiz/ArtistQuizActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mDefaultViewBgColor:I

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/music/quiz/ArtistQuizActivity;)Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mOnCheckedChangeListener:Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mSelectedArtistIds:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mUnselectedArtistIds:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/quiz/ArtistQuizActivity;)Lcom/google/android/music/cloudclient/MusicCloudImpl;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mClientImpl:Lcom/google/android/music/cloudclient/MusicCloudImpl;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/quiz/ArtistQuizActivity;Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;
    .param p1, "x1"    # Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/music/quiz/ArtistQuizActivity;->prefetchArt(Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/quiz/ArtistQuizActivity;Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;
    .param p1, "x1"    # Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/music/quiz/ArtistQuizActivity;->updateAdapter(Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/quiz/ArtistQuizActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/music/quiz/ArtistQuizActivity;->enableButtons(Z)V

    return-void
.end method

.method private enableButtons(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 448
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mShowMoreButtonContainer:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mShowMoreButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 450
    iget-boolean v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mShowMoreClicked:Z

    if-eqz v0, :cond_0

    .line 451
    iget-object v3, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mShowMoreButtonContainer:Landroid/view/View;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 452
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 457
    return-void

    :cond_1
    move v0, v2

    .line 451
    goto :goto_0

    :cond_2
    move v2, v1

    .line 452
    goto :goto_1
.end method

.method private fetchArtistList()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 137
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mClientImpl:Lcom/google/android/music/cloudclient/MusicCloudImpl;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mClientImpl:Lcom/google/android/music/cloudclient/MusicCloudImpl;

    .line 141
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mShowMoreClicked:Z

    if-nez v0, :cond_1

    .line 142
    invoke-virtual {p0, v1}, Lcom/google/android/music/quiz/ArtistQuizActivity;->setLoadingProgressVisibility(I)V

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mSourceAdapter:Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;

    if-eqz v0, :cond_2

    .line 146
    invoke-direct {p0, v1}, Lcom/google/android/music/quiz/ArtistQuizActivity;->enableButtons(Z)V

    .line 148
    :cond_2
    new-instance v0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/quiz/ArtistQuizActivity$1;-><init>(Lcom/google/android/music/quiz/ArtistQuizActivity;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 185
    return-void
.end method

.method private getUserQuizResults()V
    .locals 8

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "music_max_user_quiz_retries"

    const/4 v5, 0x3

    invoke-static {v1, v4, v5}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v4, 0x1

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 200
    .local v0, "maxRetries":I
    invoke-virtual {p0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "music_user_quiz_retry_delay_millisec"

    const-wide/16 v6, 0xfa

    invoke-static {v1, v4, v6, v7}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v2

    .line 204
    .local v2, "retryDelay":J
    iget-object v1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mClientImpl:Lcom/google/android/music/cloudclient/MusicCloudImpl;

    if-nez v1, :cond_0

    .line 205
    new-instance v1, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v1, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mClientImpl:Lcom/google/android/music/cloudclient/MusicCloudImpl;

    .line 208
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/music/quiz/ArtistQuizActivity;->enableButtons(Z)V

    .line 209
    new-instance v1, Lcom/google/android/music/quiz/ArtistQuizActivity$2;

    invoke-direct {v1, p0, v0, v2, v3}, Lcom/google/android/music/quiz/ArtistQuizActivity$2;-><init>(Lcom/google/android/music/quiz/ArtistQuizActivity;IJ)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 277
    return-void
.end method

.method private prefetchArt(Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;)V
    .locals 4
    .param p1, "responseJson"    # Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    .prologue
    .line 188
    invoke-static {p0}, Lcom/google/android/music/art/ArtResolver;->getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver;

    move-result-object v2

    .line 189
    .local v2, "resolver":Lcom/google/android/music/art/ArtResolver;
    iget-object v3, p1, Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;->mArtists:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/QuizArtistJson;

    .line 190
    .local v0, "artist":Lcom/google/android/music/cloudclient/QuizArtistJson;
    sget-object v3, Lcom/google/android/music/art/ArtType;->USER_QUIZ:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/music/art/ArtResolver;->prefetchArt(Lcom/google/android/music/cloudclient/QuizArtistJson;Lcom/google/android/music/art/ArtType;)V

    goto :goto_0

    .line 192
    .end local v0    # "artist":Lcom/google/android/music/cloudclient/QuizArtistJson;
    :cond_0
    return-void
.end method

.method public static startArtistQuiz(Landroid/app/Activity;Ljava/util/ArrayList;)V
    .locals 5
    .param p0, "hostActivity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 318
    .local p1, "selectedGenreIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "metajamIdDestination"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 320
    .local v1, "metajamIdDestination":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "playlistShareTokenDestination"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 323
    .local v2, "playlistShareTokenDestination":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 324
    .local v0, "i":Landroid/content/Intent;
    const-string v3, "selected_genres"

    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 325
    const-string v3, "metajamIdDestination"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 326
    const-string v3, "playlistShareTokenDestination"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 328
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 329
    return-void
.end method

.method private updateAdapter(Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;)V
    .locals 8
    .param p1, "response"    # Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    .prologue
    const/4 v7, 0x0

    .line 280
    iget-object v5, p1, Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;->mArtists:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/QuizArtistJson;

    .line 281
    .local v0, "artist":Lcom/google/android/music/cloudclient/QuizArtistJson;
    iget-object v5, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mAllArtistsResponseList:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 282
    iget-object v5, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mAllArtistsResponseList:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    iget-object v5, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mUnselectedArtistIds:Ljava/util/HashSet;

    iget-object v6, v0, Lcom/google/android/music/cloudclient/QuizArtistJson;->mArtistId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 288
    .end local v0    # "artist":Lcom/google/android/music/cloudclient/QuizArtistJson;
    :cond_1
    iget-object v5, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mSourceAdapter:Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;

    if-nez v5, :cond_2

    .line 289
    new-instance v5, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;

    invoke-direct {v5, p0, v7}, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;-><init>(Lcom/google/android/music/quiz/ArtistQuizActivity;Lcom/google/android/music/quiz/ArtistQuizActivity$1;)V

    iput-object v5, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mSourceAdapter:Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;

    .line 290
    invoke-virtual {p0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d0013

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 291
    .local v4, "numOfRows":I
    new-instance v5, Lcom/google/android/music/ui/GridAdapterWrapper;

    iget-object v6, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mSourceAdapter:Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;

    invoke-direct {v5, p0, v6, v4}, Lcom/google/android/music/ui/GridAdapterWrapper;-><init>(Landroid/content/Context;Landroid/widget/ListAdapter;I)V

    iput-object v5, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mWrappedAdapter:Landroid/widget/ListAdapter;

    .line 293
    invoke-virtual {p0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 294
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f0400d6

    invoke-virtual {v3, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 295
    .local v1, "footer":Landroid/view/ViewGroup;
    const v5, 0x7f0e0254

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mShowMoreButtonContainer:Landroid/view/View;

    .line 296
    const v5, 0x7f0e0255

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mShowMoreButton:Landroid/widget/ImageButton;

    .line 297
    iget-object v5, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mShowMoreButton:Landroid/widget/ImageButton;

    new-instance v6, Lcom/google/android/music/quiz/ArtistQuizActivity$MoreArtistOnClickListener;

    invoke-direct {v6, p0, v7}, Lcom/google/android/music/quiz/ArtistQuizActivity$MoreArtistOnClickListener;-><init>(Lcom/google/android/music/quiz/ArtistQuizActivity;Lcom/google/android/music/quiz/ArtistQuizActivity$1;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 298
    const v5, 0x7f0e0256

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ProgressBar;

    iput-object v5, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 299
    invoke-virtual {p0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->getListView()Landroid/widget/ListView;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v1, v7, v6}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 300
    iget-object v5, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mWrappedAdapter:Landroid/widget/ListAdapter;

    invoke-virtual {p0, v5}, Lcom/google/android/music/quiz/ArtistQuizActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 306
    .end local v1    # "footer":Landroid/view/ViewGroup;
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    .end local v4    # "numOfRows":I
    :goto_1
    return-void

    .line 303
    :cond_2
    iget-object v5, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mSourceAdapter:Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;

    invoke-virtual {v5}, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;->notifyDataSetChanged()V

    goto :goto_1
.end method


# virtual methods
.method public bridge synthetic isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Lcom/google/android/music/quiz/QuizActivity;->isActivityDestroyed()Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/google/android/music/quiz/QuizActivity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    const v2, 0x7f050007

    const v3, 0x7f050008

    invoke-virtual {p0, v2, v3}, Lcom/google/android/music/quiz/ArtistQuizActivity;->overridePendingTransition(II)V

    .line 81
    invoke-virtual {p0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "selected_genres"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mSelectedGenreIds:Ljava/util/ArrayList;

    .line 84
    :cond_0
    if-eqz p1, :cond_1

    .line 85
    const-string v2, "selected_artists"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 86
    .local v0, "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mSelectedArtistIds:Ljava/util/HashSet;

    .line 87
    const-string v2, "unselected_artists"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 89
    .local v1, "unselected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mUnselectedArtistIds:Ljava/util/HashSet;

    .line 90
    const-string v2, "allArtists"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mAllArtistsResponseList:Ljava/util/ArrayList;

    .line 97
    .end local v0    # "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v1    # "unselected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mDescription:Landroid/widget/TextView;

    const v3, 0x7f0b033b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 99
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mNextButton:Landroid/widget/Button;

    const v3, 0x7f0b033d

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(I)V

    .line 100
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mNextButton:Landroid/widget/Button;

    new-instance v3, Lcom/google/android/music/quiz/ArtistQuizActivity$FinishQuizOnClickListener;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/music/quiz/ArtistQuizActivity$FinishQuizOnClickListener;-><init>(Lcom/google/android/music/quiz/ArtistQuizActivity;Lcom/google/android/music/quiz/ArtistQuizActivity$1;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00b5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mDefaultViewBgColor:I

    .line 104
    invoke-direct {p0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->fetchArtistList()V

    .line 105
    return-void

    .line 92
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mAllArtistsResponseList:Ljava/util/ArrayList;

    .line 93
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mSelectedArtistIds:Ljava/util/HashSet;

    .line 94
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mUnselectedArtistIds:Ljava/util/HashSet;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 123
    invoke-super {p0, p1}, Lcom/google/android/music/quiz/QuizActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mSelectedArtistIds:Ljava/util/HashSet;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 125
    .local v0, "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, "selected_artists"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 126
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mUnselectedArtistIds:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 127
    .local v1, "unselected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, "unselected_artists"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 128
    const-string v2, "allArtists"

    iget-object v3, p0, Lcom/google/android/music/quiz/ArtistQuizActivity;->mAllArtistsResponseList:Ljava/util/ArrayList;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 129
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Lcom/google/android/music/quiz/QuizActivity;->onStart()V

    .line 110
    invoke-static {p0}, Lcom/google/android/music/art/ArtResolver;->getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver;

    move-result-object v0

    .line 111
    .local v0, "resolver":Lcom/google/android/music/art/ArtResolver;
    invoke-virtual {v0}, Lcom/google/android/music/art/ArtResolver;->requestMemoryCaching()V

    .line 112
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 116
    invoke-static {p0}, Lcom/google/android/music/art/ArtResolver;->getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver;

    move-result-object v0

    .line 117
    .local v0, "resolver":Lcom/google/android/music/art/ArtResolver;
    invoke-virtual {v0}, Lcom/google/android/music/art/ArtResolver;->releaseMemoryCaching()V

    .line 118
    invoke-super {p0}, Lcom/google/android/music/quiz/QuizActivity;->onStop()V

    .line 119
    return-void
.end method
