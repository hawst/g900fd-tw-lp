.class Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;
.super Ljava/lang/Object;
.source "RemoteAsyncMediaPlayer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 202
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;
    invoke-static {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$200(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/CastTokenClient;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v3, v3, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-static {v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$100(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getProvider()Landroid/support/v7/media/MediaRouter$ProviderInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/media/MediaRouter$ProviderInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v4, v4, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-static {v4}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$100(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/music/cast/CastTokenClient;->getCastToken(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "castToken":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 205
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const/4 v3, 0x1

    # setter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mFetchedCastToken:Z
    invoke-static {v2, v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$302(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)Z

    .line 210
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 211
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;
    invoke-static {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$700(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_CAST_TOKEN:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-ne v2, v3, :cond_0

    .line 212
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mFetchedCastToken:Z
    invoke-static {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$300(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 216
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 217
    .local v1, "message":Landroid/os/Message;
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 231
    .end local v1    # "message":Landroid/os/Message;
    :cond_0
    :goto_1
    return-void

    .line 207
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const-string v3, "Failed to create cast token. Retrying"

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$400(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)V

    .line 208
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # operator++ for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNumberOfRetries:I
    invoke-static {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$508(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)I

    goto :goto_0

    .line 218
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNumberOfRetries:I
    invoke-static {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$500(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)I

    move-result v2

    const/4 v3, 0x5

    if-ge v2, v3, :cond_3

    .line 221
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 222
    .restart local v1    # "message":Landroid/os/Message;
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Landroid/os/Handler;

    move-result-object v2

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    .line 225
    .end local v1    # "message":Landroid/os/Message;
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const-string v3, "Failed to fetch cast token.  Aborting playback."

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$400(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)V

    .line 226
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    sget-object v3, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V
    invoke-static {v2, v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 227
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;->this$1:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const/4 v3, 0x0

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V
    invoke-static {v2, v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$900(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)V

    goto :goto_1
.end method
