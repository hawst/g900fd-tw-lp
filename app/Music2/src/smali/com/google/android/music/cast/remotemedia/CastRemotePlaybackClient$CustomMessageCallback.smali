.class Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;
.super Ljava/lang/Object;
.source "CastRemotePlaybackClient.java"

# interfaces
.implements Lcom/google/android/gms/cast/Cast$MessageReceivedCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomMessageCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;


# direct methods
.method private constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V
    .locals 0

    .prologue
    .line 966
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
    .param p2, "x1"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;

    .prologue
    .line 966
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V

    return-void
.end method


# virtual methods
.method public getNamespace()Ljava/lang/String;
    .locals 1

    .prologue
    .line 969
    const-string v0, "urn:x-cast:com.google.android.music.cloudqueue"

    return-object v0
.end method

.method public onMessageReceived(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1, "castDevice"    # Lcom/google/android/gms/cast/CastDevice;
    .param p2, "namespace"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    .line 974
    iget-object v7, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v7}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 975
    const-string v7, "MusicCastRemote"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Custom message received: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    :cond_0
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 982
    .local v4, "jsonMessage":Lorg/json/JSONObject;
    const-string v7, "event"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 983
    .local v1, "event":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v7}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 984
    const-string v7, "MusicCastRemote"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Received message from receiver: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 991
    :cond_1
    const-string v7, "sessionStatus"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 993
    const/4 v5, 0x0

    .line 995
    .local v5, "sessionCreated":Z
    :try_start_1
    const-string v7, "sessionState"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 997
    .local v6, "sessionState":I
    if-nez v6, :cond_2

    .line 998
    const-string v7, "sessionCreated"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    .line 1006
    :cond_2
    packed-switch v6, :pswitch_data_0

    .line 1059
    const-string v7, "MusicCastRemote"

    const-string v8, "Unexpected sessionStatus state: %d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084
    .end local v1    # "event":Ljava/lang/String;
    .end local v4    # "jsonMessage":Lorg/json/JSONObject;
    .end local v5    # "sessionCreated":Z
    .end local v6    # "sessionState":I
    :cond_3
    :goto_0
    return-void

    .line 986
    :catch_0
    move-exception v0

    .line 987
    .local v0, "e":Lorg/json/JSONException;
    const-string v7, "MusicCastRemote"

    const-string v8, "Could not parse JSON message from receiver"

    invoke-static {v7, v8, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1001
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v1    # "event":Ljava/lang/String;
    .restart local v4    # "jsonMessage":Lorg/json/JSONObject;
    .restart local v5    # "sessionCreated":Z
    :catch_1
    move-exception v0

    .line 1002
    .restart local v0    # "e":Lorg/json/JSONException;
    const-string v7, "MusicCastRemote"

    const-string v8, "Could not parse sessionStatus JSON from receiver"

    invoke-static {v7, v8, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1008
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v6    # "sessionState":I
    :pswitch_0
    if-eqz v5, :cond_4

    .line 1009
    new-instance v7, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback$1;

    invoke-direct {v7, p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback$1;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;)V

    invoke-static {v7}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 1046
    :goto_1
    iget-object v7, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;
    invoke-static {v7}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1800(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/gms/cast/RemoteMediaPlayer;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;
    invoke-static {v8}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1200(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->requestStatus(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    goto :goto_0

    .line 1028
    :cond_4
    new-instance v7, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback$2;

    invoke-direct {v7, p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback$2;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;)V

    invoke-static {v7}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto :goto_1

    .line 1052
    :pswitch_1
    iget-object v7, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v7}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1053
    const-string v7, "MusicCastRemote"

    const-string v8, "Evicting sender"

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    :cond_5
    iget-object v7, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v7}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyMediaRouteInvalidated()V

    goto :goto_0

    .line 1063
    .end local v5    # "sessionCreated":Z
    .end local v6    # "sessionState":I
    :cond_6
    const-string v7, "Error"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1065
    :try_start_2
    const-string v7, "httpStatus"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1066
    .local v3, "httpStatus":I
    const-string v7, "MusicCastRemote"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Receiver encountered an error with HTTP status: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    const/16 v7, 0x191

    if-ne v3, v7, :cond_7

    .line 1068
    iget-object v7, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    const/4 v8, 0x0

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z
    invoke-static {v7, v8}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3

    .line 1075
    .end local v3    # "httpStatus":I
    :cond_7
    :goto_2
    :try_start_3
    const-string v7, "httpHeaders"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 1077
    .local v2, "httpHeaders":Lorg/json/JSONObject;
    const-string v7, "MusicCastRemote"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "HTTP response headers from receiver: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 1078
    .end local v2    # "httpHeaders":Lorg/json/JSONObject;
    :catch_2
    move-exception v7

    goto/16 :goto_0

    .line 1070
    :catch_3
    move-exception v7

    goto :goto_2

    .line 1006
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
