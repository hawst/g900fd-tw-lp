.class public Lcom/google/android/music/utils/MusicUtils;
.super Ljava/lang/Object;
.source "MusicUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/utils/MusicUtils$LogEntry;,
        Lcom/google/android/music/utils/MusicUtils$QueryCallback;,
        Lcom/google/android/music/utils/MusicUtils$ServiceBinder;,
        Lcom/google/android/music/utils/MusicUtils$ServiceToken;
    }
.end annotation


# static fields
.field private static final NAG_ABOUT_QUERIES_ON_THE_MAIN_THREAD:Z

.field private static sAsyncQueryWorker:Lcom/google/android/music/utils/LoggableHandler;

.field private static sConnectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Lcom/google/android/music/utils/MusicUtils$ServiceBinder;",
            ">;"
        }
    .end annotation
.end field

.field private static sFormatBuilder:Ljava/lang/StringBuilder;

.field private static sFormatter:Ljava/util/Formatter;

.field private static sLastLogEntry:Lcom/google/android/music/utils/MusicUtils$LogEntry;

.field private static sLogPtr:I

.field private static sMusicLog:[Lcom/google/android/music/utils/MusicUtils$LogEntry;

.field private static sOrientationSwitchCount:I

.field static sPlaylistObserver:Landroid/database/ContentObserver;

.field private static sPlaylists:Lcom/google/android/music/AsyncCursor;

.field private static sProcessName:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sProcessNameRetrieved:Z

.field public static sService:Lcom/google/android/music/playback/IMusicPlaybackService;

.field private static final sTimeArgs:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 121
    sget-boolean v0, Lcom/google/android/music/utils/DebugUtils;->IS_DEBUG_BUILD:Z

    sput-boolean v0, Lcom/google/android/music/utils/MusicUtils;->NAG_ABOUT_QUERIES_ON_THE_MAIN_THREAD:Z

    .line 257
    sput-object v4, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 258
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/utils/MusicUtils;->sConnectionMap:Ljava/util/HashMap;

    .line 936
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sUIBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    sput-object v0, Lcom/google/android/music/utils/MusicUtils;->sAsyncQueryWorker:Lcom/google/android/music/utils/LoggableHandler;

    .line 1081
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lcom/google/android/music/utils/MusicUtils;->sFormatBuilder:Ljava/lang/StringBuilder;

    .line 1082
    new-instance v0, Ljava/util/Formatter;

    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sFormatBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    sput-object v0, Lcom/google/android/music/utils/MusicUtils;->sFormatter:Ljava/util/Formatter;

    .line 1083
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lcom/google/android/music/utils/MusicUtils;->sTimeArgs:[Ljava/lang/Object;

    .line 1788
    const/16 v0, 0xc8

    new-array v0, v0, [Lcom/google/android/music/utils/MusicUtils$LogEntry;

    sput-object v0, Lcom/google/android/music/utils/MusicUtils;->sMusicLog:[Lcom/google/android/music/utils/MusicUtils$LogEntry;

    .line 1789
    sput v3, Lcom/google/android/music/utils/MusicUtils;->sLogPtr:I

    .line 1892
    sput v3, Lcom/google/android/music/utils/MusicUtils;->sOrientationSwitchCount:I

    .line 1982
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/google/android/music/utils/MusicUtils;->sProcessName:Ljava/util/concurrent/atomic/AtomicReference;

    .line 1983
    sput-boolean v3, Lcom/google/android/music/utils/MusicUtils;->sProcessNameRetrieved:Z

    return-void
.end method

.method static synthetic access$000()Lcom/google/android/music/AsyncCursor;
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lcom/google/android/music/utils/MusicUtils;->sPlaylists:Lcom/google/android/music/AsyncCursor;

    return-object v0
.end method

.method public static areUpstreamTrackDeletesEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2123
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_enable_tracks_upsync_deletion"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static assertMainProcess(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 2017
    sget-boolean v1, Lcom/google/android/music/utils/DebugUtils;->IS_DEBUG_BUILD:Z

    if-eqz v1, :cond_0

    .line 2018
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->isMainProcess(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2019
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->getProcessName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2020
    .local v0, "processName":Ljava/lang/String;
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Expected main process. Got: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2024
    .end local v0    # "processName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static assertMainThread()V
    .locals 2

    .prologue
    .line 1932
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->isMainThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1933
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "not in main thread!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1935
    :cond_0
    return-void
.end method

.method public static assertNotMainThread()V
    .locals 2

    .prologue
    .line 1938
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1939
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "On in main thread!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1941
    :cond_0
    return-void
.end method

.method public static assertUiProcess(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 2027
    sget-boolean v1, Lcom/google/android/music/utils/DebugUtils;->IS_DEBUG_BUILD:Z

    if-eqz v1, :cond_0

    .line 2028
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->isUIProcess(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2029
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->getProcessName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2030
    .local v0, "processName":Ljava/lang/String;
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Expected UI process. Got: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2034
    .end local v0    # "processName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static assertUiThread()V
    .locals 2

    .prologue
    .line 1925
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->isMainThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1926
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "not in UI thread!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1928
    :cond_0
    return-void
.end method

.method public static bindToService(Landroid/app/Activity;Landroid/content/ServiceConnection;)Lcom/google/android/music/utils/MusicUtils$ServiceToken;
    .locals 6
    .param p0, "context"    # Landroid/app/Activity;
    .param p1, "callback"    # Landroid/content/ServiceConnection;

    .prologue
    .line 275
    const/4 v0, 0x0

    .line 276
    .local v0, "current":Landroid/app/Activity;
    move-object v2, p0

    .line 277
    .local v2, "parent":Landroid/app/Activity;
    :goto_0
    if-eqz v2, :cond_0

    .line 278
    move-object v0, v2

    .line 279
    invoke-virtual {v2}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object v2

    goto :goto_0

    .line 281
    :cond_0
    new-instance v1, Landroid/content/ContextWrapper;

    invoke-direct {v1, v0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 282
    .local v1, "cw":Landroid/content/ContextWrapper;
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-direct {v4, v1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v4}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 283
    new-instance v3, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;

    invoke-direct {v3, p1}, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;-><init>(Landroid/content/ServiceConnection;)V

    .line 284
    .local v3, "sb":Lcom/google/android/music/utils/MusicUtils$ServiceBinder;
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-class v5, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v4, v1, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v3, v5}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 285
    sget-object v4, Lcom/google/android/music/utils/MusicUtils;->sConnectionMap:Ljava/util/HashMap;

    invoke-virtual {v4, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    new-instance v4, Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    invoke-direct {v4, v1}, Lcom/google/android/music/utils/MusicUtils$ServiceToken;-><init>(Landroid/content/ContextWrapper;)V

    .line 289
    :goto_1
    return-object v4

    .line 288
    :cond_1
    const-string v4, "Music"

    const-string v5, "Failed to bind to service"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Lcom/google/android/music/utils/MusicUtils$ServiceToken;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "callback"    # Landroid/content/ServiceConnection;

    .prologue
    .line 293
    new-instance v0, Landroid/content/ContextWrapper;

    invoke-direct {v0, p0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 294
    .local v0, "cw":Landroid/content/ContextWrapper;
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 295
    new-instance v1, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;

    invoke-direct {v1, p1}, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;-><init>(Landroid/content/ServiceConnection;)V

    .line 296
    .local v1, "sb":Lcom/google/android/music/utils/MusicUtils$ServiceBinder;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-class v3, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 297
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sConnectionMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    new-instance v2, Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    invoke-direct {v2, v0}, Lcom/google/android/music/utils/MusicUtils$ServiceToken;-><init>(Landroid/content/ContextWrapper;)V

    .line 301
    :goto_0
    return-object v2

    .line 300
    :cond_0
    const-string v2, "Music"

    const-string v3, "Failed to bind to service"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static buildUriWithAccountName(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 2158
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2159
    .local v1, "uri":Landroid/net/Uri;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2164
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_0
    return-object v1

    .line 2162
    .restart local v1    # "uri":Landroid/net/Uri;
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2163
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v2, "authuser"

    invoke-virtual {v0, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2164
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public static canStartImFeelingLucky(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2318
    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    .line 2319
    .local v3, "refObject":Ljava/lang/Object;
    invoke-static {p0, v3}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    .line 2321
    .local v2, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isStreamOnlyOnWifi()Z

    move-result v1

    .line 2322
    .local v1, "isStreamOnWifiOnly":Z
    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v0

    .line 2323
    .local v0, "isDownloadedOnlyMode":Z
    invoke-static {p0, v1, v0}, Lcom/google/android/music/utils/MusicUtils;->canStartImFeelingLucky(Landroid/content/Context;ZZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    .line 2325
    invoke-static {v3}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return v4

    .end local v0    # "isDownloadedOnlyMode":Z
    .end local v1    # "isStreamOnWifiOnly":Z
    :catchall_0
    move-exception v4

    invoke-static {v3}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v4
.end method

.method public static canStartImFeelingLucky(Landroid/content/Context;ZZ)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isStreamOnWifiOnly"    # Z
    .param p2, "isDownloadedOnlyMode"    # Z

    .prologue
    .line 2341
    invoke-static {p0, p1}, Lcom/google/android/music/download/DownloadUtils;->isStreamingAvailable(Landroid/content/Context;Z)Z

    move-result v0

    .line 2342
    .local v0, "isStreamingAvailable":Z
    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static checkMainThread(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 1034
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .line 1035
    .local v0, "looper":Landroid/os/Looper;
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 1036
    sget-boolean v1, Lcom/google/android/music/utils/MusicUtils;->NAG_ABOUT_QUERIES_ON_THE_MAIN_THREAD:Z

    if-eqz v1, :cond_0

    .line 1037
    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1040
    :cond_0
    const-string v1, "MusicUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "(NOT A CRASHING EXCEPTION, strictly logging): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-static {v1, v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1042
    :cond_1
    return-void
.end method

.method public static clearPlayQueue()V
    .locals 3

    .prologue
    .line 705
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 707
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->clearQueue()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 712
    .local v0, "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void

    .line 708
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 709
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v1, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static clearPlayQueue(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 687
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 688
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.android.music.clearqueue"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 689
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 690
    return-void
.end method

.method public static closePlaylistCursor()V
    .locals 1

    .prologue
    .line 880
    sget-object v0, Lcom/google/android/music/utils/MusicUtils;->sPlaylists:Lcom/google/android/music/AsyncCursor;

    if-eqz v0, :cond_0

    .line 881
    sget-object v0, Lcom/google/android/music/utils/MusicUtils;->sPlaylists:Lcom/google/android/music/AsyncCursor;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncCursor;->close()V

    .line 883
    :cond_0
    return-void
.end method

.method public static declared-synchronized debugDump(Ljava/io/PrintWriter;)V
    .locals 10
    .param p0, "out"    # Ljava/io/PrintWriter;

    .prologue
    .line 1809
    const-class v6, Lcom/google/android/music/utils/MusicUtils;

    monitor-enter v6

    :try_start_0
    new-instance v4, Ljava/sql/Time;

    const-wide/16 v8, 0x0

    invoke-direct {v4, v8, v9}, Ljava/sql/Time;-><init>(J)V

    .line 1810
    .local v4, "time":Ljava/sql/Time;
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v5, "dd-MM hh:mm:ss.SSS a"

    invoke-direct {v1, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1811
    .local v1, "format":Ljava/text/DateFormat;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v5, Lcom/google/android/music/utils/MusicUtils;->sMusicLog:[Lcom/google/android/music/utils/MusicUtils$LogEntry;

    array-length v5, v5

    if-ge v2, v5, :cond_2

    .line 1812
    sget v5, Lcom/google/android/music/utils/MusicUtils;->sLogPtr:I

    add-int v3, v5, v2

    .line 1813
    .local v3, "idx":I
    sget-object v5, Lcom/google/android/music/utils/MusicUtils;->sMusicLog:[Lcom/google/android/music/utils/MusicUtils$LogEntry;

    array-length v5, v5

    if-lt v3, v5, :cond_0

    .line 1814
    sget-object v5, Lcom/google/android/music/utils/MusicUtils;->sMusicLog:[Lcom/google/android/music/utils/MusicUtils$LogEntry;

    array-length v5, v5

    sub-int/2addr v3, v5

    .line 1816
    :cond_0
    sget-object v5, Lcom/google/android/music/utils/MusicUtils;->sMusicLog:[Lcom/google/android/music/utils/MusicUtils$LogEntry;

    aget-object v0, v5, v3

    .line 1817
    .local v0, "entry":Lcom/google/android/music/utils/MusicUtils$LogEntry;
    if-eqz v0, :cond_1

    .line 1818
    invoke-virtual {v0, p0, v4, v1}, Lcom/google/android/music/utils/MusicUtils$LogEntry;->dump(Ljava/io/PrintWriter;Ljava/sql/Time;Ljava/text/DateFormat;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1811
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1821
    .end local v0    # "entry":Lcom/google/android/music/utils/MusicUtils$LogEntry;
    .end local v3    # "idx":I
    :cond_2
    monitor-exit v6

    return-void

    .line 1809
    .end local v1    # "format":Ljava/text/DateFormat;
    .end local v2    # "i":I
    .end local v4    # "time":Ljava/sql/Time;
    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5
.end method

.method public static declared-synchronized debugLog(Ljava/lang/Object;)V
    .locals 10
    .param p0, "o"    # Ljava/lang/Object;

    .prologue
    .line 1793
    const-class v2, Lcom/google/android/music/utils/MusicUtils;

    monitor-enter v2

    :try_start_0
    new-instance v0, Lcom/google/android/music/utils/MusicUtils$LogEntry;

    invoke-direct {v0, p0}, Lcom/google/android/music/utils/MusicUtils$LogEntry;-><init>(Ljava/lang/Object;)V

    .line 1794
    .local v0, "newEntry":Lcom/google/android/music/utils/MusicUtils$LogEntry;
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sLastLogEntry:Lcom/google/android/music/utils/MusicUtils$LogEntry;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sLastLogEntry:Lcom/google/android/music/utils/MusicUtils$LogEntry;

    iget-object v1, v1, Lcom/google/android/music/utils/MusicUtils$LogEntry;->item:Ljava/lang/Object;

    iget-object v3, v0, Lcom/google/android/music/utils/MusicUtils$LogEntry;->item:Ljava/lang/Object;

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-wide v4, v0, Lcom/google/android/music/utils/MusicUtils$LogEntry;->time:J

    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sLastLogEntry:Lcom/google/android/music/utils/MusicUtils$LogEntry;

    iget-wide v6, v1, Lcom/google/android/music/utils/MusicUtils$LogEntry;->time:J

    const-wide/16 v8, 0x3e8

    add-long/2addr v6, v8

    cmp-long v1, v4, v6

    if-gez v1, :cond_1

    .line 1796
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sLastLogEntry:Lcom/google/android/music/utils/MusicUtils$LogEntry;

    iget-wide v4, v1, Lcom/google/android/music/utils/MusicUtils$LogEntry;->count:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v1, Lcom/google/android/music/utils/MusicUtils$LogEntry;->count:J

    .line 1797
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sLastLogEntry:Lcom/google/android/music/utils/MusicUtils$LogEntry;

    iget-wide v4, v0, Lcom/google/android/music/utils/MusicUtils$LogEntry;->time:J

    iput-wide v4, v1, Lcom/google/android/music/utils/MusicUtils$LogEntry;->time:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1806
    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    .line 1800
    :cond_1
    :try_start_1
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sMusicLog:[Lcom/google/android/music/utils/MusicUtils$LogEntry;

    sget v3, Lcom/google/android/music/utils/MusicUtils;->sLogPtr:I

    aput-object v0, v1, v3

    .line 1801
    sput-object v0, Lcom/google/android/music/utils/MusicUtils;->sLastLogEntry:Lcom/google/android/music/utils/MusicUtils$LogEntry;

    .line 1802
    sget v1, Lcom/google/android/music/utils/MusicUtils;->sLogPtr:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/google/android/music/utils/MusicUtils;->sLogPtr:I

    .line 1803
    sget v1, Lcom/google/android/music/utils/MusicUtils;->sLogPtr:I

    sget-object v3, Lcom/google/android/music/utils/MusicUtils;->sMusicLog:[Lcom/google/android/music/utils/MusicUtils$LogEntry;

    array-length v3, v3

    if-lt v1, v3, :cond_0

    .line 1804
    const/4 v1, 0x0

    sput v1, Lcom/google/android/music/utils/MusicUtils;->sLogPtr:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1793
    .end local v0    # "newEntry":Lcom/google/android/music/utils/MusicUtils$LogEntry;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p0, "encoded"    # Ljava/lang/String;

    .prologue
    .line 2252
    if-eqz p0, :cond_0

    const-string v0, "<null>"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2253
    :cond_0
    const/4 v0, 0x0

    .line 2257
    :goto_0
    return-object v0

    .line 2254
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 2255
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_0

    .line 2257
    :cond_2
    const/16 v0, 0x1f

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static encodeStringArray([Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "strings"    # [Ljava/lang/String;

    .prologue
    .line 2237
    if-nez p0, :cond_0

    .line 2238
    const-string v0, "<null>"

    .line 2242
    :goto_0
    return-object v0

    .line 2239
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 2240
    const-string v0, ""

    goto :goto_0

    .line 2242
    :cond_1
    const/16 v0, 0x1f

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getArtUrlsForGenreRadio(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "genreId"    # Ljava/lang/String;
    .param p2, "parentGenreId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1473
    invoke-static {p2, p1}, Lcom/google/android/music/store/MusicContent$Explore;->getGenresUriWithSubgenre(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1474
    .local v1, "queryUri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "genreArtUris"

    aput-object v0, v2, v3

    .line 1477
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 1478
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 1480
    .local v7, "result":Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    :try_start_0
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1481
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1482
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 1485
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1487
    return-object v7

    .line 1485
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static getArtistArtUrl(Landroid/content/Context;J)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistid"    # J

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2209
    const/4 v7, 0x0

    .line 2211
    .local v7, "result":Ljava/lang/String;
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 2212
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "artworkUrl"

    aput-object v0, v2, v4

    .line 2215
    .local v2, "cols":[Ljava/lang/String;
    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2218
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 2220
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2221
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 2224
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 2229
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_1
    return-object v7

    .line 2224
    .restart local v2    # "cols":[Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static getAsyncHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 939
    sget-object v0, Lcom/google/android/music/utils/MusicUtils;->sAsyncQueryWorker:Lcom/google/android/music/utils/LoggableHandler;

    return-object v0
.end method

.method public static getAudioSessionId()I
    .locals 3

    .prologue
    .line 749
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 751
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->getAudioSessionId()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 756
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return v1

    .line 752
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 753
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v1, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 756
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public static getAvailableAccounts(Landroid/content/Context;)[Landroid/accounts/Account;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2289
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 2290
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v7, "com.google"

    invoke-virtual {v1, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 2291
    .local v2, "accounts":[Landroid/accounts/Account;
    if-eqz v2, :cond_2

    array-length v7, v2

    if-eqz v7, :cond_2

    .line 2293
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 2294
    .local v6, "purgedAccounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    move-object v3, v2

    .local v3, "arr$":[Landroid/accounts/Account;
    array-length v5, v3

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v0, v3, v4

    .line 2295
    .local v0, "account":Landroid/accounts/Account;
    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v8, "@youtube.com"

    invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 2296
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2294
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2299
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    array-length v8, v2

    if-eq v7, v8, :cond_2

    .line 2300
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_3

    .line 2301
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v7, v7, [Landroid/accounts/Account;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "accounts":[Landroid/accounts/Account;
    check-cast v2, [Landroid/accounts/Account;

    .line 2307
    .end local v3    # "arr$":[Landroid/accounts/Account;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "purgedAccounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    :cond_2
    :goto_1
    return-object v2

    .line 2303
    .restart local v3    # "arr$":[Landroid/accounts/Account;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    .restart local v6    # "purgedAccounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static getCurrentAudioId()Lcom/google/android/music/download/ContentIdentifier;
    .locals 3

    .prologue
    .line 460
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 462
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->getAudioId()Lcom/google/android/music/download/ContentIdentifier;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 467
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-object v1

    .line 463
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 464
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v1, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 467
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getCurrentShuffleMode()I
    .locals 5

    .prologue
    .line 471
    const/4 v1, 0x0

    .line 472
    .local v1, "mode":I
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 473
    .local v2, "service":Lcom/google/android/music/playback/IMusicPlaybackService;
    if-eqz v2, :cond_0

    .line 475
    :try_start_0
    invoke-interface {v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->getShuffleMode()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 480
    :cond_0
    :goto_0
    return v1

    .line 476
    :catch_0
    move-exception v0

    .line 477
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static getCurrentTrackInfo()Lcom/google/android/music/playback/TrackInfo;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1649
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v2, :cond_0

    .line 1650
    const-string v2, "MusicUtils"

    const-string v3, "service not ready"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1657
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-object v1

    .line 1654
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :try_start_0
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->getCurrentTrackInfo()Lcom/google/android/music/playback/TrackInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 1655
    :catch_0
    move-exception v0

    .line 1656
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v2, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static getDocument(Landroid/view/View;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 2108
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 2109
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    if-nez v0, :cond_0

    .line 2110
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 2111
    .restart local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {p0, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2113
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 2114
    return-object v0
.end method

.method private static getFirstTrackMetajamIdAndArt(Landroid/content/Context;Lcom/google/android/music/medialist/NautilusSongList;)Landroid/util/Pair;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nautilusList"    # Lcom/google/android/music/medialist/NautilusSongList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/music/medialist/NautilusSongList;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 1213
    const/4 v7, 0x0

    .line 1215
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p1, p0}, Lcom/google/android/music/medialist/NautilusSongList;->getFullContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "Nid"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "artworkUrl"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1219
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1220
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1221
    .local v8, "metajamId":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1222
    .local v6, "artworkUrl":Ljava/lang/String;
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1223
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1225
    :cond_0
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v8, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1228
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1231
    .end local v6    # "artworkUrl":Ljava/lang/String;
    .end local v8    # "metajamId":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 1228
    :cond_1
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move-object v0, v9

    .line 1231
    goto :goto_0

    .line 1228
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static getFirstTrackMusicIdAndArt(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/util/Pair;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songlist"    # Lcom/google/android/music/medialist/SongList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/music/medialist/SongList;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 1189
    const/4 v7, 0x0

    .line 1191
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p1, p0}, Lcom/google/android/music/medialist/SongList;->getFullContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "audio_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "artworkUrl"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1195
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1196
    const/4 v6, 0x0

    .line 1197
    .local v6, "artUrl":Ljava/lang/String;
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1198
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1200
    :cond_0
    new-instance v0, Landroid/util/Pair;

    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1203
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1205
    .end local v6    # "artUrl":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 1203
    :cond_1
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move-object v0, v8

    .line 1205
    goto :goto_0

    .line 1203
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static getNautilusArtistArtUrl(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nautilusArtistId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2182
    const/4 v7, 0x0

    .line 2184
    .local v7, "result":Ljava/lang/String;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2185
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "artworkUrl"

    aput-object v0, v2, v1

    .line 2188
    .local v2, "cols":[Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistArtUrlUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2191
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 2193
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2194
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 2197
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 2201
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_1
    return-object v7

    .line 2197
    .restart local v2    # "cols":[Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static getNowPlayingList()Lcom/google/android/music/medialist/SongList;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1620
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v2, :cond_0

    .line 1621
    const-string v2, "MusicUtils"

    const-string v3, "service not ready"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1628
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-object v1

    .line 1625
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :try_start_0
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->getMediaList()Lcom/google/android/music/medialist/SongList;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 1626
    :catch_0
    move-exception v0

    .line 1627
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v2, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static getPlaybackState()Lcom/google/android/music/playback/PlaybackState;
    .locals 4

    .prologue
    .line 571
    const/4 v1, 0x0

    .line 572
    .local v1, "state":Lcom/google/android/music/playback/PlaybackState;
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v2, :cond_0

    .line 574
    :try_start_0
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 579
    :cond_0
    :goto_0
    return-object v1

    .line 575
    :catch_0
    move-exception v0

    .line 576
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static getProcessName(Landroid/content/Context;)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1986
    sget-object v6, Lcom/google/android/music/utils/MusicUtils;->sProcessName:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v6

    .line 1987
    :try_start_0
    sget-boolean v5, Lcom/google/android/music/utils/MusicUtils;->sProcessNameRetrieved:Z

    if-eqz v5, :cond_0

    .line 1988
    sget-object v5, Lcom/google/android/music/utils/MusicUtils;->sProcessName:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    monitor-exit v6

    .line 2002
    :goto_0
    return-object v5

    .line 1990
    :cond_0
    const/4 v5, 0x1

    sput-boolean v5, Lcom/google/android/music/utils/MusicUtils;->sProcessNameRetrieved:Z

    .line 1992
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    .line 1993
    .local v3, "mypid":I
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1994
    .local v0, "activityManger":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    .line 1996
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 1997
    .local v4, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v5, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v5, v3, :cond_1

    .line 1998
    sget-object v5, Lcom/google/android/music/utils/MusicUtils;->sProcessName:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v7, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 1999
    sget-object v5, Lcom/google/android/music/utils/MusicUtils;->sProcessName:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    monitor-exit v6

    goto :goto_0

    .line 2003
    .end local v0    # "activityManger":Landroid/app/ActivityManager;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    .end local v3    # "mypid":I
    .end local v4    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 2002
    .restart local v0    # "activityManger":Landroid/app/ActivityManager;
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    .restart local v3    # "mypid":I
    :cond_2
    const/4 v5, 0x0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static getQueuePosition()I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 1636
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v2, :cond_0

    .line 1637
    const-string v2, "MusicUtils"

    const-string v3, "service not ready"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1644
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return v1

    .line 1641
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :try_start_0
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->getQueuePosition()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1642
    :catch_0
    move-exception v0

    .line 1643
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v2, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static getRefreshDelay(Landroid/view/View;)J
    .locals 8
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 1960
    if-nez p0, :cond_0

    .line 1961
    const-wide v4, 0x7fffffffffffffffL

    .line 1977
    :goto_0
    return-wide v4

    .line 1963
    :cond_0
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 1964
    .local v2, "dims":Landroid/graphics/Rect;
    invoke-virtual {p0, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1965
    const-wide/32 v4, 0x249f0

    iget v3, v2, Landroid/graphics/Rect;->right:I

    iget v6, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v6

    int-to-long v6, v3

    div-long v0, v4, v6

    .line 1966
    .local v0, "delay":J
    const-wide/16 v4, 0x7d

    cmp-long v3, v0, v4

    if-gez v3, :cond_1

    .line 1967
    const-wide/16 v4, 0x64

    goto :goto_0

    .line 1968
    :cond_1
    const-wide/16 v4, 0xc8

    cmp-long v3, v0, v4

    if-gez v3, :cond_2

    .line 1969
    const-wide/16 v4, 0x7d

    goto :goto_0

    .line 1970
    :cond_2
    const-wide/16 v4, 0xfa

    cmp-long v3, v0, v4

    if-gez v3, :cond_3

    .line 1971
    const-wide/16 v4, 0xc8

    goto :goto_0

    .line 1972
    :cond_3
    const-wide/16 v4, 0x1f4

    cmp-long v3, v0, v4

    if-gez v3, :cond_4

    .line 1973
    const-wide/16 v4, 0xfa

    goto :goto_0

    .line 1974
    :cond_4
    const-wide/16 v4, 0x3e8

    cmp-long v3, v0, v4

    if-gez v3, :cond_5

    .line 1975
    const-wide/16 v4, 0x1f4

    goto :goto_0

    .line 1977
    :cond_5
    const-wide/16 v4, 0x3e8

    goto :goto_0
.end method

.method public static getSongListRadioMixDescriptor(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/mix/MixDescriptor;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 1248
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/music/utils/MusicUtils;->getSongListRadioMixDescriptor(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Z)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v0

    return-object v0
.end method

.method private static getSongListRadioMixDescriptor(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Z)Lcom/google/android/music/mix/MixDescriptor;
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "artistShuffle"    # Z

    .prologue
    .line 1253
    const/4 v3, 0x0

    .line 1254
    .local v3, "remoteSeedId":Ljava/lang/String;
    const-wide/16 v8, -0x1

    .line 1255
    .local v8, "localSeedId":J
    sget-object v4, Lcom/google/android/music/mix/MixDescriptor$Type;->TRACK_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 1256
    .local v4, "type":Lcom/google/android/music/mix/MixDescriptor$Type;
    const/4 v5, 0x0

    .line 1257
    .local v5, "name":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1259
    .local v6, "artUrl":Ljava/lang/String;
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/music/medialist/ArtistSongList;

    if-eqz v2, :cond_1

    move-object/from16 v14, p1

    .line 1260
    check-cast v14, Lcom/google/android/music/medialist/ArtistSongList;

    .line 1261
    .local v14, "artistSongList":Lcom/google/android/music/medialist/ArtistSongList;
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lcom/google/android/music/medialist/ArtistSongList;->getArtistMetajamId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 1262
    if-eqz p2, :cond_0

    .line 1263
    sget-object v4, Lcom/google/android/music/mix/MixDescriptor$Type;->ARTIST_SHUFFLE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 1267
    :goto_0
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lcom/google/android/music/medialist/ArtistSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1268
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lcom/google/android/music/medialist/ArtistSongList;->getArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 1313
    .end local v14    # "artistSongList":Lcom/google/android/music/medialist/ArtistSongList;
    :goto_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1314
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Missing radio name for "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1265
    .restart local v14    # "artistSongList":Lcom/google/android/music/medialist/ArtistSongList;
    :cond_0
    sget-object v4, Lcom/google/android/music/mix/MixDescriptor$Type;->ARTIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    goto :goto_0

    .line 1269
    .end local v14    # "artistSongList":Lcom/google/android/music/medialist/ArtistSongList;
    :cond_1
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/music/medialist/AlbumSongList;

    if-eqz v2, :cond_2

    move-object/from16 v13, p1

    .line 1270
    check-cast v13, Lcom/google/android/music/medialist/AlbumSongList;

    .line 1271
    .local v13, "albumSongList":Lcom/google/android/music/medialist/AlbumSongList;
    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Lcom/google/android/music/medialist/AlbumSongList;->getAlbumMetajamId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 1272
    sget-object v4, Lcom/google/android/music/mix/MixDescriptor$Type;->ALBUM_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 1273
    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Lcom/google/android/music/medialist/AlbumSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1274
    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Lcom/google/android/music/medialist/AlbumSongList;->getArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 1275
    goto :goto_1

    .end local v13    # "albumSongList":Lcom/google/android/music/medialist/AlbumSongList;
    :cond_2
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/music/medialist/SingleSongList;

    if-eqz v2, :cond_3

    move-object/from16 v21, p1

    .line 1276
    check-cast v21, Lcom/google/android/music/medialist/SingleSongList;

    .line 1277
    .local v21, "singleSongList":Lcom/google/android/music/medialist/SingleSongList;
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/music/medialist/SingleSongList;->getId()J

    move-result-wide v8

    .line 1278
    sget-object v4, Lcom/google/android/music/mix/MixDescriptor$Type;->TRACK_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 1279
    invoke-virtual/range {v21 .. v22}, Lcom/google/android/music/medialist/SingleSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1280
    invoke-virtual/range {v21 .. v22}, Lcom/google/android/music/medialist/SingleSongList;->getArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 1281
    goto :goto_1

    .end local v21    # "singleSongList":Lcom/google/android/music/medialist/SingleSongList;
    :cond_3
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/music/medialist/NautilusArtistSongList;

    if-eqz v2, :cond_5

    move-object/from16 v18, p1

    .line 1282
    check-cast v18, Lcom/google/android/music/medialist/NautilusArtistSongList;

    .line 1283
    .local v18, "nautilusArtistSongList":Lcom/google/android/music/medialist/NautilusArtistSongList;
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/medialist/NautilusArtistSongList;->getNautilusId()Ljava/lang/String;

    move-result-object v3

    .line 1284
    if-eqz p2, :cond_4

    .line 1285
    sget-object v4, Lcom/google/android/music/mix/MixDescriptor$Type;->ARTIST_SHUFFLE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 1289
    :goto_2
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/NautilusArtistSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1290
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/NautilusArtistSongList;->getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 1291
    goto :goto_1

    .line 1287
    :cond_4
    sget-object v4, Lcom/google/android/music/mix/MixDescriptor$Type;->ARTIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    goto :goto_2

    .line 1291
    .end local v18    # "nautilusArtistSongList":Lcom/google/android/music/medialist/NautilusArtistSongList;
    :cond_5
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    if-eqz v2, :cond_6

    move-object/from16 v17, p1

    .line 1292
    check-cast v17, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    .line 1293
    .local v17, "nautilusAlbumSongList":Lcom/google/android/music/medialist/NautilusAlbumSongList;
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getNautilusId()Ljava/lang/String;

    move-result-object v3

    .line 1294
    sget-object v4, Lcom/google/android/music/mix/MixDescriptor$Type;->ALBUM_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 1295
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1296
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 1297
    goto/16 :goto_1

    .end local v17    # "nautilusAlbumSongList":Lcom/google/android/music/medialist/NautilusAlbumSongList;
    :cond_6
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/music/medialist/NautilusSingleSongList;

    if-eqz v2, :cond_7

    move-object/from16 v19, p1

    .line 1298
    check-cast v19, Lcom/google/android/music/medialist/NautilusSingleSongList;

    .line 1299
    .local v19, "nautilusSingleSongList":Lcom/google/android/music/medialist/NautilusSingleSongList;
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/medialist/NautilusSingleSongList;->getNautilusId()Ljava/lang/String;

    move-result-object v3

    .line 1300
    sget-object v4, Lcom/google/android/music/mix/MixDescriptor$Type;->TRACK_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 1301
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/NautilusSingleSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1302
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/NautilusSingleSongList;->getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 1303
    goto/16 :goto_1

    .end local v19    # "nautilusSingleSongList":Lcom/google/android/music/medialist/NautilusSingleSongList;
    :cond_7
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-eqz v2, :cond_8

    move-object/from16 v20, p1

    .line 1304
    check-cast v20, Lcom/google/android/music/medialist/SharedWithMeSongList;

    .line 1305
    .local v20, "sharedWithMeSongList":Lcom/google/android/music/medialist/SharedWithMeSongList;
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getShareToken()Ljava/lang/String;

    move-result-object v3

    .line 1306
    sget-object v4, Lcom/google/android/music/mix/MixDescriptor$Type;->PLAYLIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 1307
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1308
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getArtUrl()Ljava/lang/String;

    move-result-object v6

    .line 1309
    goto/16 :goto_1

    .line 1310
    .end local v20    # "sharedWithMeSongList":Lcom/google/android/music/medialist/SharedWithMeSongList;
    :cond_8
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unexpected song list for radio: "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1318
    :cond_9
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-wide/16 v10, -0x1

    cmp-long v2, v8, v10

    if-nez v2, :cond_b

    .line 1319
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/music/medialist/NautilusSongList;

    if-eqz v2, :cond_d

    .line 1320
    check-cast p1, Lcom/google/android/music/medialist/NautilusSongList;

    .end local p1    # "songList":Lcom/google/android/music/medialist/SongList;
    invoke-static/range {p0 .. p1}, Lcom/google/android/music/utils/MusicUtils;->getFirstTrackMetajamIdAndArt(Landroid/content/Context;Lcom/google/android/music/medialist/NautilusSongList;)Landroid/util/Pair;

    move-result-object v16

    .line 1322
    .local v16, "idArtPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v16, :cond_a

    .line 1323
    move-object/from16 v0, v16

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local v3    # "remoteSeedId":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 1324
    .restart local v3    # "remoteSeedId":Ljava/lang/String;
    move-object/from16 v0, v16

    iget-object v6, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    .end local v6    # "artUrl":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 1333
    .end local v16    # "idArtPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v6    # "artUrl":Ljava/lang/String;
    :cond_a
    :goto_3
    sget-object v4, Lcom/google/android/music/mix/MixDescriptor$Type;->TRACK_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 1337
    :cond_b
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    invoke-static {v6}, Lcom/google/android/music/download/cache/CacheUtils;->isRemoteLocationSideLoaded(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1338
    const/4 v6, 0x0

    .line 1340
    :cond_c
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 1341
    new-instance v2, Lcom/google/android/music/mix/MixDescriptor;

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/music/mix/MixDescriptor;-><init>(Ljava/lang/String;Lcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1343
    :goto_4
    return-object v2

    .line 1327
    .restart local p1    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_d
    invoke-static/range {p0 .. p1}, Lcom/google/android/music/utils/MusicUtils;->getFirstTrackMusicIdAndArt(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/util/Pair;

    move-result-object v15

    .line 1328
    .local v15, "idArtPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    if-eqz v15, :cond_a

    .line 1329
    iget-object v2, v15, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 1330
    iget-object v6, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    .end local v6    # "artUrl":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .restart local v6    # "artUrl":Ljava/lang/String;
    goto :goto_3

    .line 1342
    .end local v15    # "idArtPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    .end local p1    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_e
    const-wide/16 v10, -0x1

    cmp-long v2, v8, v10

    if-eqz v2, :cond_f

    .line 1343
    new-instance v7, Lcom/google/android/music/mix/MixDescriptor;

    move-object v10, v4

    move-object v11, v5

    move-object v12, v6

    invoke-direct/range {v7 .. v12}, Lcom/google/android/music/mix/MixDescriptor;-><init>(JLcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v7

    goto :goto_4

    .line 1345
    :cond_f
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v7, "Missing seed id"

    invoke-direct {v2, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static getStreamingAccount(Landroid/content/Context;)Landroid/accounts/Account;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2424
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 2425
    .local v1, "refObject":Ljava/lang/Object;
    invoke-static {p0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 2427
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2429
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method public static final getUUIDFromString(Ljava/lang/String;)Ljava/util/UUID;
    .locals 2
    .param p0, "uuidString"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 2275
    if-nez p0, :cond_0

    .line 2280
    :goto_0
    return-object v1

    .line 2277
    :cond_0
    :try_start_0
    invoke-static {p0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2278
    .local v1, "uuid":Ljava/util/UUID;
    goto :goto_0

    .line 2279
    .end local v1    # "uuid":Ljava/util/UUID;
    :catch_0
    move-exception v0

    .line 2280
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    goto :goto_0
.end method

.method public static hasCount(Landroid/database/Cursor;)Z
    .locals 1
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    .line 2037
    if-nez p0, :cond_0

    .line 2038
    const/4 v0, 0x0

    .line 2046
    .end local p0    # "c":Landroid/database/Cursor;
    :goto_0
    return v0

    .line 2040
    .restart local p0    # "c":Landroid/database/Cursor;
    :cond_0
    instance-of v0, p0, Lcom/google/android/music/AsyncCursor;

    if-eqz v0, :cond_1

    .line 2041
    check-cast p0, Lcom/google/android/music/AsyncCursor;

    .end local p0    # "c":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/google/android/music/AsyncCursor;->hasCount()Z

    move-result v0

    goto :goto_0

    .line 2043
    .restart local p0    # "c":Landroid/database/Cursor;
    :cond_1
    instance-of v0, p0, Lcom/google/android/music/medialist/MediaList$MediaCursor;

    if-eqz v0, :cond_2

    .line 2044
    check-cast p0, Lcom/google/android/music/medialist/MediaList$MediaCursor;

    .end local p0    # "c":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->hasCount()Z

    move-result v0

    goto :goto_0

    .line 2046
    .restart local p0    # "c":Landroid/database/Cursor;
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isCloudQueueModeEnabled(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 621
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 622
    .local v2, "resolver":Landroid/content/ContentResolver;
    const-string v4, "music_enable_chromecast_cloud_queue"

    invoke-static {v2, v4, v3}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 626
    .local v0, "isCastCloudQueueEnabled":Z
    const-string v4, "music_enable_dial_cloud_queue"

    invoke-static {v2, v4, v3}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    .line 630
    .local v1, "isDialCloudQueueEnabled":Z
    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    const/4 v3, 0x1

    :cond_1
    return v3
.end method

.method public static isCurrentContentFromStoreUrl()Z
    .locals 6

    .prologue
    .line 543
    const/4 v1, 0x0

    .line 544
    .local v1, "result":Z
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 545
    .local v2, "service":Lcom/google/android/music/playback/IMusicPlaybackService;
    if-eqz v2, :cond_0

    .line 547
    :try_start_0
    invoke-interface {v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v3

    .line 548
    .local v3, "songList":Lcom/google/android/music/medialist/SongList;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/google/android/music/medialist/SongList;->getStoreUrl()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    if-eqz v4, :cond_0

    .line 549
    const/4 v1, 0x1

    .line 555
    .end local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_0
    :goto_0
    return v1

    .line 551
    :catch_0
    move-exception v0

    .line 552
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static isDialMediaRouteSupportEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2408
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 2409
    .local v1, "refObject":Ljava/lang/Object;
    invoke-static {p0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 2411
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isMediaRouteSupportEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isDialMediaRouteSupportEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    .line 2413
    :goto_0
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return v2

    .line 2411
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 2413
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method public static isDifferentialSyncEnabled(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 651
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 652
    .local v1, "resolver":Landroid/content/ContentResolver;
    const-string v2, "music_enable_cloud_queue_diffs"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 656
    .local v0, "isDifferentialSyncEnabled":Z
    return v0
.end method

.method public static isInCloudQueueMode(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 634
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->isCloudQueueModeEnabled(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 646
    :cond_0
    :goto_0
    return v1

    .line 638
    :cond_1
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v2, :cond_0

    .line 643
    :try_start_0
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->isInCloudQueueMode()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 644
    :catch_0
    move-exception v0

    .line 645
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static isInInfiniteMixMode()Z
    .locals 5

    .prologue
    .line 530
    const/4 v1, 0x0

    .line 531
    .local v1, "result":Z
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 532
    .local v2, "service":Lcom/google/android/music/playback/IMusicPlaybackService;
    if-eqz v2, :cond_0

    .line 534
    :try_start_0
    invoke-interface {v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->isInIniniteMixMode()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 539
    :cond_0
    :goto_0
    return v1

    .line 535
    :catch_0
    move-exception v0

    .line 536
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static isLandscape(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2172
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMainProcess(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2012
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->getProcessName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2013
    .local v0, "processName":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ":main"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isMainThread()Z
    .locals 2

    .prologue
    .line 1921
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isNautilusId(Ljava/lang/String;)Z
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 2265
    invoke-static {p0}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPlaying()Z
    .locals 3

    .prologue
    .line 391
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 393
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->isPlaying()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 398
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return v1

    .line 394
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 395
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v1, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 398
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isStreaming()Z
    .locals 3

    .prologue
    .line 427
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 429
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->isStreaming()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 434
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return v1

    .line 430
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 431
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v1, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 434
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isUIProcess(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2007
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->getProcessName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2008
    .local v0, "processName":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ":ui"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isUnknown(Ljava/lang/String;)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 254
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "<unknown>"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makeTimeString(Landroid/content/Context;J)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "secs"    # J

    .prologue
    const-wide/16 v8, 0xe10

    const/4 v4, 0x0

    const-wide/16 v6, 0x3c

    .line 1096
    cmp-long v2, p1, v8

    if-gez v2, :cond_0

    const v2, 0x7f0b0052

    :goto_0
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1102
    .local v0, "durationformat":Ljava/lang/String;
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sFormatBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1104
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sTimeArgs:[Ljava/lang/Object;

    .line 1105
    .local v1, "timeArgs":[Ljava/lang/Object;
    div-long v2, p1, v8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    .line 1106
    const/4 v2, 0x1

    div-long v4, p1, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1107
    const/4 v2, 0x2

    div-long v4, p1, v6

    rem-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1108
    const/4 v2, 0x3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1109
    const/4 v2, 0x4

    rem-long v4, p1, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1111
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sFormatter:Ljava/util/Formatter;

    invoke-virtual {v2, v0, v1}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 1096
    .end local v0    # "durationformat":Ljava/lang/String;
    .end local v1    # "timeArgs":[Ljava/lang/Object;
    :cond_0
    const v2, 0x7f0b0053

    goto :goto_0
.end method

.method public static makeTimeStringSync(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "secs"    # J

    .prologue
    .line 1118
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sFormatter:Ljava/util/Formatter;

    monitor-enter v1

    .line 1119
    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/google/android/music/utils/MusicUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1120
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static movePlaylistItem(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/medialist/PlaylistSongList;II)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "playlist"    # Lcom/google/android/music/medialist/PlaylistSongList;
    .param p3, "from"    # I
    .param p4, "to"    # I

    .prologue
    .line 1583
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 1584
    if-eq p3, p4, :cond_0

    .line 1585
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 1587
    .local v0, "colidx":I
    invoke-interface {p1, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1588
    const-string v1, "MusicUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to move item. Invalid \"from\" position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Cursor size:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1617
    .end local v0    # "colidx":I
    :cond_0
    :goto_0
    return-void

    .line 1592
    .restart local v0    # "colidx":I
    :cond_1
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1595
    .local v4, "itemToMoveId":J
    if-lez p4, :cond_4

    .line 1596
    if-le p3, p4, :cond_2

    .line 1597
    add-int/lit8 p4, p4, -0x1

    .line 1599
    :cond_2
    invoke-interface {p1, p4}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1600
    const-string v1, "MusicUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to move item. Invalid \"to\" position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Cursor size:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1604
    :cond_3
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1609
    .local v6, "desiredPreviousItemId":J
    :goto_1
    new-instance v1, Lcom/google/android/music/utils/MusicUtils$4;

    move-object v2, p0

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/utils/MusicUtils$4;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/PlaylistSongList;JJ)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1606
    .end local v6    # "desiredPreviousItemId":J
    :cond_4
    const-wide/16 v6, 0x0

    .restart local v6    # "desiredPreviousItemId":J
    goto :goto_1
.end method

.method public static moveQueueItem(Landroid/content/Context;II)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fromPos"    # I
    .param p2, "toPos"    # I

    .prologue
    .line 1557
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->isInCloudQueueMode(Landroid/content/Context;)Z

    move-result v0

    .line 1558
    .local v0, "isInCloudQueueMode":Z
    new-instance v1, Lcom/google/android/music/utils/MusicUtils$3;

    invoke-direct {v1, p0, v0, p1, p2}, Lcom/google/android/music/utils/MusicUtils$3;-><init>(Landroid/content/Context;ZII)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 1564
    return-void
.end method

.method public static openPlaylistCursor(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 858
    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v3, v7

    const-string v0, "playlist_name"

    aput-object v0, v3, v9

    .line 861
    .local v3, "cols":[Ljava/lang/String;
    new-instance v0, Lcom/google/android/music/AsyncCursor;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/store/MusicContent$Playlists;->CONTENT_URI:Landroid/net/Uri;

    move-object v5, v4

    move-object v6, v4

    move v8, v7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/music/AsyncCursor;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZZ)V

    sput-object v0, Lcom/google/android/music/utils/MusicUtils;->sPlaylists:Lcom/google/android/music/AsyncCursor;

    .line 865
    new-instance v0, Lcom/google/android/music/utils/MusicUtils$1;

    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sAsyncQueryWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-direct {v0, v1}, Lcom/google/android/music/utils/MusicUtils$1;-><init>(Landroid/os/Handler;)V

    sput-object v0, Lcom/google/android/music/utils/MusicUtils;->sPlaylistObserver:Landroid/database/ContentObserver;

    .line 874
    sget-object v0, Lcom/google/android/music/utils/MusicUtils;->sPlaylists:Lcom/google/android/music/AsyncCursor;

    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sPlaylistObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncCursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 875
    return-void
.end method

.method public static performDelete(Ljava/lang/Long;ZLcom/google/android/music/medialist/SongList;Landroid/content/Context;)V
    .locals 8
    .param p0, "trackId"    # Ljava/lang/Long;
    .param p1, "notifyOnDelete"    # Z
    .param p2, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 2372
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2375
    .local v1, "resolver":Landroid/content/ContentResolver;
    instance-of v3, p2, Lcom/google/android/music/medialist/CaqPlayQueueSongList;

    if-eqz v3, :cond_1

    .line 2377
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p3}, Lcom/google/android/music/utils/MusicUtils;->isInCloudQueueMode(Landroid/content/Context;)Z

    move-result v3

    invoke-static {v4, v5, v3}, Lcom/google/android/music/store/MusicContent$Queue;->getItemUri(JZ)Landroid/net/Uri;

    move-result-object v2

    .line 2387
    .end local p2    # "songList":Lcom/google/android/music/medialist/SongList;
    .local v2, "uri":Landroid/net/Uri;
    :goto_0
    if-nez p1, :cond_0

    .line 2388
    invoke-static {v2}, Lcom/google/android/music/store/MusicContent;->addNotifyParam(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    .line 2392
    :cond_0
    move-object v0, v2

    .line 2393
    .local v0, "finalUri":Landroid/net/Uri;
    sget-object v3, Lcom/google/android/music/utils/async/AsyncWorkers;->sUIBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v4, Lcom/google/android/music/utils/MusicUtils$6;

    invoke-direct {v4, v1, v0}, Lcom/google/android/music/utils/MusicUtils$6;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    invoke-static {v3, v4}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 2401
    return-void

    .line 2380
    .end local v0    # "finalUri":Landroid/net/Uri;
    .end local v2    # "uri":Landroid/net/Uri;
    .restart local p2    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_1
    check-cast p2, Lcom/google/android/music/medialist/PlaylistSongList;

    .end local p2    # "songList":Lcom/google/android/music/medialist/SongList;
    invoke-virtual {p2}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistId()J

    move-result-wide v4

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/music/store/MusicContent$Playlists$Members;->getPlaylistItemUri(JJ)Landroid/net/Uri;

    move-result-object v2

    .restart local v2    # "uri":Landroid/net/Uri;
    goto :goto_0
.end method

.method public static performDelete(Ljava/util/ArrayList;Lcom/google/android/music/medialist/SongList;Landroid/content/Context;)V
    .locals 3
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/google/android/music/medialist/SongList;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2354
    .local p0, "trackIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2355
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-static {v1, v2, p1, p2}, Lcom/google/android/music/utils/MusicUtils;->performDelete(Ljava/lang/Long;ZLcom/google/android/music/medialist/SongList;Landroid/content/Context;)V

    .line 2354
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2355
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 2358
    :cond_1
    return-void
.end method

.method public static playArtistShuffle(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 1392
    instance-of v5, p1, Lcom/google/android/music/medialist/NautilusArtistSongList;

    if-nez v5, :cond_0

    instance-of v5, p1, Lcom/google/android/music/medialist/ArtistSongList;

    if-eqz v5, :cond_4

    .line 1398
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1399
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->clearPlayQueue()V

    .line 1400
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->shuffleSongs(Lcom/google/android/music/medialist/SongList;)V

    .line 1424
    :goto_0
    return-void

    .line 1404
    :cond_1
    const/4 v5, 0x1

    invoke-static {p0, p1, v5}, Lcom/google/android/music/utils/MusicUtils;->getSongListRadioMixDescriptor(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Z)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v1

    .line 1405
    .local v1, "mixDescriptor":Lcom/google/android/music/mix/MixDescriptor;
    new-instance v0, Landroid/content/Intent;

    const-class v5, Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    invoke-direct {v0, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1407
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {v1}, Lcom/google/android/music/mix/MixDescriptor;->getRemoteSeedId()Ljava/lang/String;

    move-result-object v4

    .line 1408
    .local v4, "remoteSeedId":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/google/android/music/mix/MixDescriptor;->getLocalSeedId()J

    move-result-wide v2

    .line 1410
    .local v2, "localSeedId":J
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1411
    const-string v5, "remoteId"

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1417
    :goto_1
    const-string v5, "name"

    invoke-virtual {v1}, Lcom/google/android/music/mix/MixDescriptor;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1418
    const-string v5, "songlist"

    invoke-virtual {v0, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1419
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1412
    :cond_2
    const-wide/16 v6, -0x1

    cmp-long v5, v2, v6

    if-eqz v5, :cond_3

    .line 1413
    const-string v5, "localId"

    invoke-virtual {v0, v5, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_1

    .line 1415
    :cond_3
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Missing seed id"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1421
    .end local v0    # "i":Landroid/content/Intent;
    .end local v1    # "mixDescriptor":Lcom/google/android/music/mix/MixDescriptor;
    .end local v2    # "localSeedId":J
    .end local v4    # "remoteSeedId":Ljava/lang/String;
    :cond_4
    const-string v5, "MusicUtils"

    const-string v6, "Unexpected songList type to play artist shuffle"

    invoke-static {v5, v6}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static playGenreRadio(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "genreId"    # Ljava/lang/String;
    .param p3, "artLocation"    # Ljava/lang/String;

    .prologue
    .line 1432
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1433
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Missing mix name"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1435
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1436
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Missing genre id"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1438
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/activities/CreateMixActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1440
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "remoteId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1441
    const-string v1, "mixType"

    sget-object v2, Lcom/google/android/music/mix/MixDescriptor$Type;->GENRE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v2}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1442
    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1443
    const-string v1, "artLocation"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1444
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1445
    return-void
.end method

.method public static playImFeelingLuckyRadio(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1448
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/activities/CreateMixActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1449
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "mixType"

    sget-object v2, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v2}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1450
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1451
    return-void
.end method

.method public static playMediaList(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "list"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 1180
    const/4 v0, -0x1

    invoke-static {p0, p1, v0}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;I)V

    .line 1181
    return-void
.end method

.method public static playMediaList(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "list"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "position"    # I

    .prologue
    .line 1166
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.music.playSongList"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1168
    .local v0, "playSonglist":Landroid/content/Intent;
    const-string v1, "songlist"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1169
    const-string v1, "position"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1170
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1171
    return-void
.end method

.method public static playMediaList(Lcom/google/android/music/medialist/SongList;)V
    .locals 1
    .param p0, "list"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 1131
    const/4 v0, -0x1

    invoke-static {p0, v0}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Lcom/google/android/music/medialist/SongList;I)V

    .line 1132
    return-void
.end method

.method public static playMediaList(Lcom/google/android/music/medialist/SongList;I)V
    .locals 3
    .param p0, "list"    # Lcom/google/android/music/medialist/SongList;
    .param p1, "position"    # I

    .prologue
    .line 1145
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v1, :cond_0

    .line 1146
    const-string v1, "MusicUtils"

    const-string v2, "service not ready"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1154
    :goto_0
    return-void

    .line 1150
    :cond_0
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    const/4 v2, 0x1

    invoke-interface {v1, p0, p1, v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->open(Lcom/google/android/music/medialist/SongList;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1151
    :catch_0
    move-exception v0

    .line 1152
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static playNext(Lcom/google/android/music/medialist/SongList;I)V
    .locals 3
    .param p0, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p1, "fromPlayPos"    # I

    .prologue
    .line 1567
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 1569
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1, p0, p1}, Lcom/google/android/music/playback/IMusicPlaybackService;->playNext(Lcom/google/android/music/medialist/SongList;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1574
    :cond_0
    :goto_0
    return-void

    .line 1570
    :catch_0
    move-exception v0

    .line 1571
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static playRadio(Landroid/content/Context;JLjava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "radioId"    # J
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 1491
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1492
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Missing mix name"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1495
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/activities/CreateMixActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1497
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "mixType"

    sget-object v2, Lcom/google/android/music/mix/MixDescriptor$Type;->RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v2}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1498
    const-string v1, "radioLocalId"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1499
    const-string v1, "name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1500
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1501
    return-void
.end method

.method public static playRadio(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 1355
    instance-of v2, p1, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 1356
    check-cast v1, Lcom/google/android/music/medialist/PlaylistSongList;

    .line 1357
    .local v1, "playlistSongList":Lcom/google/android/music/medialist/PlaylistSongList;
    invoke-static {p0, v1}, Lcom/google/android/music/utils/MusicUtils;->startPlaylistRadio(Landroid/content/Context;Lcom/google/android/music/medialist/PlaylistSongList;)V

    .line 1362
    .end local v1    # "playlistSongList":Lcom/google/android/music/medialist/PlaylistSongList;
    :goto_0
    return-void

    .line 1359
    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/music/utils/MusicUtils;->getSongListRadioMixDescriptor(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v0

    .line 1360
    .local v0, "mixDescriptor":Lcom/google/android/music/mix/MixDescriptor;
    invoke-static {p0, v0}, Lcom/google/android/music/utils/MusicUtils;->startCreateMixActivity(Landroid/content/Context;Lcom/google/android/music/mix/MixDescriptor;)V

    goto :goto_0
.end method

.method public static playRecommendedRadio(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "seedId"    # Ljava/lang/String;
    .param p2, "seedType"    # I
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "artUrl"    # Ljava/lang/String;

    .prologue
    .line 1514
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1515
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Missing mix name"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1518
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/activities/CreateMixActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1520
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "mixType"

    invoke-static {p2}, Lcom/google/android/music/store/MusicContent$RadioStations;->getMixDescriptorSeedType(I)Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1522
    const-string v1, "remoteId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1523
    const-string v1, "name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1524
    const-string v1, "artLocation"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1525
    const-string v1, "isRecommendation"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1526
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1527
    return-void
.end method

.method public static query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 1052
    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "shouldFilter"    # Z
    .param p7, "shouldIncludeExternal"    # Z

    .prologue
    const/4 v7, 0x0

    .line 1060
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1061
    .local v0, "resolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    move-object v1, v7

    .line 1071
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    :goto_0
    return-object v1

    .line 1065
    .restart local v0    # "resolver":Landroid/content/ContentResolver;
    :cond_0
    const-string v1, "Query on main thread"

    invoke-static {p0, v1}, Lcom/google/android/music/utils/MusicUtils;->checkMainThread(Landroid/content/Context;Ljava/lang/String;)V

    .line 1066
    invoke-static {p0, p1, p6, p7}, Lcom/google/android/music/store/MusicContent;->appendFilter(Landroid/content/Context;Landroid/net/Uri;ZZ)Landroid/net/Uri;

    move-result-object p1

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 1067
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 1068
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    :catch_0
    move-exception v6

    .line 1070
    .local v6, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v1, "MusicUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UnsupportedOperationException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v7

    .line 1071
    goto :goto_0
.end method

.method public static query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/music/utils/MusicUtils$QueryCallback;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "shouldFilter"    # Z
    .param p7, "shouldIncludeExternal"    # Z
    .param p8, "cb"    # Lcom/google/android/music/utils/MusicUtils$QueryCallback;

    .prologue
    .line 1012
    new-instance v9, Landroid/os/Handler;

    invoke-direct {v9}, Landroid/os/Handler;-><init>()V

    .line 1014
    .local v9, "replyHandler":Landroid/os/Handler;
    sget-object v11, Lcom/google/android/music/utils/MusicUtils;->sAsyncQueryWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v0, Lcom/google/android/music/utils/MusicUtils$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lcom/google/android/music/utils/MusicUtils$2;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZLandroid/os/Handler;Lcom/google/android/music/utils/MusicUtils$QueryCallback;)V

    invoke-virtual {v11, v0}, Lcom/google/android/music/utils/LoggableHandler;->post(Ljava/lang/Runnable;)Z

    .line 1029
    return-void
.end method

.method public static queue(Lcom/google/android/music/medialist/SongList;)V
    .locals 3
    .param p0, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 715
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 717
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    const/4 v2, -0x1

    invoke-interface {v1, p0, v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->openAndQueue(Lcom/google/android/music/medialist/SongList;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 723
    :cond_0
    :goto_0
    return-void

    .line 719
    :catch_0
    move-exception v0

    .line 720
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static requestSync(Lcom/google/android/music/preferences/MusicPreferences;Z)V
    .locals 4
    .param p0, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;
    .param p1, "userInitiated"    # Z

    .prologue
    const/4 v3, 0x1

    .line 1713
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 1714
    .local v0, "account":Landroid/accounts/Account;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1715
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz p1, :cond_0

    .line 1716
    const-string v2, "expedited"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1717
    const-string v2, "force"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1718
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1721
    const-string v2, "com.google.android.music.MusicContent"

    invoke-static {v0, v2, v3}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 1724
    :cond_0
    const-string v2, "com.google.android.music.MusicContent"

    invoke-static {v0, v2}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_1

    .line 1725
    const-string v2, "com.google.android.music.MusicContent"

    invoke-static {v0, v2, v1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1727
    :cond_1
    return-void
.end method

.method public static runAsync(Ljava/lang/Runnable;)V
    .locals 2
    .param p0, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 943
    sget-object v0, Lcom/google/android/music/utils/MusicUtils;->sAsyncQueryWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/utils/async/TraceableRunnable;

    invoke-direct {v1, p0}, Lcom/google/android/music/utils/async/TraceableRunnable;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/utils/LoggableHandler;->post(Ljava/lang/Runnable;)Z

    .line 944
    return-void
.end method

.method public static runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V
    .locals 3
    .param p0, "runner"    # Lcom/google/android/music/utils/async/AsyncRunner;

    .prologue
    .line 963
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 965
    .local v0, "callbackHandler":Landroid/os/Handler;
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sAsyncQueryWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/utils/async/CallbackRunnable;

    invoke-direct {v2, v0, p0}, Lcom/google/android/music/utils/async/CallbackRunnable;-><init>(Landroid/os/Handler;Lcom/google/android/music/utils/async/AsyncRunner;)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/utils/LoggableHandler;->post(Ljava/lang/Runnable;)Z

    .line 966
    return-void
.end method

.method public static runDelayedOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;I)Landroid/os/Handler;
    .locals 4
    .param p0, "r"    # Ljava/lang/Runnable;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "delay"    # I

    .prologue
    .line 956
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 957
    .local v0, "callbackHandler":Landroid/os/Handler;
    new-instance v1, Lcom/google/android/music/utils/async/TraceableRunnable;

    invoke-direct {v1, p0}, Lcom/google/android/music/utils/async/TraceableRunnable;-><init>(Ljava/lang/Runnable;)V

    int-to-long v2, p2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 958
    return-object v0
.end method

.method public static runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V
    .locals 2
    .param p0, "r"    # Ljava/lang/Runnable;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 947
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 948
    .local v0, "callbackHandler":Landroid/os/Handler;
    new-instance v1, Lcom/google/android/music/utils/async/TraceableRunnable;

    invoke-direct {v1, p0}, Lcom/google/android/music/utils/async/TraceableRunnable;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 949
    return-void
.end method

.method public static safeEquals(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "o1"    # Ljava/lang/Object;
    .param p1, "o2"    # Ljava/lang/Object;

    .prologue
    .line 1754
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 1755
    const/4 v0, 0x1

    .line 1759
    :goto_0
    return v0

    .line 1756
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    .line 1757
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1759
    :cond_2
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static setMediaRouteVolume(Ljava/lang/String;D)V
    .locals 3
    .param p0, "routeId"    # Ljava/lang/String;
    .param p1, "volume"    # D

    .prologue
    .line 726
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 728
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1, p0, p1, p2}, Lcom/google/android/music/playback/IMusicPlaybackService;->setMediaRouteVolume(Ljava/lang/String;D)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 733
    :cond_0
    :goto_0
    return-void

    .line 729
    :catch_0
    move-exception v0

    .line 730
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static showSongsAddedToPlaylistToast(Landroid/content/Context;ILjava/lang/String;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "quantity"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 922
    if-ltz p1, :cond_0

    .line 923
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f120005

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    aput-object p2, v4, v6

    invoke-virtual {v2, v3, p1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 929
    .local v1, "text":Ljava/lang/String;
    :goto_0
    invoke-static {p0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 930
    return-void

    .line 926
    .end local v1    # "text":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00d4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 927
    .local v0, "message":Ljava/lang/String;
    new-array v2, v6, [Ljava/lang/Object;

    const/16 v3, 0x3e8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "text":Ljava/lang/String;
    goto :goto_0
.end method

.method public static showSongsToAddToQueue(Landroid/content/Context;III)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "maxAdded"    # I
    .param p2, "toAdd"    # I
    .param p3, "maxQueueSize"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 902
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 905
    .local v0, "resources":Landroid/content/res/Resources;
    if-gtz p1, :cond_1

    .line 906
    const v2, 0x7f0b0220

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 915
    .local v1, "text":Ljava/lang/String;
    :goto_0
    if-eqz v1, :cond_0

    .line 916
    invoke-static {p0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 918
    :cond_0
    return-void

    .line 907
    .end local v1    # "text":Ljava/lang/String;
    :cond_1
    if-ge p1, p2, :cond_2

    .line 908
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f120007

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, p1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "text":Ljava/lang/String;
    goto :goto_0

    .line 912
    .end local v1    # "text":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "text":Ljava/lang/String;
    goto :goto_0
.end method

.method public static shuffleAll()V
    .locals 3

    .prologue
    .line 599
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 601
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->cancelMix()V

    .line 602
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->shuffleAll()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 607
    .local v0, "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void

    .line 603
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 604
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v1, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static shuffleOnDevice()V
    .locals 3

    .prologue
    .line 610
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 612
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->cancelMix()V

    .line 613
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->shuffleOnDevice()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 618
    .local v0, "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void

    .line 614
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 615
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v1, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static shuffleOnDeviceEarly(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 665
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 666
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.android.music.shuffleOnDevice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 667
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 668
    return-void
.end method

.method public static shuffleSongs(Lcom/google/android/music/medialist/SongList;)V
    .locals 3
    .param p0, "songsToShuffle"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 588
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 590
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->cancelMix()V

    .line 591
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1, p0}, Lcom/google/android/music/playback/IMusicPlaybackService;->shuffleSongs(Lcom/google/android/music/medialist/SongList;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 596
    :cond_0
    :goto_0
    return-void

    .line 592
    :catch_0
    move-exception v0

    .line 593
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static startCreateMixActivity(Landroid/content/Context;Lcom/google/android/music/mix/MixDescriptor;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mixDescriptor"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    .line 1365
    new-instance v0, Landroid/content/Intent;

    const-class v4, Lcom/google/android/music/activities/CreateMixActivity;

    invoke-direct {v0, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1367
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p1}, Lcom/google/android/music/mix/MixDescriptor;->getRemoteSeedId()Ljava/lang/String;

    move-result-object v1

    .line 1368
    .local v1, "remoteSeedId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/music/mix/MixDescriptor;->getLocalSeedId()J

    move-result-wide v2

    .line 1370
    .local v2, "localSeedId":J
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1371
    const-string v4, "remoteId"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1377
    :goto_0
    const-string v4, "mixType"

    invoke-virtual {p1}, Lcom/google/android/music/mix/MixDescriptor;->getType()Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1378
    const-string v4, "name"

    invoke-virtual {p1}, Lcom/google/android/music/mix/MixDescriptor;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1379
    const-string v4, "artLocation"

    invoke-virtual {p1}, Lcom/google/android/music/mix/MixDescriptor;->getArtLocation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1380
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1381
    return-void

    .line 1372
    :cond_0
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 1373
    const-string v4, "localId"

    invoke-virtual {v0, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_0

    .line 1375
    :cond_1
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Missing seed id"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private static startPlaylistRadio(Landroid/content/Context;Lcom/google/android/music/medialist/PlaylistSongList;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/PlaylistSongList;

    .prologue
    .line 1454
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/activities/CreateMixActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1455
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "mixType"

    sget-object v2, Lcom/google/android/music/mix/MixDescriptor$Type;->PLAYLIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v2}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1456
    const-string v1, "localId"

    invoke-virtual {p1}, Lcom/google/android/music/medialist/PlaylistSongList;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1457
    const-string v1, "remoteId"

    invoke-virtual {p1}, Lcom/google/android/music/medialist/PlaylistSongList;->getShareToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1458
    const-string v1, "artLocation"

    invoke-virtual {p1}, Lcom/google/android/music/medialist/PlaylistSongList;->getArtUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1459
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1460
    return-void
.end method

.method public static startVideoPlayerActivity(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 1545
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/youtube/VideoPlayerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1546
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1547
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1548
    return-void
.end method

.method public static startVideoSearchActivity(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "trackName"    # Ljava/lang/String;
    .param p2, "artistName"    # Ljava/lang/String;

    .prologue
    .line 1535
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/youtube/VideoSearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1536
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "trackName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1537
    const-string v1, "artistName"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1538
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1539
    return-void
.end method

.method public static toggleShuffle()V
    .locals 4

    .prologue
    .line 671
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v2, :cond_0

    .line 673
    :try_start_0
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getCurrentShuffleMode()I

    move-result v1

    .line 674
    .local v1, "shuffle":I
    if-nez v1, :cond_1

    .line 675
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/google/android/music/playback/IMusicPlaybackService;->setShuffleMode(I)V

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 677
    :cond_1
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/google/android/music/playback/IMusicPlaybackService;->setShuffleMode(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 679
    :catch_0
    move-exception v0

    .line 680
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static unbindFromService(Lcom/google/android/music/utils/MusicUtils$ServiceToken;)V
    .locals 4
    .param p0, "token"    # Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    .prologue
    .line 305
    if-nez p0, :cond_0

    .line 306
    const-string v2, "MusicUtils"

    const-string v3, "Trying to unbind with null token"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :goto_0
    return-void

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/utils/MusicUtils$ServiceToken;->mWrappedContext:Landroid/content/ContextWrapper;

    .line 310
    .local v0, "cw":Landroid/content/ContextWrapper;
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sConnectionMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;

    .line 311
    .local v1, "sb":Lcom/google/android/music/utils/MusicUtils$ServiceBinder;
    if-nez v1, :cond_1

    .line 312
    const-string v2, "MusicUtils"

    const-string v3, "Trying to unbind for unknown Context"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 325
    :cond_1
    invoke-virtual {v1, v0}, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;->unbindFromServiceAfterServiceConnected(Landroid/content/ContextWrapper;)V

    goto :goto_0
.end method

.method public static updateMediaRouteVolume(Ljava/lang/String;D)V
    .locals 3
    .param p0, "routeId"    # Ljava/lang/String;
    .param p1, "deltaRatio"    # D

    .prologue
    .line 736
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 738
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1, p0, p1, p2}, Lcom/google/android/music/playback/IMusicPlaybackService;->updateMediaRouteVolume(Ljava/lang/String;D)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 743
    :cond_0
    :goto_0
    return-void

    .line 739
    :catch_0
    move-exception v0

    .line 740
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MusicUtils"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
