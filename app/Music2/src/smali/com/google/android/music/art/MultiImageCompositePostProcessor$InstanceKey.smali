.class final Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;
.super Ljava/lang/Object;
.source "MultiImageCompositePostProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/MultiImageCompositePostProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "InstanceKey"
.end annotation


# instance fields
.field final columns:I

.field final rows:I


# direct methods
.method private constructor <init>(II)V
    .locals 0
    .param p1, "columns"    # I
    .param p2, "rows"    # I

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    iput p1, p0, Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;->columns:I

    .line 179
    iput p2, p0, Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;->rows:I

    .line 180
    return-void
.end method

.method synthetic constructor <init>(IILcom/google/android/music/art/MultiImageCompositePostProcessor$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/google/android/music/art/MultiImageCompositePostProcessor$1;

    .prologue
    .line 173
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;-><init>(II)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 184
    if-ne p0, p1, :cond_1

    .line 192
    :cond_0
    :goto_0
    return v1

    .line 185
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 187
    check-cast v0, Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;

    .line 189
    .local v0, "that":Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;
    iget v3, p0, Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;->columns:I

    iget v4, v0, Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;->columns:I

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 190
    :cond_4
    iget v3, p0, Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;->rows:I

    iget v4, v0, Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;->rows:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 197
    iget v0, p0, Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;->columns:I

    .line 198
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;->rows:I

    add-int v0, v1, v2

    .line 199
    return v0
.end method
