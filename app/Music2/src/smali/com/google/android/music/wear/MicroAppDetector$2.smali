.class Lcom/google/android/music/wear/MicroAppDetector$2;
.super Ljava/lang/Object;
.source "MicroAppDetector.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/wear/MicroAppDetector;->refreshMicroAppPresence()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/wear/MicroAppDetector;

.field final synthetic val$client:Lcom/google/android/gms/common/api/GoogleApiClient;

.field final synthetic val$microAppNodeCallback:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;


# direct methods
.method constructor <init>(Lcom/google/android/music/wear/MicroAppDetector;Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/music/wear/MicroAppDetector$2;->this$0:Lcom/google/android/music/wear/MicroAppDetector;

    iput-object p2, p0, Lcom/google/android/music/wear/MicroAppDetector$2;->val$client:Lcom/google/android/gms/common/api/GoogleApiClient;

    iput-object p3, p0, Lcom/google/android/music/wear/MicroAppDetector$2;->val$microAppNodeCallback:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/music/wear/MicroAppDetector$2;->this$0:Lcom/google/android/music/wear/MicroAppDetector;

    # getter for: Lcom/google/android/music/wear/MicroAppDetector;->mRegistry:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;
    invoke-static {v0}, Lcom/google/android/music/wear/MicroAppDetector;->access$200(Lcom/google/android/music/wear/MicroAppDetector;)Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/wear/MicroAppDetector$2;->val$client:Lcom/google/android/gms/common/api/GoogleApiClient;

    const-string v2, "music_micro_app_v2"

    iget-object v3, p0, Lcom/google/android/music/wear/MicroAppDetector$2;->val$microAppNodeCallback:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;->getNodesForCapabilityWithConnectedClient(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;)V

    .line 112
    return-void
.end method

.method public onConnectionSuspended(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 116
    return-void
.end method
