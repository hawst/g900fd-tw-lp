.class public Lcom/google/android/music/medialist/StoreSongList;
.super Lcom/google/android/music/medialist/AutoPlaylistSongList;
.source "StoreSongList.java"


# instance fields
.field private final mHasStore:Z


# direct methods
.method public constructor <init>(IZ)V
    .locals 2
    .param p1, "sortOrder"    # I
    .param p2, "hasStore"    # Z

    .prologue
    .line 18
    const-wide/16 v0, -0x3

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/music/medialist/AutoPlaylistSongList;-><init>(IJ)V

    .line 19
    iput-boolean p2, p0, Lcom/google/android/music/medialist/StoreSongList;->mHasStore:Z

    .line 20
    return-void
.end method


# virtual methods
.method public getArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 52
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/music/medialist/StoreSongList;->getSortOrder()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/music/medialist/StoreSongList;->mHasStore:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected getListingNameResourceId()I
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/music/medialist/StoreSongList;->mHasStore:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0b00bd

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0b00bf

    goto :goto_0
.end method

.method protected getTitleResourceId()I
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/music/medialist/StoreSongList;->mHasStore:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0b00be

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0b00c0

    goto :goto_0
.end method

.method public getValidSortOrders()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 34
    .local v0, "orders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    return-object v0
.end method

.method public storeDefaultSortOrder(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-static {p1, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 44
    .local v0, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    iget v1, p0, Lcom/google/android/music/medialist/StoreSongList;->mSortOrder:I

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setStoreSongsSortOrder(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 48
    return-void

    .line 46
    :catchall_0
    move-exception v1

    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v1
.end method
