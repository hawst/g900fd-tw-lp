.class Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$1;
.super Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;
.source "LeanbackPlaylistDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;-><init>(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;Landroid/content/Context;Landroid/support/v17/leanback/widget/PresenterSelector;Landroid/support/v17/leanback/widget/ObjectAdapter;Lcom/google/android/music/leanback/SongTitleRow;Lcom/google/android/music/leanback/SongFooterRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

.field final synthetic val$this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$1;->val$this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->access$300(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->notifyChanged()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->access$400(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;)V

    .line 186
    return-void
.end method

.method public onItemRangeChanged(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 190
    if-nez p1, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->notifyItemRangeChanged(II)V
    invoke-static {v0, p1, v1}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->access$500(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;II)V

    .line 193
    :cond_0
    return-void
.end method

.method public onItemRangeInserted(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 197
    if-nez p1, :cond_0

    .line 198
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->access$300(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->notifyItemRangeInserted(II)V
    invoke-static {v0, p1, v1}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->access$600(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;II)V

    .line 201
    :cond_0
    return-void
.end method

.method public onItemRangeRemoved(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 205
    if-nez p1, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->access$300(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;)V

    .line 207
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->notifyItemRangeRemoved(II)V
    invoke-static {v0, p1, v1}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->access$700(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;II)V

    .line 209
    :cond_0
    return-void
.end method
