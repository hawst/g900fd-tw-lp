.class public Lcom/google/android/music/store/SituationsCache;
.super Ljava/lang/Object;
.source "SituationsCache.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mLastResponse:Lcom/google/android/music/cloudclient/GetSituationsResponse;

.field private mLastResponseTime:J

.field private final mNetworkConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/net/NetworkMonitorServiceConnection;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkConnection"    # Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/music/store/SituationsCache;->mContext:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/google/android/music/store/SituationsCache;->mNetworkConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .line 29
    return-void
.end method

.method private getSituationsTree(Landroid/content/Context;)Lcom/google/android/music/cloudclient/GetSituationsResponse;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 63
    :try_start_0
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p1}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 64
    .local v0, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    invoke-interface {v0}, Lcom/google/android/music/cloudclient/MusicCloud;->getSituations()Lcom/google/android/music/cloudclient/GetSituationsResponse;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 72
    .end local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :goto_0
    return-object v2

    .line 65
    :catch_0
    move-exception v1

    .line 66
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v3, "SituationsCache"

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 68
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 69
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "SituationsCache"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private isNautilusEnabled()Z
    .locals 3

    .prologue
    .line 76
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 77
    .local v1, "prefsOwner":Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/music/store/SituationsCache;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 79
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 81
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return v2

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method private shouldUpdate(JZ)Z
    .locals 7
    .param p1, "now"    # J
    .param p3, "isAutoUpdate"    # Z

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/music/store/SituationsCache;->mNetworkConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->hasConnectivity()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/music/store/SituationsCache;->mLastResponseTime:J

    sub-long v0, p1, v0

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->getSituationsResponseTTLMinutes()J

    move-result-wide v2

    const-wide/16 v4, 0x3c

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    if-nez p3, :cond_1

    iget-object v0, p0, Lcom/google/android/music/store/SituationsCache;->mLastResponse:Lcom/google/android/music/cloudclient/GetSituationsResponse;

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/music/store/SituationsCache;->mLastResponseTime:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized getResponse(Z)Lcom/google/android/music/cloudclient/GetSituationsResponse;
    .locals 7
    .param p1, "isAutoUpdate"    # Z

    .prologue
    const/4 v3, 0x0

    .line 40
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/store/SituationsCache;->isNautilusEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isConciergeListenNowEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_1

    .line 56
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v3

    .line 43
    :cond_1
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 44
    .local v0, "now":J
    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/music/store/SituationsCache;->shouldUpdate(JZ)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 45
    iget-object v2, p0, Lcom/google/android/music/store/SituationsCache;->mLastResponse:Lcom/google/android/music/cloudclient/GetSituationsResponse;

    .line 46
    .local v2, "oldResponse":Lcom/google/android/music/cloudclient/GetSituationsResponse;
    iget-object v3, p0, Lcom/google/android/music/store/SituationsCache;->mContext:Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/google/android/music/store/SituationsCache;->getSituationsTree(Landroid/content/Context;)Lcom/google/android/music/cloudclient/GetSituationsResponse;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/store/SituationsCache;->mLastResponse:Lcom/google/android/music/cloudclient/GetSituationsResponse;

    .line 47
    iput-wide v0, p0, Lcom/google/android/music/store/SituationsCache;->mLastResponseTime:J

    .line 50
    iget-object v3, p0, Lcom/google/android/music/store/SituationsCache;->mLastResponse:Lcom/google/android/music/cloudclient/GetSituationsResponse;

    if-eq v3, v2, :cond_2

    .line 51
    iget-object v3, p0, Lcom/google/android/music/store/SituationsCache;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/store/MusicContent$Situations;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 56
    .end local v2    # "oldResponse":Lcom/google/android/music/cloudclient/GetSituationsResponse;
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/store/SituationsCache;->mLastResponse:Lcom/google/android/music/cloudclient/GetSituationsResponse;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 40
    .end local v0    # "now":J
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method
