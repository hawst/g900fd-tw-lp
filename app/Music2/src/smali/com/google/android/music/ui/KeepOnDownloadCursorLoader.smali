.class public Lcom/google/android/music/ui/KeepOnDownloadCursorLoader;
.super Landroid/support/v4/content/CursorLoader;
.source "KeepOnDownloadCursorLoader.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;)V

    .line 15
    invoke-virtual {p0, p2}, Lcom/google/android/music/ui/KeepOnDownloadCursorLoader;->setProjection([Ljava/lang/String;)V

    .line 16
    return-void
.end method


# virtual methods
.method public loadInBackground()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/music/store/MusicContent$DownloadContainers;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/KeepOnDownloadCursorLoader;->setUri(Landroid/net/Uri;)V

    .line 21
    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/google/android/music/ui/KeepOnDownloadCursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
