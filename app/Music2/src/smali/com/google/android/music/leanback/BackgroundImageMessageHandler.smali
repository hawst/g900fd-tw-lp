.class Lcom/google/android/music/leanback/BackgroundImageMessageHandler;
.super Landroid/os/Handler;
.source "BackgroundImageMessageHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetter;,
        Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDefaultDrawable:Landroid/graphics/drawable/Drawable;

.field private final mGridDrawable:Landroid/graphics/drawable/Drawable;

.field private final mIdleListener:Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask$OnIdleListener;

.field private mItem:Lcom/google/android/music/leanback/Item;

.field private mLastLoadingStartTime:J

.field private final mTargetHeight:I

.field private final mTargetWidth:I

.field private final mWindow:Landroid/view/Window;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->TAG:Ljava/lang/String;

    .line 41
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ALBUM_ART:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->DEBUG:Z

    return-void
.end method

.method constructor <init>(Landroid/view/Window;Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V
    .locals 5
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "defaultDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 85
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 67
    new-instance v3, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$1;

    invoke-direct {v3, p0}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$1;-><init>(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)V

    iput-object v3, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mIdleListener:Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask$OnIdleListener;

    .line 86
    iput-object p1, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mWindow:Landroid/view/Window;

    .line 87
    iput-object p2, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mContext:Landroid/content/Context;

    .line 88
    iput-object p3, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mDefaultDrawable:Landroid/graphics/drawable/Drawable;

    .line 89
    const-string v3, "window"

    invoke-virtual {p2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 90
    .local v2, "wm":Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 91
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 92
    .local v1, "size":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 93
    iget v3, v1, Landroid/graphics/Point;->y:I

    iput v3, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mTargetHeight:I

    .line 94
    iget v3, v1, Landroid/graphics/Point;->x:I

    iput v3, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mTargetWidth:I

    .line 95
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0201c0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mGridDrawable:Landroid/graphics/drawable/Drawable;

    .line 96
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 38
    sget-boolean v0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->DEBUG:Z

    return v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)Lcom/google/android/music/leanback/Item;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mItem:Lcom/google/android/music/leanback/Item;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;Lcom/google/android/music/leanback/Item;)Lcom/google/android/music/leanback/Item;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/BackgroundImageMessageHandler;
    .param p1, "x1"    # Lcom/google/android/music/leanback/Item;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mItem:Lcom/google/android/music/leanback/Item;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mLastLoadingStartTime:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;JLcom/google/android/music/leanback/Item;)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/BackgroundImageMessageHandler;
    .param p1, "x1"    # J
    .param p3, "x2"    # Lcom/google/android/music/leanback/Item;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->loadItemBackground(JLcom/google/android/music/leanback/Item;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mTargetWidth:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mTargetHeight:I

    return v0
.end method

.method private loadImage(Landroid/util/Pair;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 179
    .local p1, "timeItemPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Object;>;"
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    instance-of v0, v0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetter;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetter;

    new-instance v1, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$2;-><init>(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;Landroid/util/Pair;)V

    invoke-interface {v0, v1}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetter;->getItem(Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;)V

    .line 189
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/leanback/Item;

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->loadItemBackground(JLcom/google/android/music/leanback/Item;)V

    goto :goto_0
.end method

.method private loadItemBackground(JLcom/google/android/music/leanback/Item;)V
    .locals 9
    .param p1, "startTime"    # J
    .param p3, "item"    # Lcom/google/android/music/leanback/Item;

    .prologue
    const/4 v2, 0x0

    .line 192
    iput-object p3, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mItem:Lcom/google/android/music/leanback/Item;

    .line 193
    iput-wide p1, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mLastLoadingStartTime:J

    .line 195
    invoke-static {}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->getTaskCount()I

    move-result v7

    .line 197
    .local v7, "taskCount":I
    sget-boolean v0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 198
    sget-object v3, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadItemBackground "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/google/android/music/leanback/Item;->getTitle()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " item "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " task count "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :cond_0
    if-nez p3, :cond_2

    move-object v1, v2

    .line 204
    .local v1, "bitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;
    :goto_1
    if-lez v7, :cond_3

    .line 244
    :goto_2
    return-void

    .end local v1    # "bitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;
    :cond_1
    move-object v0, v2

    .line 198
    goto :goto_0

    .line 202
    :cond_2
    invoke-virtual {p3}, Lcom/google/android/music/leanback/Item;->getBitmapGettersGetter()Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    move-result-object v1

    goto :goto_1

    .line 208
    .restart local v1    # "bitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mTargetHeight:I

    iget v3, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mTargetHeight:I

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-instance v6, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;

    invoke-direct {v6, p0, p1, p2, p3}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;-><init>(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;JLcom/google/android/music/leanback/Item;)V

    invoke-static/range {v0 .. v6}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getBitmap(Landroid/content/Context;Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;IIZZLcom/google/android/music/leanback/bitmap/BitmapListener;)V

    goto :goto_2
.end method

.method private postItemSelectedMessage(Ljava/lang/Object;JJ)V
    .locals 6
    .param p1, "item"    # Ljava/lang/Object;
    .param p2, "loadDelayMs"    # J
    .param p4, "startTimeOffsetMs"    # J

    .prologue
    const/4 v4, 0x1

    .line 126
    invoke-virtual {p0, v4}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->removeMessages(I)V

    .line 127
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->removeMessages(I)V

    .line 128
    new-instance v0, Landroid/util/Pair;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, p4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {p0, v4, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 131
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 152
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 176
    :goto_0
    return-void

    .line 154
    :pswitch_0
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Landroid/util/Pair;

    invoke-direct {p0, v4}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->loadImage(Landroid/util/Pair;)V

    goto :goto_0

    .line 157
    :pswitch_1
    iget-object v4, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mWindow:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 158
    .local v1, "currentBackground":Landroid/graphics/drawable/Drawable;
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 159
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    new-array v4, v9, [Landroid/graphics/drawable/Drawable;

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v6, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v5, v4, v8

    iget-object v5, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mGridDrawable:Landroid/graphics/drawable/Drawable;

    aput-object v5, v4, v7

    invoke-direct {v2, v4}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 162
    .local v2, "newBackground":Landroid/graphics/drawable/Drawable;
    :goto_1
    new-instance v3, Landroid/graphics/drawable/TransitionDrawable;

    new-array v4, v9, [Landroid/graphics/drawable/Drawable;

    aput-object v1, v4, v8

    aput-object v2, v4, v7

    invoke-direct {v3, v4}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 164
    .local v3, "transitionDrawable":Landroid/graphics/drawable/TransitionDrawable;
    iget-object v4, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mWindow:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 165
    invoke-virtual {v3, v7}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 166
    const/16 v4, 0x12c

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 167
    const/4 v4, 0x3

    invoke-static {p0, v4, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    const-wide/16 v6, 0x12c

    invoke-virtual {p0, v4, v6, v7}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 159
    .end local v2    # "newBackground":Landroid/graphics/drawable/Drawable;
    .end local v3    # "transitionDrawable":Landroid/graphics/drawable/TransitionDrawable;
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mDefaultDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    .line 171
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "currentBackground":Landroid/graphics/drawable/Drawable;
    :pswitch_2
    iget-object v4, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mWindow:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method onActivityStarted()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mIdleListener:Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask$OnIdleListener;

    invoke-static {v0}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->setOnIdleListener(Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask$OnIdleListener;)V

    .line 103
    return-void
.end method

.method postDisplayBitmapMessage(JLandroid/graphics/Bitmap;)V
    .locals 7
    .param p1, "startTime"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v6, 0x2

    .line 140
    invoke-virtual {p0, v6}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->removeMessages(I)V

    .line 141
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 142
    .local v0, "currentTime":J
    const-wide/16 v4, 0x2bc

    add-long/2addr v4, p1

    sub-long v2, v4, v0

    .line 143
    .local v2, "delayTime":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 144
    invoke-static {p0, v6, p3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {p0, v4, v2, v3}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 148
    :goto_0
    return-void

    .line 146
    :cond_0
    invoke-static {p0, v6, p3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method postItemSelectedMessage(Ljava/lang/Object;)V
    .locals 6
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 112
    const-wide/16 v2, 0x1f4

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->postItemSelectedMessage(Ljava/lang/Object;JJ)V

    .line 113
    return-void
.end method

.method postItemSelectedMessageImmediate(Ljava/lang/Object;)V
    .locals 6
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 122
    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x2bc

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->postItemSelectedMessage(Ljava/lang/Object;JJ)V

    .line 123
    return-void
.end method
