.class public Lcom/google/android/music/MusicUserContentBinder;
.super Lcom/google/android/play/IUserContentService$Stub;
.source "MusicUserContentBinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/MusicUserContentBinder$MusicUserContentService;
    }
.end annotation


# static fields
.field private static sNotifier:Lcom/google/android/music/MusicUserContentNotifier;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/music/MusicUserContentNotifier;

    invoke-direct {v0}, Lcom/google/android/music/MusicUserContentNotifier;-><init>()V

    sput-object v0, Lcom/google/android/music/MusicUserContentBinder;->sNotifier:Lcom/google/android/music/MusicUserContentNotifier;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/play/IUserContentService$Stub;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/google/android/music/MusicUserContentBinder;->mContext:Landroid/content/Context;

    .line 55
    sget-object v0, Lcom/google/android/music/MusicUserContentBinder;->sNotifier:Lcom/google/android/music/MusicUserContentNotifier;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/MusicUserContentNotifier;->setContext(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method private getPromoteNautilus()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 92
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 93
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/google/android/music/MusicUserContentBinder;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/ui/HomeActivity;->createHomeScreenIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 94
    .local v2, "viewIntent":Landroid/content/Intent;
    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    const-string v3, "Play.ViewIntent"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 96
    const-string v3, "Play.LastUpdateTimeMillis"

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 97
    const-string v3, "Play.ImageUri"

    invoke-static {}, Lcom/google/android/music/store/MusicContent$MusicUserContentArt;->getPromoteNautilusArtUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 99
    const-string v3, "Play.IsGenerated"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 100
    const-string v3, "Play.Reason"

    iget-object v4, p0, Lcom/google/android/music/MusicUserContentBinder;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0356

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    sget-object v3, Lcom/google/android/music/MusicUserContentBinder;->sNotifier:Lcom/google/android/music/MusicUserContentNotifier;

    invoke-virtual {v3}, Lcom/google/android/music/MusicUserContentNotifier;->unregisterListeners()V

    .line 104
    sget-object v3, Lcom/google/android/music/MusicUserContentBinder;->sNotifier:Lcom/google/android/music/MusicUserContentNotifier;

    invoke-virtual {v3}, Lcom/google/android/music/MusicUserContentNotifier;->registerPromoteNautilusListeners()V

    .line 105
    return-object v1
.end method

.method private getWhatsNext(I)Ljava/util/List;
    .locals 4
    .param p1, "numItems"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v2

    .line 114
    .local v2, "whatsNext":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    invoke-direct {p0, v2, p1}, Lcom/google/android/music/MusicUserContentBinder;->populateRecents(Ljava/util/List;I)V

    .line 115
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 117
    iget-object v3, p0, Lcom/google/android/music/MusicUserContentBinder;->mContext:Landroid/content/Context;

    invoke-static {v3, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 119
    .local v0, "pref":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 121
    .local v1, "streamingAccount":Landroid/accounts/Account;
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 125
    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/google/android/music/MusicUserContentBinder;->isSyncPendingOrActive(Landroid/accounts/Account;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 127
    const/4 v2, 0x0

    .line 130
    .end local v0    # "pref":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v1    # "streamingAccount":Landroid/accounts/Account;
    .end local v2    # "whatsNext":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :cond_0
    return-object v2

    .line 121
    .restart local v0    # "pref":Lcom/google/android/music/preferences/MusicPreferences;
    .restart local v2    # "whatsNext":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :catchall_0
    move-exception v3

    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v3
.end method

.method private static isSyncPendingOrActive(Landroid/accounts/Account;)Z
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 134
    const-string v0, "com.google.android.music.MusicContent"

    invoke-static {p0, v0}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.google.android.music.MusicContent"

    invoke-static {p0, v0}, Landroid/content/ContentResolver;->isSyncPending(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private populateRecents(Ljava/util/List;I)V
    .locals 45
    .param p2, "numToRetrieve"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "bundleToAddTo":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    const-string v8, "MusicUserContentService"

    const/4 v9, 0x2

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    .line 146
    .local v4, "LOGV":Z
    const/16 v8, 0x9

    new-array v0, v8, [Ljava/lang/String;

    move-object/from16 v25, v0

    const/4 v8, 0x0

    const-string v9, "RECENT.RecentAlbumId"

    aput-object v9, v25, v8

    const/4 v8, 0x1

    const-string v9, "MUSIC.Album"

    aput-object v9, v25, v8

    const/4 v8, 0x2

    const-string v9, "MUSIC.AlbumArtist"

    aput-object v9, v25, v8

    const/4 v8, 0x3

    const-string v9, "RECENT.RecentListId"

    aput-object v9, v25, v8

    const/4 v8, 0x4

    const-string v9, "LISTS.Name"

    aput-object v9, v25, v8

    const/4 v8, 0x5

    const-string v9, "LISTS.ListType"

    aput-object v9, v25, v8

    const/4 v8, 0x6

    const-string v9, "RECENT.ItemDate"

    aput-object v9, v25, v8

    const/4 v8, 0x7

    const-string v9, "MUSIC.AlbumArtLocation"

    aput-object v9, v25, v8

    const/16 v8, 0x8

    const-string v9, "ARTWORK_CACHE.LocalLocation"

    aput-object v9, v25, v8

    .line 157
    .local v25, "cursorCols":[Ljava/lang/String;
    const/16 v21, 0x0

    .line 158
    .local v21, "albumIdIndex":I
    const/16 v22, 0x1

    .line 159
    .local v22, "albumNameIndex":I
    const/16 v20, 0x2

    .line 160
    .local v20, "albumArtistIndex":I
    const/16 v31, 0x3

    .line 161
    .local v31, "listIdIndex":I
    const/16 v32, 0x4

    .line 162
    .local v32, "listNameIndex":I
    const/16 v34, 0x5

    .line 163
    .local v34, "listTypeIndex":I
    const/16 v26, 0x6

    .line 164
    .local v26, "dateIndex":I
    const/16 v39, 0x7

    .line 165
    .local v39, "remoteArtworkLocationIndex":I
    const/16 v35, 0x8

    .line 167
    .local v35, "localArtworkLocationIndex":I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/MusicUserContentBinder;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v8

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Lcom/google/android/music/store/Store;->getRecentsJoinedWithArtwork([Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    .line 168
    .local v24, "c":Landroid/database/Cursor;
    if-nez v24, :cond_0

    .line 169
    const-string v8, "MusicUserContentService"

    const-string v9, "Recent URI returned a null cursor.  Not returning any recents"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :goto_0
    return-void

    .line 173
    :cond_0
    :try_start_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v8

    add-int v27, v8, p2

    .line 174
    .local v27, "finalListSize":I
    invoke-static {}, Lcom/google/android/play/utils/collections/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v40

    .line 175
    .local v40, "remoteUrlsToWatch":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_1
    :goto_1
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_e

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v8

    move/from16 v0, v27

    if-ge v8, v0, :cond_e

    .line 176
    const/4 v8, 0x6

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v42

    .line 178
    .local v42, "updatedDate":J
    const-wide/16 v6, -0x1

    .line 179
    .local v6, "albumId":J
    const-wide/16 v36, -0x1

    .line 180
    .local v36, "playlistId":J
    const/16 v33, -0x1

    .line 181
    .local v33, "listType":I
    const/16 v44, 0x0

    .line 183
    .local v44, "willBeGeneratedImage":Z
    const/4 v8, 0x0

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-nez v8, :cond_2

    .line 184
    const/4 v8, 0x0

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 186
    :cond_2
    const/4 v8, 0x3

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-nez v8, :cond_3

    .line 187
    const/4 v8, 0x3

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v36

    .line 189
    :cond_3
    const/4 v8, 0x5

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-nez v8, :cond_4

    .line 190
    const/4 v8, 0x5

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    .line 194
    :cond_4
    const/16 v29, 0x0

    .line 195
    .local v29, "imageUri":Landroid/net/Uri;
    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-eqz v8, :cond_b

    .line 196
    new-instance v5, Lcom/google/android/music/medialist/AlbumSongList;

    const/4 v8, 0x1

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    move-object/from16 v0, v24

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-direct/range {v5 .. v10}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JLjava/lang/String;Ljava/lang/String;Z)V

    .line 199
    .local v5, "mediaList":Lcom/google/android/music/medialist/SongList;
    const/4 v8, 0x7

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-nez v8, :cond_6

    const/16 v28, 0x1

    .line 200
    .local v28, "hasRemoteLocation":Z
    :goto_2
    if-eqz v28, :cond_7

    const/4 v8, 0x7

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v38

    .line 203
    .local v38, "remoteArtworkLocation":Ljava/lang/String;
    :goto_3
    if-eqz v28, :cond_8

    invoke-static/range {v38 .. v38}, Lcom/google/android/music/download/cache/CacheUtils;->isRemoteLocationSideLoaded(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    const/16 v30, 0x1

    .line 205
    .local v30, "isSideloadedImage":Z
    :goto_4
    const/16 v8, 0x8

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-eqz v8, :cond_9

    if-nez v30, :cond_9

    const/16 v44, 0x1

    .line 207
    :goto_5
    if-eqz v28, :cond_5

    if-nez v30, :cond_5

    if-eqz v44, :cond_5

    .line 208
    move-object/from16 v0, v40

    move-object/from16 v1, v38

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 213
    :cond_5
    if-eqz v44, :cond_a

    .line 214
    const/4 v8, -0x1

    const/4 v9, -0x1

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getFauxAlbumArtUri(JII)Landroid/net/Uri;

    move-result-object v29

    .line 241
    .end local v28    # "hasRemoteLocation":Z
    .end local v30    # "isSideloadedImage":Z
    .end local v38    # "remoteArtworkLocation":Ljava/lang/String;
    :goto_6
    new-instance v23, Landroid/os/Bundle;

    invoke-direct/range {v23 .. v23}, Landroid/os/Bundle;-><init>()V

    .line 242
    .local v23, "bundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/MusicUserContentBinder;->mContext:Landroid/content/Context;

    invoke-static {v8, v5}, Lcom/google/android/music/ui/AppNavigation;->getShowSonglistIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v41

    .line 243
    .local v41, "viewIntent":Landroid/content/Intent;
    const-string v8, "Play.ViewIntent"

    move-object/from16 v0, v23

    move-object/from16 v1, v41

    invoke-virtual {v0, v8, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 244
    const-string v8, "Play.LastUpdateTimeMillis"

    move-object/from16 v0, v23

    move-wide/from16 v1, v42

    invoke-virtual {v0, v8, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 245
    const-string v8, "Play.ImageUri"

    move-object/from16 v0, v23

    move-object/from16 v1, v29

    invoke-virtual {v0, v8, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 246
    const-string v8, "Play.IsGenerated"

    move-object/from16 v0, v23

    move/from16 v1, v44

    invoke-virtual {v0, v8, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 249
    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    if-eqz v4, :cond_1

    .line 252
    const-string v8, "MusicUserContentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Adding bundle to return: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v23

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 263
    .end local v5    # "mediaList":Lcom/google/android/music/medialist/SongList;
    .end local v6    # "albumId":J
    .end local v23    # "bundle":Landroid/os/Bundle;
    .end local v27    # "finalListSize":I
    .end local v29    # "imageUri":Landroid/net/Uri;
    .end local v33    # "listType":I
    .end local v36    # "playlistId":J
    .end local v40    # "remoteUrlsToWatch":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v41    # "viewIntent":Landroid/content/Intent;
    .end local v42    # "updatedDate":J
    .end local v44    # "willBeGeneratedImage":Z
    :catchall_0
    move-exception v8

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    throw v8

    .line 199
    .restart local v5    # "mediaList":Lcom/google/android/music/medialist/SongList;
    .restart local v6    # "albumId":J
    .restart local v27    # "finalListSize":I
    .restart local v29    # "imageUri":Landroid/net/Uri;
    .restart local v33    # "listType":I
    .restart local v36    # "playlistId":J
    .restart local v40    # "remoteUrlsToWatch":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v42    # "updatedDate":J
    .restart local v44    # "willBeGeneratedImage":Z
    :cond_6
    const/16 v28, 0x0

    goto/16 :goto_2

    .line 200
    .restart local v28    # "hasRemoteLocation":Z
    :cond_7
    const/16 v38, 0x0

    goto/16 :goto_3

    .line 203
    .restart local v38    # "remoteArtworkLocation":Ljava/lang/String;
    :cond_8
    const/16 v30, 0x0

    goto/16 :goto_4

    .line 205
    .restart local v30    # "isSideloadedImage":Z
    :cond_9
    const/16 v44, 0x0

    goto :goto_5

    .line 216
    :cond_a
    const/4 v8, 0x1

    const/4 v9, -0x1

    const/4 v10, -0x1

    :try_start_1
    invoke-static {v6, v7, v8, v9, v10}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v29

    goto :goto_6

    .line 220
    .end local v5    # "mediaList":Lcom/google/android/music/medialist/SongList;
    .end local v28    # "hasRemoteLocation":Z
    .end local v30    # "isSideloadedImage":Z
    .end local v38    # "remoteArtworkLocation":Ljava/lang/String;
    :cond_b
    const-wide/16 v8, -0x1

    cmp-long v8, v36, v8

    if-nez v8, :cond_c

    .line 221
    const-string v8, "MusicUserContentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Recents must return an album or playlist (albumId:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " playlistId: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-wide/from16 v0, v36

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 226
    :cond_c
    const/16 v8, 0xa

    move/from16 v0, v33

    if-ne v0, v8, :cond_d

    .line 229
    if-eqz v4, :cond_1

    .line 230
    const-string v8, "MusicUserContentService"

    const-string v9, "Recents should not contain the play queue."

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 234
    :cond_d
    new-instance v5, Lcom/google/android/music/medialist/PlaylistSongList;

    const/4 v8, 0x3

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const/4 v8, 0x4

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v8, 0x5

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object v9, v5

    invoke-direct/range {v9 .. v19}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 237
    .restart local v5    # "mediaList":Lcom/google/android/music/medialist/SongList;
    const/4 v8, -0x1

    const/4 v9, -0x1

    move-wide/from16 v0, v36

    invoke-static {v0, v1, v8, v9}, Lcom/google/android/music/store/MusicContent$PlaylistArt;->getPlaylistArtUri(JII)Landroid/net/Uri;

    move-result-object v29

    .line 238
    const/16 v44, 0x1

    goto/16 :goto_6

    .line 256
    .end local v5    # "mediaList":Lcom/google/android/music/medialist/SongList;
    .end local v6    # "albumId":J
    .end local v29    # "imageUri":Landroid/net/Uri;
    .end local v33    # "listType":I
    .end local v36    # "playlistId":J
    .end local v42    # "updatedDate":J
    .end local v44    # "willBeGeneratedImage":Z
    :cond_e
    sget-object v8, Lcom/google/android/music/MusicUserContentBinder;->sNotifier:Lcom/google/android/music/MusicUserContentNotifier;

    invoke-virtual {v8}, Lcom/google/android/music/MusicUserContentNotifier;->unregisterListeners()V

    .line 257
    sget-object v8, Lcom/google/android/music/MusicUserContentBinder;->sNotifier:Lcom/google/android/music/MusicUserContentNotifier;

    move-object/from16 v0, v40

    invoke-virtual {v8, v0}, Lcom/google/android/music/MusicUserContentNotifier;->registerArtListeners(Ljava/util/Set;)V

    .line 261
    sget-object v8, Lcom/google/android/music/MusicUserContentBinder;->sNotifier:Lcom/google/android/music/MusicUserContentNotifier;

    invoke-virtual {v8}, Lcom/google/android/music/MusicUserContentNotifier;->registerPromoteNautilusListeners()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 263
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public static shouldPromoteNautilus(Landroid/content/Context;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 272
    new-instance v5, Ljava/lang/Object;

    invoke-direct {v5}, Ljava/lang/Object;-><init>()V

    .line 273
    .local v5, "refObject":Ljava/lang/Object;
    invoke-static {p0, v5}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    .line 275
    .local v4, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    .line 276
    .local v2, "hasNautilus":Z
    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->wasTutorialViewed()Z

    move-result v6

    .line 277
    .local v6, "wasTutorialViewed":Z
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "music_play_widget_promote_nautilus"

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    .line 280
    .local v1, "gServicesEnabled":Z
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isPlayWidgetPromoteNautilusEnabled()Z

    move-result v0

    .line 281
    .local v0, "configEnabled":Z
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isNautilusFreeTrialAvailable()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 283
    .local v3, "nautilusAvailable":Z
    if-nez v2, :cond_0

    if-eqz v3, :cond_0

    if-nez v6, :cond_0

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v7, 0x1

    .line 286
    :cond_0
    invoke-static {v5}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return v7

    .end local v0    # "configEnabled":Z
    .end local v1    # "gServicesEnabled":Z
    .end local v2    # "hasNautilus":Z
    .end local v3    # "nautilusAvailable":Z
    .end local v6    # "wasTutorialViewed":Z
    :catchall_0
    move-exception v7

    invoke-static {v5}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v7
.end method


# virtual methods
.method public getDocuments(II)Ljava/util/List;
    .locals 6
    .param p1, "dataTypeToFetch"    # I
    .param p2, "numItemsToReturn"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 60
    packed-switch p1, :pswitch_data_0

    .line 80
    const-string v1, "MusicUserContentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown dataTypeToFetch: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1

    .line 63
    :pswitch_0
    const/4 v0, 0x0

    .line 64
    .local v0, "shouldPromoteNautilus":Z
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 66
    .local v2, "token":J
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/MusicUserContentBinder;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/MusicUserContentBinder;->shouldPromoteNautilus(Landroid/content/Context;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 68
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 71
    sget-object v1, Lcom/google/android/music/MusicUserContentBinder;->sNotifier:Lcom/google/android/music/MusicUserContentNotifier;

    invoke-virtual {v1, v0}, Lcom/google/android/music/MusicUserContentNotifier;->setPrevShouldPromoteNautilus(Z)V

    .line 73
    if-eqz v0, :cond_0

    .line 74
    invoke-direct {p0}, Lcom/google/android/music/MusicUserContentBinder;->getPromoteNautilus()Ljava/util/List;

    move-result-object v1

    .line 76
    :goto_0
    return-object v1

    .line 68
    :catchall_0
    move-exception v1

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v1

    .line 76
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/music/MusicUserContentBinder;->getWhatsNext(I)Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
