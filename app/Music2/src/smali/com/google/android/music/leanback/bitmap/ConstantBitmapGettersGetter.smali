.class public Lcom/google/android/music/leanback/bitmap/ConstantBitmapGettersGetter;
.super Ljava/lang/Object;
.source "ConstantBitmapGettersGetter.java"

# interfaces
.implements Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;


# instance fields
.field private final mBitmapGetters:[Lcom/google/android/music/leanback/bitmap/BitmapGetter;


# direct methods
.method public constructor <init>([Lcom/google/android/music/leanback/bitmap/BitmapGetter;)V
    .locals 0
    .param p1, "bitmapGetters"    # [Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/google/android/music/leanback/bitmap/ConstantBitmapGettersGetter;->mBitmapGetters:[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    .line 10
    return-void
.end method


# virtual methods
.method public getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/ConstantBitmapGettersGetter;->mBitmapGetters:[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    return-object v0
.end method
