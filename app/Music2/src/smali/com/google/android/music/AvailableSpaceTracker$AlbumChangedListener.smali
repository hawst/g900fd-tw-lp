.class public interface abstract Lcom/google/android/music/AvailableSpaceTracker$AlbumChangedListener;
.super Ljava/lang/Object;
.source "AvailableSpaceTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AvailableSpaceTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AlbumChangedListener"
.end annotation


# virtual methods
.method public abstract onAlbumChanged(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation
.end method
