.class final Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$3;
.super Ljava/lang/Object;
.source "DocumentClickHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->onDocumentClick(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$doc:Lcom/google/android/music/ui/cardlib/model/Document;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$3;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$3;->val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$3;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$3;->val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0, v1}, Lcom/google/android/music/eventlog/MusicEventLogger;->logMainstageCardClicked(Lcom/google/android/music/ui/cardlib/model/Document;)V

    .line 203
    return-void
.end method
