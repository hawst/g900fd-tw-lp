.class public Lcom/google/android/music/store/QueueUtils;
.super Ljava/lang/Object;
.source "QueueUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/QueueUtils$1;,
        Lcom/google/android/music/store/QueueUtils$MoveMode;,
        Lcom/google/android/music/store/QueueUtils$ItemIdAndOrders;
    }
.end annotation


# direct methods
.method private static addToDatabase(Landroid/content/Context;Lcom/google/android/music/medialist/ExternalSongList;)Lcom/google/android/music/medialist/SongList;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "externalSongList"    # Lcom/google/android/music/medialist/ExternalSongList;

    .prologue
    .line 220
    const/4 v2, 0x0

    invoke-virtual {p1, p0, v2}, Lcom/google/android/music/medialist/ExternalSongList;->addToStore(Landroid/content/Context;Z)[J

    move-result-object v1

    .line 221
    .local v1, "localIds":[J
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    .line 222
    invoke-virtual {p1, p0}, Lcom/google/android/music/medialist/ExternalSongList;->getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 223
    .local v0, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v2, Lcom/google/android/music/medialist/SelectedSongList;

    invoke-direct {v2, v0, v1}, Lcom/google/android/music/medialist/SelectedSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;[J)V

    .line 225
    .end local v0    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method static appendQueueItems(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;IJIJJZJ)V
    .locals 14
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "musicCursor"    # Landroid/database/Cursor;
    .param p2, "maxSizeToAdd"    # I
    .param p3, "queueContainerId"    # J
    .param p5, "state"    # I
    .param p6, "startItemOrder"    # J
    .param p8, "startItemUnshuffledOrder"    # J
    .param p10, "isInCloudQueueMode"    # Z
    .param p11, "queueVersion"    # J

    .prologue
    .line 88
    if-nez p1, :cond_0

    .line 111
    :goto_0
    return-void

    .line 92
    :cond_0
    move/from16 v0, p10

    invoke-static {p0, v0}, Lcom/google/android/music/store/QueueItem;->compileInsertStatement(Landroid/database/sqlite/SQLiteDatabase;Z)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    .local v2, "insert":Landroid/database/sqlite/SQLiteStatement;
    move-wide/from16 v10, p8

    .end local p8    # "startItemUnshuffledOrder":J
    .local v10, "startItemUnshuffledOrder":J
    move-wide/from16 v8, p6

    .end local p6    # "startItemOrder":J
    .local v8, "startItemOrder":J
    move/from16 v3, p2

    .line 94
    .end local p2    # "maxSizeToAdd":I
    .local v3, "maxSizeToAdd":I
    :goto_1
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-eqz v7, :cond_1

    add-int/lit8 p2, v3, -0x1

    .end local v3    # "maxSizeToAdd":I
    .restart local p2    # "maxSizeToAdd":I
    if-lez v3, :cond_2

    .line 95
    :try_start_1
    new-instance v6, Lcom/google/android/music/store/QueueItem;

    invoke-direct {v6}, Lcom/google/android/music/store/QueueItem;-><init>()V

    .line 96
    .local v6, "queueItem":Lcom/google/android/music/store/QueueItem;
    const/4 v7, 0x0

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 98
    .local v4, "musicId":J
    invoke-virtual {v6}, Lcom/google/android/music/store/QueueItem;->reset()V

    .line 99
    invoke-virtual {v6, v4, v5}, Lcom/google/android/music/store/QueueItem;->setMusicId(J)V

    .line 100
    move-wide/from16 v0, p3

    invoke-virtual {v6, v0, v1}, Lcom/google/android/music/store/QueueItem;->setQueueContainerId(J)V

    .line 101
    move/from16 v0, p5

    invoke-virtual {v6, v0}, Lcom/google/android/music/store/QueueItem;->setState(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 102
    const-wide/16 v12, 0x1

    add-long p6, v8, v12

    .end local v8    # "startItemOrder":J
    .restart local p6    # "startItemOrder":J
    :try_start_2
    invoke-virtual {v6, v8, v9}, Lcom/google/android/music/store/QueueItem;->setItemOrder(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 103
    const-wide/16 v12, 0x1

    add-long p8, v10, v12

    .end local v10    # "startItemUnshuffledOrder":J
    .restart local p8    # "startItemUnshuffledOrder":J
    :try_start_3
    invoke-virtual {v6, v10, v11}, Lcom/google/android/music/store/QueueItem;->setItemUnshuffledOrder(J)V

    .line 104
    move-wide/from16 v0, p11

    invoke-virtual {v6, v0, v1}, Lcom/google/android/music/store/QueueItem;->setVersion(J)V

    .line 105
    invoke-virtual {v6}, Lcom/google/android/music/store/QueueItem;->generateNewCloudQueueId()V

    .line 106
    invoke-virtual {v6, v2}, Lcom/google/android/music/store/QueueItem;->insert(Landroid/database/sqlite/SQLiteStatement;)J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-wide/from16 v10, p8

    .end local p8    # "startItemUnshuffledOrder":J
    .restart local v10    # "startItemUnshuffledOrder":J
    move-wide/from16 v8, p6

    .end local p6    # "startItemOrder":J
    .restart local v8    # "startItemOrder":J
    move/from16 v3, p2

    .line 107
    .end local p2    # "maxSizeToAdd":I
    .restart local v3    # "maxSizeToAdd":I
    goto :goto_1

    .end local v4    # "musicId":J
    .end local v6    # "queueItem":Lcom/google/android/music/store/QueueItem;
    :cond_1
    move/from16 p2, v3

    .line 109
    .end local v3    # "maxSizeToAdd":I
    .restart local p2    # "maxSizeToAdd":I
    :cond_2
    invoke-static {v2}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    move-wide/from16 p8, v10

    .end local v10    # "startItemUnshuffledOrder":J
    .restart local p8    # "startItemUnshuffledOrder":J
    move-wide/from16 p6, v8

    .line 111
    .end local v8    # "startItemOrder":J
    .restart local p6    # "startItemOrder":J
    goto :goto_0

    .line 109
    .end local p2    # "maxSizeToAdd":I
    .end local p6    # "startItemOrder":J
    .end local p8    # "startItemUnshuffledOrder":J
    .restart local v3    # "maxSizeToAdd":I
    .restart local v8    # "startItemOrder":J
    .restart local v10    # "startItemUnshuffledOrder":J
    :catchall_0
    move-exception v7

    move-wide/from16 p8, v10

    .end local v10    # "startItemUnshuffledOrder":J
    .restart local p8    # "startItemUnshuffledOrder":J
    move-wide/from16 p6, v8

    .end local v8    # "startItemOrder":J
    .restart local p6    # "startItemOrder":J
    move/from16 p2, v3

    .end local v3    # "maxSizeToAdd":I
    .restart local p2    # "maxSizeToAdd":I
    :goto_2
    invoke-static {v2}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    throw v7

    .end local p6    # "startItemOrder":J
    .end local p8    # "startItemUnshuffledOrder":J
    .restart local v8    # "startItemOrder":J
    .restart local v10    # "startItemUnshuffledOrder":J
    :catchall_1
    move-exception v7

    move-wide/from16 p8, v10

    .end local v10    # "startItemUnshuffledOrder":J
    .restart local p8    # "startItemUnshuffledOrder":J
    move-wide/from16 p6, v8

    .end local v8    # "startItemOrder":J
    .restart local p6    # "startItemOrder":J
    goto :goto_2

    .end local p8    # "startItemUnshuffledOrder":J
    .restart local v4    # "musicId":J
    .restart local v6    # "queueItem":Lcom/google/android/music/store/QueueItem;
    .restart local v10    # "startItemUnshuffledOrder":J
    :catchall_2
    move-exception v7

    move-wide/from16 p8, v10

    .end local v10    # "startItemUnshuffledOrder":J
    .restart local p8    # "startItemUnshuffledOrder":J
    goto :goto_2

    :catchall_3
    move-exception v7

    goto :goto_2
.end method

.method public static getSongList(Landroid/content/Context;Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;)Lcom/google/android/music/medialist/SongList;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "containerType"    # Lcom/google/android/music/store/ContainerDescriptor$Type;
    .param p2, "containerId"    # J
    .param p4, "containerExtId"    # Ljava/lang/String;

    .prologue
    .line 121
    const/4 v3, 0x0

    .line 123
    .local v3, "songList":Lcom/google/android/music/medialist/SongList;
    sget-object v4, Lcom/google/android/music/store/QueueUtils$1;->$SwitchMap$com$google$android$music$store$ContainerDescriptor$Type:[I

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/store/ContainerDescriptor$Type;->ordinal()I

    move-result v6

    aget v4, v4, v6

    packed-switch v4, :pswitch_data_0

    .line 212
    const-string v4, "QueueUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unhandled containerType: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :goto_0
    return-object v3

    .line 125
    :pswitch_0
    new-instance v3, Lcom/google/android/music/medialist/PlaylistSongList;

    .end local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-wide/from16 v4, p2

    invoke-direct/range {v3 .. v13}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 127
    .restart local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    goto :goto_0

    .line 130
    :pswitch_1
    new-instance v15, Ljava/lang/Object;

    invoke-direct {v15}, Ljava/lang/Object;-><init>()V

    .line 131
    .local v15, "refObject":Ljava/lang/Object;
    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v14

    .line 133
    .local v14, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const-wide/16 v6, -0x4

    const/4 v4, 0x0

    :try_start_0
    invoke-static {v6, v7, v4, v14}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 137
    invoke-static {v15}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-static {v15}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v4

    .line 143
    .end local v14    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v15    # "refObject":Ljava/lang/Object;
    :pswitch_2
    new-instance v15, Ljava/lang/Object;

    invoke-direct {v15}, Ljava/lang/Object;-><init>()V

    .line 144
    .restart local v15    # "refObject":Ljava/lang/Object;
    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v14

    .line 146
    .restart local v14    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const-wide/16 v6, -0x1

    const/4 v4, 0x0

    :try_start_1
    invoke-static {v6, v7, v4, v14}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 150
    invoke-static {v15}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_1
    move-exception v4

    invoke-static {v15}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v4

    .line 156
    .end local v14    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v15    # "refObject":Ljava/lang/Object;
    :pswitch_3
    new-instance v15, Ljava/lang/Object;

    invoke-direct {v15}, Ljava/lang/Object;-><init>()V

    .line 157
    .restart local v15    # "refObject":Ljava/lang/Object;
    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v14

    .line 159
    .restart local v14    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const-wide/16 v6, -0x3

    const/4 v4, 0x0

    :try_start_2
    invoke-static {v6, v7, v4, v14}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v3

    .line 163
    invoke-static {v15}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_2
    move-exception v4

    invoke-static {v15}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v4

    .line 169
    .end local v14    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v15    # "refObject":Ljava/lang/Object;
    :pswitch_4
    new-instance v15, Ljava/lang/Object;

    invoke-direct {v15}, Ljava/lang/Object;-><init>()V

    .line 170
    .restart local v15    # "refObject":Ljava/lang/Object;
    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v14

    .line 172
    .restart local v14    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const-wide/16 v6, -0x2

    const/4 v4, 0x0

    :try_start_3
    invoke-static {v6, v7, v4, v14}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-object v3

    .line 176
    invoke-static {v15}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_3
    move-exception v4

    invoke-static {v15}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v4

    .line 181
    .end local v14    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v15    # "refObject":Ljava/lang/Object;
    :pswitch_5
    new-instance v5, Lcom/google/android/music/medialist/SharedWithMeSongList;

    const-wide/16 v6, -0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v8, p4

    invoke-direct/range {v5 .. v13}, Lcom/google/android/music/medialist/SharedWithMeSongList;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    .local v5, "sharedWithMeSongList":Lcom/google/android/music/medialist/SharedWithMeSongList;
    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/google/android/music/store/QueueUtils;->addToDatabase(Landroid/content/Context;Lcom/google/android/music/medialist/ExternalSongList;)Lcom/google/android/music/medialist/SongList;

    move-result-object v3

    .line 184
    goto/16 :goto_0

    .line 187
    .end local v5    # "sharedWithMeSongList":Lcom/google/android/music/medialist/SharedWithMeSongList;
    :pswitch_6
    new-instance v3, Lcom/google/android/music/medialist/AlbumSongList;

    .end local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    const/4 v4, 0x1

    move-wide/from16 v0, p2

    invoke-direct {v3, v0, v1, v4}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    .line 188
    .restart local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    goto/16 :goto_0

    .line 191
    :pswitch_7
    new-instance v2, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    move-object/from16 v0, p4

    invoke-direct {v2, v0}, Lcom/google/android/music/medialist/NautilusAlbumSongList;-><init>(Ljava/lang/String;)V

    .line 193
    .local v2, "nautilusAlbumSongList":Lcom/google/android/music/medialist/NautilusAlbumSongList;
    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/music/store/QueueUtils;->addToDatabase(Landroid/content/Context;Lcom/google/android/music/medialist/ExternalSongList;)Lcom/google/android/music/medialist/SongList;

    move-result-object v3

    .line 194
    goto/16 :goto_0

    .line 208
    .end local v2    # "nautilusAlbumSongList":Lcom/google/android/music/medialist/NautilusAlbumSongList;
    :pswitch_8
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/store/QueueUtils;->logGetSongsNotImplemented(Lcom/google/android/music/store/ContainerDescriptor$Type;)V

    goto/16 :goto_0

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public static getTracksCursor(Landroid/content/Context;Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "containerType"    # Lcom/google/android/music/store/ContainerDescriptor$Type;
    .param p2, "containerId"    # J
    .param p4, "containerExtId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 240
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/music/store/QueueUtils;->getSongList(Landroid/content/Context;Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    .line 242
    .local v0, "songList":Lcom/google/android/music/medialist/SongList;
    if-nez v0, :cond_0

    .line 243
    const-string v2, "QueueUtils"

    const-string v3, "getTracksCursor: Failed to handle containerType=%s containerId=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    :goto_0
    return-object v1

    .line 248
    :cond_0
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "audio_id"

    aput-object v3, v2, v5

    invoke-virtual {v0, p0, v2, v1}, Lcom/google/android/music/medialist/SongList;->getSyncMediaCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;

    move-result-object v1

    .line 253
    .local v1, "tracksCursor":Landroid/database/Cursor;
    goto :goto_0
.end method

.method private static logGetSongsNotImplemented(Lcom/google/android/music/store/ContainerDescriptor$Type;)V
    .locals 4
    .param p0, "containerType"    # Lcom/google/android/music/store/ContainerDescriptor$Type;

    .prologue
    .line 114
    const-string v0, "QueueUtils"

    const-string v1, "getSongList for type=%s NOT IMPLEMENTED"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    return-void
.end method

.method public static notifyChange(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/store/MusicContent$Queue;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 47
    return-void
.end method
