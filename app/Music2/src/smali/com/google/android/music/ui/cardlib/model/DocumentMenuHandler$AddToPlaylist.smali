.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;
.source "DocumentMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddToPlaylist"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V
    .locals 2
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 981
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 982
    const v0, 0x7f0e0023

    const v1, 0x7f0b00c9

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;-><init>(Landroid/content/res/Resources;II)V

    .line 983
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 8

    .prologue
    .line 987
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v6, v7, :cond_3

    .line 994
    :cond_0
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v6}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 995
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_1

    .line 996
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v7, v7, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v5

    .line 997
    .local v5, "songList":Lcom/google/android/music/medialist/SongList;
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v6, v7, :cond_2

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v2

    .line 1000
    .local v2, "excludedId":J
    :goto_0
    new-instance v4, Lcom/google/android/music/ui/AddToPlaylistFragment;

    invoke-direct {v4}, Lcom/google/android/music/ui/AddToPlaylistFragment;-><init>()V

    .line 1001
    .local v4, "fragment":Landroid/support/v4/app/Fragment;
    invoke-static {v5, v2, v3}, Lcom/google/android/music/ui/AddToPlaylistFragment;->createArgs(Lcom/google/android/music/medialist/SongList;J)Landroid/os/Bundle;

    move-result-object v1

    .line 1003
    .local v1, "args":Landroid/os/Bundle;
    invoke-static {v0, v4, v1}, Lcom/google/android/music/ui/FragmentUtils;->addFragment(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/Fragment;Landroid/os/Bundle;)V

    .line 1008
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .end local v1    # "args":Landroid/os/Bundle;
    .end local v2    # "excludedId":J
    .end local v4    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_1
    :goto_1
    return-void

    .line 997
    .restart local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .restart local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_2
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 1006
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_3
    const-string v6, "DocumentMenuHandler"

    const-string v7, "Unexpected doc type"

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
