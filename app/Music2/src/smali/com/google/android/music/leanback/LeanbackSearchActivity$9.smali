.class Lcom/google/android/music/leanback/LeanbackSearchActivity$9;
.super Ljava/lang/Object;
.source "LeanbackSearchActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackSearchActivity;->handleSearchResult(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final cols:[Ljava/lang/String;

.field mArtistName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

.field final synthetic val$artistId:J

.field final synthetic val$shouldAutoPlay:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;JZ)V
    .locals 4

    .prologue
    .line 518
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    iput-wide p2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->val$artistId:J

    iput-boolean p4, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->val$shouldAutoPlay:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 520
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "artist"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->cols:[Ljava/lang/String;

    .line 524
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->mArtistName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 528
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    iget-wide v4, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->val$artistId:J

    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->cols:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 532
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 533
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->mArtistName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 536
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 538
    return-void

    .line 536
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public taskCompleted()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 542
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    iget-wide v6, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->val$artistId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/leanback/LeanbackUtils;->getArtistDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 545
    iget-boolean v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->val$shouldAutoPlay:Z

    if-eqz v0, :cond_0

    .line 546
    new-instance v1, Lcom/google/android/music/medialist/ArtistSongList;

    iget-wide v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->val$artistId:J

    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->mArtistName:Ljava/lang/String;

    move v6, v5

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/medialist/ArtistSongList;-><init>(JLjava/lang/String;IZ)V

    .line 548
    .local v1, "artistSongList":Lcom/google/android/music/medialist/SongList;
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/MusicUtils;->playArtistShuffle(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 551
    .end local v1    # "artistSongList":Lcom/google/android/music/medialist/SongList;
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->finish()V

    .line 552
    return-void
.end method
