.class public Lcom/google/android/music/leanback/IconCardView;
.super Landroid/support/v17/leanback/widget/BaseCardView;
.source "IconCardView.java"


# instance fields
.field private mAnimDuration:I

.field private mCircle:Landroid/widget/ImageView;

.field private mIcon:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/leanback/IconCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/leanback/IconCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v17/leanback/widget/BaseCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/IconCardView;->setCardType(I)V

    .line 26
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/IconCardView;->setInfoVisibility(I)V

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/leanback/IconCardView;->mAnimDuration:I

    .line 28
    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 57
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->onDetachedFromWindow()V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/music/leanback/IconCardView;->clearAnimation()V

    .line 59
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->onFinishInflate()V

    .line 33
    const v0, 0x7f0e0162

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/IconCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/leanback/IconCardView;->mCircle:Landroid/widget/ImageView;

    .line 34
    const v0, 0x7f0e007c

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/IconCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/leanback/IconCardView;->mIcon:Landroid/widget/ImageView;

    .line 35
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 0
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 39
    invoke-super {p0, p1, p2, p3}, Landroid/support/v17/leanback/widget/BaseCardView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 40
    invoke-virtual {p0, p1}, Lcom/google/android/music/leanback/IconCardView;->setSelected(Z)V

    .line 41
    return-void
.end method

.method public setSelected(Z)V
    .locals 4
    .param p1, "selected"    # Z

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseCardView;->setSelected(Z)V

    .line 47
    iget-object v0, p0, Lcom/google/android/music/leanback/IconCardView;->mCircle:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/android/music/leanback/IconCardView;->mCircle:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    if-eqz p1, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/leanback/IconCardView;->mAnimDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 53
    :cond_0
    return-void

    .line 48
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
