.class public final Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;
.super Ljava/lang/Object;
.source "PlaybackStateCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/session/PlaybackStateCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private mActions:J

.field private mActiveItemId:J

.field private mBufferedPosition:J

.field private mCustomActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;",
            ">;"
        }
    .end annotation
.end field

.field private mErrorMessage:Ljava/lang/CharSequence;

.field private mPosition:J

.field private mSpeed:F

.field private mState:I

.field private mUpdateTime:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 438
    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/music/playback/session/PlaybackStateCompat;
    .locals 18

    .prologue
    .line 546
    new-instance v2, Lcom/google/android/music/playback/session/PlaybackStateCompat;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mState:I

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mPosition:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mUpdateTime:J

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mSpeed:F

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mBufferedPosition:J

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mActions:J

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mCustomActions:Ljava/util/List;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mActiveItemId:J

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mErrorMessage:Ljava/lang/CharSequence;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-direct/range {v2 .. v17}, Lcom/google/android/music/playback/session/PlaybackStateCompat;-><init>(IJJFJJLjava/util/List;JLjava/lang/CharSequence;Lcom/google/android/music/playback/session/PlaybackStateCompat$1;)V

    return-object v2
.end method

.method public setActions(J)Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;
    .locals 1
    .param p1, "capabilities"    # J

    .prologue
    .line 519
    iput-wide p1, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mActions:J

    .line 520
    return-object p0
.end method

.method public setActiveItemId(J)Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 538
    iput-wide p1, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mActiveItemId:J

    .line 539
    return-object p0
.end method

.method public setCustomActions(Ljava/util/List;)Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;",
            ">;)",
            "Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;"
        }
    .end annotation

    .prologue
    .line 533
    .local p1, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;>;"
    iput-object p1, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mCustomActions:Ljava/util/List;

    .line 534
    return-object p0
.end method

.method public setErrorMessage(Ljava/lang/CharSequence;)Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;
    .locals 0
    .param p1, "errorMessage"    # Ljava/lang/CharSequence;

    .prologue
    .line 528
    iput-object p1, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mErrorMessage:Ljava/lang/CharSequence;

    .line 529
    return-object p0
.end method

.method public setState(IJF)Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;
    .locals 2
    .param p1, "state"    # I
    .param p2, "position"    # J
    .param p4, "playbackRate"    # F

    .prologue
    .line 486
    iput p1, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mState:I

    .line 487
    iput-wide p2, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mPosition:J

    .line 488
    iput p4, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mSpeed:F

    .line 489
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->mUpdateTime:J

    .line 490
    return-object p0
.end method
