.class public Lcom/google/android/music/download/cache/StorageMigrationService;
.super Landroid/app/Service;
.source "StorageMigrationService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static volatile sMigrationWorker:Lcom/google/android/music/download/cache/StorageMigrationWorker;

.field private static final sStorageIntentFilter:Landroid/content/IntentFilter;


# instance fields
.field private mIsDestroyed:Z

.field private mStorageBroadcastReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CACHE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/cache/StorageMigrationService;->LOGV:Z

    .line 37
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/google/android/music/download/cache/StorageMigrationService;->sStorageIntentFilter:Landroid/content/IntentFilter;

    .line 39
    sget-object v0, Lcom/google/android/music/download/cache/StorageMigrationService;->sStorageIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.music.download.cache.CacheLocationManager.LocationsChanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 43
    new-instance v0, Lcom/google/android/music/download/cache/StorageMigrationService$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/cache/StorageMigrationService$1;-><init>(Lcom/google/android/music/download/cache/StorageMigrationService;)V

    iput-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationService;->mStorageBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/download/cache/StorageMigrationService;->mIsDestroyed:Z

    .line 54
    return-void
.end method

.method static synthetic access$000()Lcom/google/android/music/download/cache/StorageMigrationWorker;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/music/download/cache/StorageMigrationService;->sMigrationWorker:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/download/cache/StorageMigrationService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationService;
    .param p1, "x1"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/music/download/cache/StorageMigrationService;->onMigrationFinished(I)V

    return-void
.end method

.method private getTargetVolume(Landroid/content/Intent;)Ljava/util/UUID;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 170
    const-string v1, "targetVolumeId"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "volumeIdStr":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 172
    const/4 v1, 0x0

    .line 174
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v1

    goto :goto_0
.end method

.method private onMigrationFinished(I)V
    .locals 3
    .param p1, "startId"    # I

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/google/android/music/download/cache/StorageMigrationService;->mIsDestroyed:Z

    if-eqz v0, :cond_1

    .line 180
    const-string v0, "StorageMigrationSvc"

    const-string v1, "The service is destroyed by the time migration is finished"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 185
    sget-boolean v0, Lcom/google/android/music/download/cache/StorageMigrationService;->LOGV:Z

    if-eqz v0, :cond_2

    .line 186
    const-string v0, "StorageMigrationSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Stopping startId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/music/download/cache/StorageMigrationService;->stopSelf(I)V

    goto :goto_0
.end method

.method public static resumeMigrationIfNeeded(Landroid/content/Context;Ljava/util/UUID;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "targetVolumeId"    # Ljava/util/UUID;

    .prologue
    .line 104
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/download/cache/StorageMigrationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 105
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.google.android.music.ResumeStorageMigration"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    if-eqz p1, :cond_0

    .line 107
    const-string v1, "targetVolumeId"

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 110
    return-void
.end method

.method public static startMigration(Landroid/content/Context;Ljava/util/UUID;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "targetVolumeId"    # Ljava/util/UUID;

    .prologue
    .line 91
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/download/cache/StorageMigrationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 92
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.google.android.music.StartStorageMigration"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    if-eqz p1, :cond_0

    .line 94
    const-string v1, "targetVolumeId"

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 97
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 141
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 114
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 115
    sget-object v1, Lcom/google/android/music/download/cache/StorageMigrationService;->sMigrationWorker:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    if-nez v1, :cond_1

    .line 116
    const-class v2, Lcom/google/android/music/download/cache/StorageMigrationService;

    monitor-enter v2

    .line 117
    :try_start_0
    sget-object v1, Lcom/google/android/music/download/cache/StorageMigrationService;->sMigrationWorker:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    if-nez v1, :cond_0

    .line 118
    new-instance v1, Lcom/google/android/music/download/cache/StorageMigrationWorker;

    invoke-direct {v1, p0}, Lcom/google/android/music/download/cache/StorageMigrationWorker;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/google/android/music/download/cache/StorageMigrationService;->sMigrationWorker:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    .line 120
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    :cond_1
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 123
    .local v0, "manager":Landroid/support/v4/content/LocalBroadcastManager;
    iget-object v1, p0, Lcom/google/android/music/download/cache/StorageMigrationService;->mStorageBroadcastReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Lcom/google/android/music/download/cache/StorageMigrationService;->sStorageIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 124
    return-void

    .line 120
    .end local v0    # "manager":Landroid/support/v4/content/LocalBroadcastManager;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 128
    sget-boolean v1, Lcom/google/android/music/download/cache/StorageMigrationService;->LOGV:Z

    if-eqz v1, :cond_0

    .line 129
    const-string v1, "StorageMigrationSvc"

    const-string v2, "Service destroyed"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/download/cache/StorageMigrationService;->mIsDestroyed:Z

    .line 132
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 133
    .local v0, "manager":Landroid/support/v4/content/LocalBroadcastManager;
    iget-object v1, p0, Lcom/google/android/music/download/cache/StorageMigrationService;->mStorageBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 134
    sget-object v1, Lcom/google/android/music/download/cache/StorageMigrationService;->sMigrationWorker:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    invoke-virtual {v1}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->abortMigration()V

    .line 135
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 136
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 146
    sget-boolean v3, Lcom/google/android/music/download/cache/StorageMigrationService;->LOGV:Z

    if-eqz v3, :cond_0

    .line 147
    const-string v3, "StorageMigrationSvc"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Started "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " startId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 150
    .local v0, "action":Ljava/lang/String;
    :goto_0
    if-nez v0, :cond_3

    .line 151
    const-string v3, "StorageMigrationSvc"

    const-string v4, "started with null-action intent"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_1
    :goto_1
    const/4 v3, 0x3

    return v3

    .line 149
    .end local v0    # "action":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 152
    .restart local v0    # "action":Ljava/lang/String;
    :cond_3
    const-string v3, "com.google.android.music.StartStorageMigration"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 153
    sget-object v3, Lcom/google/android/music/download/cache/StorageMigrationService;->sMigrationWorker:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    new-instance v4, Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;

    invoke-direct {v4, p0, p3}, Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;-><init>(Lcom/google/android/music/download/cache/StorageMigrationService;I)V

    invoke-direct {p0, p1}, Lcom/google/android/music/download/cache/StorageMigrationService;->getTargetVolume(Landroid/content/Intent;)Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->beginMigration(Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;Ljava/util/UUID;)V

    goto :goto_1

    .line 155
    :cond_4
    const-string v3, "com.google.android.music.ResumeStorageMigration"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 156
    new-instance v1, Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;

    invoke-direct {v1, p0, p3}, Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;-><init>(Lcom/google/android/music/download/cache/StorageMigrationService;I)V

    .line 157
    .local v1, "callbacks":Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;
    sget-object v3, Lcom/google/android/music/download/cache/StorageMigrationService;->sMigrationWorker:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    invoke-direct {p0, p1}, Lcom/google/android/music/download/cache/StorageMigrationService;->getTargetVolume(Landroid/content/Intent;)Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->resumeMigration(Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;Ljava/util/UUID;)Z

    move-result v2

    .line 158
    .local v2, "resumed":Z
    if-nez v2, :cond_1

    .line 161
    invoke-virtual {v1}, Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;->onMigrationFinished()V

    goto :goto_1

    .line 164
    .end local v1    # "callbacks":Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;
    .end local v2    # "resumed":Z
    :cond_5
    const-string v3, "StorageMigrationSvc"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown action: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
