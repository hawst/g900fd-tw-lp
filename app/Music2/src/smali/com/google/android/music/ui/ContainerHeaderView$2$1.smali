.class Lcom/google/android/music/ui/ContainerHeaderView$2$1;
.super Ljava/lang/Object;
.source "ContainerHeaderView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ContainerHeaderView$2;->taskCompleted()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final color:I

.field final synthetic this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ContainerHeaderView$2;)V
    .locals 2

    .prologue
    .line 383
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$1;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$1;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    iget-object v0, v0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/ContainerHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0085

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$1;->color:I

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 389
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 408
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v1, 0x0

    return v1

    .line 391
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$1;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    iget-object v1, v1, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1000(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 392
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 393
    iget v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$1;->color:I

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 394
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$1;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    iget-object v1, v1, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1000(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    goto :goto_0

    .line 400
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$1;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    iget-object v1, v1, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1000(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 401
    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 402
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 403
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2$1;->this$1:Lcom/google/android/music/ui/ContainerHeaderView$2;

    iget-object v1, v1, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1000(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    goto :goto_0

    .line 389
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
