.class public Lcom/google/android/music/MusicPicker;
.super Landroid/app/ListActivity;
.source "MusicPicker.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/MusicPicker$QueryHandler;,
        Lcom/google/android/music/MusicPicker$TrackListAdapter;
    }
.end annotation


# static fields
.field static final CURSOR_COLS:[Ljava/lang/String;

.field static sFormatBuilder:Ljava/lang/StringBuilder;

.field static sFormatter:Ljava/util/Formatter;

.field static final sTimeArgs:[Ljava/lang/Object;


# instance fields
.field mAdapter:Lcom/google/android/music/MusicPicker$TrackListAdapter;

.field mBaseUri:Landroid/net/Uri;

.field mCancelButton:Landroid/view/View;

.field mCursor:Landroid/database/Cursor;

.field mListContainer:Landroid/view/View;

.field mListHasFocus:Z

.field mListShown:Z

.field mListState:Landroid/os/Parcelable;

.field mMediaPlayer:Landroid/media/MediaPlayer;

.field mOkayButton:Landroid/view/View;

.field mPlayingId:J

.field mProgressContainer:Landroid/view/View;

.field mQueryHandler:Lcom/google/android/music/MusicPicker$QueryHandler;

.field mSelectedId:J

.field mSelectedUri:Landroid/net/Uri;

.field mSortMode:I

.field mSortOrder:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 77
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title_key"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "album"

    aput-object v2, v0, v1

    const-string v1, "artist"

    aput-object v1, v0, v3

    const/4 v1, 0x6

    const-string v2, "artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "track"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/MusicPicker;->CURSOR_COLS:[Ljava/lang/String;

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lcom/google/android/music/MusicPicker;->sFormatBuilder:Ljava/lang/StringBuilder;

    .line 92
    new-instance v0, Ljava/util/Formatter;

    sget-object v1, Lcom/google/android/music/MusicPicker;->sFormatBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    sput-object v0, Lcom/google/android/music/MusicPicker;->sFormatter:Ljava/util/Formatter;

    .line 94
    new-array v0, v3, [Ljava/lang/Object;

    sput-object v0, Lcom/google/android/music/MusicPicker;->sTimeArgs:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 52
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mListState:Landroid/os/Parcelable;

    .line 112
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/MusicPicker;->mSortMode:I

    .line 131
    iput-wide v2, p0, Lcom/google/android/music/MusicPicker;->mSelectedId:J

    .line 137
    iput-wide v2, p0, Lcom/google/android/music/MusicPicker;->mPlayingId:J

    .line 315
    return-void
.end method


# virtual methods
.method doQuery(ZLjava/lang/String;)Landroid/database/Cursor;
    .locals 19
    .param p1, "sync"    # Z
    .param p2, "filterstring"    # Ljava/lang/String;

    .prologue
    .line 533
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/MusicPicker;->mQueryHandler:Lcom/google/android/music/MusicPicker$QueryHandler;

    const/16 v2, 0x2a

    invoke-virtual {v1, v2}, Lcom/google/android/music/MusicPicker$QueryHandler;->cancelOperation(I)V

    .line 535
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 536
    .local v18, "where":Ljava/lang/StringBuilder;
    const-string v1, "title != \'\'"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 539
    const/4 v5, 0x0

    .line 540
    .local v5, "keywords":[Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 541
    const-string v1, " "

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 542
    .local v17, "searchWords":[Ljava/lang/String;
    move-object/from16 v0, v17

    array-length v1, v0

    new-array v5, v1, [Ljava/lang/String;

    .line 543
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v14

    .line 544
    .local v14, "col":Ljava/text/Collator;
    const/4 v1, 0x0

    invoke-virtual {v14, v1}, Ljava/text/Collator;->setStrength(I)V

    .line 545
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    move-object/from16 v0, v17

    array-length v1, v0

    if-ge v15, v1, :cond_0

    .line 546
    aget-object v1, v17, v15

    invoke-static {v1}, Landroid/provider/MediaStore$Audio;->keyFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 547
    .local v16, "key":Ljava/lang/String;
    const-string v1, "\\"

    const-string v2, "\\\\"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v16

    .line 548
    const-string v1, "%"

    const-string v2, "\\%"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v16

    .line 549
    const-string v1, "_"

    const-string v2, "\\_"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v16

    .line 550
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x25

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x25

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v15

    .line 545
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 552
    .end local v16    # "key":Ljava/lang/String;
    :cond_0
    const/4 v15, 0x0

    :goto_1
    move-object/from16 v0, v17

    array-length v1, v0

    if-ge v15, v1, :cond_1

    .line 553
    const-string v1, " AND "

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    const-string v1, "artist_key||"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 555
    const-string v1, "album_key||"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    const-string v1, "title_key LIKE ? ESCAPE \'\\\'"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 564
    .end local v14    # "col":Ljava/text/Collator;
    .end local v15    # "i":I
    .end local v17    # "searchWords":[Ljava/lang/String;
    :cond_1
    if-eqz p1, :cond_2

    .line 568
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/MusicPicker;->mBaseUri:Landroid/net/Uri;

    sget-object v3, Lcom/google/android/music/MusicPicker;->CURSOR_COLS:[Ljava/lang/String;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/MusicPicker;->mSortOrder:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v8}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 579
    :goto_2
    return-object v1

    .line 574
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/MusicPicker;->mAdapter:Lcom/google/android/music/MusicPicker$TrackListAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/music/MusicPicker$TrackListAdapter;->setLoading(Z)V

    .line 575
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/MusicPicker;->setProgressBarIndeterminateVisibility(Z)V

    .line 576
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/MusicPicker;->mQueryHandler:Lcom/google/android/music/MusicPicker$QueryHandler;

    const/16 v7, 0x2a

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/MusicPicker;->mBaseUri:Landroid/net/Uri;

    sget-object v10, Lcom/google/android/music/MusicPicker;->CURSOR_COLS:[Ljava/lang/String;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/MusicPicker;->mSortOrder:Ljava/lang/String;

    move-object v12, v5

    invoke-virtual/range {v6 .. v13}, Lcom/google/android/music/MusicPicker$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    :goto_3
    const/4 v1, 0x0

    goto :goto_2

    .line 571
    :catch_0
    move-exception v1

    goto :goto_3
.end method

.method makeListShown()V
    .locals 2

    .prologue
    .line 511
    iget-boolean v0, p0, Lcom/google/android/music/MusicPicker;->mListShown:Z

    if-nez v0, :cond_0

    .line 512
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/MusicPicker;->mListShown:Z

    .line 513
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mProgressContainer:Landroid/view/View;

    const v1, 0x10a0001

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 515
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mProgressContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 516
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mListContainer:Landroid/view/View;

    const/high16 v1, 0x10a0000

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 518
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mListContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 520
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 642
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 654
    :cond_0
    :goto_0
    return-void

    .line 644
    :pswitch_0
    iget-wide v0, p0, Lcom/google/android/music/MusicPicker;->mSelectedId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 645
    const/4 v0, -0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/google/android/music/MusicPicker;->mSelectedUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/MusicPicker;->setResult(ILandroid/content/Intent;)V

    .line 646
    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->finish()V

    goto :goto_0

    .line 651
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->finish()V

    goto :goto_0

    .line 642
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e01ad
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-ne v0, p1, :cond_0

    .line 624
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->stop()V

    .line 625
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->release()V

    .line 626
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 627
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/MusicPicker;->mPlayingId:J

    .line 628
    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 630
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v12, 0x0

    .line 347
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 349
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/music/MusicPicker;->requestWindowFeature(I)Z

    .line 351
    const/4 v11, 0x1

    .line 352
    .local v11, "sortMode":I
    if-nez p1, :cond_3

    .line 353
    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.extra.ringtone.EXISTING_URI"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mSelectedUri:Landroid/net/Uri;

    .line 364
    :goto_0
    const-string v0, "android.intent.action.GET_CONTENT"

    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 365
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mBaseUri:Landroid/net/Uri;

    .line 375
    :cond_0
    const v0, 0x7f04007c

    invoke-virtual {p0, v0}, Lcom/google/android/music/MusicPicker;->setContentView(I)V

    .line 377
    const-string v0, "title_key"

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mSortOrder:Ljava/lang/String;

    .line 379
    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->getListView()Landroid/widget/ListView;

    move-result-object v3

    .line 381
    .local v3, "listView":Landroid/widget/ListView;
    invoke-virtual {v3, v12}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 383
    new-instance v0, Lcom/google/android/music/MusicPicker$TrackListAdapter;

    const v4, 0x7f04007d

    new-array v5, v12, [Ljava/lang/String;

    new-array v6, v12, [I

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/MusicPicker$TrackListAdapter;-><init>(Lcom/google/android/music/MusicPicker;Landroid/content/Context;Landroid/widget/ListView;I[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mAdapter:Lcom/google/android/music/MusicPicker$TrackListAdapter;

    .line 387
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mAdapter:Lcom/google/android/music/MusicPicker$TrackListAdapter;

    invoke-virtual {p0, v0}, Lcom/google/android/music/MusicPicker;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 389
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    .line 392
    invoke-virtual {v3, v12}, Landroid/widget/ListView;->setSaveEnabled(Z)V

    .line 394
    new-instance v0, Lcom/google/android/music/MusicPicker$QueryHandler;

    invoke-direct {v0, p0, p0}, Lcom/google/android/music/MusicPicker$QueryHandler;-><init>(Lcom/google/android/music/MusicPicker;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mQueryHandler:Lcom/google/android/music/MusicPicker$QueryHandler;

    .line 396
    const v0, 0x7f0e01ab

    invoke-virtual {p0, v0}, Lcom/google/android/music/MusicPicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mProgressContainer:Landroid/view/View;

    .line 397
    const v0, 0x7f0e01ac

    invoke-virtual {p0, v0}, Lcom/google/android/music/MusicPicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mListContainer:Landroid/view/View;

    .line 399
    const v0, 0x7f0e01ad

    invoke-virtual {p0, v0}, Lcom/google/android/music/MusicPicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mOkayButton:Landroid/view/View;

    .line 400
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mOkayButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 401
    const v0, 0x7f0e01ae

    invoke-virtual {p0, v0}, Lcom/google/android/music/MusicPicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mCancelButton:Landroid/view/View;

    .line 402
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mCancelButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 406
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mSelectedUri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 407
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mSelectedUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    .line 408
    .local v8, "builder":Landroid/net/Uri$Builder;
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mSelectedUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v10

    .line 409
    .local v10, "path":Ljava/lang/String;
    const/16 v0, 0x2f

    invoke-virtual {v10, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v9

    .line 410
    .local v9, "idx":I
    if-ltz v9, :cond_1

    .line 411
    invoke-virtual {v10, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 413
    :cond_1
    invoke-virtual {v8, v10}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 414
    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    .line 418
    .local v7, "baseSelectedUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mBaseUri:Landroid/net/Uri;

    invoke-virtual {v7, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 421
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mSelectedUri:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/MusicPicker;->mSelectedId:J

    .line 425
    .end local v7    # "baseSelectedUri":Landroid/net/Uri;
    .end local v8    # "builder":Landroid/net/Uri$Builder;
    .end local v9    # "idx":I
    .end local v10    # "path":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0, v11}, Lcom/google/android/music/MusicPicker;->setSortMode(I)Z

    .line 426
    .end local v3    # "listView":Landroid/widget/ListView;
    :goto_1
    return-void

    .line 356
    :cond_3
    const-string v0, "android.intent.extra.ringtone.EXISTING_URI"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mSelectedUri:Landroid/net/Uri;

    .line 360
    const-string v0, "liststate"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mListState:Landroid/os/Parcelable;

    .line 361
    const-string v0, "focused"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/MusicPicker;->mListHasFocus:Z

    .line 362
    const-string v0, "sortMode"

    invoke-virtual {p1, v0, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v11

    goto/16 :goto_0

    .line 367
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mBaseUri:Landroid/net/Uri;

    .line 368
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mBaseUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 369
    const-string v0, "MusicPicker"

    const-string v1, "No data URI given to PICK action"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->finish()V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 441
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 442
    const v0, 0x7f0b00f6

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 443
    const/4 v0, 0x2

    const v1, 0x7f0b0159

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 444
    const/4 v0, 0x3

    const v1, 0x7f0b0158

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 445
    return v3
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 1
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 584
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 590
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/google/android/music/MusicPicker;->setSelected(Landroid/database/Cursor;)V

    .line 591
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 434
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/MusicPicker;->setSortMode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 435
    const/4 v0, 0x1

    .line 437
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 458
    invoke-super {p0}, Landroid/app/ListActivity;->onPause()V

    .line 459
    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->stopMediaPlayer()V

    .line 460
    return-void
.end method

.method public onRestart()V
    .locals 2

    .prologue
    .line 429
    invoke-super {p0}, Landroid/app/ListActivity;->onRestart()V

    .line 430
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/MusicPicker;->doQuery(ZLjava/lang/String;)Landroid/database/Cursor;

    .line 431
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 449
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 452
    const-string v0, "liststate"

    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 453
    const-string v0, "focused"

    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->hasFocus()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 454
    const-string v0, "sortMode"

    iget v1, p0, Lcom/google/android/music/MusicPicker;->mSortMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 455
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 463
    invoke-super {p0}, Landroid/app/ListActivity;->onStop()V

    .line 469
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mAdapter:Lcom/google/android/music/MusicPicker$TrackListAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/MusicPicker$TrackListAdapter;->setLoading(Z)V

    .line 470
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mAdapter:Lcom/google/android/music/MusicPicker$TrackListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/MusicPicker$TrackListAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 471
    return-void
.end method

.method setSelected(Landroid/database/Cursor;)V
    .locals 7
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 594
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 595
    .local v1, "uri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/google/android/music/MusicPicker;->mCursor:Landroid/database/Cursor;

    iget-object v5, p0, Lcom/google/android/music/MusicPicker;->mCursor:Landroid/database/Cursor;

    const-string v6, "_id"

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 596
    .local v2, "newId":J
    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/MusicPicker;->mSelectedUri:Landroid/net/Uri;

    .line 598
    iput-wide v2, p0, Lcom/google/android/music/MusicPicker;->mSelectedId:J

    .line 599
    iget-wide v4, p0, Lcom/google/android/music/MusicPicker;->mPlayingId:J

    cmp-long v4, v2, v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/music/MusicPicker;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v4, :cond_2

    .line 600
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->stopMediaPlayer()V

    .line 601
    new-instance v4, Landroid/media/MediaPlayer;

    invoke-direct {v4}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v4, p0, Lcom/google/android/music/MusicPicker;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 603
    :try_start_0
    iget-object v4, p0, Lcom/google/android/music/MusicPicker;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v5, p0, Lcom/google/android/music/MusicPicker;->mSelectedUri:Landroid/net/Uri;

    invoke-virtual {v4, p0, v5}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 604
    iget-object v4, p0, Lcom/google/android/music/MusicPicker;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 608
    iget-object v4, p0, Lcom/google/android/music/MusicPicker;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 609
    iget-object v4, p0, Lcom/google/android/music/MusicPicker;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->prepare()V

    .line 610
    iget-object v4, p0, Lcom/google/android/music/MusicPicker;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->start()V

    .line 611
    iput-wide v2, p0, Lcom/google/android/music/MusicPicker;->mPlayingId:J

    .line 612
    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->invalidateViews()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 620
    :cond_1
    :goto_0
    return-void

    .line 613
    :catch_0
    move-exception v0

    .line 614
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "MusicPicker"

    const-string v5, "Unable to play track"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 616
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    iget-object v4, p0, Lcom/google/android/music/MusicPicker;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v4, :cond_1

    .line 617
    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->stopMediaPlayer()V

    .line 618
    invoke-virtual {p0}, Lcom/google/android/music/MusicPicker;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->invalidateViews()V

    goto :goto_0
.end method

.method setSortMode(I)Z
    .locals 4
    .param p1, "sortMode"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 478
    iget v2, p0, Lcom/google/android/music/MusicPicker;->mSortMode:I

    if-eq p1, v2, :cond_0

    .line 479
    packed-switch p1, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 503
    :goto_0
    return v0

    .line 481
    :pswitch_0
    iput p1, p0, Lcom/google/android/music/MusicPicker;->mSortMode:I

    .line 482
    const-string v2, "title_key"

    iput-object v2, p0, Lcom/google/android/music/MusicPicker;->mSortOrder:Ljava/lang/String;

    .line 483
    invoke-virtual {p0, v1, v3}, Lcom/google/android/music/MusicPicker;->doQuery(ZLjava/lang/String;)Landroid/database/Cursor;

    goto :goto_0

    .line 486
    :pswitch_1
    iput p1, p0, Lcom/google/android/music/MusicPicker;->mSortMode:I

    .line 487
    const-string v2, "album_key ASC, track ASC, title_key ASC"

    iput-object v2, p0, Lcom/google/android/music/MusicPicker;->mSortOrder:Ljava/lang/String;

    .line 490
    invoke-virtual {p0, v1, v3}, Lcom/google/android/music/MusicPicker;->doQuery(ZLjava/lang/String;)Landroid/database/Cursor;

    goto :goto_0

    .line 493
    :pswitch_2
    iput p1, p0, Lcom/google/android/music/MusicPicker;->mSortMode:I

    .line 494
    const-string v2, "artist_key ASC, album_key ASC, track ASC, title_key ASC"

    iput-object v2, p0, Lcom/google/android/music/MusicPicker;->mSortOrder:Ljava/lang/String;

    .line 498
    invoke-virtual {p0, v1, v3}, Lcom/google/android/music/MusicPicker;->doQuery(ZLjava/lang/String;)Landroid/database/Cursor;

    goto :goto_0

    .line 479
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method stopMediaPlayer()V
    .locals 2

    .prologue
    .line 633
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 634
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 635
    iget-object v0, p0, Lcom/google/android/music/MusicPicker;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 636
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/MusicPicker;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 637
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/MusicPicker;->mPlayingId:J

    .line 639
    :cond_0
    return-void
.end method
