.class public final Lcom/google/android/music/utils/DeviceType;
.super Ljava/lang/Object;
.source "DeviceType.java"


# direct methods
.method private static calculateDefaultScreenSizeInInches(Landroid/content/Context;)D
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 211
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 212
    .local v0, "display":Landroid/util/DisplayMetrics;
    const-string v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 214
    .local v1, "windowManager":Landroid/view/WindowManager;
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_0

    .line 215
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 219
    :goto_0
    invoke-static {v0}, Lcom/google/android/music/utils/DeviceType;->calculateScreenSizeInInches(Landroid/util/DisplayMetrics;)D

    move-result-wide v2

    return-wide v2

    .line 217
    :cond_0
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    goto :goto_0
.end method

.method static calculateScreenSizeInInches(Landroid/util/DisplayMetrics;)D
    .locals 10
    .param p0, "display"    # Landroid/util/DisplayMetrics;

    .prologue
    const/4 v7, 0x0

    .line 233
    iget v6, p0, Landroid/util/DisplayMetrics;->xdpi:F

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_0

    iget v6, p0, Landroid/util/DisplayMetrics;->ydpi:F

    cmpl-float v6, v6, v7

    if-nez v6, :cond_1

    .line 234
    :cond_0
    const-wide/16 v2, 0x0

    .line 241
    :goto_0
    return-wide v2

    .line 237
    :cond_1
    iget v6, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v6, v6

    iget v8, p0, Landroid/util/DisplayMetrics;->xdpi:F

    float-to-double v8, v8

    div-double v4, v6, v8

    .line 238
    .local v4, "widthInInches":D
    iget v6, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v6, v6

    iget v8, p0, Landroid/util/DisplayMetrics;->ydpi:F

    float-to-double v8, v8

    div-double v0, v6, v8

    .line 239
    .local v0, "heightInInches":D
    mul-double v6, v4, v4

    mul-double v8, v0, v0

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 241
    .local v2, "screenInches":D
    goto :goto_0
.end method

.method private static guessIsSmartphoneDeviceType(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 198
    invoke-static {p0}, Lcom/google/android/music/utils/DeviceType;->isCellCapable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/music/utils/DeviceType;->isSmartphoneScreenSize(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/music/utils/DeviceType;->hasDialerActivity(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static hasDialerActivity(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 291
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 292
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.DIAL"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 293
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v3, 0x10000

    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 295
    .local v2, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v2, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static isActiveCellularCheckEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 321
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_device_enable_active_check"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static isActiveCellularSubscriber(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 333
    const-string v4, "phone"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 336
    .local v1, "telephony":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_0

    .line 358
    :goto_0
    :pswitch_0
    return v3

    .line 341
    :cond_0
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    .line 342
    .local v0, "phoneType":I
    packed-switch v0, :pswitch_data_0

    .line 357
    const-string v2, "MusicDevice"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected phone type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    move v3, v2

    .line 353
    goto :goto_0

    .line 355
    :pswitch_2
    invoke-static {v1}, Lcom/google/android/music/utils/DeviceType;->isSimCardPresent(Landroid/telephony/TelephonyManager;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v1}, Lcom/google/android/music/utils/DeviceType;->isRegisteredOnNetwork(Landroid/telephony/TelephonyManager;)Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1

    .line 342
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static isCellCapable(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 257
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v5, 0x7

    if-lt v4, v5, :cond_1

    .line 258
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 259
    .local v0, "packages":Landroid/content/pm/PackageManager;
    const-string v4, "android.hardware.telephony"

    invoke-virtual {v0, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 280
    .end local v0    # "packages":Landroid/content/pm/PackageManager;
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 265
    :cond_1
    const-string v4, "phone"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 267
    .local v2, "telephony":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_0

    .line 270
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    .line 271
    .local v1, "phoneType":I
    packed-switch v1, :pswitch_data_0

    .line 279
    const-string v4, "MusicDevice"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected phone type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 277
    :pswitch_1
    const/4 v3, 0x1

    goto :goto_0

    .line 271
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static isRegisteredOnNetwork(Landroid/telephony/TelephonyManager;)Z
    .locals 1
    .param p0, "telephony"    # Landroid/telephony/TelephonyManager;

    .prologue
    .line 368
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isSimCardPresent(Landroid/telephony/TelephonyManager;)Z
    .locals 2
    .param p0, "telephony"    # Landroid/telephony/TelephonyManager;

    .prologue
    const/4 v0, 0x1

    .line 364
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSmartphone(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 135
    invoke-static {p0}, Lcom/google/android/music/utils/DeviceType;->isSmartphoneDeviceType(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    const/4 v0, 0x0

    .line 144
    :goto_0
    return v0

    .line 141
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/utils/DeviceType;->isActiveCellularCheckEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    invoke-static {p0}, Lcom/google/android/music/utils/DeviceType;->isActiveCellularSubscriber(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0

    .line 144
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static isSmartphoneDeviceType(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 175
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "music_device_type"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 180
    .local v0, "gservicesDeviceType":I
    packed-switch v0, :pswitch_data_0

    .line 188
    const-string v1, "MusicDevice"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected gservice device type value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    invoke-static {p0}, Lcom/google/android/music/utils/DeviceType;->guessIsSmartphoneDeviceType(Landroid/content/Context;)Z

    move-result v1

    :goto_0
    :pswitch_0
    return v1

    .line 182
    :pswitch_1
    const/4 v1, 0x1

    goto :goto_0

    .line 186
    :pswitch_2
    invoke-static {p0}, Lcom/google/android/music/utils/DeviceType;->guessIsSmartphoneDeviceType(Landroid/content/Context;)Z

    move-result v1

    goto :goto_0

    .line 180
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static isSmartphoneScreenSize(Landroid/content/Context;)Z
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const-wide/high16 v12, 0x4024000000000000L    # 10.0

    .line 305
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "music_smartphone_min_screen_size_in_tenth_of_inch"

    const/16 v10, 0x1e

    invoke-static {v8, v9, v10}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 308
    .local v3, "minSizeInTenthOfInch":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "music_smartphone_max_screen_size_in_tenth_of_inch"

    const/16 v10, 0x3c

    invoke-static {v8, v9, v10}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 312
    .local v2, "maxSizeInTenthOfInch":I
    int-to-double v8, v3

    div-double v4, v8, v12

    .line 313
    .local v4, "minSizeInInches":D
    int-to-double v8, v2

    div-double v0, v8, v12

    .line 315
    .local v0, "maxSizeInInches":D
    invoke-static {p0}, Lcom/google/android/music/utils/DeviceType;->calculateDefaultScreenSizeInInches(Landroid/content/Context;)D

    move-result-wide v6

    .line 317
    .local v6, "sizeInInches":D
    cmpl-double v8, v6, v4

    if-ltz v8, :cond_0

    cmpg-double v8, v6, v0

    if-gtz v8, :cond_0

    const/4 v8, 0x1

    :goto_0
    return v8

    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method
