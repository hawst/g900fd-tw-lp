.class Lcom/google/android/music/xdi/MyMusicItemsCursor;
.super Lcom/google/android/music/xdi/XdiCursor;
.source "MyMusicItemsCursor.java"


# static fields
.field private static final PROJECTION_ALBUMS:[Ljava/lang/String;

.field private static final PROJECTION_ARTISTS:[Ljava/lang/String;

.field private static final PROJECTION_GENRES:[Ljava/lang/String;


# instance fields
.field private final mArtistItemHeight:I

.field private final mArtistItemWidth:I

.field private final mHeaderId:J

.field private final mImageHeight:I

.field private final mImageWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "name"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->PROJECTION_GENRES:[Ljava/lang/String;

    .line 30
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "artist"

    aput-object v1, v0, v3

    const-string v1, "artworkUrl"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->PROJECTION_ARTISTS:[Ljava/lang/String;

    .line 40
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "album_name"

    aput-object v1, v0, v3

    const-string v1, "album_artist"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "headerId"    # J
    .param p5, "startIndex"    # Ljava/lang/String;
    .param p6, "fetchCount"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-static {p1, p3, p4, p5, p6}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->getCursorForHeader(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/xdi/XdiCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/database/Cursor;)V

    .line 60
    iput-wide p3, p0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->mHeaderId:J

    .line 62
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->mImageWidth:I

    .line 63
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->mImageHeight:I

    .line 64
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistItemWidthDp(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->mArtistItemWidth:I

    .line 65
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistItemHeightDp(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->mArtistItemHeight:I

    .line 66
    return-void
.end method

.method private extractDataForAlbums([Ljava/lang/Object;)Z
    .locals 13
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x2

    const/4 v10, 0x1

    .line 227
    invoke-virtual {p0}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v3

    .line 228
    .local v3, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 229
    .local v2, "context":Landroid/content/Context;
    const/4 v8, 0x0

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 230
    .local v4, "id":J
    invoke-interface {v3, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 231
    .local v7, "name":Ljava/lang/String;
    invoke-interface {v3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 232
    .local v1, "artist":Ljava/lang/String;
    iget v8, p0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->mImageWidth:I

    iget v9, p0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->mImageHeight:I

    invoke-static {v4, v5, v10, v8, v9}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v0

    .line 234
    .local v0, "artUri":Landroid/net/Uri;
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumDetailsIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 236
    .local v6, "intent":Landroid/content/Intent;
    const-string v8, "_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 237
    const-string v8, "parent_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 239
    const-string v8, "display_name"

    invoke-virtual {p0, p1, v8, v7}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 240
    const-string v8, "display_description"

    invoke-virtual {p0, p1, v8, v1}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 241
    const-string v8, "image_uri"

    invoke-virtual {p0, p1, v8, v0}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 242
    const-string v8, "width"

    invoke-virtual {p0, p1, v8, v12}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 243
    const-string v8, "height"

    invoke-virtual {p0, p1, v8, v12}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 244
    const-string v8, "intent_uri"

    invoke-virtual {v6, v10}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 246
    return v10
.end method

.method private extractDataForArtists([Ljava/lang/Object;)Z
    .locals 14
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v4

    .line 196
    .local v4, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 197
    .local v2, "context":Landroid/content/Context;
    const/4 v9, 0x0

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 198
    .local v6, "id":J
    const/4 v9, 0x1

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 199
    .local v8, "name":Ljava/lang/String;
    const/4 v9, 0x2

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 200
    .local v1, "artUri":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 201
    invoke-static {v2}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 204
    :cond_0
    const/4 v9, 0x0

    invoke-static {v2, v6, v7, v9}, Lcom/google/android/music/store/MusicContent$Artists;->getAlbumsByArtistCount(Landroid/content/Context;JZ)I

    move-result v3

    .line 207
    .local v3, "count":I
    sget-object v9, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v9

    const-string v10, "details/artists"

    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/music/xdi/XdiContract;->getDetailsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v5

    .line 211
    .local v5, "intent":Landroid/content/Intent;
    const-string v9, "_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {p0, p1, v9, v10}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 212
    const-string v9, "parent_id"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {p0, p1, v9, v10}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 214
    const-string v9, "display_name"

    invoke-virtual {p0, p1, v9, v8}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 215
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f120003

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v3, v11}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 217
    .local v0, "albumCountText":Ljava/lang/String;
    const-string v9, "display_description"

    invoke-virtual {p0, p1, v9, v0}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 218
    const-string v9, "image_uri"

    invoke-virtual {p0, p1, v9, v1}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 219
    const-string v9, "width"

    iget v10, p0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->mArtistItemWidth:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {p0, p1, v9, v10}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 220
    const-string v9, "height"

    iget v10, p0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->mArtistItemHeight:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {p0, p1, v9, v10}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 221
    const-string v9, "intent_uri"

    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, p1, v9, v10}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 223
    const/4 v9, 0x1

    return v9
.end method

.method private extractDataForGenres([Ljava/lang/Object;)Z
    .locals 13
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v3

    .line 163
    .local v3, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 164
    .local v4, "id":J
    const/4 v8, 0x1

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 167
    .local v7, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v4, v5}, Lcom/google/android/music/store/MusicContent$Genres;->getAlbumsOfGenreCount(Landroid/content/Context;J)I

    move-result v2

    .line 168
    .local v2, "count":I
    invoke-virtual {p0}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v8

    iget v9, p0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->mImageWidth:I

    iget v10, p0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->mImageHeight:I

    invoke-static {v8, v4, v5, v9, v10}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->getGenreArt(Landroid/content/Context;JII)Ljava/lang/String;

    move-result-object v1

    .line 170
    .local v1, "artUri":Ljava/lang/String;
    sget-object v8, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "mygenres"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    invoke-interface {v3}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    invoke-static {v8, v9}, Lcom/google/android/music/xdi/XdiContract;->getBrowseIntent(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v6

    .line 174
    .local v6, "intent":Landroid/content/Intent;
    const-string v8, "meta_uri"

    const-wide/16 v10, 0x2

    invoke-static {v10, v11}, Lcom/google/android/music/xdi/XdiUtils;->getMetaUri(J)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    const-string v8, "_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 178
    const-string v8, "parent_id"

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 180
    const-string v8, "display_name"

    invoke-virtual {p0, p1, v8, v7}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 181
    invoke-virtual {p0}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f120003

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v2, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 183
    .local v0, "albumCountText":Ljava/lang/String;
    const-string v8, "display_description"

    invoke-virtual {p0, p1, v8, v0}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 185
    const-string v8, "image_uri"

    invoke-virtual {p0, p1, v8, v1}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 186
    const-string v8, "width"

    const/4 v9, 0x0

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 187
    const-string v8, "height"

    const/4 v9, 0x0

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 188
    const-string v8, "intent_uri"

    const/4 v9, 0x1

    invoke-virtual {v6, v9}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 191
    const/4 v8, 0x1

    return v8
.end method

.method private static getCursorForHeader(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "headerId"    # J
    .param p3, "startIndex"    # Ljava/lang/String;
    .param p4, "fetchCount"    # Ljava/lang/String;

    .prologue
    .line 70
    long-to-int v0, p1

    packed-switch v0, :pswitch_data_0

    .line 81
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected header id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    new-instance v0, Landroid/database/MatrixCursor;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    :goto_0
    return-object v0

    .line 72
    :pswitch_0
    sget-object v0, Lcom/google/android/music/store/MusicContent$Genres;->CONTENT_URI:Landroid/net/Uri;

    sget-object v1, Lcom/google/android/music/xdi/MyMusicItemsCursor;->PROJECTION_GENRES:[Ljava/lang/String;

    invoke-static {p0, v0, v1, p3, p4}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->queryWithLimit(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 75
    :pswitch_1
    sget-object v0, Lcom/google/android/music/store/MusicContent$Artists;->CONTENT_URI:Landroid/net/Uri;

    sget-object v1, Lcom/google/android/music/xdi/MyMusicItemsCursor;->PROJECTION_ARTISTS:[Ljava/lang/String;

    invoke-static {p0, v0, v1, p3, p4}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->queryWithLimit(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 78
    :pswitch_2
    sget-object v0, Lcom/google/android/music/store/MusicContent$Albums;->CONTENT_URI:Landroid/net/Uri;

    sget-object v1, Lcom/google/android/music/xdi/MyMusicItemsCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    invoke-static {p0, v0, v1, p3, p4}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->queryWithLimit(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 70
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static getGenreArt(Landroid/content/Context;JII)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "genreId"    # J
    .param p3, "imageWidth"    # I
    .param p4, "imageHeight"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 133
    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$Genres;->getAlbumsOfGenreUri(J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, v6, v4}, Lcom/google/android/music/store/MusicContent;->addLimitParam(Landroid/net/Uri;II)Landroid/net/Uri;

    move-result-object v1

    .line 136
    .local v1, "limitUri":Landroid/net/Uri;
    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    .local v2, "cols":[Ljava/lang/String;
    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move v7, v6

    .line 140
    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v10

    .line 144
    .local v10, "cursor":Landroid/database/Cursor;
    if-nez v10, :cond_0

    .line 145
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultGenreArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 157
    :goto_0
    return-object v0

    .line 149
    :cond_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 151
    .local v8, "albumId":J
    const/4 v0, 0x1

    invoke-static {v8, v9, v0, p3, p4}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 157
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .line 154
    .end local v8    # "albumId":J
    :cond_1
    :try_start_1
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultGenreArtUri(Landroid/content/Context;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 157
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static queryWithLimit(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "startIndex"    # Ljava/lang/String;
    .param p4, "fetchCount"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 110
    invoke-static {p1, p3, p4}, Lcom/google/android/music/xdi/XdiUtils;->addLimitParam(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 111
    .local v1, "limitUri":Landroid/net/Uri;
    if-nez v1, :cond_0

    .line 112
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 114
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move v7, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected extractDataForCurrentRow([Ljava/lang/Object;)Z
    .locals 4
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 88
    iget-wide v0, p0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->mHeaderId:J

    long-to-int v0, v0

    packed-switch v0, :pswitch_data_0

    .line 99
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected header id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/xdi/MyMusicItemsCursor;->mHeaderId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const/4 v0, 0x0

    .line 103
    :goto_0
    return v0

    .line 90
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->extractDataForGenres([Ljava/lang/Object;)Z

    .line 103
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 93
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->extractDataForArtists([Ljava/lang/Object;)Z

    goto :goto_1

    .line 96
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/MyMusicItemsCursor;->extractDataForAlbums([Ljava/lang/Object;)Z

    goto :goto_1

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
