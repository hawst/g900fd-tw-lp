.class public Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;
.super Landroid/support/v4/app/Fragment;
.source "TutorialTryNautilusFragment.java"


# instance fields
.field private mIcon:Landroid/widget/ImageView;

.field private mSummary:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static newInstance(III)Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;
    .locals 3
    .param p0, "titleId"    # I
    .param p1, "summaryId"    # I
    .param p2, "iconId"    # I

    .prologue
    .line 36
    new-instance v1, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;

    invoke-direct {v1}, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;-><init>()V

    .line 37
    .local v1, "fragment":Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 38
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "titleId"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 39
    const-string v2, "resourceId"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 40
    const-string v2, "iconId"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 41
    invoke-virtual {v1, v0}, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;->setArguments(Landroid/os/Bundle;)V

    .line 42
    return-object v1
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    const v2, 0x7f040104

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 51
    .local v1, "view":Landroid/view/ViewGroup;
    const v2, 0x7f0e00b8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;->mTitle:Landroid/widget/TextView;

    .line 52
    const v2, 0x7f0e0289

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;->mSummary:Landroid/widget/TextView;

    .line 53
    const v2, 0x7f0e007c

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;->mIcon:Landroid/widget/ImageView;

    .line 55
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 56
    .local v0, "args":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;->mTitle:Landroid/widget/TextView;

    const-string v3, "titleId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 57
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;->mSummary:Landroid/widget/TextView;

    const-string v3, "resourceId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 58
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;->mIcon:Landroid/widget/ImageView;

    const-string v3, "iconId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 60
    return-object v1
.end method
