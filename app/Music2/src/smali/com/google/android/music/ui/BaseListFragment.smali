.class public abstract Lcom/google/android/music/ui/BaseListFragment;
.super Landroid/support/v4/app/ListFragment;
.source "BaseListFragment.java"

# interfaces
.implements Lcom/google/android/music/ui/MusicFragment;


# instance fields
.field private mEmptyImageView:Landroid/widget/ImageView;

.field private mEmptyScreen:Landroid/view/View;

.field private mEmptyTextView:Landroid/widget/TextView;

.field private mIsCardHeaderShowing:Z

.field private mLearnMore:Landroid/widget/TextView;

.field private mListContainer:Landroid/view/ViewGroup;

.field private mPaddingView:Landroid/view/View;

.field private mShouldShowEmptyScreen:Z

.field private final mUIStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 37
    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mShouldShowEmptyScreen:Z

    .line 38
    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mIsCardHeaderShowing:Z

    .line 40
    new-instance v0, Lcom/google/android/music/ui/BaseListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/BaseListFragment$1;-><init>(Lcom/google/android/music/ui/BaseListFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mUIStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    return-void
.end method

.method private refreshEmptyScreen()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyScreen:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 155
    iget-object v1, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyScreen:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mShouldShowEmptyScreen:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mIsCardHeaderShowing:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 158
    :cond_0
    return-void

    .line 155
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method protected clearEmptyScreenText()V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyTextView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    return-void
.end method

.method protected getActionBarController()Lcom/google/android/music/ui/ActionBarController;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseListFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v0

    .line 220
    .local v0, "controller":Lcom/google/android/music/ui/ActionBarController;
    if-nez v0, :cond_0

    .line 221
    new-instance v0, Lcom/google/android/music/ui/NoOpActionBarControllerImpl;

    .end local v0    # "controller":Lcom/google/android/music/ui/ActionBarController;
    invoke-direct {v0}, Lcom/google/android/music/ui/NoOpActionBarControllerImpl;-><init>()V

    .line 223
    .restart local v0    # "controller":Lcom/google/android/music/ui/ActionBarController;
    :cond_0
    return-object v0
.end method

.method public getAlbumId()J
    .locals 2

    .prologue
    .line 74
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getAlbumMetajamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return-object v0
.end method

.method public getArtistId()J
    .locals 2

    .prologue
    .line 69
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getArtistMetajamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return-object v0
.end method

.method getBaseActivity()Lcom/google/android/music/ui/BaseActivity;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/BaseActivity;

    return-object v0
.end method

.method protected getEmptyScreen()Landroid/view/View;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyScreen:Landroid/view/View;

    return-object v0
.end method

.method public final getFragment()Landroid/support/v4/app/Fragment;
    .locals 0

    .prologue
    .line 49
    return-object p0
.end method

.method public getFragmentMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method getPreferences()Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseListFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    return-object v0
.end method

.method protected initEmptyScreen()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 232
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyScreen:Landroid/view/View;

    if-nez v0, :cond_0

    .line 233
    const v0, 0x7f040031

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseListFragment;->setEmptyScreen(I)V

    .line 235
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyScreen:Landroid/view/View;

    const v1, 0x7f0e0112

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyImageView:Landroid/widget/ImageView;

    .line 236
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyScreen:Landroid/view/View;

    const v1, 0x7f0e0114

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyTextView:Landroid/widget/TextView;

    .line 237
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyScreen:Landroid/view/View;

    const v1, 0x7f0e0116

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mPaddingView:Landroid/view/View;

    .line 238
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyScreen:Landroid/view/View;

    const v1, 0x7f0e0115

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mLearnMore:Landroid/widget/TextView;

    .line 239
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mLearnMore:Landroid/widget/TextView;

    const v1, 0x7f0b02a3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 240
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyTextView:Landroid/widget/TextView;

    invoke-static {v0, v2}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 241
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mLearnMore:Landroid/widget/TextView;

    invoke-static {v0, v2}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 243
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mLearnMore:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/music/ui/BaseListFragment$2;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/BaseListFragment$2;-><init>(Lcom/google/android/music/ui/BaseListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 120
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 121
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 122
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseListFragment;->initEmptyScreen()V

    .line 123
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 98
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onAttach(Landroid/app/Activity;)V

    .line 99
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 100
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 105
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 106
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 111
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/ListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 112
    .local v0, "result":Landroid/view/View;
    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v1, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 114
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/BaseListFragment;->mUIStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/UIStateManager;->registerUIStateChangeListener(Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;)V

    .line 115
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 204
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDestroy()V

    .line 205
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 206
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 195
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/BaseListFragment;->mUIStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->unregisterUIStateChangeListener(Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;)V

    .line 197
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDestroyView()V

    .line 198
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyScreen:Landroid/view/View;

    .line 200
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 210
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDetach()V

    .line 211
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 212
    return-void
.end method

.method protected onNewNautilusStatus(Lcom/google/android/music/NautilusStatus;)V
    .locals 0
    .param p1, "newStatus"    # Lcom/google/android/music/NautilusStatus;

    .prologue
    .line 161
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 183
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onPause()V

    .line 184
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 185
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 177
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 178
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 179
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 93
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 94
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 171
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onStart()V

    .line 172
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 173
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 189
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onStop()V

    .line 190
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 191
    return-void
.end method

.method public onTutorialCardClosed()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseListFragment;->setIsCardHeaderShowing(Z)V

    .line 80
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 165
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 166
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 167
    return-void
.end method

.method protected setEmptyImageView(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 264
    return-void
.end method

.method protected setEmptyImageViewVisibile(Z)V
    .locals 2
    .param p1, "visibile"    # Z

    .prologue
    .line 267
    iget-object v1, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyImageView:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 268
    return-void

    .line 267
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected setEmptyScreen(I)V
    .locals 2
    .param p1, "resourceId"    # I

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mListContainer:Landroid/view/ViewGroup;

    .line 132
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyScreen:Landroid/view/View;

    .line 133
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mListContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyScreen:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 134
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseListFragment;->setEmptyScreenVisible(Z)V

    .line 135
    return-void
.end method

.method protected setEmptyScreenLearnMoreVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 271
    iget-object v1, p0, Lcom/google/android/music/ui/BaseListFragment;->mLearnMore:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 272
    return-void

    .line 271
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected setEmptyScreenPadding(Z)V
    .locals 2
    .param p1, "hasPadding"    # Z

    .prologue
    .line 280
    iget-object v1, p0, Lcom/google/android/music/ui/BaseListFragment;->mPaddingView:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 281
    return-void

    .line 280
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected setEmptyScreenText(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/music/ui/BaseListFragment;->mEmptyTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 256
    return-void
.end method

.method protected setEmptyScreenVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 149
    iput-boolean p1, p0, Lcom/google/android/music/ui/BaseListFragment;->mShouldShowEmptyScreen:Z

    .line 150
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseListFragment;->refreshEmptyScreen()V

    .line 151
    return-void
.end method

.method protected setIsCardHeaderShowing(Z)V
    .locals 0
    .param p1, "showing"    # Z

    .prologue
    .line 290
    iput-boolean p1, p0, Lcom/google/android/music/ui/BaseListFragment;->mIsCardHeaderShowing:Z

    .line 291
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseListFragment;->refreshEmptyScreen()V

    .line 292
    return-void
.end method
