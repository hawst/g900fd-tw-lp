.class Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "MaterialMainstageFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/MaterialMainstageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SituationCardViewHolder"
.end annotation


# instance fields
.field public final failureView:Landroid/widget/TextView;

.field public final headerView:Landroid/widget/TextView;

.field public final spinner:Landroid/widget/ProgressBar;

.field public final subtitleView:Landroid/widget/TextView;

.field public final topSituations:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;)V
    .locals 5
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "adapter"    # Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 226
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 227
    const v1, 0x7f0e0261

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->headerView:Landroid/widget/TextView;

    .line 228
    const v1, 0x7f0e0262

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->subtitleView:Landroid/widget/TextView;

    .line 229
    const v1, 0x7f0e01d2

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->spinner:Landroid/widget/ProgressBar;

    .line 230
    const v1, 0x7f0e0260

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->failureView:Landroid/widget/TextView;

    .line 232
    const v1, 0x7f0e0263

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    iput-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->topSituations:Landroid/support/v7/widget/RecyclerView;

    .line 233
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v3, v3}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 236
    .local v0, "manager":Landroid/support/v7/widget/LinearLayoutManager;
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->topSituations:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 237
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->topSituations:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 238
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->topSituations:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, p2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 239
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 240
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->headerView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 241
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->failureView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 242
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->spinner:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 243
    return-void
.end method
