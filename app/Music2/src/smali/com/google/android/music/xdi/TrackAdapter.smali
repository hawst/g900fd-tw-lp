.class public Lcom/google/android/music/xdi/TrackAdapter;
.super Ljava/lang/Object;
.source "TrackAdapter.java"


# static fields
.field public static final TRACK_COLUMNS:[Ljava/lang/String;


# instance fields
.field mAlbumArtistIdIdx:I

.field mAlbumIdIdx:I

.field mAlbumIdx:I

.field mAlbumMetajamIdx:I

.field mAlbumStoreIdIdx:I

.field mArtistIdx:I

.field mArtistMetajamIdx:I

.field mArtworkUrlIdx:I

.field mAudioIdIdx:I

.field mDomainIdx:I

.field mDurationIdx:I

.field mHasRemoteIdx:I

.field mIsLocalIdx:I

.field mPlaylistMemberIdIdx:I

.field private final mShowAlbumArt:Z

.field mSongIdIdx:I

.field private final mSongList:Lcom/google/android/music/medialist/SongList;

.field mStoreSongIdx:I

.field mTitleIdx:I

.field mTrackArtistIdIdx:I

.field mTrackMetajamIdx:I

.field private final mUnknownAlbum:Ljava/lang/String;

.field private final mUnknownArtist:Ljava/lang/String;

.field mYearIdx:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "audio_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "AlbumArtistId"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "artistSort"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "hasRemote"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "hasLocal"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "year"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "Genre"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "StoreId"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "SongId"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Domain"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Nid"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "ArtistArtLocation"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/TrackAdapter;->TRACK_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "medialist"    # Lcom/google/android/music/medialist/MediaList;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    invoke-direct {p0, p3}, Lcom/google/android/music/xdi/TrackAdapter;->getColumnIndices(Landroid/database/Cursor;)V

    .line 95
    instance-of v0, p2, Lcom/google/android/music/medialist/PlaylistSongList;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/android/music/medialist/AutoPlaylistSongList;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mShowAlbumArt:Z

    .line 98
    instance-of v0, p2, Lcom/google/android/music/medialist/SongList;

    if-eqz v0, :cond_2

    check-cast p2, Lcom/google/android/music/medialist/SongList;

    .end local p2    # "medialist":Lcom/google/android/music/medialist/MediaList;
    :goto_1
    iput-object p2, p0, Lcom/google/android/music/xdi/TrackAdapter;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 100
    const v0, 0x7f0b00c4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mUnknownArtist:Ljava/lang/String;

    .line 101
    const v0, 0x7f0b00c5

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mUnknownAlbum:Ljava/lang/String;

    .line 102
    return-void

    .line 95
    .restart local p2    # "medialist":Lcom/google/android/music/medialist/MediaList;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 98
    :cond_2
    const/4 p2, 0x0

    goto :goto_1
.end method

.method private getAlbum(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 203
    iget v1, p0, Lcom/google/android/music/xdi/TrackAdapter;->mAlbumIdx:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "album":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mUnknownAlbum:Ljava/lang/String;

    .line 207
    :cond_0
    return-object v0
.end method

.method private getAlbumMetajamId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 224
    iget v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mAlbumMetajamIdx:I

    if-ltz v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mAlbumMetajamIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mAlbumMetajamIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getArtistMetajamId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 229
    iget v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mArtistMetajamIdx:I

    if-ltz v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mArtistMetajamIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mArtistMetajamIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getColumnIndices(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 161
    if-nez p1, :cond_0

    .line 195
    :goto_0
    return-void

    .line 166
    :cond_0
    const-string v0, "title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mTitleIdx:I

    .line 167
    const-string v0, "artist"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mArtistIdx:I

    .line 168
    const-string v0, "AlbumArtistId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mAlbumArtistIdIdx:I

    .line 170
    const-string v0, "artist_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mTrackArtistIdIdx:I

    .line 172
    const-string v0, "year"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mYearIdx:I

    .line 173
    const-string v0, "duration"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mDurationIdx:I

    .line 175
    const-string v0, "audio_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mAudioIdIdx:I

    .line 177
    iget-object v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v0, :cond_1

    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mPlaylistMemberIdIdx:I

    .line 180
    const-string v0, "album"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mAlbumIdx:I

    .line 181
    const-string v0, "album_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mAlbumIdIdx:I

    .line 182
    const-string v0, "StoreAlbumId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mAlbumStoreIdIdx:I

    .line 185
    const-string v0, "hasLocal"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mIsLocalIdx:I

    .line 186
    const-string v0, "hasRemote"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mHasRemoteIdx:I

    .line 188
    const-string v0, "Domain"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mDomainIdx:I

    .line 189
    const-string v0, "SongId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mSongIdIdx:I

    .line 190
    const-string v0, "StoreId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mStoreSongIdx:I

    .line 191
    const-string v0, "Nid"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mTrackMetajamIdx:I

    .line 192
    const-string v0, "StoreAlbumId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mAlbumMetajamIdx:I

    .line 193
    const-string v0, "ArtistMetajamId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mArtistMetajamIdx:I

    .line 194
    const-string v0, "artworkUrl"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mArtworkUrlIdx:I

    goto/16 :goto_0

    .line 177
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private getPlaylistMemberId(Landroid/database/Cursor;)J
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 198
    iget v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mPlaylistMemberIdIdx:I

    if-ltz v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mPlaylistMemberIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private getTrackArtist(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 211
    iget v1, p0, Lcom/google/android/music/xdi/TrackAdapter;->mArtistIdx:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 212
    .local v0, "artist":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    iget-object v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mUnknownArtist:Ljava/lang/String;

    .line 215
    :cond_0
    return-object v0
.end method

.method private getTrackMetajamId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 219
    iget v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mTrackMetajamIdx:I

    if-ltz v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mTrackMetajamIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mTrackMetajamIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public populateDocumentFromCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)V
    .locals 4
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 113
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 114
    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 115
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 116
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 118
    iget v1, p0, Lcom/google/android/music/xdi/TrackAdapter;->mAudioIdIdx:I

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 120
    :cond_0
    iget v1, p0, Lcom/google/android/music/xdi/TrackAdapter;->mTitleIdx:I

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 121
    invoke-direct {p0, p2}, Lcom/google/android/music/xdi/TrackAdapter;->getPlaylistMemberId(Landroid/database/Cursor;)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setIdInParent(J)V

    .line 123
    iget v1, p0, Lcom/google/android/music/xdi/TrackAdapter;->mAlbumIdIdx:I

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumId(J)V

    .line 124
    iget v1, p0, Lcom/google/android/music/xdi/TrackAdapter;->mAlbumStoreIdIdx:I

    invoke-interface {p2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 125
    iget v1, p0, Lcom/google/android/music/xdi/TrackAdapter;->mAlbumStoreIdIdx:I

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 127
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/music/xdi/TrackAdapter;->getAlbum(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumName(Ljava/lang/String;)V

    .line 129
    invoke-direct {p0, p2}, Lcom/google/android/music/xdi/TrackAdapter;->getTrackArtist(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 130
    iget v1, p0, Lcom/google/android/music/xdi/TrackAdapter;->mTrackArtistIdIdx:I

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistId(J)V

    .line 132
    invoke-direct {p0, p2}, Lcom/google/android/music/xdi/TrackAdapter;->getTrackMetajamId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setTrackMetajamId(Ljava/lang/String;)V

    .line 133
    invoke-direct {p0, p2}, Lcom/google/android/music/xdi/TrackAdapter;->getAlbumMetajamId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 134
    invoke-direct {p0, p2}, Lcom/google/android/music/xdi/TrackAdapter;->getArtistMetajamId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistMetajamId(Ljava/lang/String;)V

    .line 135
    iget v1, p0, Lcom/google/android/music/xdi/TrackAdapter;->mStoreSongIdx:I

    invoke-interface {p2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 136
    iget v1, p0, Lcom/google/android/music/xdi/TrackAdapter;->mStoreSongIdx:I

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setSongStoreId(Ljava/lang/String;)V

    .line 138
    :cond_2
    iget v1, p0, Lcom/google/android/music/xdi/TrackAdapter;->mArtworkUrlIdx:I

    invoke-interface {p2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 139
    iget v1, p0, Lcom/google/android/music/xdi/TrackAdapter;->mArtworkUrlIdx:I

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 143
    :cond_3
    iget v1, p0, Lcom/google/android/music/xdi/TrackAdapter;->mDomainIdx:I

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    const/4 v0, 0x1

    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setIsNautilus(Z)V

    .line 145
    iget v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mDurationIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 146
    iget v0, p0, Lcom/google/android/music/xdi/TrackAdapter;->mDurationIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setDuration(J)V

    .line 149
    :cond_5
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 150
    return-void
.end method
