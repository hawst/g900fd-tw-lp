.class Lcom/google/android/music/preferences/MusicPreferences$7;
.super Lcom/google/android/music/preferences/IPreferenceChangeListener$Stub;
.source "MusicPreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/preferences/MusicPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/preferences/MusicPreferences;


# direct methods
.method constructor <init>(Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 0

    .prologue
    .line 1786
    iput-object p1, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-direct {p0}, Lcom/google/android/music/preferences/IPreferenceChangeListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onBooleanChanged(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1810
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    monitor-enter v1

    .line 1811
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/music/preferences/MusicPreferences;->access$600(Lcom/google/android/music/preferences/MusicPreferences;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1812
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1813
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # invokes: Lcom/google/android/music/preferences/MusicPreferences;->notifyPreferenceChangeListeners(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->access$700(Lcom/google/android/music/preferences/MusicPreferences;Ljava/lang/String;)V

    .line 1814
    return-void

    .line 1812
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onIntChanged(Ljava/lang/String;I)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1803
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    monitor-enter v1

    .line 1804
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/music/preferences/MusicPreferences;->access$600(Lcom/google/android/music/preferences/MusicPreferences;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1805
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1806
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # invokes: Lcom/google/android/music/preferences/MusicPreferences;->notifyPreferenceChangeListeners(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->access$700(Lcom/google/android/music/preferences/MusicPreferences;Ljava/lang/String;)V

    .line 1807
    return-void

    .line 1805
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onLongChanged(Ljava/lang/String;J)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1796
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    monitor-enter v1

    .line 1797
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/music/preferences/MusicPreferences;->access$600(Lcom/google/android/music/preferences/MusicPreferences;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1798
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1799
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # invokes: Lcom/google/android/music/preferences/MusicPreferences;->notifyPreferenceChangeListeners(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->access$700(Lcom/google/android/music/preferences/MusicPreferences;Ljava/lang/String;)V

    .line 1800
    return-void

    .line 1798
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onPreferenceRemoved(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1817
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    monitor-enter v1

    .line 1818
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/music/preferences/MusicPreferences;->access$600(Lcom/google/android/music/preferences/MusicPreferences;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1819
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1820
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # invokes: Lcom/google/android/music/preferences/MusicPreferences;->notifyPreferenceChangeListeners(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->access$700(Lcom/google/android/music/preferences/MusicPreferences;Ljava/lang/String;)V

    .line 1821
    return-void

    .line 1819
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onStringChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1789
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    monitor-enter v1

    .line 1790
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/music/preferences/MusicPreferences;->access$600(Lcom/google/android/music/preferences/MusicPreferences;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1791
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1792
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences$7;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # invokes: Lcom/google/android/music/preferences/MusicPreferences;->notifyPreferenceChangeListeners(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->access$700(Lcom/google/android/music/preferences/MusicPreferences;Ljava/lang/String;)V

    .line 1793
    return-void

    .line 1791
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
