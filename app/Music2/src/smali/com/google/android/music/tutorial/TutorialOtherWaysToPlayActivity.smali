.class public Lcom/google/android/music/tutorial/TutorialOtherWaysToPlayActivity;
.super Lcom/google/android/music/tutorial/TutorialActivity;
.source "TutorialOtherWaysToPlayActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mList:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialActivity;-><init>()V

    return-void
.end method

.method private initListView()V
    .locals 9

    .prologue
    const v8, 0x7f0b0144

    .line 49
    const v5, 0x7f0e027e

    invoke-virtual {p0, v5}, Lcom/google/android/music/tutorial/TutorialOtherWaysToPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/google/android/music/tutorial/TutorialOtherWaysToPlayActivity;->mList:Landroid/widget/ListView;

    .line 51
    new-instance v2, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;

    const v5, 0x7f0200dd

    const v6, 0x7f0b0138

    invoke-direct {v2, v5, v6, v8}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;-><init>(III)V

    .line 54
    .local v2, "listEntry1":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;
    new-instance v3, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;

    const v5, 0x7f020154

    const v6, 0x7f0b0137

    const v7, 0x7f0b0145

    invoke-direct {v3, v5, v6, v7}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;-><init>(III)V

    .line 57
    .local v3, "listEntry2":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;
    new-instance v4, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;

    const v5, 0x7f02016f

    const v6, 0x7f0b0139

    invoke-direct {v4, v5, v6, v8}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;-><init>(III)V

    .line 60
    .local v4, "listEntry3":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;
    const/4 v5, 0x3

    new-array v1, v5, [Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;

    const/4 v5, 0x0

    aput-object v2, v1, v5

    const/4 v5, 0x1

    aput-object v3, v1, v5

    const/4 v5, 0x2

    aput-object v4, v1, v5

    .line 63
    .local v1, "listData":[Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;
    new-instance v0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;

    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialOtherWaysToPlayActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    const v6, 0x7f0400fe

    invoke-direct {v0, p0, v5, v6, v1}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;I[Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;)V

    .line 65
    .local v0, "adapter":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;
    iget-object v5, p0, Lcom/google/android/music/tutorial/TutorialOtherWaysToPlayActivity;->mList:Landroid/widget/ListView;

    invoke-virtual {v5, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 67
    return-void
.end method


# virtual methods
.method public bridge synthetic isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 20
    invoke-super {p0}, Lcom/google/android/music/tutorial/TutorialActivity;->isActivityDestroyed()Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 43
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0e027f

    if-ne v0, v1, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialOtherWaysToPlayActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-static {p0, v2, v2, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->finishTutorialPermanently(Landroid/app/Activity;ZZLcom/google/android/music/preferences/MusicPreferences;)V

    .line 46
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/google/android/music/tutorial/TutorialActivity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const v2, 0x7f0400fc

    invoke-virtual {p0, v2}, Lcom/google/android/music/tutorial/TutorialOtherWaysToPlayActivity;->setContentView(I)V

    .line 29
    invoke-static {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/tutorial/TutorialOtherWaysToPlayActivity;->mTracker:Lcom/google/android/music/eventlog/MusicEventLogger;

    .line 31
    const v2, 0x7f0e01c5

    invoke-virtual {p0, v2}, Lcom/google/android/music/tutorial/TutorialOtherWaysToPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 32
    .local v1, "title":Landroid/widget/TextView;
    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 34
    const v2, 0x7f0e027f

    invoke-virtual {p0, v2}, Lcom/google/android/music/tutorial/TutorialOtherWaysToPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 35
    .local v0, "doneBtn":Landroid/widget/Button;
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 38
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialOtherWaysToPlayActivity;->initListView()V

    .line 39
    return-void
.end method
