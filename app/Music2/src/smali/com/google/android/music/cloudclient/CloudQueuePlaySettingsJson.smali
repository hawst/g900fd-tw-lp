.class public Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;
.super Lcom/google/api/client/json/GenericJson;
.source "CloudQueuePlaySettingsJson.java"


# instance fields
.field public mContentFilter:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "contentFilter"
    .end annotation
.end field

.field public mMode:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "mode"
    .end annotation
.end field

.field public mRadioContainerKey:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "radioContainerKey"
    .end annotation
.end field

.field public mShuffled:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "shuffled"
    .end annotation
.end field

.field public mVersion:Ljava/lang/Long;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "version"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
