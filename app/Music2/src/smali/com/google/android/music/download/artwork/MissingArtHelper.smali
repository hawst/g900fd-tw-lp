.class public Lcom/google/android/music/download/artwork/MissingArtHelper;
.super Ljava/lang/Object;
.source "MissingArtHelper.java"


# instance fields
.field private final mArtMonitor:Lcom/google/android/music/download/artwork/ArtMonitor;

.field private final mArtServiceIdleListener:Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;

.field private final mChangeListener:Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;

.field private final mContextRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private mIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;Lcom/google/android/music/download/artwork/ArtMonitor;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "changeListener"    # Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;
    .param p3, "artConnection"    # Lcom/google/android/music/download/artwork/ArtMonitor;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Lcom/google/android/music/download/artwork/MissingArtHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/artwork/MissingArtHelper$1;-><init>(Lcom/google/android/music/download/artwork/MissingArtHelper;)V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mArtServiceIdleListener:Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;

    .line 52
    const-string v0, "changeListener must not be null"

    invoke-static {p2, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v0, "artConnection must not be null"

    invoke-static {p3, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mContextRef:Ljava/lang/ref/WeakReference;

    .line 56
    iput-object p2, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mChangeListener:Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;

    .line 57
    iput-object p3, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mArtMonitor:Lcom/google/android/music/download/artwork/ArtMonitor;

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/download/artwork/MissingArtHelper;)Lcom/google/android/music/download/artwork/ArtMonitor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/MissingArtHelper;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mArtMonitor:Lcom/google/android/music/download/artwork/ArtMonitor;

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/music/download/artwork/MissingArtHelper;->unregister()V

    .line 80
    iget-object v0, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mIds:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 83
    :cond_0
    return-void
.end method

.method public register()V
    .locals 4

    .prologue
    .line 95
    iget-object v2, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mIds:Ljava/util/Set;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mIds:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 97
    iget-object v2, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mArtMonitor:Lcom/google/android/music/download/artwork/ArtMonitor;

    iget-object v3, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mArtServiceIdleListener:Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;

    invoke-interface {v2, v3}, Lcom/google/android/music/download/artwork/ArtMonitor;->registerArtServiceIdleListener(Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;)V

    .line 98
    iget-object v2, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mIds:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 99
    .local v1, "id":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mArtMonitor:Lcom/google/android/music/download/artwork/ArtMonitor;

    iget-object v3, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mChangeListener:Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;

    invoke-interface {v2, v1, v3}, Lcom/google/android/music/download/artwork/ArtMonitor;->registerArtChangeListener(Ljava/lang/String;Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;)V

    goto :goto_0

    .line 102
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "id":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public removeId(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mIds:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/google/android/music/download/artwork/MissingArtHelper;->unregister()V

    .line 89
    iget-object v0, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 90
    invoke-virtual {p0}, Lcom/google/android/music/download/artwork/MissingArtHelper;->register()V

    .line 92
    :cond_0
    return-void
.end method

.method public set(Ljava/util/Set;Z)V
    .locals 0
    .param p2, "registerIds"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/google/android/music/download/artwork/MissingArtHelper;->unregister()V

    .line 71
    iput-object p1, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mIds:Ljava/util/Set;

    .line 72
    if-eqz p2, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/google/android/music/download/artwork/MissingArtHelper;->register()V

    .line 75
    :cond_0
    return-void
.end method

.method protected startArtDownloads(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p1, "urls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 42
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 43
    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/music/download/artwork/ArtDownloadUtils;->startArtDownloads(Landroid/content/Context;Ljava/util/Collection;Z)V

    .line 45
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 120
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 121
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    iget-object v4, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mIds:Ljava/util/Set;

    if-eqz v4, :cond_1

    .line 124
    const/4 v0, 0x1

    .line 125
    .local v0, "first":Z
    iget-object v4, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mIds:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 126
    .local v2, "id":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 127
    const/4 v0, 0x0

    .line 131
    :goto_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 129
    :cond_0
    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 134
    .end local v0    # "first":Z
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "id":Ljava/lang/String;
    :cond_1
    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public unregister()V
    .locals 4

    .prologue
    .line 105
    iget-object v2, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mIds:Ljava/util/Set;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mIds:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 107
    iget-object v2, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mArtMonitor:Lcom/google/android/music/download/artwork/ArtMonitor;

    iget-object v3, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mArtServiceIdleListener:Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;

    invoke-interface {v2, v3}, Lcom/google/android/music/download/artwork/ArtMonitor;->unregisterArtServiceIdleListener(Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;)V

    .line 108
    iget-object v2, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mIds:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 109
    .local v1, "id":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mArtMonitor:Lcom/google/android/music/download/artwork/ArtMonitor;

    iget-object v3, p0, Lcom/google/android/music/download/artwork/MissingArtHelper;->mChangeListener:Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;

    invoke-interface {v2, v1, v3}, Lcom/google/android/music/download/artwork/ArtMonitor;->unregisterArtChangeListener(Ljava/lang/String;Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;)V

    goto :goto_0

    .line 112
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "id":Ljava/lang/String;
    :cond_0
    return-void
.end method
