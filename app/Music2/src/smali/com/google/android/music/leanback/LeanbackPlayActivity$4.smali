.class Lcom/google/android/music/leanback/LeanbackPlayActivity$4;
.super Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter;
.source "LeanbackPlayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity;->setupFragment(Landroid/app/Fragment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$4;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onBindDescription(Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;Ljava/lang/Object;)V
    .locals 4
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;
    .param p2, "itemObject"    # Ljava/lang/Object;

    .prologue
    .line 289
    move-object v1, p2

    check-cast v1, Lcom/google/android/music/leanback/Item;

    .line 290
    .local v1, "item":Lcom/google/android/music/leanback/Item;
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->getTitle()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getDescriptionGetter()Lcom/google/android/music/leanback/Item$StringGetter;

    move-result-object v0

    .line 292
    .local v0, "getter":Lcom/google/android/music/leanback/Item$StringGetter;
    if-eqz v0, :cond_0

    .line 293
    new-instance v2, Lcom/google/android/music/leanback/LeanbackPlayActivity$4$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/music/leanback/LeanbackPlayActivity$4$1;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity$4;Lcom/google/android/music/leanback/Item$StringGetter;Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;)V

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 309
    :goto_0
    return-void

    .line 307
    :cond_0
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->getSubtitle()Landroid/widget/TextView;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
