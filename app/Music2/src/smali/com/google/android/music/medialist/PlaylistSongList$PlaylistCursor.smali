.class public Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;
.super Lcom/google/android/music/medialist/MediaList$MediaCursor;
.source "PlaylistSongList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/medialist/PlaylistSongList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "PlaylistCursor"
.end annotation


# instance fields
.field mCursor:Landroid/database/Cursor;

.field mId:J

.field mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "id"    # J

    .prologue
    .line 220
    invoke-direct {p0, p2}, Lcom/google/android/music/medialist/MediaList$MediaCursor;-><init>(Landroid/database/Cursor;)V

    .line 221
    iput-wide p3, p0, Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;->mId:J

    .line 222
    iput-object p2, p0, Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;->mCursor:Landroid/database/Cursor;

    .line 223
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;->mResolver:Landroid/content/ContentResolver;

    .line 224
    return-void
.end method


# virtual methods
.method public moveItem(II)V
    .locals 8
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    .line 229
    if-eq p1, p2, :cond_0

    .line 230
    iget-object v1, p0, Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;->mCursor:Landroid/database/Cursor;

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 232
    .local v0, "colidx":I
    iget-object v1, p0, Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 233
    const-string v1, "PlaylistSongList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to move item. Invalid \"from\" position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Cursor size:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    .end local v0    # "colidx":I
    :cond_0
    :goto_0
    return-void

    .line 237
    .restart local v0    # "colidx":I
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 239
    .local v4, "itemToMoveId":J
    const-wide/16 v6, 0x0

    .line 240
    .local v6, "desiredPreviousItemId":J
    if-lez p2, :cond_4

    .line 241
    if-le p1, p2, :cond_2

    .line 242
    add-int/lit8 p2, p2, -0x1

    .line 244
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 245
    const-string v1, "PlaylistSongList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to move item. Invalid \"to\" position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Cursor size:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 249
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 251
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;->mResolver:Landroid/content/ContentResolver;

    iget-wide v2, p0, Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;->mId:J

    invoke-static/range {v1 .. v7}, Lcom/google/android/music/store/MusicContent$Playlists;->movePlaylistItem(Landroid/content/ContentResolver;JJJ)V

    goto :goto_0
.end method
