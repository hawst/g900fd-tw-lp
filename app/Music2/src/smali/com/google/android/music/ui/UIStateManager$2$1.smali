.class Lcom/google/android/music/ui/UIStateManager$2$1;
.super Ljava/lang/Object;
.source "UIStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/UIStateManager$2;->onStreamabilityChanged(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/ui/UIStateManager$2;

.field final synthetic val$isStreamable:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/UIStateManager$2;Z)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/google/android/music/ui/UIStateManager$2$1;->this$1:Lcom/google/android/music/ui/UIStateManager$2;

    iput-boolean p2, p0, Lcom/google/android/music/ui/UIStateManager$2$1;->val$isStreamable:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$2$1;->this$1:Lcom/google/android/music/ui/UIStateManager$2;

    iget-object v0, v0, Lcom/google/android/music/ui/UIStateManager$2;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # getter for: Lcom/google/android/music/ui/UIStateManager;->mIsStreamingEnabled:Z
    invoke-static {v0}, Lcom/google/android/music/ui/UIStateManager;->access$200(Lcom/google/android/music/ui/UIStateManager;)Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/music/ui/UIStateManager$2$1;->val$isStreamable:Z

    if-eq v0, v1, :cond_0

    .line 148
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$2$1;->this$1:Lcom/google/android/music/ui/UIStateManager$2;

    iget-object v0, v0, Lcom/google/android/music/ui/UIStateManager$2;->this$0:Lcom/google/android/music/ui/UIStateManager;

    iget-boolean v1, p0, Lcom/google/android/music/ui/UIStateManager$2$1;->val$isStreamable:Z

    # setter for: Lcom/google/android/music/ui/UIStateManager;->mIsStreamingEnabled:Z
    invoke-static {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->access$202(Lcom/google/android/music/ui/UIStateManager;Z)Z

    .line 149
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$2$1;->this$1:Lcom/google/android/music/ui/UIStateManager$2;

    iget-object v0, v0, Lcom/google/android/music/ui/UIStateManager$2;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # invokes: Lcom/google/android/music/ui/UIStateManager;->notifyStreamabilityChanged()V
    invoke-static {v0}, Lcom/google/android/music/ui/UIStateManager;->access$300(Lcom/google/android/music/ui/UIStateManager;)V

    .line 151
    :cond_0
    return-void
.end method
