.class Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;
.super Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
.source "AsyncAlbumArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PlaylistMode"
.end annotation


# instance fields
.field public id:J

.field public mainLabel:Ljava/lang/String;

.field public style:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;)V
    .locals 2
    .param p1, "other"    # Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;

    .prologue
    .line 242
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    .line 243
    iget-wide v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->id:J

    iput-wide v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->id:J

    .line 244
    iget-object v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->mainLabel:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->mainLabel:Ljava/lang/String;

    .line 245
    iget v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->style:I

    iput v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->style:I

    .line 246
    return-void
.end method


# virtual methods
.method public copy()Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .locals 1

    .prologue
    .line 272
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;

    invoke-direct {v0, p0}, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;)V

    return-object v0
.end method

.method public createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "missingUrlSink"    # Lcom/google/android/music/download/artwork/RemoteUrlSink;

    .prologue
    .line 251
    iget v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->style:I

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->id:J

    iget-object v8, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->mainLabel:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    move-object v1, p1

    move v6, p2

    move/from16 v7, p3

    move-object/from16 v11, p4

    invoke-static/range {v1 .. v12}, Lcom/google/android/music/utils/AlbumArtUtils;->getFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 253
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 263
    instance-of v2, p1, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;

    if-nez v2, :cond_1

    .line 267
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 266
    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;

    .line 267
    .local v0, "o":Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;
    iget-wide v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->id:J

    iget-wide v4, v0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->id:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->mainLabel:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->mainLabel:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->style:I

    iget v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->style:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/String;JI)Z
    .locals 2
    .param p1, "mainLabel"    # Ljava/lang/String;
    .param p2, "id"    # J
    .param p4, "style"    # I

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->mainLabel:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->id:J

    cmp-long v0, v0, p2

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->style:I

    if-ne v0, p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PlaylistMode<id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mainLabel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->mainLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", style:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->style:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
