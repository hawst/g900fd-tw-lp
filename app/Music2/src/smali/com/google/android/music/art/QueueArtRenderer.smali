.class public Lcom/google/android/music/art/QueueArtRenderer;
.super Ljava/lang/Object;
.source "QueueArtRenderer.java"


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ALBUM_ART:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/art/QueueArtRenderer;->LOGV:Z

    return-void
.end method

.method private static createQueueHeaderBitmap(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/google/android/music/art/QueueArtRenderer;->getQueueWidth(Landroid/content/Context;I)I

    move-result v0

    .line 73
    .local v0, "shortEdge":I
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, p2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method private static getQueueWidth(Landroid/content/Context;I)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "w"    # I

    .prologue
    .line 98
    const-string v3, "window"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 100
    .local v2, "wm":Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 101
    .local v0, "display":Landroid/view/Display;
    invoke-static {v0}, Lcom/google/android/music/utils/ViewUtils;->getScreenSortedDimensions(Landroid/view/Display;)Landroid/util/Pair;

    move-result-object v1

    .line 102
    .local v1, "screenSize":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-lez p1, :cond_0

    .line 103
    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3, p1}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 109
    :goto_0
    return v3

    .line 108
    :cond_0
    const-string v3, "QueueArtRenderer"

    const-string v4, "getQueueWidth: w is 0. This shouldn\'t happen."

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    goto :goto_0
.end method

.method public static makeArtForBitmaps(Landroid/content/Context;Ljava/util/List;II)Landroid/graphics/Bitmap;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "w"    # I
    .param p3, "h"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;II)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 33
    .local p1, "bitmaps":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-static {v0, v1}, Lcom/google/android/music/art/QueueArtRenderer;->getQueueWidth(Landroid/content/Context;I)I

    move-result v10

    .line 34
    .local v10, "shortEdge":I
    move v11, v10

    .line 35
    .local v11, "slotWidth":I
    const/4 v4, 0x0

    .line 36
    .local v4, "artCount":I
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v15

    if-lez v15, :cond_0

    .line 37
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v15

    const/16 v16, 0x4

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 38
    div-int/2addr v11, v4

    .line 41
    :cond_0
    sget-boolean v15, Lcom/google/android/music/art/QueueArtRenderer;->LOGV:Z

    if-eqz v15, :cond_1

    .line 42
    const-string v15, "QueueArtRenderer"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "makeArtForBitmaps artCount="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", slotWidth="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", w="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", h="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :cond_1
    if-nez v4, :cond_2

    .line 46
    const/16 v15, 0x65

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/music/art/StaticArt;->getArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 48
    .local v14, "staticArt":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v14, v1, v2}, Lcom/google/android/music/art/QueueArtRenderer;->tileDefaultArtBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 67
    .end local v14    # "staticArt":Landroid/graphics/Bitmap;
    :goto_0
    return-object v5

    .line 50
    :cond_2
    if-nez p3, :cond_3

    .line 51
    const-string v15, "QueueArtRenderer"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "h=0 when requesting art, substituting h=w to prevent crash. bitmaps="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", w="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", h="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    move/from16 p3, v10

    .line 55
    :cond_3
    sget-object v15, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p3

    invoke-static {v10, v0, v15}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 56
    .local v5, "bmp":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 57
    .local v6, "canvas":Landroid/graphics/Canvas;
    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    .line 58
    .local v9, "paint":Landroid/graphics/Paint;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v4, :cond_4

    .line 59
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    .line 60
    .local v3, "art":Landroid/graphics/Bitmap;
    move/from16 v0, p3

    invoke-static {v3, v11, v0}, Lcom/google/android/music/art/ArtRenderingUtils;->sliceBitmapSectionAndScale(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 61
    .local v12, "slotted":Landroid/graphics/Bitmap;
    new-instance v13, Landroid/graphics/Rect;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    move/from16 v1, p3

    invoke-direct {v13, v15, v0, v11, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 62
    .local v13, "srcRect":Landroid/graphics/Rect;
    new-instance v7, Landroid/graphics/Rect;

    mul-int v15, v11, v8

    const/16 v16, 0x0

    add-int/lit8 v17, v8, 0x1

    mul-int v17, v17, v11

    move/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, p3

    invoke-direct {v7, v15, v0, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 63
    .local v7, "destRect":Landroid/graphics/Rect;
    invoke-virtual {v6, v12, v13, v7, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 64
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->recycle()V

    .line 58
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 66
    .end local v3    # "art":Landroid/graphics/Bitmap;
    .end local v7    # "destRect":Landroid/graphics/Rect;
    .end local v12    # "slotted":Landroid/graphics/Bitmap;
    .end local v13    # "srcRect":Landroid/graphics/Rect;
    :cond_4
    invoke-virtual {v6}, Landroid/graphics/Canvas;->save()I

    goto/16 :goto_0
.end method

.method public static tileDefaultArtBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bmp"    # Landroid/graphics/Bitmap;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v11, 0x0

    .line 77
    invoke-static {p0, p2, p3}, Lcom/google/android/music/art/QueueArtRenderer;->createQueueHeaderBitmap(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 78
    .local v3, "result":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 79
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 80
    .local v2, "p":Landroid/graphics/Paint;
    int-to-float v8, p3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    div-float v4, v8, v9

    .line 81
    .local v4, "scale":F
    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-direct {v6, v11, v11, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 82
    .local v6, "srcRect":Landroid/graphics/Rect;
    const/4 v7, 0x0

    .line 83
    .local v7, "w":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v4

    float-to-int v5, v8

    .line 84
    .local v5, "scaledBmpWidth":I
    sget-boolean v8, Lcom/google/android/music/art/QueueArtRenderer;->LOGV:Z

    if-eqz v8, :cond_0

    .line 85
    const-string v8, "QueueArtRenderer"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "width: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", height="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", scale="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", scaledBmpWidth="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_0
    :goto_0
    if-ge v7, p2, :cond_1

    .line 89
    new-instance v1, Landroid/graphics/Rect;

    add-int v8, v7, v5

    invoke-direct {v1, v7, v11, v8, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 90
    .local v1, "destRect":Landroid/graphics/Rect;
    invoke-virtual {v0, p1, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 91
    add-int/2addr v7, v5

    .line 92
    goto :goto_0

    .line 93
    .end local v1    # "destRect":Landroid/graphics/Rect;
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 94
    return-object v3
.end method
