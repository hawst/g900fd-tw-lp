.class Lcom/google/android/music/playback/LocalDevicePlayback$21;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->refreshRadio()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

.field final synthetic val$state:Lcom/google/android/music/mix/MixGenerationState;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/mix/MixGenerationState;)V
    .locals 0

    .prologue
    .line 2205
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$21;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iput-object p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$21;->val$state:Lcom/google/android/music/mix/MixGenerationState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 2210
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$21;->val$state:Lcom/google/android/music/mix/MixGenerationState;

    invoke-virtual {v1}, Lcom/google/android/music/mix/MixGenerationState;->getMix()Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v0

    .line 2211
    .local v0, "md":Lcom/google/android/music/mix/MixDescriptor;
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$21;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayQueueFeeder:Lcom/google/android/music/mix/PlayQueueFeeder;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$6600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/mix/PlayQueueFeeder;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$21;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->canStream()Z
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$6500(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v3

    sget-object v4, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->CLEAR_REFRESH:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/music/mix/PlayQueueFeeder;->requestContent(Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List;ZLcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    .line 2215
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$21;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$21;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/eventlog/MusicEventLogger;->logRefreshRadioAsync(Lcom/google/android/music/store/ContainerDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2218
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$21;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2220
    return-void

    .line 2218
    .end local v0    # "md":Lcom/google/android/music/mix/MixDescriptor;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$21;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v1
.end method
