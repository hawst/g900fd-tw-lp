.class final Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;
.super Ljava/lang/Object;
.source "AppNavigationMetajamHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/AppNavigationMetajamHelper;->showNautilusAlbum(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Lcom/google/android/music/ui/BaseActivity;

.field final synthetic val$callback:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

.field final synthetic val$metajamId:Ljava/lang/String;

.field final synthetic val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseActivity;Ljava/lang/String;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 0

    .prologue
    .line 470
    iput-object p1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iput-object p2, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$metajamId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    iput-object p4, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$callback:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 472
    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 531
    :cond_0
    :goto_0
    return-void

    .line 475
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$metajamId:Ljava/lang/String;

    invoke-static {v0, v1, v6}, Lcom/google/android/music/store/MusicContent$Albums;->getAudioInNautilusAlbumCount(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v0

    if-lez v0, :cond_2

    move v9, v7

    .line 479
    .local v9, "availableInNautilus":Z
    :goto_1
    if-eqz v9, :cond_3

    .line 480
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "album_name"

    aput-object v0, v2, v6

    .line 481
    .local v2, "cols":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$metajamId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/music/store/MusicContent$Albums;->getNautilusAlbumsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v8, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5$1;

    invoke-direct {v8, p0}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5$1;-><init>(Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;)V

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v8}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/music/utils/MusicUtils$QueryCallback;)V

    goto :goto_0

    .end local v2    # "cols":[Ljava/lang/String;
    .end local v9    # "availableInNautilus":Z
    :cond_2
    move v9, v6

    .line 475
    goto :goto_1

    .line 523
    .restart local v9    # "availableInNautilus":Z
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    invoke-virtual {v0}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->canSendToStore()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 524
    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$metajamId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/music/purchase/Finsky;->startBuyAlbumActivity(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 525
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$callback:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    if-eqz v0, :cond_5

    .line 526
    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$callback:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    invoke-interface {v0}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;->onAlbumError()V

    goto :goto_0

    .line 528
    :cond_5
    iget-object v0, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->finish()V

    goto :goto_0
.end method
