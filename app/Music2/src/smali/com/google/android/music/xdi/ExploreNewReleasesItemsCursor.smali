.class Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;
.super Lcom/google/android/music/xdi/XdiCursor;
.source "ExploreNewReleasesItemsCursor.java"


# instance fields
.field private final mGroupType:I

.field private final mImageHeight:I

.field private final mImageWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;JILjava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "groupId"    # J
    .param p5, "groupType"    # I
    .param p6, "genreId"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-static {p1, p3, p4, p5, p6}, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;->getCursorForHeader(Landroid/content/Context;JILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/xdi/XdiCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/database/Cursor;)V

    .line 29
    iput p5, p0, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;->mGroupType:I

    .line 30
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 31
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;->mImageWidth:I

    .line 32
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;->mImageHeight:I

    .line 33
    return-void
.end method

.method private static getCursorForHeader(Landroid/content/Context;JILjava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "groupId"    # J
    .param p3, "groupType"    # I
    .param p4, "genreId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 37
    invoke-static {p3}, Lcom/google/android/music/xdi/XdiUtils;->getClusterProjection(I)[Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "projection":[Ljava/lang/String;
    if-nez v2, :cond_0

    .line 40
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 43
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2, p4}, Lcom/google/android/music/store/MusicContent$Explore;->getNewReleasesUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected extractDataForCurrentRow([Ljava/lang/Object;)Z
    .locals 7
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    iget v4, p0, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;->mGroupType:I

    iget v5, p0, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;->mImageWidth:I

    iget v6, p0, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;->mImageHeight:I

    move-object v3, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/music/xdi/XdiUtils;->extractDataForGroupType(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/xdi/ProjectionMap;[Ljava/lang/Object;III)Z

    move-result v0

    return v0
.end method
