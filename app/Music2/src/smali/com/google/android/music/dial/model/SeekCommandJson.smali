.class public Lcom/google/android/music/dial/model/SeekCommandJson;
.super Lcom/google/android/music/dial/model/GenericDialCommand;
.source "SeekCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/model/SeekCommandJson$SeekCommandBody;
    }
.end annotation


# instance fields
.field public mBody:Lcom/google/android/music/dial/model/SeekCommandJson$SeekCommandBody;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "body"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/music/dial/model/GenericDialCommand;-><init>()V

    .line 31
    iget-object v0, p0, Lcom/google/android/music/dial/model/SeekCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "session:1"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mNamespace:Ljava/lang/String;

    .line 32
    iget-object v0, p0, Lcom/google/android/music/dial/model/SeekCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "seek"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mCommand:Ljava/lang/String;

    .line 34
    new-instance v0, Lcom/google/android/music/dial/model/SeekCommandJson$SeekCommandBody;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/SeekCommandJson$SeekCommandBody;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/dial/model/SeekCommandJson;->mBody:Lcom/google/android/music/dial/model/SeekCommandJson$SeekCommandBody;

    .line 35
    return-void
.end method


# virtual methods
.method protected getBodyAsJsonString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/music/dial/model/SeekCommandJson;->mBody:Lcom/google/android/music/dial/model/SeekCommandJson$SeekCommandBody;

    invoke-virtual {v0}, Lcom/google/android/music/dial/model/SeekCommandJson$SeekCommandBody;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
