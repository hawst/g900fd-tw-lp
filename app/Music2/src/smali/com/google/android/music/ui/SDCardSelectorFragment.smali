.class public Lcom/google/android/music/ui/SDCardSelectorFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "SDCardSelectorFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;,
        Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field public SIZE_SORT_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/music/download/cache/CacheLocation;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentLocation:Lcom/google/android/music/download/cache/CacheLocation;

.field private mEnableDebugUi:Z

.field private mFakeRowObject:Ljava/lang/Object;

.field private mListAdapter:Landroid/widget/BaseAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mLocationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/cache/CacheLocation;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

.field private mShowCardCapability:Z

.field private mShowMissingCard:Z

.field private mUnavailableOpacity:F

.field private mUnitStrings:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CACHE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/SDCardSelectorFragment;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 50
    new-instance v0, Lcom/google/android/music/ui/SDCardSelectorFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/SDCardSelectorFragment$1;-><init>(Lcom/google/android/music/ui/SDCardSelectorFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->SIZE_SORT_COMPARATOR:Ljava/util/Comparator;

    .line 69
    iput-boolean v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mShowMissingCard:Z

    .line 71
    iput-boolean v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mShowCardCapability:Z

    .line 72
    iput-boolean v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mEnableDebugUi:Z

    .line 77
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mFakeRowObject:Ljava/lang/Object;

    .line 179
    new-instance v0, Lcom/google/android/music/ui/SDCardSelectorFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/SDCardSelectorFragment$3;-><init>(Lcom/google/android/music/ui/SDCardSelectorFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mListAdapter:Landroid/widget/BaseAdapter;

    .line 295
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/SDCardSelectorFragment;)Lcom/google/android/music/download/cache/CacheLocationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SDCardSelectorFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/SDCardSelectorFragment;Lcom/google/android/music/download/cache/CacheLocation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SDCardSelectorFragment;
    .param p1, "x1"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/SDCardSelectorFragment;->checkLocation(Lcom/google/android/music/download/cache/CacheLocation;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/music/ui/SDCardSelectorFragment;)Lcom/google/android/music/download/cache/CacheLocation;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SDCardSelectorFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mCurrentLocation:Lcom/google/android/music/download/cache/CacheLocation;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/ui/SDCardSelectorFragment;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SDCardSelectorFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mUnitStrings:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/music/ui/SDCardSelectorFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SDCardSelectorFragment;

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mUnavailableOpacity:F

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/SDCardSelectorFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SDCardSelectorFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/music/ui/SDCardSelectorFragment;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/SDCardSelectorFragment;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/SDCardSelectorFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/SDCardSelectorFragment;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->updateList()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/SDCardSelectorFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SDCardSelectorFragment;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mShowCardCapability:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/SDCardSelectorFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SDCardSelectorFragment;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mShowMissingCard:Z

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/SDCardSelectorFragment;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SDCardSelectorFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mFakeRowObject:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/SDCardSelectorFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SDCardSelectorFragment;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mEnableDebugUi:Z

    return v0
.end method

.method static synthetic access$800()Z
    .locals 1

    .prologue
    .line 42
    sget-boolean v0, Lcom/google/android/music/ui/SDCardSelectorFragment;->LOGV:Z

    return v0
.end method

.method private checkLocation(Lcom/google/android/music/download/cache/CacheLocation;)Z
    .locals 6
    .param p1, "loc"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 131
    iget-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    invoke-virtual {v2, p1}, Lcom/google/android/music/download/cache/CacheLocationManager;->getVolumeInformation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;

    move-result-object v0

    .line 132
    .local v0, "info":Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    iget-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    invoke-virtual {v2, p1}, Lcom/google/android/music/download/cache/CacheLocationManager;->getStorageMetadata(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->isEmulated()Z

    move-result v1

    .line 133
    .local v1, "isEmulated":Z
    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->getFreeSpaceInMegaBytes()J

    move-result-wide v2

    const-wide/16 v4, 0x1f4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/music/download/cache/CacheLocation;->hasMusicFiles()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lcom/google/android/music/ui/SDCardSelectorFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/SDCardSelectorFragment$2;-><init>(Lcom/google/android/music/ui/SDCardSelectorFragment;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 164
    return-void
.end method

.method private updateList()V
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mListAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 168
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 81
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "music_debug_logs_enabled"

    invoke-static {v2, v5, v4}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-boolean v2, Lcom/google/android/music/ui/SDCardSelectorFragment;->LOGV:Z

    if-eqz v2, :cond_2

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mEnableDebugUi:Z

    .line 84
    sget-boolean v2, Lcom/google/android/music/ui/SDCardSelectorFragment;->LOGV:Z

    if-eqz v2, :cond_0

    .line 85
    const-string v5, "SDCardSelectorFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Debug UI is "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mEnableDebugUi:Z

    if-eqz v2, :cond_3

    const-string v2, "enabled"

    :goto_1
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    .line 88
    invoke-direct {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->init()V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/utils/IOUtils;->getSizeUnitStrings(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mUnitStrings:[Ljava/lang/String;

    .line 90
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 91
    .local v1, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getSelectedStorageVolumeId()Ljava/util/UUID;

    move-result-object v0

    .line 92
    .local v0, "currentVolId":Ljava/util/UUID;
    iget-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    invoke-virtual {v2, v0}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownLocationByID(Ljava/util/UUID;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mCurrentLocation:Lcom/google/android/music/download/cache/CacheLocation;

    .line 93
    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mCurrentLocation:Lcom/google/android/music/download/cache/CacheLocation;

    if-nez v2, :cond_4

    :goto_2
    iput-boolean v3, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mShowMissingCard:Z

    .line 94
    iget-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mCurrentLocation:Lcom/google/android/music/download/cache/CacheLocation;

    if-nez v2, :cond_1

    .line 95
    iget-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInternal(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mCurrentLocation:Lcom/google/android/music/download/cache/CacheLocation;

    .line 97
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    invoke-virtual {v2}, Lcom/google/android/music/download/cache/CacheLocationManager;->deviceHasExternalStorage()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mShowCardCapability:Z

    .line 98
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0011

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mUnavailableOpacity:F

    .line 100
    return-void

    .end local v0    # "currentVolId":Ljava/util/UUID;
    .end local v1    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_2
    move v2, v4

    .line 82
    goto/16 :goto_0

    .line 85
    :cond_3
    const-string v2, "disabled"

    goto :goto_1

    .restart local v0    # "currentVolId":Ljava/util/UUID;
    .restart local v1    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_4
    move v3, v4

    .line 93
    goto :goto_2
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 105
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f0400d3

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 107
    .local v4, "root":Landroid/view/View;
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    .line 109
    .local v3, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const v5, 0x7f0e0250

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mListView:Landroid/widget/ListView;

    .line 110
    iget-object v5, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mListAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 111
    iget-object v5, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 113
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 114
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v5, 0x7f0b02b1

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 115
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 117
    sget-boolean v5, Lcom/google/android/music/ui/SDCardSelectorFragment;->LOGV:Z

    if-eqz v5, :cond_0

    .line 118
    const-string v5, "SDCardSelectorFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "current secondary external storage is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->getSelectedStorageVolumeId()Ljava/util/UUID;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :cond_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 123
    .local v1, "d":Landroid/app/Dialog;
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 124
    return-object v1
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 172
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 174
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 177
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "view"    # Landroid/view/View;
    .param p3, "i"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 345
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v7, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mListAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v7, p3}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 346
    .local v1, "item":Ljava/lang/Object;
    iget-object v7, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mFakeRowObject:Ljava/lang/Object;

    if-ne v1, v7, :cond_0

    .line 348
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->dismiss()V

    .line 373
    :goto_0
    return-void

    :cond_0
    move-object v2, v1

    .line 351
    check-cast v2, Lcom/google/android/music/download/cache/CacheLocation;

    .line 352
    .local v2, "loc":Lcom/google/android/music/download/cache/CacheLocation;
    iget-object v7, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    invoke-virtual {v7, v2}, Lcom/google/android/music/download/cache/CacheLocationManager;->getVolumeInformation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;

    move-result-object v0

    .line 354
    .local v0, "information":Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    iget-object v7, p0, Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;

    invoke-virtual {v7, v2}, Lcom/google/android/music/download/cache/CacheLocationManager;->getStorageMetadata(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    move-result-object v3

    .line 356
    .local v3, "metadata":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    const-string v7, "SDCardSelectorFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Storage selected: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    .line 358
    .local v4, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v2}, Lcom/google/android/music/download/cache/CacheLocation;->getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v7

    sget-object v8, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->INTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    if-ne v7, v8, :cond_1

    .line 359
    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->clearSelectedStorageVolume()V

    .line 360
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->dismiss()V

    goto :goto_0

    .line 362
    :cond_1
    invoke-static {v2}, Lcom/google/android/music/download/cache/CacheUtils;->setupSecondaryStorageLocation(Lcom/google/android/music/download/cache/CacheLocation;)Z

    move-result v6

    .line 363
    .local v6, "usableLocation":Z
    if-eqz v6, :cond_2

    .line 364
    invoke-virtual {v4, v2, v3, v0}, Lcom/google/android/music/preferences/MusicPreferences;->setSelectedStorageVolume(Lcom/google/android/music/download/cache/CacheLocation;Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;)V

    .line 365
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->dismiss()V

    goto :goto_0

    .line 367
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    const v8, 0x7f0b02b5

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    .line 369
    .local v5, "toast":Landroid/widget/Toast;
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
