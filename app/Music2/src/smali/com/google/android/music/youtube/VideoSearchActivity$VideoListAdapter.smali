.class Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "VideoSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/youtube/VideoSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VideoListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/music/youtube/YouTubeVideo;",
        ">;"
    }
.end annotation


# instance fields
.field private final mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/google/android/music/youtube/VideoSearchActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/music/youtube/VideoSearchActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/youtube/YouTubeVideo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 140
    .local p4, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/youtube/YouTubeVideo;>;"
    iput-object p1, p0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    .line 141
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 142
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 143
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 147
    const/4 v1, 0x0

    .line 148
    .local v1, "view":Landroid/view/View;
    if-nez p2, :cond_0

    .line 149
    iget-object v2, p0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f040106

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 150
    new-instance v2, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;

    invoke-direct {v2, p0, v1}, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;-><init>(Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 155
    :goto_0
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;

    .line 156
    .local v0, "vh":Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;
    invoke-virtual {p0, p1}, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/youtube/YouTubeVideo;

    invoke-virtual {v0, v2}, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;->show(Lcom/google/android/music/youtube/YouTubeVideo;)V

    .line 157
    return-object v1

    .line 152
    .end local v0    # "vh":Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;
    :cond_0
    move-object v1, p2

    goto :goto_0
.end method
