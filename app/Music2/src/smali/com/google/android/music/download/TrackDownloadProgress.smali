.class public Lcom/google/android/music/download/TrackDownloadProgress;
.super Lcom/google/android/music/download/BaseDownloadProgress;
.source "TrackDownloadProgress.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/music/download/BaseDownloadProgress",
        "<",
        "Lcom/google/android/music/download/TrackDownloadRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final mSeekMs:J

.field private final mStreamFidelity:I

.field private final mTrackTitle:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/music/download/BaseDownloadProgress;-><init>(Landroid/os/Parcel;)V

    .line 31
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mTrackTitle:Ljava/lang/String;

    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mSeekMs:J

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mStreamFidelity:I

    .line 34
    return-void
.end method

.method constructor <init>(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/DownloadState;J)V
    .locals 1
    .param p1, "downloadRequest"    # Lcom/google/android/music/download/TrackDownloadRequest;
    .param p2, "downloadState"    # Lcom/google/android/music/download/DownloadState;
    .param p3, "seekMs"    # J

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/download/BaseDownloadProgress;-><init>(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)V

    .line 24
    iput-wide p3, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mSeekMs:J

    .line 25
    invoke-virtual {p1}, Lcom/google/android/music/download/TrackDownloadRequest;->getTrackTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mTrackTitle:Ljava/lang/String;

    .line 26
    invoke-virtual {p2}, Lcom/google/android/music/download/DownloadState;->getStreamFidelity()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mStreamFidelity:I

    .line 27
    return-void
.end method


# virtual methods
.method protected bridge synthetic checkFullCopy(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)Z
    .locals 1
    .param p1, "x0"    # Lcom/google/android/music/download/DownloadRequest;
    .param p2, "x1"    # Lcom/google/android/music/download/DownloadState;

    .prologue
    .line 11
    check-cast p1, Lcom/google/android/music/download/TrackDownloadRequest;

    .end local p1    # "x0":Lcom/google/android/music/download/DownloadRequest;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/download/TrackDownloadProgress;->checkFullCopy(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/DownloadState;)Z

    move-result v0

    return v0
.end method

.method protected checkFullCopy(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/DownloadState;)Z
    .locals 4
    .param p1, "downloadRequest"    # Lcom/google/android/music/download/TrackDownloadRequest;
    .param p2, "state"    # Lcom/google/android/music/download/DownloadState;

    .prologue
    .line 76
    invoke-super {p0, p1, p2}, Lcom/google/android/music/download/BaseDownloadProgress;->checkFullCopy(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method protected getOwnerFromInt(I)Lcom/google/android/music/download/DownloadRequest$Owner;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 38
    invoke-static {}, Lcom/google/android/music/download/TrackOwner;->values()[Lcom/google/android/music/download/TrackOwner;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getSeekMs()J
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mSeekMs:J

    return-wide v0
.end method

.method public getStreamFidelity()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mStreamFidelity:I

    return v0
.end method

.method public getTrackTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mTrackTitle:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    const-string v1, " mTrackTitle=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mTrackTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    const-string v1, " mSeekMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mSeekMs:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 62
    const-string v1, " mStreamFidelity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mStreamFidelity:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 63
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 84
    invoke-super {p0, p1, p2}, Lcom/google/android/music/download/BaseDownloadProgress;->writeToParcel(Landroid/os/Parcel;I)V

    .line 85
    iget-object v0, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mTrackTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86
    iget-wide v0, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mSeekMs:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 87
    iget v0, p0, Lcom/google/android/music/download/TrackDownloadProgress;->mStreamFidelity:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 88
    return-void
.end method
