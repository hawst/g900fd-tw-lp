.class public Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;
.super Ljava/lang/Object;
.source "OtherWaysToPlayListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OtherWaysToPlayListEntry"
.end annotation


# instance fields
.field mFirstTextId:I

.field mIconId:I

.field mSecondTextId:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "iconId"    # I
    .param p2, "textId_1"    # I
    .param p3, "textId_2"    # I

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput p1, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;->mIconId:I

    .line 37
    iput p2, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;->mFirstTextId:I

    .line 38
    iput p3, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;->mSecondTextId:I

    .line 39
    return-void
.end method
