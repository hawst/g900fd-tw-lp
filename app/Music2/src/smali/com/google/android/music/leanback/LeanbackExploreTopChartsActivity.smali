.class public Lcom/google/android/music/leanback/LeanbackExploreTopChartsActivity;
.super Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;
.source "LeanbackExploreTopChartsActivity.java"


# instance fields
.field private mGenreId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getGroupItemsUri(J)Landroid/net/Uri;
    .locals 1
    .param p1, "groupId"    # J

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreTopChartsActivity;->mGenreId:Ljava/lang/String;

    invoke-static {p1, p2, v0}, Lcom/google/android/music/store/MusicContent$Explore;->getTopChartsUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected getGroupsUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreTopChartsActivity;->mGenreId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Explore;->getTopChartGroupsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackExploreTopChartsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "genre_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreTopChartsActivity;->mGenreId:Ljava/lang/String;

    .line 32
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    return-void
.end method
