.class public Lcom/google/android/music/ui/AppNavigationMetajamHelper;
.super Ljava/lang/Object;
.source "AppNavigationMetajamHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;,
        Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->LOGV:Z

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 30
    sget-boolean v0, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->LOGV:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "x1"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    .param p2, "x2"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    .prologue
    .line 30
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->openMetajamItemFromNautilus(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "x1"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    .param p2, "x2"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    .prologue
    .line 30
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->openTrackInStore(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    return-void
.end method

.method private static checkForAlbumInLocker(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 9
    .param p0, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "openMetajamItemInfo"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    .param p2, "callback"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 223
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "album_name"

    aput-object v0, v2, v6

    const-string v0, "album_id"

    aput-object v0, v2, v7

    .line 225
    .local v2, "cols":[Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->getMetajamId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Albums;->getLockerAlbumUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v8, Lcom/google/android/music/ui/AppNavigationMetajamHelper$2;

    invoke-direct {v8, p0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$2;-><init>(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v8}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/music/utils/MusicUtils$QueryCallback;)V

    .line 261
    return-void
.end method

.method private static checkForArtistInLocker(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 9
    .param p0, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "openMetajamItemInfo"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    .param p2, "callback"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 274
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "artist"

    aput-object v0, v2, v6

    const-string v0, "_id"

    aput-object v0, v2, v7

    .line 276
    .local v2, "cols":[Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->getMetajamId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Artists;->getLockerArtistUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v8, Lcom/google/android/music/ui/AppNavigationMetajamHelper$3;

    invoke-direct {v8, p0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$3;-><init>(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v8}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/music/utils/MusicUtils$QueryCallback;)V

    .line 315
    return-void
.end method

.method private static checkForTrackInLocker(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 9
    .param p0, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "openMetajamItemInfo"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    .param p2, "callback"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 170
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "title"

    aput-object v0, v2, v6

    const-string v0, "album_id"

    aput-object v0, v2, v7

    const/4 v0, 0x2

    const-string v1, "SongId"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 173
    .local v2, "cols":[Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->getMetajamId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$XAudio;->getLockerTrackUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v8, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;

    invoke-direct {v8, p0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;-><init>(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v8}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/music/utils/MusicUtils$QueryCallback;)V

    .line 210
    return-void
.end method

.method public static openMetajamItem(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 4
    .param p0, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "openMetajamItemInfo"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    .param p2, "callback"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    .prologue
    .line 136
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->getMetajamId()Ljava/lang/String;

    move-result-object v0

    .line 137
    .local v0, "metajamId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->isSignUpForced()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->isNautilusEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 138
    sget-object v1, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->LINK:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-static {p0, v1, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnDemandWithMetajamDestination(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    :goto_0
    return-void

    .line 146
    :cond_0
    const-string v1, "T"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 147
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->checkForTrackInLocker(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    goto :goto_0

    .line 148
    :cond_1
    const-string v1, "B"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 149
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->checkForAlbumInLocker(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    goto :goto_0

    .line 150
    :cond_2
    const-string v1, "A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 151
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->checkForArtistInLocker(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    goto :goto_0

    .line 153
    :cond_3
    const-string v1, "MusicNavigationHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Metajam id started with unknown prefix: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->finish()V

    goto :goto_0
.end method

.method private static openMetajamItemFromNautilus(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 4
    .param p0, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "openMetajamItemInfo"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    .param p2, "callback"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    .prologue
    .line 330
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->getMetajamId()Ljava/lang/String;

    move-result-object v0

    .line 331
    .local v0, "metajamId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 332
    const-string v1, "MusicNavigationHelper"

    const-string v2, "Empty metajam id."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->finish()V

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    invoke-static {p0}, Lcom/google/android/music/utils/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 338
    const-string v1, "MusicNavigationHelper"

    const-string v2, "No internet connection."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    if-eqz p2, :cond_2

    .line 340
    invoke-interface {p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;->onConnectionError()V

    goto :goto_0

    .line 342
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->finish()V

    goto :goto_0

    .line 347
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->isNautilusEnabled()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 348
    const-string v1, "T"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 349
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->showNautilusTrack(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    goto :goto_0

    .line 350
    :cond_4
    const-string v1, "B"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 351
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->showNautilusAlbum(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    goto :goto_0

    .line 352
    :cond_5
    const-string v1, "A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 353
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->showNautilusArtist(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    goto :goto_0

    .line 355
    :cond_6
    const-string v1, "MusicNavigationHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Metajam id started with unknown prefix: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->finish()V

    goto :goto_0

    .line 359
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->isSignUpIfNeeded()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 363
    sget-object v1, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->LINK:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-static {p0, v1, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnDemandWithMetajamDestination(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 367
    :cond_8
    const-string v1, "T"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 368
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->openTrackInStore(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    goto :goto_0

    .line 369
    :cond_9
    const-string v1, "B"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 370
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->canSendToStore()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 371
    invoke-static {p0, v0}, Lcom/google/android/music/purchase/Finsky;->startBuyAlbumActivity(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 372
    :cond_a
    if-eqz p2, :cond_b

    .line 373
    invoke-interface {p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;->onAlbumError()V

    goto/16 :goto_0

    .line 375
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->finish()V

    goto/16 :goto_0

    .line 377
    :cond_c
    const-string v1, "A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 378
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->canSendToStore()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 379
    invoke-static {p0, v0}, Lcom/google/android/music/purchase/Finsky;->startBuyArtistActivity(Landroid/content/Context;Ljava/lang/String;)V

    .line 380
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->finish()V

    goto/16 :goto_0

    .line 381
    :cond_d
    if-eqz p2, :cond_e

    .line 382
    invoke-interface {p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;->onArtistError()V

    goto/16 :goto_0

    .line 384
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->finish()V

    goto/16 :goto_0

    .line 387
    :cond_f
    const-string v1, "MusicNavigationHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Metajam id started with unknown prefix: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private static openTrackInStore(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 10
    .param p0, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "openMetajamItemInfo"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    .param p2, "callback"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 627
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->getMetajamId()Ljava/lang/String;

    move-result-object v9

    .line 628
    .local v9, "trackMetajamId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->canSendToStore()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "title"

    aput-object v0, v2, v6

    const-string v0, "StoreAlbumId"

    aput-object v0, v2, v7

    .line 632
    .local v2, "cols":[Ljava/lang/String;
    invoke-static {v9}, Lcom/google/android/music/store/MusicContent$XAudio;->getNautilusAudioUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v8, Lcom/google/android/music/ui/AppNavigationMetajamHelper$7;

    invoke-direct {v8, p0, v9}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$7;-><init>(Lcom/google/android/music/ui/BaseActivity;Ljava/lang/String;)V

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v8}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/music/utils/MusicUtils$QueryCallback;)V

    .line 671
    .end local v2    # "cols":[Ljava/lang/String;
    :goto_0
    return-void

    .line 665
    :cond_0
    if-eqz p2, :cond_1

    .line 666
    invoke-interface {p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;->onTrackError()V

    goto :goto_0

    .line 668
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->finish()V

    goto :goto_0
.end method

.method private static showNautilusAlbum(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 2
    .param p0, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "openMetajamItemInfo"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    .param p2, "callback"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    .prologue
    .line 469
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->getMetajamId()Ljava/lang/String;

    move-result-object v0

    .line 470
    .local v0, "metajamId":Ljava/lang/String;
    new-instance v1, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;

    invoke-direct {v1, p0, v0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$5;-><init>(Lcom/google/android/music/ui/BaseActivity;Ljava/lang/String;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 533
    return-void
.end method

.method private static showNautilusArtist(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 2
    .param p0, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "openMetajamItemInfo"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    .param p2, "callback"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    .prologue
    .line 545
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->getMetajamId()Ljava/lang/String;

    move-result-object v0

    .line 546
    .local v0, "metajamId":Ljava/lang/String;
    new-instance v1, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;

    invoke-direct {v1, p0, v0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;-><init>(Lcom/google/android/music/ui/BaseActivity;Ljava/lang/String;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 612
    return-void
.end method

.method private static showNautilusTrack(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 10
    .param p0, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "openMetajamItemInfo"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    .param p2, "callback"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 402
    invoke-virtual {p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->getMetajamId()Ljava/lang/String;

    move-result-object v9

    .line 403
    .local v9, "metajamId":Ljava/lang/String;
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "title"

    aput-object v0, v2, v6

    const-string v0, "StoreAlbumId"

    aput-object v0, v2, v7

    const/4 v0, 0x2

    const-string v1, "track"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "trackAvailableForSubscription"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "trackAvailableForPurchase"

    aput-object v1, v2, v0

    .line 407
    .local v2, "cols":[Ljava/lang/String;
    invoke-static {v9}, Lcom/google/android/music/store/MusicContent$XAudio;->getNautilusAudioUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v8, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;

    invoke-direct {v8, p0, v9, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;-><init>(Lcom/google/android/music/ui/BaseActivity;Ljava/lang/String;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v8}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/music/utils/MusicUtils$QueryCallback;)V

    .line 457
    return-void
.end method
