.class public abstract Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;
.super Ljava/lang/Object;
.source "CacheUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/cache/CacheUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ArtworkPathResolver"
.end annotation


# instance fields
.field protected mExternalCacheDirectory:Ljava/io/File;

.field protected mInternalCacheDirectory:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public resolveArtworkPath(Ljava/lang/String;I)Ljava/io/File;
    .locals 4
    .param p1, "localPath"    # Ljava/lang/String;
    .param p2, "storageType"    # I

    .prologue
    .line 156
    const/4 v0, 0x0

    .line 157
    .local v0, "parentDir":Ljava/io/File;
    packed-switch p2, :pswitch_data_0

    .line 167
    const-string v1, "CacheUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected storage type value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :goto_0
    :pswitch_0
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 159
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;->mExternalCacheDirectory:Ljava/io/File;

    .line 160
    goto :goto_0

    .line 162
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;->mInternalCacheDirectory:Ljava/io/File;

    .line 163
    goto :goto_0

    .line 170
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_1

    .line 157
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
