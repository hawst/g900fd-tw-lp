.class public interface abstract Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;
.super Ljava/lang/Object;
.source "MediaSessionCompatPreL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/session/MediaSessionCompatPreL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation


# virtual methods
.method public abstract onGetPlaybackPosition()J
.end method

.method public abstract onMetadataUpdate(ILjava/lang/Object;)V
.end method

.method public abstract onPlaybackPositionUpdate(J)V
.end method
