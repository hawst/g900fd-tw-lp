.class Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;
.super Ljava/lang/Object;
.source "StorageProbeUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/cache/StorageProbeUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DiscoveredLocation"
.end annotation


# instance fields
.field final description:Ljava/lang/String;

.field final discoveryMethod:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

.field final isEmulated:Z

.field final isUsable:Z

.field final location:Lcom/google/android/music/download/cache/CacheLocation;

.field final mountPoint:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/cache/CacheLocation;Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;Ljava/io/File;Ljava/lang/String;ZZ)V
    .locals 0
    .param p1, "location"    # Lcom/google/android/music/download/cache/CacheLocation;
    .param p2, "discoveryMethod"    # Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;
    .param p3, "mountPoint"    # Ljava/io/File;
    .param p4, "description"    # Ljava/lang/String;
    .param p5, "isEmulated"    # Z
    .param p6, "isUsable"    # Z

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;->location:Lcom/google/android/music/download/cache/CacheLocation;

    .line 69
    iput-object p2, p0, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;->discoveryMethod:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    .line 70
    iput-object p3, p0, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;->mountPoint:Ljava/io/File;

    .line 71
    iput-object p4, p0, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;->description:Ljava/lang/String;

    .line 72
    iput-boolean p5, p0, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;->isEmulated:Z

    .line 73
    iput-boolean p6, p0, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;->isUsable:Z

    .line 74
    return-void
.end method
