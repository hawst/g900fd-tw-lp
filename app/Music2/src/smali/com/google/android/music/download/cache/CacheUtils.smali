.class public Lcom/google/android/music/download/cache/CacheUtils;
.super Ljava/lang/Object;
.source "CacheUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/cache/CacheUtils$ArtWorkCachePathResolver;,
        Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;,
        Lcom/google/android/music/download/cache/CacheUtils$StorageType;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static final LOGV_CACHE:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/cache/CacheUtils;->LOGV:Z

    .line 43
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CACHE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/google/android/music/download/cache/CacheUtils;->LOGV:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/music/download/cache/CacheUtils;->LOGV_CACHE:Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static cleanUpDirectory(Ljava/io/File;)Z
    .locals 8
    .param p0, "dir"    # Ljava/io/File;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 355
    const/4 v4, 0x1

    .line 356
    .local v4, "successful":Z
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-nez v7, :cond_0

    .line 361
    :goto_0
    return v6

    .line 357
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 358
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz v4, :cond_1

    move v4, v5

    .line 357
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v4, v6

    .line 358
    goto :goto_2

    .line 360
    .end local v1    # "f":Ljava/io/File;
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v7

    if-eqz v7, :cond_3

    if-eqz v4, :cond_3

    move v4, v5

    :goto_3
    move v6, v4

    .line 361
    goto :goto_0

    :cond_3
    move v4, v6

    .line 360
    goto :goto_3
.end method

.method public static getExternalAlbumArtworkCacheDirectory(Landroid/content/Context;)Ljava/io/File;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 236
    const-string v0, "artwork"

    invoke-static {p0, v0}, Lcom/google/android/music/download/cache/CacheUtils;->getExternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static getExternalArtworkCacheDirectory_Old(Landroid/content/Context;)Ljava/io/File;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 409
    const-string v0, "artwork"

    invoke-static {p0, v0}, Lcom/google/android/music/download/cache/CacheUtils;->getExternalCacheDirectory_Old(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static getExternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "subDir"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 314
    invoke-static {}, Lcom/google/android/music/download/cache/CacheUtils;->isExternalStorageMounted()Z

    move-result v3

    if-nez v3, :cond_1

    move-object v1, v2

    .line 331
    :cond_0
    :goto_0
    return-object v1

    .line 321
    :cond_1
    const/4 v1, 0x0

    .line 323
    .local v1, "externalCache":Ljava/io/File;
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p0, v3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 328
    :goto_1
    if-nez v1, :cond_2

    move-object v1, v2

    .line 329
    goto :goto_0

    .line 324
    :catch_0
    move-exception v0

    .line 325
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v3, "CacheUtils"

    const-string v4, "Context.getExternalFilesDir exploded."

    invoke-static {v3, v4, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 331
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_2
    if-eqz p1, :cond_0

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_0
.end method

.method private static getExternalCacheDirectory_Old(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dir"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 438
    invoke-static {}, Lcom/google/android/music/download/cache/CacheUtils;->isExternalStorageMounted()Z

    move-result v2

    if-nez v2, :cond_1

    .line 446
    :cond_0
    :goto_0
    return-object v1

    .line 441
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    .line 443
    .local v0, "externalCache":Ljava/io/File;
    if-eqz v0, :cond_0

    .line 446
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getExternalMusicCacheDirectory(Landroid/content/Context;)Ljava/io/File;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 196
    const-string v0, "music"

    invoke-static {p0, v0}, Lcom/google/android/music/download/cache/CacheUtils;->getExternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static getExternalMusicCacheDirectory_Old(Landroid/content/Context;)Ljava/io/File;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 379
    const-string v0, "music"

    invoke-static {p0, v0}, Lcom/google/android/music/download/cache/CacheUtils;->getExternalCacheDirectory_Old(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static getInternalAlbumArtworkCacheDirectory(Landroid/content/Context;)Ljava/io/File;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 271
    const-string v0, "artwork"

    invoke-static {p0, v0}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static getInternalArtworkCacheDirectory_Old(Landroid/content/Context;)Ljava/io/File;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 424
    const-string v0, "artwork"

    invoke-static {p0, v0}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalCacheDirectory_Old(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static getInternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "subDir"    # Ljava/lang/String;

    .prologue
    .line 289
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 290
    .local v0, "cacheDir":Ljava/io/File;
    if-eqz v0, :cond_1

    .line 291
    if-eqz p1, :cond_0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v0, v1

    .line 295
    .end local v0    # "cacheDir":Ljava/io/File;
    :cond_0
    :goto_0
    return-object v0

    .line 294
    .restart local v0    # "cacheDir":Ljava/io/File;
    :cond_1
    const-string v1, "CacheUtils"

    const-string v2, "Context.getFilesDir() returned null. Check if a bad context was provided."

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getInternalCacheDirectory_Old(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dir"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 429
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 430
    .local v0, "cacheDir":Ljava/io/File;
    if-eqz v0, :cond_0

    .line 431
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 433
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getInternalMusicCacheDirectory(Landroid/content/Context;)Ljava/io/File;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 209
    const-string v0, "music"

    invoke-static {p0, v0}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static getInternalMusicCacheDirectory_Old(Landroid/content/Context;)Ljava/io/File;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 394
    const-string v0, "music"

    invoke-static {p0, v0}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalCacheDirectory_Old(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static getMusicCacheDirectoryById(Landroid/content/Context;Ljava/util/UUID;)Ljava/io/File;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "volumeId"    # Ljava/util/UUID;

    .prologue
    .line 241
    invoke-static {p0}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownLocationByID(Ljava/util/UUID;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v1

    .line 243
    .local v1, "volume":Lcom/google/android/music/download/cache/CacheLocation;
    if-nez v1, :cond_1

    .line 246
    invoke-static {p0}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalMusicCacheDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 250
    .local v0, "result":Ljava/io/File;
    :goto_0
    sget-boolean v2, Lcom/google/android/music/download/cache/CacheUtils;->LOGV_CACHE:Z

    if-eqz v2, :cond_0

    .line 251
    const-string v2, "CacheUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMusicCacheDirectoryById resolved to: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :cond_0
    return-object v0

    .line 248
    .end local v0    # "result":Ljava/io/File;
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v2

    const-string v3, "music"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .restart local v0    # "result":Ljava/io/File;
    goto :goto_0
.end method

.method public static getSchemaValueForStorageType(Lcom/google/android/music/download/cache/CacheUtils$StorageType;)I
    .locals 2
    .param p0, "type"    # Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    .prologue
    .line 454
    const/4 v0, 0x2

    .line 455
    .local v0, "value":I
    sget-object v1, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->INTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    if-ne p0, v1, :cond_1

    .line 456
    const/4 v0, 0x1

    .line 460
    :cond_0
    :goto_0
    return v0

    .line 457
    :cond_1
    sget-object v1, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->SECONDARY_EXTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    if-ne p0, v1, :cond_0

    .line 458
    const/4 v0, 0x3

    goto :goto_0
.end method

.method static getSelectedVolume(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocation;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 335
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 336
    .local v1, "prefObject":Ljava/lang/Object;
    invoke-static {p0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    .line 338
    .local v2, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->getSelectedStorageVolumeId()Ljava/util/UUID;

    move-result-object v3

    .line 339
    .local v3, "volId":Ljava/util/UUID;
    invoke-static {p0}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownLocationByID(Ljava/util/UUID;)Lcom/google/android/music/download/cache/CacheLocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 343
    .local v0, "location":Lcom/google/android/music/download/cache/CacheLocation;
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return-object v0

    .end local v0    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    .end local v3    # "volId":Ljava/util/UUID;
    :catchall_0
    move-exception v4

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v4
.end method

.method public static getSelectedVolumeMusicCacheDirectory(Landroid/content/Context;)Ljava/io/File;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 219
    invoke-static {p0}, Lcom/google/android/music/download/cache/CacheUtils;->getSelectedVolume(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v0

    .line 220
    .local v0, "selected":Lcom/google/android/music/download/cache/CacheLocation;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 221
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v2

    const-string v3, "music"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isExternalStorageMounted()Z
    .locals 3

    .prologue
    .line 66
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "state":Ljava/lang/String;
    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 68
    sget-boolean v1, Lcom/google/android/music/download/cache/CacheUtils;->LOGV:Z

    if-eqz v1, :cond_0

    .line 69
    const-string v1, "CacheUtils"

    const-string v2, "External storage is not mounted"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_0
    const/4 v1, 0x0

    .line 73
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isRemoteLocationSideLoaded(Ljava/lang/String;)Z
    .locals 1
    .param p0, "remoteLocation"    # Ljava/lang/String;

    .prologue
    .line 485
    const-string v0, "mediastore:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isVolumeMounted(Ljava/util/UUID;Landroid/content/Context;)Z
    .locals 1
    .param p0, "volumeId"    # Ljava/util/UUID;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 257
    invoke-static {p1}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownLocationByID(Ljava/util/UUID;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static resolveMusicPath(Landroid/content/Context;Lcom/google/android/music/store/MusicFile;)Ljava/io/File;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "file"    # Lcom/google/android/music/store/MusicFile;

    .prologue
    .line 134
    invoke-virtual {p1}, Lcom/google/android/music/store/MusicFile;->getLocalCopyPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/music/store/MusicFile;->getLocalCopyStorageType()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/music/store/MusicFile;->getLocalCopyStorageVolumeId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->getUUIDFromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/music/download/cache/CacheUtils;->resolveMusicPath(Landroid/content/Context;Ljava/lang/String;ILjava/util/UUID;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static resolveMusicPath(Landroid/content/Context;Ljava/lang/String;ILjava/util/UUID;)Ljava/io/File;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "localCopyPath"    # Ljava/lang/String;
    .param p2, "localCopyStorageType"    # I
    .param p3, "volumeId"    # Ljava/util/UUID;

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-object v1

    .line 97
    :cond_1
    const/4 v0, 0x0

    .line 98
    .local v0, "parentDir":Ljava/io/File;
    if-eqz p3, :cond_3

    .line 99
    invoke-static {p0, p3}, Lcom/google/android/music/download/cache/CacheUtils;->getMusicCacheDirectoryById(Landroid/content/Context;Ljava/util/UUID;)Ljava/io/File;

    move-result-object v0

    .line 118
    :goto_1
    :pswitch_0
    if-eqz v0, :cond_0

    .line 121
    sget-boolean v1, Lcom/google/android/music/download/cache/CacheUtils;->LOGV_CACHE:Z

    if-eqz v1, :cond_2

    .line 122
    const-string v1, "CacheUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Path resolved to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_2
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 101
    :cond_3
    packed-switch p2, :pswitch_data_0

    .line 114
    const-string v2, "CacheUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected storage type value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 103
    :pswitch_1
    invoke-static {p0}, Lcom/google/android/music/download/cache/CacheUtils;->getExternalMusicCacheDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 104
    goto :goto_1

    .line 106
    :pswitch_2
    invoke-static {p0}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalMusicCacheDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 107
    goto :goto_1

    .line 109
    :pswitch_3
    invoke-static {p0}, Lcom/google/android/music/download/cache/CacheUtils;->getSelectedVolumeMusicCacheDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 110
    goto :goto_1

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public static setupSecondaryStorageLocation(Lcom/google/android/music/download/cache/CacheLocation;)Z
    .locals 4
    .param p0, "loc"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 471
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v0

    .line 472
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 473
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    .line 474
    const-string v1, "CacheUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Path given already exists and is not a folder: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    const/4 v1, 0x0

    .line 480
    :goto_0
    return v1

    .line 478
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 480
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    goto :goto_0
.end method
