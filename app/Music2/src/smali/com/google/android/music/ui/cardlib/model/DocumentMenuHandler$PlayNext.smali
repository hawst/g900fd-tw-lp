.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;
.source "DocumentMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayNext"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V
    .locals 2
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 902
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 903
    const v0, 0x7f0e0021

    const v1, 0x7f0b023d

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;-><init>(Landroid/content/res/Resources;II)V

    .line 904
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 6

    .prologue
    .line 908
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/Document$Type;->GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v2, v3, :cond_3

    .line 915
    :cond_0
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v2, :cond_2

    .line 916
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v3, v3, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    .line 917
    .local v1, "songList":Lcom/google/android/music/medialist/SongList;
    const/4 v0, -0x1

    .line 918
    .local v0, "fromPlayPos":I
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getQueueItemId()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 919
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getPosition()I

    move-result v0

    .line 921
    :cond_1
    invoke-static {v1, v0}, Lcom/google/android/music/utils/MusicUtils;->playNext(Lcom/google/android/music/medialist/SongList;I)V

    .line 926
    .end local v0    # "fromPlayPos":I
    .end local v1    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_2
    :goto_0
    return-void

    .line 924
    :cond_3
    const-string v2, "DocumentMenuHandler"

    const-string v3, "Unexpected doc type"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
