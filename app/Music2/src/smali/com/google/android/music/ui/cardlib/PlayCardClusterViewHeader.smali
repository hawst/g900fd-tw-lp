.class public Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;
.super Lcom/google/android/music/widgets/ForegroundLinearLayout;
.source "PlayCardClusterViewHeader.java"


# instance fields
.field private mMoreView:Landroid/widget/TextView;

.field private mTitleMain:Landroid/widget/TextView;

.field private mTitleSecondary:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/widgets/ForegroundLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 1

    .prologue
    .line 83
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setVisibility(I)V

    .line 84
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Lcom/google/android/music/widgets/ForegroundLinearLayout;->onFinishInflate()V

    .line 42
    const v0, 0x7f0e01ee

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->mTitleMain:Landroid/widget/TextView;

    .line 43
    const v0, 0x7f0e01ef

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    .line 44
    const v0, 0x7f0e01f0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    .line 45
    return-void
.end method

.method public setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "titleMain"    # Ljava/lang/String;
    .param p2, "titleSecondary"    # Ljava/lang/String;
    .param p3, "more"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 48
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->mTitleMain:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 57
    :goto_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 63
    :goto_1
    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    invoke-static {p2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {p3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setMoreButtonClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setClickable(Z)V

    .line 68
    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setFocusable(Z)V

    .line 69
    return-void

    :cond_0
    move v0, v2

    .line 67
    goto :goto_0

    :cond_1
    move v1, v2

    .line 68
    goto :goto_1
.end method

.method public show()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setVisibility(I)V

    .line 76
    return-void
.end method
