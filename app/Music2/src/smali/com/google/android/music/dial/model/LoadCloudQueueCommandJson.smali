.class public Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;
.super Lcom/google/android/music/dial/model/GenericDialCommand;
.source "LoadCloudQueueCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;
    }
.end annotation


# instance fields
.field public mBody:Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "body"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/music/dial/model/GenericDialCommand;-><init>()V

    .line 48
    iget-object v0, p0, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "session:1"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mNamespace:Ljava/lang/String;

    .line 49
    iget-object v0, p0, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "loadCloudQueue"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mCommand:Ljava/lang/String;

    .line 51
    new-instance v0, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;->mBody:Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;

    .line 52
    return-void
.end method


# virtual methods
.method protected getBodyAsJsonString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;->mBody:Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;

    invoke-virtual {v0}, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
