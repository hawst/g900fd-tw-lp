.class public Lcom/google/android/music/ui/mrp/MediaRouteManager;
.super Ljava/lang/Object;
.source "MediaRouteManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mMediaRestoreHandler:Landroid/os/Handler;

.field private mMediaRestoreRunnable:Ljava/lang/Runnable;

.field private mMediaRouteButton:Landroid/support/v7/app/MediaRouteButton;

.field private mMediaRouteButtonVisible:Z

.field private mMediaRouteIdToRestore:Ljava/lang/String;

.field private mMediaRouteInvalidatedReceiver:Landroid/content/BroadcastReceiver;

.field private mMediaRouteSelector:Landroid/support/v7/media/MediaRouteSelector;

.field private mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;

.field private mMediaRouter:Landroid/support/v7/media/MediaRouter;

.field private mMediaRouterCallback:Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;

.field private mMediaRouterCallbacksToRegister:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/support/v7/media/MediaRouter$Callback;",
            ">;"
        }
    .end annotation
.end field

.field private final mRouteChecker:Lcom/google/android/music/utils/RouteChecker;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    const-string v0, "MusicCast"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    new-instance v0, Lcom/google/android/music/ui/mrp/MediaRouteManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/mrp/MediaRouteManager$1;-><init>(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    iput-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteInvalidatedReceiver:Landroid/content/BroadcastReceiver;

    .line 197
    iput-object p1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    .line 198
    new-instance v0, Lcom/google/android/music/utils/RouteChecker;

    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/music/utils/RouteChecker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mRouteChecker:Lcom/google/android/music/utils/RouteChecker;

    .line 199
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 42
    sget-boolean v0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->LOGV:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/mrp/MediaRouteManager;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->refreshVisibility()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/mrp/MediaRouteManager;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRestoreRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/mrp/MediaRouteManager;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->restoreMediaRoute()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/mrp/MediaRouteManager;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/music/ui/mrp/MediaRouteManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/mrp/MediaRouteManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/mrp/MediaRouteManager;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/music/ui/mrp/MediaRouteManager;Landroid/support/v7/media/MediaRouter$RouteInfo;)Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/mrp/MediaRouteManager;
    .param p1, "x1"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/support/v7/media/MediaRouter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/mrp/MediaRouteManager;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/mrp/MediaRouteManager;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->getCastAppName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/mrp/MediaRouteManager;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->resetMediaRouteRestoreState()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Lcom/google/android/music/utils/RouteChecker;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/mrp/MediaRouteManager;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mRouteChecker:Lcom/google/android/music/utils/RouteChecker;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/mrp/MediaRouteManager;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/mrp/MediaRouteManager;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/mrp/MediaRouteManager;
    .param p1, "x1"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->setMediaRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    return-void
.end method

.method private getCastAppName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_cast_app_name"

    const-string v2, "GoogleMusic"

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private refreshVisibility()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 309
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteButton:Landroid/support/v7/app/MediaRouteButton;

    if-eqz v4, :cond_1

    .line 310
    iget-boolean v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteButtonVisible:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v4, :cond_2

    move v0, v2

    .line 311
    .local v0, "isVisible":Z
    :goto_0
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v4, :cond_0

    .line 312
    if-eqz v0, :cond_3

    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteSelector:Landroid/support/v7/media/MediaRouteSelector;

    invoke-virtual {v4, v5, v2}, Landroid/support/v7/media/MediaRouter;->isRouteAvailable(Landroid/support/v7/media/MediaRouteSelector;I)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v2

    .line 315
    :cond_0
    :goto_1
    if-eqz v0, :cond_4

    move v1, v3

    .line 316
    .local v1, "visibility":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteButton:Landroid/support/v7/app/MediaRouteButton;

    invoke-virtual {v2, v1}, Landroid/support/v7/app/MediaRouteButton;->setVisibility(I)V

    .line 318
    .end local v0    # "isVisible":Z
    .end local v1    # "visibility":I
    :cond_1
    return-void

    :cond_2
    move v0, v3

    .line 310
    goto :goto_0

    .restart local v0    # "isVisible":Z
    :cond_3
    move v0, v3

    .line 312
    goto :goto_1

    .line 315
    :cond_4
    const/16 v1, 0x8

    goto :goto_2
.end method

.method private resetMediaRouteRestoreState()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 425
    iput-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;

    .line 426
    iput-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 427
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRestoreHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRestoreHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 429
    iput-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRestoreHandler:Landroid/os/Handler;

    .line 431
    :cond_0
    return-void
.end method

.method private restoreMediaRoute()V
    .locals 10

    .prologue
    .line 350
    invoke-direct {p0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->resetMediaRouteRestoreState()V

    .line 352
    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v5}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v4

    .line 354
    .local v4, "selectedRoute":Landroid/support/v7/media/MediaRouter$RouteInfo;
    sget-object v5, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v5, :cond_9

    .line 356
    :try_start_0
    sget-object v5, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v5}, Lcom/google/android/music/playback/IMusicPlaybackService;->getSelectedMediaRouteId()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;

    .line 357
    sget-boolean v5, Lcom/google/android/music/ui/mrp/MediaRouteManager;->LOGV:Z

    if-eqz v5, :cond_0

    const-string v5, "MediaRouteManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Attempting to restore route: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 362
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    invoke-static {v5, v4}, Lcom/google/android/music/cast/CastUtils;->isRemoteControlIntentRoute(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 364
    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v5}, Landroid/support/v7/media/MediaRouter;->getDefaultRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 390
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 392
    :cond_2
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    iput-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRestoreHandler:Landroid/os/Handler;

    .line 393
    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRestoreHandler:Landroid/os/Handler;

    new-instance v6, Lcom/google/android/music/ui/mrp/MediaRouteManager$3;

    invoke-direct {v6, p0}, Lcom/google/android/music/ui/mrp/MediaRouteManager$3;-><init>(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    const-wide/16 v8, 0x1388

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 411
    :cond_3
    sget-boolean v5, Lcom/google/android/music/ui/mrp/MediaRouteManager;->LOGV:Z

    if-eqz v5, :cond_4

    .line 412
    const-string v5, "MediaRouteManager"

    const-string v6, "Registering media route callback"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    :cond_4
    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouterCallbacksToRegister:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/MediaRouter$Callback;

    .line 416
    .local v0, "callback":Landroid/support/v7/media/MediaRouter$Callback;
    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    iget-object v6, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteSelector:Landroid/support/v7/media/MediaRouteSelector;

    const/4 v7, 0x4

    invoke-virtual {v5, v6, v0, v7}, Landroid/support/v7/media/MediaRouter;->addCallback(Landroid/support/v7/media/MediaRouteSelector;Landroid/support/v7/media/MediaRouter$Callback;I)V

    goto :goto_2

    .line 358
    .end local v0    # "callback":Landroid/support/v7/media/MediaRouter$Callback;
    .end local v1    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 359
    .local v2, "re":Landroid/os/RemoteException;
    const-string v5, "MediaRouteManager"

    const-string v6, "Faied to communicate with playback service"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 366
    .end local v2    # "re":Landroid/os/RemoteException;
    :cond_5
    if-eqz v4, :cond_6

    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;

    invoke-virtual {v4}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 370
    invoke-direct {p0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->resetMediaRouteRestoreState()V

    goto :goto_1

    .line 372
    :cond_6
    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v5}, Landroid/support/v7/media/MediaRouter;->getRoutes()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 373
    .local v3, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;

    invoke-virtual {v3}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 375
    const-string v5, "MediaRouteManager"

    const-string v6, "Found selected route. Restoring."

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    iput-object v3, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 377
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;

    .line 381
    .end local v3    # "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    :cond_8
    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-nez v5, :cond_1

    .line 382
    sget-boolean v5, Lcom/google/android/music/ui/mrp/MediaRouteManager;->LOGV:Z

    if-eqz v5, :cond_1

    const-string v5, "MediaRouteManager"

    const-string v6, "Selected route is not found yet"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 387
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_9
    const-string v5, "MediaRouteManager"

    const-string v6, "onStart -- service not yet bound."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 419
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_a
    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-eqz v5, :cond_b

    .line 420
    iget-object v5, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    iget-object v6, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v5, v6}, Landroid/support/v7/media/MediaRouter;->selectRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 422
    :cond_b
    return-void
.end method

.method private setMediaRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 4
    .param p1, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 435
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_1

    .line 436
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/google/android/music/cast/CastUtils;->isRemoteControlIntentRoute(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 437
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/music/playback/IMusicPlaybackService;->setMediaRoute(ZLjava/lang/String;)V

    .line 448
    :goto_0
    return-void

    .line 439
    :cond_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/music/playback/IMusicPlaybackService;->setMediaRoute(ZLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 445
    :catch_0
    move-exception v0

    .line 446
    .local v0, "re":Landroid/os/RemoteException;
    const-string v1, "MediaRouteManager"

    const-string v2, "Unable to switch route."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 443
    .end local v0    # "re":Landroid/os/RemoteException;
    :cond_1
    :try_start_1
    const-string v1, "MediaRouteManager"

    const-string v2, "Unable to switch routes -- service not yet bound."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public addMediaRouterCallback(Landroid/support/v7/media/MediaRouter$Callback;)V
    .locals 1
    .param p1, "callback"    # Landroid/support/v7/media/MediaRouter$Callback;

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouterCallbacksToRegister:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 458
    return-void
.end method

.method public bindMediaRouteButton(Landroid/support/v7/app/MediaRouteButton;)V
    .locals 1
    .param p1, "mediaRouteButton"    # Landroid/support/v7/app/MediaRouteButton;

    .prologue
    .line 293
    iput-object p1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteButton:Landroid/support/v7/app/MediaRouteButton;

    .line 294
    new-instance v0, Lcom/google/android/music/ui/mrp/MusicMediaRouteDialogFactory;

    invoke-direct {v0}, Lcom/google/android/music/ui/mrp/MusicMediaRouteDialogFactory;-><init>()V

    invoke-virtual {p1, v0}, Landroid/support/v7/app/MediaRouteButton;->setDialogFactory(Landroid/support/v7/app/MediaRouteDialogFactory;)V

    .line 295
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteSelector:Landroid/support/v7/media/MediaRouteSelector;

    invoke-virtual {p1, v0}, Landroid/support/v7/app/MediaRouteButton;->setRouteSelector(Landroid/support/v7/media/MediaRouteSelector;)V

    .line 296
    invoke-direct {p0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->refreshVisibility()V

    .line 297
    return-void
.end method

.method public isRemoteRouteSelected()Z
    .locals 2

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/cast/CastUtils;->isRemoteControlIntentRoute(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 202
    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v1, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 203
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/cast/CastUtils;->getCastV2Selector(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "castV2Selector":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v7/media/MediaRouter;->getInstance(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    .line 205
    new-instance v1, Landroid/support/v7/media/MediaRouteSelector$Builder;

    invoke-direct {v1}, Landroid/support/v7/media/MediaRouteSelector$Builder;-><init>()V

    const-string v2, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v1, v2}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addControlCategory(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    move-result-object v1

    const-string v2, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v1, v2}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addControlCategory(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    move-result-object v1

    const-string v2, "com.google.cast.CATEGORY_CAST"

    invoke-virtual {v1, v2}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addControlCategory(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "com.google.cast.CATEGORY_CAST_APP_NAME:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->getCastAppName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addControlCategory(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addControlCategory(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouteSelector$Builder;->build()Landroid/support/v7/media/MediaRouteSelector;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteSelector:Landroid/support/v7/media/MediaRouteSelector;

    .line 213
    new-instance v1, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;-><init>(Lcom/google/android/music/ui/mrp/MediaRouteManager;Lcom/google/android/music/ui/mrp/MediaRouteManager$1;)V

    iput-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouterCallback:Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;

    .line 214
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouterCallbacksToRegister:Ljava/util/Set;

    .line 215
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouterCallbacksToRegister:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouterCallback:Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 216
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 280
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v2, :cond_0

    .line 281
    const v2, 0x7f0e01da

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 282
    .local v1, "mediaRouteMenuItem":Landroid/view/MenuItem;
    invoke-static {v1}, Landroid/support/v4/view/MenuItemCompat;->getActionProvider(Landroid/view/MenuItem;)Landroid/support/v4/view/ActionProvider;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/MediaRouteActionProvider;

    .line 284
    .local v0, "mediaRouteActionProvider":Landroid/support/v7/app/MediaRouteActionProvider;
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteSelector:Landroid/support/v7/media/MediaRouteSelector;

    invoke-virtual {v0, v2}, Landroid/support/v7/app/MediaRouteActionProvider;->setRouteSelector(Landroid/support/v7/media/MediaRouteSelector;)V

    .line 286
    .end local v0    # "mediaRouteActionProvider":Landroid/support/v7/app/MediaRouteActionProvider;
    .end local v1    # "mediaRouteMenuItem":Landroid/view/MenuItem;
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 274
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 275
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mRouteChecker:Lcom/google/android/music/utils/RouteChecker;

    invoke-virtual {v0}, Lcom/google/android/music/utils/RouteChecker;->onDestroy()V

    .line 276
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 321
    packed-switch p1, :pswitch_data_0

    .line 339
    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 323
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v2, :cond_1

    .line 324
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    .line 325
    .local v0, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/google/android/music/cast/CastUtils;->isRemoteControlIntentRoute(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 326
    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->requestUpdateVolume(I)V

    goto :goto_0

    .line 331
    .end local v0    # "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    :cond_1
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v2, :cond_0

    .line 332
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    .line 333
    .restart local v0    # "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/google/android/music/cast/CastUtils;->isRemoteControlIntentRoute(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 334
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->requestUpdateVolume(I)V

    goto :goto_0

    .line 321
    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 251
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 252
    return-void
.end method

.method public onRestart()V
    .locals 1

    .prologue
    .line 243
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 244
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 247
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 248
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 219
    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v1, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 221
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouterCallbacksToRegister:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouterCallback:Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 222
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v1, :cond_1

    .line 223
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRestoreRunnable:Ljava/lang/Runnable;

    if-nez v1, :cond_0

    .line 224
    new-instance v1, Lcom/google/android/music/ui/mrp/MediaRouteManager$2;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/mrp/MediaRouteManager$2;-><init>(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    iput-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRestoreRunnable:Ljava/lang/Runnable;

    .line 232
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 233
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.android.music.mediarouteinvalidated"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 234
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteInvalidatedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 236
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRestoreRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/UIStateManager;->addRunOnPlaybackServiceConnected(Ljava/lang/Runnable;)V

    .line 240
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    .line 255
    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v2, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 257
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v2, :cond_2

    .line 258
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouterCallbacksToRegister:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/MediaRouter$Callback;

    .line 259
    .local v0, "callback":Landroid/support/v7/media/MediaRouter$Callback;
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v2, v0}, Landroid/support/v7/media/MediaRouter;->removeCallback(Landroid/support/v7/media/MediaRouter$Callback;)V

    goto :goto_0

    .line 261
    .end local v0    # "callback":Landroid/support/v7/media/MediaRouter$Callback;
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouterCallbacksToRegister:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 262
    sget-boolean v2, Lcom/google/android/music/ui/mrp/MediaRouteManager;->LOGV:Z

    if-eqz v2, :cond_1

    const-string v2, "MediaRouteManager"

    const-string v3, "Unregistered media route callback"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteInvalidatedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 266
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRestoreRunnable:Ljava/lang/Runnable;

    if-eqz v2, :cond_3

    .line 267
    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRestoreRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/google/android/music/ui/UIStateManager;->removeRunOnPlaybackServiceConnected(Ljava/lang/Runnable;)V

    .line 269
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRestoreRunnable:Ljava/lang/Runnable;

    .line 271
    :cond_3
    return-void
.end method

.method public setMediaRouteButtonVisibility(Z)V
    .locals 0
    .param p1, "isVisible"    # Z

    .prologue
    .line 300
    iput-boolean p1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteButtonVisible:Z

    .line 301
    invoke-direct {p0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->refreshVisibility()V

    .line 302
    return-void
.end method
