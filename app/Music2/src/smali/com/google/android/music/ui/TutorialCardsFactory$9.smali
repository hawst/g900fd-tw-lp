.class final Lcom/google/android/music/ui/TutorialCardsFactory$9;
.super Ljava/lang/Object;
.source "TutorialCardsFactory.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/TutorialCardsFactory;->createActionsClickListener(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$dismissHandler:Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

.field final synthetic val$fragment:Lcom/google/android/music/ui/MusicFragment;

.field final synthetic val$header:Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;

.field final synthetic val$prefKey:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V
    .locals 0

    .prologue
    .line 709
    iput-object p1, p0, Lcom/google/android/music/ui/TutorialCardsFactory$9;->val$fragment:Lcom/google/android/music/ui/MusicFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/TutorialCardsFactory$9;->val$header:Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;

    iput-object p3, p0, Lcom/google/android/music/ui/TutorialCardsFactory$9;->val$prefKey:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/music/ui/TutorialCardsFactory$9;->val$dismissHandler:Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 713
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-wide/32 v2, 0x7f0b028b

    cmp-long v1, p4, v2

    if-nez v1, :cond_1

    .line 714
    iget-object v1, p0, Lcom/google/android/music/ui/TutorialCardsFactory$9;->val$fragment:Lcom/google/android/music/ui/MusicFragment;

    iget-object v2, p0, Lcom/google/android/music/ui/TutorialCardsFactory$9;->val$header:Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;

    iget-object v3, p0, Lcom/google/android/music/ui/TutorialCardsFactory$9;->val$prefKey:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/music/ui/TutorialCardsFactory$9;->val$dismissHandler:Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    # invokes: Lcom/google/android/music/ui/TutorialCardsFactory;->dismissTutorialCard(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/music/ui/TutorialCardsFactory;->access$100(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    .line 723
    :cond_0
    :goto_0
    return-void

    .line 715
    :cond_1
    const-wide/32 v2, 0x7f0b0289

    cmp-long v1, p4, v2

    if-eqz v1, :cond_2

    const-wide/32 v2, 0x7f0b028a

    cmp-long v1, p4, v2

    if-nez v1, :cond_3

    .line 716
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/TutorialCardsFactory$9;->val$fragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v1}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 717
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/ui/SearchActivity;->showSearch(Landroid/content/Context;)V

    goto :goto_0

    .line 718
    .end local v0    # "context":Landroid/content/Context;
    :cond_3
    const-wide/32 v2, 0x7f0b028c

    cmp-long v1, p4, v2

    if-nez v1, :cond_0

    .line 719
    iget-object v1, p0, Lcom/google/android/music/ui/TutorialCardsFactory$9;->val$fragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v1}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->MAINSTAGE:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-static {v1, v2}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnDemand(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)Z

    .line 721
    iget-object v1, p0, Lcom/google/android/music/ui/TutorialCardsFactory$9;->val$fragment:Lcom/google/android/music/ui/MusicFragment;

    iget-object v2, p0, Lcom/google/android/music/ui/TutorialCardsFactory$9;->val$header:Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;

    iget-object v3, p0, Lcom/google/android/music/ui/TutorialCardsFactory$9;->val$prefKey:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/music/ui/TutorialCardsFactory$9;->val$dismissHandler:Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    # invokes: Lcom/google/android/music/ui/TutorialCardsFactory;->dismissTutorialCard(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/music/ui/TutorialCardsFactory;->access$100(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    goto :goto_0
.end method
