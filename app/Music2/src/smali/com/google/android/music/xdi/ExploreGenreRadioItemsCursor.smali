.class Lcom/google/android/music/xdi/ExploreGenreRadioItemsCursor;
.super Landroid/database/MatrixCursor;
.source "ExploreGenreRadioItemsCursor.java"


# instance fields
.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "genreId"    # Ljava/lang/String;
    .param p4, "genreName"    # Ljava/lang/String;
    .param p5, "parentGenreId"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 27
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreRadioItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 28
    invoke-direct {p0, p1, p3, p4, p5}, Lcom/google/android/music/xdi/ExploreGenreRadioItemsCursor;->addStartGenreRadio(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method private addStartGenreRadio(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "genreId"    # Ljava/lang/String;
    .param p3, "genreName"    # Ljava/lang/String;
    .param p4, "parentGenreId"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 33
    iget-object v2, p0, Lcom/google/android/music/xdi/ExploreGenreRadioItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v2}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v2

    new-array v1, v2, [Ljava/lang/Object;

    .line 34
    .local v1, "values":[Ljava/lang/Object;
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "container"

    const/16 v4, 0x16

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "name"

    invoke-virtual {v2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "id_string"

    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "art_uri"

    invoke-static {p1, p2, p4}, Lcom/google/android/music/xdi/XdiUtils;->getGenreArtUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 42
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/android/music/xdi/ExploreGenreRadioItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "_id"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 43
    iget-object v2, p0, Lcom/google/android/music/xdi/ExploreGenreRadioItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_name"

    invoke-virtual {v2, v1, v3, p3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 44
    iget-object v2, p0, Lcom/google/android/music/xdi/ExploreGenreRadioItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_description"

    invoke-virtual {v2, v1, v3, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 45
    iget-object v2, p0, Lcom/google/android/music/xdi/ExploreGenreRadioItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "image_uri"

    const v4, 0x7f02012f

    invoke-static {p1, v4}, Lcom/google/android/music/xdi/UriUtils;->getAndroidResourceUri(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 47
    iget-object v2, p0, Lcom/google/android/music/xdi/ExploreGenreRadioItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "width"

    invoke-virtual {v2, v1, v3, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 48
    iget-object v2, p0, Lcom/google/android/music/xdi/ExploreGenreRadioItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "height"

    invoke-virtual {v2, v1, v3, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 49
    iget-object v2, p0, Lcom/google/android/music/xdi/ExploreGenreRadioItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "intent_uri"

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 51
    invoke-virtual {p0, v1}, Lcom/google/android/music/xdi/ExploreGenreRadioItemsCursor;->addRow([Ljava/lang/Object;)V

    .line 52
    return-void
.end method
