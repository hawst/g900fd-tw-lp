.class Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;
.super Ljava/lang/Object;
.source "CastRemotePlaybackClient.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectionCallbacks"
.end annotation


# instance fields
.field private final mAppId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;


# direct methods
.method private constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Ljava/lang/String;)V
    .locals 0
    .param p2, "appId"    # Ljava/lang/String;

    .prologue
    .line 657
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 658
    iput-object p2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->mAppId:Ljava/lang/String;

    .line 659
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Ljava/lang/String;Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;

    .prologue
    .line 653
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    .prologue
    .line 653
    invoke-direct {p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->createMessageCallbacks()V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;Lcom/google/android/gms/cast/MediaStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;
    .param p1, "x1"    # Lcom/google/android/gms/cast/MediaStatus;

    .prologue
    .line 653
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->handleStatusUpdate(Lcom/google/android/gms/cast/MediaStatus;)V

    return-void
.end method

.method private createMessageCallbacks()V
    .locals 5

    .prologue
    .line 771
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    new-instance v2, Lcom/google/android/gms/cast/RemoteMediaPlayer;

    invoke-direct {v2}, Lcom/google/android/gms/cast/RemoteMediaPlayer;-><init>()V

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;
    invoke-static {v1, v2}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer;

    .line 773
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1800(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/gms/cast/RemoteMediaPlayer;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2;

    invoke-direct {v2, p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->setOnStatusUpdatedListener(Lcom/google/android/gms/cast/RemoteMediaPlayer$OnStatusUpdatedListener;)V

    .line 796
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1800(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/gms/cast/RemoteMediaPlayer;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$3;

    invoke-direct {v2, p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$3;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->setOnMetadataUpdatedListener(Lcom/google/android/gms/cast/RemoteMediaPlayer$OnMetadataUpdatedListener;)V

    .line 821
    :try_start_0
    sget-object v1, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;
    invoke-static {v2}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1200(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;
    invoke-static {v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1800(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/gms/cast/RemoteMediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->getNamespace()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;
    invoke-static {v4}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1800(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/gms/cast/RemoteMediaPlayer;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/gms/cast/Cast$CastApi;->setMessageReceivedCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Lcom/google/android/gms/cast/Cast$MessageReceivedCallback;)V

    .line 826
    sget-object v1, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;
    invoke-static {v2}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1200(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mCustomMessageCallback:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;
    invoke-static {v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1700(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->getNamespace()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mCustomMessageCallback:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;
    invoke-static {v4}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1700(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/gms/cast/Cast$CastApi;->setMessageReceivedCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Lcom/google/android/gms/cast/Cast$MessageReceivedCallback;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 833
    :goto_0
    return-void

    .line 830
    :catch_0
    move-exception v0

    .line 831
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "MusicCastRemote"

    const-string v2, "Exception while creating media channel"

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private handleStatusUpdate(Lcom/google/android/gms/cast/MediaStatus;)V
    .locals 5
    .param p1, "mediaStatus"    # Lcom/google/android/gms/cast/MediaStatus;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 836
    invoke-virtual {p1}, Lcom/google/android/gms/cast/MediaStatus;->getCustomData()Lorg/json/JSONObject;

    move-result-object v0

    .line 838
    .local v0, "customData":Lorg/json/JSONObject;
    invoke-virtual {p1}, Lcom/google/android/gms/cast/MediaStatus;->getPlayerState()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 939
    const-string v1, "MusicCastRemote"

    const-string v2, "Invalid MediaStatus playerState"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z
    invoke-static {v1, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 944
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/MediaStatus;->getStreamPosition()J

    move-result-wide v2

    # invokes: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->updatePosition(J)V
    invoke-static {v1, v2, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$600(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;J)V

    .line 945
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/MediaStatus;->getStreamVolume()D

    move-result-wide v2

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mVolume:D
    invoke-static {v1, v2, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$2102(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;D)D

    .line 946
    return-void

    .line 840
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 841
    const-string v1, "MusicCastRemote"

    const-string v2, "PLAYER_STATE_BUFFERING"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsPreparing:Z
    invoke-static {v1, v4}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$902(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 844
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsStreaming:Z
    invoke-static {v1, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$502(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 845
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyPlayStateChanged()V

    goto :goto_0

    .line 849
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 850
    const-string v1, "MusicCastRemote"

    const-string v2, "PLAYER_STATE_IDLE"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsPreparing:Z
    invoke-static {v1, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$902(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 853
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsStreaming:Z
    invoke-static {v1, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$502(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 854
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$700(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/StopWatch;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/playback/StopWatch;->pause()V

    .line 855
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyPlayStateChanged()V

    .line 856
    invoke-virtual {p1}, Lcom/google/android/gms/cast/MediaStatus;->getIdleReason()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 891
    const-string v1, "MusicCastRemote"

    const-string v2, "Invalid MediaStatus idleReason"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z
    invoke-static {v1, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    goto :goto_0

    .line 858
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 859
    const-string v1, "MusicCastRemote"

    const-string v2, "IDLE_REASON_CANCELED"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z
    invoke-static {v1, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    goto/16 :goto_0

    .line 864
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 865
    const-string v1, "MusicCastRemote"

    const-string v2, "IDLE_REASON_ERROR"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z
    invoke-static {v1, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    goto/16 :goto_0

    .line 870
    :pswitch_4
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 871
    const-string v1, "MusicCastRemote"

    const-string v2, "IDLE_REASON_FINISHED"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    :cond_5
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyPlaybackComplete()V

    .line 874
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->reportTrackEnded()V

    .line 875
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z
    invoke-static {v1, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    goto/16 :goto_0

    .line 878
    :pswitch_5
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 879
    const-string v1, "MusicCastRemote"

    const-string v2, "IDLE_REASON_INTERRUPTED"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    :cond_6
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z
    invoke-static {v1, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    goto/16 :goto_0

    .line 884
    :pswitch_6
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 885
    const-string v1, "MusicCastRemote"

    const-string v2, "IDLE_REASON_NONE"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 899
    :pswitch_7
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 900
    const-string v1, "MusicCastRemote"

    const-string v2, "PLAYER_STATE_PAUSED"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 903
    :cond_7
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # invokes: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->updateCurrentItemIdFromCustomData(Lorg/json/JSONObject;)V
    invoke-static {v1, v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$2000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Lorg/json/JSONObject;)V

    .line 905
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsPreparing:Z
    invoke-static {v1, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$902(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 906
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsStreaming:Z
    invoke-static {v1, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$502(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 907
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z
    invoke-static {v1, v4}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 908
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$700(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/StopWatch;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/playback/StopWatch;->pause()V

    .line 909
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->reportTrackPaused()V

    .line 910
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyPlayStateChanged()V

    goto/16 :goto_0

    .line 914
    :pswitch_8
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 915
    const-string v1, "MusicCastRemote"

    const-string v2, "PLAYER_STATE_PLAYING"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    :cond_8
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # invokes: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->updateCurrentItemIdFromCustomData(Lorg/json/JSONObject;)V
    invoke-static {v1, v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$2000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Lorg/json/JSONObject;)V

    .line 920
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsPreparing:Z
    invoke-static {v1, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$902(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 921
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsStreaming:Z
    invoke-static {v1, v4}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$502(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 922
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z
    invoke-static {v1, v4}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 924
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyOpenComplete()V

    .line 925
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->reportTrackPlaying()V

    .line 926
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyPlayStateChanged()V

    .line 928
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$700(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/StopWatch;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/playback/StopWatch;->start()V

    goto/16 :goto_0

    .line 932
    :pswitch_9
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 933
    const-string v1, "MusicCastRemote"

    const-string v2, "PLAYER_STATE_UNKNOWN"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    :cond_9
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z
    invoke-static {v1, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    goto/16 :goto_0

    .line 838
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_1
        :pswitch_8
        :pswitch_7
        :pswitch_0
    .end packed-switch

    .line 856
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_4
        :pswitch_2
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onConnected(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "connectionHint"    # Landroid/os/Bundle;

    .prologue
    .line 663
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 664
    const-string v0, "MusicCastRemote"

    const-string v1, "onConnected"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1200(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    if-nez v0, :cond_1

    .line 755
    :goto_0
    return-void

    .line 671
    :cond_1
    sget-object v0, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1200(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->mAppId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/cast/Cast$CastApi;->launchApplication(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    goto :goto_0
.end method

.method public onConnectionSuspended(I)V
    .locals 2
    .param p1, "cause"    # I

    .prologue
    .line 759
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 760
    const-string v0, "MusicCastRemote"

    const-string v1, "onConnectionSuspended"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyMediaRouteInvalidated()V

    .line 763
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # invokes: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->tearDown()V
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1100(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V

    .line 764
    return-void
.end method
