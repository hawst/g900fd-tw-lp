.class public Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;
.super Lcom/google/api/client/json/GenericJson;
.source "CloudQueueFlatQueueJson.java"


# instance fields
.field public mContainers:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "containers"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/CloudQueueContainerJson;",
            ">;"
        }
    .end annotation
.end field

.field public mItemIdsInShuffledOrder:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "itemIdsInShuffledOrder"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mItemIdsInUnshuffledOrder:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "itemIdsInUnshuffledOrder"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mItems:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/CloudQueueItemJson;",
            ">;"
        }
    .end annotation
.end field

.field public mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "settings"
    .end annotation
.end field

.field public mTracks:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "tracks"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/TrackJson;",
            ">;"
        }
    .end annotation
.end field

.field public mVersion:Ljava/lang/Long;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "version"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
