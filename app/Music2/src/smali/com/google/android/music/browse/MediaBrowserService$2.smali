.class Lcom/google/android/music/browse/MediaBrowserService$2;
.super Ljava/lang/Object;
.source "MediaBrowserService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/browse/MediaBrowserService;->onLoadChildren(Ljava/lang/String;Landroid/service/media/MediaBrowserService$Result;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/browse/MediaBrowserService;

.field final synthetic val$client:Lcom/google/android/music/browse/config/ClientConfiguration;

.field final synthetic val$id:Lcom/google/android/music/browse/BrowsableMediaId;

.field final synthetic val$listResult:Landroid/service/media/MediaBrowserService$Result;


# direct methods
.method constructor <init>(Lcom/google/android/music/browse/MediaBrowserService;Landroid/service/media/MediaBrowserService$Result;Lcom/google/android/music/browse/config/ClientConfiguration;Lcom/google/android/music/browse/BrowsableMediaId;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/music/browse/MediaBrowserService$2;->this$0:Lcom/google/android/music/browse/MediaBrowserService;

    iput-object p2, p0, Lcom/google/android/music/browse/MediaBrowserService$2;->val$listResult:Landroid/service/media/MediaBrowserService$Result;

    iput-object p3, p0, Lcom/google/android/music/browse/MediaBrowserService$2;->val$client:Lcom/google/android/music/browse/config/ClientConfiguration;

    iput-object p4, p0, Lcom/google/android/music/browse/MediaBrowserService$2;->val$id:Lcom/google/android/music/browse/BrowsableMediaId;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/music/browse/MediaBrowserService$2;->val$listResult:Landroid/service/media/MediaBrowserService$Result;

    iget-object v1, p0, Lcom/google/android/music/browse/MediaBrowserService$2;->this$0:Lcom/google/android/music/browse/MediaBrowserService;

    iget-object v2, p0, Lcom/google/android/music/browse/MediaBrowserService$2;->val$client:Lcom/google/android/music/browse/config/ClientConfiguration;

    iget-object v3, p0, Lcom/google/android/music/browse/MediaBrowserService$2;->val$id:Lcom/google/android/music/browse/BrowsableMediaId;

    invoke-virtual {v2, v3}, Lcom/google/android/music/browse/config/ClientConfiguration;->getItemById(Lcom/google/android/music/browse/BrowsableMediaId;)Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/browse/MediaBrowserService;->getMediaItem(Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/service/media/MediaBrowserService$Result;->sendResult(Ljava/lang/Object;)V

    .line 116
    return-void
.end method
