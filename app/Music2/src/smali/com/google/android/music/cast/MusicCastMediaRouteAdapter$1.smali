.class Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;
.super Lcom/google/android/music/playback/IMusicCastMediaRouterCallback$Stub;
.source "MusicCastMediaRouteAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;->this$0:Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;

    invoke-direct {p0}, Lcom/google/android/music/playback/IMusicCastMediaRouterCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onRouteVolumeChanged(Ljava/lang/String;D)V
    .locals 4
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "volume"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v1, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;->this$0:Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;

    # getter for: Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mRouteId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->access$000(Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;)Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "selectedRouteId":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 40
    const-string v1, "MCMediaRouteAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignoring onRouteVolumeChanged for routeId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :goto_0
    return-void

    .line 43
    :cond_0
    new-instance v1, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1$1;

    invoke-direct {v1, p0, p2, p3}, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1$1;-><init>(Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;D)V

    iget-object v2, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;->this$0:Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;

    # getter for: Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->access$200(Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    goto :goto_0
.end method
