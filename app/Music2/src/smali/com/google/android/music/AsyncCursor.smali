.class public Lcom/google/android/music/AsyncCursor;
.super Landroid/database/AbstractCursor;
.source "AsyncCursor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/AsyncCursor$1;,
        Lcom/google/android/music/AsyncCursor$EmptyCursor;,
        Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;,
        Lcom/google/android/music/AsyncCursor$CursorFragmentObserver;,
        Lcom/google/android/music/AsyncCursor$CursorFragment;
    }
.end annotation


# static fields
.field static final LOG:Z

.field static mSqlSerializer:Ljava/lang/Object;

.field private static final sCountProjection:[Ljava/lang/String;

.field private static sHandler:Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;


# instance fields
.field private mAsyncCommandsExecuted:I

.field mContext:Landroid/content/Context;

.field private final mCreatedFromTrace:[Ljava/lang/StackTraceElement;

.field mCurrentRowCursor:Landroid/database/Cursor;

.field mCurrentRowCursorOffset:I

.field mEmptyCursor:Landroid/database/Cursor;

.field mFragments:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/AsyncCursor$CursorFragment;",
            ">;"
        }
    .end annotation
.end field

.field volatile mHaveExactSize:Z

.field private final mIncludeExternal:Z

.field private mIsHighPriority:Z

.field mLastFetchOffset:I

.field mNewSize:I

.field mProjection:[Ljava/lang/String;

.field mSelection:Ljava/lang/String;

.field mSelectionArgs:[Ljava/lang/String;

.field private final mShouldFilter:Z

.field mSize:I

.field mSortOrder:Ljava/lang/String;

.field mUri:Landroid/net/Uri;

.field mUriObserver:Lcom/google/android/music/AsyncCursor$CursorFragmentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "count(*)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/AsyncCursor;->sCountProjection:[Ljava/lang/String;

    .line 29
    new-instance v0, Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;

    invoke-direct {v0}, Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;-><init>()V

    sput-object v0, Lcom/google/android/music/AsyncCursor;->sHandler:Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;

    .line 44
    const-string v0, "AsyncCursor"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/AsyncCursor;->LOG:Z

    .line 55
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/music/AsyncCursor;->mSqlSerializer:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "projection"    # [Ljava/lang/String;
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;
    .param p6, "sortOrder"    # Ljava/lang/String;
    .param p7, "highPriority"    # Z
    .param p8, "shouldFilter"    # Z
    .param p9, "includeExternal"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 154
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/AsyncCursor;->mLastFetchOffset:I

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/AsyncCursor;->mFragments:Ljava/util/ArrayList;

    .line 54
    new-instance v0, Lcom/google/android/music/AsyncCursor$EmptyCursor;

    invoke-direct {v0}, Lcom/google/android/music/AsyncCursor$EmptyCursor;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/AsyncCursor;->mEmptyCursor:Landroid/database/Cursor;

    .line 56
    iput-boolean v1, p0, Lcom/google/android/music/AsyncCursor;->mHaveExactSize:Z

    .line 62
    iput v1, p0, Lcom/google/android/music/AsyncCursor;->mAsyncCommandsExecuted:I

    .line 156
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/AsyncCursor;->mContext:Landroid/content/Context;

    .line 157
    iput-object p2, p0, Lcom/google/android/music/AsyncCursor;->mUri:Landroid/net/Uri;

    .line 159
    iput-object p3, p0, Lcom/google/android/music/AsyncCursor;->mProjection:[Ljava/lang/String;

    .line 160
    iput-object p4, p0, Lcom/google/android/music/AsyncCursor;->mSelection:Ljava/lang/String;

    .line 161
    iput-object p5, p0, Lcom/google/android/music/AsyncCursor;->mSelectionArgs:[Ljava/lang/String;

    .line 162
    iput-object p6, p0, Lcom/google/android/music/AsyncCursor;->mSortOrder:Ljava/lang/String;

    .line 163
    iput v1, p0, Lcom/google/android/music/AsyncCursor;->mNewSize:I

    iput v1, p0, Lcom/google/android/music/AsyncCursor;->mSize:I

    .line 164
    iput-boolean p7, p0, Lcom/google/android/music/AsyncCursor;->mIsHighPriority:Z

    .line 165
    iput-boolean p8, p0, Lcom/google/android/music/AsyncCursor;->mShouldFilter:Z

    .line 166
    iput-boolean p9, p0, Lcom/google/android/music/AsyncCursor;->mIncludeExternal:Z

    .line 168
    sget-boolean v0, Lcom/google/android/music/AsyncCursor;->LOG:Z

    if-eqz v0, :cond_0

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Initialized for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", proj:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Lcom/google/android/music/AsyncCursor;->arrayToStr([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sel: \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', args: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", order: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "high priority: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/AsyncCursor;->printf(Ljava/lang/String;)V

    .line 177
    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/music/AsyncCursor;->sendMessageToHandler(I)V

    .line 180
    new-instance v0, Lcom/google/android/music/AsyncCursor$CursorFragmentObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/AsyncCursor$CursorFragmentObserver;-><init>(Lcom/google/android/music/AsyncCursor;Lcom/google/android/music/AsyncCursor$1;)V

    iput-object v0, p0, Lcom/google/android/music/AsyncCursor;->mUriObserver:Lcom/google/android/music/AsyncCursor$CursorFragmentObserver;

    .line 181
    iget-object v0, p0, Lcom/google/android/music/AsyncCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/AsyncCursor;->mUriObserver:Lcom/google/android/music/AsyncCursor$CursorFragmentObserver;

    invoke-virtual {v0, p2, v2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 183
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/AsyncCursor;->mCreatedFromTrace:[Ljava/lang/StackTraceElement;

    .line 184
    return-void
.end method

.method static synthetic access$000()Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/music/AsyncCursor;->sHandler:Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/AsyncCursor;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AsyncCursor;
    .param p1, "x1"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/music/AsyncCursor;->sendMessageToHandler(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/AsyncCursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AsyncCursor;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/music/AsyncCursor;->doCount()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/AsyncCursor;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AsyncCursor;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/AsyncCursor;->doFetch(II)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/AsyncCursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AsyncCursor;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/music/AsyncCursor;->doFreshen()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/AsyncCursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AsyncCursor;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/music/AsyncCursor;->doClose()V

    return-void
.end method

.method private static arrayToStr([Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "input"    # [Ljava/lang/String;

    .prologue
    .line 139
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 141
    .local v1, "strBuilder":Ljava/lang/StringBuilder;
    if-eqz p0, :cond_1

    .line 142
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 143
    if-lez v0, :cond_0

    .line 144
    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    :cond_0
    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 149
    .end local v0    # "i":I
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private doClose()V
    .locals 5

    .prologue
    .line 439
    monitor-enter p0

    .line 440
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/AsyncCursor;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 441
    .local v2, "len":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 442
    iget-object v3, p0, Lcom/google/android/music/AsyncCursor;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncCursor$CursorFragment;

    .line 443
    .local v0, "cf":Lcom/google/android/music/AsyncCursor$CursorFragment;
    iget-object v3, v0, Lcom/google/android/music/AsyncCursor$CursorFragment;->cursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 441
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 445
    .end local v0    # "cf":Lcom/google/android/music/AsyncCursor$CursorFragment;
    :cond_0
    sget-boolean v3, Lcom/google/android/music/AsyncCursor;->LOG:Z

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "closed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " cursors"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/music/AsyncCursor;->printf(Ljava/lang/String;)V

    .line 446
    :cond_1
    monitor-exit p0

    .line 447
    return-void

    .line 446
    .end local v1    # "i":I
    .end local v2    # "len":I
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private doCount()V
    .locals 12

    .prologue
    .line 240
    sget-object v9, Lcom/google/android/music/AsyncCursor;->mSqlSerializer:Ljava/lang/Object;

    monitor-enter v9

    .line 242
    :try_start_0
    sget-boolean v0, Lcom/google/android/music/AsyncCursor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "getting size"

    invoke-virtual {p0, v0}, Lcom/google/android/music/AsyncCursor;->printf(Ljava/lang/String;)V

    .line 244
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 245
    .local v10, "startTime":J
    iget-object v0, p0, Lcom/google/android/music/AsyncCursor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/AsyncCursor;->mUri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/music/AsyncCursor;->sCountProjection:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/music/AsyncCursor;->mSelection:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/music/AsyncCursor;->mSelectionArgs:[Ljava/lang/String;

    const/4 v5, 0x0

    iget-boolean v6, p0, Lcom/google/android/music/AsyncCursor;->mShouldFilter:Z

    iget-boolean v7, p0, Lcom/google/android/music/AsyncCursor;->mIncludeExternal:Z

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v8

    .line 248
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 250
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/AsyncCursor;->mNewSize:I

    .line 251
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 254
    :cond_1
    if-eqz v8, :cond_2

    .line 255
    :try_start_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 260
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v10    # "startTime":J
    :cond_2
    :goto_0
    :try_start_4
    sget-boolean v0, Lcom/google/android/music/AsyncCursor;->LOG:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "size is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/AsyncCursor;->mNewSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/AsyncCursor;->printf(Ljava/lang/String;)V

    .line 262
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/AsyncCursor;->onChange(Z)V

    .line 263
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 264
    return-void

    .line 251
    .restart local v8    # "c":Landroid/database/Cursor;
    .restart local v10    # "startTime":J
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 254
    :catchall_1
    move-exception v0

    if-eqz v8, :cond_4

    .line 255
    :try_start_7
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
    :try_end_7
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 258
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v10    # "startTime":J
    :catch_0
    move-exception v0

    goto :goto_0

    .line 263
    :catchall_2
    move-exception v0

    :try_start_8
    monitor-exit v9
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v0
.end method

.method private doFetch(II)V
    .locals 17
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 278
    sget-object v16, Lcom/google/android/music/AsyncCursor;->mSqlSerializer:Ljava/lang/Object;

    monitor-enter v16

    .line 279
    :try_start_0
    sget-boolean v2, Lcom/google/android/music/AsyncCursor;->LOG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "starting fetch at offset "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/music/AsyncCursor;->printf(Ljava/lang/String;)V

    .line 280
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/AsyncCursor;->mUri:Landroid/net/Uri;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v2, v0, v1}, Lcom/google/android/music/store/MusicContent;->addLimitParam(Landroid/net/Uri;II)Landroid/net/Uri;

    move-result-object v3

    .line 281
    .local v3, "limituri":Landroid/net/Uri;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 283
    .local v14, "startTime":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/AsyncCursor;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/AsyncCursor;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/AsyncCursor;->mSelection:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/AsyncCursor;->mSelectionArgs:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/music/AsyncCursor;->mSortOrder:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/music/AsyncCursor;->mShouldFilter:Z

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/music/AsyncCursor;->mIncludeExternal:Z

    invoke-static/range {v2 .. v9}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v10

    .line 285
    .local v10, "c":Landroid/database/Cursor;
    if-eqz v10, :cond_9

    .line 286
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v12

    .line 287
    .local v12, "count":I
    if-eqz v12, :cond_5

    .line 288
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 289
    :try_start_1
    invoke-direct/range {p0 .. p1}, Lcom/google/android/music/AsyncCursor;->fragmentForPosition(I)Lcom/google/android/music/AsyncCursor$CursorFragment;

    move-result-object v11

    .line 290
    .local v11, "cf":Lcom/google/android/music/AsyncCursor$CursorFragment;
    if-eqz v11, :cond_3

    .line 291
    iget-boolean v2, v11, Lcom/google/android/music/AsyncCursor$CursorFragment;->fresh:Z

    if-eqz v2, :cond_1

    .line 292
    const-string v2, "possible unnecessary query: fragment was fresh"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/music/AsyncCursor;->printf(Ljava/lang/String;)V

    .line 294
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    iget-object v4, v11, Lcom/google/android/music/AsyncCursor$CursorFragment;->cursor:Landroid/database/Cursor;

    if-ne v2, v4, :cond_2

    .line 295
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/AsyncCursor;->mEmptyCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    .line 297
    :cond_2
    iget-object v2, v11, Lcom/google/android/music/AsyncCursor$CursorFragment;->cursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 298
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/AsyncCursor;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 300
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/AsyncCursor;->mFragments:Ljava/util/ArrayList;

    new-instance v4, Lcom/google/android/music/AsyncCursor$CursorFragment;

    move/from16 v0, p1

    invoke-direct {v4, v0, v10}, Lcom/google/android/music/AsyncCursor$CursorFragment;-><init>(ILandroid/database/Cursor;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 301
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/AsyncCursor;->getPosition()I

    move-result v13

    .line 302
    .local v13, "pos":I
    move/from16 v0, p1

    if-lt v13, v0, :cond_4

    add-int v2, p1, p2

    if-ge v13, v2, :cond_4

    .line 305
    const/4 v2, -0x1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/AsyncCursor;->getPosition()I

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Lcom/google/android/music/AsyncCursor;->onMove(II)Z

    .line 308
    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 310
    .end local v11    # "cf":Lcom/google/android/music/AsyncCursor$CursorFragment;
    .end local v13    # "pos":I
    :cond_5
    :try_start_2
    sget-boolean v2, Lcom/google/android/music/AsyncCursor;->LOG:Z

    if-eqz v2, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fetched "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " rows ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " requested)"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/music/AsyncCursor;->printf(Ljava/lang/String;)V

    .line 315
    :cond_6
    if-nez v12, :cond_8

    .line 319
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/music/AsyncCursor;->mNewSize:I

    .line 322
    add-int/lit8 v2, p1, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/music/AsyncCursor;->moveToPosition(I)Z

    .line 323
    sget-boolean v2, Lcom/google/android/music/AsyncCursor;->LOG:Z

    if-eqz v2, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "new size estimate: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/AsyncCursor;->mNewSize:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/music/AsyncCursor;->printf(Ljava/lang/String;)V

    .line 337
    :cond_7
    :goto_0
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/music/AsyncCursor;->onChange(Z)V

    .line 341
    .end local v12    # "count":I
    :goto_1
    monitor-exit v16
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 342
    return-void

    .line 308
    .restart local v12    # "count":I
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2

    .line 341
    .end local v3    # "limituri":Landroid/net/Uri;
    .end local v10    # "c":Landroid/database/Cursor;
    .end local v12    # "count":I
    .end local v14    # "startTime":J
    :catchall_1
    move-exception v2

    monitor-exit v16
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    .line 324
    .restart local v3    # "limituri":Landroid/net/Uri;
    .restart local v10    # "c":Landroid/database/Cursor;
    .restart local v12    # "count":I
    .restart local v14    # "startTime":J
    :cond_8
    move/from16 v0, p2

    if-ge v12, v0, :cond_7

    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/music/AsyncCursor;->mHaveExactSize:Z

    if-nez v2, :cond_7

    .line 327
    add-int v2, p1, v12

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/music/AsyncCursor;->mNewSize:I

    .line 334
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/music/AsyncCursor;->mHaveExactSize:Z

    .line 335
    sget-boolean v2, Lcom/google/android/music/AsyncCursor;->LOG:Z

    if-eqz v2, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Adjusting size from "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/AsyncCursor;->mSize:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " to "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/AsyncCursor;->mNewSize:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/music/AsyncCursor;->printf(Ljava/lang/String;)V

    goto :goto_0

    .line 339
    .end local v12    # "count":I
    :cond_9
    const-string v2, "Got null Cursor"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/music/AsyncCursor;->printf(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1
.end method

.method private doFreshen()V
    .locals 4

    .prologue
    .line 376
    monitor-enter p0

    .line 377
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/AsyncCursor;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 378
    .local v2, "len":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 379
    iget-object v3, p0, Lcom/google/android/music/AsyncCursor;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncCursor$CursorFragment;

    .line 380
    .local v0, "cf":Lcom/google/android/music/AsyncCursor$CursorFragment;
    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/android/music/AsyncCursor$CursorFragment;->fresh:Z

    .line 378
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 385
    .end local v0    # "cf":Lcom/google/android/music/AsyncCursor$CursorFragment;
    :cond_0
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/google/android/music/AsyncCursor;->sendMessageToHandler(I)V

    .line 387
    monitor-exit p0

    .line 388
    return-void

    .line 387
    .end local v1    # "i":I
    .end local v2    # "len":I
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private fetchMoreIfNeeded(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 609
    div-int/lit8 v1, p1, 0x32

    .line 610
    .local v1, "fragment":I
    rem-int/lit8 v3, p1, 0x32

    .line 611
    .local v3, "offset":I
    invoke-direct {p0, p1}, Lcom/google/android/music/AsyncCursor;->fragmentForPosition(I)Lcom/google/android/music/AsyncCursor$CursorFragment;

    move-result-object v4

    if-nez v4, :cond_1

    .line 612
    mul-int/lit8 v4, v1, 0x32

    invoke-direct {p0, v4}, Lcom/google/android/music/AsyncCursor;->queueFetch(I)V

    .line 631
    :cond_0
    :goto_0
    return-void

    .line 615
    :cond_1
    const/16 v4, 0xf

    if-ge v3, v4, :cond_2

    if-lez v1, :cond_2

    .line 617
    add-int/lit8 v4, v1, -0x1

    mul-int/lit8 v0, v4, 0x32

    .line 618
    .local v0, "fetchpos":I
    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncCursor;->fragmentForPosition(I)Lcom/google/android/music/AsyncCursor$CursorFragment;

    move-result-object v4

    if-nez v4, :cond_2

    .line 619
    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncCursor;->queueFetch(I)V

    goto :goto_0

    .line 623
    .end local v0    # "fetchpos":I
    :cond_2
    iget v4, p0, Lcom/google/android/music/AsyncCursor;->mSize:I

    add-int/lit8 v4, v4, -0x1

    div-int/lit8 v2, v4, 0x32

    .line 624
    .local v2, "lastfragment":I
    const/16 v4, 0x23

    if-le v3, v4, :cond_0

    if-ge v1, v2, :cond_0

    .line 626
    add-int/lit8 v4, v1, 0x1

    mul-int/lit8 v0, v4, 0x32

    .line 627
    .restart local v0    # "fetchpos":I
    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncCursor;->fragmentForPosition(I)Lcom/google/android/music/AsyncCursor$CursorFragment;

    move-result-object v4

    if-nez v4, :cond_0

    .line 628
    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncCursor;->queueFetch(I)V

    goto :goto_0
.end method

.method private fragmentForPosition(I)Lcom/google/android/music/AsyncCursor$CursorFragment;
    .locals 7
    .param p1, "position"    # I

    .prologue
    const/4 v5, 0x0

    .line 589
    iget-object v6, p0, Lcom/google/android/music/AsyncCursor;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 590
    .local v3, "len":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_3

    .line 591
    iget-object v6, p0, Lcom/google/android/music/AsyncCursor;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncCursor$CursorFragment;

    .line 592
    .local v0, "cf":Lcom/google/android/music/AsyncCursor$CursorFragment;
    iget v4, v0, Lcom/google/android/music/AsyncCursor$CursorFragment;->offset:I

    .line 593
    .local v4, "start":I
    iget-object v6, v0, Lcom/google/android/music/AsyncCursor$CursorFragment;->cursor:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-eqz v6, :cond_1

    move-object v0, v5

    .line 604
    .end local v0    # "cf":Lcom/google/android/music/AsyncCursor$CursorFragment;
    .end local v4    # "start":I
    :cond_0
    :goto_1
    return-object v0

    .line 596
    .restart local v0    # "cf":Lcom/google/android/music/AsyncCursor$CursorFragment;
    .restart local v4    # "start":I
    :cond_1
    iget-object v6, v0, Lcom/google/android/music/AsyncCursor$CursorFragment;->cursor:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v6

    add-int v1, v4, v6

    .line 597
    .local v1, "end":I
    if-lt p1, v4, :cond_2

    if-ge p1, v1, :cond_2

    .line 598
    iget-boolean v5, v0, Lcom/google/android/music/AsyncCursor$CursorFragment;->fresh:Z

    if-nez v5, :cond_0

    .line 599
    invoke-direct {p0, v4}, Lcom/google/android/music/AsyncCursor;->queueFetch(I)V

    goto :goto_1

    .line 590
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v0    # "cf":Lcom/google/android/music/AsyncCursor$CursorFragment;
    .end local v1    # "end":I
    .end local v4    # "start":I
    :cond_3
    move-object v0, v5

    .line 604
    goto :goto_1
.end method

.method private queueFetch(I)V
    .locals 3
    .param p1, "offset"    # I

    .prologue
    .line 267
    sget-boolean v0, Lcom/google/android/music/AsyncCursor;->LOG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "queuing fetch at offset "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/AsyncCursor;->printf(Ljava/lang/String;)V

    .line 268
    :cond_0
    iget v0, p0, Lcom/google/android/music/AsyncCursor;->mLastFetchOffset:I

    if-eq p1, v0, :cond_1

    .line 269
    sget-object v0, Lcom/google/android/music/AsyncCursor;->sHandler:Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;

    const/4 v1, 0x2

    const/16 v2, 0x32

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncCursor;->sendMessageToHandler(Landroid/os/Message;)V

    .line 270
    iput p1, p0, Lcom/google/android/music/AsyncCursor;->mLastFetchOffset:I

    .line 272
    :cond_1
    return-void
.end method

.method private sendMessageToHandler(I)V
    .locals 1
    .param p1, "what"    # I

    .prologue
    .line 120
    sget-object v0, Lcom/google/android/music/AsyncCursor;->sHandler:Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncCursor;->sendMessageToHandler(Landroid/os/Message;)V

    .line 121
    return-void
.end method

.method private sendMessageToHandler(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 124
    iput-object p0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 126
    iget v0, p0, Lcom/google/android/music/AsyncCursor;->mAsyncCommandsExecuted:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 127
    iget-boolean v0, p0, Lcom/google/android/music/AsyncCursor;->mIsHighPriority:Z

    if-eqz v0, :cond_0

    .line 128
    sget-object v0, Lcom/google/android/music/AsyncCursor;->sHandler:Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 135
    :goto_0
    iget v0, p0, Lcom/google/android/music/AsyncCursor;->mAsyncCommandsExecuted:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/music/AsyncCursor;->mAsyncCommandsExecuted:I

    .line 136
    return-void

    .line 130
    :cond_0
    sget-object v0, Lcom/google/android/music/AsyncCursor;->sHandler:Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, p1, v2, v3}, Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 133
    :cond_1
    sget-object v0, Lcom/google/android/music/AsyncCursor;->sHandler:Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 432
    sget-boolean v0, Lcom/google/android/music/AsyncCursor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "close"

    invoke-virtual {p0, v0}, Lcom/google/android/music/AsyncCursor;->printf(Ljava/lang/String;)V

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/AsyncCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/AsyncCursor;->mUriObserver:Lcom/google/android/music/AsyncCursor$CursorFragmentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 434
    invoke-super {p0}, Landroid/database/AbstractCursor;->close()V

    .line 435
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncCursor;->sendMessageToHandler(I)V

    .line 436
    return-void
.end method

.method protected finalize()V
    .locals 7

    .prologue
    .line 224
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/AsyncCursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_1

    .line 225
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 226
    .local v4, "sb":Ljava/lang/StringBuilder;
    const-string v5, "AsyncCursor created from:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    iget-object v0, p0, Lcom/google/android/music/AsyncCursor;->mCreatedFromTrace:[Ljava/lang/StackTraceElement;

    .local v0, "arr$":[Ljava/lang/StackTraceElement;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 228
    .local v1, "element":Ljava/lang/StackTraceElement;
    const-string v5, "\n\tat "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 227
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 230
    .end local v1    # "element":Ljava/lang/StackTraceElement;
    :cond_0
    const-string v5, "AsyncCursor"

    const-string v6, "not closed in finalizer, exiting thread"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    const-string v5, "AsyncCursor"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    .end local v0    # "arr$":[Ljava/lang/StackTraceElement;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    invoke-super {p0}, Landroid/database/AbstractCursor;->finalize()V

    .line 236
    return-void

    .line 234
    :catchall_0
    move-exception v5

    invoke-super {p0}, Landroid/database/AbstractCursor;->finalize()V

    throw v5
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/music/AsyncCursor;->mProjection:[Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 457
    monitor-enter p0

    .line 459
    :try_start_0
    iget v0, p0, Lcom/google/android/music/AsyncCursor;->mSize:I

    monitor-exit p0

    return v0

    .line 460
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCountSync()I
    .locals 2

    .prologue
    .line 474
    sget-object v1, Lcom/google/android/music/AsyncCursor;->mSqlSerializer:Ljava/lang/Object;

    monitor-enter v1

    .line 475
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 476
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/music/AsyncCursor;->mHaveExactSize:Z

    if-nez v0, :cond_0

    .line 477
    invoke-direct {p0}, Lcom/google/android/music/AsyncCursor;->doCount()V

    .line 478
    invoke-virtual {p0}, Lcom/google/android/music/AsyncCursor;->requery()Z

    .line 480
    :cond_0
    iget v0, p0, Lcom/google/android/music/AsyncCursor;->mSize:I

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return v0

    .line 481
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 482
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public declared-synchronized getDouble(I)D
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 492
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/AsyncCursor;->checkPosition()V

    .line 493
    iget-object v0, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    .line 492
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getFloat(I)F
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 499
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/AsyncCursor;->checkPosition()V

    .line 500
    iget-object v0, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 499
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getInt(I)I
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 506
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/AsyncCursor;->checkPosition()V

    .line 507
    iget-object v0, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 506
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLong(I)J
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 513
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/AsyncCursor;->checkPosition()V

    .line 514
    iget-object v0, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    .line 513
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getShort(I)S
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 520
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/AsyncCursor;->checkPosition()V

    .line 521
    iget-object v0, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 520
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getString(I)Ljava/lang/String;
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 527
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/AsyncCursor;->checkPosition()V

    .line 528
    iget-object v0, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 527
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public hasCount()Z
    .locals 1

    .prologue
    .line 486
    iget-boolean v0, p0, Lcom/google/android/music/AsyncCursor;->mHaveExactSize:Z

    return v0
.end method

.method public declared-synchronized isNull(I)Z
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 538
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/AsyncCursor;->checkPosition()V

    .line 539
    iget-object v0, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 538
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onMove(II)Z
    .locals 10
    .param p1, "oldPosition"    # I
    .param p2, "newPosition"    # I

    .prologue
    const/16 v9, 0x23

    const/16 v8, 0xf

    const/4 v7, 0x1

    .line 546
    monitor-enter p0

    .line 548
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    iget-object v6, p0, Lcom/google/android/music/AsyncCursor;->mEmptyCursor:Landroid/database/Cursor;

    if-eq v5, v6, :cond_3

    if-ltz p1, :cond_3

    if-ltz p2, :cond_3

    div-int/lit8 v5, p1, 0x32

    div-int/lit8 v6, p2, 0x32

    if-ne v5, v6, :cond_3

    .line 552
    iget-object v5, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    iget v6, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursorOffset:I

    sub-int v6, p2, v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    .line 553
    .local v1, "moved":Z
    rem-int/lit8 v4, p1, 0x32

    .line 554
    .local v4, "oldoffset":I
    rem-int/lit8 v2, p2, 0x32

    .line 555
    .local v2, "newoffset":I
    if-lt v4, v8, :cond_0

    if-lt v2, v8, :cond_1

    :cond_0
    if-gt v4, v9, :cond_2

    if-le v2, v9, :cond_2

    .line 558
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/music/AsyncCursor;->fetchMoreIfNeeded(I)V

    .line 560
    :cond_2
    if-eqz v1, :cond_4

    .line 561
    monitor-exit p0

    .line 585
    .end local v1    # "moved":Z
    .end local v2    # "newoffset":I
    .end local v4    # "oldoffset":I
    :goto_0
    return v7

    .line 567
    :cond_3
    invoke-direct {p0, p2}, Lcom/google/android/music/AsyncCursor;->fragmentForPosition(I)Lcom/google/android/music/AsyncCursor$CursorFragment;

    move-result-object v0

    .line 568
    .local v0, "cf":Lcom/google/android/music/AsyncCursor$CursorFragment;
    if-eqz v0, :cond_4

    .line 569
    iget-object v5, v0, Lcom/google/android/music/AsyncCursor$CursorFragment;->cursor:Landroid/database/Cursor;

    iput-object v5, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    .line 570
    iget v5, v0, Lcom/google/android/music/AsyncCursor$CursorFragment;->offset:I

    iput v5, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursorOffset:I

    .line 571
    iget v5, v0, Lcom/google/android/music/AsyncCursor$CursorFragment;->offset:I

    sub-int v3, p2, v5

    .line 572
    .local v3, "offsetInCursor":I
    iget-object v5, v0, Lcom/google/android/music/AsyncCursor$CursorFragment;->cursor:Landroid/database/Cursor;

    invoke-interface {v5, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    .line 573
    .restart local v1    # "moved":Z
    invoke-direct {p0, p2}, Lcom/google/android/music/AsyncCursor;->fetchMoreIfNeeded(I)V

    .line 574
    if-eqz v1, :cond_4

    .line 575
    monitor-exit p0

    goto :goto_0

    .line 584
    .end local v0    # "cf":Lcom/google/android/music/AsyncCursor$CursorFragment;
    .end local v1    # "moved":Z
    .end local v3    # "offsetInCursor":I
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 580
    :cond_4
    :try_start_1
    iget-object v5, p0, Lcom/google/android/music/AsyncCursor;->mEmptyCursor:Landroid/database/Cursor;

    iput-object v5, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    .line 581
    div-int/lit8 v5, p2, 0x32

    mul-int/lit8 v5, v5, 0x32

    iput v5, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursorOffset:I

    .line 582
    iget-object v5, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    rem-int/lit8 v6, p2, 0x32

    invoke-interface {v5, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 583
    invoke-direct {p0, p2}, Lcom/google/android/music/AsyncCursor;->fetchMoreIfNeeded(I)V

    .line 584
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method printf(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 80
    const-string v0, "AsyncCursor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/music/AsyncCursor;->mUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 346
    invoke-super {p0, p1}, Landroid/database/AbstractCursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 348
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/AsyncCursor;->onChange(Z)V

    .line 349
    return-void
.end method

.method public requery()Z
    .locals 6

    .prologue
    .line 393
    sget-boolean v4, Lcom/google/android/music/AsyncCursor;->LOG:Z

    if-eqz v4, :cond_0

    const-string v4, "requery"

    invoke-virtual {p0, v4}, Lcom/google/android/music/AsyncCursor;->printf(Ljava/lang/String;)V

    .line 394
    :cond_0
    monitor-enter p0

    .line 395
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/AsyncCursor;->getPosition()I

    move-result v3

    .line 396
    .local v3, "pos":I
    invoke-direct {p0, v3}, Lcom/google/android/music/AsyncCursor;->fragmentForPosition(I)Lcom/google/android/music/AsyncCursor$CursorFragment;

    move-result-object v0

    .line 397
    .local v0, "cf":Lcom/google/android/music/AsyncCursor$CursorFragment;
    if-eqz v0, :cond_2

    .line 398
    iget-object v4, v0, Lcom/google/android/music/AsyncCursor$CursorFragment;->cursor:Landroid/database/Cursor;

    iput-object v4, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    .line 399
    iget v4, v0, Lcom/google/android/music/AsyncCursor$CursorFragment;->offset:I

    iput v4, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursorOffset:I

    .line 400
    iget v4, v0, Lcom/google/android/music/AsyncCursor$CursorFragment;->offset:I

    sub-int v1, v3, v4

    .line 401
    .local v1, "offsetInCursor":I
    iget-object v4, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    invoke-interface {v4, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 406
    .end local v1    # "offsetInCursor":I
    :goto_0
    iget v4, p0, Lcom/google/android/music/AsyncCursor;->mSize:I

    iget v5, p0, Lcom/google/android/music/AsyncCursor;->mNewSize:I

    if-eq v4, v5, :cond_1

    .line 407
    iget v2, p0, Lcom/google/android/music/AsyncCursor;->mSize:I

    .line 408
    .local v2, "oldsize":I
    iget v4, p0, Lcom/google/android/music/AsyncCursor;->mNewSize:I

    iput v4, p0, Lcom/google/android/music/AsyncCursor;->mSize:I

    .line 421
    if-lt v3, v2, :cond_1

    .line 422
    iget v4, p0, Lcom/google/android/music/AsyncCursor;->mNewSize:I

    invoke-virtual {p0, v4}, Lcom/google/android/music/AsyncCursor;->moveToPosition(I)Z

    .line 425
    .end local v2    # "oldsize":I
    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/music/AsyncCursor;->mHaveExactSize:Z

    .line 426
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 427
    invoke-super {p0}, Landroid/database/AbstractCursor;->requery()Z

    move-result v4

    return v4

    .line 403
    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/google/android/music/AsyncCursor;->mEmptyCursor:Landroid/database/Cursor;

    iput-object v4, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursor:Landroid/database/Cursor;

    .line 404
    div-int/lit8 v4, v3, 0x32

    mul-int/lit8 v4, v4, 0x32

    iput v4, p0, Lcom/google/android/music/AsyncCursor;->mCurrentRowCursorOffset:I

    goto :goto_0

    .line 426
    .end local v0    # "cf":Lcom/google/android/music/AsyncCursor$CursorFragment;
    .end local v3    # "pos":I
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method
