.class public Lcom/google/android/music/ui/TopChartsExploreFragment;
.super Lcom/google/android/music/ui/ExploreClusterListFragment;
.source "TopChartsExploreFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mListHeaderView:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;-><init>()V

    .line 32
    return-void
.end method

.method private updateCardHeader()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 74
    invoke-virtual {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getGenreId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 75
    invoke-virtual {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    .line 79
    .local v2, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->showStartRadioButtonsInActionBar()Z

    move-result v6

    if-nez v6, :cond_1

    .line 80
    invoke-virtual {p0, v7}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 81
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f040064

    iget-object v7, p0, Lcom/google/android/music/ui/TopChartsExploreFragment;->mListHeaderView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6, v7, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 83
    .local v3, "root":Landroid/view/View;
    const v6, 0x7f0e011e

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 84
    .local v1, "parent":Landroid/view/ViewGroup;
    const v6, 0x7f0e0120

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 85
    .local v4, "textView":Landroid/widget/TextView;
    const v6, 0x7f0b00a9

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    .line 86
    const v6, 0x7f02012f

    invoke-virtual {v4, v6, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f00c5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 90
    invoke-virtual {v1, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    invoke-virtual {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/utils/MusicUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/google/android/music/utils/ViewUtils;->setWidthToShortestEdge(Landroid/content/Context;Landroid/view/ViewGroup;)I

    .line 94
    :cond_0
    iget-object v5, p0, Lcom/google/android/music/ui/TopChartsExploreFragment;->mListHeaderView:Landroid/view/ViewGroup;

    invoke-virtual {v5, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 104
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v1    # "parent":Landroid/view/ViewGroup;
    .end local v2    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v3    # "root":Landroid/view/View;
    .end local v4    # "textView":Landroid/widget/TextView;
    :cond_1
    :goto_0
    return-void

    .line 98
    :cond_2
    iget-object v6, p0, Lcom/google/android/music/ui/TopChartsExploreFragment;->mListHeaderView:Landroid/view/ViewGroup;

    invoke-static {p0, v6, v7}, Lcom/google/android/music/ui/TutorialCardsFactory;->setupExploreTutorial(Lcom/google/android/music/ui/MusicFragment;Landroid/view/ViewGroup;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    .line 101
    iget-object v6, p0, Lcom/google/android/music/ui/TopChartsExploreFragment;->mListHeaderView:Landroid/view/ViewGroup;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/music/ui/TopChartsExploreFragment;->mListHeaderView:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-lez v6, :cond_3

    const/4 v5, 0x1

    :cond_3
    invoke-virtual {p0, v5}, Lcom/google/android/music/ui/TopChartsExploreFragment;->setIsCardHeaderShowing(Z)V

    goto :goto_0
.end method


# virtual methods
.method protected getGroupQueryUri(J)Landroid/net/Uri;
    .locals 1
    .param p1, "groupId"    # J

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getGenreId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/google/android/music/store/MusicContent$Explore;->getTopChartsUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 109
    .local v5, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getGenreId()Ljava/lang/String;

    move-result-object v3

    .line 110
    .local v3, "genreId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "parentNautilusId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 112
    .local v4, "parentGenreId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 114
    .local v2, "appContext":Landroid/content/Context;
    new-instance v0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/ui/TopChartsExploreFragment$1;-><init>(Lcom/google/android/music/ui/TopChartsExploreFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 133
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/google/android/music/ui/ExploreClusterListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-virtual {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getGenreId()Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "genreId":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Explore;->getTopChartGroupsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/TopChartsExploreFragment;->init(Landroid/net/Uri;)V

    .line 44
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->onDestroyView()V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/TopChartsExploreFragment;->mListHeaderView:Landroid/view/ViewGroup;

    .line 60
    return-void
.end method

.method protected onNewNautilusStatus(Lcom/google/android/music/NautilusStatus;)V
    .locals 1
    .param p1, "newStatus"    # Lcom/google/android/music/NautilusStatus;

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/google/android/music/ui/ExploreClusterListFragment;->onNewNautilusStatus(Lcom/google/android/music/NautilusStatus;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/TopChartsExploreFragment;->mListHeaderView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 67
    invoke-direct {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->updateCardHeader()V

    .line 69
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/ExploreClusterListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 51
    .local v0, "listView":Landroid/widget/ListView;
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/music/ui/TopChartsExploreFragment;->mListHeaderView:Landroid/view/ViewGroup;

    .line 52
    iget-object v1, p0, Lcom/google/android/music/ui/TopChartsExploreFragment;->mListHeaderView:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 53
    invoke-direct {p0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->updateCardHeader()V

    .line 54
    return-void
.end method
