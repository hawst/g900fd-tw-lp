.class Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;
.super Lcom/google/android/music/utils/LoggableHandler;
.source "SharedSongsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/activities/SharedSongsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/activities/SharedSongsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/music/activities/SharedSongsActivity;)V
    .locals 1

    .prologue
    .line 178
    iput-object p1, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    .line 179
    const-class v0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    .line 180
    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->checkAccountsMatch()V

    return-void
.end method

.method private checkAccountsMatch()V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 400
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v3}, Lcom/google/android/music/activities/SharedSongsActivity;->access$100(Lcom/google/android/music/activities/SharedSongsActivity;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v2

    .line 401
    .local v2, "musicAccount":Landroid/accounts/Account;
    if-nez v2, :cond_1

    .line 402
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    sget-object v4, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->UNKNOWN:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-static {v3, v4}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnDemand(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 405
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    invoke-virtual {v3}, Lcom/google/android/music/activities/SharedSongsActivity;->finish()V

    .line 430
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    invoke-virtual {v3}, Lcom/google/android/music/activities/SharedSongsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "authAccount"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 410
    .local v1, "esAccount":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 411
    const-string v3, "SharedSongsActivity"

    const-string v4, "G+ did not provide account extra"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    invoke-virtual {v3}, Lcom/google/android/music/activities/SharedSongsActivity;->finish()V

    goto :goto_0

    .line 415
    :cond_2
    iget-object v3, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 416
    .local v0, "accountMatch":Z
    if-nez v0, :cond_3

    .line 417
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mTextView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/music/activities/SharedSongsActivity;->access$500(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    const v5, 0x7f0b020c

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v9

    const/4 v7, 0x1

    iget-object v8, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/android/music/activities/SharedSongsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 419
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mSecondaryButton:Landroid/widget/Button;
    invoke-static {v3}, Lcom/google/android/music/activities/SharedSongsActivity;->access$900(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/widget/Button;

    move-result-object v3

    const v4, 0x7f0b020d

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(I)V

    .line 420
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mSecondaryButton:Landroid/widget/Button;
    invoke-static {v3}, Lcom/google/android/music/activities/SharedSongsActivity;->access$900(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 421
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mTextView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/music/activities/SharedSongsActivity;->access$500(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/widget/TextView;

    move-result-object v3

    const/16 v4, 0x13

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 422
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v3}, Lcom/google/android/music/activities/SharedSongsActivity;->access$600(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/widget/ProgressBar;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 424
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mTextView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/music/activities/SharedSongsActivity;->access$500(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    invoke-virtual {v4}, Lcom/google/android/music/activities/SharedSongsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0213

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 425
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v3}, Lcom/google/android/music/activities/SharedSongsActivity;->access$600(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/widget/ProgressBar;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 426
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mSecondaryButton:Landroid/widget/Button;
    invoke-static {v3}, Lcom/google/android/music/activities/SharedSongsActivity;->access$900(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 427
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mTextView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/music/activities/SharedSongsActivity;->access$500(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/widget/TextView;

    move-result-object v3

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 428
    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mAsyncWorker:Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;
    invoke-static {v3}, Lcom/google/android/music/activities/SharedSongsActivity;->access$000(Lcom/google/android/music/activities/SharedSongsActivity;)Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method

.method private processGetShared()V
    .locals 14

    .prologue
    .line 307
    iget-object v11, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    invoke-virtual {v11}, Lcom/google/android/music/activities/SharedSongsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 309
    .local v1, "appContext":Landroid/content/Context;
    new-instance v2, Lcom/google/android/music/sharedpreview/SharedPreviewClient;

    invoke-direct {v2, v1}, Lcom/google/android/music/sharedpreview/SharedPreviewClient;-><init>(Landroid/content/Context;)V

    .line 310
    .local v2, "client":Lcom/google/android/music/sharedpreview/SharedPreviewClient;
    iget-object v11, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;
    invoke-static {v11}, Lcom/google/android/music/activities/SharedSongsActivity;->access$200(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->getMetaDataResponse(Ljava/lang/String;)Lcom/google/android/music/sharedpreview/JsonResponse;

    move-result-object v5

    .line 311
    .local v5, "response":Lcom/google/android/music/sharedpreview/JsonResponse;
    if-nez v5, :cond_0

    .line 312
    const-string v11, "SharedSongsActivity"

    const-string v12, "Failed to retrieve shared content."

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    const/4 v11, 0x4

    invoke-virtual {p0, v11}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->sendEmptyMessage(I)Z

    .line 382
    :goto_0
    return-void

    .line 316
    :cond_0
    const/4 v4, 0x0

    .line 317
    .local v4, "list":Lcom/google/android/music/medialist/SongList;
    instance-of v11, v5, Lcom/google/android/music/sharedpreview/SharedAlbumResponse;

    if-eqz v11, :cond_2

    move-object v0, v5

    .line 318
    check-cast v0, Lcom/google/android/music/sharedpreview/SharedAlbumResponse;

    .line 320
    .local v0, "albumResponse":Lcom/google/android/music/sharedpreview/SharedAlbumResponse;
    new-instance v8, Lcom/google/android/music/medialist/SongDataList;

    invoke-direct {v8}, Lcom/google/android/music/medialist/SongDataList;-><init>()V

    .line 321
    .local v8, "songs":Lcom/google/android/music/medialist/SongDataList;
    iget-object v11, v0, Lcom/google/android/music/sharedpreview/SharedAlbumResponse;->mTracks:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/music/sharedpreview/Track;

    .line 322
    .local v9, "track":Lcom/google/android/music/sharedpreview/Track;
    new-instance v6, Lcom/google/android/music/medialist/SongData;

    invoke-direct {v6}, Lcom/google/android/music/medialist/SongData;-><init>()V

    .line 324
    .local v6, "song":Lcom/google/android/music/medialist/SongData;
    iget-object v11, v0, Lcom/google/android/music/sharedpreview/SharedAlbumResponse;->mAlbumArtist:Ljava/lang/String;

    iput-object v11, v6, Lcom/google/android/music/medialist/SongData;->mAlbumArtist:Ljava/lang/String;

    .line 325
    iget-object v11, v0, Lcom/google/android/music/sharedpreview/SharedAlbumResponse;->mAlbumArtist:Ljava/lang/String;

    iput-object v11, v6, Lcom/google/android/music/medialist/SongData;->mArtist:Ljava/lang/String;

    .line 326
    iget-object v11, v0, Lcom/google/android/music/sharedpreview/SharedAlbumResponse;->mAlbumTitle:Ljava/lang/String;

    iput-object v11, v6, Lcom/google/android/music/medialist/SongData;->mAlbum:Ljava/lang/String;

    .line 328
    iget-object v11, v9, Lcom/google/android/music/sharedpreview/Track;->mTitle:Ljava/lang/String;

    iput-object v11, v6, Lcom/google/android/music/medialist/SongData;->mTitle:Ljava/lang/String;

    .line 329
    iget v11, v9, Lcom/google/android/music/sharedpreview/Track;->mDurationMsecs:I

    int-to-long v12, v11

    iput-wide v12, v6, Lcom/google/android/music/medialist/SongData;->mDuration:J

    .line 332
    const-wide/16 v12, 0x0

    iput-wide v12, v6, Lcom/google/android/music/medialist/SongData;->mAlbumId:J

    .line 333
    const-wide/16 v12, 0x0

    iput-wide v12, v6, Lcom/google/android/music/medialist/SongData;->mAlbumArtistId:J

    .line 334
    const-string v11, ""

    iput-object v11, v6, Lcom/google/android/music/medialist/SongData;->mArtistSort:Ljava/lang/String;

    .line 335
    const/4 v11, 0x0

    iput v11, v6, Lcom/google/android/music/medialist/SongData;->mHasRemote:I

    .line 336
    const/4 v11, 0x0

    iput v11, v6, Lcom/google/android/music/medialist/SongData;->mHasLocal:I

    .line 337
    const/4 v11, 0x0

    iput v11, v6, Lcom/google/android/music/medialist/SongData;->mRating:I

    .line 338
    const-string v11, ""

    iput-object v11, v6, Lcom/google/android/music/medialist/SongData;->mSourceId:Ljava/lang/String;

    .line 340
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;
    invoke-static {v11}, Lcom/google/android/music/activities/SharedSongsActivity;->access$200(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 341
    .local v10, "urlBuilder":Ljava/lang/StringBuilder;
    const-string v11, "&mode=streaming"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    const-string v11, "&tid="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    iget-object v11, v9, Lcom/google/android/music/sharedpreview/Track;->mId:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/google/android/music/medialist/SongData;->mDomainParam:Ljava/lang/String;

    .line 346
    iget-object v11, v8, Lcom/google/android/music/medialist/SongDataList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 348
    .end local v6    # "song":Lcom/google/android/music/medialist/SongData;
    .end local v9    # "track":Lcom/google/android/music/sharedpreview/Track;
    .end local v10    # "urlBuilder":Ljava/lang/StringBuilder;
    :cond_1
    new-instance v4, Lcom/google/android/music/medialist/SharedAlbumSongList;

    .end local v4    # "list":Lcom/google/android/music/medialist/SongList;
    iget-object v11, v0, Lcom/google/android/music/sharedpreview/SharedAlbumResponse;->mAlbumArtUrl:Ljava/lang/String;

    iget-object v12, v0, Lcom/google/android/music/sharedpreview/SharedAlbumResponse;->mStoreUrl:Ljava/lang/String;

    invoke-direct {v4, v11, v12, v8}, Lcom/google/android/music/medialist/SharedAlbumSongList;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/medialist/SongDataList;)V

    .line 381
    .end local v0    # "albumResponse":Lcom/google/android/music/sharedpreview/SharedAlbumResponse;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v8    # "songs":Lcom/google/android/music/medialist/SongDataList;
    .restart local v4    # "list":Lcom/google/android/music/medialist/SongList;
    :goto_2
    invoke-direct {p0, v4}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->showSongList(Lcom/google/android/music/medialist/SongList;)V

    goto/16 :goto_0

    .line 350
    :cond_2
    instance-of v11, v5, Lcom/google/android/music/sharedpreview/SharedSongResponse;

    if-eqz v11, :cond_3

    move-object v7, v5

    .line 351
    check-cast v7, Lcom/google/android/music/sharedpreview/SharedSongResponse;

    .line 352
    .local v7, "songResponse":Lcom/google/android/music/sharedpreview/SharedSongResponse;
    new-instance v6, Lcom/google/android/music/medialist/SongData;

    invoke-direct {v6}, Lcom/google/android/music/medialist/SongData;-><init>()V

    .line 354
    .restart local v6    # "song":Lcom/google/android/music/medialist/SongData;
    iget-object v11, v7, Lcom/google/android/music/sharedpreview/SharedSongResponse;->mTrackArtist:Ljava/lang/String;

    iput-object v11, v6, Lcom/google/android/music/medialist/SongData;->mArtist:Ljava/lang/String;

    .line 355
    iget-object v11, v7, Lcom/google/android/music/sharedpreview/SharedSongResponse;->mAlbumTitle:Ljava/lang/String;

    iput-object v11, v6, Lcom/google/android/music/medialist/SongData;->mAlbum:Ljava/lang/String;

    .line 356
    iget-object v11, v7, Lcom/google/android/music/sharedpreview/SharedSongResponse;->mTrackTitle:Ljava/lang/String;

    iput-object v11, v6, Lcom/google/android/music/medialist/SongData;->mTitle:Ljava/lang/String;

    .line 357
    iget v11, v7, Lcom/google/android/music/sharedpreview/SharedSongResponse;->mDurationMsecs:I

    int-to-long v12, v11

    iput-wide v12, v6, Lcom/google/android/music/medialist/SongData;->mDuration:J

    .line 358
    iget-object v11, v6, Lcom/google/android/music/medialist/SongData;->mArtist:Ljava/lang/String;

    iput-object v11, v6, Lcom/google/android/music/medialist/SongData;->mAlbumArtist:Ljava/lang/String;

    .line 361
    const-wide/16 v12, 0x0

    iput-wide v12, v6, Lcom/google/android/music/medialist/SongData;->mAlbumId:J

    .line 362
    const-wide/16 v12, 0x0

    iput-wide v12, v6, Lcom/google/android/music/medialist/SongData;->mAlbumArtistId:J

    .line 363
    const-string v11, ""

    iput-object v11, v6, Lcom/google/android/music/medialist/SongData;->mArtistSort:Ljava/lang/String;

    .line 364
    const/4 v11, 0x0

    iput v11, v6, Lcom/google/android/music/medialist/SongData;->mHasRemote:I

    .line 365
    const/4 v11, 0x0

    iput v11, v6, Lcom/google/android/music/medialist/SongData;->mHasLocal:I

    .line 366
    const/4 v11, 0x0

    iput v11, v6, Lcom/google/android/music/medialist/SongData;->mRating:I

    .line 367
    const-string v11, ""

    iput-object v11, v6, Lcom/google/android/music/medialist/SongData;->mSourceId:Ljava/lang/String;

    .line 369
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;
    invoke-static {v11}, Lcom/google/android/music/activities/SharedSongsActivity;->access$200(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 370
    .restart local v10    # "urlBuilder":Ljava/lang/StringBuilder;
    const-string v11, "&mode=streaming"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 371
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/google/android/music/medialist/SongData;->mDomainParam:Ljava/lang/String;

    .line 373
    new-instance v4, Lcom/google/android/music/medialist/SharedSingleSongList;

    .end local v4    # "list":Lcom/google/android/music/medialist/SongList;
    iget-object v11, v7, Lcom/google/android/music/sharedpreview/SharedSongResponse;->mAlbumArtUrl:Ljava/lang/String;

    iget-object v12, v7, Lcom/google/android/music/sharedpreview/SharedSongResponse;->mStoreUrl:Ljava/lang/String;

    invoke-direct {v4, v11, v12, v6}, Lcom/google/android/music/medialist/SharedSingleSongList;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/medialist/SongData;)V

    .line 375
    .restart local v4    # "list":Lcom/google/android/music/medialist/SongList;
    goto :goto_2

    .line 376
    .end local v6    # "song":Lcom/google/android/music/medialist/SongData;
    .end local v7    # "songResponse":Lcom/google/android/music/sharedpreview/SharedSongResponse;
    .end local v10    # "urlBuilder":Ljava/lang/StringBuilder;
    :cond_3
    const-string v11, "SharedSongsActivity"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Unknown song list: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    const/4 v11, 0x4

    invoke-virtual {p0, v11}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method

.method private processGetSharedPlaylist()V
    .locals 19

    .prologue
    .line 237
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;
    invoke-static {v4}, Lcom/google/android/music/activities/SharedSongsActivity;->access$200(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v13

    .line 238
    .local v13, "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x3

    if-ge v4, v5, :cond_1

    .line 239
    const-string v4, "SharedSongsActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid url: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;
    invoke-static {v7}, Lcom/google/android/music/activities/SharedSongsActivity;->access$200(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    const/4 v4, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->sendEmptyMessage(I)Z

    .line 304
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    const/4 v4, 0x2

    invoke-interface {v13, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 244
    .local v6, "shareToken":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->LOGV:Z
    invoke-static {v4}, Lcom/google/android/music/activities/SharedSongsActivity;->access$700(Lcom/google/android/music/activities/SharedSongsActivity;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 245
    const-string v4, "SharedSongsActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "shareToken="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :cond_2
    if-nez v6, :cond_3

    .line 249
    const-string v4, "SharedSongsActivity"

    const-string v5, "Failed to get share token"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    const/4 v4, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 254
    :cond_3
    const/4 v12, 0x0

    .line 255
    .local v12, "hasNautilus":Z
    new-instance v16, Ljava/lang/Object;

    invoke-direct/range {v16 .. v16}, Ljava/lang/Object;-><init>()V

    .line 256
    .local v16, "refObject":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    move-object/from16 v0, v16

    invoke-static {v4, v0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v15

    .line 259
    .local v15, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v15}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v12

    .line 261
    invoke-static/range {v16 .. v16}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 264
    const-string v4, "1"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;
    invoke-static {v5}, Lcom/google/android/music/activities/SharedSongsActivity;->access$200(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/net/Uri;

    move-result-object v5

    const-string v7, "signup"

    invoke-virtual {v5, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    .line 266
    .local v17, "signUpForced":Z
    if-eqz v17, :cond_4

    if-nez v12, :cond_4

    .line 267
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    sget-object v5, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->LINK:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-static {v4, v5, v6}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnDemandWithPlaylistDestination(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 273
    :cond_4
    const/4 v3, 0x0

    .line 275
    .local v3, "songList":Lcom/google/android/music/medialist/SongList;
    const/4 v14, 0x0

    .line 277
    .local v14, "playlistEntity":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mCloudClient:Lcom/google/android/music/cloudclient/MusicCloud;
    invoke-static {v4}, Lcom/google/android/music/activities/SharedSongsActivity;->access$800(Lcom/google/android/music/activities/SharedSongsActivity;)Lcom/google/android/music/cloudclient/MusicCloud;

    move-result-object v4

    invoke-interface {v4, v6}, Lcom/google/android/music/cloudclient/MusicCloud;->getPlaylist(Ljava/lang/String;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v14

    .line 288
    if-nez v14, :cond_5

    .line 289
    const-string v4, "SharedSongsActivity"

    const-string v5, "Failed to get playlist"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    const/4 v4, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 261
    .end local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    .end local v14    # "playlistEntity":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .end local v17    # "signUpForced":Z
    :catchall_0
    move-exception v4

    invoke-static/range {v16 .. v16}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v4

    .line 278
    .restart local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    .restart local v14    # "playlistEntity":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .restart local v17    # "signUpForced":Z
    :catch_0
    move-exception v2

    .line 279
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "SharedSongsActivity"

    const-string v5, "Failed to get shared playlist"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 280
    const/4 v4, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 282
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 283
    .local v2, "e":Ljava/lang/InterruptedException;
    const-string v4, "SharedSongsActivity"

    const-string v5, "Failed to get shared playlist"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 284
    const/4 v4, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 293
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_5
    iget-object v0, v14, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mArtUrls:Ljava/util/List;

    move-object/from16 v18, v0

    .line 294
    .local v18, "urls":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/TrackJson$ImageRef;>;"
    const/4 v10, 0x0

    .line 295
    .local v10, "artUrl":Ljava/lang/String;
    if-eqz v18, :cond_6

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    .line 296
    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;

    iget-object v10, v4, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;->mUrl:Ljava/lang/String;

    .line 298
    :cond_6
    new-instance v3, Lcom/google/android/music/medialist/SharedWithMeSongList;

    .end local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    const-wide/16 v4, -0x1

    iget-object v7, v14, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mName:Ljava/lang/String;

    iget-object v8, v14, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mDescription:Ljava/lang/String;

    iget-object v9, v14, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mOwnerName:Ljava/lang/String;

    iget-object v11, v14, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    invoke-direct/range {v3 .. v11}, Lcom/google/android/music/medialist/SharedWithMeSongList;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    .restart local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->showSongList(Lcom/google/android/music/medialist/SongList;)V

    goto/16 :goto_0
.end method

.method private showSongList(Lcom/google/android/music/medialist/SongList;)V
    .locals 2
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    new-instance v1, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker$3;-><init>(Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;Lcom/google/android/music/medialist/SongList;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/activities/SharedSongsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 397
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 184
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 234
    :goto_0
    return-void

    .line 186
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v1}, Lcom/google/android/music/activities/SharedSongsActivity;->access$100(Lcom/google/android/music/activities/SharedSongsActivity;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getStoreAvailableLastChecked()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 187
    iget-object v1, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    invoke-static {v1}, Lcom/google/android/music/tutorial/SignupStatus;->launchVerificationCheck(Landroid/content/Context;)V

    goto :goto_0

    .line 190
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v1}, Lcom/google/android/music/activities/SharedSongsActivity;->access$100(Lcom/google/android/music/activities/SharedSongsActivity;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isStoreAvailablilityVerified()Z

    move-result v1

    if-nez v1, :cond_1

    .line 192
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 193
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    # getter for: Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/music/activities/SharedSongsActivity;->access$200(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 194
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 195
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 196
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 197
    iget-object v1, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/music/activities/SharedSongsActivity;->startActivity(Landroid/content/Intent;)V

    .line 198
    iget-object v1, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    invoke-virtual {v1}, Lcom/google/android/music/activities/SharedSongsActivity;->finish()V

    goto :goto_0

    .line 203
    .end local v0    # "i":Landroid/content/Intent;
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 207
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->processGetShared()V

    goto :goto_0

    .line 211
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->processGetSharedPlaylist()V

    goto :goto_0

    .line 215
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    new-instance v2, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker$1;

    invoke-direct {v2, p0}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker$1;-><init>(Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/activities/SharedSongsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 224
    :pswitch_4
    iget-object v1, p0, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->this$0:Lcom/google/android/music/activities/SharedSongsActivity;

    new-instance v2, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker$2;

    invoke-direct {v2, p0}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker$2;-><init>(Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/activities/SharedSongsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 184
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method
