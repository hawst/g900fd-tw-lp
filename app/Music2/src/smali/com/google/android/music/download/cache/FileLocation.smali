.class public final Lcom/google/android/music/download/cache/FileLocation;
.super Ljava/lang/Object;
.source "FileLocation.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/download/cache/FileLocation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCacheType:I

.field private final mFullPath:Ljava/io/File;

.field private final mStorageType:Lcom/google/android/music/download/cache/CacheUtils$StorageType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    new-instance v0, Lcom/google/android/music/download/cache/FileLocation$1;

    invoke-direct {v0}, Lcom/google/android/music/download/cache/FileLocation$1;-><init>()V

    sput-object v0, Lcom/google/android/music/download/cache/FileLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/download/cache/FileLocation;->mFullPath:Ljava/io/File;

    .line 53
    invoke-static {}, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->values()[Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/music/download/cache/FileLocation;->mStorageType:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/download/cache/FileLocation;->mCacheType:I

    .line 55
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/music/download/cache/FileLocation$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/music/download/cache/FileLocation$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/music/download/cache/FileLocation;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;I)V
    .locals 2
    .param p1, "fullPath"    # Ljava/io/File;
    .param p2, "storageType"    # Lcom/google/android/music/download/cache/CacheUtils$StorageType;
    .param p3, "cacheType"    # I

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    if-nez p1, :cond_0

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The full path provided is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/download/cache/FileLocation;->mFullPath:Ljava/io/File;

    .line 47
    iput-object p2, p0, Lcom/google/android/music/download/cache/FileLocation;->mStorageType:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    .line 48
    iput p3, p0, Lcom/google/android/music/download/cache/FileLocation;->mCacheType:I

    .line 49
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    return v0
.end method

.method public getCacheType()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/google/android/music/download/cache/FileLocation;->mCacheType:I

    return v0
.end method

.method public getFullPath()Ljava/io/File;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/music/download/cache/FileLocation;->mFullPath:Ljava/io/File;

    return-object v0
.end method

.method public getSchemaValueForCacheType()I
    .locals 3

    .prologue
    .line 86
    const/4 v0, 0x0

    .line 87
    .local v0, "value":I
    iget v1, p0, Lcom/google/android/music/download/cache/FileLocation;->mCacheType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 88
    const/16 v0, 0x64

    .line 92
    :cond_0
    :goto_0
    return v0

    .line 89
    :cond_1
    iget v1, p0, Lcom/google/android/music/download/cache/FileLocation;->mCacheType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 90
    const/16 v0, 0xc8

    goto :goto_0
.end method

.method public getSchemaValueForStorageType()I
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/music/download/cache/FileLocation;->mStorageType:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-static {v0}, Lcom/google/android/music/download/cache/CacheUtils;->getSchemaValueForStorageType(Lcom/google/android/music/download/cache/CacheUtils$StorageType;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    const-string v1, "mFullPath="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/cache/FileLocation;->mFullPath:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    const-string v1, " mStorageType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/cache/FileLocation;->mStorageType:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 109
    const-string v1, " mCacheType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/download/cache/FileLocation;->mCacheType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 110
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/music/download/cache/FileLocation;->mFullPath:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/music/download/cache/FileLocation;->mStorageType:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-virtual {v0}, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 60
    iget v0, p0, Lcom/google/android/music/download/cache/FileLocation;->mCacheType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 61
    return-void
.end method
