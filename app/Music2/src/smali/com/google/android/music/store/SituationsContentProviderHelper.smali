.class public Lcom/google/android/music/store/SituationsContentProviderHelper;
.super Ljava/lang/Object;
.source "SituationsContentProviderHelper.java"


# static fields
.field private static final LOGV:Z

.field private static sInstance:Lcom/google/android/music/store/SituationsContentProviderHelper;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mSituationsCache:Lcom/google/android/music/store/SituationsCache;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/SituationsContentProviderHelper;->LOGV:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/music/store/SituationsCache;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cache"    # Lcom/google/android/music/store/SituationsCache;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/music/store/SituationsContentProviderHelper;->mContext:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lcom/google/android/music/store/SituationsContentProviderHelper;->mSituationsCache:Lcom/google/android/music/store/SituationsCache;

    .line 38
    return-void
.end method

.method private createMatrixCursor([Ljava/lang/String;)Landroid/database/MatrixCursor;
    .locals 3
    .param p1, "projection"    # [Ljava/lang/String;

    .prologue
    .line 186
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, p1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 187
    .local v0, "cursor":Landroid/database/MatrixCursor;
    iget-object v1, p0, Lcom/google/android/music/store/SituationsContentProviderHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/store/MusicContent$Situations;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/database/MatrixCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 188
    return-object v0
.end method

.method public static final declared-synchronized getInstance(Landroid/content/Context;Lcom/google/android/music/store/SituationsCache;)Lcom/google/android/music/store/SituationsContentProviderHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cache"    # Lcom/google/android/music/store/SituationsCache;

    .prologue
    .line 29
    const-class v1, Lcom/google/android/music/store/SituationsContentProviderHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/music/store/SituationsContentProviderHelper;->sInstance:Lcom/google/android/music/store/SituationsContentProviderHelper;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/google/android/music/store/SituationsContentProviderHelper;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/store/SituationsContentProviderHelper;-><init>(Landroid/content/Context;Lcom/google/android/music/store/SituationsCache;)V

    sput-object v0, Lcom/google/android/music/store/SituationsContentProviderHelper;->sInstance:Lcom/google/android/music/store/SituationsContentProviderHelper;

    .line 32
    :cond_0
    sget-object v0, Lcom/google/android/music/store/SituationsContentProviderHelper;->sInstance:Lcom/google/android/music/store/SituationsContentProviderHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private responseToSubSituationsColumns(Lcom/google/android/music/cloudclient/GetSituationsResponse;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "response"    # Lcom/google/android/music/cloudclient/GetSituationsResponse;
    .param p2, "situationId"    # Ljava/lang/String;
    .param p3, "projection"    # [Ljava/lang/String;

    .prologue
    .line 156
    invoke-direct {p0, p3}, Lcom/google/android/music/store/SituationsContentProviderHelper;->createMatrixCursor([Ljava/lang/String;)Landroid/database/MatrixCursor;

    move-result-object v0

    .line 158
    .local v0, "cursor":Landroid/database/MatrixCursor;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 159
    const-string v6, "SituationsHelper"

    const-string v7, "Empty situation id"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    :cond_0
    return-object v0

    .line 163
    :cond_1
    iget-object v5, p1, Lcom/google/android/music/cloudclient/GetSituationsResponse;->mSituations:Ljava/util/List;

    .line 165
    .local v5, "topLevelSituations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/SituationJson;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/cloudclient/SituationJson;

    .line 166
    .local v3, "situation":Lcom/google/android/music/cloudclient/SituationJson;
    iget-object v6, v3, Lcom/google/android/music/cloudclient/SituationJson;->mId:Ljava/lang/String;

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 167
    iget-object v6, v3, Lcom/google/android/music/cloudclient/SituationJson;->mSituations:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/cloudclient/SituationJson;

    .line 168
    .local v4, "subSituation":Lcom/google/android/music/cloudclient/SituationJson;
    invoke-static {p3}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 169
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4}, Lcom/google/android/music/cloudclient/SituationJson;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v0, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 171
    :cond_3
    sget-boolean v6, Lcom/google/android/music/store/SituationsContentProviderHelper;->LOGV:Z

    if-eqz v6, :cond_4

    .line 172
    const-string v6, "SituationsHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Adding subSituation: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    :cond_4
    invoke-static {v4, p3}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/SituationJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private responseToTopSituationsColumns(Lcom/google/android/music/cloudclient/GetSituationsResponse;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "response"    # Lcom/google/android/music/cloudclient/GetSituationsResponse;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    .line 135
    invoke-direct {p0, p2}, Lcom/google/android/music/store/SituationsContentProviderHelper;->createMatrixCursor([Ljava/lang/String;)Landroid/database/MatrixCursor;

    move-result-object v0

    .line 137
    .local v0, "cursor":Landroid/database/MatrixCursor;
    iget-object v3, p1, Lcom/google/android/music/cloudclient/GetSituationsResponse;->mSituations:Ljava/util/List;

    .line 139
    .local v3, "topLevelSituations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/SituationJson;>;"
    invoke-static {p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 140
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 150
    :cond_0
    return-object v0

    .line 142
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/SituationJson;

    .line 143
    .local v2, "situation":Lcom/google/android/music/cloudclient/SituationJson;
    sget-boolean v4, Lcom/google/android/music/store/SituationsContentProviderHelper;->LOGV:Z

    if-eqz v4, :cond_2

    .line 144
    const-string v4, "SituationsHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding situation: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :cond_2
    invoke-static {v2, p2}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/SituationJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized getRadioStations(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p1, "situationId"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    sget-boolean v9, Lcom/google/android/music/store/SituationsContentProviderHelper;->LOGV:Z

    if-eqz v9, :cond_0

    .line 85
    const-string v9, "SituationsHelper"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getRadioStations: situationId="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/music/store/SituationsContentProviderHelper;->createMatrixCursor([Ljava/lang/String;)Landroid/database/MatrixCursor;

    move-result-object v0

    .line 89
    .local v0, "cursor":Landroid/database/MatrixCursor;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 90
    const-string v9, "SituationsHelper"

    const-string v10, "Empty situation id"

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    .end local v0    # "cursor":Landroid/database/MatrixCursor;
    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    .line 94
    .restart local v0    # "cursor":Landroid/database/MatrixCursor;
    :cond_2
    :try_start_1
    iget-object v9, p0, Lcom/google/android/music/store/SituationsContentProviderHelper;->mSituationsCache:Lcom/google/android/music/store/SituationsCache;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/google/android/music/store/SituationsCache;->getResponse(Z)Lcom/google/android/music/cloudclient/GetSituationsResponse;

    move-result-object v5

    .line 96
    .local v5, "response":Lcom/google/android/music/cloudclient/GetSituationsResponse;
    if-eqz v5, :cond_3

    iget-object v9, v5, Lcom/google/android/music/cloudclient/GetSituationsResponse;->mSituations:Ljava/util/List;

    if-eqz v9, :cond_3

    iget-object v9, v5, Lcom/google/android/music/cloudclient/GetSituationsResponse;->mSituations:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 97
    :cond_3
    invoke-direct {p0, p2}, Lcom/google/android/music/store/SituationsContentProviderHelper;->createMatrixCursor([Ljava/lang/String;)Landroid/database/MatrixCursor;

    move-result-object v0

    goto :goto_0

    .line 100
    :cond_4
    iget-object v7, v5, Lcom/google/android/music/cloudclient/GetSituationsResponse;->mSituations:Ljava/util/List;

    .line 102
    .local v7, "topLevelSituations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/SituationJson;>;"
    const/4 v4, 0x0

    .line 104
    .local v4, "radioStations":Lcom/google/android/music/cloudclient/SituationJson;
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/music/cloudclient/SituationJson;

    .line 105
    .local v8, "topSituation":Lcom/google/android/music/cloudclient/SituationJson;
    iget-object v9, v8, Lcom/google/android/music/cloudclient/SituationJson;->mId:Ljava/lang/String;

    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 106
    move-object v4, v8

    .line 117
    .end local v8    # "topSituation":Lcom/google/android/music/cloudclient/SituationJson;
    :cond_6
    if-eqz v4, :cond_1

    .line 118
    iget-object v9, v4, Lcom/google/android/music/cloudclient/SituationJson;->mStations:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    .line 119
    .local v3, "radioStation":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    invoke-static {p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 120
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v3}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v0, v9}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 84
    .end local v0    # "cursor":Landroid/database/MatrixCursor;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "radioStation":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .end local v4    # "radioStations":Lcom/google/android/music/cloudclient/SituationJson;
    .end local v5    # "response":Lcom/google/android/music/cloudclient/GetSituationsResponse;
    .end local v7    # "topLevelSituations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/SituationJson;>;"
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 109
    .restart local v0    # "cursor":Landroid/database/MatrixCursor;
    .restart local v4    # "radioStations":Lcom/google/android/music/cloudclient/SituationJson;
    .restart local v5    # "response":Lcom/google/android/music/cloudclient/GetSituationsResponse;
    .restart local v7    # "topLevelSituations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/SituationJson;>;"
    .restart local v8    # "topSituation":Lcom/google/android/music/cloudclient/SituationJson;
    :cond_7
    :try_start_2
    iget-object v9, v8, Lcom/google/android/music/cloudclient/SituationJson;->mSituations:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/cloudclient/SituationJson;

    .line 110
    .local v6, "subSituation":Lcom/google/android/music/cloudclient/SituationJson;
    iget-object v9, v6, Lcom/google/android/music/cloudclient/SituationJson;->mId:Ljava/lang/String;

    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 111
    move-object v4, v6

    .line 112
    goto :goto_1

    .line 122
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v6    # "subSituation":Lcom/google/android/music/cloudclient/SituationJson;
    .end local v8    # "topSituation":Lcom/google/android/music/cloudclient/SituationJson;
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v3    # "radioStation":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    :cond_9
    sget-boolean v9, Lcom/google/android/music/store/SituationsContentProviderHelper;->LOGV:Z

    if-eqz v9, :cond_a

    .line 123
    const-string v9, "SituationsHelper"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Adding radioStation: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_a
    invoke-static {v3, p2}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/sync/google/model/SyncableRadioStation;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public declared-synchronized getSubSituations(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4
    .param p1, "situationId"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    .line 71
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lcom/google/android/music/store/SituationsContentProviderHelper;->LOGV:Z

    if-eqz v1, :cond_0

    .line 72
    const-string v1, "SituationsHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSubSituations: situationId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/store/SituationsContentProviderHelper;->mSituationsCache:Lcom/google/android/music/store/SituationsCache;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/music/store/SituationsCache;->getResponse(Z)Lcom/google/android/music/cloudclient/GetSituationsResponse;

    move-result-object v0

    .line 76
    .local v0, "response":Lcom/google/android/music/cloudclient/GetSituationsResponse;
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/google/android/music/cloudclient/GetSituationsResponse;->mSituations:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/music/cloudclient/GetSituationsResponse;->mSituations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 77
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/music/store/SituationsContentProviderHelper;->createMatrixCursor([Ljava/lang/String;)Landroid/database/MatrixCursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 79
    :goto_0
    monitor-exit p0

    return-object v1

    :cond_2
    :try_start_1
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/music/store/SituationsContentProviderHelper;->responseToSubSituationsColumns(Lcom/google/android/music/cloudclient/GetSituationsResponse;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 71
    .end local v0    # "response":Lcom/google/android/music/cloudclient/GetSituationsResponse;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getTopLevelSituations([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 3
    .param p1, "projection"    # [Ljava/lang/String;

    .prologue
    .line 57
    sget-boolean v1, Lcom/google/android/music/store/SituationsContentProviderHelper;->LOGV:Z

    if-eqz v1, :cond_0

    .line 58
    const-string v1, "SituationsHelper"

    const-string v2, "getTopLevelSituations"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/store/SituationsContentProviderHelper;->mSituationsCache:Lcom/google/android/music/store/SituationsCache;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/music/store/SituationsCache;->getResponse(Z)Lcom/google/android/music/cloudclient/GetSituationsResponse;

    move-result-object v0

    .line 63
    .local v0, "response":Lcom/google/android/music/cloudclient/GetSituationsResponse;
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/google/android/music/cloudclient/GetSituationsResponse;->mSituations:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/music/cloudclient/GetSituationsResponse;->mSituations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 64
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/music/store/SituationsContentProviderHelper;->createMatrixCursor([Ljava/lang/String;)Landroid/database/MatrixCursor;

    move-result-object v1

    .line 67
    :goto_0
    return-object v1

    :cond_2
    invoke-direct {p0, v0, p1}, Lcom/google/android/music/store/SituationsContentProviderHelper;->responseToTopSituationsColumns(Lcom/google/android/music/cloudclient/GetSituationsResponse;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0
.end method

.method public declared-synchronized getTopSituationHeader([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p1, "projection"    # [Ljava/lang/String;

    .prologue
    .line 41
    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lcom/google/android/music/store/SituationsContentProviderHelper;->LOGV:Z

    if-eqz v2, :cond_0

    .line 42
    const-string v2, "SituationsHelper"

    const-string v3, "getTopSituationHeader"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/store/SituationsContentProviderHelper;->mSituationsCache:Lcom/google/android/music/store/SituationsCache;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/music/store/SituationsCache;->getResponse(Z)Lcom/google/android/music/cloudclient/GetSituationsResponse;

    move-result-object v1

    .line 46
    .local v1, "response":Lcom/google/android/music/cloudclient/GetSituationsResponse;
    if-nez v1, :cond_1

    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/music/store/SituationsContentProviderHelper;->createMatrixCursor([Ljava/lang/String;)Landroid/database/MatrixCursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 53
    :goto_0
    monitor-exit p0

    return-object v0

    .line 50
    :cond_1
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/music/store/SituationsContentProviderHelper;->createMatrixCursor([Ljava/lang/String;)Landroid/database/MatrixCursor;

    move-result-object v0

    .line 51
    .local v0, "c":Landroid/database/MatrixCursor;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, v1, Lcom/google/android/music/cloudclient/GetSituationsResponse;->mPrimaryHeader:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 52
    iget-object v2, p0, Lcom/google/android/music/store/SituationsContentProviderHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/store/MusicContent$Situations;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3}, Landroid/database/MatrixCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 41
    .end local v0    # "c":Landroid/database/MatrixCursor;
    .end local v1    # "response":Lcom/google/android/music/cloudclient/GetSituationsResponse;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
