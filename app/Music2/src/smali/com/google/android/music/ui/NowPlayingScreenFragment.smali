.class public Lcom/google/android/music/ui/NowPlayingScreenFragment;
.super Lcom/google/android/music/ui/BaseFragment;
.source "NowPlayingScreenFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/google/android/music/ui/MusicFragment;
.implements Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/NowPlayingScreenFragment$16;,
        Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;,
        Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;,
        Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;
    }
.end annotation


# static fields
.field private static final DEBUG_QUEUE:Z


# instance fields
.field private mArtPager:Lcom/google/android/music/widgets/LinkableViewPager;

.field private mArtPagerAdapter:Landroid/support/v4/view/PagerAdapter;

.field private mCurrentAudioId:Lcom/google/android/music/download/ContentIdentifier;

.field private mCurrentContainer:Lcom/google/android/music/store/ContainerDescriptor;

.field private mCurrentLightsState:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

.field private mCurrentState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

.field private mCurrentTime:Landroid/widget/TextView;

.field private mCurrentTimeColor:Landroid/content/res/ColorStateList;

.field private mDuration:J

.field private mFlashWhenPausedColor:Landroid/content/res/ColorStateList;

.field private mFooterButtonsWrapper:Landroid/view/ViewGroup;

.field private final mHandler:Landroid/os/Handler;

.field private mHeaderButtonsWrapper:Landroid/view/View;

.field private mHeaderPager:Lcom/google/android/music/widgets/LinkableViewPager;

.field private mHeaderPagerAdapter:Landroid/support/v4/view/PagerAdapter;

.field private mHeaderPauseButton:Lcom/google/android/music/PlayPauseButton;

.field private mIsRestrictedPlaybackMode:Z

.field private mIsStarted:Z

.field private mIsTablet:Z

.field private mLightUpOnlyViewsAll:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mLightUpOnlyViewsLimited:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mLightsUpInterceptor:Landroid/view/View;

.field private mLockNowPlayingScreen:Z

.field private mMediaRouteButton:Landroid/support/v7/app/MediaRouteButton;

.field private mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

.field private mNextButton:Lcom/google/android/music/RepeatingImageButton;

.field private mOverflowMenu:Landroid/widget/ImageButton;

.field private mPauseButton:Lcom/google/android/music/PlayPauseButton;

.field private mPlaybackControls:Lcom/google/android/music/PlaybackControls;

.field private mPlaybackServiceConnection:Landroid/content/ServiceConnection;

.field private mPlaybackServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

.field private mPosOverride:J

.field private mPrevButton:Lcom/google/android/music/RepeatingImageButton;

.field private final mProgresAnimationListener:Lcom/google/android/music/animator/AnimatorListener;

.field private mProgress:Lcom/google/android/music/SizableTrackSeekBar;

.field private mProgressAccessibilityDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

.field private mQueue:Lcom/google/android/music/ui/BaseTrackListView;

.field private mQueueAdapter:Lcom/google/android/music/ui/QueueTrackListAdapter;

.field private mQueueCursor:Landroid/database/Cursor;

.field private mQueueFadeInAnimation:Landroid/view/animation/Animation;

.field private mQueueFadeOutAnimation:Landroid/view/animation/Animation;

.field private mQueueHeaderListener:Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueuePlayingFromHeaderListener;

.field private mQueuePlayingFromHeaderView:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

.field private mQueueShown:Z

.field private final mQueueSlideDownAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private final mQueueSlideUpAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mQueueSwitcher:Landroid/widget/ImageButton;

.field private mQueueWrapper:Landroid/view/View;

.field private mRatings:Lcom/google/android/music/RatingSelector;

.field private mRefreshDelayMs:J

.field private mRepeatButton:Landroid/widget/ImageView;

.field private mRootView:Landroid/view/View;

.field private mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mService:Lcom/google/android/music/playback/IMusicPlaybackService;

.field private mShuffleButton:Landroid/widget/ImageView;

.field private mSongBufferListener:Lcom/google/android/music/BufferProgressListener;

.field private mSongList:Lcom/google/android/music/medialist/SongList;

.field private mStatusListener:Landroid/content/BroadcastReceiver;

.field private mTabletCollapsedPlaybackControls:Lcom/google/android/music/PlaybackControls;

.field private mTabletHeaderPlaybackControlsWrapper:Landroid/view/View;

.field private mTabletHeaderRatings:Lcom/google/android/music/RatingSelector;

.field private mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

.field private mThumbAlphaAnimation:Lcom/google/android/music/animator/PropertyAnimator;

.field private mTotalTime:Landroid/widget/TextView;

.field private mUIInteractionListener:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->DEBUG_QUEUE:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 100
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseFragment;-><init>()V

    .line 153
    iput-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 211
    iput-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueShown:Z

    .line 218
    iput-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLockNowPlayingScreen:Z

    .line 223
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPosOverride:J

    .line 244
    sget-object v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;->OFF:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentLightsState:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    .line 246
    iput-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mThumbAlphaAnimation:Lcom/google/android/music/animator/PropertyAnimator;

    .line 249
    new-instance v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment$1;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueSlideDownAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 259
    new-instance v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment$2;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueSlideUpAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 273
    new-instance v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment$3;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mUIInteractionListener:Landroid/content/BroadcastReceiver;

    .line 314
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 320
    new-instance v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment$4;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueHeaderListener:Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueuePlayingFromHeaderListener;

    .line 786
    new-instance v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$7;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment$7;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgresAnimationListener:Lcom/google/android/music/animator/AnimatorListener;

    .line 926
    new-instance v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment$9;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 972
    new-instance v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$10;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment$10;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgressAccessibilityDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

    .line 1096
    new-instance v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$11;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment$11;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPlaybackServiceConnection:Landroid/content/ServiceConnection;

    .line 1227
    new-instance v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$12;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment$12;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHandler:Landroid/os/Handler;

    .line 1251
    new-instance v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mStatusListener:Landroid/content/BroadcastReceiver;

    .line 2017
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueWrapper:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setTabletMetadataWidth()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/ui/mrp/MediaRouteManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/SizableTrackSeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/android/music/ui/NowPlayingScreenFragment;Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;)Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;
    .param p1, "x1"    # Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentLightsState:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->accessibilityAnnounceSeekTime()V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/music/ui/NowPlayingScreenFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    iget-wide v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPosOverride:J

    return-wide v0
.end method

.method static synthetic access$1502(Lcom/google/android/music/ui/NowPlayingScreenFragment;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;
    .param p1, "x1"    # J

    .prologue
    .line 100
    iput-wide p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPosOverride:J

    return-wide p1
.end method

.method static synthetic access$1600(Lcom/google/android/music/ui/NowPlayingScreenFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    iget-wide v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mDuration:J

    return-wide v0
.end method

.method static synthetic access$1700(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/playback/IMusicPlaybackService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/google/android/music/ui/NowPlayingScreenFragment;Lcom/google/android/music/playback/IMusicPlaybackService;)Lcom/google/android/music/playback/IMusicPlaybackService;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;
    .param p1, "x1"    # Lcom/google/android/music/playback/IMusicPlaybackService;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    return-object p1
.end method

.method static synthetic access$1800()Z
    .locals 1

    .prologue
    .line 100
    sget-boolean v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->DEBUG_QUEUE:Z

    return v0
.end method

.method static synthetic access$1900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->refreshPlaybackControls()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueShown:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateTrackInfo()V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/music/ui/NowPlayingScreenFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->refreshNow()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$2200(Lcom/google/android/music/ui/NowPlayingScreenFragment;J)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;
    .param p1, "x1"    # J

    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->queueNextRefresh(J)V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setupPlayQueue()V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/music/ui/NowPlayingScreenFragment;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateTrackInfoImpl(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->turnLightsOff()V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/music/ui/NowPlayingScreenFragment;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateTrackInfo(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->queueNextRefreshNow()V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateButtonsVisibility()V

    return-void
.end method

.method static synthetic access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/NowPlayingScreenFragment;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->showQueue(ZZ)V

    return-void
.end method

.method static synthetic access$3000(Lcom/google/android/music/ui/NowPlayingScreenFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getPageCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/google/android/music/ui/NowPlayingScreenFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;
    .param p1, "x1"    # I

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->handlePageSelected(I)V

    return-void
.end method

.method static synthetic access$3300(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderButtonsWrapper:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsStarted:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/NowPlayingScreenFragment;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;
    .param p1, "x1"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setCurrentState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLockNowPlayingScreen:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->lockNowPlayingScreen()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->unlockNowPlayingScreen()V

    return-void
.end method

.method private accessibilityAnnounceSeekTime()V
    .locals 3

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgressAccessibilityDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/AccessibilityDelegateCompat;->sendAccessibilityEvent(Landroid/view/View;I)V

    .line 1023
    return-void
.end method

.method private clearDisplay()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1027
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 1029
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongBufferListener:Lcom/google/android/music/BufferProgressListener;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/music/BufferProgressListener;->updateCurrentSong(Lcom/google/android/music/download/ContentIdentifier;Z)V

    .line 1030
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    invoke-virtual {v0, v1}, Lcom/google/android/music/SizableTrackSeekBar;->setProgress(I)V

    .line 1032
    invoke-direct {p0, v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setRatings(I)V

    .line 1033
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTotalTime:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1034
    return-void
.end method

.method private findViewById(I)Landroid/view/View;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 687
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRootView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private getLightsAnimationDuration(Z)I
    .locals 1
    .param p1, "turnOn"    # Z

    .prologue
    .line 818
    const/16 v0, 0xc8

    return v0
.end method

.method private getPageCount()I
    .locals 2

    .prologue
    .line 1792
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 1798
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 1799
    .local v0, "count":I
    :goto_0
    return v0

    .line 1798
    .end local v0    # "count":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handlePageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1831
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v0, :cond_0

    .line 1833
    new-instance v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$15;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/NowPlayingScreenFragment$15;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;I)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 1848
    :cond_0
    return-void
.end method

.method private hideActionBar()V
    .locals 1

    .prologue
    .line 1562
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActionBarController()Lcom/google/android/music/ui/ActionBarController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/ui/ActionBarController;->hideActionBar()V

    .line 1563
    return-void
.end method

.method private initializeTabletHeaderControls()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 673
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderPlaybackControlsWrapper:Landroid/view/View;

    const v1, 0x7f0e01b5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/RepeatingImageButton;

    .line 675
    .local v2, "prev":Lcom/google/android/music/RepeatingImageButton;
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderPlaybackControlsWrapper:Landroid/view/View;

    const v1, 0x7f0e01b7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/PlayPauseButton;

    .line 677
    .local v3, "playPause":Lcom/google/android/music/PlayPauseButton;
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderPlaybackControlsWrapper:Landroid/view/View;

    const v1, 0x7f0e01b8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/RepeatingImageButton;

    .line 680
    .local v4, "next":Lcom/google/android/music/RepeatingImageButton;
    new-instance v0, Lcom/google/android/music/PlaybackControls;

    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/PlaybackControls;-><init>(Landroid/content/Context;Lcom/google/android/music/RepeatingImageButton;Lcom/google/android/music/PlayPauseButton;Lcom/google/android/music/RepeatingImageButton;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletCollapsedPlaybackControls:Lcom/google/android/music/PlaybackControls;

    .line 682
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderPlaybackControlsWrapper:Landroid/view/View;

    const v1, 0x7f0e0275

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/RatingSelector;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderRatings:Lcom/google/android/music/RatingSelector;

    .line 684
    return-void
.end method

.method private initializeView()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 564
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 566
    .local v7, "resources":Landroid/content/res/Resources;
    const v0, 0x7f0e01d8

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTime:Landroid/widget/TextView;

    .line 567
    const v0, 0x7f0e01d9

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTotalTime:Landroid/widget/TextView;

    .line 568
    const v0, 0x102000d

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/SizableTrackSeekBar;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    .line 569
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/SizableTrackSeekBar;->setEnabled(Z)V

    .line 570
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->getRefreshDelay(Landroid/view/View;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRefreshDelayMs:J

    .line 572
    const v0, 0x7f0e01dc

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueSwitcher:Landroid/widget/ImageButton;

    .line 573
    const v0, 0x7f0e00f3

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mOverflowMenu:Landroid/widget/ImageButton;

    .line 574
    const v0, 0x7f0e01dd

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/PlayPauseButton;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPauseButton:Lcom/google/android/music/PlayPauseButton;

    .line 575
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPauseButton:Lcom/google/android/music/PlayPauseButton;

    if-eqz v0, :cond_0

    .line 576
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPauseButton:Lcom/google/android/music/PlayPauseButton;

    invoke-virtual {v0, p0}, Lcom/google/android/music/PlayPauseButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 577
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPauseButton:Lcom/google/android/music/PlayPauseButton;

    invoke-virtual {v0, v8}, Lcom/google/android/music/PlayPauseButton;->setVisibility(I)V

    .line 580
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/SizableTrackSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 581
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/google/android/music/SizableTrackSeekBar;->setMax(I)V

    .line 582
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgressAccessibilityDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    .line 584
    const v0, 0x7f0e01db

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderButtonsWrapper:Landroid/view/View;

    .line 586
    const v0, 0x7f0e01d7

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mFooterButtonsWrapper:Landroid/view/ViewGroup;

    .line 588
    const v0, 0x7f0e0242

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/RatingSelector;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRatings:Lcom/google/android/music/RatingSelector;

    .line 590
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mFooterButtonsWrapper:Landroid/view/ViewGroup;

    const v1, 0x7f0e01b5

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/RepeatingImageButton;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPrevButton:Lcom/google/android/music/RepeatingImageButton;

    .line 591
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mFooterButtonsWrapper:Landroid/view/ViewGroup;

    const v1, 0x7f0e01b7

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/PlayPauseButton;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPauseButton:Lcom/google/android/music/PlayPauseButton;

    .line 593
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mFooterButtonsWrapper:Landroid/view/ViewGroup;

    const v1, 0x7f0e01b8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/RepeatingImageButton;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mNextButton:Lcom/google/android/music/RepeatingImageButton;

    .line 594
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mFooterButtonsWrapper:Landroid/view/ViewGroup;

    const v1, 0x7f0e00f1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mShuffleButton:Landroid/widget/ImageView;

    .line 595
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mFooterButtonsWrapper:Landroid/view/ViewGroup;

    const v1, 0x7f0e0201

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRepeatButton:Landroid/widget/ImageView;

    .line 596
    new-instance v0, Lcom/google/android/music/PlaybackControls;

    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPrevButton:Lcom/google/android/music/RepeatingImageButton;

    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPauseButton:Lcom/google/android/music/PlayPauseButton;

    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mNextButton:Lcom/google/android/music/RepeatingImageButton;

    iget-object v5, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRepeatButton:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mShuffleButton:Landroid/widget/ImageView;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/PlaybackControls;-><init>(Landroid/content/Context;Lcom/google/android/music/RepeatingImageButton;Lcom/google/android/music/PlayPauseButton;Lcom/google/android/music/RepeatingImageButton;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPlaybackControls:Lcom/google/android/music/PlaybackControls;

    .line 600
    const v0, 0x7f0e01d5

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/widgets/LinkableViewPager;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPager:Lcom/google/android/music/widgets/LinkableViewPager;

    .line 601
    const v0, 0x7f0e01d6

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/widgets/LinkableViewPager;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPager:Lcom/google/android/music/widgets/LinkableViewPager;

    .line 602
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPager:Lcom/google/android/music/widgets/LinkableViewPager;

    if-eqz v0, :cond_1

    .line 603
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPager:Lcom/google/android/music/widgets/LinkableViewPager;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPager:Lcom/google/android/music/widgets/LinkableViewPager;

    invoke-virtual {v0, v1}, Lcom/google/android/music/widgets/LinkableViewPager;->link(Lcom/google/android/music/widgets/LinkableViewPager;)V

    .line 606
    :cond_1
    const v0, 0x7f0e01df

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    .line 607
    const v0, 0x7f0e01e0

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderPlaybackControlsWrapper:Landroid/view/View;

    .line 608
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderPlaybackControlsWrapper:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 609
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->initializeTabletHeaderControls()V

    .line 612
    :cond_2
    const v0, 0x7f0e01e3

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/BaseTrackListView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueue:Lcom/google/android/music/ui/BaseTrackListView;

    .line 613
    const v0, 0x7f0e01e1

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueWrapper:Landroid/view/View;

    .line 615
    new-instance v0, Lcom/google/android/music/StreamingBufferProgressListener;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    invoke-direct {v0, v1}, Lcom/google/android/music/StreamingBufferProgressListener;-><init>(Landroid/widget/ProgressBar;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongBufferListener:Lcom/google/android/music/BufferProgressListener;

    .line 617
    const v0, 0x7f0c0090

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTimeColor:Landroid/content/res/ColorStateList;

    .line 618
    const v0, 0x7f0c0091

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mFlashWhenPausedColor:Landroid/content/res/ColorStateList;

    .line 620
    const v0, 0x7f0e01de

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLightsUpInterceptor:Landroid/view/View;

    .line 621
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLightsUpInterceptor:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 622
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLightsUpInterceptor:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 625
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTime:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTotalTime:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRatings:Lcom/google/android/music/RatingSelector;

    invoke-static {v0, v1, v2}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLightUpOnlyViewsAll:Ljava/util/Set;

    .line 626
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTime:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTotalTime:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRatings:Lcom/google/android/music/RatingSelector;

    invoke-static {v0, v1, v2}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLightUpOnlyViewsLimited:Ljava/util/Set;

    .line 629
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueSwitcher:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 630
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mOverflowMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 633
    invoke-direct {p0, v8, v8}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->showQueue(ZZ)V

    .line 634
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mOverflowMenu:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 637
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f050009

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueFadeInAnimation:Landroid/view/animation/Animation;

    .line 639
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f05001c

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueFadeOutAnimation:Landroid/view/animation/Animation;

    .line 642
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0e01da

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/MediaRouteButton;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteButton:Landroid/support/v7/app/MediaRouteButton;

    .line 644
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteButton:Landroid/support/v7/app/MediaRouteButton;

    if-eqz v0, :cond_4

    .line 645
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isMediaRouteSupportEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 647
    new-instance v0, Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    .line 648
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onCreate()V

    .line 649
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteButton:Landroid/support/v7/app/MediaRouteButton;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->bindMediaRouteButton(Landroid/support/v7/app/MediaRouteButton;)V

    .line 655
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRootView:Landroid/view/View;

    new-instance v1, Lcom/google/android/music/ui/NowPlayingScreenFragment$6;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment$6;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 665
    const v0, 0x7f0e01e2

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueuePlayingFromHeaderView:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    .line 667
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueuePlayingFromHeaderView:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    if-eqz v0, :cond_5

    .line 668
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueuePlayingFromHeaderView:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueHeaderListener:Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueuePlayingFromHeaderListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setQueuePlayingFromHeaderListener(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueuePlayingFromHeaderListener;)V

    .line 670
    :cond_5
    return-void

    .line 651
    :cond_6
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteButton:Landroid/support/v7/app/MediaRouteButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/app/MediaRouteButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private lockNowPlayingScreen()V
    .locals 2

    .prologue
    .line 1302
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueAdapter:Lcom/google/android/music/ui/QueueTrackListAdapter;

    if-eqz v0, :cond_0

    .line 1303
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueAdapter:Lcom/google/android/music/ui/QueueTrackListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/QueueTrackListAdapter;->setEnabled(Z)V

    .line 1305
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueuePlayingFromHeaderView:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    if-eqz v0, :cond_1

    .line 1306
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueuePlayingFromHeaderView:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->lockQueueHeader()V

    .line 1308
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLockNowPlayingScreen:Z

    .line 1309
    return-void
.end method

.method private queueNextRefresh(J)V
    .locals 3
    .param p1, "delay"    # J

    .prologue
    const/4 v2, 0x1

    .line 1131
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1132
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1133
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1134
    return-void
.end method

.method private queueNextRefreshNow()V
    .locals 2

    .prologue
    .line 1127
    const-wide/16 v0, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->queueNextRefresh(J)V

    .line 1128
    return-void
.end method

.method private refreshNow()J
    .locals 12

    .prologue
    .line 1143
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v9, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v8, v9, :cond_1

    .line 1144
    :cond_0
    const-wide/16 v4, 0x1f4

    .line 1224
    :goto_0
    return-wide v4

    .line 1148
    :cond_1
    :try_start_0
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v8}, Lcom/google/android/music/playback/IMusicPlaybackService;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v6

    .line 1150
    .local v6, "state":Lcom/google/android/music/playback/PlaybackState;
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPauseButton:Lcom/google/android/music/PlayPauseButton;

    if-eqz v8, :cond_2

    .line 1151
    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isPreparing()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isStreaming()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1152
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPauseButton:Lcom/google/android/music/PlayPauseButton;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/google/android/music/PlayPauseButton;->setCurrentPlayState(I)V

    .line 1160
    :cond_2
    :goto_1
    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isPreparing()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isStreaming()Z

    move-result v8

    if-nez v8, :cond_4

    :cond_3
    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isPlaying()Z

    move-result v8

    if-eqz v8, :cond_5

    :cond_4
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v9, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v8, v9, :cond_8

    .line 1162
    :cond_5
    const-wide/16 v4, -0x1

    goto :goto_0

    .line 1153
    :cond_6
    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isPlaying()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1154
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPauseButton:Lcom/google/android/music/PlayPauseButton;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Lcom/google/android/music/PlayPauseButton;->setCurrentPlayState(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1221
    .end local v6    # "state":Lcom/google/android/music/playback/PlaybackState;
    :catch_0
    move-exception v0

    .line 1222
    .local v0, "e":Landroid/os/RemoteException;
    const-string v8, "NowPlayingFragment"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1224
    const-wide/16 v4, 0x1f4

    goto :goto_0

    .line 1156
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v6    # "state":Lcom/google/android/music/playback/PlaybackState;
    :cond_7
    :try_start_1
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPauseButton:Lcom/google/android/music/PlayPauseButton;

    const/4 v9, 0x3

    invoke-virtual {v8, v9}, Lcom/google/android/music/PlayPauseButton;->setCurrentPlayState(I)V

    goto :goto_1

    .line 1165
    :cond_8
    iget-wide v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPosOverride:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-gez v8, :cond_a

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->getPosition()J

    move-result-wide v2

    .line 1166
    .local v2, "pos":J
    :goto_2
    iget-wide v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRefreshDelayMs:J

    iget-wide v10, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRefreshDelayMs:J

    rem-long v10, v2, v10

    sub-long v4, v8, v10

    .line 1167
    .local v4, "remaining":J
    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-ltz v8, :cond_d

    iget-wide v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mDuration:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_d

    .line 1168
    const-wide/16 v8, 0x3e8

    div-long v8, v2, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 1169
    .local v7, "time":Ljava/lang/Long;
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_9

    .line 1170
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v9, v10, v11}, Lcom/google/android/music/utils/MusicUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1171
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 1174
    :cond_9
    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isPlaying()Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1175
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTime:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTimeColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1183
    :goto_3
    const-wide/16 v8, 0x3e8

    mul-long/2addr v8, v2

    iget-wide v10, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mDuration:J

    div-long/2addr v8, v10

    long-to-int v1, v8

    .line 1184
    .local v1, "progress":I
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    invoke-virtual {v8, v1}, Lcom/google/android/music/SizableTrackSeekBar;->setProgress(I)V

    goto/16 :goto_0

    .line 1165
    .end local v1    # "progress":I
    .end local v2    # "pos":J
    .end local v4    # "remaining":J
    .end local v7    # "time":Ljava/lang/Long;
    :cond_a
    iget-wide v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPosOverride:J

    goto :goto_2

    .line 1178
    .restart local v2    # "pos":J
    .restart local v4    # "remaining":J
    .restart local v7    # "time":Ljava/lang/Long;
    :cond_b
    iget-object v9, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTime:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v8

    iget-object v10, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTimeColor:Landroid/content/res/ColorStateList;

    if-ne v8, v10, :cond_c

    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mFlashWhenPausedColor:Landroid/content/res/ColorStateList;

    :goto_4
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1180
    const-wide/16 v4, 0x1f4

    goto :goto_3

    .line 1178
    :cond_c
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTimeColor:Landroid/content/res/ColorStateList;

    goto :goto_4

    .line 1212
    .end local v7    # "time":Ljava/lang/Long;
    :cond_d
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTime:Landroid/widget/TextView;

    const-string v9, "--:--"

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1213
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTime:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 1214
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTime:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTimeColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1215
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/google/android/music/SizableTrackSeekBar;->setProgress(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private refreshPlaybackControls()V
    .locals 1

    .prologue
    .line 1120
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPlaybackControls:Lcom/google/android/music/PlaybackControls;

    invoke-virtual {v0}, Lcom/google/android/music/PlaybackControls;->refreshButtonImages()V

    .line 1121
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletCollapsedPlaybackControls:Lcom/google/android/music/PlaybackControls;

    if-eqz v0, :cond_0

    .line 1122
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletCollapsedPlaybackControls:Lcom/google/android/music/PlaybackControls;

    invoke-virtual {v0}, Lcom/google/android/music/PlaybackControls;->refreshButtonImages()V

    .line 1124
    :cond_0
    return-void
.end method

.method private setCurrentPage()V
    .locals 6

    .prologue
    .line 1806
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v4, :cond_1

    .line 1825
    :cond_0
    :goto_0
    return-void

    .line 1810
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v4}, Lcom/google/android/music/playback/IMusicPlaybackService;->getQueuePosition()I

    move-result v3

    .line 1813
    .local v3, "queuePos":I
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPagerAdapter:Landroid/support/v4/view/PagerAdapter;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPagerAdapter:Landroid/support/v4/view/PagerAdapter;

    invoke-virtual {v4}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 1814
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v4}, Lcom/google/android/music/playback/IMusicPlaybackService;->getAudioId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v1

    .line 1816
    .local v1, "audioId":Lcom/google/android/music/download/ContentIdentifier;
    if-eqz v1, :cond_2

    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentAudioId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v1, v4}, Lcom/google/android/music/download/ContentIdentifier;->equals(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v0, 0x1

    .line 1817
    .local v0, "animate":Z
    :goto_1
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPager:Lcom/google/android/music/widgets/LinkableViewPager;

    invoke-virtual {v4, v3, v0}, Lcom/google/android/music/widgets/LinkableViewPager;->setCurrentItem(IZ)V

    .line 1818
    iput-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentAudioId:Lcom/google/android/music/download/ContentIdentifier;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1822
    .end local v0    # "animate":Z
    .end local v1    # "audioId":Lcom/google/android/music/download/ContentIdentifier;
    .end local v3    # "queuePos":I
    :catch_0
    move-exception v2

    .line 1823
    .local v2, "e":Landroid/os/RemoteException;
    const-string v4, "NowPlayingFragment"

    invoke-virtual {v2}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1816
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v1    # "audioId":Lcom/google/android/music/download/ContentIdentifier;
    .restart local v3    # "queuePos":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1819
    .end local v1    # "audioId":Lcom/google/android/music/download/ContentIdentifier;
    :cond_3
    :try_start_1
    sget-boolean v4, Lcom/google/android/music/ui/NowPlayingScreenFragment;->DEBUG_QUEUE:Z

    if-eqz v4, :cond_0

    .line 1820
    const-string v4, "NowPlayingFragment"

    const-string v5, "setCurrentPage: Cursor not updated yet. Wait for the next update."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private setCurrentState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V
    .locals 3
    .param p1, "newState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 338
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 339
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isEmptyScreenShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 340
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-eq v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/BaseActivity;->enableSideDrawer(Z)V

    .line 344
    :cond_0
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-eq p1, v0, :cond_1

    .line 345
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->queueNextRefreshNow()V

    .line 347
    :cond_1
    return-void

    .line 340
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setRatings(I)V
    .locals 1
    .param p1, "rating"    # I

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRatings:Lcom/google/android/music/RatingSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/music/RatingSelector;->setRating(I)V

    .line 1043
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderRatings:Lcom/google/android/music/RatingSelector;

    if-eqz v0, :cond_0

    .line 1044
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderRatings:Lcom/google/android/music/RatingSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/music/RatingSelector;->setRating(I)V

    .line 1046
    :cond_0
    return-void
.end method

.method private setRatingsVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 1074
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRatings:Lcom/google/android/music/RatingSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/music/RatingSelector;->setVisibility(I)V

    .line 1075
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderRatings:Lcom/google/android/music/RatingSelector;

    if-eqz v0, :cond_0

    .line 1076
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderRatings:Lcom/google/android/music/RatingSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/music/RatingSelector;->setVisibility(I)V

    .line 1078
    :cond_0
    return-void
.end method

.method private setTabletMetadataWidth()V
    .locals 3

    .prologue
    .line 1613
    iget-boolean v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsTablet:Z

    if-nez v2, :cond_0

    .line 1618
    :goto_0
    return-void

    .line 1614
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRootView:Landroid/view/View;

    .line 1615
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1616
    .local v1, "width":I
    :goto_1
    if-nez v1, :cond_1

    const/4 v1, -0x2

    .line 1617
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    invoke-virtual {v2, v1}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->setMetadataViewWidth(I)V

    goto :goto_0

    .line 1615
    .end local v1    # "width":I
    :cond_2
    const/4 v1, -0x2

    goto :goto_1
.end method

.method private setupNormalPlaybackMode()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1049
    iput-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsRestrictedPlaybackMode:Z

    .line 1050
    invoke-direct {p0, v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setRatingsVisibility(I)V

    .line 1051
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTotalTime:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1052
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1053
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueShown:Z

    if-nez v0, :cond_0

    .line 1054
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Lcom/google/android/music/SizableTrackSeekBar;->setThumbAlpha(I)V

    .line 1055
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/SizableTrackSeekBar;->setEnabled(Z)V

    .line 1057
    :cond_0
    return-void
.end method

.method private setupPlayQueue()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1433
    iget-boolean v5, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLockNowPlayingScreen:Z

    if-eqz v5, :cond_1

    .line 1487
    :cond_0
    :goto_0
    return-void

    .line 1434
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;

    move-result-object v0

    .line 1435
    .local v0, "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v3

    .line 1436
    .local v3, "state":Lcom/google/android/music/playback/PlaybackState;
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lcom/google/android/music/playback/PlaybackState;->hasValidPlaylist()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1437
    invoke-virtual {v3}, Lcom/google/android/music/playback/PlaybackState;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 1438
    iget-object v5, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    if-nez v5, :cond_2

    .line 1439
    const-string v4, "NowPlayingFragment"

    const-string v5, "The play queue not available."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1442
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-nez v5, :cond_3

    .line 1445
    const-string v4, "NowPlayingFragment"

    const-string v5, "Not attached to an activity."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1448
    :cond_3
    new-instance v5, Lcom/google/android/music/ui/QueueTrackListAdapter;

    iget-object v6, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-direct {v5, p0, v2, v6}, Lcom/google/android/music/ui/QueueTrackListAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ZLcom/google/android/music/medialist/MediaList;)V

    iput-object v5, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueAdapter:Lcom/google/android/music/ui/QueueTrackListAdapter;

    .line 1451
    iget-object v5, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueAdapter:Lcom/google/android/music/ui/QueueTrackListAdapter;

    iget-boolean v6, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueShown:Z

    invoke-virtual {v5, v6}, Lcom/google/android/music/ui/QueueTrackListAdapter;->showAlbumArt(Z)V

    .line 1452
    iget-object v5, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueue:Lcom/google/android/music/ui/BaseTrackListView;

    iget-object v6, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueAdapter:Lcom/google/android/music/ui/QueueTrackListAdapter;

    invoke-virtual {v5, v6}, Lcom/google/android/music/ui/BaseTrackListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1453
    iget-object v5, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueue:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-virtual {v5, v4}, Lcom/google/android/music/ui/BaseTrackListView;->setFastScrollEnabled(Z)V

    .line 1454
    iget-object v5, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueue:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-virtual {v5}, Lcom/google/android/music/ui/BaseTrackListView;->scrollToNowPlaying()V

    .line 1455
    iget-object v5, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueue:Lcom/google/android/music/ui/BaseTrackListView;

    iget-object v6, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v5, v6}, Lcom/google/android/music/ui/BaseTrackListView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    .line 1456
    iget-object v5, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueue:Lcom/google/android/music/ui/BaseTrackListView;

    new-instance v6, Lcom/google/android/music/ui/NowPlayingScreenFragment$14;

    invoke-direct {v6, p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment$14;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    invoke-virtual {v5, v6}, Lcom/google/android/music/ui/BaseTrackListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1468
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateButtonsVisibility()V

    .line 1470
    iget-object v5, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    const/16 v6, 0x100

    invoke-virtual {v5, v6}, Lcom/google/android/music/medialist/SongList;->isFlagSet(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1471
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setupNormalPlaybackMode()V

    .line 1476
    :goto_1
    iget-object v5, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/music/medialist/SongList;->getFullContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_4

    move v2, v4

    .line 1478
    .local v2, "loaderIdToInit":I
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    .line 1479
    .local v1, "lm":Landroid/support/v4/app/LoaderManager;
    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 1481
    if-eqz v0, :cond_0

    invoke-virtual {v0, v4}, Lcom/google/android/music/widgets/ExpandingScrollView;->setHidden(Z)V

    goto/16 :goto_0

    .line 1473
    .end local v1    # "lm":Landroid/support/v4/app/LoaderManager;
    .end local v2    # "loaderIdToInit":I
    :cond_5
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setupRestrictedPlaybackMode()V

    goto :goto_1

    .line 1483
    :cond_6
    invoke-direct {p0, v4, v4}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->showQueue(ZZ)V

    .line 1484
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->showActionBar()V

    .line 1485
    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Lcom/google/android/music/widgets/ExpandingScrollView;->setHidden(Z)V

    goto/16 :goto_0
.end method

.method private setupRestrictedPlaybackMode()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 1060
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsRestrictedPlaybackMode:Z

    .line 1061
    invoke-direct {p0, v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setRatingsVisibility(I)V

    .line 1062
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTotalTime:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1063
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1064
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    invoke-virtual {v0, v2}, Lcom/google/android/music/SizableTrackSeekBar;->setThumbAlpha(I)V

    .line 1065
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    invoke-virtual {v0, v2}, Lcom/google/android/music/SizableTrackSeekBar;->setEnabled(Z)V

    .line 1066
    return-void
.end method

.method private setupViewPager()V
    .locals 2

    .prologue
    .line 1766
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPagerAdapter:Landroid/support/v4/view/PagerAdapter;

    if-nez v0, :cond_1

    .line 1767
    new-instance v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPager:Lcom/google/android/music/widgets/LinkableViewPager;

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;Landroid/support/v4/view/ViewPager;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPagerAdapter:Landroid/support/v4/view/PagerAdapter;

    .line 1776
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPager:Lcom/google/android/music/widgets/LinkableViewPager;

    if-eqz v0, :cond_0

    .line 1777
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPagerAdapter:Landroid/support/v4/view/PagerAdapter;

    if-nez v0, :cond_2

    .line 1778
    new-instance v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPager:Lcom/google/android/music/widgets/LinkableViewPager;

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;Landroid/support/v4/view/ViewPager;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPagerAdapter:Landroid/support/v4/view/PagerAdapter;

    .line 1786
    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setCurrentPage()V

    .line 1787
    return-void

    .line 1771
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPager:Lcom/google/android/music/widgets/LinkableViewPager;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPagerAdapter:Landroid/support/v4/view/PagerAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/music/widgets/LinkableViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 1772
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPagerAdapter:Landroid/support/v4/view/PagerAdapter;

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 1782
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPager:Lcom/google/android/music/widgets/LinkableViewPager;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPagerAdapter:Landroid/support/v4/view/PagerAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/music/widgets/LinkableViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 1783
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPagerAdapter:Landroid/support/v4/view/PagerAdapter;

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V

    goto :goto_1
.end method

.method private shouldHideShuffleAndRepeat()Z
    .locals 1

    .prologue
    .line 691
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->isInInfiniteMixMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->isCurrentContentFromStoreUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showActionBar()V
    .locals 1

    .prologue
    .line 1558
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActionBarController()Lcom/google/android/music/ui/ActionBarController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/ui/ActionBarController;->showActionBar()V

    .line 1559
    return-void
.end method

.method private showQueue(ZZ)V
    .locals 5
    .param p1, "show"    # Z
    .param p2, "animate"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1499
    iget-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLockNowPlayingScreen:Z

    if-eqz v1, :cond_0

    .line 1555
    :goto_0
    return-void

    .line 1501
    :cond_0
    if-eqz p2, :cond_2

    .line 1503
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPager:Lcom/google/android/music/widgets/LinkableViewPager;

    invoke-virtual {v1}, Lcom/google/android/music/widgets/LinkableViewPager;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 1504
    .local v0, "existingAnimation":Landroid/view/animation/Animation;
    if-eqz v0, :cond_1

    .line 1505
    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1507
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPager:Lcom/google/android/music/widgets/LinkableViewPager;

    invoke-virtual {v1}, Lcom/google/android/music/widgets/LinkableViewPager;->clearAnimation()V

    .line 1510
    .end local v0    # "existingAnimation":Landroid/view/animation/Animation;
    :cond_2
    if-eqz p1, :cond_7

    .line 1512
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueWrapper:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1513
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueAdapter:Lcom/google/android/music/ui/QueueTrackListAdapter;

    if-eqz v1, :cond_3

    .line 1515
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueAdapter:Lcom/google/android/music/ui/QueueTrackListAdapter;

    invoke-virtual {v1, v3}, Lcom/google/android/music/ui/QueueTrackListAdapter;->showAlbumArt(Z)V

    .line 1518
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueue:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseTrackListView;->scrollToNowPlaying()V

    .line 1522
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isXLargeScreen()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1523
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    invoke-virtual {v1, v2}, Lcom/google/android/music/SizableTrackSeekBar;->setThumbAlpha(I)V

    .line 1524
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    invoke-virtual {v1, v2}, Lcom/google/android/music/SizableTrackSeekBar;->setEnabled(Z)V

    .line 1527
    :cond_4
    if-eqz p2, :cond_6

    .line 1528
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPager:Lcom/google/android/music/widgets/LinkableViewPager;

    invoke-virtual {v1}, Lcom/google/android/music/widgets/LinkableViewPager;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 1529
    .restart local v0    # "existingAnimation":Landroid/view/animation/Animation;
    if-eqz v0, :cond_5

    .line 1530
    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1532
    :cond_5
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueFadeInAnimation:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueSlideDownAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1533
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueWrapper:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueFadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1553
    .end local v0    # "existingAnimation":Landroid/view/animation/Animation;
    :cond_6
    :goto_1
    iput-boolean p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueShown:Z

    .line 1554
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateQueueSwitcherState()V

    goto :goto_0

    .line 1537
    :cond_7
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPager:Lcom/google/android/music/widgets/LinkableViewPager;

    invoke-virtual {v1, v2}, Lcom/google/android/music/widgets/LinkableViewPager;->setVisibility(I)V

    .line 1539
    iget-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsRestrictedPlaybackMode:Z

    if-nez v1, :cond_8

    .line 1541
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Lcom/google/android/music/SizableTrackSeekBar;->setThumbAlpha(I)V

    .line 1542
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    invoke-virtual {v1, v3}, Lcom/google/android/music/SizableTrackSeekBar;->setEnabled(Z)V

    .line 1545
    :cond_8
    if-eqz p2, :cond_9

    .line 1546
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueFadeOutAnimation:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueSlideUpAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1547
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueWrapper:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueFadeOutAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    .line 1549
    :cond_9
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueWrapper:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private startLightsAnimation(Z)V
    .locals 10
    .param p1, "on"    # Z

    .prologue
    const/16 v5, 0xff

    const/4 v0, 0x0

    .line 741
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 742
    if-eqz p1, :cond_0

    .line 743
    sget-object v1, Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;->TRANSITIONING_UP:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    iput-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentLightsState:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    .line 755
    :goto_0
    if-eqz p1, :cond_2

    .line 756
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->shouldHideShuffleAndRepeat()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 757
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLightUpOnlyViewsLimited:Ljava/util/Set;

    .line 765
    .local v8, "targetViews":Ljava/util/Set;, "Ljava/util/Set<Landroid/view/View;>;"
    :goto_1
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    .line 766
    .local v9, "view":Landroid/view/View;
    invoke-direct {p0, p1, v9}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->startLightsAnimation(ZLandroid/view/View;)V

    goto :goto_2

    .line 745
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "targetViews":Ljava/util/Set;, "Ljava/util/Set<Landroid/view/View;>;"
    .end local v9    # "view":Landroid/view/View;
    :cond_0
    sget-object v1, Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;->TRANSITIONING_DOWN:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    iput-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentLightsState:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    goto :goto_0

    .line 759
    :cond_1
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLightUpOnlyViewsAll:Ljava/util/Set;

    .restart local v8    # "targetViews":Ljava/util/Set;, "Ljava/util/Set<Landroid/view/View;>;"
    goto :goto_1

    .line 762
    .end local v8    # "targetViews":Ljava/util/Set;, "Ljava/util/Set<Landroid/view/View;>;"
    :cond_2
    iget-object v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLightUpOnlyViewsAll:Ljava/util/Set;

    .restart local v8    # "targetViews":Ljava/util/Set;, "Ljava/util/Set<Landroid/view/View;>;"
    goto :goto_1

    .line 769
    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_3
    if-eqz p1, :cond_6

    move v4, v0

    .line 770
    .local v4, "startAlpha":I
    :goto_3
    if-eqz p1, :cond_7

    .line 771
    .local v5, "endAlpha":I
    :goto_4
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mThumbAlphaAnimation:Lcom/google/android/music/animator/PropertyAnimator;

    if-eqz v0, :cond_5

    .line 772
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mThumbAlphaAnimation:Lcom/google/android/music/animator/PropertyAnimator;

    invoke-virtual {v0}, Lcom/google/android/music/animator/PropertyAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v6

    .line 773
    .local v6, "animatedValue":Ljava/lang/Object;
    if-eqz v6, :cond_4

    instance-of v0, v6, Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 774
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mThumbAlphaAnimation:Lcom/google/android/music/animator/PropertyAnimator;

    invoke-virtual {v0}, Lcom/google/android/music/animator/PropertyAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 776
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mThumbAlphaAnimation:Lcom/google/android/music/animator/PropertyAnimator;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgresAnimationListener:Lcom/google/android/music/animator/AnimatorListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/animator/PropertyAnimator;->removeListener(Lcom/google/android/music/animator/AnimatorListener;)V

    .line 777
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mThumbAlphaAnimation:Lcom/google/android/music/animator/PropertyAnimator;

    invoke-virtual {v0}, Lcom/google/android/music/animator/PropertyAnimator;->cancel()V

    .line 779
    .end local v6    # "animatedValue":Ljava/lang/Object;
    :cond_5
    new-instance v0, Lcom/google/android/music/animator/PropertyAnimator;

    invoke-direct {p0, p1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getLightsAnimationDuration(Z)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;

    const-string v3, "thumbAlpha"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/animator/PropertyAnimator;-><init>(ILjava/lang/Object;Ljava/lang/String;II)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mThumbAlphaAnimation:Lcom/google/android/music/animator/PropertyAnimator;

    .line 781
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mThumbAlphaAnimation:Lcom/google/android/music/animator/PropertyAnimator;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgresAnimationListener:Lcom/google/android/music/animator/AnimatorListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/animator/PropertyAnimator;->addListener(Lcom/google/android/music/animator/AnimatorListener;)V

    .line 782
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mThumbAlphaAnimation:Lcom/google/android/music/animator/PropertyAnimator;

    invoke-virtual {v0}, Lcom/google/android/music/animator/PropertyAnimator;->start()V

    .line 784
    return-void

    .end local v4    # "startAlpha":I
    .end local v5    # "endAlpha":I
    :cond_6
    move v4, v5

    .line 769
    goto :goto_3

    .restart local v4    # "startAlpha":I
    :cond_7
    move v5, v0

    .line 770
    goto :goto_4
.end method

.method private startLightsAnimation(ZLandroid/view/View;)V
    .locals 7
    .param p1, "turnOn"    # Z
    .param p2, "target"    # Landroid/view/View;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 823
    invoke-virtual {p2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    .line 824
    .local v2, "existingAnimation":Landroid/view/animation/Animation;
    if-eqz v2, :cond_0

    .line 827
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 829
    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/4 v6, 0x4

    if-ne v5, v6, :cond_1

    .line 831
    invoke-virtual {p2}, Landroid/view/View;->clearAnimation()V

    .line 865
    .end local v2    # "existingAnimation":Landroid/view/animation/Animation;
    :goto_0
    return-void

    .line 835
    .restart local v2    # "existingAnimation":Landroid/view/animation/Animation;
    :cond_1
    if-eqz v2, :cond_2

    instance-of v5, v2, Lcom/google/android/music/animator/StatefulAlphaAnimation;

    if-eqz v5, :cond_2

    .line 836
    check-cast v2, Lcom/google/android/music/animator/StatefulAlphaAnimation;

    .end local v2    # "existingAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v2}, Lcom/google/android/music/animator/StatefulAlphaAnimation;->getCurrentAlpha()F

    move-result v1

    .line 840
    .local v1, "currentAlpha":F
    :goto_1
    invoke-virtual {p2}, Landroid/view/View;->clearAnimation()V

    .line 841
    new-instance v0, Lcom/google/android/music/animator/StatefulAlphaAnimation;

    if-eqz p1, :cond_4

    :goto_2
    invoke-direct {v0, v1, v4}, Lcom/google/android/music/animator/StatefulAlphaAnimation;-><init>(FF)V

    .line 843
    .local v0, "alphaAnim":Lcom/google/android/music/animator/StatefulAlphaAnimation;
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getLightsAnimationDuration(Z)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v0, v4, v5}, Lcom/google/android/music/animator/StatefulAlphaAnimation;->setDuration(J)V

    .line 845
    new-instance v3, Lcom/google/android/music/ui/NowPlayingScreenFragment$8;

    invoke-direct {v3, p0, p1, p2}, Lcom/google/android/music/ui/NowPlayingScreenFragment$8;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;ZLandroid/view/View;)V

    invoke-virtual {v0, v3}, Lcom/google/android/music/animator/StatefulAlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 864
    invoke-virtual {p2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 838
    .end local v0    # "alphaAnim":Lcom/google/android/music/animator/StatefulAlphaAnimation;
    .end local v1    # "currentAlpha":F
    .restart local v2    # "existingAnimation":Landroid/view/animation/Animation;
    :cond_2
    if-eqz p1, :cond_3

    move v1, v3

    .restart local v1    # "currentAlpha":F
    :goto_3
    goto :goto_1

    .end local v1    # "currentAlpha":F
    :cond_3
    move v1, v4

    goto :goto_3

    .end local v2    # "existingAnimation":Landroid/view/animation/Animation;
    .restart local v1    # "currentAlpha":F
    :cond_4
    move v4, v3

    .line 841
    goto :goto_2
.end method

.method private turnLightsOff()V
    .locals 2

    .prologue
    .line 729
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 730
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 732
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentLightsState:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    sget-object v1, Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;->OFF:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentLightsState:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    sget-object v1, Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;->TRANSITIONING_DOWN:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    if-ne v0, v1, :cond_1

    .line 738
    :cond_0
    :goto_0
    return-void

    .line 736
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->startLightsAnimation(Z)V

    goto :goto_0
.end method

.method private unlockNowPlayingScreen()V
    .locals 2

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueAdapter:Lcom/google/android/music/ui/QueueTrackListAdapter;

    if-eqz v0, :cond_0

    .line 1316
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueAdapter:Lcom/google/android/music/ui/QueueTrackListAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/QueueTrackListAdapter;->setEnabled(Z)V

    .line 1318
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueuePlayingFromHeaderView:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    if-eqz v0, :cond_1

    .line 1319
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueuePlayingFromHeaderView:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->unlockQueueHeader()V

    .line 1321
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLockNowPlayingScreen:Z

    .line 1322
    return-void
.end method

.method private updateButtonsVisibility()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 715
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->shouldHideShuffleAndRepeat()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 716
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mShuffleButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 717
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mShuffleButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 718
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRepeatButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 719
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRepeatButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 726
    :goto_0
    return-void

    .line 721
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mShuffleButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 722
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mShuffleButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 723
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRepeatButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 724
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRepeatButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0
.end method

.method private updateQueueSwitcherState()V
    .locals 2

    .prologue
    .line 1490
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueSwitcher:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueShown:Z

    if-eqz v0, :cond_0

    const v0, 0x7f020138

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1492
    return-void

    .line 1490
    :cond_0
    const v0, 0x7f020137

    goto :goto_0
.end method

.method private updateTrackInfo()V
    .locals 3

    .prologue
    .line 1325
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v0, :cond_0

    .line 1331
    :goto_0
    return-void

    .line 1329
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.metachanged"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->populateExtras(Landroid/content/Intent;Lcom/google/android/music/playback/IMusicPlaybackService;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateTrackInfo(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private updateTrackInfo(Landroid/content/Intent;)V
    .locals 4
    .param p1, "sourceIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x5

    .line 1334
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1335
    .local v0, "numsg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1336
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1337
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1338
    return-void
.end method

.method private updateTrackInfoImpl(Landroid/content/Intent;)V
    .locals 30
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 1343
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    if-nez v4, :cond_0

    .line 1430
    :goto_0
    return-void

    .line 1345
    :cond_0
    const-string v4, "currentSongLoaded"

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1346
    new-instance v23, Lcom/google/android/music/download/ContentIdentifier;

    const-string v4, "id"

    const-wide/16 v26, -0x1

    move-object/from16 v0, p1

    move-wide/from16 v1, v26

    invoke-virtual {v0, v4, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v26

    invoke-static {}, Lcom/google/android/music/download/ContentIdentifier$Domain;->values()[Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v4

    const-string v8, "domain"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    aget-object v4, v4, v8

    move-object/from16 v0, v23

    move-wide/from16 v1, v26

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/download/ContentIdentifier;-><init>(JLcom/google/android/music/download/ContentIdentifier$Domain;)V

    .line 1351
    .local v23, "songId":Lcom/google/android/music/download/ContentIdentifier;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongBufferListener:Lcom/google/android/music/BufferProgressListener;

    const-string v8, "streaming"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    move-object/from16 v0, v23

    invoke-virtual {v4, v0, v8}, Lcom/google/android/music/BufferProgressListener;->updateCurrentSong(Lcom/google/android/music/download/ContentIdentifier;Z)V

    .line 1353
    const-string v4, "artist"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1354
    .local v6, "artistName":Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1355
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v8, 0x7f0b00c4

    invoke-virtual {v4, v8}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1358
    :cond_1
    const-string v4, "album"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1359
    .local v7, "albumName":Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1360
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v8, 0x7f0b00c5

    invoke-virtual {v4, v8}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1363
    :cond_2
    const-string v4, "albumId"

    const-wide/16 v26, -0x1

    move-object/from16 v0, p1

    move-wide/from16 v1, v26

    invoke-virtual {v0, v4, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v18

    .line 1365
    .local v18, "albumId":J
    const-string v4, "track"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1367
    .local v5, "trackName":Ljava/lang/String;
    const-string v4, "duration"

    const-wide/16 v26, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v26

    invoke-virtual {v0, v4, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v26

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mDuration:J

    .line 1368
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mDuration:J

    move-wide/from16 v26, v0

    const-wide/16 v28, 0x3e8

    div-long v26, v26, v28

    move-wide/from16 v0, v26

    invoke-static {v4, v0, v1}, Lcom/google/android/music/utils/MusicUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v20

    .line 1371
    .local v20, "finalTotalTime":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTotalTime:Landroid/widget/TextView;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1373
    const-string v4, "rating"

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setRatings(I)V

    .line 1376
    const-string v4, "externalAlbumArtUrl"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1378
    .local v9, "artUrl":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/google/android/music/preferences/MusicPreferences;->isYouTubeAvailable(Landroid/app/Activity;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1379
    const-string v4, "videoId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1383
    .local v10, "vid":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    if-eqz v4, :cond_3

    .line 1384
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual/range {v4 .. v10}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->updateView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    .line 1388
    :cond_3
    const-string v4, "currentContainerTypeValue"

    const/4 v8, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    .line 1390
    .local v17, "containerTypeDbValue":I
    const/4 v4, -0x1

    move/from16 v0, v17

    if-eq v0, v4, :cond_7

    .line 1391
    const-string v4, "currentContainerId"

    const-wide/16 v26, -0x1

    move-object/from16 v0, p1

    move-wide/from16 v1, v26

    invoke-virtual {v0, v4, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v12

    .line 1392
    .local v12, "id":J
    const-string v4, "currentContainerName"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1394
    .local v14, "name":Ljava/lang/String;
    const-string v4, "currentContainerExtId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1396
    .local v15, "extId":Ljava/lang/String;
    const-string v4, "currentContainerExtData"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1400
    .local v16, "extData":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/google/android/music/store/ContainerDescriptor$Type;->fromDBValue(I)Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v11

    invoke-static/range {v11 .. v16}, Lcom/google/android/music/store/ContainerDescriptor;->newUnvalidatedDescriptor(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentContainer:Lcom/google/android/music/store/ContainerDescriptor;

    .line 1407
    .end local v12    # "id":J
    .end local v14    # "name":Ljava/lang/String;
    .end local v15    # "extId":Ljava/lang/String;
    .end local v16    # "extData":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueuePlayingFromHeaderView:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    if-eqz v4, :cond_5

    .line 1408
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v24

    .line 1409
    .local v24, "state":Lcom/google/android/music/playback/PlaybackState;
    const/16 v21, 0x0

    .line 1410
    .local v21, "mixDescriptor":Lcom/google/android/music/mix/MixDescriptor;
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/playback/PlaybackState;->isInIniniteMixMode()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1412
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v4}, Lcom/google/android/music/playback/IMusicPlaybackService;->getMixState()Lcom/google/android/music/mix/MixGenerationState;

    move-result-object v22

    .line 1413
    .local v22, "mixState":Lcom/google/android/music/mix/MixGenerationState;
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/music/mix/MixGenerationState;->getMix()Lcom/google/android/music/mix/MixDescriptor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v21

    .line 1420
    .end local v22    # "mixState":Lcom/google/android/music/mix/MixGenerationState;
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueuePlayingFromHeaderView:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentContainer:Lcom/google/android/music/store/ContainerDescriptor;

    move-object/from16 v0, v21

    invoke-virtual {v4, v0, v8}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->update(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/store/ContainerDescriptor;)V

    .line 1423
    .end local v21    # "mixDescriptor":Lcom/google/android/music/mix/MixDescriptor;
    .end local v24    # "state":Lcom/google/android/music/playback/PlaybackState;
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRootView:Landroid/view/View;

    const v8, 0x7f0b0352

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/16 v25, 0x0

    aput-object v5, v11, v25

    const/16 v25, 0x1

    aput-object v6, v11, v25

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v11}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1426
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setCurrentPage()V

    goto/16 :goto_0

    .line 1381
    .end local v10    # "vid":Ljava/lang/String;
    .end local v17    # "containerTypeDbValue":I
    :cond_6
    const/4 v10, 0x0

    .restart local v10    # "vid":Ljava/lang/String;
    goto/16 :goto_1

    .line 1404
    .restart local v17    # "containerTypeDbValue":I
    :cond_7
    const-string v4, "NowPlayingFragment"

    const-string v8, "Failed to get container type"

    invoke-static {v4, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1428
    .end local v5    # "trackName":Ljava/lang/String;
    .end local v6    # "artistName":Ljava/lang/String;
    .end local v7    # "albumName":Ljava/lang/String;
    .end local v9    # "artUrl":Ljava/lang/String;
    .end local v10    # "vid":Ljava/lang/String;
    .end local v17    # "containerTypeDbValue":I
    .end local v18    # "albumId":J
    .end local v20    # "finalTotalTime":Ljava/lang/String;
    .end local v23    # "songId":Lcom/google/android/music/download/ContentIdentifier;
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongBufferListener:Lcom/google/android/music/BufferProgressListener;

    const/4 v8, 0x0

    const/4 v11, 0x0

    invoke-virtual {v4, v8, v11}, Lcom/google/android/music/BufferProgressListener;->updateCurrentSong(Lcom/google/android/music/download/ContentIdentifier;Z)V

    goto/16 :goto_0

    .line 1414
    .restart local v5    # "trackName":Ljava/lang/String;
    .restart local v6    # "artistName":Ljava/lang/String;
    .restart local v7    # "albumName":Ljava/lang/String;
    .restart local v9    # "artUrl":Ljava/lang/String;
    .restart local v10    # "vid":Ljava/lang/String;
    .restart local v17    # "containerTypeDbValue":I
    .restart local v18    # "albumId":J
    .restart local v20    # "finalTotalTime":Ljava/lang/String;
    .restart local v21    # "mixDescriptor":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v23    # "songId":Lcom/google/android/music/download/ContentIdentifier;
    .restart local v24    # "state":Lcom/google/android/music/playback/PlaybackState;
    :catch_0
    move-exception v4

    goto :goto_3
.end method


# virtual methods
.method public getFragmentMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 403
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 406
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;

    move-result-object v0

    .line 407
    .local v0, "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRootView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->setContent(Landroid/view/View;)V

    .line 409
    iget-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsTablet:Z

    if-eqz v1, :cond_1

    const v1, 0x7f0e01e0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->setViewIdForSizingCollapsedState(I)V

    .line 412
    if-eqz p1, :cond_0

    .line 413
    const-string v1, "expandingState"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->valueOf(Ljava/lang/String;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setCurrentState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    .line 414
    const-string v1, "queueShown"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueShown:Z

    .line 421
    :cond_0
    return-void

    .line 409
    :cond_1
    const v1, 0x7f0e01db

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    .line 1696
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v4, :cond_1

    .line 1731
    :cond_0
    :goto_0
    return-void

    .line 1699
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueSwitcher:Landroid/widget/ImageButton;

    if-ne p1, v4, :cond_3

    .line 1700
    iget-boolean v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueShown:Z

    if-nez v4, :cond_2

    move v4, v5

    :goto_1
    invoke-direct {p0, v4, v5}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->showQueue(ZZ)V

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 1701
    :cond_3
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mOverflowMenu:Landroid/widget/ImageButton;

    if-ne p1, v4, :cond_5

    .line 1702
    iget-boolean v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsRestrictedPlaybackMode:Z

    if-eqz v4, :cond_4

    .line 1704
    new-instance v2, Lcom/google/android/music/ui/ScreenMenuHandler;

    new-instance v4, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v4}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    sget-object v5, Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;->NOW_PLAYING_RESTRICTED:Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;

    invoke-direct {v2, p0, v4, v5}, Lcom/google/android/music/ui/ScreenMenuHandler;-><init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;)V

    .line 1706
    .local v2, "menuHandler":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler;
    invoke-interface {v2, p1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler;->showPopupMenu(Landroid/view/View;)V

    goto :goto_0

    .line 1707
    .end local v2    # "menuHandler":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler;
    :cond_4
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueAdapter:Lcom/google/android/music/ui/QueueTrackListAdapter;

    if-eqz v4, :cond_0

    .line 1709
    :try_start_0
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v4}, Lcom/google/android/music/playback/IMusicPlaybackService;->getQueuePosition()I

    move-result v3

    .line 1710
    .local v3, "position":I
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueAdapter:Lcom/google/android/music/ui/QueueTrackListAdapter;

    invoke-virtual {v4, v3}, Lcom/google/android/music/ui/QueueTrackListAdapter;->getDocument(I)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 1711
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    new-instance v2, Lcom/google/android/music/ui/ScreenMenuHandler;

    sget-object v4, Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;->NOW_PLAYING:Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;

    invoke-direct {v2, p0, v0, v4}, Lcom/google/android/music/ui/ScreenMenuHandler;-><init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;)V

    .line 1713
    .restart local v2    # "menuHandler":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler;
    invoke-interface {v2, p1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler;->showPopupMenu(Landroid/view/View;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1714
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v2    # "menuHandler":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler;
    .end local v3    # "position":I
    :catch_0
    move-exception v1

    .line 1715
    .local v1, "e":Landroid/os/RemoteException;
    const-string v4, "NowPlayingFragment"

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1718
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_5
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPauseButton:Lcom/google/android/music/PlayPauseButton;

    if-ne p1, v4, :cond_0

    .line 1720
    :try_start_1
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v4}, Lcom/google/android/music/playback/IMusicPlaybackService;->isPreparing()Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v4}, Lcom/google/android/music/playback/IMusicPlaybackService;->isStreaming()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1721
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v4}, Lcom/google/android/music/playback/IMusicPlaybackService;->stop()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1727
    :catch_1
    move-exception v1

    .line 1728
    .restart local v1    # "e":Landroid/os/RemoteException;
    const-string v4, "NowPlayingFragment"

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1722
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_6
    :try_start_2
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v4}, Lcom/google/android/music/playback/IMusicPlaybackService;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1723
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v4}, Lcom/google/android/music/playback/IMusicPlaybackService;->pause()V

    goto :goto_0

    .line 1725
    :cond_7
    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v4}, Lcom/google/android/music/playback/IMusicPlaybackService;->play()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1852
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1853
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueuePlayingFromHeaderView:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    if-eqz v0, :cond_0

    .line 1854
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueuePlayingFromHeaderView:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateBackground()V

    .line 1856
    :cond_0
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 4
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1735
    sget-boolean v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->DEBUG_QUEUE:Z

    if-eqz v0, :cond_0

    .line 1736
    const-string v0, "NowPlayingFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreateLoader: id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1738
    :cond_0
    if-nez p1, :cond_1

    .line 1739
    new-instance v0, Lcom/google/android/music/ui/MediaListCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    sget-object v3, Lcom/google/android/music/ui/TrackListAdapter;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/ui/MediaListCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 1741
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    sget-object v3, Lcom/google/android/music/ui/TrackListAdapter;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 357
    const v1, 0x7f04008d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRootView:Landroid/view/View;

    .line 359
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->initializeView()V

    .line 360
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->clearDisplay()V

    .line 361
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsTablet:Z

    .line 363
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->queueNextRefreshNow()V

    .line 364
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->refreshPlaybackControls()V

    .line 366
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPlaybackServiceConnection:Landroid/content/ServiceConnection;

    invoke-static {v1, v2}, Lcom/google/android/music/utils/MusicUtils;->bindToService(Landroid/app/Activity;Landroid/content/ServiceConnection;)Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPlaybackServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    .line 368
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 369
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 370
    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 371
    const-string v1, "com.android.music.asyncopencomplete"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 372
    const-string v1, "com.android.music.asyncopenstart"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 373
    const-string v1, "com.android.music.playbackfailed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 374
    const-string v1, "com.android.music.queuechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 375
    const-string v1, "com.google.android.music.mix.playbackmodechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 376
    const-string v1, "com.google.android.music.refreshcomplete"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 377
    const-string v1, "com.google.android.music.refreshfailed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 378
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mStatusListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 380
    new-instance v0, Landroid/content/IntentFilter;

    .end local v0    # "f":Landroid/content/IntentFilter;
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 381
    .restart local v0    # "f":Landroid/content/IntentFilter;
    const-string v1, "com.google.android.music.nowplaying.HEADER_CLICKED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 382
    const-string v1, "com.google.android.music.nowplaying.HEADER_ART_CLICKED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 383
    const-string v1, "com.google.android.music.OPEN_DRAWER"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 384
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mUIInteractionListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 387
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateButtonsVisibility()V

    .line 394
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/BaseFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1082
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1083
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1084
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1086
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mUIInteractionListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1087
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mStatusListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1088
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongBufferListener:Lcom/google/android/music/BufferProgressListener;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/google/android/music/BufferProgressListener;->updateCurrentSong(Lcom/google/android/music/download/ContentIdentifier;Z)V

    .line 1089
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPlaybackServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->unbindFromService(Lcom/google/android/music/utils/MusicUtils$ServiceToken;)V

    .line 1090
    iput-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 1092
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongBufferListener:Lcom/google/android/music/BufferProgressListener;

    invoke-virtual {v0}, Lcom/google/android/music/BufferProgressListener;->destroy()V

    .line 1093
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onDestroy()V

    .line 1094
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    if-eqz v0, :cond_0

    .line 549
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->onDestroyView()V

    .line 551
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v0, :cond_1

    .line 552
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onDestroy()V

    .line 554
    :cond_1
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onDestroyView()V

    .line 555
    return-void
.end method

.method public onDragEnded(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V
    .locals 4
    .param p1, "source"    # Lcom/google/android/music/widgets/ExpandingScrollView;
    .param p2, "endState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1679
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isEmptyScreenShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1680
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v3

    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-eq p2, v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/music/ui/BaseActivity;->enableSideDrawer(Z)V

    .line 1682
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsTablet:Z

    if-eqz v0, :cond_1

    .line 1683
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne p2, v0, :cond_3

    .line 1684
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->setIsClosed(Z)V

    .line 1685
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderPlaybackControlsWrapper:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1686
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateButtonsVisibility()V

    .line 1692
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 1680
    goto :goto_0

    .line 1687
    :cond_3
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne p2, v0, :cond_1

    .line 1688
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->setIsClosed(Z)V

    .line 1689
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderPlaybackControlsWrapper:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public onDragStarted(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V
    .locals 2
    .param p1, "source"    # Lcom/google/android/music/widgets/ExpandingScrollView;
    .param p2, "currentState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    const/4 v1, 0x0

    .line 1669
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/BaseActivity;->enableSideDrawer(Z)V

    .line 1670
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsTablet:Z

    if-eqz v0, :cond_0

    .line 1671
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->setIsClosed(Z)V

    .line 1672
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderPlaybackControlsWrapper:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1674
    :cond_0
    return-void
.end method

.method public onExpandingStateChanged(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V
    .locals 6
    .param p1, "source"    # Lcom/google/android/music/widgets/ExpandingScrollView;
    .param p2, "oldState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .param p3, "newState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    const/16 v5, 0xff

    const/16 v4, 0xe6

    const/4 v3, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1573
    invoke-direct {p0, p3}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setCurrentState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    .line 1574
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne p3, v0, :cond_5

    .line 1575
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPauseButton:Lcom/google/android/music/PlayPauseButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPauseButton:Lcom/google/android/music/PlayPauseButton;

    invoke-virtual {v0, v2}, Lcom/google/android/music/PlayPauseButton;->setVisibility(I)V

    .line 1576
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsTablet:Z

    if-eqz v0, :cond_1

    .line 1577
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->setIsClosed(Z)V

    .line 1578
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderPlaybackControlsWrapper:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1580
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mOverflowMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1581
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueSwitcher:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1582
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v0, :cond_2

    .line 1583
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v0, v3}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->setMediaRouteButtonVisibility(Z)V

    .line 1587
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPager:Lcom/google/android/music/widgets/LinkableViewPager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPager:Lcom/google/android/music/widgets/LinkableViewPager;

    invoke-virtual {v0}, Lcom/google/android/music/widgets/LinkableViewPager;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1588
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1608
    :cond_4
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.music.nowplaying.DRAWER_STATE_CHANGED_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 1610
    return-void

    .line 1589
    :cond_5
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne p3, v0, :cond_4

    .line 1590
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPauseButton:Lcom/google/android/music/PlayPauseButton;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPauseButton:Lcom/google/android/music/PlayPauseButton;

    invoke-virtual {v0, v1}, Lcom/google/android/music/PlayPauseButton;->setVisibility(I)V

    .line 1591
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsTablet:Z

    if-eqz v0, :cond_7

    .line 1592
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    invoke-virtual {v0, v3}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->setIsClosed(Z)V

    .line 1593
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderPlaybackControlsWrapper:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1595
    :cond_7
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mOverflowMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1596
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueSwitcher:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1597
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v0, :cond_8

    .line 1598
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->setMediaRouteButtonVisibility(Z)V

    .line 1602
    :cond_8
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPager:Lcom/google/android/music/widgets/LinkableViewPager;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderPager:Lcom/google/android/music/widgets/LinkableViewPager;

    invoke-virtual {v0}, Lcom/google/android/music/widgets/LinkableViewPager;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1603
    :cond_9
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mTabletHeaderView:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1605
    :cond_a
    invoke-direct {p0, v1, v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->showQueue(ZZ)V

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1748
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iput-object p2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;

    .line 1749
    sget-boolean v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->DEBUG_QUEUE:Z

    if-eqz v0, :cond_0

    .line 1750
    const-string v0, "NowPlayingFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadFinished: Cursor updated: count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1752
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setupViewPager()V

    .line 1753
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueAdapter:Lcom/google/android/music/ui/QueueTrackListAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/music/ui/QueueTrackListAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 1754
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 100
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const/4 v2, 0x0

    .line 1758
    sget-boolean v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->DEBUG_QUEUE:Z

    if-eqz v0, :cond_0

    .line 1759
    const-string v0, "NowPlayingFragment"

    const-string v1, "onLoaderReset"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1761
    :cond_0
    iput-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;

    .line 1762
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueAdapter:Lcom/google/android/music/ui/QueueTrackListAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/QueueTrackListAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 1763
    return-void
.end method

.method public onMoving(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;F)V
    .locals 6
    .param p1, "source"    # Lcom/google/android/music/widgets/ExpandingScrollView;
    .param p2, "baseState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .param p3, "ratio"    # F

    .prologue
    const/4 v3, 0x0

    .line 1626
    sget-object v1, Lcom/google/android/music/ui/NowPlayingScreenFragment$16;->$SwitchMap$com$google$android$music$widgets$ExpandingScrollView$ExpandingState:[I

    invoke-virtual {p2}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1665
    :goto_0
    return-void

    .line 1628
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/music/ui/BaseActivity;->setRootVisibility(I)V

    .line 1631
    float-to-double v2, p3

    const-wide v4, 0x3fe999999999999aL    # 0.8

    cmpl-double v1, v2, v4

    if-lez v1, :cond_1

    .line 1632
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->hideActionBar()V

    .line 1639
    :goto_1
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isHoneycombOrGreater()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1640
    const/high16 v1, 0x3f000000    # 0.5f

    invoke-static {p3, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1641
    .local v0, "alpha":F
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPager:Lcom/google/android/music/widgets/LinkableViewPager;

    invoke-virtual {v1, v0}, Lcom/google/android/music/widgets/LinkableViewPager;->setAlpha(F)V

    .line 1645
    .end local v0    # "alpha":F
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/widgets/ExpandingScrollView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v2, p3

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0

    .line 1634
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->showActionBar()V

    goto :goto_1

    .line 1648
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/BaseActivity;->setRootVisibility(I)V

    .line 1649
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v1, v2, :cond_2

    .line 1651
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->hideActionBar()V

    .line 1654
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mArtPager:Lcom/google/android/music/widgets/LinkableViewPager;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 1657
    invoke-virtual {p1}, Lcom/google/android/music/widgets/ExpandingScrollView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0

    .line 1660
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/music/ui/BaseActivity;->setRootVisibility(I)V

    .line 1662
    invoke-virtual {p1}, Lcom/google/android/music/widgets/ExpandingScrollView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0

    .line 1626
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 509
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onPause()V

    .line 512
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;

    move-result-object v0

    .line 513
    .local v0, "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    if-eqz v0, :cond_0

    .line 514
    invoke-virtual {v0, p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->removeListener(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;)Z

    .line 517
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v1, :cond_1

    .line 518
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onPause()V

    .line 520
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 454
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onResume()V

    .line 460
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->refreshNow()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->queueNextRefresh(J)V

    .line 464
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;

    move-result-object v0

    .line 465
    .local v0, "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    if-eqz v0, :cond_1

    .line 468
    :try_start_0
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->hasValidPlaylist()Z

    move-result v2

    if-nez v2, :cond_3

    .line 472
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/music/widgets/ExpandingScrollView;->setHidden(Z)V

    .line 473
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-direct {p0, v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setCurrentState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    .line 483
    :goto_0
    invoke-virtual {v0, p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->addListener(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 489
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v2, :cond_5

    .line 490
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v2}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onResume()V

    .line 502
    :cond_2
    :goto_2
    return-void

    .line 475
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v3, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v2, v3, :cond_4

    .line 478
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-direct {p0, v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->setCurrentState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    .line 480
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {v0, v2}, Lcom/google/android/music/widgets/ExpandingScrollView;->moveToExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    .line 481
    iget-boolean v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueShown:Z

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->showQueue(ZZ)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 484
    :catch_0
    move-exception v1

    .line 485
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "NowPlayingFragment"

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 491
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isMediaRouteSupportEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 496
    new-instance v2, Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/music/ui/mrp/MediaRouteManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    .line 497
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v2}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onCreate()V

    .line 498
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteButton:Landroid/support/v7/app/MediaRouteButton;

    invoke-virtual {v2, v3}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->bindMediaRouteButton(Landroid/support/v7/app/MediaRouteButton;)V

    .line 499
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v2}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onStart()V

    .line 500
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v2}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onResume()V

    goto :goto_2
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 559
    const-string v0, "expandingState"

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {v1}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    const-string v0, "queueShown"

    iget-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueShown:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 561
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 425
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onStart()V

    .line 430
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;

    move-result-object v0

    .line 431
    .local v0, "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    invoke-virtual {v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getContent()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    .line 432
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRootView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->setContent(Landroid/view/View;)V

    .line 433
    iget-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsTablet:Z

    if-eqz v1, :cond_2

    const v1, 0x7f0e01e0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->setViewIdForSizingCollapsedState(I)V

    .line 437
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsStarted:Z

    .line 438
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateTrackInfo()V

    .line 440
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mRootView:Landroid/view/View;

    new-instance v2, Lcom/google/android/music/ui/NowPlayingScreenFragment$5;

    invoke-direct {v2, p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment$5;-><init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 447
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v1, :cond_1

    .line 448
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onStart()V

    .line 450
    :cond_1
    return-void

    .line 433
    :cond_2
    const v1, 0x7f0e01db

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 524
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onStop()V

    .line 528
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mIsStarted:Z

    .line 530
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;

    move-result-object v0

    .line 531
    .local v0, "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    if-eqz v0, :cond_0

    .line 532
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->setContent(Landroid/view/View;)V

    .line 536
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->clearDisplay()V

    .line 538
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v1, :cond_1

    .line 539
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onStop()V

    .line 541
    :cond_1
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 923
    const/4 v0, 0x0

    return v0
.end method
