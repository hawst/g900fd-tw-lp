.class public Lcom/google/android/music/ui/PlayCardViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "PlayCardViewHolder.java"


# instance fields
.field public document:Lcom/google/android/music/ui/cardlib/model/Document;

.field public playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 20
    const v0, 0x7f0e0104

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    iput-object v0, p0, Lcom/google/android/music/ui/PlayCardViewHolder;->playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 21
    iget-object v0, p0, Lcom/google/android/music/ui/PlayCardViewHolder;->playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    invoke-virtual {v0, p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setTag(Ljava/lang/Object;)V

    .line 22
    return-void
.end method
