.class public Lcom/google/android/music/medialist/SharedSingleSongList;
.super Lcom/google/android/music/medialist/SharedSongList;
.source "SharedSingleSongList.java"


# static fields
.field private static final LOGV:Z

.field private static final LOG_PROJ:Z

.field private static final TAG:Ljava/lang/String; = "SharedSingleSongList"


# instance fields
.field private mAlbumArtUrl:Ljava/lang/String;

.field private mSong:Lcom/google/android/music/medialist/SongData;

.field private mStoreUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->MEDIA_LIST:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/medialist/SharedSingleSongList;->LOGV:Z

    .line 20
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/medialist/SharedSingleSongList;->LOG_PROJ:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/medialist/SongData;)V
    .locals 1
    .param p1, "albumArtUrl"    # Ljava/lang/String;
    .param p2, "storeUrl"    # Ljava/lang/String;
    .param p3, "song"    # Lcom/google/android/music/medialist/SongData;

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->SHARED:Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-direct {p0, v0}, Lcom/google/android/music/medialist/SharedSongList;-><init>(Lcom/google/android/music/download/ContentIdentifier$Domain;)V

    .line 28
    iput-object p1, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mAlbumArtUrl:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mStoreUrl:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mSong:Lcom/google/android/music/medialist/SongData;

    .line 31
    return-void
.end method


# virtual methods
.method public createSyncCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "proj"    # [Ljava/lang/String;
    .param p3, "filter"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 36
    array-length v4, p2

    new-array v1, v4, [Ljava/lang/String;

    .line 38
    .local v1, "cursorCols":[Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mSong:Lcom/google/android/music/medialist/SongData;

    if-nez v4, :cond_0

    .line 58
    :goto_0
    return-object v0

    .line 42
    :cond_0
    sget-boolean v4, Lcom/google/android/music/medialist/SharedSingleSongList;->LOG_PROJ:Z

    if-eqz v4, :cond_1

    .line 43
    invoke-virtual {p0, p2}, Lcom/google/android/music/medialist/SharedSingleSongList;->logProjection([Ljava/lang/String;)V

    .line 46
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v4, p2

    if-ge v2, v4, :cond_2

    .line 47
    aget-object v4, p2, v2

    aput-object v4, v1, v2

    .line 46
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 50
    :cond_2
    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mSong:Lcom/google/android/music/medialist/SongData;

    invoke-virtual {p0, v4, v5, v1, v6}, Lcom/google/android/music/medialist/SharedSingleSongList;->createRow(J[Ljava/lang/String;Lcom/google/android/music/medialist/SongData;)Ljava/util/ArrayList;

    move-result-object v3

    .line 51
    .local v3, "row":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-nez v3, :cond_3

    .line 52
    const-string v4, "SharedSingleSongList"

    const-string v5, "Failed to creat row for the cursor"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 56
    :cond_3
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 57
    .local v0, "c":Landroid/database/MatrixCursor;
    invoke-virtual {v0, v3}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    goto :goto_0
.end method

.method public getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mAlbumArtUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mAlbumArtUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mStoreUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mSong:Lcom/google/android/music/medialist/SongData;

    invoke-static {v2}, Lcom/google/android/music/medialist/SongData;->toJson(Lcom/google/android/music/medialist/SongData;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mSong:Lcom/google/android/music/medialist/SongData;

    if-nez v0, :cond_0

    .line 83
    const/4 v0, 0x0

    .line 85
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mSong:Lcom/google/android/music/medialist/SongData;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongData;->mAlbum:Ljava/lang/String;

    goto :goto_0
.end method

.method public getScreenTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mSong:Lcom/google/android/music/medialist/SongData;

    if-nez v0, :cond_0

    .line 75
    const/4 v0, 0x0

    .line 77
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mSong:Lcom/google/android/music/medialist/SongData;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongData;->mTitle:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSecondaryName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mSong:Lcom/google/android/music/medialist/SongData;

    if-nez v0, :cond_0

    .line 91
    const/4 v0, 0x0

    .line 93
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mSong:Lcom/google/android/music/medialist/SongData;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongData;->mArtist:Ljava/lang/String;

    goto :goto_0
.end method

.method public getStoreUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedSingleSongList;->mStoreUrl:Ljava/lang/String;

    return-object v0
.end method
