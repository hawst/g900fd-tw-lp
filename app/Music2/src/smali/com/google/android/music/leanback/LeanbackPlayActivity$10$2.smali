.class Lcom/google/android/music/leanback/LeanbackPlayActivity$10$2;
.super Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;
.source "LeanbackPlayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity$10;->getItem(Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity$10;

.field final synthetic val$adapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

.field final synthetic val$itemGetterListener:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity$10;Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;)V
    .locals 0

    .prologue
    .line 952
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$2;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity$10;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$2;->val$adapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$2;->val$itemGetterListener:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 6

    .prologue
    .line 955
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$2;->val$adapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    invoke-virtual {v1}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 956
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$2;->val$adapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 957
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$2;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity$10;

    iget-object v1, v1, Lcom/google/android/music/leanback/LeanbackPlayActivity$10;->val$leanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistId()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistMetajamId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$2;->val$itemGetterListener:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->setBackground(Landroid/content/Context;JLjava/lang/String;Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;)V
    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$2300(Landroid/content/Context;JLjava/lang/String;Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;)V

    .line 960
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_0
    return-void
.end method
