.class public abstract Lcom/google/android/music/download/AbstractSchedulingService;
.super Lcom/google/android/music/service/ForegroundService;
.source "AbstractSchedulingService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/AbstractSchedulingService$7;,
        Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;,
        Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;,
        Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final mBlacklistedIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

.field private final mCacheServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

.field private final mDeleteFilter:Lcom/google/android/music/download/cache/IDeleteFilter;

.field private final mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

.field private volatile mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;

.field private final mDownloadServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

.field private volatile mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private final mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

.field private final mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

.field private final mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

.field private mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field mRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/TrackDownloadRequest;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mStartId:I

.field private final mStartupReceiverClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/content/BroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mStartupReceiverEnabled:Z

.field private volatile mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

.field private final mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/google/android/music/download/DownloadRequest$Owner;Ljava/lang/Class;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "owner"    # Lcom/google/android/music/download/DownloadRequest$Owner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/music/download/DownloadRequest$Owner;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/content/BroadcastReceiver;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 747
    .local p3, "startupReceiverClass":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/content/BroadcastReceiver;>;"
    invoke-direct {p0}, Lcom/google/android/music/service/ForegroundService;-><init>()V

    .line 81
    sget-object v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->NOT_STARTED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    iput-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    .line 105
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    .line 109
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mBlacklistedIds:Ljava/util/Map;

    .line 114
    new-instance v0, Lcom/google/android/music/download/AbstractSchedulingService$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/AbstractSchedulingService$1;-><init>(Lcom/google/android/music/download/AbstractSchedulingService;)V

    iput-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mDownloadServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    .line 128
    new-instance v0, Lcom/google/android/music/download/AbstractSchedulingService$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/AbstractSchedulingService$2;-><init>(Lcom/google/android/music/download/AbstractSchedulingService;)V

    iput-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mCacheServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    .line 146
    new-instance v0, Lcom/google/android/music/download/AbstractSchedulingService$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/AbstractSchedulingService$3;-><init>(Lcom/google/android/music/download/AbstractSchedulingService;)V

    iput-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

    .line 158
    new-instance v0, Lcom/google/android/music/download/AbstractSchedulingService$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/AbstractSchedulingService$4;-><init>(Lcom/google/android/music/download/AbstractSchedulingService;)V

    iput-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mDeleteFilter:Lcom/google/android/music/download/cache/IDeleteFilter;

    .line 187
    new-instance v0, Lcom/google/android/music/download/AbstractSchedulingService$5;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/AbstractSchedulingService$5;-><init>(Lcom/google/android/music/download/AbstractSchedulingService;)V

    iput-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

    .line 196
    new-instance v0, Lcom/google/android/music/download/AbstractSchedulingService$6;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/AbstractSchedulingService$6;-><init>(Lcom/google/android/music/download/AbstractSchedulingService;)V

    iput-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 748
    iput-object p1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;

    .line 749
    new-instance v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;-><init>(Lcom/google/android/music/download/AbstractSchedulingService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    .line 750
    new-instance v0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

    invoke-direct {v0, v1}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;-><init>(Lcom/google/android/music/net/INetworkChangeListener;)V

    iput-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .line 751
    iput-object p3, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mStartupReceiverClass:Ljava/lang/Class;

    .line 752
    iput-object p2, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    .line 753
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/IDownloadQueueManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/music/download/AbstractSchedulingService;Lcom/google/android/music/download/IDownloadQueueManager;)Lcom/google/android/music/download/IDownloadQueueManager;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;
    .param p1, "x1"    # Lcom/google/android/music/download/IDownloadQueueManager;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/music/download/AbstractSchedulingService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/music/download/AbstractSchedulingService;->checkDependentServices()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mBlacklistedIds:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/music/download/AbstractSchedulingService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/music/download/AbstractSchedulingService;->enableStartupReceiver()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/music/download/AbstractSchedulingService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/music/download/AbstractSchedulingService;->disableStartupReceiver()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/IDownloadProgressListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/music/download/AbstractSchedulingService;Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;
    .param p1, "x1"    # Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/music/download/AbstractSchedulingService;->sendUpdateEnabledMessage(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/music/download/AbstractSchedulingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/music/download/AbstractSchedulingService;->isAllServicesConnected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/music/download/AbstractSchedulingService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;
    .param p1, "x1"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/music/download/AbstractSchedulingService;->sendFinalCheckInitCompletedMessage(I)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/net/NetworkMonitorServiceConnection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/DownloadRequest$Owner;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/cache/ICacheManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/music/download/AbstractSchedulingService;Lcom/google/android/music/download/cache/ICacheManager;)Lcom/google/android/music/download/cache/ICacheManager;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;
    .param p1, "x1"    # Lcom/google/android/music/download/cache/ICacheManager;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/cache/IDeleteFilter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mDeleteFilter:Lcom/google/android/music/download/cache/IDeleteFilter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/music/download/AbstractSchedulingService;Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;
    .param p1, "x1"    # Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/music/download/AbstractSchedulingService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/music/download/AbstractSchedulingService;->sendUpdateEnabledMessage()V

    return-void
.end method

.method static synthetic access$700()Z
    .locals 1

    .prologue
    .line 55
    sget-boolean v0, Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/music/download/AbstractSchedulingService;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;

    .prologue
    .line 55
    iget v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mStartId:I

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/music/download/AbstractSchedulingService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;
    .param p1, "x1"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mStartId:I

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/music/download/AbstractSchedulingService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/AbstractSchedulingService;
    .param p1, "x1"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/music/download/AbstractSchedulingService;->sendCheckInitCompletedMessage(I)V

    return-void
.end method

.method private checkDependentServices()V
    .locals 2

    .prologue
    .line 879
    sget-boolean v0, Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z

    if-eqz v0, :cond_0

    .line 880
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;

    const-string v1, "updateServiceState"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/download/AbstractSchedulingService;->isAllServicesConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 883
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->WAITING_FOR_SERVICES:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-ne v0, v1, :cond_1

    .line 884
    sget-object v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->INITIALIZED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    iput-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    .line 887
    :cond_1
    return-void
.end method

.method private disableStartupReceiver()V
    .locals 4

    .prologue
    .line 962
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mStartupReceiverClass:Ljava/lang/Class;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mStartupReceiverEnabled:Z

    if-nez v0, :cond_1

    .line 972
    :cond_0
    :goto_0
    return-void

    .line 965
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mStartupReceiverEnabled:Z

    .line 966
    sget-boolean v0, Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z

    if-eqz v0, :cond_2

    .line 967
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;

    const-string v1, "Disabled startup receiver"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/download/AbstractSchedulingService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mStartupReceiverClass:Ljava/lang/Class;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto :goto_0
.end method

.method private enableStartupReceiver()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 943
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mStartupReceiverClass:Ljava/lang/Class;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mStartupReceiverEnabled:Z

    if-eqz v0, :cond_1

    .line 953
    :cond_0
    :goto_0
    return-void

    .line 946
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mStartupReceiverEnabled:Z

    .line 947
    sget-boolean v0, Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z

    if-eqz v0, :cond_2

    .line 948
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;

    const-string v1, "Enabled startup receiver"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/download/AbstractSchedulingService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mStartupReceiverClass:Ljava/lang/Class;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto :goto_0
.end method

.method private isAllServicesConnected()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 890
    iget-object v4, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v4}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;

    move-result-object v0

    .line 891
    .local v0, "networkMonitor":Lcom/google/android/music/net/INetworkMonitor;
    iget-object v4, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    if-eqz v4, :cond_1

    if-eqz v0, :cond_1

    move v1, v2

    .line 892
    .local v1, "res":Z
    :goto_0
    sget-boolean v4, Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z

    if-eqz v4, :cond_0

    .line 893
    iget-object v4, p0, Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isAllServicesConnected: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    if-nez v1, :cond_0

    .line 895
    iget-object v5, p0, Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;

    const-string v6, "bindings: dm=%b cm=%b nm=%b"

    const/4 v4, 0x3

    new-array v7, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;

    if-eqz v4, :cond_2

    move v4, v2

    :goto_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v7, v3

    iget-object v4, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    if-eqz v4, :cond_3

    move v4, v2

    :goto_2
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v7, v2

    const/4 v4, 0x2

    if-eqz v0, :cond_4

    :goto_3
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v7, v4

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    :cond_0
    return v1

    .end local v1    # "res":Z
    :cond_1
    move v1, v3

    .line 891
    goto :goto_0

    .restart local v1    # "res":Z
    :cond_2
    move v4, v3

    .line 895
    goto :goto_1

    :cond_3
    move v4, v3

    goto :goto_2

    :cond_4
    move v2, v3

    goto :goto_3
.end method

.method private sendCheckInitCompletedMessage(I)V
    .locals 4
    .param p1, "startId"    # I

    .prologue
    const/4 v2, 0x4

    .line 848
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    invoke-virtual {v1, v2}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 849
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    invoke-virtual {v1, v2}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->removeMessages(I)V

    .line 850
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 851
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 852
    return-void
.end method

.method private sendFinalCheckInitCompletedMessage(I)V
    .locals 4
    .param p1, "startId"    # I

    .prologue
    const/4 v2, 0x5

    .line 841
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    invoke-virtual {v1, v2}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 842
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    invoke-virtual {v1, v2}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->removeMessages(I)V

    .line 843
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 844
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 845
    return-void
.end method

.method private sendUpdateEnabledMessage()V
    .locals 1

    .prologue
    .line 855
    invoke-virtual {p0}, Lcom/google/android/music/download/AbstractSchedulingService;->isDownloadingPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 856
    sget-object v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->DOWNLOAD_PAUSED:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    invoke-direct {p0, v0}, Lcom/google/android/music/download/AbstractSchedulingService;->sendUpdateEnabledMessage(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V

    .line 860
    :goto_0
    return-void

    .line 858
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/download/AbstractSchedulingService;->sendUpdateEnabledMessage(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V

    goto :goto_0
.end method

.method private sendUpdateEnabledMessage(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V
    .locals 3
    .param p1, "reason"    # Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    .prologue
    .line 873
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 874
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 875
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    invoke-virtual {v1, v0}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->sendMessage(Landroid/os/Message;)Z

    .line 876
    return-void
.end method


# virtual methods
.method protected deleteFromStorage(Lcom/google/android/music/download/TrackDownloadRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/google/android/music/download/TrackDownloadRequest;

    .prologue
    .line 927
    if-eqz p1, :cond_0

    .line 929
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    invoke-interface {v1, p1}, Lcom/google/android/music/download/cache/ICacheManager;->requestDelete(Lcom/google/android/music/download/DownloadRequest;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 934
    :cond_0
    :goto_0
    return-void

    .line 930
    :catch_0
    move-exception v0

    .line 931
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;

    const-string v2, "Failed to request delete"

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method protected getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getNextDownloads(Lcom/google/android/music/download/cache/ICacheManager;Ljava/util/Collection;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/download/cache/ICacheManager;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/TrackDownloadRequest;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/download/cache/OutOfSpaceException;
        }
    .end annotation
.end method

.method protected abstract getTotalDownloadSize()J
.end method

.method protected abstract isDownloadingPaused()Z
.end method

.method protected abstract notifyAllWorkFinished()V
.end method

.method protected abstract notifyDisabled(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V
.end method

.method protected abstract notifyDownloadCompleted(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/TrackDownloadProgress;)V
.end method

.method protected abstract notifyDownloadFailed(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/TrackDownloadProgress;)V
.end method

.method protected abstract notifyDownloadProgress(FLcom/google/android/music/download/TrackDownloadProgress;)V
.end method

.method protected abstract notifyDownloadStarting()V
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 812
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 757
    invoke-super {p0}, Lcom/google/android/music/service/ForegroundService;->onCreate()V

    .line 758
    sget-boolean v2, Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z

    if-eqz v2, :cond_0

    .line 759
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;

    const-string v3, "onCreate"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    :cond_0
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 763
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mDownloadServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/music/download/TrackDownloadQueueService;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, p0, v3, v1}, Lcom/google/android/music/utils/SafeServiceConnection;->bindService(Landroid/content/Context;Landroid/content/Intent;I)Z

    .line 766
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mCacheServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/music/download/cache/TrackCacheService;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, p0, v3, v1}, Lcom/google/android/music/utils/SafeServiceConnection;->bindService(Landroid/content/Context;Landroid/content/Intent;I)Z

    .line 769
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v2, p0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->bindToService(Landroid/content/Context;)V

    .line 771
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    iget-object v3, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v2, v3}, Lcom/google/android/music/preferences/MusicPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 773
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mStartupReceiverClass:Ljava/lang/Class;

    if-eqz v2, :cond_1

    .line 774
    invoke-virtual {p0}, Lcom/google/android/music/download/AbstractSchedulingService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mStartupReceiverClass:Ljava/lang/Class;

    invoke-direct {v3, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v0

    .line 776
    .local v0, "enabledSetting":I
    if-ne v0, v1, :cond_2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mStartupReceiverEnabled:Z

    .line 779
    .end local v0    # "enabledSetting":I
    :cond_1
    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->WAITING_FOR_SERVICES:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    iput-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    .line 780
    return-void

    .line 776
    .restart local v0    # "enabledSetting":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 784
    sget-boolean v1, Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z

    if-eqz v1, :cond_0

    .line 785
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;

    const-string v2, "onDestroy"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v1, v2}, Lcom/google/android/music/preferences/MusicPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 790
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    invoke-virtual {v1}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->quit()V

    .line 793
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    if-eqz v1, :cond_1

    .line 794
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mDeleteFilter:Lcom/google/android/music/download/cache/IDeleteFilter;

    invoke-interface {v1, v2}, Lcom/google/android/music/download/cache/ICacheManager;->unregisterDeleteFilter(Lcom/google/android/music/download/cache/IDeleteFilter;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 800
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mCacheServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    invoke-virtual {v1, p0}, Lcom/google/android/music/utils/SafeServiceConnection;->unbindService(Landroid/content/Context;)V

    .line 802
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mDownloadServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    invoke-virtual {v1, p0}, Lcom/google/android/music/utils/SafeServiceConnection;->unbindService(Landroid/content/Context;)V

    .line 804
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v1, p0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->unbindFromService(Landroid/content/Context;)V

    .line 806
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 807
    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->FINISHED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    iput-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    .line 808
    return-void

    .line 796
    :catch_0
    move-exception v0

    .line 797
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;

    const-string v2, "Failed to unregister delete filter"

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected postRunnable(Ljava/lang/Runnable;)Z
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 866
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    invoke-virtual {v0, p1}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->post(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method protected sendCancelDownloadsMessage(I)V
    .locals 3
    .param p1, "startId"    # I

    .prologue
    .line 828
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 829
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->removeMessages(I)V

    .line 830
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 831
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    invoke-virtual {v1, v0}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->sendMessage(Landroid/os/Message;)Z

    .line 832
    return-void
.end method

.method protected sendInitScheduleMessage(I)V
    .locals 3
    .param p1, "startId"    # I

    .prologue
    .line 816
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 817
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 818
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    invoke-virtual {v1, v0}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->sendMessage(Landroid/os/Message;)Z

    .line 819
    return-void
.end method

.method protected sendScheduleDownloadsMessage(I)V
    .locals 3
    .param p1, "startId"    # I

    .prologue
    .line 822
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 823
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 824
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    invoke-virtual {v1, v0}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->sendMessage(Landroid/os/Message;)Z

    .line 825
    return-void
.end method

.method protected sendUpdateProgressMessage(Lcom/google/android/music/download/TrackDownloadProgress;)V
    .locals 3
    .param p1, "progress"    # Lcom/google/android/music/download/TrackDownloadProgress;

    .prologue
    .line 835
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 836
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 837
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mWorker:Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;

    invoke-virtual {v1, v0}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->sendMessage(Landroid/os/Message;)Z

    .line 838
    return-void
.end method

.method protected storeInCache(Lcom/google/android/music/download/TrackDownloadRequest;Ljava/lang/String;JI)V
    .locals 7
    .param p1, "request"    # Lcom/google/android/music/download/TrackDownloadRequest;
    .param p2, "httpContentType"    # Ljava/lang/String;
    .param p3, "size"    # J
    .param p5, "streamFidelity"    # I

    .prologue
    .line 913
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/music/download/TrackDownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/download/cache/FileLocation;->getCacheType()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 915
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/google/android/music/download/cache/ICacheManager;->storeInCache(Lcom/google/android/music/download/DownloadRequest;Ljava/lang/String;JI)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 921
    :cond_0
    :goto_0
    return-void

    .line 918
    :catch_0
    move-exception v0

    .line 919
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;

    const-string v2, "Failed to store file in the cache"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
