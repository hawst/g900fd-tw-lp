.class public Lcom/google/android/music/widgets/ActionBarItem;
.super Landroid/widget/FrameLayout;
.source "ActionBarItem.java"


# instance fields
.field private final mBackgroundDrawable:Landroid/graphics/drawable/ColorDrawable;

.field private final mFocusBackground:Landroid/graphics/drawable/BitmapDrawable;

.field private final mMaxAlpha:I

.field private mMaxAlphaSet:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x0

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    iput-boolean v4, p0, Lcom/google/android/music/widgets/ActionBarItem;->mMaxAlphaSet:Z

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 38
    .local v1, "res":Landroid/content/res/Resources;
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const v3, 0x7f0c006d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/music/widgets/ActionBarItem;->mBackgroundDrawable:Landroid/graphics/drawable/ColorDrawable;

    .line 39
    const v2, 0x7f0201cc

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 40
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, v1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/google/android/music/widgets/ActionBarItem;->mFocusBackground:Landroid/graphics/drawable/BitmapDrawable;

    .line 41
    iget-object v2, p0, Lcom/google/android/music/widgets/ActionBarItem;->mBackgroundDrawable:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/ColorDrawable;->getAlpha()I

    move-result v2

    iput v2, p0, Lcom/google/android/music/widgets/ActionBarItem;->mMaxAlpha:I

    .line 42
    iget-object v2, p0, Lcom/google/android/music/widgets/ActionBarItem;->mBackgroundDrawable:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v2, v4}, Landroid/graphics/drawable/ColorDrawable;->setAlpha(I)V

    .line 43
    iget-object v2, p0, Lcom/google/android/music/widgets/ActionBarItem;->mBackgroundDrawable:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ActionBarItem;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 44
    return-void
.end method


# virtual methods
.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 92
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 93
    if-eqz p1, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/android/music/widgets/ActionBarItem;->mFocusBackground:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/ActionBarItem;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/widgets/ActionBarItem;->mBackgroundDrawable:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/ActionBarItem;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ActionBarItem;->clearAnimation()V

    .line 50
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 51
    .local v0, "action":I
    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    if-ne v0, v3, :cond_2

    .line 52
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/widgets/ActionBarItem;->mBackgroundDrawable:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/ColorDrawable;->setAlpha(I)V

    .line 53
    iput-boolean v2, p0, Lcom/google/android/music/widgets/ActionBarItem;->mMaxAlphaSet:Z

    .line 62
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1

    .line 54
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/music/widgets/ActionBarItem;->mMaxAlphaSet:Z

    if-nez v1, :cond_1

    .line 55
    iput-boolean v3, p0, Lcom/google/android/music/widgets/ActionBarItem;->mMaxAlphaSet:Z

    .line 56
    iget-object v1, p0, Lcom/google/android/music/widgets/ActionBarItem;->mBackgroundDrawable:Landroid/graphics/drawable/ColorDrawable;

    iget v2, p0, Lcom/google/android/music/widgets/ActionBarItem;->mMaxAlpha:I

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/ColorDrawable;->setAlpha(I)V

    .line 57
    iget-object v1, p0, Lcom/google/android/music/widgets/ActionBarItem;->mBackgroundDrawable:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ActionBarItem;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ActionBarItem;->invalidate()V

    goto :goto_0
.end method
