.class public Lcom/google/android/music/animator/StatefulAlphaAnimation;
.super Landroid/view/animation/Animation;
.source "StatefulAlphaAnimation.java"


# instance fields
.field private mCurrentAlpha:F

.field private final mFromAlpha:F

.field private final mToAlpha:F


# direct methods
.method public constructor <init>(FF)V
    .locals 1
    .param p1, "fromAlpha"    # F
    .param p2, "toAlpha"    # F

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 13
    iput p1, p0, Lcom/google/android/music/animator/StatefulAlphaAnimation;->mFromAlpha:F

    .line 14
    iput p2, p0, Lcom/google/android/music/animator/StatefulAlphaAnimation;->mToAlpha:F

    .line 15
    iget v0, p0, Lcom/google/android/music/animator/StatefulAlphaAnimation;->mFromAlpha:F

    iput v0, p0, Lcom/google/android/music/animator/StatefulAlphaAnimation;->mCurrentAlpha:F

    .line 16
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    .line 20
    iget v0, p0, Lcom/google/android/music/animator/StatefulAlphaAnimation;->mFromAlpha:F

    iget v1, p0, Lcom/google/android/music/animator/StatefulAlphaAnimation;->mToAlpha:F

    iget v2, p0, Lcom/google/android/music/animator/StatefulAlphaAnimation;->mFromAlpha:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/animator/StatefulAlphaAnimation;->mCurrentAlpha:F

    .line 21
    iget v0, p0, Lcom/google/android/music/animator/StatefulAlphaAnimation;->mCurrentAlpha:F

    invoke-virtual {p2, v0}, Landroid/view/animation/Transformation;->setAlpha(F)V

    .line 22
    return-void
.end method

.method public getCurrentAlpha()F
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/google/android/music/animator/StatefulAlphaAnimation;->mCurrentAlpha:F

    return v0
.end method

.method public willChangeBounds()Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method public willChangeTransformationMatrix()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method
