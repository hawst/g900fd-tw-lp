.class Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;
.super Lcom/google/android/music/utils/LoggableHandler;
.source "AsyncAlbumArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AlbumArtHandler"
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1977
    const-string v0, "AsyncAlbumArtDrawable"

    invoke-direct {p0, v0}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    .line 1974
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;->mHandler:Landroid/os/Handler;

    .line 1978
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1982
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1987
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1984
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;->renderBitmap(Lcom/google/android/music/AsyncAlbumArtImageView;)V

    .line 1989
    return-void

    .line 1982
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method renderBitmap(Lcom/google/android/music/AsyncAlbumArtImageView;)V
    .locals 21
    .param p1, "imageView"    # Lcom/google/android/music/AsyncAlbumArtImageView;

    .prologue
    .line 1996
    monitor-enter p1

    .line 1997
    :try_start_0
    # getter for: Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$900(Lcom/google/android/music/AsyncAlbumArtImageView;)Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    move-result-object v19

    .line 1998
    .local v19, "viewMode":Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    if-nez v19, :cond_0

    .line 1999
    monitor-exit p1

    .line 2042
    :goto_0
    return-void

    .line 2001
    :cond_0
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;->copy()Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    move-result-object v2

    .line 2002
    .local v2, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/AsyncAlbumArtImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2003
    .local v3, "context":Landroid/content/Context;
    # invokes: Lcom/google/android/music/AsyncAlbumArtImageView;->getAlbumWidth()I
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$1000(Lcom/google/android/music/AsyncAlbumArtImageView;)I

    move-result v4

    .line 2004
    .local v4, "width":I
    # invokes: Lcom/google/android/music/AsyncAlbumArtImageView;->getAlbumHeight()I
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$1100(Lcom/google/android/music/AsyncAlbumArtImageView;)I

    move-result v5

    .line 2008
    .local v5, "height":I
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2010
    new-instance v6, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;

    invoke-direct {v6}, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;-><init>()V

    .line 2011
    .local v6, "missingAlbumArt":Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;
    new-instance v7, Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;

    invoke-direct {v7}, Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;-><init>()V

    .line 2012
    .local v7, "missingUrls":Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;
    invoke-virtual/range {v2 .. v7}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;->createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 2015
    .local v13, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v6}, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;->extractIds()Ljava/util/Set;

    move-result-object v16

    .line 2016
    .local v16, "albumIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    if-eqz v16, :cond_2

    .line 2017
    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 2018
    .local v14, "albumId":J
    invoke-static {v3, v14, v15}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumArtLocation(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v18

    .line 2020
    .local v18, "remoteArtUrl":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 2021
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;->report(Ljava/lang/Object;)V

    goto :goto_1

    .line 2008
    .end local v2    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .end local v3    # "context":Landroid/content/Context;
    .end local v4    # "width":I
    .end local v5    # "height":I
    .end local v6    # "missingAlbumArt":Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;
    .end local v7    # "missingUrls":Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;
    .end local v13    # "bitmap":Landroid/graphics/Bitmap;
    .end local v14    # "albumId":J
    .end local v16    # "albumIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v18    # "remoteArtUrl":Ljava/lang/String;
    .end local v19    # "viewMode":Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    :catchall_0
    move-exception v8

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    .line 2026
    .restart local v2    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .restart local v3    # "context":Landroid/content/Context;
    .restart local v4    # "width":I
    .restart local v5    # "height":I
    .restart local v6    # "missingAlbumArt":Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;
    .restart local v7    # "missingUrls":Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;
    .restart local v13    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v16    # "albumIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .restart local v19    # "viewMode":Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;->mHandler:Landroid/os/Handler;

    move-object/from16 v20, v0

    new-instance v8, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object v11, v2

    move-object v12, v7

    invoke-direct/range {v8 .. v13}, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;Lcom/google/android/music/AsyncAlbumArtImageView;Lcom/google/android/music/AsyncAlbumArtImageView$Mode;Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;Landroid/graphics/Bitmap;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
