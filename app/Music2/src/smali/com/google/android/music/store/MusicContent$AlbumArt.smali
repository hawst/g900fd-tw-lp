.class public final Lcom/google/android/music/store/MusicContent$AlbumArt;
.super Ljava/lang/Object;
.source "MusicContent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/MusicContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AlbumArt"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2250
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "albumart"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent$AlbumArt;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public static getAlbumArtUri(JZII)Landroid/net/Uri;
    .locals 4
    .param p0, "albumId"    # J
    .param p2, "allowFaux"    # Z
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 2321
    if-eqz p2, :cond_0

    .line 2322
    sget-object v1, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "albumorfauxart"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2325
    .local v0, "builder":Landroid/net/Uri$Builder;
    # invokes: Lcom/google/android/music/store/MusicContent;->appendWidthAndHeightIfAvailable(Landroid/net/Uri$Builder;II)V
    invoke-static {v0, p3, p4}, Lcom/google/android/music/store/MusicContent;->access$000(Landroid/net/Uri$Builder;II)V

    .line 2326
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 2330
    :goto_0
    return-object v1

    .line 2328
    .end local v0    # "builder":Landroid/net/Uri$Builder;
    :cond_0
    sget-object v1, Lcom/google/android/music/store/MusicContent$AlbumArt;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2329
    .restart local v0    # "builder":Landroid/net/Uri$Builder;
    # invokes: Lcom/google/android/music/store/MusicContent;->appendWidthAndHeightIfAvailable(Landroid/net/Uri$Builder;II)V
    invoke-static {v0, p3, p4}, Lcom/google/android/music/store/MusicContent;->access$000(Landroid/net/Uri$Builder;II)V

    .line 2330
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public static getFauxAlbumArtUri(JII)Landroid/net/Uri;
    .locals 4
    .param p0, "albumId"    # J
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 2313
    sget-object v1, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "albumfauxart"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2316
    .local v0, "builder":Landroid/net/Uri$Builder;
    # invokes: Lcom/google/android/music/store/MusicContent;->appendWidthAndHeightIfAvailable(Landroid/net/Uri$Builder;II)V
    invoke-static {v0, p2, p3}, Lcom/google/android/music/store/MusicContent;->access$000(Landroid/net/Uri$Builder;II)V

    .line 2317
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static getMediaStoreAlbumArt(J)Landroid/net/Uri;
    .locals 2
    .param p0, "mediaAlbumId"    # J

    .prologue
    .line 2304
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://media/external/audio/albumart/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getRemoteArtLocationForAlbum(Landroid/content/Context;J)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2341
    sget-object v0, Lcom/google/android/music/store/MusicContent$AlbumArt;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    .line 2342
    .local v6, "builder":Landroid/net/Uri$Builder;
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "AlbumArtLocation"

    aput-object v0, v2, v4

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2345
    .local v7, "c":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2346
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 2349
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 2351
    :goto_0
    return-object v3

    .line 2349
    :cond_0
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static openFileDescriptor(Landroid/content/Context;JII)Landroid/os/ParcelFileDescriptor;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J
    .param p3, "width"    # I
    .param p4, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 2272
    sget-object v4, Lcom/google/android/music/art/ArtLoader$DownloadMode;->ASYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    move-object v1, p0

    move-wide v2, p1

    move v5, p3

    move v6, p4

    invoke-static/range {v1 .. v6}, Lcom/google/android/music/store/MusicContent$AlbumArt;->openFileDescriptor(Landroid/content/Context;JLcom/google/android/music/art/ArtLoader$DownloadMode;II)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public static openFileDescriptor(Landroid/content/Context;JLcom/google/android/music/art/ArtLoader$DownloadMode;II)Landroid/os/ParcelFileDescriptor;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J
    .param p3, "mode"    # Lcom/google/android/music/art/ArtLoader$DownloadMode;
    .param p4, "width"    # I
    .param p5, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 2278
    sget-object v3, Lcom/google/android/music/store/MusicContent$AlbumArt;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2279
    .local v0, "builder":Landroid/net/Uri$Builder;
    # invokes: Lcom/google/android/music/store/MusicContent;->appendWidthAndHeightIfAvailable(Landroid/net/Uri$Builder;II)V
    invoke-static {v0, p4, p5}, Lcom/google/android/music/store/MusicContent;->access$000(Landroid/net/Uri$Builder;II)V

    .line 2280
    invoke-static {p3}, Lcom/google/android/music/store/MusicContent$UrlArt;->getModeString(Lcom/google/android/music/art/ArtLoader$DownloadMode;)Ljava/lang/String;

    move-result-object v2

    .line 2281
    .local v2, "modeString":Ljava/lang/String;
    const-string v3, "mode"

    invoke-virtual {v0, v3, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2283
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "r"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2288
    :goto_0
    return-object v3

    .line 2284
    :catch_0
    move-exception v1

    .line 2288
    .local v1, "e":Ljava/lang/NullPointerException;
    const/4 v3, 0x0

    goto :goto_0
.end method
