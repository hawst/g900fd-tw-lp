.class Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;
.super Ljava/lang/Object;
.source "ContainerHeaderView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/ContainerHeaderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ArtHelper"
.end annotation


# instance fields
.field private mRequestTime:J

.field private mRequestedUrl:Ljava/lang/String;

.field private final mViewId:I

.field private mViewReady:Z

.field final synthetic this$0:Lcom/google/android/music/ui/ContainerHeaderView;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ContainerHeaderView;I)V
    .locals 0
    .param p2, "viewId"    # I

    .prologue
    .line 867
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 868
    iput p2, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->mViewId:I

    .line 869
    return-void
.end method


# virtual methods
.method public getViewName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 872
    iget v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->mViewId:I

    const v1, 0x7f0e00e8

    if-ne v0, v1, :cond_0

    const-string v0, "view1"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "view2"

    goto :goto_0
.end method

.method public isViewReady()Z
    .locals 1

    .prologue
    .line 876
    iget-boolean v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->mViewReady:Z

    return v0
.end method

.method public onBitmapResult(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 7
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v6, 0x1

    .line 900
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->mRequestedUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 901
    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2200()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 902
    const-string v0, "ContainerHeader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ignoring stale result in onBitmapResult for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->getViewName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " requested: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->mRequestedUrl:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    :cond_0
    :goto_0
    return-void

    .line 909
    :cond_1
    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2200()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 910
    const-string v0, "ContainerHeader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBitmapResult for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->getViewName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " time: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->mRequestTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    :cond_2
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtClearedOnStop:Z
    invoke-static {v0}, Lcom/google/android/music/ui/ContainerHeaderView;->access$3000(Lcom/google/android/music/ui/ContainerHeaderView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 916
    iget v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->mViewId:I

    const v1, 0x7f0e00e8

    if-ne v0, v1, :cond_3

    .line 917
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1000(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 918
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # setter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1ShowingNonDefaultArt:Z
    invoke-static {v0, v6}, Lcom/google/android/music/ui/ContainerHeaderView;->access$3102(Lcom/google/android/music/ui/ContainerHeaderView;Z)Z

    .line 922
    :goto_1
    iput-boolean v6, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->mViewReady:Z

    goto :goto_0

    .line 920
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt2:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2500(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public requestBitmap(Ljava/lang/String;)V
    .locals 2
    .param p1, "remoteUrl"    # Ljava/lang/String;

    .prologue
    .line 880
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->mRequestedUrl:Ljava/lang/String;

    .line 881
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->mViewReady:Z

    .line 882
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->mRequestTime:J

    .line 884
    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->sBackgroundHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2900()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper$1;-><init>(Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 897
    return-void
.end method
