.class public abstract Lcom/google/android/music/BufferProgressListener;
.super Ljava/lang/Object;
.source "BufferProgressListener.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

.field protected mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

.field private mProgressBar:Landroid/widget/ProgressBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/BufferProgressListener;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/widget/ProgressBar;)V
    .locals 1
    .param p1, "progress"    # Landroid/widget/ProgressBar;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    new-instance v0, Lcom/google/android/music/BufferProgressListener$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/BufferProgressListener$1;-><init>(Lcom/google/android/music/BufferProgressListener;)V

    iput-object v0, p0, Lcom/google/android/music/BufferProgressListener;->mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

    .line 32
    iput-object p1, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/BufferProgressListener;Lcom/google/android/music/download/TrackDownloadProgress;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/BufferProgressListener;
    .param p1, "x1"    # Lcom/google/android/music/download/TrackDownloadProgress;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/music/BufferProgressListener;->processBufferBroadcast(Lcom/google/android/music/download/TrackDownloadProgress;)V

    return-void
.end method

.method private declared-synchronized connectReceiver(Lcom/google/android/music/download/ContentIdentifier;)V
    .locals 1
    .param p1, "trackId"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/music/BufferProgressListener;->connectListener(Lcom/google/android/music/download/ContentIdentifier;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    monitor-exit p0

    return-void

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized processBufferBroadcast(Lcom/google/android/music/download/TrackDownloadProgress;)V
    .locals 6
    .param p1, "downloadProgress"    # Lcom/google/android/music/download/TrackDownloadProgress;

    .prologue
    .line 36
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/BufferProgressListener;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/BufferProgressListener;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {p1}, Lcom/google/android/music/download/TrackDownloadProgress;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/download/ContentIdentifier;->equals(Lcom/google/android/music/download/ContentIdentifier;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 61
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 40
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_0

    .line 43
    invoke-virtual {p1}, Lcom/google/android/music/download/TrackDownloadProgress;->getState()Lcom/google/android/music/download/DownloadState$State;

    move-result-object v0

    .line 44
    .local v0, "downloadState":Lcom/google/android/music/download/DownloadState$State;
    sget-object v2, Lcom/google/android/music/download/DownloadState$State;->COMPLETED:Lcom/google/android/music/download/DownloadState$State;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/google/android/music/download/DownloadState$State;->FAILED:Lcom/google/android/music/download/DownloadState$State;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/google/android/music/download/DownloadState$State;->CANCELED:Lcom/google/android/music/download/DownloadState$State;

    if-ne v0, v2, :cond_3

    .line 47
    :cond_2
    invoke-direct {p0}, Lcom/google/android/music/BufferProgressListener;->stopReceiver()V

    .line 49
    sget-object v2, Lcom/google/android/music/download/DownloadState$State;->COMPLETED:Lcom/google/android/music/download/DownloadState$State;

    if-ne v0, v2, :cond_0

    .line 50
    iget-object v2, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v3, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getMax()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 36
    .end local v0    # "downloadState":Lcom/google/android/music/download/DownloadState$State;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 53
    .restart local v0    # "downloadState":Lcom/google/android/music/download/DownloadState$State;
    :cond_3
    :try_start_2
    invoke-virtual {p1}, Lcom/google/android/music/download/TrackDownloadProgress;->getCompletedBytes()J

    move-result-wide v2

    long-to-float v2, v2

    invoke-virtual {p1}, Lcom/google/android/music/download/TrackDownloadProgress;->getDownloadByteLength()J

    move-result-wide v4

    long-to-float v3, v4

    div-float v1, v2, v3

    .line 55
    .local v1, "progressRatio":F
    const v2, 0x3f7d70a4    # 0.99f

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_4

    .line 56
    iget-object v2, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v3, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getMax()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    goto :goto_0

    .line 58
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v3, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getMax()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v1

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized stopReceiver()V
    .locals 1

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/BufferProgressListener;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/google/android/music/BufferProgressListener;->disconnectListener()V

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/BufferProgressListener;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    :cond_0
    monitor-exit p0

    return-void

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected abstract connectListener(Lcom/google/android/music/download/ContentIdentifier;)V
.end method

.method public declared-synchronized destroy()V
    .locals 1

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/BufferProgressListener;->stopReceiver()V

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    monitor-exit p0

    return-void

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract disconnectListener()V
.end method

.method public declared-synchronized updateCurrentSong(Lcom/google/android/music/download/ContentIdentifier;Z)V
    .locals 5
    .param p1, "trackId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "isStreaming"    # Z

    .prologue
    .line 64
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 91
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 66
    :cond_1
    :try_start_1
    sget-boolean v2, Lcom/google/android/music/BufferProgressListener;->LOGV:Z

    if-eqz v2, :cond_2

    .line 67
    const-string v2, "BufferProgressListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateCurrentSong: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_2
    if-nez p1, :cond_3

    .line 70
    invoke-direct {p0}, Lcom/google/android/music/BufferProgressListener;->stopReceiver()V

    .line 71
    iget-object v2, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v3, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getMax()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 72
    :cond_3
    :try_start_2
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/BufferProgressListener;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {p1, v2}, Lcom/google/android/music/download/ContentIdentifier;->equals(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 73
    invoke-direct {p0}, Lcom/google/android/music/BufferProgressListener;->stopReceiver()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 74
    if-eqz p2, :cond_6

    .line 75
    const/4 v0, 0x0

    .line 77
    .local v0, "buffered":Z
    :try_start_3
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->isStreamingFullyBuffered()Z
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    .line 84
    :cond_4
    :goto_1
    :try_start_4
    iget-object v3, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getMax()I

    move-result v2

    :goto_2
    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 88
    .end local v0    # "buffered":Z
    :goto_3
    invoke-direct {p0, p1}, Lcom/google/android/music/BufferProgressListener;->connectReceiver(Lcom/google/android/music/download/ContentIdentifier;)V

    .line 89
    iput-object p1, p0, Lcom/google/android/music/BufferProgressListener;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    goto :goto_0

    .line 78
    .restart local v0    # "buffered":Z
    :catch_0
    move-exception v1

    .line 80
    .local v1, "e":Landroid/os/RemoteException;
    sget-boolean v2, Lcom/google/android/music/BufferProgressListener;->LOGV:Z

    if-eqz v2, :cond_4

    .line 81
    const-string v2, "BufferProgressListener"

    const-string v3, "Failed to check if the current song is fully buffered"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 84
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    .line 86
    .end local v0    # "buffered":Z
    :cond_6
    iget-object v2, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v3, p0, Lcom/google/android/music/BufferProgressListener;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getMax()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3
.end method
