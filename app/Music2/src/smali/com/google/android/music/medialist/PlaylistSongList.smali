.class public Lcom/google/android/music/medialist/PlaylistSongList;
.super Lcom/google/android/music/medialist/SongList;
.source "PlaylistSongList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "PlaylistSongList"


# instance fields
.field private final mArtUrl:Ljava/lang/String;

.field private mDescription:Ljava/lang/String;

.field private final mId:J

.field private mName:Ljava/lang/String;

.field private final mOwnerName:Ljava/lang/String;

.field private final mOwnerProfilePhotoUrl:Ljava/lang/String;

.field private final mPlaylistType:I

.field private final mShareToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "playlistId"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "playlistType"    # I
    .param p5, "description"    # Ljava/lang/String;
    .param p6, "ownerName"    # Ljava/lang/String;
    .param p7, "shareToken"    # Ljava/lang/String;
    .param p8, "artUrl"    # Ljava/lang/String;
    .param p9, "ownerProfilePhotoUrl"    # Ljava/lang/String;
    .param p10, "shouldFilter"    # Z

    .prologue
    .line 67
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p10, v1}, Lcom/google/android/music/medialist/SongList;-><init>(IZZ)V

    .line 69
    iput-wide p1, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    .line 70
    iput-object p3, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mName:Ljava/lang/String;

    .line 71
    iput p4, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mPlaylistType:I

    .line 72
    iput-object p5, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mDescription:Ljava/lang/String;

    .line 73
    iput-object p6, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mOwnerName:Ljava/lang/String;

    .line 74
    iput-object p7, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mShareToken:Ljava/lang/String;

    .line 75
    iput-object p8, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mArtUrl:Ljava/lang/String;

    .line 76
    iput-object p9, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    .line 77
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    .line 78
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid playlist id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/medialist/PlaylistSongList;->supportsShuffle()Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/music/medialist/PlaylistSongList;->clearFlag(I)V

    .line 84
    :cond_1
    return-void
.end method

.method public static doesPlaylistSupportKeepOn(I)Z
    .locals 1
    .param p0, "playlistType"    # I

    .prologue
    .line 328
    sparse-switch p0, :sswitch_data_0

    .line 338
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 336
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 328
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x46 -> :sswitch_0
        0x47 -> :sswitch_0
        0x50 -> :sswitch_0
        0x64 -> :sswitch_0
    .end sparse-switch
.end method

.method private final supportsShuffle()Z
    .locals 1

    .prologue
    .line 344
    iget v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mPlaylistType:I

    sparse-switch v0, :sswitch_data_0

    .line 352
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 350
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 344
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x50 -> :sswitch_0
        0x64 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public appendToPlaylist(Landroid/content/Context;J)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "playlistId"    # J

    .prologue
    .line 285
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-static {v0, p2, p3, v2, v3}, Lcom/google/android/music/store/MusicContent$Playlists;->appendPlaylistToPlayList(Landroid/content/ContentResolver;JJ)I

    move-result v0

    return v0
.end method

.method public containsLocalItems(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 399
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "hasLocal"

    aput-object v0, v2, v8

    .line 402
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 405
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 406
    :cond_0
    const-string v0, "PlaylistSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown playlist id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v8

    :goto_0
    return v0

    .line 409
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    move v0, v7

    .line 412
    :goto_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    move v0, v8

    .line 409
    goto :goto_1

    .line 412
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public containsRemoteItems(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 380
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "hasRemote"

    aput-object v0, v2, v8

    .line 383
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 386
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 387
    :cond_0
    const-string v0, "PlaylistSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown playlist id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 393
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v8

    :goto_0
    return v0

    .line 390
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-ne v0, v7, :cond_2

    move v0, v7

    .line 393
    :goto_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    move v0, v8

    .line 390
    goto :goto_1

    .line 393
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method protected createMediaCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/medialist/MediaList$MediaCursor;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 210
    new-instance v0, Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;

    iget-wide v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-direct {v0, p1, p2, v2, v3}, Lcom/google/android/music/medialist/PlaylistSongList$PlaylistCursor;-><init>(Landroid/content/Context;Landroid/database/Cursor;J)V

    return-object v0
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 107
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mPlaylistType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mDescription:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mOwnerName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mShareToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mArtUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mShouldFilter:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getArtUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mArtUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 121
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/PlaylistSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "name":Ljava/lang/String;
    iget-wide v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-static {v2, v3, v0}, Lcom/google/android/music/store/ContainerDescriptor;->newPlaylistDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v1

    return-object v1
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 205
    iget-wide v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Playlists$Members;->getPlaylistItemsUri(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 143
    iget-object v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mDescription:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 144
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "playlist_description"

    aput-object v0, v2, v1

    .line 147
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 151
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mDescription:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 158
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mDescription:Ljava/lang/String;

    return-object v0

    .line 155
    .restart local v2    # "cols":[Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getDownloadedSongCount(Landroid/content/Context;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 437
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "keeponDownloadedSongCount"

    aput-object v0, v2, v1

    .line 440
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 443
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 444
    :cond_0
    const-string v0, "PlaylistSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown playlist id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445
    const/4 v0, -0x1

    .line 450
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return v0

    .line 447
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 450
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 319
    iget-wide v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    return-wide v0
.end method

.method public getImage(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "allowAlias"    # Z

    .prologue
    .line 198
    iget v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mPlaylistType:I

    invoke-static {v0}, Lcom/google/android/music/utils/AlbumArtUtils;->playlistTypeToArtStyle(I)I

    move-result v2

    .line 199
    .local v2, "style":I
    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    iget-object v8, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mName:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v1, p1

    move v6, p2

    move/from16 v7, p3

    move-object/from16 v11, p4

    move/from16 v12, p5

    invoke-static/range {v1 .. v12}, Lcom/google/android/music/utils/AlbumArtUtils;->getFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getKeepOnSongCount(Landroid/content/Context;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 456
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "keeponSongCount"

    aput-object v0, v2, v1

    .line 459
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 462
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 463
    :cond_0
    const-string v0, "PlaylistSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown playlist id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    const/4 v0, -0x1

    .line 469
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return v0

    .line 466
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 469
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mName:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/PlaylistSongList;->refreshMetaData(Landroid/content/Context;)V

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mName:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mPlaylistType:I

    invoke-static {p1, v0, v1}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistPrimaryLabel(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOwnerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mOwnerName:Ljava/lang/String;

    return-object v0
.end method

.method public getOwnerProfilePhotoUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaylistId()J
    .locals 2

    .prologue
    .line 126
    iget-wide v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    return-wide v0
.end method

.method public getPlaylistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaylistType()I
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mPlaylistType:I

    return v0
.end method

.method public getSecondaryName(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 179
    iget v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mPlaylistType:I

    const/16 v3, 0x47

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mOwnerName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 182
    iget-object v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mOwnerName:Ljava/lang/String;

    .line 190
    :goto_0
    return-object v2

    .line 184
    :cond_0
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 185
    .local v1, "prefsHolder":Ljava/lang/Object;
    invoke-static {p1, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 188
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    iget v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mPlaylistType:I

    invoke-static {p1, v0, v2}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistSecondaryLabel(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 190
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method public getShareToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mShareToken:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestedPositionSearchRadius()I
    .locals 1

    .prologue
    .line 485
    const/16 v0, 0x3e8

    return v0
.end method

.method public getValidSortOrders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasArtistArt()Z
    .locals 1

    .prologue
    .line 501
    const/4 v0, 0x1

    return v0
.end method

.method public hasMetaData()Z
    .locals 1

    .prologue
    .line 309
    const/4 v0, 0x1

    return v0
.end method

.method public hasStablePrimaryIds()Z
    .locals 1

    .prologue
    .line 496
    const/4 v0, 0x0

    return v0
.end method

.method public hasUniqueAudioId()Z
    .locals 1

    .prologue
    .line 490
    const/4 v0, 0x0

    return v0
.end method

.method public isAllLocal(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 418
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "isAllLocal"

    aput-object v0, v2, v8

    .line 421
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 424
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 425
    :cond_0
    const-string v0, "PlaylistSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown playlist id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 431
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v8

    :goto_0
    return v0

    .line 428
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    move v0, v7

    .line 431
    :goto_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    move v0, v8

    .line 428
    goto :goto_1

    .line 431
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public isEditable()Z
    .locals 2

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mPlaylistType:I

    const/16 v1, 0x32

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mPlaylistType:I

    const/16 v1, 0x47

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mPlaylistType:I

    const/16 v1, 0x46

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mPlaylistType:I

    const/16 v1, 0x50

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/AvailableSpaceTracker;Lcom/google/android/music/store/IStoreService;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "offlineMusicManager"    # Lcom/google/android/music/AvailableSpaceTracker;
    .param p3, "storeService"    # Lcom/google/android/music/store/IStoreService;

    .prologue
    .line 369
    iget-wide v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-virtual {p2, v2, v3}, Lcom/google/android/music/AvailableSpaceTracker;->isPlaylistSelected(J)Ljava/lang/Boolean;

    move-result-object v0

    .line 370
    .local v0, "result":Ljava/lang/Boolean;
    if-nez v0, :cond_0

    .line 373
    invoke-virtual {p0, p1, p3}, Lcom/google/android/music/medialist/PlaylistSongList;->isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/store/IStoreService;)Z

    move-result v1

    .line 375
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method public isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/store/IStoreService;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "storeService"    # Lcom/google/android/music/store/IStoreService;

    .prologue
    .line 359
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-interface {p2, v2, v3}, Lcom/google/android/music/store/IStoreService;->isPlaylistSelectedAsKeepOn(J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 362
    :goto_0
    return v1

    .line 360
    :catch_0
    move-exception v0

    .line 361
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "PlaylistSongList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error trying to get offline status for playlist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 362
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public refreshMetaData(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 291
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "playlist_name"

    aput-object v0, v2, v7

    const-string v0, "playlist_description"

    aput-object v0, v2, v8

    .line 295
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 297
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 298
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 299
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 300
    invoke-interface {v6, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    :goto_0
    iput-object v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mName:Ljava/lang/String;

    .line 301
    invoke-interface {v6, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, ""

    :goto_1
    iput-object v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mDescription:Ljava/lang/String;

    .line 303
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 305
    :cond_1
    return-void

    .line 300
    :cond_2
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 301
    :cond_3
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public registerMetaDataObserver(Landroid/content/Context;Landroid/database/ContentObserver;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 314
    iget-wide v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 315
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, p2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 316
    return-void
.end method

.method public storeDefaultSortOrder(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "not supported for PlaylistSongList"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public supportsAppendToPlaylist()Z
    .locals 1

    .prologue
    .line 272
    iget v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mPlaylistType:I

    sparse-switch v0, :sswitch_data_0

    .line 279
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 277
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 272
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0xa -> :sswitch_0
        0x47 -> :sswitch_0
    .end sparse-switch
.end method

.method protected supportsOfflineCaching()Z
    .locals 1

    .prologue
    .line 324
    iget v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mPlaylistType:I

    invoke-static {v0}, Lcom/google/android/music/medialist/PlaylistSongList;->doesPlaylistSupportKeepOn(I)Z

    move-result v0

    return v0
.end method

.method public toggleOfflineCaching(Landroid/content/Context;Lcom/google/android/music/activitymanagement/KeepOnManager;Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keepOnManager"    # Lcom/google/android/music/activitymanagement/KeepOnManager;
    .param p3, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p4, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;

    .prologue
    .line 479
    iget-wide v2, p0, Lcom/google/android/music/medialist/PlaylistSongList;->mId:J

    move-object v1, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/activitymanagement/KeepOnManager;->togglePlaylistKeepOn(JLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Landroid/content/Context;)V

    .line 480
    return-void
.end method
