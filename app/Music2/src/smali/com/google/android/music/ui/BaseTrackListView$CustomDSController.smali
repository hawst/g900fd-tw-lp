.class Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;
.super Lcom/mobeta/android/dslv/DragSortController;
.source "BaseTrackListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/BaseTrackListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CustomDSController"
.end annotation


# instance fields
.field private mGrabHandleWidth:I

.field private mLongPressReorderEnabled:Z

.field private mLongPressed:Z

.field private mUndoDialog:Lcom/google/android/music/ui/dialogs/UndoDialog;


# direct methods
.method public constructor <init>(Lcom/mobeta/android/dslv/DragSortListView;Lcom/google/android/music/ui/dialogs/UndoDialog;Landroid/content/Context;)V
    .locals 4
    .param p1, "dslv"    # Lcom/mobeta/android/dslv/DragSortListView;
    .param p2, "dialog"    # Lcom/google/android/music/ui/dialogs/UndoDialog;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 429
    invoke-direct {p0, p1}, Lcom/mobeta/android/dslv/DragSortController;-><init>(Lcom/mobeta/android/dslv/DragSortListView;)V

    .line 425
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->mLongPressed:Z

    .line 430
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 431
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0f00a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->mGrabHandleWidth:I

    .line 432
    const v1, 0x7f0c0080

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->setBackgroundColor(I)V

    .line 433
    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->setRemoveEnabled(Z)V

    .line 434
    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->setRemoveMode(I)V

    .line 436
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_enable_longpress_reorder"

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->mLongPressReorderEnabled:Z

    .line 439
    iput-object p2, p0, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->mUndoDialog:Lcom/google/android/music/ui/dialogs/UndoDialog;

    .line 440
    return-void
.end method


# virtual methods
.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 468
    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->mLongPressReorderEnabled:Z

    if-eqz v0, :cond_0

    .line 469
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->mLongPressed:Z

    .line 474
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->onDown(Landroid/view/MotionEvent;)Z

    .line 478
    :goto_0
    return-void

    .line 476
    :cond_0
    invoke-super {p0, p1}, Lcom/mobeta/android/dslv/DragSortController;->onLongPress(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 458
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 461
    :cond_0
    const-string v0, "BaseTrackListView"

    const-string v1, "onScroll called with null MotionEvents!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    const/4 v0, 0x0

    .line 464
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/mobeta/android/dslv/DragSortController;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto :goto_0
.end method

.method public startDragPosition(Landroid/view/MotionEvent;)I
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 444
    invoke-super {p0, p1}, Lcom/mobeta/android/dslv/DragSortController;->dragHandleHitPosition(Landroid/view/MotionEvent;)I

    move-result v0

    .line 445
    .local v0, "res":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iget v2, p0, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->mGrabHandleWidth:I

    if-lt v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->mLongPressed:Z

    if-eqz v1, :cond_2

    .line 446
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->mLongPressed:Z

    .line 447
    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->mUndoDialog:Lcom/google/android/music/ui/dialogs/UndoDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->mUndoDialog:Lcom/google/android/music/ui/dialogs/UndoDialog;

    invoke-virtual {v1}, Lcom/google/android/music/ui/dialogs/UndoDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 448
    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;->mUndoDialog:Lcom/google/android/music/ui/dialogs/UndoDialog;

    invoke-virtual {v1}, Lcom/google/android/music/ui/dialogs/UndoDialog;->dismiss()V

    .line 452
    .end local v0    # "res":I
    :cond_1
    :goto_0
    return v0

    .restart local v0    # "res":I
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method
