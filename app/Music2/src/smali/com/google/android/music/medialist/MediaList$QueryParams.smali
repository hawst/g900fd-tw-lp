.class Lcom/google/android/music/medialist/MediaList$QueryParams;
.super Ljava/lang/Object;
.source "MediaList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/medialist/MediaList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueryParams"
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field mFilter:Ljava/lang/String;

.field mProjection:[Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "filter"    # Ljava/lang/String;

    .prologue
    .line 699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 700
    iput-object p1, p0, Lcom/google/android/music/medialist/MediaList$QueryParams;->mContext:Landroid/content/Context;

    .line 701
    iput-object p2, p0, Lcom/google/android/music/medialist/MediaList$QueryParams;->mProjection:[Ljava/lang/String;

    .line 702
    iput-object p3, p0, Lcom/google/android/music/medialist/MediaList$QueryParams;->mFilter:Ljava/lang/String;

    .line 703
    return-void
.end method
