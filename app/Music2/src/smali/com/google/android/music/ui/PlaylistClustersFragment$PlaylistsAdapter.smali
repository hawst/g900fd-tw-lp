.class final Lcom/google/android/music/ui/PlaylistClustersFragment$PlaylistsAdapter;
.super Lcom/google/android/music/ui/MediaListCardAdapter;
.source "PlaylistClustersFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/PlaylistClustersFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PlaylistsAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/PlaylistClustersFragment;)V
    .locals 1
    .param p2, "fragment"    # Lcom/google/android/music/ui/PlaylistClustersFragment;

    .prologue
    .line 379
    iput-object p1, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$PlaylistsAdapter;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    .line 380
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/music/ui/MediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;I)V

    .line 381
    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/PlaylistClustersFragment;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "fragment"    # Lcom/google/android/music/ui/PlaylistClustersFragment;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 383
    iput-object p1, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$PlaylistsAdapter;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    .line 384
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v0

    invoke-direct {p0, p2, v0, p3}, Lcom/google/android/music/ui/MediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ILandroid/database/Cursor;)V

    .line 385
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/PlaylistClustersFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/PlaylistClustersFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/PlaylistClustersFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/PlaylistClustersFragment;
    .param p3, "x2"    # Landroid/database/Cursor;
    .param p4, "x3"    # Lcom/google/android/music/ui/PlaylistClustersFragment$1;

    .prologue
    .line 377
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/ui/PlaylistClustersFragment$PlaylistsAdapter;-><init>(Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/PlaylistClustersFragment;Landroid/database/Cursor;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/PlaylistClustersFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/PlaylistClustersFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/PlaylistClustersFragment;
    .param p3, "x2"    # Lcom/google/android/music/ui/PlaylistClustersFragment$1;

    .prologue
    .line 377
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/PlaylistClustersFragment$PlaylistsAdapter;-><init>(Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/PlaylistClustersFragment;)V

    return-void
.end method


# virtual methods
.method protected bindViewToLoadingItem(Landroid/view/View;Landroid/content/Context;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 411
    instance-of v1, p1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 412
    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 413
    .local v0, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bindLoading()V

    .line 415
    .end local v0    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_0
    return-void
.end method

.method protected bindViewToMediaListItem(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;J)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "itemId"    # J

    .prologue
    .line 400
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->getDocument(Landroid/view/View;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 401
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    # invokes: Lcom/google/android/music/ui/PlaylistClustersFragment;->populatePlaylistDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;Landroid/content/Context;)Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v0, p3, p2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->access$900(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;Landroid/content/Context;)Lcom/google/android/music/ui/cardlib/model/Document;

    .line 403
    instance-of v2, p1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 404
    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 405
    .local v1, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    iget-object v2, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$PlaylistsAdapter;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    iget-object v2, v2, Lcom/google/android/music/ui/PlaylistClustersFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 407
    .end local v1    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_0
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 389
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/MediaListCardAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 390
    .local v0, "view":Landroid/view/View;
    instance-of v1, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 391
    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    iget-object v2, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$PlaylistsAdapter;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    # getter for: Lcom/google/android/music/ui/PlaylistClustersFragment;->mCardType:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {v2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->access$800(Lcom/google/android/music/ui/PlaylistClustersFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 394
    :cond_0
    return-object v0
.end method
