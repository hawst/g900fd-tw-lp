.class Lcom/google/android/music/download/artwork/MissingArtHelper$1;
.super Ljava/lang/Object;
.source "MissingArtHelper.java"

# interfaces
.implements Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/artwork/MissingArtHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/artwork/MissingArtHelper;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/artwork/MissingArtHelper;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/music/download/artwork/MissingArtHelper$1;->this$0:Lcom/google/android/music/download/artwork/MissingArtHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceIdle()V
    .locals 2

    .prologue
    .line 34
    iget-object v1, p0, Lcom/google/android/music/download/artwork/MissingArtHelper$1;->this$0:Lcom/google/android/music/download/artwork/MissingArtHelper;

    # getter for: Lcom/google/android/music/download/artwork/MissingArtHelper;->mArtMonitor:Lcom/google/android/music/download/artwork/ArtMonitor;
    invoke-static {v1}, Lcom/google/android/music/download/artwork/MissingArtHelper;->access$000(Lcom/google/android/music/download/artwork/MissingArtHelper;)Lcom/google/android/music/download/artwork/ArtMonitor;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/download/artwork/ArtMonitor;->getWatchedArtUrls()Ljava/util/List;

    move-result-object v0

    .line 35
    .local v0, "artIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/music/download/artwork/MissingArtHelper$1;->this$0:Lcom/google/android/music/download/artwork/MissingArtHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/music/download/artwork/MissingArtHelper;->startArtDownloads(Ljava/util/List;)V

    .line 36
    return-void
.end method
