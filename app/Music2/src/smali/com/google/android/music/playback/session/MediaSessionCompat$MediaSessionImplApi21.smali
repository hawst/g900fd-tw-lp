.class Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;
.super Ljava/lang/Object;
.source "MediaSessionCompat.java"

# interfaces
.implements Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/session/MediaSessionCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MediaSessionImplApi21"
.end annotation


# instance fields
.field private mActive:Z

.field private final mSessionObj:Ljava/lang/Object;

.field private final mToken:Lcom/google/android/music/playback/session/MediaSessionCompat$Token;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "pendingIntent"    # Landroid/app/PendingIntent;

    .prologue
    .line 791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 792
    invoke-static {p1, p2}, Lcom/google/android/music/playback/session/MediaSessionCompatApi21;->createSession(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mSessionObj:Ljava/lang/Object;

    .line 793
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mSessionObj:Ljava/lang/Object;

    invoke-static {v0, p3}, Lcom/google/android/music/playback/session/MediaSessionCompatApi21;->setMediaButtonReceiver(Ljava/lang/Object;Landroid/app/PendingIntent;)V

    .line 794
    new-instance v0, Lcom/google/android/music/playback/session/MediaSessionCompat$Token;

    iget-object v1, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mSessionObj:Ljava/lang/Object;

    invoke-static {v1}, Lcom/google/android/music/playback/session/MediaSessionCompatApi21;->getSessionToken(Ljava/lang/Object;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompat$Token;-><init>(Landroid/os/Parcelable;)V

    iput-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mToken:Lcom/google/android/music/playback/session/MediaSessionCompat$Token;

    .line 795
    return-void
.end method


# virtual methods
.method public getMediaSession()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 874
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mSessionObj:Ljava/lang/Object;

    return-object v0
.end method

.method public getRemoteControlClient()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 879
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSessionToken()Lcom/google/android/music/playback/session/MediaSessionCompat$Token;
    .locals 1

    .prologue
    .line 843
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mToken:Lcom/google/android/music/playback/session/MediaSessionCompat$Token;

    return-object v0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 838
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mSessionObj:Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/music/playback/session/MediaSessionCompatApi21;->release(Ljava/lang/Object;)V

    .line 839
    return-void
.end method

.method public setActive(Z)V
    .locals 1
    .param p1, "active"    # Z

    .prologue
    .line 820
    iget-boolean v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mActive:Z

    if-nez v0, :cond_0

    .line 821
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mSessionObj:Ljava/lang/Object;

    invoke-static {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompatApi21;->setActive(Ljava/lang/Object;Z)V

    .line 822
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mActive:Z

    .line 824
    :cond_0
    return-void
.end method

.method public setCallback(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;Landroid/os/Handler;)V
    .locals 2
    .param p1, "callback"    # Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 799
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mSessionObj:Ljava/lang/Object;

    iget-object v1, p1, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->mCallbackObj:Ljava/lang/Object;

    invoke-static {v0, v1, p2}, Lcom/google/android/music/playback/session/MediaSessionCompatApi21;->setCallback(Ljava/lang/Object;Ljava/lang/Object;Landroid/os/Handler;)V

    .line 800
    return-void
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 863
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mSessionObj:Ljava/lang/Object;

    invoke-static {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompatApi21;->setExtras(Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 864
    return-void
.end method

.method public setFlags(I)V
    .locals 1
    .param p1, "flags"    # I

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mSessionObj:Ljava/lang/Object;

    invoke-static {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompatApi21;->setFlags(Ljava/lang/Object;I)V

    .line 805
    return-void
.end method

.method public setMetadata(Lcom/google/android/music/playback/session/MediaMetadataCompat;)V
    .locals 2
    .param p1, "metadata"    # Lcom/google/android/music/playback/session/MediaMetadataCompat;

    .prologue
    .line 868
    iget-object v1, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mSessionObj:Ljava/lang/Object;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/music/playback/session/MediaSessionCompatApi21;->setMetadata(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 870
    return-void

    .line 868
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getMediaMetadata()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public setPlaybackState(Lcom/google/android/music/playback/session/PlaybackStateCompat;)V
    .locals 2
    .param p1, "state"    # Lcom/google/android/music/playback/session/PlaybackStateCompat;

    .prologue
    .line 848
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mSessionObj:Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/music/playback/session/PlaybackStateCompat;->getPlaybackState()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompatApi21;->setPlaybackState(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 849
    return-void
.end method

.method public setQueue(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 853
    .local p1, "queue":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;>;"
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mSessionObj:Ljava/lang/Object;

    invoke-static {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompatApi21;->setQueue(Ljava/lang/Object;Ljava/util/List;)V

    .line 854
    return-void
.end method

.method public setQueueTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 858
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mSessionObj:Ljava/lang/Object;

    invoke-static {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompatApi21;->setQueueTitle(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 859
    return-void
.end method

.method public setSessionActivity(Landroid/app/PendingIntent;)V
    .locals 1
    .param p1, "pi"    # Landroid/app/PendingIntent;

    .prologue
    .line 883
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;->mSessionObj:Ljava/lang/Object;

    invoke-static {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompatApi21;->setSessionActivity(Ljava/lang/Object;Landroid/app/PendingIntent;)V

    .line 884
    return-void
.end method
