.class Lcom/google/android/music/playback/MusicPlaybackService$6$3;
.super Ljava/lang/Object;
.source "MusicPlaybackService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/MusicPlaybackService$6;->onPlayFromMediaId(Ljava/lang/String;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

.field final synthetic val$mediaId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService$6;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 702
    iput-object p1, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$3;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iput-object p2, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$3;->val$mediaId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 22

    .prologue
    .line 705
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService$6$3;->val$mediaId:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/music/browse/PlayableMediaId;->parseFromExportString(Ljava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;

    move-result-object v18

    .line 707
    .local v18, "playableMediaId":Lcom/google/android/music/browse/PlayableMediaId;
    const/4 v3, 0x0

    .line 708
    .local v3, "songList":Lcom/google/android/music/medialist/SongList;
    sget-object v4, Lcom/google/android/music/playback/MusicPlaybackService$16;->$SwitchMap$com$google$android$music$browse$PlayableMediaId$Type:[I

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getType()Lcom/google/android/music/browse/PlayableMediaId$Type;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/browse/PlayableMediaId$Type;->ordinal()I

    move-result v6

    aget v4, v4, v6

    packed-switch v4, :pswitch_data_0

    .line 761
    const-string v4, "MusicPlaybackService"

    const-string v6, "unrecogonized type"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 763
    :goto_0
    if-eqz v3, :cond_0

    .line 764
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService$6$3;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v4, v4, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    const/4 v6, 0x0

    const/4 v8, 0x1

    invoke-virtual {v4, v3, v6, v8}, Lcom/google/android/music/playback/MusicPlaybackService;->open(Lcom/google/android/music/medialist/SongList;IZ)V

    .line 766
    :cond_0
    :goto_1
    return-void

    .line 710
    :pswitch_0
    new-instance v3, Lcom/google/android/music/medialist/AlbumSongList;

    .end local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getId()J

    move-result-wide v8

    const/4 v4, 0x0

    invoke-direct {v3, v8, v9, v4}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    .line 711
    .restart local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    goto :goto_0

    .line 713
    :pswitch_1
    new-instance v3, Lcom/google/android/music/medialist/PlaylistSongList;

    .end local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getId()J

    move-result-wide v4

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getExtId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v3 .. v13}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 717
    .restart local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    goto :goto_0

    .line 719
    :pswitch_2
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getExtData()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Lcom/google/android/music/store/MusicContent$RadioStations;->getMixDescriptorSeedType(I)Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v7

    .line 723
    .local v7, "seedType":Lcom/google/android/music/mix/MixDescriptor$Type;
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getId()J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v4, v8, v10

    if-eqz v4, :cond_1

    .line 725
    new-instance v5, Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getId()J

    move-result-wide v6

    sget-object v8, Lcom/google/android/music/mix/MixDescriptor$Type;->RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-direct/range {v5 .. v10}, Lcom/google/android/music/mix/MixDescriptor;-><init>(JLcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    .local v5, "mix":Lcom/google/android/music/mix/MixDescriptor;
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService$6$3;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v4, v4, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v4, v5}, Lcom/google/android/music/playback/MusicPlaybackService;->openMix(Lcom/google/android/music/mix/MixDescriptor;)V

    goto :goto_1

    .line 729
    .end local v5    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    :cond_1
    new-instance v5, Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getExtId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-direct/range {v5 .. v10}, Lcom/google/android/music/mix/MixDescriptor;-><init>(Ljava/lang/String;Lcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;Z)V

    .restart local v5    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    goto :goto_2

    .line 735
    .end local v5    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    .end local v7    # "seedType":Lcom/google/android/music/mix/MixDescriptor$Type;
    :pswitch_3
    new-instance v3, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    .end local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getExtId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/music/medialist/NautilusAlbumSongList;-><init>(Ljava/lang/String;)V

    .line 736
    .restart local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    goto :goto_0

    .line 738
    :pswitch_4
    new-instance v3, Lcom/google/android/music/medialist/SharedWithMeSongList;

    .end local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getId()J

    move-result-wide v10

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getExtId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getName()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getExtData()Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    move-object v9, v3

    invoke-direct/range {v9 .. v17}, Lcom/google/android/music/medialist/SharedWithMeSongList;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    .restart local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    goto/16 :goto_0

    .line 743
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService$6$3;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v4, v4, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v4}, Lcom/google/android/music/playback/MusicPlaybackService;->access$900(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 744
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService$6$3;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v4, v4, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v4}, Lcom/google/android/music/playback/MusicPlaybackService;->shuffleOnDevice()V

    goto/16 :goto_1

    .line 746
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService$6$3;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v4, v4, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-static {v4}, Lcom/google/android/music/mix/MixDescriptor;->getLuckyRadio(Landroid/content/Context;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v2

    .line 748
    .local v2, "luckyMix":Lcom/google/android/music/mix/MixDescriptor;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService$6$3;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v4, v4, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v4, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->openMix(Lcom/google/android/music/mix/MixDescriptor;)V

    goto/16 :goto_1

    .line 752
    .end local v2    # "luckyMix":Lcom/google/android/music/mix/MixDescriptor;
    :pswitch_6
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getId()J

    move-result-wide v20

    .line 753
    .local v20, "trackId":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService$6$3;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v4, v4, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    move-wide/from16 v0, v20

    # invokes: Lcom/google/android/music/playback/MusicPlaybackService;->skipToTrack(J)V
    invoke-static {v4, v0, v1}, Lcom/google/android/music/playback/MusicPlaybackService;->access$700(Lcom/google/android/music/playback/MusicPlaybackService;J)V

    goto/16 :goto_1

    .line 756
    .end local v20    # "trackId":J
    :pswitch_7
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->getId()J

    move-result-wide v8

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/playback/MusicPlaybackService$6$3;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v6, v6, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v6}, Lcom/google/android/music/playback/MusicPlaybackService;->access$900(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    invoke-static {v8, v9, v4, v6}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;

    move-result-object v3

    .line 759
    goto/16 :goto_0

    .line 708
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
