.class abstract Lcom/google/android/music/tutorial/TutorialListActivity;
.super Landroid/app/ListActivity;
.source "TutorialListActivity.java"


# instance fields
.field private isDestroyed:Z

.field private mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

.field protected mTracker:Lcom/google/android/music/eventlog/MusicEventLogger;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/tutorial/TutorialListActivity;->isDestroyed:Z

    return-void
.end method


# virtual methods
.method protected getPrefs()Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialListActivity;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method public isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/music/tutorial/TutorialListActivity;->isDestroyed:Z

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const v0, 0x7f050007

    const v1, 0x7f050008

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/tutorial/TutorialListActivity;->overridePendingTransition(II)V

    .line 38
    invoke-static {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialListActivity;->mTracker:Lcom/google/android/music/eventlog/MusicEventLogger;

    .line 39
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialListActivity;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    .line 41
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/tutorial/TutorialListActivity;->requestWindowFeature(I)Z

    .line 42
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/tutorial/TutorialListActivity;->isDestroyed:Z

    .line 47
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialListActivity;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    .line 49
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 50
    return-void
.end method
