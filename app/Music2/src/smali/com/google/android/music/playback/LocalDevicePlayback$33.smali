.class Lcom/google/android/music/playback/LocalDevicePlayback$33;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->setShuffleMode(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0

    .prologue
    .line 4253
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 4256
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->isInCloudQueueMode(Landroid/content/Context;)Z
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5100(Lcom/google/android/music/playback/LocalDevicePlayback;Landroid/content/Context;)Z

    move-result v0

    .line 4257
    .local v0, "isInCloudQueueMode":Z
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5200(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->isUsingPlayQueue()Z
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8900(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4260
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/music/store/Store;->caqShuffle(IZ)Z

    .line 4262
    if-eqz v0, :cond_0

    .line 4263
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5200(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/store/Store;->setCloudQueueShuffleMode(I)V

    .line 4265
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/cloudclient/CloudQueueManager;->setCloudQueue(Landroid/content/Context;)V

    .line 4267
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v1, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 4276
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/store/QueueUtils;->notifyChange(Landroid/content/Context;)V

    .line 4277
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    monitor-enter v2

    .line 4278
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8100(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;

    move-result-object v3

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 4279
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->shouldPlayInRandomOrder()Z
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8300(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 4280
    new-instance v1, Ljava/lang/Exception;

    const-string v4, "shuffle enabled"

    invoke-direct {v1, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 4282
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mRand:Lcom/google/android/music/StrictShuffler;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/StrictShuffler;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2900(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/android/music/StrictShuffler;->setHistorySize(I)V

    .line 4283
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->fillShuffleList()V
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8500(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 4288
    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4289
    :try_start_2
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v3, 0x0

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->saveQueue(Z)V
    invoke-static {v1, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5900(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    .line 4290
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 4291
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->setNextTrack()V
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$700(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 4293
    :cond_1
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 4294
    return-void

    .line 4269
    :cond_2
    if-eqz v0, :cond_3

    .line 4270
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5200(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/store/Store;->setCloudQueueShuffleMode(I)V

    .line 4272
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/cloudclient/CloudQueueManager;->setRepeatShuffleMode(Landroid/content/Context;)V

    .line 4274
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->caqUnshuffle(Z)Z

    goto :goto_0

    .line 4285
    :cond_4
    :try_start_3
    const-string v1, "shuffle disabled"

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 4286
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$33;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8100(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    goto :goto_1

    .line 4288
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1

    .line 4293
    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method
