.class public Lcom/google/android/music/menu/MusicListMenuItem;
.super Lcom/google/android/music/menu/MusicMenuItem;
.source "MusicListMenuItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/menu/MusicListMenuItem$WidgetMode;
    }
.end annotation


# instance fields
.field private mChecked:Z

.field private mItemView:Lcom/google/android/music/menu/MusicListMenuItemView;

.field private mLayoutId:I

.field private mRequestFocus:Z

.field private mSelected:Z

.field private mWidgetMode:Lcom/google/android/music/menu/MusicListMenuItem$WidgetMode;

.field private mWidgetOffDrawable:I

.field private mWidgetOnDrawable:I


# direct methods
.method private refreshWidget()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mItemView:Lcom/google/android/music/menu/MusicListMenuItemView;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mWidgetMode:Lcom/google/android/music/menu/MusicListMenuItem$WidgetMode;

    sget-object v1, Lcom/google/android/music/menu/MusicListMenuItem$WidgetMode;->WIDGET_NONE:Lcom/google/android/music/menu/MusicListMenuItem$WidgetMode;

    if-ne v0, v1, :cond_1

    .line 130
    iget-object v0, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mItemView:Lcom/google/android/music/menu/MusicListMenuItemView;

    invoke-virtual {v0, v2}, Lcom/google/android/music/menu/MusicListMenuItemView;->setWidgetVisible(Z)V

    .line 135
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mItemView:Lcom/google/android/music/menu/MusicListMenuItemView;

    iget-boolean v1, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mSelected:Z

    invoke-virtual {v0, v1}, Lcom/google/android/music/menu/MusicListMenuItemView;->setSelected(Z)V

    .line 136
    iget-boolean v0, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mRequestFocus:Z

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mItemView:Lcom/google/android/music/menu/MusicListMenuItemView;

    invoke-virtual {v0}, Lcom/google/android/music/menu/MusicListMenuItemView;->requestFocus()Z

    .line 138
    iput-boolean v2, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mRequestFocus:Z

    .line 141
    :cond_0
    return-void

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mItemView:Lcom/google/android/music/menu/MusicListMenuItemView;

    invoke-virtual {p0}, Lcom/google/android/music/menu/MusicListMenuItem;->getWidgetImage()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/menu/MusicListMenuItemView;->setWidgetImage(I)V

    .line 133
    iget-object v0, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mItemView:Lcom/google/android/music/menu/MusicListMenuItemView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/menu/MusicListMenuItemView;->setWidgetVisible(Z)V

    goto :goto_0
.end method


# virtual methods
.method public getCheckboxEnabled()Z
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mWidgetMode:Lcom/google/android/music/menu/MusicListMenuItem$WidgetMode;

    sget-object v1, Lcom/google/android/music/menu/MusicListMenuItem$WidgetMode;->WIDGET_CHECKBOX:Lcom/google/android/music/menu/MusicListMenuItem$WidgetMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChecked()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mChecked:Z

    return v0
.end method

.method public getItemView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 172
    iget-object v1, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mItemView:Lcom/google/android/music/menu/MusicListMenuItemView;

    if-nez v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mMusicMenu:Lcom/google/android/music/menu/MusicMenu;

    invoke-virtual {v1}, Lcom/google/android/music/menu/MusicMenu;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 174
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget v1, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mLayoutId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/menu/MusicListMenuItemView;

    iput-object v1, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mItemView:Lcom/google/android/music/menu/MusicListMenuItemView;

    .line 176
    iget-object v1, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mItemView:Lcom/google/android/music/menu/MusicListMenuItemView;

    invoke-virtual {v1, p0}, Lcom/google/android/music/menu/MusicListMenuItemView;->initialize(Lcom/google/android/music/menu/MusicListMenuItem;)V

    .line 177
    iget-object v1, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mItemView:Lcom/google/android/music/menu/MusicListMenuItemView;

    invoke-virtual {p0}, Lcom/google/android/music/menu/MusicListMenuItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/menu/MusicListMenuItemView;->setTitle(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v1, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mItemView:Lcom/google/android/music/menu/MusicListMenuItemView;

    invoke-virtual {p0}, Lcom/google/android/music/menu/MusicListMenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/menu/MusicListMenuItemView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 179
    iget-object v1, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mItemView:Lcom/google/android/music/menu/MusicListMenuItemView;

    invoke-virtual {p0}, Lcom/google/android/music/menu/MusicListMenuItem;->isEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/menu/MusicListMenuItemView;->setEnabled(Z)V

    .line 180
    invoke-direct {p0}, Lcom/google/android/music/menu/MusicListMenuItem;->refreshWidget()V

    .line 182
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mItemView:Lcom/google/android/music/menu/MusicListMenuItemView;

    return-object v1
.end method

.method public getWidgetImage()I
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/google/android/music/menu/MusicListMenuItem;->getChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget v0, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mWidgetOnDrawable:I

    .line 151
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mWidgetOffDrawable:I

    goto :goto_0
.end method

.method public onInvoke()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 200
    invoke-virtual {p0}, Lcom/google/android/music/menu/MusicListMenuItem;->getCheckboxEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 201
    invoke-virtual {p0}, Lcom/google/android/music/menu/MusicListMenuItem;->getChecked()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/music/menu/MusicListMenuItem;->setChecked(Z)V

    .line 205
    :cond_0
    :goto_1
    return-void

    .line 201
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 202
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mMusicMenu:Lcom/google/android/music/menu/MusicMenu;

    invoke-virtual {v1}, Lcom/google/android/music/menu/MusicMenu;->getRadioButtonsEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    invoke-virtual {p0, v0}, Lcom/google/android/music/menu/MusicListMenuItem;->setChecked(Z)V

    goto :goto_1
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/google/android/music/menu/MusicListMenuItem;->setCheckedInternal(Z)V

    .line 86
    iget-object v0, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mMusicMenu:Lcom/google/android/music/menu/MusicMenu;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/music/menu/MusicMenu;->onItemChecked(Lcom/google/android/music/menu/MusicMenuItem;Z)V

    .line 87
    return-void
.end method

.method protected setCheckedInternal(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/google/android/music/menu/MusicListMenuItem;->mChecked:Z

    .line 91
    invoke-direct {p0}, Lcom/google/android/music/menu/MusicListMenuItem;->refreshWidget()V

    .line 92
    return-void
.end method
