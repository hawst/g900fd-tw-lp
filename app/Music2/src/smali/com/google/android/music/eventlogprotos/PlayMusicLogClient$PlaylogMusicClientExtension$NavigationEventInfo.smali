.class public final Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMusicLogClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NavigationEventInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;,
        Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;,
        Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;,
        Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;,
        Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;,
        Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;


# instance fields
.field public appStartedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

.field public listenNowCardClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;

.field public listenNowCardsFetchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;

.field public navbarItemClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

.field public navigationEventType:I

.field public searchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;

.field public youtubeVideoWatchedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2059
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    sput-object v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2060
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2457
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navigationEventType:I

    .line 2460
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->appStartedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    .line 2463
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navbarItemClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    .line 2466
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardsFetchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;

    .line 2469
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->searchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;

    .line 2472
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;

    .line 2475
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->youtubeVideoWatchedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;

    .line 2060
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2505
    const/4 v0, 0x0

    .line 2506
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navigationEventType:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 2507
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navigationEventType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2510
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->appStartedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    if-eqz v1, :cond_1

    .line 2511
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->appStartedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2514
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navbarItemClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    if-eqz v1, :cond_2

    .line 2515
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navbarItemClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2518
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardsFetchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;

    if-eqz v1, :cond_3

    .line 2519
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardsFetchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2522
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->searchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;

    if-eqz v1, :cond_4

    .line 2523
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->searchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2526
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;

    if-eqz v1, :cond_5

    .line 2527
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2530
    :cond_5
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->youtubeVideoWatchedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;

    if-eqz v1, :cond_6

    .line 2531
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->youtubeVideoWatchedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2534
    :cond_6
    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->cachedSize:I

    .line 2535
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2543
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2544
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2548
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2549
    :sswitch_0
    return-object p0

    .line 2554
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2555
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1

    const/16 v2, 0x9

    if-eq v1, v2, :cond_1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_1

    const/16 v2, 0xb

    if-eq v1, v2, :cond_1

    const/16 v2, 0xc

    if-ne v1, v2, :cond_2

    .line 2568
    :cond_1
    iput v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navigationEventType:I

    goto :goto_0

    .line 2570
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navigationEventType:I

    goto :goto_0

    .line 2575
    .end local v1    # "temp":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->appStartedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    if-nez v2, :cond_3

    .line 2576
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->appStartedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    .line 2578
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->appStartedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2582
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navbarItemClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    if-nez v2, :cond_4

    .line 2583
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navbarItemClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    .line 2585
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navbarItemClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2589
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardsFetchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;

    if-nez v2, :cond_5

    .line 2590
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardsFetchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;

    .line 2592
    :cond_5
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardsFetchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2596
    :sswitch_5
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->searchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;

    if-nez v2, :cond_6

    .line 2597
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->searchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;

    .line 2599
    :cond_6
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->searchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2603
    :sswitch_6
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;

    if-nez v2, :cond_7

    .line 2604
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;

    .line 2606
    :cond_7
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2610
    :sswitch_7
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->youtubeVideoWatchedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;

    if-nez v2, :cond_8

    .line 2611
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->youtubeVideoWatchedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;

    .line 2613
    :cond_8
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->youtubeVideoWatchedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2544
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x32 -> :sswitch_4
        0x52 -> :sswitch_5
        0x62 -> :sswitch_6
        0x6a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2056
    invoke-virtual {p0, p1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2480
    iget v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navigationEventType:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 2481
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navigationEventType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2483
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->appStartedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    if-eqz v0, :cond_1

    .line 2484
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->appStartedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2486
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navbarItemClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    if-eqz v0, :cond_2

    .line 2487
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navbarItemClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2489
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardsFetchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;

    if-eqz v0, :cond_3

    .line 2490
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardsFetchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2492
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->searchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;

    if-eqz v0, :cond_4

    .line 2493
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->searchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2495
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;

    if-eqz v0, :cond_5

    .line 2496
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2498
    :cond_5
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->youtubeVideoWatchedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;

    if-eqz v0, :cond_6

    .line 2499
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->youtubeVideoWatchedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2501
    :cond_6
    return-void
.end method
