.class synthetic Lcom/google/android/music/activities/MusicSettingsActivity$6;
.super Ljava/lang/Object;
.source "MusicSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/activities/MusicSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$google$android$music$NautilusStatus:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 454
    invoke-static {}, Lcom/google/android/music/NautilusStatus;->values()[Lcom/google/android/music/NautilusStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/music/activities/MusicSettingsActivity$6;->$SwitchMap$com$google$android$music$NautilusStatus:[I

    :try_start_0
    sget-object v0, Lcom/google/android/music/activities/MusicSettingsActivity$6;->$SwitchMap$com$google$android$music$NautilusStatus:[I

    sget-object v1, Lcom/google/android/music/NautilusStatus;->TRIAL_AVAILABLE:Lcom/google/android/music/NautilusStatus;

    invoke-virtual {v1}, Lcom/google/android/music/NautilusStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    :try_start_1
    sget-object v0, Lcom/google/android/music/activities/MusicSettingsActivity$6;->$SwitchMap$com$google$android$music$NautilusStatus:[I

    sget-object v1, Lcom/google/android/music/NautilusStatus;->PURCHASE_AVAILABLE_NO_TRIAL:Lcom/google/android/music/NautilusStatus;

    invoke-virtual {v1}, Lcom/google/android/music/NautilusStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    sget-object v0, Lcom/google/android/music/activities/MusicSettingsActivity$6;->$SwitchMap$com$google$android$music$NautilusStatus:[I

    sget-object v1, Lcom/google/android/music/NautilusStatus;->GOT_NAUTILUS:Lcom/google/android/music/NautilusStatus;

    invoke-virtual {v1}, Lcom/google/android/music/NautilusStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_0
.end method
