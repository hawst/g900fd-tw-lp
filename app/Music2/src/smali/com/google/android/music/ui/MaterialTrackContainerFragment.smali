.class public Lcom/google/android/music/ui/MaterialTrackContainerFragment;
.super Lcom/google/android/music/ui/TrackContainerFragment;
.source "MaterialTrackContainerFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;
    }
.end annotation


# static fields
.field private static LOGV:Z

.field static TAG:Ljava/lang/String;


# instance fields
.field private mContainerMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

.field private mHeaderBg:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

.field private mMaterialHeader:Lcom/google/android/music/ui/MaterialContainerHeaderView;

.field private mPlayHeaderListLayout:Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;

.field private mSongListMetadataObserver:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-string v0, "MaterialContainer"

    sput-object v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->TAG:Ljava/lang/String;

    .line 37
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/music/ui/TrackContainerFragment;-><init>()V

    .line 273
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/MaterialTrackContainerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->asyncPopulateContainerMetadata()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/MaterialTrackContainerFragment;)Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mContainerMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/music/ui/MaterialTrackContainerFragment;Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;)Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialTrackContainerFragment;
    .param p1, "x1"    # Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mContainerMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/MaterialTrackContainerFragment;)Lcom/google/android/music/ui/MaterialContainerHeaderView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mMaterialHeader:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/MaterialTrackContainerFragment;)Lcom/google/android/music/ui/MaterialContainerHeaderBg;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mHeaderBg:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    return-object v0
.end method

.method private asyncPopulateContainerMetadata()V
    .locals 2

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 198
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;

    invoke-direct {v1, p0, v0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;-><init>(Lcom/google/android/music/ui/MaterialTrackContainerFragment;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 261
    return-void
.end method

.method public static newInstance(Lcom/google/android/music/medialist/MediaList;Lcom/google/android/music/ui/cardlib/model/Document;JLjava/lang/String;)Lcom/google/android/music/ui/MaterialTrackContainerFragment;
    .locals 10
    .param p0, "medialist"    # Lcom/google/android/music/medialist/MediaList;
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p2, "highlightTrackSongId"    # J
    .param p4, "highlightTrackMetajamId"    # Ljava/lang/String;

    .prologue
    .line 71
    new-instance v1, Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    invoke-direct {v1}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;-><init>()V

    .line 72
    .local v1, "fragment":Lcom/google/android/music/ui/MaterialTrackContainerFragment;
    sget-object v4, Lcom/google/android/music/ui/TrackListAdapter;->PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v2, p0

    move-object v3, p1

    move-wide v6, p2

    move-object v8, p4

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->init(Lcom/google/android/music/medialist/MediaList;Lcom/google/android/music/ui/cardlib/model/Document;[Ljava/lang/String;ZJLjava/lang/String;)V

    .line 75
    invoke-virtual {v1}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->saveMediaListAsArguments()V

    .line 76
    return-object v1
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 82
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->getMediaList()Lcom/google/android/music/medialist/MediaList;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/medialist/SongList;

    iput-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 84
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    if-nez v1, :cond_0

    .line 85
    sget-object v1, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->TAG:Ljava/lang/String;

    const-string v3, "Fragment was not initialized with a songlist. Abort."

    invoke-static {v1, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 157
    :goto_0
    return-object v1

    .line 89
    :cond_0
    const v1, 0x7f040072

    invoke-virtual {p1, v1, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mView:Landroid/view/View;

    .line 90
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mView:Landroid/view/View;

    const v3, 0x7f0e019d

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;

    iput-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mPlayHeaderListLayout:Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;

    .line 92
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mPlayHeaderListLayout:Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mPlayHeaderListLayout:Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;

    invoke-virtual {v3}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->getConfigurator()Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    invoke-virtual {v1, v3, v4, v5}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->configureForMusic(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 95
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mPlayHeaderListLayout:Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;

    const v3, 0x7f0e0195

    invoke-virtual {v1, v3}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    iput-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mHeaderBg:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    .line 97
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mHeaderBg:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v1, v3}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    .line 99
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mPlayHeaderListLayout:Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;

    const v3, 0x102000a

    invoke-virtual {v1, v3}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/BaseTrackListView;

    iput-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mTrackList:Lcom/google/android/music/ui/BaseTrackListView;

    .line 101
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mTrackList:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/BaseTrackListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 110
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isJellyBeanOrGreater()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mTrackList:Lcom/google/android/music/ui/BaseTrackListView;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/google/android/music/ui/BaseTrackListView;->setImportantForAccessibility(I)V

    .line 114
    :cond_1
    const v1, 0x7f040070

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/MaterialContainerHeaderView;

    iput-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mMaterialHeader:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    .line 116
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mMaterialHeader:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    invoke-virtual {v1, v6}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->setClickable(Z)V

    .line 117
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mMaterialHeader:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    invoke-virtual {v1, p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->setFragment(Lcom/google/android/music/ui/BaseListFragment;)V

    .line 118
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mMaterialHeader:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v1, v3}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    .line 119
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mMaterialHeader:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1, v3}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->setContainerDocument(Lcom/google/android/music/ui/cardlib/model/Document;)V

    .line 123
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mTrackList:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-virtual {v1, v6}, Lcom/google/android/music/ui/BaseTrackListView;->setClipChildren(Z)V

    .line 124
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mTrackList:Lcom/google/android/music/ui/BaseTrackListView;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mMaterialHeader:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    invoke-virtual {v1, v3, v2, v6}, Lcom/google/android/music/ui/BaseTrackListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 125
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mTrackList:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-virtual {v1, v6}, Lcom/google/android/music/ui/BaseTrackListView;->setFastScrollEnabled(Z)V

    .line 129
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v1}, Lcom/google/android/music/medialist/SongList;->hasMetaData()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 130
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 132
    .local v0, "appContext":Landroid/content/Context;
    new-instance v1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;-><init>(Lcom/google/android/music/ui/MaterialTrackContainerFragment;Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongListMetadataObserver:Landroid/database/ContentObserver;

    .line 152
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongListMetadataObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/medialist/SongList;->registerMetaDataObserver(Landroid/content/Context;Landroid/database/ContentObserver;)V

    .line 156
    .end local v0    # "appContext":Landroid/content/Context;
    :cond_2
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->asyncPopulateContainerMetadata()V

    .line 157
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mView:Landroid/view/View;

    goto/16 :goto_0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mPlayHeaderListLayout:Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->unregisterSharedPreferenceChangeListener()V

    .line 182
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongListMetadataObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongListMetadataObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/medialist/SongList;->unregisterMetaDataObserver(Landroid/content/Context;Landroid/database/ContentObserver;)V

    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongListMetadataObserver:Landroid/database/ContentObserver;

    .line 186
    :cond_0
    invoke-super {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->onDestroyView()V

    .line 187
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/TrackContainerFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mMaterialHeader:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    invoke-virtual {v0, p2}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->setCursor(Landroid/database/Cursor;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mHeaderBg:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    invoke-virtual {v0, p2}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->setCursor(Landroid/database/Cursor;)V

    .line 165
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 265
    .local p1, "cursor":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-super {p0, p1}, Lcom/google/android/music/ui/TrackContainerFragment;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    .line 266
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->onStart()V

    .line 170
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mHeaderBg:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->onStart()V

    .line 171
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mHeaderBg:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->onStop()V

    .line 176
    invoke-super {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->onStop()V

    .line 177
    return-void
.end method
