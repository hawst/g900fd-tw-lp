.class Lcom/google/android/music/SizableTrackSeekBar$1;
.super Ljava/lang/Object;
.source "SizableTrackSeekBar.java"

# interfaces
.implements Lcom/google/android/music/animator/AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/SizableTrackSeekBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/SizableTrackSeekBar;


# direct methods
.method constructor <init>(Lcom/google/android/music/SizableTrackSeekBar;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/music/SizableTrackSeekBar$1;->this$0:Lcom/google/android/music/SizableTrackSeekBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Lcom/google/android/music/animator/Animator;)V
    .locals 3
    .param p1, "animation"    # Lcom/google/android/music/animator/Animator;

    .prologue
    .line 63
    iget-object v1, p0, Lcom/google/android/music/SizableTrackSeekBar$1;->this$0:Lcom/google/android/music/SizableTrackSeekBar;

    invoke-virtual {p1}, Lcom/google/android/music/animator/Animator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    # setter for: Lcom/google/android/music/SizableTrackSeekBar;->mCurrentThumbSizeRatio:F
    invoke-static {v1, v0}, Lcom/google/android/music/SizableTrackSeekBar;->access$002(Lcom/google/android/music/SizableTrackSeekBar;F)F

    .line 64
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar$1;->this$0:Lcom/google/android/music/SizableTrackSeekBar;

    # getter for: Lcom/google/android/music/SizableTrackSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/music/SizableTrackSeekBar;->access$200(Lcom/google/android/music/SizableTrackSeekBar;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/SizableTrackSeekBar$1;->this$0:Lcom/google/android/music/SizableTrackSeekBar;

    # getter for: Lcom/google/android/music/SizableTrackSeekBar;->mCurrentThumbSizeRatio:F
    invoke-static {v1}, Lcom/google/android/music/SizableTrackSeekBar;->access$000(Lcom/google/android/music/SizableTrackSeekBar;)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/music/SizableTrackSeekBar$1;->this$0:Lcom/google/android/music/SizableTrackSeekBar;

    # getter for: Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSizeRatio:Ljava/lang/Float;
    invoke-static {v2}, Lcom/google/android/music/SizableTrackSeekBar;->access$100(Lcom/google/android/music/SizableTrackSeekBar;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    div-float/2addr v1, v2

    const v2, 0x461c4000    # 10000.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 65
    return-void
.end method
