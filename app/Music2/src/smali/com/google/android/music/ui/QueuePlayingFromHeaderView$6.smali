.class Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;
.super Ljava/lang/Object;
.source "QueuePlayingFromHeaderView.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->handleHeaderClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mList:Lcom/google/android/music/medialist/SongList;

.field final synthetic this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$extId:Ljava/lang/String;

.field final synthetic val$id:J

.field final synthetic val$type:Lcom/google/android/music/store/ContainerDescriptor$Type;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 620
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    iput-object p2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->val$type:Lcom/google/android/music/store/ContainerDescriptor$Type;

    iput-wide p3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->val$id:J

    iput-object p5, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->val$extId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 5

    .prologue
    .line 624
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->val$type:Lcom/google/android/music/store/ContainerDescriptor$Type;

    iget-wide v2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->val$id:J

    iget-object v4, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->val$extId:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/music/store/QueueUtils;->getSongList(Landroid/content/Context;Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->mList:Lcom/google/android/music/medialist/SongList;

    .line 625
    return-void
.end method

.method public taskCompleted()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 629
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->mList:Lcom/google/android/music/medialist/SongList;

    if-nez v1, :cond_1

    .line 630
    const-string v1, "QueueHeaderView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to navigate to container "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;
    invoke-static {v3}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->access$300(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", songList built from it is null."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    :cond_0
    :goto_0
    return-void

    .line 634
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mIsAttached:Z
    invoke-static {v1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->access$900(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 635
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->val$type:Lcom/google/android/music/store/ContainerDescriptor$Type;

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->NAUTILUS_ALBUM:Lcom/google/android/music/store/ContainerDescriptor$Type;

    if-ne v1, v2, :cond_2

    .line 640
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->val$extId:Ljava/lang/String;

    invoke-static {v1, v2, v3, v3}, Lcom/google/android/music/ui/TrackContainerActivity;->showNautilusAlbum(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V

    goto :goto_0

    .line 643
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    invoke-virtual {v1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->mList:Lcom/google/android/music/medialist/SongList;

    invoke-static {v1, v2}, Lcom/google/android/music/ui/AppNavigation;->getShowSonglistIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v0

    .line 645
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;->val$context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
