.class Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter$1;
.super Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;
.source "LeanbackLauncherActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/ObjectAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;)V
    .locals 0

    .prologue
    .line 795
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->access$1200(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;)V

    .line 799
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->notifyChanged()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->access$1300(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;)V

    .line 800
    return-void
.end method

.method public onItemRangeChanged(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mOffset:I
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->access$1400(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;)I

    move-result v1

    add-int/2addr v1, p1

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->notifyItemRangeChanged(II)V
    invoke-static {v0, v1, p2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->access$1500(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;II)V

    .line 805
    return-void
.end method

.method public onItemRangeInserted(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 809
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->access$1200(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;)V

    .line 810
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mOffset:I
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->access$1400(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;)I

    move-result v1

    add-int/2addr v1, p1

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->notifyItemRangeInserted(II)V
    invoke-static {v0, v1, p2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->access$1600(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;II)V

    .line 811
    return-void
.end method

.method public onItemRangeRemoved(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 815
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->access$1200(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;)V

    .line 816
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mOffset:I
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->access$1400(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;)I

    move-result v1

    add-int/2addr v1, p1

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->notifyItemRangeRemoved(II)V
    invoke-static {v0, v1, p2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->access$1700(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;II)V

    .line 817
    return-void
.end method
