.class public Lcom/google/android/music/dial/RemoteDeviceManager;
.super Ljava/lang/Object;
.source "RemoteDeviceManager.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/music/dial/SsdpListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/RemoteDeviceManager$2;,
        Lcom/google/android/music/dial/RemoteDeviceManager$Listener;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDiscoveredRoutes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mHandler:Landroid/os/Handler;

.field private final mHandlerThread:Landroid/os/HandlerThread;

.field private final mListener:Lcom/google/android/music/dial/RemoteDeviceManager$Listener;

.field private final mRouteDeviceInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/music/dial/RemoteDeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mSearchTarget:Ljava/lang/String;

.field private final mSsdpClients:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/music/dial/SsdpClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_MANAGEMENT:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/music/dial/RemoteDeviceManager$Listener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchTarget"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/google/android/music/dial/RemoteDeviceManager$Listener;

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mRouteDeviceInfoMap:Ljava/util/Map;

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSsdpClients:Ljava/util/Map;

    .line 83
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mDiscoveredRoutes:Ljava/util/Set;

    .line 125
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mContext:Landroid/content/Context;

    .line 126
    iput-object p2, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSearchTarget:Ljava/lang/String;

    .line 127
    iput-object p3, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mListener:Lcom/google/android/music/dial/RemoteDeviceManager$Listener;

    .line 129
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "RemoteDeviceManager"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandlerThread:Landroid/os/HandlerThread;

    .line 130
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 131
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    .line 133
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 134
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Lcom/google/android/music/dial/SsdpMessage;)Lcom/google/android/music/dial/RemoteDeviceInfo;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Lcom/google/android/music/dial/SsdpMessage;

    .prologue
    .line 52
    invoke-static {p0, p1}, Lcom/google/android/music/dial/RemoteDeviceManager;->remoteDeviceInfoFromSsdpMessage(Ljava/lang/String;Lcom/google/android/music/dial/SsdpMessage;)Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/dial/RemoteDeviceManager;Lcom/google/android/music/dial/RemoteDeviceInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceManager;
    .param p1, "x1"    # Lcom/google/android/music/dial/RemoteDeviceInfo;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/music/dial/RemoteDeviceManager;->validRemoteDeviceInfo(Lcom/google/android/music/dial/RemoteDeviceInfo;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/music/dial/RemoteDeviceManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceManager;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private addDiscoveredRoute(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 654
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mDiscoveredRoutes:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 655
    return-void
.end method

.method private addRemoteDeviceInfo(Ljava/lang/String;Lcom/google/android/music/dial/RemoteDeviceInfo;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "device"    # Lcom/google/android/music/dial/RemoteDeviceInfo;

    .prologue
    .line 638
    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mRouteDeviceInfoMap:Ljava/util/Map;

    monitor-enter v1

    .line 639
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mRouteDeviceInfoMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 640
    monitor-exit v1

    .line 641
    return-void

    .line 640
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private clearDiscoveredRoutes()V
    .locals 1

    .prologue
    .line 650
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mDiscoveredRoutes:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 651
    return-void
.end method

.method private static createRemoteDeviceInfoFromXml(Ljava/lang/String;Lcom/google/android/music/dial/SsdpMessage;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo;
    .locals 15
    .param p0, "usn"    # Ljava/lang/String;
    .param p1, "message"    # Lcom/google/android/music/dial/SsdpMessage;
    .param p2, "xml"    # Ljava/lang/String;
    .param p3, "appUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 770
    new-instance v2, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    invoke-direct {v2}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;-><init>()V

    .line 771
    .local v2, "deviceBuilder":Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    invoke-virtual {v2, p0}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setDeviceId(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .line 772
    invoke-static/range {p3 .. p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v2, v12}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setApplicationUrl(Landroid/net/Uri;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .line 774
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 775
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v12, 0x1

    invoke-virtual {v4, v12}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 776
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v11

    .line 777
    .local v11, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v12, Ljava/io/StringReader;

    move-object/from16 v0, p2

    invoke-direct {v12, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v11, v12}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 778
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 779
    .local v3, "eventType":I
    const/4 v8, 0x0

    .line 781
    .local v8, "serviceBuilder":Lcom/google/android/music/dial/RemoteService$Builder;
    const-string v1, ""

    .line 783
    .local v1, "currentText":Ljava/lang/String;
    :goto_0
    const/4 v12, 0x1

    if-eq v3, v12, :cond_12

    .line 784
    if-nez v3, :cond_1

    .line 831
    :cond_0
    :goto_1
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_0

    .line 786
    :cond_1
    const/4 v12, 0x2

    if-ne v3, v12, :cond_2

    .line 787
    const-string v1, ""

    .line 788
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "service"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 789
    new-instance v8, Lcom/google/android/music/dial/RemoteService$Builder;

    .end local v8    # "serviceBuilder":Lcom/google/android/music/dial/RemoteService$Builder;
    invoke-direct {v8}, Lcom/google/android/music/dial/RemoteService$Builder;-><init>()V

    .restart local v8    # "serviceBuilder":Lcom/google/android/music/dial/RemoteService$Builder;
    goto :goto_1

    .line 791
    :cond_2
    const/4 v12, 0x3

    if-ne v3, v12, :cond_11

    .line 792
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v10

    .line 793
    .local v10, "tag":Ljava/lang/String;
    const-string v12, "deviceType"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 794
    invoke-virtual {v2, v1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setDeviceType(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    goto :goto_1

    .line 795
    :cond_3
    const-string v12, "friendlyName"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 796
    invoke-virtual {v2, v1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setFriendlyName(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    goto :goto_1

    .line 797
    :cond_4
    const-string v12, "manufacturer"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 798
    invoke-virtual {v2, v1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setManufacturer(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    goto :goto_1

    .line 799
    :cond_5
    const-string v12, "manufacturerUrl"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 800
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v2, v12}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setManufacturerUrl(Landroid/net/Uri;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    goto :goto_1

    .line 801
    :cond_6
    const-string v12, "modelDescription"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 802
    invoke-virtual {v2, v1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setModelDescription(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    goto :goto_1

    .line 803
    :cond_7
    const-string v12, "modelName"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 804
    invoke-virtual {v2, v1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setModelName(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    goto :goto_1

    .line 805
    :cond_8
    const-string v12, "modelNumber"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 806
    invoke-virtual {v2, v1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setModelNumber(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    goto :goto_1

    .line 807
    :cond_9
    const-string v12, "serialNumber"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 808
    invoke-virtual {v2, v1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setSerialNumber(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    goto/16 :goto_1

    .line 809
    :cond_a
    const-string v12, "udn"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 810
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    .line 811
    .local v7, "lc":Ljava/lang/String;
    const-string v12, "uuid:"

    invoke-virtual {v7, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 812
    const-string v12, "uuid:"

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v1, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setUniqueDeviceName(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    goto/16 :goto_1

    .line 814
    .end local v7    # "lc":Ljava/lang/String;
    :cond_b
    const-string v12, "service"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_c

    if-eqz v8, :cond_c

    .line 815
    invoke-virtual {v8}, Lcom/google/android/music/dial/RemoteService$Builder;->build()Lcom/google/android/music/dial/RemoteService;

    move-result-object v12

    invoke-virtual {v2, v12}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->addRemoteService(Lcom/google/android/music/dial/RemoteService;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .line 816
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 817
    :cond_c
    const-string v12, "serviceType"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_d

    if-eqz v8, :cond_d

    .line 818
    invoke-virtual {v8, v1}, Lcom/google/android/music/dial/RemoteService$Builder;->setServiceType(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;

    goto/16 :goto_1

    .line 819
    :cond_d
    const-string v12, "serviceId"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_e

    if-eqz v8, :cond_e

    .line 820
    invoke-virtual {v8, v1}, Lcom/google/android/music/dial/RemoteService$Builder;->setServiceId(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;

    goto/16 :goto_1

    .line 821
    :cond_e
    const-string v12, "SCPDUrl"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_f

    if-eqz v8, :cond_f

    .line 822
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Lcom/google/android/music/dial/RemoteService$Builder;->setScpdUrl(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;

    goto/16 :goto_1

    .line 823
    :cond_f
    const-string v12, "controlURL"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_10

    if-eqz v8, :cond_10

    .line 824
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Lcom/google/android/music/dial/RemoteService$Builder;->setControlUrl(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;

    goto/16 :goto_1

    .line 825
    :cond_10
    const-string v12, "eventSubURL"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    if-eqz v8, :cond_0

    .line 826
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Lcom/google/android/music/dial/RemoteService$Builder;->setEventSubUrl(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;

    goto/16 :goto_1

    .line 828
    .end local v10    # "tag":Ljava/lang/String;
    :cond_11
    const/4 v12, 0x4

    if-ne v3, v12, :cond_0

    .line 829
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 837
    :cond_12
    const-string v12, "groupinfo.sonos.com"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/google/android/music/dial/SsdpMessage;->getFirstHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 839
    .local v5, "groupInfo":Ljava/lang/String;
    if-eqz v5, :cond_16

    .line 840
    invoke-static {v5}, Lcom/google/android/music/dial/SonosGroupInfo;->fromString(Ljava/lang/String;)Lcom/google/android/music/dial/SonosGroupInfo;

    move-result-object v9

    .line 841
    .local v9, "sgi":Lcom/google/android/music/dial/SonosGroupInfo;
    if-nez v9, :cond_13

    .line 842
    const-string v12, "RemoteDeviceManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Invalid Sonos group info.  Ignoring "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 843
    const/4 v12, 0x0

    .line 865
    .end local v9    # "sgi":Lcom/google/android/music/dial/SonosGroupInfo;
    :goto_2
    return-object v12

    .line 845
    .restart local v9    # "sgi":Lcom/google/android/music/dial/SonosGroupInfo;
    :cond_13
    invoke-virtual {v9}, Lcom/google/android/music/dial/SonosGroupInfo;->getGroupId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setGroupId(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .line 846
    invoke-virtual {v9}, Lcom/google/android/music/dial/SonosGroupInfo;->getGroupName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setGroupName(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .line 847
    invoke-virtual {v9}, Lcom/google/android/music/dial/SonosGroupInfo;->getIsGroupCoordinator()Z

    move-result v12

    invoke-virtual {v2, v12}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setIsGroupCoordinator(Z)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .line 855
    .end local v9    # "sgi":Lcom/google/android/music/dial/SonosGroupInfo;
    :cond_14
    :goto_3
    const-string v12, "household.sonos.com"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/google/android/music/dial/SsdpMessage;->getFirstHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 857
    .local v6, "householdId":Ljava/lang/String;
    if-eqz v6, :cond_17

    .line 858
    invoke-virtual {v2, v6}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setHouseholdId(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .line 865
    :cond_15
    :goto_4
    invoke-virtual {v2}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->build()Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v12

    goto :goto_2

    .line 849
    .end local v6    # "householdId":Ljava/lang/String;
    :cond_16
    sget-boolean v12, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v12, :cond_14

    .line 850
    const-string v12, "RemoteDeviceManager"

    const-string v13, "Group info header not found."

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 860
    .restart local v6    # "householdId":Ljava/lang/String;
    :cond_17
    sget-boolean v12, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v12, :cond_15

    .line 861
    const-string v12, "RemoteDeviceManager"

    const-string v13, "Household header not found."

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4
.end method

.method private static getNetworkInterfaces()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/net/NetworkInterface;",
            ">;"
        }
    .end annotation

    .prologue
    .line 875
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    .line 878
    .local v1, "filteredNetworkInterfaces":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/net/NetworkInterface;>;"
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v5

    .line 880
    .local v5, "networkInterfaces":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    if-eqz v5, :cond_2

    .line 881
    :cond_0
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 882
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    .line 883
    .local v4, "networkInterface":Ljava/net/NetworkInterface;
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->isUp()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->isLoopback()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->isPointToPoint()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->supportsMulticast()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 888
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInterfaceAddresses()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InterfaceAddress;

    .line 890
    .local v3, "ifAddr":Ljava/net/InterfaceAddress;
    invoke-virtual {v3}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v6

    instance-of v6, v6, Ljava/net/Inet4Address;

    if-eqz v6, :cond_1

    .line 891
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 897
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "ifAddr":Ljava/net/InterfaceAddress;
    .end local v4    # "networkInterface":Ljava/net/NetworkInterface;
    .end local v5    # "networkInterfaces":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :catch_0
    move-exception v0

    .line 898
    .local v0, "e":Ljava/io/IOException;
    const-string v6, "RemoteDeviceManager"

    const-string v7, "Exception while getting network interfaces"

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 899
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 902
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    return-object v1
.end method

.method private getRemoteDeviceNames()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 632
    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mRouteDeviceInfoMap:Ljava/util/Map;

    monitor-enter v1

    .line 633
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mRouteDeviceInfoMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 634
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private handleActiveRemoteDevice(Lcom/google/android/music/dial/RemoteDeviceInfo;)V
    .locals 4
    .param p1, "device"    # Lcom/google/android/music/dial/RemoteDeviceInfo;

    .prologue
    .line 559
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 560
    .local v0, "deviceId":Ljava/lang/String;
    sget-boolean v1, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v1, :cond_0

    .line 561
    const-string v1, "RemoteDeviceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleActiveRemoteDevice "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/google/android/music/dial/RemoteDeviceManager;->addRemoteDeviceInfo(Ljava/lang/String;Lcom/google/android/music/dial/RemoteDeviceInfo;)V

    .line 565
    invoke-direct {p0, v0}, Lcom/google/android/music/dial/RemoteDeviceManager;->addDiscoveredRoute(Ljava/lang/String;)V

    .line 566
    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mListener:Lcom/google/android/music/dial/RemoteDeviceManager$Listener;

    if-eqz v1, :cond_1

    .line 567
    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mListener:Lcom/google/android/music/dial/RemoteDeviceManager$Listener;

    invoke-interface {v1, p1}, Lcom/google/android/music/dial/RemoteDeviceManager$Listener;->onDeviceOnline(Lcom/google/android/music/dial/RemoteDeviceInfo;)V

    .line 569
    :cond_1
    return-void
.end method

.method private handleInactiveRemoteDevice(Lcom/google/android/music/dial/RemoteDeviceInfo;)V
    .locals 5
    .param p1, "device"    # Lcom/google/android/music/dial/RemoteDeviceInfo;

    .prologue
    .line 577
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 578
    .local v0, "deviceId":Ljava/lang/String;
    sget-boolean v2, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v2, :cond_0

    .line 579
    const-string v2, "RemoteDeviceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleInactiveRemoteDevice "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/music/dial/RemoteDeviceManager;->removeRemoteDeviceInfo(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v1

    .line 583
    .local v1, "lDevice":Lcom/google/android/music/dial/RemoteDeviceInfo;
    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mListener:Lcom/google/android/music/dial/RemoteDeviceManager$Listener;

    if-eqz v2, :cond_1

    .line 584
    iget-object v2, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mListener:Lcom/google/android/music/dial/RemoteDeviceManager$Listener;

    invoke-interface {v2, v1}, Lcom/google/android/music/dial/RemoteDeviceManager$Listener;->onDeviceOffline(Lcom/google/android/music/dial/RemoteDeviceInfo;)V

    .line 586
    :cond_1
    return-void
.end method

.method private handleOfflineDevices()V
    .locals 8

    .prologue
    .line 594
    sget-boolean v5, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v5, :cond_0

    .line 595
    const-string v5, "RemoteDeviceManager"

    const-string v6, "handleOfflineDevices"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v3

    .line 600
    .local v3, "offlineRoutes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->getRemoteDeviceNames()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 601
    .local v1, "eRoute":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mDiscoveredRoutes:Ljava/util/Set;

    invoke-interface {v5, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 602
    sget-boolean v5, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v5, :cond_2

    .line 603
    const-string v5, "RemoteDeviceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Offline route "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    :cond_2
    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 609
    .end local v1    # "eRoute":Ljava/lang/String;
    :cond_3
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 610
    .local v4, "route":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/google/android/music/dial/RemoteDeviceManager;->removeRemoteDeviceInfo(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v0

    .line 611
    .local v0, "device":Lcom/google/android/music/dial/RemoteDeviceInfo;
    iget-object v5, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mListener:Lcom/google/android/music/dial/RemoteDeviceManager$Listener;

    if-eqz v5, :cond_4

    .line 612
    iget-object v5, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mListener:Lcom/google/android/music/dial/RemoteDeviceManager$Listener;

    invoke-interface {v5, v0}, Lcom/google/android/music/dial/RemoteDeviceManager$Listener;->onDeviceOffline(Lcom/google/android/music/dial/RemoteDeviceInfo;)V

    goto :goto_1

    .line 616
    .end local v0    # "device":Lcom/google/android/music/dial/RemoteDeviceInfo;
    .end local v4    # "route":Ljava/lang/String;
    :cond_5
    sget-boolean v5, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v5, :cond_6

    .line 617
    const-string v5, "RemoteDeviceManager"

    const-string v6, "handleOfflineDevices - done."

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    :cond_6
    return-void
.end method

.method private handleRepeatActiveScan()V
    .locals 5

    .prologue
    .line 288
    sget-boolean v0, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v0, :cond_0

    .line 289
    const-string v0, "RemoteDeviceManager"

    const-string v1, "handleRepeatActiveScan %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSearchTarget:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->handleOfflineDevices()V

    .line 296
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->startScanInternal()V

    .line 299
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/dial/Utils;->getScanIntervalMillis(Landroid/content/Context;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 300
    return-void
.end method

.method private handleScanError(Lcom/google/android/music/dial/SsdpListener$ScanError;)V
    .locals 3
    .param p1, "error"    # Lcom/google/android/music/dial/SsdpListener$ScanError;

    .prologue
    .line 544
    sget-boolean v0, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v0, :cond_0

    .line 545
    const-string v0, "RemoteDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleScanError: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/music/dial/SsdpListener$ScanError;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mListener:Lcom/google/android/music/dial/RemoteDeviceManager$Listener;

    if-eqz v0, :cond_1

    .line 549
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mListener:Lcom/google/android/music/dial/RemoteDeviceManager$Listener;

    sget-object v1, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;->SSDP_CLIENT_SCAN_ERROR:Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    invoke-interface {v0, v1}, Lcom/google/android/music/dial/RemoteDeviceManager$Listener;->onScanError(Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;)V

    .line 551
    :cond_1
    return-void
.end method

.method private handleShutdown()V
    .locals 8

    .prologue
    .line 354
    sget-boolean v5, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v5, :cond_0

    .line 355
    const-string v5, "RemoteDeviceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handleShutdown "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSearchTarget:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->stopDiscoveryScans()V

    .line 361
    iget-object v5, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSsdpClients:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 362
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/music/dial/SsdpClient;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 363
    .local v3, "name":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/dial/SsdpClient;

    .line 365
    .local v4, "ssdpClient":Lcom/google/android/music/dial/SsdpClient;
    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/music/dial/SsdpClient;->shutdown()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 366
    :catch_0
    move-exception v0

    .line 367
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "RemoteDeviceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error while stopping SsdpClient scan for interface "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 371
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/music/dial/SsdpClient;>;"
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "ssdpClient":Lcom/google/android/music/dial/SsdpClient;
    :cond_1
    iget-object v5, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSsdpClients:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    .line 374
    iget-object v5, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v5}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 375
    return-void
.end method

.method private handleSsdpMessage(Lcom/google/android/music/dial/SsdpMessage;)V
    .locals 9
    .param p1, "message"    # Lcom/google/android/music/dial/SsdpMessage;

    .prologue
    .line 468
    sget-boolean v6, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v6, :cond_0

    .line 469
    const-string v6, "RemoteDeviceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleSsdpMessage: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/android/music/dial/SsdpMessage;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/dial/SsdpMessage;->getMessageType()Lcom/google/android/music/dial/SsdpMessage$MessageType;

    move-result-object v4

    .line 473
    .local v4, "type":Lcom/google/android/music/dial/SsdpMessage$MessageType;
    const/4 v0, 0x1

    .line 475
    .local v0, "active":Z
    sget-object v6, Lcom/google/android/music/dial/RemoteDeviceManager$2;->$SwitchMap$com$google$android$music$dial$SsdpMessage$MessageType:[I

    invoke-virtual {v4}, Lcom/google/android/music/dial/SsdpMessage$MessageType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 500
    :goto_0
    const-string v6, "ST"

    invoke-virtual {p1, v6}, Lcom/google/android/music/dial/SsdpMessage;->getFirstHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 501
    .local v3, "searchTarget":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 502
    const-string v6, "RemoteDeviceManager"

    const-string v7, "No ST (Search Target) specified in message.  Ignoring."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    .end local v3    # "searchTarget":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 477
    :pswitch_0
    const/4 v0, 0x1

    .line 478
    goto :goto_0

    .line 480
    :pswitch_1
    const-string v6, "NTS"

    invoke-virtual {p1, v6}, Lcom/google/android/music/dial/SsdpMessage;->getFirstHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 481
    .local v2, "nts":Ljava/lang/String;
    const-string v6, "ssdp:byebye"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 482
    const/4 v0, 0x0

    goto :goto_0

    .line 483
    :cond_2
    const-string v6, "ssdp:alive"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 484
    const/4 v0, 0x1

    goto :goto_0

    .line 486
    :cond_3
    sget-boolean v6, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v6, :cond_1

    .line 487
    const-string v6, "RemoteDeviceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Received notification with unexpected NTS "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".  Ignoring."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 494
    .end local v2    # "nts":Ljava/lang/String;
    :pswitch_2
    sget-boolean v6, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v6, :cond_1

    .line 495
    const-string v6, "RemoteDeviceManager"

    const-string v7, "Received an M-SEARCH request.  Ignoring."

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 505
    .restart local v3    # "searchTarget":Ljava/lang/String;
    :cond_4
    iget-object v6, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSearchTarget:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 506
    const-string v6, "RemoteDeviceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unsupported search target in message.  Ignoring "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 509
    :cond_5
    const-string v6, "USN"

    invoke-virtual {p1, v6}, Lcom/google/android/music/dial/SsdpMessage;->getFirstHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 510
    .local v5, "usn":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 511
    const-string v6, "RemoteDeviceManager"

    const-string v7, "No USN specified in message.  Ignoring."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 515
    :cond_6
    if-eqz v0, :cond_7

    .line 518
    iget-object v6, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v7, Lcom/google/android/music/dial/RemoteDeviceManager$1;

    invoke-direct {v7, p0, v5, p1}, Lcom/google/android/music/dial/RemoteDeviceManager$1;-><init>(Lcom/google/android/music/dial/RemoteDeviceManager;Ljava/lang/String;Lcom/google/android/music/dial/SsdpMessage;)V

    invoke-interface {v6, v7}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    .line 530
    :cond_7
    invoke-direct {p0, v5}, Lcom/google/android/music/dial/RemoteDeviceManager;->removeRemoteDeviceInfo(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v1

    .line 531
    .local v1, "device":Lcom/google/android/music/dial/RemoteDeviceInfo;
    if-eqz v1, :cond_1

    invoke-direct {p0, v1}, Lcom/google/android/music/dial/RemoteDeviceManager;->validRemoteDeviceInfo(Lcom/google/android/music/dial/RemoteDeviceInfo;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 532
    iget-object v6, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    const/16 v8, 0x9

    invoke-virtual {v7, v8, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 475
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleStartActiveScan()V
    .locals 5

    .prologue
    .line 269
    sget-boolean v0, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v0, :cond_0

    .line 270
    const-string v0, "RemoteDeviceManager"

    const-string v1, "handleStartActiveScan %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSearchTarget:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->startScanInternal()V

    .line 277
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/dial/Utils;->getScanIntervalMillis(Landroid/content/Context;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 278
    return-void
.end method

.method private handleStartPassiveScan()V
    .locals 5

    .prologue
    .line 313
    sget-boolean v0, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v0, :cond_0

    .line 314
    const-string v0, "RemoteDeviceManager"

    const-string v1, "handleStartPassiveScan %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSearchTarget:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->startScanInternal()V

    .line 321
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/dial/Utils;->getScanIntervalMillis(Landroid/content/Context;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 323
    return-void
.end method

.method private handleStopScan()V
    .locals 3

    .prologue
    .line 331
    sget-boolean v0, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v0, :cond_0

    .line 332
    const-string v0, "RemoteDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleStopScan "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSearchTarget:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->stopDiscoveryScans()V

    .line 336
    return-void
.end method

.method private static remoteDeviceInfoFromHttpResponse(Ljava/lang/String;Lcom/google/android/music/dial/SsdpMessage;Lorg/apache/http/HttpResponse;)Lcom/google/android/music/dial/RemoteDeviceInfo;
    .locals 11
    .param p0, "usn"    # Ljava/lang/String;
    .param p1, "message"    # Lcom/google/android/music/dial/SsdpMessage;
    .param p2, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 700
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    .line 701
    .local v5, "statusCode":I
    const/16 v7, 0xc8

    if-eq v5, v7, :cond_1

    .line 702
    sget-boolean v7, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v7, :cond_1

    .line 703
    const-string v7, "RemoteDeviceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invalid HTTP status code in response: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v6

    .line 750
    :cond_0
    :goto_0
    return-object v2

    .line 708
    :cond_1
    const/4 v0, 0x0

    .line 709
    .local v0, "appUrl":Ljava/lang/String;
    const-string v7, "application-url"

    invoke-interface {p2, v7}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v4

    .line 710
    .local v4, "header":Lorg/apache/http/Header;
    if-eqz v4, :cond_2

    .line 711
    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 713
    :cond_2
    if-nez v0, :cond_4

    .line 715
    sget-boolean v7, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v7, :cond_3

    .line 716
    const-string v7, "RemoteDeviceManager"

    const-string v8, "No application-url header found in response."

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    :cond_3
    const-string v7, "websock.sonos.com"

    invoke-virtual {p1, v7}, Lcom/google/android/music/dial/SsdpMessage;->getFirstHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 722
    if-nez v0, :cond_4

    .line 723
    const-string v7, "RemoteDeviceManager"

    const-string v8, "No application URL header found in response.  Ignoring."

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v6

    .line 724
    goto :goto_0

    .line 727
    :cond_4
    const-string v7, "/"

    invoke-virtual {v0, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 728
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v0, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 730
    :cond_5
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    .line 731
    .local v3, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v3, :cond_8

    .line 732
    invoke-static {v3}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v1

    .line 733
    .local v1, "body":Ljava/lang/String;
    invoke-static {p0, p1, v1, v0}, Lcom/google/android/music/dial/RemoteDeviceManager;->createRemoteDeviceInfoFromXml(Ljava/lang/String;Lcom/google/android/music/dial/SsdpMessage;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v2

    .line 736
    .local v2, "deviceInfo":Lcom/google/android/music/dial/RemoteDeviceInfo;
    invoke-virtual {v2}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getGroupId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 737
    const-string v7, "RemoteDeviceManager"

    const-string v8, "Ignoring invalid group.  %s"

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v2, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v6

    .line 738
    goto :goto_0

    .line 740
    :cond_6
    invoke-virtual {v2}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getIsGroupCoordinator()Z

    move-result v7

    if-nez v7, :cond_0

    .line 741
    sget-boolean v7, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v7, :cond_7

    .line 742
    const-string v7, "RemoteDeviceManager"

    const-string v8, "Ignoring device that is not a group coordinator.  %s"

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v2, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    move-object v2, v6

    .line 745
    goto/16 :goto_0

    .line 749
    .end local v1    # "body":Ljava/lang/String;
    .end local v2    # "deviceInfo":Lcom/google/android/music/dial/RemoteDeviceInfo;
    :cond_8
    const-string v7, "RemoteDeviceManager"

    const-string v8, "Device body information missing"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v6

    .line 750
    goto/16 :goto_0
.end method

.method private static remoteDeviceInfoFromSsdpMessage(Ljava/lang/String;Lcom/google/android/music/dial/SsdpMessage;)Lcom/google/android/music/dial/RemoteDeviceInfo;
    .locals 7
    .param p0, "usn"    # Ljava/lang/String;
    .param p1, "message"    # Lcom/google/android/music/dial/SsdpMessage;

    .prologue
    const/4 v4, 0x0

    .line 670
    const-string v5, "LOCATION"

    invoke-virtual {p1, v5}, Lcom/google/android/music/dial/SsdpMessage;->getFirstHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 672
    .local v3, "location":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 673
    const-string v5, "RemoteDeviceManager"

    const-string v6, "Invalid location in SsdpMessage.  Ignoring."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    :goto_0
    return-object v4

    .line 677
    :cond_0
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 680
    .local v0, "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    :try_start_0
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    new-instance v5, Ljava/net/URI;

    invoke-direct {v5, v3}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 681
    .local v2, "get":Lorg/apache/http/client/methods/HttpGet;
    invoke-virtual {v0, v2}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v5

    invoke-static {p0, p1, v5}, Lcom/google/android/music/dial/RemoteDeviceManager;->remoteDeviceInfoFromHttpResponse(Ljava/lang/String;Lcom/google/android/music/dial/SsdpMessage;Lorg/apache/http/HttpResponse;)Lcom/google/android/music/dial/RemoteDeviceInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 682
    .end local v2    # "get":Lorg/apache/http/client/methods/HttpGet;
    :catch_0
    move-exception v1

    .line 683
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "RemoteDeviceManager"

    const-string v6, "Unable to get device information"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private removeRemoteDeviceInfo(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 644
    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mRouteDeviceInfoMap:Ljava/util/Map;

    monitor-enter v1

    .line 645
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mRouteDeviceInfoMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/dial/RemoteDeviceInfo;

    monitor-exit v1

    return-object v0

    .line 646
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private startScanInternal()V
    .locals 10

    .prologue
    .line 383
    sget-boolean v7, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v7, :cond_0

    .line 384
    const-string v7, "RemoteDeviceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "startScanInternal "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSearchTarget:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->clearDiscoveredRoutes()V

    .line 391
    invoke-static {}, Lcom/google/android/music/dial/RemoteDeviceManager;->getNetworkInterfaces()Ljava/util/Map;

    move-result-object v5

    .line 393
    .local v5, "networkInterfaces":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/net/NetworkInterface;>;"
    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 394
    iget-object v7, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mListener:Lcom/google/android/music/dial/RemoteDeviceManager$Listener;

    if-eqz v7, :cond_1

    .line 395
    iget-object v7, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mListener:Lcom/google/android/music/dial/RemoteDeviceManager$Listener;

    sget-object v8, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;->NO_NETWORK_INTERFACES_FOUND:Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    invoke-interface {v7, v8}, Lcom/google/android/music/dial/RemoteDeviceManager$Listener;->onScanError(Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;)V

    .line 433
    :cond_1
    return-void

    .line 401
    :cond_2
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 402
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/net/NetworkInterface;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 403
    .local v4, "ifaceName":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSsdpClients:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 404
    new-instance v6, Lcom/google/android/music/dial/SsdpClient;

    iget-object v8, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mContext:Landroid/content/Context;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/net/NetworkInterface;

    iget-object v9, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSearchTarget:Ljava/lang/String;

    invoke-direct {v6, v8, v7, v9, p0}, Lcom/google/android/music/dial/SsdpClient;-><init>(Landroid/content/Context;Ljava/net/NetworkInterface;Ljava/lang/String;Lcom/google/android/music/dial/SsdpListener;)V

    .line 406
    .local v6, "ssdpClient":Lcom/google/android/music/dial/SsdpClient;
    invoke-virtual {v6}, Lcom/google/android/music/dial/SsdpClient;->initialize()Z

    move-result v7

    if-nez v7, :cond_4

    .line 407
    const-string v7, "RemoteDeviceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error initializing SsdpClient for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 410
    :cond_4
    iget-object v7, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSsdpClients:Ljava/util/Map;

    invoke-interface {v7, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 415
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/net/NetworkInterface;>;"
    .end local v4    # "ifaceName":Ljava/lang/String;
    .end local v6    # "ssdpClient":Lcom/google/android/music/dial/SsdpClient;
    :cond_5
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 416
    .restart local v4    # "ifaceName":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSsdpClients:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 417
    iget-object v7, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSsdpClients:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/dial/SsdpClient;

    .line 419
    .restart local v6    # "ssdpClient":Lcom/google/android/music/dial/SsdpClient;
    :try_start_0
    invoke-virtual {v6}, Lcom/google/android/music/dial/SsdpClient;->shutdown()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 420
    :catch_0
    move-exception v0

    .line 421
    .local v0, "e":Ljava/io/IOException;
    const-string v7, "RemoteDeviceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error shutting down SsdpClient for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 427
    .end local v0    # "e":Ljava/io/IOException;
    .end local v4    # "ifaceName":Ljava/lang/String;
    .end local v6    # "ssdpClient":Lcom/google/android/music/dial/SsdpClient;
    :cond_7
    iget-object v7, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSsdpClients:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 428
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/music/dial/SsdpClient;>;"
    sget-boolean v7, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v7, :cond_8

    .line 429
    const-string v8, "RemoteDeviceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Starting scan on interface "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    :cond_8
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/music/dial/SsdpClient;

    invoke-virtual {v7}, Lcom/google/android/music/dial/SsdpClient;->startScan()V

    goto :goto_2
.end method

.method private stopDiscoveryScans()V
    .locals 2

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 343
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 344
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 345
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 346
    return-void
.end method

.method private validRemoteDeviceInfo(Lcom/google/android/music/dial/RemoteDeviceInfo;)Z
    .locals 1
    .param p1, "device"    # Lcom/google/android/music/dial/RemoteDeviceInfo;

    .prologue
    .line 628
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public getRemoteDeviceInfos()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/dial/RemoteDeviceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mRouteDeviceInfoMap:Ljava/util/Map;

    monitor-enter v1

    .line 183
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mRouteDeviceInfoMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x1

    .line 208
    sget-boolean v0, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v0, :cond_0

    .line 209
    const-string v0, "RemoteDeviceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 253
    const-string v0, "RemoteDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 213
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->handleStartActiveScan()V

    move v0, v1

    .line 214
    goto :goto_0

    .line 217
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->handleRepeatActiveScan()V

    move v0, v1

    .line 218
    goto :goto_0

    .line 221
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->handleStartPassiveScan()V

    move v0, v1

    .line 222
    goto :goto_0

    .line 225
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->handleStopScan()V

    move v0, v1

    .line 226
    goto :goto_0

    .line 229
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->handleShutdown()V

    move v0, v1

    .line 230
    goto :goto_0

    .line 233
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/dial/SsdpMessage;

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/RemoteDeviceManager;->handleSsdpMessage(Lcom/google/android/music/dial/SsdpMessage;)V

    move v0, v1

    .line 234
    goto :goto_0

    .line 237
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/dial/SsdpListener$ScanError;

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/RemoteDeviceManager;->handleScanError(Lcom/google/android/music/dial/SsdpListener$ScanError;)V

    move v0, v1

    .line 238
    goto :goto_0

    .line 241
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/dial/RemoteDeviceInfo;

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/RemoteDeviceManager;->handleActiveRemoteDevice(Lcom/google/android/music/dial/RemoteDeviceInfo;)V

    move v0, v1

    .line 242
    goto :goto_0

    .line 245
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/dial/RemoteDeviceInfo;

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/RemoteDeviceManager;->handleInactiveRemoteDevice(Lcom/google/android/music/dial/RemoteDeviceInfo;)V

    move v0, v1

    .line 246
    goto :goto_0

    .line 249
    :pswitch_9
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->handleOfflineDevices()V

    move v0, v1

    .line 250
    goto :goto_0

    .line 211
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public onScanError(Lcom/google/android/music/dial/SsdpListener$ScanError;)V
    .locals 3
    .param p1, "error"    # Lcom/google/android/music/dial/SsdpListener$ScanError;

    .prologue
    .line 455
    sget-boolean v0, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v0, :cond_0

    .line 456
    const-string v0, "RemoteDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onScanError "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/music/dial/SsdpListener$ScanError;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 460
    return-void
.end method

.method public onSsdpMessage(Lcom/google/android/music/dial/SsdpMessage;)V
    .locals 3
    .param p1, "message"    # Lcom/google/android/music/dial/SsdpMessage;

    .prologue
    .line 442
    sget-boolean v0, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v0, :cond_0

    .line 443
    const-string v0, "RemoteDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSsdpMessage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/music/dial/SsdpMessage;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 446
    return-void
.end method

.method public startScan(Z)V
    .locals 3
    .param p1, "isActiveScan"    # Z

    .prologue
    .line 144
    sget-boolean v0, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v0, :cond_0

    .line 145
    const-string v0, "RemoteDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startScan "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSearchTarget:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/dial/RemoteDeviceManager;->stopDiscoveryScans()V

    .line 149
    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 150
    return-void

    .line 149
    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public stopScan()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 156
    sget-boolean v0, Lcom/google/android/music/dial/RemoteDeviceManager;->LOGV:Z

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "RemoteDeviceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopScan "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mSearchTarget:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 161
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 162
    return-void
.end method
