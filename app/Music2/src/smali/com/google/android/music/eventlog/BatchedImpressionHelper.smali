.class Lcom/google/android/music/eventlog/BatchedImpressionHelper;
.super Ljava/lang/Object;
.source "BatchedImpressionHelper.java"


# static fields
.field private static final DEBUG_LOG:Z


# instance fields
.field private final mAllDocuments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation
.end field

.field private mContinuous:Z

.field private mHandler:Landroid/os/Handler;

.field private final mLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

.field private final mNewDocuments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->EVENT_LOG:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->DEBUG_LOG:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/eventlog/MusicEventLogger;)V
    .locals 1
    .param p1, "musicEventLogger"    # Lcom/google/android/music/eventlog/MusicEventLogger;

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mAllDocuments:Ljava/util/HashSet;

    .line 68
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mNewDocuments:Ljava/util/HashSet;

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mContinuous:Z

    .line 81
    new-instance v0, Lcom/google/android/music/eventlog/BatchedImpressionHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/eventlog/BatchedImpressionHelper$1;-><init>(Lcom/google/android/music/eventlog/BatchedImpressionHelper;)V

    iput-object v0, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mHandler:Landroid/os/Handler;

    .line 106
    iput-object p1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    .line 107
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/eventlog/BatchedImpressionHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/eventlog/BatchedImpressionHelper;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->createEventImpl()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/eventlog/BatchedImpressionHelper;Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/eventlog/BatchedImpressionHelper;
    .param p1, "x1"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->insertDocumentImpl(Lcom/google/android/music/ui/cardlib/model/Document;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/eventlog/BatchedImpressionHelper;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/eventlog/BatchedImpressionHelper;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mAllDocuments:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/eventlog/BatchedImpressionHelper;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/eventlog/BatchedImpressionHelper;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mNewDocuments:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/music/eventlog/BatchedImpressionHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/eventlog/BatchedImpressionHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mContinuous:Z

    return p1
.end method

.method private createEventImpl()V
    .locals 3

    .prologue
    .line 158
    sget-boolean v0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->DEBUG_LOG:Z

    if-eqz v0, :cond_0

    .line 159
    const-string v0, "MusicImpressions"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Creating impression event. continuous="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mContinuous:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    iget-object v1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mNewDocuments:Ljava/util/HashSet;

    iget-boolean v2, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mContinuous:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/eventlog/MusicEventLogger;->logMainstageImpressions(Ljava/util/Collection;Z)V

    .line 162
    iget-object v0, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mNewDocuments:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 163
    iget-boolean v0, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mContinuous:Z

    if-nez v0, :cond_1

    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mContinuous:Z

    .line 167
    :cond_1
    return-void
.end method

.method private insertDocumentImpl(Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 5
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    const/4 v4, 0x1

    .line 136
    iget-object v1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mAllDocuments:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 137
    sget-boolean v1, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->DEBUG_LOG:Z

    if-eqz v1, :cond_0

    .line 138
    const-string v1, "MusicImpressions"

    const-string v2, "Dropping document, already added"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    sget-boolean v1, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->DEBUG_LOG:Z

    if-eqz v1, :cond_2

    .line 143
    const-string v1, "MusicImpressions"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adding new document to sets: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mAllDocuments:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mNewDocuments:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mAllDocuments:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 147
    iget-object v1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mNewDocuments:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 148
    iget-object v1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 149
    iget-object v1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 150
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method


# virtual methods
.method public insertDocument(Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 3
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 114
    iget-object v1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 115
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 116
    iget-object v1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 117
    return-void
.end method

.method public newSession()V
    .locals 2

    .prologue
    .line 123
    sget-boolean v0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->DEBUG_LOG:Z

    if-eqz v0, :cond_0

    .line 124
    const-string v0, "MusicImpressions"

    const-string v1, "New impression session"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 127
    return-void
.end method
