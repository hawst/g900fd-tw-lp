.class public Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList;
.super Lcom/google/android/music/medialist/NautilusAlbumList;
.source "ExploreNewReleasesAlbumList.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mGenreId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList$1;

    invoke-direct {v0}, Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList$1;-><init>()V

    sput-object v0, Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "genreId"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/music/medialist/NautilusAlbumList;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList;->mGenreId:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public getArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 29
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList;->mGenreId:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const-wide/16 v0, 0x0

    iget-object v2, p0, Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList;->mGenreId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContent$Explore;->getNewReleasesUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList;->mGenreId:Ljava/lang/String;

    return-object v0
.end method

.method public getNautilusId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList;->mGenreId:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList;->mGenreId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 57
    return-void
.end method
