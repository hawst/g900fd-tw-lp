.class public Lcom/google/android/music/xdi/XdiUtils;
.super Ljava/lang/Object;
.source "XdiUtils.java"


# static fields
.field private static final COUNT_COLUMNS:[Ljava/lang/String;

.field private static final LOGV:Z

.field private static final XDI_PACKAGE_NAMES:[Ljava/lang/String;

.field static final sRandom:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->XDI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/xdi/XdiUtils;->LOGV:Z

    .line 48
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/music/xdi/XdiUtils;->sRandom:Ljava/util/Random;

    .line 50
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_count"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/music/xdi/XdiUtils;->COUNT_COLUMNS:[Ljava/lang/String;

    .line 57
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.google.android.pano"

    aput-object v1, v0, v2

    const-string v1, "com.google.android.canvas"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/xdi/XdiUtils;->XDI_PACKAGE_NAMES:[Ljava/lang/String;

    return-void
.end method

.method static addLimitParam(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 7
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "offsetString"    # Ljava/lang/String;
    .param p2, "lengthString"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 818
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 819
    const-string v4, "MusicXdi"

    const-string v5, "Invalid offsetString."

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    :goto_0
    return-object v3

    .line 822
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 823
    const-string v4, "MusicXdi"

    const-string v5, "Invalid lengthString."

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 829
    :cond_1
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 834
    .local v2, "offset":I
    if-gez v2, :cond_2

    .line 835
    const-string v4, "MusicXdi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "offset has to be >= 0.  parsed value: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 830
    .end local v2    # "offset":I
    :catch_0
    move-exception v0

    .line 831
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v4, "MusicXdi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error parsing integer from offsetString: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 841
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v2    # "offset":I
    :cond_2
    :try_start_1
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 846
    .local v1, "length":I
    if-gtz v1, :cond_3

    .line 847
    const-string v4, "MusicXdi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "length has to be > 0.  parsed value: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 842
    .end local v1    # "length":I
    :catch_1
    move-exception v0

    .line 843
    .restart local v0    # "e":Ljava/lang/NumberFormatException;
    const-string v4, "MusicXdi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error parsing integer from lengthString: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 851
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v1    # "length":I
    :cond_3
    invoke-static {p0, v2, v1}, Lcom/google/android/music/store/MusicContent;->addLimitParam(Landroid/net/Uri;II)Landroid/net/Uri;

    move-result-object v3

    goto/16 :goto_0
.end method

.method static extractDataForAlbums(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/xdi/ProjectionMap;[Ljava/lang/Object;II)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "projectionMap"    # Lcom/google/android/music/xdi/ProjectionMap;
    .param p3, "values"    # [Ljava/lang/Object;
    .param p4, "imageWidth"    # I
    .param p5, "imageHeight"    # I

    .prologue
    .line 418
    new-instance v8, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v8}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    invoke-static {v8, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->getAlbumDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v5

    .line 420
    .local v5, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v1

    .line 422
    .local v1, "artworkUrl":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v0

    .line 423
    .local v0, "albumId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 424
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 425
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultAlbumArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 436
    :cond_0
    :goto_0
    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v7

    .line 437
    .local v7, "name":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v4

    .line 438
    .local v4, "description":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumDetailsIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 440
    .local v6, "intent":Landroid/content/Intent;
    const-string v8, "_id"

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {p2, p3, v8, v9}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 441
    const-string v8, "parent_id"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {p2, p3, v8, v9}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 443
    const-string v8, "display_name"

    invoke-virtual {p2, p3, v8, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 444
    const-string v8, "display_description"

    invoke-virtual {p2, p3, v8, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 445
    const-string v8, "image_uri"

    invoke-virtual {p2, p3, v8, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 446
    const-string v8, "width"

    const/4 v9, 0x0

    invoke-virtual {p2, p3, v8, v9}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 447
    const-string v8, "height"

    const/4 v9, 0x0

    invoke-virtual {p2, p3, v8, v9}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 448
    const-string v8, "intent_uri"

    const/4 v9, 0x1

    invoke-virtual {v6, v9}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, p3, v8, v9}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 450
    return-void

    .line 428
    .end local v4    # "description":Ljava/lang/String;
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v7    # "name":Ljava/lang/String;
    :cond_1
    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumId()J

    move-result-wide v2

    .line 429
    .local v2, "albumIdLong":J
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 430
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 431
    const/4 v8, 0x1

    invoke-static {v2, v3, v8, p4, p5}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method static extractDataForArtists(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/xdi/ProjectionMap;[Ljava/lang/Object;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "projectionMap"    # Lcom/google/android/music/xdi/ProjectionMap;
    .param p3, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v11, 0x0

    .line 463
    new-instance v7, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v7}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    invoke-static {v7, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->getArtistDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v4

    .line 465
    .local v4, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v1

    .line 466
    .local v1, "artworkUrl":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 467
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 470
    :cond_0
    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistMetajamId()Ljava/lang/String;

    move-result-object v0

    .line 471
    .local v0, "artistId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 472
    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistId()J

    move-result-wide v2

    .line 473
    .local v2, "artistIdLong":J
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 475
    .end local v2    # "artistIdLong":J
    :cond_1
    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v6

    .line 476
    .local v6, "name":Ljava/lang/String;
    new-instance v5, Landroid/content/Intent;

    const-string v7, "com.google.android.xdi.action.DETAIL"

    sget-object v8, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "details/artists/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v5, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 480
    .local v5, "intent":Landroid/content/Intent;
    const-string v7, "_id"

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {p2, p3, v7, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 481
    const-string v7, "parent_id"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {p2, p3, v7, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 483
    const-string v7, "display_name"

    invoke-virtual {p2, p3, v7, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 484
    const-string v7, "display_description"

    invoke-virtual {p2, p3, v7, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 485
    const-string v7, "image_uri"

    invoke-virtual {p2, p3, v7, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 486
    const-string v7, "width"

    invoke-virtual {p2, p3, v7, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 487
    const-string v7, "height"

    invoke-virtual {p2, p3, v7, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 488
    const-string v7, "intent_uri"

    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, p3, v7, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 490
    return-void
.end method

.method static extractDataForGroupType(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/xdi/ProjectionMap;[Ljava/lang/Object;III)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "projectionMap"    # Lcom/google/android/music/xdi/ProjectionMap;
    .param p3, "values"    # [Ljava/lang/Object;
    .param p4, "groupType"    # I
    .param p5, "imageWidth"    # I
    .param p6, "imageHeight"    # I

    .prologue
    .line 592
    packed-switch p4, :pswitch_data_0

    .line 608
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected group type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    const/4 v0, 0x0

    .line 612
    :goto_0
    return v0

    .line 594
    :pswitch_0
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/music/xdi/XdiUtils;->extractDataForSongs(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/xdi/ProjectionMap;[Ljava/lang/Object;)V

    .line 612
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p5

    move v5, p6

    .line 597
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/xdi/XdiUtils;->extractDataForAlbums(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/xdi/ProjectionMap;[Ljava/lang/Object;II)V

    goto :goto_1

    .line 601
    :pswitch_2
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/music/xdi/XdiUtils;->extractDataForArtists(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/xdi/ProjectionMap;[Ljava/lang/Object;)V

    goto :goto_1

    :pswitch_3
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p5

    move v5, p6

    .line 604
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/xdi/XdiUtils;->extractDataForSharedPlaylists(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/xdi/ProjectionMap;[Ljava/lang/Object;II)V

    goto :goto_1

    .line 592
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static extractDataForSharedPlaylists(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/xdi/ProjectionMap;[Ljava/lang/Object;II)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "projectionMap"    # Lcom/google/android/music/xdi/ProjectionMap;
    .param p3, "values"    # [Ljava/lang/Object;
    .param p4, "imageWidth"    # I
    .param p5, "imageHeight"    # I

    .prologue
    .line 518
    new-instance v7, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v7}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    invoke-static {v7, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->getPlaylistDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v3

    .line 520
    .local v3, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v8

    .line 521
    .local v8, "playlistId":J
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v4

    .line 522
    .local v4, "encodedArtUrl":Ljava/lang/String;
    const/4 v0, 0x0

    .line 524
    .local v0, "artUrl":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 525
    invoke-static {v4}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 526
    .local v1, "artUrls":[Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v7, v1

    if-lez v7, :cond_0

    .line 528
    const/4 v7, 0x0

    aget-object v0, v1, v7

    .line 534
    .end local v1    # "artUrls":[Ljava/lang/String;
    :cond_0
    if-nez v0, :cond_1

    .line 535
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 538
    :cond_1
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistName()Ljava/lang/String;

    move-result-object v6

    .line 539
    .local v6, "name":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getDescription()Ljava/lang/String;

    move-result-object v2

    .line 541
    .local v2, "description":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v5

    .line 542
    .local v5, "intent":Landroid/content/Intent;
    const-string v7, "container"

    const/16 v10, 0xb

    invoke-virtual {v5, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 543
    const-string v7, "id"

    invoke-virtual {v5, v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 544
    const-string v7, "offset"

    const/4 v10, 0x0

    invoke-virtual {v5, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 545
    const-string v7, "name"

    invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 546
    const-string v7, "playlist_type"

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v10

    invoke-virtual {v5, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 547
    const-string v7, "playlist_share_token"

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistShareToken()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 548
    const-string v7, "description"

    invoke-virtual {v5, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 549
    const-string v7, "owner_name"

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistOwnerName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 550
    const-string v7, "playlist_owner_photo_url"

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getProfilePhotoUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 552
    const-string v7, "art_uri"

    invoke-virtual {v5, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 554
    const-string v7, "_id"

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {p2, p3, v7, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 555
    const-string v7, "parent_id"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {p2, p3, v7, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 557
    const-string v7, "display_name"

    invoke-virtual {p2, p3, v7, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 558
    const-string v7, "display_description"

    invoke-virtual {p2, p3, v7, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 559
    const-string v7, "image_uri"

    invoke-virtual {p2, p3, v7, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 560
    const-string v7, "width"

    const/4 v10, 0x0

    invoke-virtual {p2, p3, v7, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 561
    const-string v7, "height"

    const/4 v10, 0x0

    invoke-virtual {p2, p3, v7, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 562
    const-string v7, "intent_uri"

    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p2, p3, v7, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 564
    return-void
.end method

.method static extractDataForSongs(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/xdi/ProjectionMap;[Ljava/lang/Object;)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "projectionMap"    # Lcom/google/android/music/xdi/ProjectionMap;
    .param p3, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v10, 0x0

    .line 369
    new-instance v6, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v6}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    invoke-static {v6, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->getTrackDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v2

    .line 371
    .local v2, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v1

    .line 372
    .local v1, "artworkUrl":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 373
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 376
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v5

    .line 377
    .local v5, "name":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumName()Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, "albumName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v3

    .line 380
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v4

    .line 382
    .local v4, "metajamTrackId":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 383
    const-string v6, "container"

    const/4 v7, 0x7

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 385
    const-string v6, "id_string"

    invoke-virtual {v3, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 390
    :goto_0
    const-string v6, "name"

    invoke-virtual {v3, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 391
    const-string v6, "art_uri"

    invoke-virtual {v3, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 393
    const-string v6, "_id"

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p2, p3, v6, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 394
    const-string v6, "parent_id"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p2, p3, v6, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 396
    const-string v6, "display_name"

    invoke-virtual {p2, p3, v6, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 397
    const-string v6, "display_description"

    invoke-virtual {p2, p3, v6, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 398
    const-string v6, "image_uri"

    invoke-virtual {p2, p3, v6, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 399
    const-string v6, "width"

    invoke-virtual {p2, p3, v6, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 400
    const-string v6, "height"

    invoke-virtual {p2, p3, v6, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 401
    const-string v6, "intent_uri"

    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, p3, v6, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 403
    return-void

    .line 387
    :cond_1
    const-string v6, "container"

    const/4 v7, 0x6

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 388
    const-string v6, "id"

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v8

    invoke-virtual {v3, v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_0
.end method

.method static extractDataFromItemCursor(Landroid/database/Cursor;[Ljava/lang/Object;)V
    .locals 8
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1235
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    sget-object v1, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    .line 1238
    .local v0, "projectionMap":Lcom/google/android/music/xdi/ProjectionMap;
    invoke-interface {p0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1239
    const-string v1, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1242
    :cond_0
    invoke-interface {p0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1243
    const-string v1, "parent_id"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1246
    :cond_1
    invoke-interface {p0, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1247
    const-string v1, "display_name"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1250
    :cond_2
    invoke-interface {p0, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1251
    const-string v1, "display_description"

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1254
    :cond_3
    invoke-interface {p0, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1255
    const-string v1, "image_uri"

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1258
    :cond_4
    const/4 v1, 0x5

    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1259
    const-string v1, "width"

    const/4 v2, 0x5

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1262
    :cond_5
    const/4 v1, 0x6

    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1263
    const-string v1, "height"

    const/4 v2, 0x6

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1266
    :cond_6
    const/4 v1, 0x7

    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1267
    const-string v1, "intent_uri"

    const/4 v2, 0x7

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1270
    :cond_7
    return-void
.end method

.method static extractTrackIds(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 738
    .local p0, "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 739
    .local v2, "songIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 740
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 742
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_0
    return-object v2
.end method

.method public static extractTrackMetajamIds(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 721
    .local p0, "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 722
    .local v3, "songIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 723
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v2

    .line 724
    .local v2, "id":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 725
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 728
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v2    # "id":Ljava/lang/String;
    :cond_1
    return-object v3
.end method

.method static getAlbumArtUri(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    const/4 v6, -0x1

    .line 1002
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v4

    .line 1005
    .local v4, "isNautilusAlbum":Z
    if-eqz v4, :cond_0

    .line 1006
    invoke-static {p0, p1}, Lcom/google/android/music/xdi/XdiUtils;->getNautilusAlbumArtUri(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1020
    :goto_0
    return-object v0

    .line 1010
    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1015
    .local v2, "id":J
    const/4 v5, 0x1

    invoke-static {v2, v3, v5, v6, v6}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .local v0, "albumArtUrl":Ljava/lang/String;
    goto :goto_0

    .line 1011
    .end local v0    # "albumArtUrl":Ljava/lang/String;
    .end local v2    # "id":J
    :catch_0
    move-exception v1

    .line 1012
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string v5, "MusicXdi"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error parsing long value from album ID: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static getAlbumArtUriOrDefault(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 1032
    invoke-static {p0, p1}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumArtUri(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1033
    .local v0, "albumArt":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1034
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultAlbumArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1036
    :cond_0
    return-object v0
.end method

.method static getAlbumCursor(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 864
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v11

    .line 866
    .local v11, "isNautilusId":Z
    if-eqz v11, :cond_0

    .line 867
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Albums;->getNautilusAlbumsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v7, 0x1

    move-object v0, p0

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v3

    .line 879
    :goto_0
    return-object v3

    .line 872
    :cond_0
    const-wide/16 v8, 0x0

    .line 874
    .local v8, "albumIdLong":J
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    .line 879
    invoke-static {v8, v9}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move v7, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_0

    .line 875
    :catch_0
    move-exception v10

    .line 876
    .local v10, "e":Ljava/lang/NumberFormatException;
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error converting to long: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static getAlbumDetailsIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "albumId"    # Ljava/lang/String;

    .prologue
    .line 96
    sget-object v1, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "details/albums"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/xdi/XdiContract;->getDetailsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 99
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "start_index"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 101
    return-object v0
.end method

.method static getAlbumSongCount(Landroid/content/Context;Ljava/lang/String;)I
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1295
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1296
    invoke-static {p0, p1, v1}, Lcom/google/android/music/store/MusicContent$Albums;->getAudioInNautilusAlbumCount(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v1

    .line 1306
    :goto_0
    return v1

    .line 1301
    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1306
    .local v2, "id":J
    invoke-static {p0, v2, v3, v1}, Lcom/google/android/music/store/MusicContent$Albums;->getAudioInAlbumCount(Landroid/content/Context;JZ)I

    move-result v1

    goto :goto_0

    .line 1302
    .end local v2    # "id":J
    :catch_0
    move-exception v0

    .line 1303
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v4, "MusicXdi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error parsing long value from album ID: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static getArtistCursor(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistId"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 929
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v11

    .line 931
    .local v11, "isNautilusId":Z
    if-eqz v11, :cond_0

    .line 932
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Artists;->getNautilusArtistsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v7, 0x1

    move-object v0, p0

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v3

    .line 945
    :goto_0
    return-object v3

    .line 938
    :cond_0
    const-wide/16 v8, 0x0

    .line 940
    .local v8, "artistIdLong":J
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    .line 945
    invoke-static {v8, v9}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move v7, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_0

    .line 941
    :catch_0
    move-exception v10

    .line 942
    .local v10, "e":Ljava/lang/NumberFormatException;
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error converting to long: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static getArtistDetailsIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "artistId"    # Ljava/lang/String;

    .prologue
    .line 111
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "details/artists"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/xdi/XdiContract;->getDetailsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static getArtistMetajamId(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistId"    # J

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 756
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "ArtistMetajamId"

    aput-object v0, v2, v1

    .line 759
    .local v2, "cols":[Ljava/lang/String;
    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 761
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 771
    :goto_0
    return-object v3

    .line 765
    :cond_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 766
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 771
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method static getAudioInAlbumCursor(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 896
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v11

    .line 898
    .local v11, "isNautilusId":Z
    if-eqz v11, :cond_0

    .line 899
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Albums;->getAudioInNautilusAlbumUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v7, 0x1

    move-object v0, p0

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v3

    .line 912
    :goto_0
    return-object v3

    .line 905
    :cond_0
    const-wide/16 v8, 0x0

    .line 907
    .local v8, "albumIdLong":J
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    .line 912
    invoke-static {v8, v9}, Lcom/google/android/music/store/MusicContent$Albums;->getAudioInAlbumUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move v7, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_0

    .line 908
    :catch_0
    move-exception v10

    .line 909
    .local v10, "e":Ljava/lang/NumberFormatException;
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error converting to long: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getClusterProjection(I)[Ljava/lang/String;
    .locals 3
    .param p0, "groupType"    # I

    .prologue
    .line 574
    packed-switch p0, :pswitch_data_0

    .line 584
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid group type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 576
    :pswitch_0
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->SONG_COLUMNS:[Ljava/lang/String;

    goto :goto_0

    .line 578
    :pswitch_1
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->ALBUM_COLUMNS:[Ljava/lang/String;

    goto :goto_0

    .line 580
    :pswitch_2
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->ARTIST_COLUMNS:[Ljava/lang/String;

    goto :goto_0

    .line 582
    :pswitch_3
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->SHARED_WITH_ME_PLAYLIST_COLUMNS:[Ljava/lang/String;

    goto :goto_0

    .line 574
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static getDefaultAlbumArtUri(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 312
    const v0, 0x7f02003a

    invoke-static {p0, v0}, Lcom/google/android/music/xdi/UriUtils;->getAndroidResourceUri(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 327
    const v0, 0x7f02003a

    invoke-static {p0, v0}, Lcom/google/android/music/xdi/UriUtils;->getAndroidResourceUri(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getDefaultArtistArtUri(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 315
    const v0, 0x7f02003a

    invoke-static {p0, v0}, Lcom/google/android/music/xdi/UriUtils;->getAndroidResourceUri(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getDefaultArtistItemHeightDp(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 290
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method static getDefaultArtistItemWidthDp(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 258
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method static getDefaultBadgeUri(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 330
    const v0, 0x7f02003a

    invoke-static {p0, v0}, Lcom/google/android/music/xdi/UriUtils;->getAndroidResourceUri(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getDefaultGenreArtUri(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 321
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getDefaultItemHeightDp(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 287
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method static getDefaultItemHeightPx(Landroid/content/Context;)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 302
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightDp(Landroid/content/Context;)I

    move-result v0

    int-to-float v0, v0

    invoke-static {p0, v0}, Lcom/google/android/music/xdi/DipUtil;->getPxFromDip(Landroid/content/Context;F)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method static getDefaultItemWidthDp(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 255
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method static getDefaultItemWidthPx(Landroid/content/Context;)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 270
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthDp(Landroid/content/Context;)I

    move-result v0

    int-to-float v0, v0

    invoke-static {p0, v0}, Lcom/google/android/music/xdi/DipUtil;->getPxFromDip(Landroid/content/Context;F)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method static getDefaultPlaylistArtUri(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 318
    const v0, 0x7f02003a

    invoke-static {p0, v0}, Lcom/google/android/music/xdi/UriUtils;->getAndroidResourceUri(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getDefaultRadioArtUri(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 324
    const v0, 0x7f02003a

    invoke-static {p0, v0}, Lcom/google/android/music/xdi/UriUtils;->getAndroidResourceUri(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getDisplayDimensions(Landroid/content/Context;)Landroid/graphics/Point;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 341
    const-string v3, "window"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 342
    .local v2, "wm":Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 343
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 344
    .local v1, "size":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 345
    return-object v1
.end method

.method static getGenreArtUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "genreId"    # Ljava/lang/String;
    .param p2, "parentGenreId"    # Ljava/lang/String;

    .prologue
    .line 979
    invoke-static {p0, p1, p2}, Lcom/google/android/music/utils/MusicUtils;->getArtUrlsForGenreRadio(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 980
    .local v1, "encodedArtUrl":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 981
    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 982
    .local v0, "artUrls":[Ljava/lang/String;
    if-eqz v0, :cond_0

    array-length v2, v0

    if-lez v2, :cond_0

    .line 984
    const/4 v2, 0x0

    aget-object v2, v0, v2

    .line 990
    .end local v0    # "artUrls":[Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_0
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method static getGenreIdForIntentPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "genreId"    # Ljava/lang/String;

    .prologue
    .line 1280
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1281
    const-string p0, "null"

    .line 1283
    .end local p0    # "genreId":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method static getLockerSongsByArtist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 682
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 683
    new-instance v13, Lcom/google/android/music/store/TagNormalizer;

    invoke-direct {v13}, Lcom/google/android/music/store/TagNormalizer;-><init>()V

    .line 684
    .local v13, "normalizer":Lcom/google/android/music/store/TagNormalizer;
    invoke-static {v13, p1}, Lcom/google/android/music/store/Store;->canonicalizeAndGenerateId(Lcom/google/android/music/store/TagNormalizer;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v12

    .line 686
    .local v12, "idAndCanonical":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    iget-object v0, v12, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 696
    .end local v12    # "idAndCanonical":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    .end local v13    # "normalizer":Lcom/google/android/music/store/TagNormalizer;
    .local v8, "artistIdLong":J
    :goto_0
    invoke-static {v8, v9, v3}, Lcom/google/android/music/store/MusicContent$Artists;->getAudioByArtistUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->SONG_COLUMNS:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move v7, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v10

    .line 702
    .local v10, "cursor":Landroid/database/Cursor;
    if-nez v10, :cond_1

    .line 703
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 709
    .end local v8    # "artistIdLong":J
    .end local v10    # "cursor":Landroid/database/Cursor;
    :goto_1
    return-object v0

    .line 689
    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    .restart local v8    # "artistIdLong":J
    goto :goto_0

    .line 690
    .end local v8    # "artistIdLong":J
    :catch_0
    move-exception v11

    .line 691
    .local v11, "e":Ljava/lang/NumberFormatException;
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error converting artist ID to long: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1

    .line 707
    .end local v11    # "e":Ljava/lang/NumberFormatException;
    .restart local v8    # "artistIdLong":J
    .restart local v10    # "cursor":Landroid/database/Cursor;
    :cond_1
    :try_start_1
    invoke-static {v10}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->buildTrackDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 709
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method static getMetaTitleUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "title"    # Ljava/lang/String;

    .prologue
    .line 152
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "metatitle"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static getMetaUri(J)Landroid/net/Uri;
    .locals 2
    .param p0, "metaId"    # J

    .prologue
    .line 139
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "meta"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static getNautilusAlbumArtUri(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nautilusAlbumId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1048
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1049
    const-string v3, "MusicXdi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid nautilus album ID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    :cond_0
    :goto_0
    return-object v2

    .line 1053
    :cond_1
    const/4 v3, 0x1

    new-array v1, v3, [Ljava/lang/String;

    const-string v3, "album_art"

    aput-object v3, v1, v4

    .line 1056
    .local v1, "cols":[Ljava/lang/String;
    invoke-static {p0, p1, v1}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumCursor(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1057
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 1061
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1062
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1067
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2
.end method

.method static getPlaylistCursor(Landroid/content/Context;J[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistId"    # J
    .param p3, "projection"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 961
    sget-object v0, Lcom/google/android/music/store/MusicContent$Playlists;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v0, p0

    move-object v2, p3

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static getPlaylistDetailsIntent(J)Landroid/content/Intent;
    .locals 4
    .param p0, "playlistId"    # J

    .prologue
    .line 123
    sget-object v1, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "details/playlists"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/xdi/XdiContract;->getDetailsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 126
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "start_index"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 128
    return-object v0
.end method

.method static getSelectedAccount(Landroid/content/Context;)Landroid/accounts/Account;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 176
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 177
    .local v1, "reference":Ljava/lang/Object;
    invoke-static {p0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 179
    .local v0, "musicPrefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 181
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method public static getSongListForPlaylistCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/medialist/SongList;
    .locals 24
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1106
    if-nez p1, :cond_0

    .line 1107
    const/4 v3, 0x0

    .line 1172
    :goto_0
    return-object v3

    .line 1110
    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1111
    .local v4, "id":J
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1113
    .local v7, "type":I
    const/4 v6, 0x0

    .line 1114
    .local v6, "name":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1115
    .local v8, "description":Ljava/lang/String;
    const/4 v9, 0x0

    .line 1116
    .local v9, "ownerName":Ljava/lang/String;
    const/4 v10, 0x0

    .line 1117
    .local v10, "shareToken":Ljava/lang/String;
    const/4 v11, 0x0

    .line 1118
    .local v11, "artUrl":Ljava/lang/String;
    const/4 v12, 0x0

    .line 1120
    .local v12, "ownerProfilePhotoUrl":Ljava/lang/String;
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1121
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1123
    :cond_1
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1124
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1126
    :cond_2
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1127
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1129
    :cond_3
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1130
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1132
    :cond_4
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1133
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1135
    :cond_5
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_6

    .line 1136
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1141
    :cond_6
    sparse-switch v7, :sswitch_data_0

    .line 1171
    const-string v3, "MusicXdi"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Unexpected playlist type: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v3, v13}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 1172
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 1144
    :sswitch_0
    new-instance v23, Ljava/lang/Object;

    invoke-direct/range {v23 .. v23}, Ljava/lang/Object;-><init>()V

    .line 1145
    .local v23, "reference":Ljava/lang/Object;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    .line 1148
    .local v2, "musicPrefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v22

    .line 1149
    .local v22, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-static {v4, v5, v3, v0}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 1151
    invoke-static/range {v23 .. v23}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto/16 :goto_0

    .end local v22    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :catchall_0
    move-exception v3

    invoke-static/range {v23 .. v23}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v3

    .line 1160
    .end local v2    # "musicPrefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v23    # "reference":Ljava/lang/Object;
    :sswitch_1
    new-instance v3, Lcom/google/android/music/medialist/PlaylistSongList;

    const/4 v13, 0x0

    invoke-direct/range {v3 .. v13}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 1166
    :sswitch_2
    new-instance v13, Lcom/google/android/music/medialist/SharedWithMeSongList;

    move-wide v14, v4

    move-object/from16 v16, v10

    move-object/from16 v17, v6

    move-object/from16 v18, v8

    move-object/from16 v19, v9

    move-object/from16 v20, v11

    move-object/from16 v21, v12

    invoke-direct/range {v13 .. v21}, Lcom/google/android/music/medialist/SharedWithMeSongList;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v13

    goto/16 :goto_0

    .line 1141
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_1
        0x32 -> :sswitch_1
        0x3c -> :sswitch_1
        0x46 -> :sswitch_2
        0x47 -> :sswitch_1
        0x64 -> :sswitch_0
    .end sparse-switch
.end method

.method static getTopSongsByArtist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 628
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->isNautilusEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 629
    sget-boolean v0, Lcom/google/android/music/xdi/XdiUtils;->LOGV:Z

    if-eqz v0, :cond_0

    .line 630
    const-string v0, "MusicXdi"

    const-string v1, "Nautilus not enabled for user.  Return empty top songs."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 663
    :goto_0
    return-object v0

    .line 635
    :cond_1
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 638
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 643
    .local v8, "artistIdLong":Ljava/lang/Long;
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/google/android/music/xdi/XdiUtils;->getArtistMetajamId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    .line 644
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 645
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not get nautilus ID for artist: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 639
    .end local v8    # "artistIdLong":Ljava/lang/Long;
    :catch_0
    move-exception v10

    .line 640
    .local v10, "e":Ljava/lang/NumberFormatException;
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error converting artist ID to long: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 650
    .end local v10    # "e":Ljava/lang/NumberFormatException;
    :cond_2
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Artists;->getTopSongsByArtistUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->SONG_COLUMNS:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v9

    .line 656
    .local v9, "cursor":Landroid/database/Cursor;
    if-nez v9, :cond_3

    .line 657
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 661
    :cond_3
    :try_start_1
    invoke-static {v9}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->buildTrackDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 663
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static hasPlaylists(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 228
    sget-object v1, Lcom/google/android/music/store/MusicContent$Playlists;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/music/xdi/XdiUtils;->COUNT_COLUMNS:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v8

    .line 233
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_0

    .line 240
    :goto_0
    return v6

    .line 238
    :cond_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_1

    .line 240
    :goto_1
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v6, v7

    goto :goto_0

    :cond_1
    move v7, v6

    .line 238
    goto :goto_1

    .line 240
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method static hasSubgenres(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "genreId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 209
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Explore;->getGenresUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/XdiUtils;->COUNT_COLUMNS:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 213
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 220
    :goto_0
    return v7

    .line 218
    :cond_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 220
    :goto_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v7, v0

    goto :goto_0

    :cond_1
    move v0, v7

    .line 218
    goto :goto_1

    .line 220
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method static isNautilusEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 196
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 197
    .local v1, "reference":Ljava/lang/Object;
    invoke-static {p0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 199
    .local v0, "musicPrefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 201
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return v2

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method public static isXdiClientInstalled(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 164
    sget-object v0, Lcom/google/android/music/xdi/XdiUtils;->XDI_PACKAGE_NAMES:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 165
    .local v3, "pkg":Ljava/lang/String;
    invoke-static {p0, v3}, Lcom/google/android/music/utils/SystemUtils;->isPackageInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 166
    const/4 v4, 0x1

    .line 169
    .end local v3    # "pkg":Ljava/lang/String;
    :goto_1
    return v4

    .line 164
    .restart local v3    # "pkg":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 169
    .end local v3    # "pkg":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static isXdiEnvironment(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isGlass()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->isXdiClientInstalled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static launchXdiInterfaceIfNeeded(Landroid/app/Activity;)Z
    .locals 2
    .param p0, "hostActivity"    # Landroid/app/Activity;

    .prologue
    .line 78
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->isXdiEnvironment(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 79
    const/4 v1, 0x0

    .line 86
    :goto_0
    return v1

    .line 82
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/xdi/XdiLauncherActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 83
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 84
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 85
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 86
    const/4 v1, 0x1

    goto :goto_0
.end method

.method static newPanoPlayIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 355
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.music.xdi.intent.PLAY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static setSonglistInfoInIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Landroid/content/Intent;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1185
    const/4 v1, 0x0

    .line 1186
    .local v1, "name":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1187
    .local v0, "description":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1188
    .local v2, "ownerName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1189
    .local v6, "shareToken":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1190
    .local v5, "playlistArtUrl":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1192
    .local v3, "ownerProfilePhotoUrl":Ljava/lang/String;
    instance-of v7, p1, Lcom/google/android/music/medialist/AutoPlaylistSongList;

    if-eqz v7, :cond_0

    move-object v4, p1

    .line 1193
    check-cast v4, Lcom/google/android/music/medialist/AutoPlaylistSongList;

    .line 1194
    .local v4, "pl":Lcom/google/android/music/medialist/AutoPlaylistSongList;
    invoke-virtual {v4, p0}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1216
    .end local v4    # "pl":Lcom/google/android/music/medialist/AutoPlaylistSongList;
    :goto_0
    const-string v7, "name"

    invoke-virtual {p2, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "description"

    invoke-virtual {v7, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "owner_name"

    invoke-virtual {v7, v8, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "playlist_share_token"

    invoke-virtual {v7, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "art_uri"

    invoke-virtual {v7, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "playlist_owner_photo_url"

    invoke-virtual {v7, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1223
    :goto_1
    return-void

    .line 1195
    :cond_0
    instance-of v7, p1, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v7, :cond_1

    move-object v4, p1

    .line 1196
    check-cast v4, Lcom/google/android/music/medialist/PlaylistSongList;

    .line 1197
    .local v4, "pl":Lcom/google/android/music/medialist/PlaylistSongList;
    invoke-virtual {v4, p0}, Lcom/google/android/music/medialist/PlaylistSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1198
    invoke-virtual {v4}, Lcom/google/android/music/medialist/PlaylistSongList;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 1199
    invoke-virtual {v4}, Lcom/google/android/music/medialist/PlaylistSongList;->getOwnerName()Ljava/lang/String;

    move-result-object v2

    .line 1200
    invoke-virtual {v4}, Lcom/google/android/music/medialist/PlaylistSongList;->getShareToken()Ljava/lang/String;

    move-result-object v6

    .line 1201
    invoke-virtual {v4}, Lcom/google/android/music/medialist/PlaylistSongList;->getArtUrl()Ljava/lang/String;

    move-result-object v5

    .line 1202
    invoke-virtual {v4}, Lcom/google/android/music/medialist/PlaylistSongList;->getOwnerProfilePhotoUrl()Ljava/lang/String;

    move-result-object v3

    .line 1203
    goto :goto_0

    .end local v4    # "pl":Lcom/google/android/music/medialist/PlaylistSongList;
    :cond_1
    instance-of v7, p1, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-eqz v7, :cond_2

    move-object v4, p1

    .line 1204
    check-cast v4, Lcom/google/android/music/medialist/SharedWithMeSongList;

    .line 1205
    .local v4, "pl":Lcom/google/android/music/medialist/SharedWithMeSongList;
    invoke-virtual {v4, p0}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1206
    invoke-virtual {v4}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 1207
    invoke-virtual {v4}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getOwnerName()Ljava/lang/String;

    move-result-object v2

    .line 1208
    invoke-virtual {v4}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getShareToken()Ljava/lang/String;

    move-result-object v6

    .line 1209
    invoke-virtual {v4}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getArtUrl()Ljava/lang/String;

    move-result-object v5

    .line 1210
    invoke-virtual {v4}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getOwnerProfilePhotoUrl()Ljava/lang/String;

    move-result-object v3

    .line 1211
    goto :goto_0

    .line 1212
    .end local v4    # "pl":Lcom/google/android/music/medialist/SharedWithMeSongList;
    :cond_2
    const-string v7, "MusicXdi"

    const-string v8, "Unsupported playlist type."

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
