.class public Lcom/google/android/music/utils/VoiceActionHelper;
.super Ljava/lang/Object;
.source "VoiceActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;,
        Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private final CURSOR_COLUMNS:[Ljava/lang/String;

.field private final FOCUS_AUDIO:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mFallbackCategories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mFocusToCategory:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsCancelled:Z

.field private mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

.field private mSearchActivityString:Ljava/lang/String;

.field private mSearchItemName:Ljava/lang/String;

.field private mServerSearchQuery:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/music/utils/VoiceActionHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/utils/VoiceActionHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-boolean v3, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mIsCancelled:Z

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "artist"

    aput-object v2, v1, v3

    const-string v2, "album"

    aput-object v2, v1, v4

    const-string v2, "track"

    aput-object v2, v1, v5

    const-string v2, "genre"

    aput-object v2, v1, v6

    const-string v2, "playlist"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mFallbackCategories:Ljava/util/List;

    .line 72
    const-string v0, "vnd.android.cursor.item/audio"

    iput-object v0, p0, Lcom/google/android/music/utils/VoiceActionHelper;->FOCUS_AUDIO:Ljava/lang/String;

    .line 74
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "searchType"

    aput-object v1, v0, v4

    const-string v1, "searchName"

    aput-object v1, v0, v5

    const-string v1, "artworkUrl"

    aput-object v1, v0, v6

    const-string v1, "Nid"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ListType"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "genreArtUris"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "genreServerId"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "name"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "parentGenreId"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "subgenreCount"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "AlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "SongId"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "Album"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Artist"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "AlbumArtist"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/music/utils/VoiceActionHelper;->CURSOR_COLUMNS:[Ljava/lang/String;

    .line 107
    new-instance v0, Lcom/google/android/music/utils/VoiceActionHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/utils/VoiceActionHelper$1;-><init>(Lcom/google/android/music/utils/VoiceActionHelper;)V

    iput-object v0, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mFocusToCategory:Ljava/util/HashMap;

    .line 96
    iput-object p1, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mContext:Landroid/content/Context;

    .line 97
    iput-object p2, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    .line 98
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/utils/VoiceActionHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/utils/VoiceActionHelper;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mIsCancelled:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/utils/VoiceActionHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/utils/VoiceActionHelper;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mSearchItemName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/utils/VoiceActionHelper;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/utils/VoiceActionHelper;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mFallbackCategories:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/utils/VoiceActionHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/utils/VoiceActionHelper;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/utils/VoiceActionHelper;)Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/utils/VoiceActionHelper;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/utils/VoiceActionHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/utils/VoiceActionHelper;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mSearchActivityString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/utils/VoiceActionHelper;Ljava/lang/String;ILcom/google/android/music/utils/VoiceActionHelper$SearchCallback;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/utils/VoiceActionHelper;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;
    .param p4, "x4"    # Z

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/music/utils/VoiceActionHelper;->lookupVoiceQuery(Ljava/lang/String;ILcom/google/android/music/utils/VoiceActionHelper$SearchCallback;Z)V

    return-void
.end method

.method private lookupVoiceQuery(Ljava/lang/String;ILcom/google/android/music/utils/VoiceActionHelper$SearchCallback;Z)V
    .locals 15
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "currentCategoryIndex"    # I
    .param p3, "callback"    # Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;
    .param p4, "eyesFree"    # Z

    .prologue
    .line 496
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mSearchActivityString:Ljava/lang/String;

    .line 497
    iget-object v1, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mFallbackCategories:Ljava/util/List;

    move/from16 v0, p2

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Search;->getSearchUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 499
    .local v11, "uri":Landroid/net/Uri;
    iget-object v10, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mContext:Landroid/content/Context;

    iget-object v12, p0, Lcom/google/android/music/utils/VoiceActionHelper;->CURSOR_COLUMNS:[Ljava/lang/String;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    new-instance v1, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    move-object v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move-object/from16 v5, p3

    move/from16 v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;-><init>(Lcom/google/android/music/utils/VoiceActionHelper;Ljava/lang/String;ILcom/google/android/music/utils/VoiceActionHelper$SearchCallback;Z)V

    move-object v2, v10

    move-object v3, v11

    move-object v4, v12

    move-object v5, v13

    move-object v6, v14

    move-object v10, v1

    invoke-static/range {v2 .. v10}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/music/utils/MusicUtils$QueryCallback;)V

    .line 501
    return-void
.end method


# virtual methods
.method public cancelSearch()V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mIsCancelled:Z

    .line 102
    return-void
.end method

.method public processVoiceQuery(Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;Z)V
    .locals 9
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "callback"    # Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;
    .param p4, "eyesFree"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 130
    iget-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/music/utils/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v2

    .line 131
    .local v2, "hasNetworkConnection":Z
    iget-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mContext:Landroid/content/Context;

    invoke-static {v5, v8}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    .line 134
    .local v3, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    iget-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    .line 137
    .local v1, "hasNautilus":Z
    iput-object p1, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mSearchItemName:Ljava/lang/String;

    .line 138
    iget-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mSearchItemName:Ljava/lang/String;

    if-nez v5, :cond_2

    const-string v5, ""

    :goto_0
    iput-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mSearchItemName:Ljava/lang/String;

    .line 139
    if-nez p2, :cond_3

    move-object v5, v6

    :goto_1
    iput-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mServerSearchQuery:Ljava/lang/String;

    .line 140
    iget-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mServerSearchQuery:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mSearchItemName:Ljava/lang/String;

    :goto_2
    iput-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mServerSearchQuery:Ljava/lang/String;

    .line 143
    iget-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mSearchItemName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 144
    iget-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mFocusToCategory:Ljava/util/HashMap;

    const-string v6, "android.intent.extra.focus"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 146
    .local v0, "focusCategory":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 147
    iget-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mFallbackCategories:Ljava/util/List;

    invoke-interface {v5, v7, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 152
    :cond_0
    if-eqz v1, :cond_5

    if-eqz v2, :cond_5

    const/4 v4, 0x1

    .line 153
    .local v4, "useNautilusSearch":Z
    :goto_3
    if-eqz v4, :cond_1

    .line 154
    iget-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mFallbackCategories:Ljava/util/List;

    const-string v6, "bestmatch"

    invoke-interface {v5, v7, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 156
    :cond_1
    if-eqz v4, :cond_6

    iget-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mServerSearchQuery:Ljava/lang/String;

    :goto_4
    invoke-direct {p0, v5, v7, p3, p4}, Lcom/google/android/music/utils/VoiceActionHelper;->lookupVoiceQuery(Ljava/lang/String;ILcom/google/android/music/utils/VoiceActionHelper$SearchCallback;Z)V

    .line 162
    .end local v0    # "focusCategory":Ljava/lang/String;
    .end local v4    # "useNautilusSearch":Z
    :goto_5
    return-void

    .line 138
    :cond_2
    iget-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mSearchItemName:Ljava/lang/String;

    goto :goto_0

    .line 139
    :cond_3
    const-string v5, "queryComplete"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 140
    :cond_4
    iget-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mServerSearchQuery:Ljava/lang/String;

    goto :goto_2

    .restart local v0    # "focusCategory":Ljava/lang/String;
    :cond_5
    move v4, v7

    .line 152
    goto :goto_3

    .line 156
    .restart local v4    # "useNautilusSearch":Z
    :cond_6
    iget-object v5, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mSearchItemName:Ljava/lang/String;

    goto :goto_4

    .line 159
    .end local v0    # "focusCategory":Ljava/lang/String;
    .end local v4    # "useNautilusSearch":Z
    :cond_7
    const/4 v5, -0x1

    iget-object v7, p0, Lcom/google/android/music/utils/VoiceActionHelper;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/google/android/music/mix/MixDescriptor;->getLuckyRadio(Landroid/content/Context;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v7

    invoke-interface {p3, v5, v6, v7}, Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;->onSearchComplete(ILcom/google/android/music/medialist/SongList;Lcom/google/android/music/mix/MixDescriptor;)V

    goto :goto_5
.end method
