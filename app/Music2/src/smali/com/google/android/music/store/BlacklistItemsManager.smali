.class public Lcom/google/android/music/store/BlacklistItemsManager;
.super Ljava/lang/Object;
.source "BlacklistItemsManager.java"


# static fields
.field private static final LOGV:Z

.field private static final UPDATE_BLACKLIST_ITEMS_MESSAGE_TYPE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/BlacklistItemsManager;->LOGV:Z

    .line 23
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-static {v0}, Lcom/google/android/music/utils/async/AsyncWorkers;->getUniqueMessageType(Lcom/google/android/music/utils/LoggableHandler;)I

    move-result v0

    sput v0, Lcom/google/android/music/store/BlacklistItemsManager;->UPDATE_BLACKLIST_ITEMS_MESSAGE_TYPE:I

    return-void
.end method

.method public static cleanupDuplicateItems(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    const/4 v2, 0x0

    .line 116
    .local v2, "success":Z
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    .line 117
    .local v1, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 119
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {v0}, Lcom/google/android/music/store/BlacklistItemsManager;->cleanupDuplicateItems(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    const/4 v2, 0x1

    .line 122
    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 124
    return-void

    .line 122
    :catchall_0
    move-exception v3

    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method private static cleanupDuplicateItems(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 86
    const-string v0, "Id IN (SELECT Id FROM MAINSTAGE_BLACKLIST AS MB1 WHERE EXISTS (SELECT 1 FROM MAINSTAGE_BLACKLIST AS MB2 WHERE (MB1.Id!=MB2.Id AND MB1.ReasonType=MB2.ReasonType AND (MB1.DismissDate<MB2.DismissDate OR (MB1.DismissDate=MB2.DismissDate AND MB1.Id<MB2.Id)) AND ((MB1.AlbumMetajamId NOT NULL AND MB2.AlbumMetajamId NOT NULL AND MB1.AlbumLocalId=MB2.AlbumLocalId AND MB1.AlbumMetajamId=MB2.AlbumMetajamId) OR (MB1.AlbumMetajamId IS NULL AND MB2.AlbumMetajamId IS NULL AND MB1.AlbumLocalId=MB2.AlbumLocalId) OR (MB1.ListShareToken=MB2.ListShareToken) OR (MB1.RadioSeedId=MB2.RadioSeedId AND MB1.RadioSeedType=MB2.RadioSeedType)))))"

    .line 87
    .local v0, "whereClause":Ljava/lang/String;
    const-string v1, "MAINSTAGE_BLACKLIST"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 88
    return-void
.end method

.method public static cleanupDuplicateItemsAsync(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 95
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    .line 97
    .local v0, "bgWorker":Landroid/os/Handler;
    sget v2, Lcom/google/android/music/store/BlacklistItemsManager;->UPDATE_BLACKLIST_ITEMS_MESSAGE_TYPE:I

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 99
    new-instance v2, Lcom/google/android/music/store/BlacklistItemsManager$1;

    invoke-direct {v2, p0}, Lcom/google/android/music/store/BlacklistItemsManager$1;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;

    move-result-object v1

    .line 105
    .local v1, "msg":Landroid/os/Message;
    sget v2, Lcom/google/android/music/store/BlacklistItemsManager;->UPDATE_BLACKLIST_ITEMS_MESSAGE_TYPE:I

    iput v2, v1, Landroid/os/Message;->what:I

    .line 106
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 107
    return-void
.end method
