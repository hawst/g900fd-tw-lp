.class public Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;
.super Lcom/google/android/music/ui/PlaylistDialogFragment;
.source "CreatePlaylistShortcutDialogFragment.java"


# instance fields
.field private mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    const v0, 0x7f0b00df

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/ui/PlaylistDialogFragment;-><init>(IZ)V

    .line 28
    new-instance v0, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment$1;-><init>(Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 24
    iget-object v0, p0, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 25
    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;->setShowNewPlaylist(Z)V

    .line 26
    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/google/android/music/ui/PlaylistDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 60
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 63
    :cond_0
    return-void
.end method
