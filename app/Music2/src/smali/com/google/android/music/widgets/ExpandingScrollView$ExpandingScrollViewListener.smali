.class public interface abstract Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;
.super Ljava/lang/Object;
.source "ExpandingScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/widgets/ExpandingScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ExpandingScrollViewListener"
.end annotation


# virtual methods
.method public abstract onDragEnded(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V
.end method

.method public abstract onDragStarted(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V
.end method

.method public abstract onExpandingStateChanged(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V
.end method

.method public abstract onMoving(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;F)V
.end method
