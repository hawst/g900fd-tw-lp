.class Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;
.super Landroid/database/MatrixCursor;
.source "DetailAlbumSectionsCursor.java"


# static fields
.field private static final PROJECTION_ALBUMS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "album_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "album_artist"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "albumId"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 51
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mContext:Landroid/content/Context;

    .line 52
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 54
    invoke-direct {p0, p3}, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->addSectionsForAlbum(Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method private addAlbumSection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;)V
    .locals 4
    .param p1, "sectionName"    # Ljava/lang/String;
    .param p2, "sectionPrimaryText"    # Ljava/lang/String;
    .param p3, "sectionSecondaryText"    # Ljava/lang/String;
    .param p4, "sectionType"    # I
    .param p5, "sectionId"    # I
    .param p6, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 167
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v1}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/Object;

    .line 169
    .local v0, "values":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "_id"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 170
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "name"

    invoke-virtual {v1, v0, v2, p1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 171
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "display_header"

    invoke-virtual {v1, v0, v2, p1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 173
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "display_name"

    invoke-virtual {v1, v0, v2, p2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 175
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "display_subname"

    invoke-virtual {v1, v0, v2, p3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 177
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "section_type"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 179
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "content_uri"

    invoke-virtual {p6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 182
    invoke-virtual {p0, v0}, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->addRow([Ljava/lang/Object;)V

    .line 183
    return-void
.end method

.method private addSectionsForAlbum(Ljava/lang/String;)V
    .locals 30
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 58
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v2, v0, v3}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumCursor(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 60
    .local v25, "cursor":Landroid/database/Cursor;
    if-nez v25, :cond_0

    .line 151
    :goto_0
    return-void

    .line 65
    :cond_0
    :try_start_0
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 66
    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 67
    .local v4, "albumName":Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 68
    .local v5, "artist":Ljava/lang/String;
    const/16 v24, 0x0

    .line 69
    .local v24, "artistMetajamId":Ljava/lang/String;
    const/4 v2, 0x3

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 70
    const/4 v2, 0x3

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 72
    :cond_1
    const-wide/16 v22, 0x0

    .line 73
    .local v22, "artistIdLong":J
    const/4 v2, 0x2

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 74
    const/4 v2, 0x2

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 77
    :cond_2
    const/16 v21, 0x0

    .line 78
    .local v21, "artistId":Ljava/lang/String;
    const-wide/16 v2, 0x0

    cmp-long v2, v22, v2

    if-lez v2, :cond_4

    .line 79
    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v21

    .line 87
    :goto_1
    const/4 v7, 0x0

    .line 89
    .local v7, "sectionId":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mContext:Landroid/content/Context;

    const v3, 0x7f0b02d1

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x2

    add-int/lit8 v13, v7, 0x1

    .end local v7    # "sectionId":I
    .local v13, "sectionId":I
    const-string v2, "album"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->getContentUriForAlbumDetailsSection(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->addAlbumSection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;)V

    .line 94
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mContext:Landroid/content/Context;

    const v3, 0x7f0b02d0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v12, 0x1

    add-int/lit8 v7, v13, 0x1

    .end local v13    # "sectionId":I
    .restart local v7    # "sectionId":I
    const-string v2, "tracks"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->getContentUriForAlbumDetailsSection(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    move-object/from16 v8, p0

    move-object v10, v4

    move-object v11, v5

    invoke-direct/range {v8 .. v14}, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->addAlbumSection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;)V

    .line 100
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mContext:Landroid/content/Context;

    const v3, 0x7f0b02d3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 102
    .local v15, "allAlbumsTitle":Ljava/lang/String;
    sget-object v2, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "details/artists"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "section"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "albums"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v20

    .line 108
    .local v20, "allAlbumsContentUri":Landroid/net/Uri;
    const/16 v17, 0x0

    const/16 v18, 0x1

    add-int/lit8 v13, v7, 0x1

    .end local v7    # "sectionId":I
    .restart local v13    # "sectionId":I
    move-object/from16 v14, p0

    move-object/from16 v16, v5

    move/from16 v19, v7

    invoke-direct/range {v14 .. v20}, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->addAlbumSection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;)V

    .line 113
    .end local v15    # "allAlbumsTitle":Ljava/lang/String;
    .end local v20    # "allAlbumsContentUri":Landroid/net/Uri;
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/xdi/XdiUtils;->isNautilusEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 115
    if-nez v24, :cond_7

    .line 116
    invoke-static/range {v21 .. v21}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 117
    move-object/from16 v27, v21

    .line 133
    .local v27, "possibleArtistMetajamId":Ljava/lang/String;
    :goto_3
    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 134
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00b1

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 136
    .local v9, "relatedArtistsTitle":Ljava/lang/String;
    sget-object v2, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "details/artists"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "section"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "related"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v14

    .line 142
    .local v14, "relatedArtistsContentUri":Landroid/net/Uri;
    const/4 v11, 0x0

    const/4 v12, 0x1

    add-int/lit8 v7, v13, 0x1

    .end local v13    # "sectionId":I
    .restart local v7    # "sectionId":I
    move-object/from16 v8, p0

    move-object v10, v9

    invoke-direct/range {v8 .. v14}, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->addAlbumSection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    .end local v4    # "albumName":Ljava/lang/String;
    .end local v5    # "artist":Ljava/lang/String;
    .end local v7    # "sectionId":I
    .end local v9    # "relatedArtistsTitle":Ljava/lang/String;
    .end local v14    # "relatedArtistsContentUri":Landroid/net/Uri;
    .end local v21    # "artistId":Ljava/lang/String;
    .end local v22    # "artistIdLong":J
    .end local v24    # "artistMetajamId":Ljava/lang/String;
    .end local v27    # "possibleArtistMetajamId":Ljava/lang/String;
    :cond_3
    invoke-static/range {v25 .. v25}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 80
    .restart local v4    # "albumName":Ljava/lang/String;
    .restart local v5    # "artist":Ljava/lang/String;
    .restart local v21    # "artistId":Ljava/lang/String;
    .restart local v22    # "artistIdLong":J
    .restart local v24    # "artistMetajamId":Ljava/lang/String;
    :cond_4
    :try_start_1
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 81
    move-object/from16 v21, v24

    goto/16 :goto_1

    .line 83
    :cond_5
    const-string v2, "MusicXdi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid artist ID in album "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 149
    .end local v4    # "albumName":Ljava/lang/String;
    .end local v5    # "artist":Ljava/lang/String;
    .end local v21    # "artistId":Ljava/lang/String;
    .end local v22    # "artistIdLong":J
    .end local v24    # "artistMetajamId":Ljava/lang/String;
    :catchall_0
    move-exception v2

    invoke-static/range {v25 .. v25}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    .line 119
    .restart local v4    # "albumName":Ljava/lang/String;
    .restart local v5    # "artist":Ljava/lang/String;
    .restart local v13    # "sectionId":I
    .restart local v21    # "artistId":Ljava/lang/String;
    .restart local v22    # "artistIdLong":J
    .restart local v24    # "artistMetajamId":Ljava/lang/String;
    :cond_6
    const-wide/16 v28, 0x0

    .line 121
    .local v28, "possibleArtistIdLong":J
    :try_start_2
    invoke-static/range {v21 .. v21}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v28

    .line 127
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;->mContext:Landroid/content/Context;

    move-wide/from16 v0, v28

    invoke-static {v2, v0, v1}, Lcom/google/android/music/xdi/XdiUtils;->getArtistMetajamId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v27

    .line 129
    .restart local v27    # "possibleArtistMetajamId":Ljava/lang/String;
    goto :goto_3

    .line 122
    .end local v27    # "possibleArtistMetajamId":Ljava/lang/String;
    :catch_0
    move-exception v26

    .line 123
    .local v26, "e":Ljava/lang/NumberFormatException;
    const-string v2, "MusicXdi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error converting artist id to long: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 149
    invoke-static/range {v25 .. v25}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 131
    .end local v26    # "e":Ljava/lang/NumberFormatException;
    .end local v28    # "possibleArtistIdLong":J
    :cond_7
    move-object/from16 v27, v24

    .restart local v27    # "possibleArtistMetajamId":Ljava/lang/String;
    goto/16 :goto_3

    .end local v13    # "sectionId":I
    .end local v27    # "possibleArtistMetajamId":Ljava/lang/String;
    .restart local v7    # "sectionId":I
    :cond_8
    move v13, v7

    .end local v7    # "sectionId":I
    .restart local v13    # "sectionId":I
    goto/16 :goto_2
.end method

.method private static getContentUriForAlbumDetailsSection(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "albumId"    # Ljava/lang/String;
    .param p1, "sectionPath"    # Ljava/lang/String;

    .prologue
    .line 193
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "details/albums"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "section"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
