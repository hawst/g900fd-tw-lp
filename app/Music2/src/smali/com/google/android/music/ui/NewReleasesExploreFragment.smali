.class public Lcom/google/android/music/ui/NewReleasesExploreFragment;
.super Lcom/google/android/music/ui/AlbumGridFragment;
.source "NewReleasesExploreFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/android/music/ui/AlbumGridFragment;-><init>()V

    return-void
.end method

.method private getGenreId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/google/android/music/ui/NewReleasesExploreFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 21
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 22
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Either genre level, genre ID or genre name is required."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 25
    :cond_0
    const-string v2, "nautilusId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 26
    .local v1, "genreId":Ljava/lang/String;
    return-object v1
.end method


# virtual methods
.method protected initEmptyScreen()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Lcom/google/android/music/ui/AlbumGridFragment;->initEmptyScreen()V

    .line 32
    const v0, 0x7f0200f2

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/NewReleasesExploreFragment;->setEmptyImageView(I)V

    .line 33
    const v0, 0x7f0b02a6

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/NewReleasesExploreFragment;->setEmptyScreenText(I)V

    .line 34
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 14
    invoke-super {p0, p1}, Lcom/google/android/music/ui/AlbumGridFragment;->onCreate(Landroid/os/Bundle;)V

    .line 15
    invoke-direct {p0}, Lcom/google/android/music/ui/NewReleasesExploreFragment;->getGenreId()Ljava/lang/String;

    move-result-object v0

    .line 16
    .local v0, "genreId":Ljava/lang/String;
    new-instance v1, Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList;

    invoke-direct {v1, v0}, Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/NewReleasesExploreFragment;->init(Lcom/google/android/music/medialist/AlbumList;)V

    .line 17
    return-void
.end method
