.class public Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;
.super Lcom/google/api/client/json/GenericJson;
.source "SkipToItemCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/model/SkipToItemCommandJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SkipToItemCommandBody"
.end annotation


# instance fields
.field public mItemId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "itemId"
    .end annotation
.end field

.field public mPlayOnCompletion:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "playOnCompletion"
    .end annotation
.end field

.field public mPositionMillis:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "positionMillis"
    .end annotation
.end field

.field public mQueueVersion:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "queueVersion"
    .end annotation
.end field

.field public mSessionId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "sessionId"
    .end annotation
.end field

.field public mTrackMetadata:Lcom/google/android/music/dial/model/TrackMetadataJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackMetadata"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
