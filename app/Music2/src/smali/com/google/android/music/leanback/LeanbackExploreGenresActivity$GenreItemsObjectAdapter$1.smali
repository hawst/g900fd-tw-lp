.class Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$1;
.super Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;
.source "LeanbackExploreGenresActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;-><init>(Lcom/google/android/music/leanback/ItemPresenter;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/ObjectAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->recalculateSizeAndIndices()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->access$600(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)V

    .line 308
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyChanged()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->access$700(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)V

    .line 309
    return-void
.end method

.method public onItemRangeChanged(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mFeaturedAdapterStartIndex:I
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->access$800(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)I

    move-result v1

    add-int/2addr v1, p1

    # invokes: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyItemRangeChanged(II)V
    invoke-static {v0, v1, p2}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->access$900(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;II)V

    .line 314
    return-void
.end method

.method public onItemRangeInserted(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->recalculateSizeAndIndices()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->access$600(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)V

    .line 319
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mFeaturedAdapterStartIndex:I
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->access$800(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)I

    move-result v1

    add-int/2addr v1, p1

    # invokes: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyItemRangeInserted(II)V
    invoke-static {v0, v1, p2}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->access$1000(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;II)V

    .line 320
    return-void
.end method

.method public onItemRangeRemoved(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->recalculateSizeAndIndices()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->access$600(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)V

    .line 325
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->mFeaturedAdapterStartIndex:I
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->access$800(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;)I

    move-result v1

    add-int/2addr v1, p1

    # invokes: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->notifyItemRangeRemoved(II)V
    invoke-static {v0, v1, p2}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->access$1100(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;II)V

    .line 326
    return-void
.end method
