.class final enum Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;
.super Ljava/lang/Enum;
.source "DialRemotePlaybackClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/DialRemotePlaybackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "GroupStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

.field public static final enum GONE:Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

.field public static final enum MOVED:Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 135
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

    const-string v1, "MOVED"

    const-string v2, "GROUP_STATUS_MOVED"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;->MOVED:Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

    .line 136
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

    const-string v1, "GONE"

    const-string v2, "GROUP_STATUS_GONE"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;->GONE:Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

    .line 134
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

    sget-object v1, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;->MOVED:Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;->GONE:Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;->$VALUES:[Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "v"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 140
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 141
    iput-object p3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;->value:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;
    .locals 7
    .param p0, "v"    # Ljava/lang/String;

    .prologue
    .line 145
    invoke-static {}, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;->values()[Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 146
    .local v3, "s":Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;
    iget-object v4, v3, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 147
    return-object v3

    .line 145
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 150
    .end local v3    # "s":Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;
    :cond_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid value "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 134
    const-class v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;->$VALUES:[Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

    invoke-virtual {v0}, [Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;

    return-object v0
.end method
