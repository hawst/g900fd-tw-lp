.class public Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;
.super Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
.source "CuratedRadioPlayCard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard$OnExpandListener;
    }
.end annotation


# instance fields
.field protected mImageOverlay:Landroid/view/View;

.field protected mImageView:Lcom/google/android/music/art/DocumentArtView;

.field private mIsExpanded:Z

.field protected mRadioIcon:Landroid/widget/ImageView;

.field protected mReadMoreView:Landroid/widget/TextView;

.field mReadMoreViewHitRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;)V

    .line 40
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreViewHitRect:Landroid/graphics/Rect;

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreViewHitRect:Landroid/graphics/Rect;

    .line 51
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V
    .locals 2
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p2, "contextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .prologue
    .line 232
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 233
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mImageView:Lcom/google/android/music/art/DocumentArtView;

    sget-object v1, Lcom/google/android/music/art/ArtType;->MAINSTAGE_CARD:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/music/art/DocumentArtView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/art/ArtType;)V

    .line 235
    instance-of v0, p1, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadioDocument;

    if-eqz v0, :cond_0

    .line 236
    check-cast p1, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadioDocument;

    .end local p1    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    iget-boolean v0, p1, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadioDocument;->mIsExpanded:Z

    iput-boolean v0, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mIsExpanded:Z

    .line 238
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 55
    invoke-super {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->onFinishInflate()V

    .line 56
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mDescription:Landroid/widget/TextView;

    const-string v1, "mDescription must be in curated radio card"

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const v0, 0x7f0e0105

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/art/DocumentArtView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mImageView:Lcom/google/android/music/art/DocumentArtView;

    .line 58
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mImageView:Lcom/google/android/music/art/DocumentArtView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/music/art/DocumentArtView;->setAspectRatio(F)V

    .line 59
    const v0, 0x7f0e0107

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mRadioIcon:Landroid/widget/ImageView;

    .line 60
    const v0, 0x7f0e0106

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mImageOverlay:Landroid/view/View;

    .line 61
    const v0, 0x7f0e010c

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreView:Landroid/widget/TextView;

    .line 62
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 242
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 243
    .local v1, "xPos":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 245
    .local v2, "yPos":F
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreViewHitRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    cmpg-float v3, v1, v3

    if-gtz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreViewHitRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    cmpl-float v3, v1, v3

    if-ltz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreViewHitRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    cmpl-float v3, v2, v3

    if-ltz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreViewHitRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    cmpg-float v3, v2, v3

    if-gtz v3, :cond_0

    const/4 v0, 0x1

    .line 249
    .local v0, "inside":Z
    :goto_0
    return v0

    .line 245
    .end local v0    # "inside":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 39
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 160
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->getPaddingLeft()I

    move-result v17

    .line 161
    .local v17, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->getPaddingRight()I

    move-result v18

    .line 162
    .local v18, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->getPaddingTop()I

    move-result v19

    .line 163
    .local v19, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->getPaddingBottom()I

    move-result v16

    .line 165
    .local v16, "paddingBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mImageView:Lcom/google/android/music/art/DocumentArtView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/music/art/DocumentArtView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 166
    .local v13, "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mTitle:Landroid/widget/TextView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v30

    check-cast v30, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 167
    .local v30, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mDescription:Landroid/widget/TextView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 169
    .local v8, "descriptionLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mImageView:Lcom/google/android/music/art/DocumentArtView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/music/art/DocumentArtView;->getMeasuredHeight()I

    move-result v11

    .line 170
    .local v11, "imageHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mImageView:Lcom/google/android/music/art/DocumentArtView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/music/art/DocumentArtView;->getMeasuredWidth()I

    move-result v15

    .line 171
    .local v15, "imageWidth":I
    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v33, v0

    add-int v12, v17, v33

    .line 172
    .local v12, "imageLeft":I
    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v33, v0

    add-int v14, v19, v33

    .line 173
    .local v14, "imageTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mImageView:Lcom/google/android/music/art/DocumentArtView;

    move-object/from16 v33, v0

    add-int v34, v12, v15

    add-int v35, v14, v11

    move-object/from16 v0, v33

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v12, v14, v1, v2}, Lcom/google/android/music/art/DocumentArtView;->layout(IIII)V

    .line 175
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mImageOverlay:Landroid/view/View;

    move-object/from16 v33, v0

    add-int v34, v12, v15

    add-int v35, v14, v11

    move-object/from16 v0, v33

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v12, v14, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mTitle:Landroid/widget/TextView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v32

    .line 179
    .local v32, "titleWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mTitle:Landroid/widget/TextView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v28

    .line 180
    .local v28, "titleHeight":I
    add-int v33, v17, v15

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v34, v0

    add-int v33, v33, v34

    move-object/from16 v0, v30

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v34, v0

    add-int v29, v33, v34

    .line 181
    .local v29, "titleLeft":I
    move-object/from16 v0, v30

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v33, v0

    add-int v31, v19, v33

    .line 182
    .local v31, "titleTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mTitle:Landroid/widget/TextView;

    move-object/from16 v33, v0

    add-int v34, v29, v32

    add-int v35, v31, v28

    move-object/from16 v0, v33

    move/from16 v1, v29

    move/from16 v2, v31

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mDescription:Landroid/widget/TextView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v10

    .line 185
    .local v10, "descriptionWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mDescription:Landroid/widget/TextView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    .line 186
    .local v6, "descriptionHeight":I
    add-int v33, v17, v15

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v34, v0

    add-int v33, v33, v34

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v34, v0

    add-int v7, v33, v34

    .line 188
    .local v7, "descriptionLeft":I
    add-int v33, v31, v28

    move-object/from16 v0, v30

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v34, v0

    add-int v33, v33, v34

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v34, v0

    add-int v9, v33, v34

    .line 190
    .local v9, "descriptionTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mDescription:Landroid/widget/TextView;

    move-object/from16 v33, v0

    add-int v34, v7, v10

    add-int v35, v9, v6

    move-object/from16 v0, v33

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v7, v9, v1, v2}, Landroid/widget/TextView;->layout(IIII)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreView:Landroid/widget/TextView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getVisibility()I

    move-result v33

    const/16 v34, 0x8

    move/from16 v0, v33

    move/from16 v1, v34

    if-eq v0, v1, :cond_0

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreView:Landroid/widget/TextView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v27

    .line 196
    .local v27, "readMoreWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreView:Landroid/widget/TextView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v25

    .line 197
    .local v25, "readMoreHeight":I
    add-int v24, v9, v6

    .line 198
    .local v24, "readMoreBottom":I
    add-int v26, v7, v10

    .line 199
    .local v26, "readMoreRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreView:Landroid/widget/TextView;

    move-object/from16 v33, v0

    sub-int v34, v26, v27

    sub-int v35, v24, v25

    move-object/from16 v0, v33

    move/from16 v1, v34

    move/from16 v2, v35

    move/from16 v3, v26

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreView:Landroid/widget/TextView;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreViewHitRect:Landroid/graphics/Rect;

    move-object/from16 v34, v0

    invoke-virtual/range {v33 .. v34}, Landroid/widget/TextView;->getHitRect(Landroid/graphics/Rect;)V

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreViewHitRect:Landroid/graphics/Rect;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mOverflowTouchExtend:I

    move/from16 v35, v0

    sub-int v34, v34, v35

    move/from16 v0, v34

    move-object/from16 v1, v33

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreViewHitRect:Landroid/graphics/Rect;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mOverflowTouchExtend:I

    move/from16 v35, v0

    add-int v34, v34, v35

    move/from16 v0, v34

    move-object/from16 v1, v33

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreViewHitRect:Landroid/graphics/Rect;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mOverflowTouchExtend:I

    move/from16 v35, v0

    sub-int v34, v34, v35

    move/from16 v0, v34

    move-object/from16 v1, v33

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreViewHitRect:Landroid/graphics/Rect;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mOverflowTouchExtend:I

    move/from16 v35, v0

    add-int v34, v34, v35

    move/from16 v0, v34

    move-object/from16 v1, v33

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 208
    new-instance v33, Landroid/view/TouchDelegate;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreViewHitRect:Landroid/graphics/Rect;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreView:Landroid/widget/TextView;

    move-object/from16 v35, v0

    invoke-direct/range {v33 .. v35}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 211
    .end local v24    # "readMoreBottom":I
    .end local v25    # "readMoreHeight":I
    .end local v26    # "readMoreRight":I
    .end local v27    # "readMoreWidth":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mRadioIcon:Landroid/widget/ImageView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v20

    .line 212
    .local v20, "radioHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mRadioIcon:Landroid/widget/ImageView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v23

    .line 213
    .local v23, "radioWidth":I
    sub-int v33, v11, v20

    div-int/lit8 v33, v33, 0x2

    add-int v22, v19, v33

    .line 214
    .local v22, "radioTop":I
    sub-int v33, v15, v23

    div-int/lit8 v33, v33, 0x2

    add-int v21, v17, v33

    .line 215
    .local v21, "radioLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mRadioIcon:Landroid/widget/ImageView;

    move-object/from16 v33, v0

    add-int v34, v21, v23

    add-int v35, v22, v20

    move-object/from16 v0, v33

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 220
    .local v5, "accessibilityLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v33, v0

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v34, v0

    add-int v34, v34, v17

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v35, v0

    add-int v35, v35, v19

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v36, v0

    add-int v36, v36, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getMeasuredWidth()I

    move-result v37

    add-int v36, v36, v37

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v37, v0

    add-int v37, v37, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/view/View;->getMeasuredHeight()I

    move-result v38

    add-int v37, v37, v38

    invoke-virtual/range {v33 .. v37}, Landroid/view/View;->layout(IIII)V

    .line 227
    return-void
.end method

.method protected onMeasure(II)V
    .locals 29
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 66
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->getPaddingLeft()I

    move-result v16

    .line 67
    .local v16, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->getPaddingRight()I

    move-result v17

    .line 68
    .local v17, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->getPaddingTop()I

    move-result v18

    .line 69
    .local v18, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->getPaddingBottom()I

    move-result v15

    .line 72
    .local v15, "paddingBottom":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v20

    .line 75
    .local v20, "resultWidth":I
    add-int v19, v15, v18

    .line 77
    .local v19, "resultHeight":I
    sub-int v25, v20, v16

    sub-int v13, v25, v17

    .line 79
    .local v13, "innerWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mRadioIcon:Landroid/widget/ImageView;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Landroid/widget/ImageView;->measure(II)V

    .line 81
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mImageView:Lcom/google/android/music/art/DocumentArtView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/music/art/DocumentArtView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 82
    .local v11, "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mTitle:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v22

    check-cast v22, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 83
    .local v22, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mDescription:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 85
    .local v8, "descriptionLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mImageView:Lcom/google/android/music/art/DocumentArtView;

    move-object/from16 v25, v0

    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    move/from16 v26, v0

    const/high16 v27, 0x40000000    # 2.0f

    invoke-static/range {v26 .. v27}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v27, v0

    const/high16 v28, 0x40000000    # 2.0f

    invoke-static/range {v27 .. v28}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    invoke-virtual/range {v25 .. v27}, Lcom/google/android/music/art/DocumentArtView;->measure(II)V

    .line 87
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mImageOverlay:Landroid/view/View;

    move-object/from16 v25, v0

    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    move/from16 v26, v0

    const/high16 v27, 0x40000000    # 2.0f

    invoke-static/range {v26 .. v27}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v27, v0

    const/high16 v28, 0x40000000    # 2.0f

    invoke-static/range {v27 .. v28}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    invoke-virtual/range {v25 .. v27}, Landroid/view/View;->measure(II)V

    .line 91
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mImageView:Lcom/google/android/music/art/DocumentArtView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/music/art/DocumentArtView;->getMeasuredWidth()I

    move-result v25

    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v26, v0

    add-int v25, v25, v26

    sub-int v13, v13, v25

    .line 93
    move-object/from16 v0, v22

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v25, v0

    sub-int v25, v13, v25

    move-object/from16 v0, v22

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v26, v0

    sub-int v23, v25, v26

    .line 94
    .local v23, "titleWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mTitle:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/high16 v26, -0x80000000

    move/from16 v0, v23

    move/from16 v1, v26

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Landroid/widget/TextView;->measure(II)V

    .line 97
    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v25, v0

    sub-int v25, v13, v25

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v26, v0

    sub-int v9, v25, v26

    .line 98
    .local v9, "descriptionWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mDescription:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/16 v26, 0x64

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 99
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mDescription:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/high16 v26, -0x80000000

    move/from16 v0, v26

    invoke-static {v9, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Landroid/widget/TextView;->measure(II)V

    .line 102
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mImageView:Lcom/google/android/music/art/DocumentArtView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/music/art/DocumentArtView;->getMeasuredHeight()I

    move-result v25

    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v26, v0

    add-int v25, v25, v26

    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v26, v0

    add-int v10, v25, v26

    .line 104
    .local v10, "imageHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mTitle:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v25

    move-object/from16 v0, v22

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v26, v0

    add-int v25, v25, v26

    move-object/from16 v0, v22

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v26, v0

    add-int v21, v25, v26

    .line 109
    .local v21, "titleAndMarginHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mDescription:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v25

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v26, v0

    add-int v25, v25, v26

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v26, v0

    add-int v6, v25, v26

    .line 111
    .local v6, "descriptionAndMarginHeight":I
    add-int v24, v21, v6

    .line 113
    .local v24, "totalTitlesHeight":I
    move/from16 v0, v24

    if-lt v10, v0, :cond_0

    .line 115
    add-int v19, v19, v10

    .line 116
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreView:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/16 v26, 0x8

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TextView;->setVisibility(I)V

    .line 141
    :goto_0
    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->setMeasuredDimension(II)V

    .line 144
    sub-int v25, v19, v18

    sub-int v12, v25, v15

    .line 147
    .local v12, "innerHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 149
    .local v4, "accessibilityLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v25, v0

    sub-int v25, v20, v25

    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v26, v0

    sub-int v5, v25, v26

    .line 151
    .local v5, "accessibilityWidth":I
    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v25, v0

    sub-int v25, v19, v25

    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v26, v0

    sub-int v3, v25, v26

    .line 153
    .local v3, "accessibilityHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v25, v0

    const/high16 v26, 0x40000000    # 2.0f

    move/from16 v0, v26

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    const/high16 v27, 0x40000000    # 2.0f

    move/from16 v0, v27

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    invoke-virtual/range {v25 .. v27}, Landroid/view/View;->measure(II)V

    .line 156
    return-void

    .line 118
    .end local v3    # "accessibilityHeight":I
    .end local v4    # "accessibilityLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v5    # "accessibilityWidth":I
    .end local v12    # "innerHeight":I
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mIsExpanded:Z

    move/from16 v25, v0

    if-eqz v25, :cond_1

    .line 120
    add-int v19, v19, v24

    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreView:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/16 v26, 0x8

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 124
    :cond_1
    add-int v19, v19, v10

    .line 128
    sub-int v25, v10, v21

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v26, v0

    sub-int v7, v25, v26

    .line 130
    .local v7, "descriptionHeightLimit":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mDescription:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getLineHeight()I

    move-result v25

    div-int v14, v7, v25

    .line 131
    .local v14, "lineCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mDescription:Landroid/widget/TextView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 132
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mDescription:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/high16 v26, -0x80000000

    move/from16 v0, v26

    invoke-static {v9, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    const/high16 v27, -0x80000000

    move/from16 v0, v27

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    invoke-virtual/range {v25 .. v27}, Landroid/widget/TextView;->measure(II)V

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreView:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TextView;->setVisibility(I)V

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreView:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Landroid/widget/TextView;->measure(II)V

    goto/16 :goto_0
.end method

.method public setAlpha(F)V
    .locals 1
    .param p1, "alpha"    # F

    .prologue
    .line 255
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-super {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setAlpha(F)V

    .line 256
    return-void
.end method

.method public setOnExpandListener(Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard$OnExpandListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard$OnExpandListener;

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->mReadMoreView:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard$1;-><init>(Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard$OnExpandListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    return-void
.end method
