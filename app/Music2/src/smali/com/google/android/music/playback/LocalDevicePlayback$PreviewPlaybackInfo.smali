.class Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PreviewPlaybackInfo"
.end annotation


# instance fields
.field public mPreviewDuration:J

.field public mPreviewPlayType:I

.field public mPreviewUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;IJ)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "duration"    # J

    .prologue
    .line 5421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5417
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;->mPreviewDuration:J

    .line 5418
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;->mPreviewPlayType:I

    .line 5422
    iput-wide p3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;->mPreviewDuration:J

    .line 5423
    iput p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;->mPreviewPlayType:I

    .line 5424
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;->mPreviewUrl:Ljava/lang/String;

    .line 5425
    return-void
.end method
