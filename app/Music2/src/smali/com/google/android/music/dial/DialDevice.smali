.class public abstract Lcom/google/android/music/dial/DialDevice;
.super Ljava/lang/Object;
.source "DialDevice.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/music/dial/WebSocket$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/DialDevice$Callback;,
        Lcom/google/android/music/dial/DialDevice$Listener;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mCallbackMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/music/dial/DialDevice$Callback;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mFailedPings:I

.field private final mHandler:Landroid/os/Handler;

.field private final mHandlerThread:Landroid/os/HandlerThread;

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/dial/DialDevice$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final mMessageBuffer:Ljava/lang/StringBuilder;

.field private mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

.field private final mRouteId:Ljava/lang/String;

.field private final mRouteInfo:Landroid/support/v7/media/MediaRouter$RouteInfo;

.field private mSocket:Lcom/google/android/music/dial/WebSocket;

.field private mSocketConnected:Z

.field private mSocketConnectionStatusAvailable:Z

.field private final mSocketLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_COMMUNICATION:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "routeInfo"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mMessageBuffer:Ljava/lang/StringBuilder;

    .line 100
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mListeners:Ljava/util/List;

    .line 116
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mSocketLock:Ljava/lang/Object;

    .line 125
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mCallbackMap:Ljava/util/Map;

    .line 128
    iput-object p1, p0, Lcom/google/android/music/dial/DialDevice;->mContext:Landroid/content/Context;

    .line 129
    iput-object p2, p0, Lcom/google/android/music/dial/DialDevice;->mRouteInfo:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 130
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mRouteInfo:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mRouteId:Ljava/lang/String;

    .line 132
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 133
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "com.google.android.music.dial.RemoteDevice"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/dial/RemoteDeviceInfo;

    iput-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    .line 135
    new-instance v1, Landroid/os/HandlerThread;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DialDevice-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    invoke-virtual {v3}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mHandlerThread:Landroid/os/HandlerThread;

    .line 136
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 137
    new-instance v1, Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/music/dial/DialDevice;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mHandler:Landroid/os/Handler;

    .line 138
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/dial/DialDevice;)Lcom/google/android/music/dial/WebSocket;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/DialDevice;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/music/dial/DialDevice;->getSocketInternal()Lcom/google/android/music/dial/WebSocket;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/dial/DialDevice;)Lcom/google/android/music/dial/RemoteDeviceInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/DialDevice;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/dial/DialDevice;Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/dial/DialDevice;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/music/dial/model/GenericDialEvent;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/dial/DialDevice;->callListenersOnMessageReceived(Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/music/dial/DialDevice;Lcom/google/android/music/dial/RemoteDeviceInfo;)Lcom/google/android/music/dial/RemoteDeviceInfo;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/dial/DialDevice;
    .param p1, "x1"    # Lcom/google/android/music/dial/RemoteDeviceInfo;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/music/dial/DialDevice;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/DialDevice;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mRouteId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/dial/DialDevice;Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Listener$Error;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/dial/DialDevice;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/music/dial/DialDevice$Listener$Error;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/dial/DialDevice;->callListenersOnError(Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Listener$Error;)V

    return-void
.end method

.method static synthetic access$400()Z
    .locals 1

    .prologue
    .line 40
    sget-boolean v0, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/music/dial/DialDevice;)Lcom/google/android/music/dial/WebSocket;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/DialDevice;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/music/dial/DialDevice;Lcom/google/android/music/dial/WebSocket;)Lcom/google/android/music/dial/WebSocket;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/dial/DialDevice;
    .param p1, "x1"    # Lcom/google/android/music/dial/WebSocket;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/music/dial/DialDevice;Lcom/google/android/music/dial/WebSocket;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/dial/DialDevice;
    .param p1, "x1"    # Lcom/google/android/music/dial/WebSocket;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/music/dial/DialDevice;->teardownSocket(Lcom/google/android/music/dial/WebSocket;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/dial/DialDevice;Lcom/google/android/music/dial/WebSocket;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/DialDevice;
    .param p1, "x1"    # Lcom/google/android/music/dial/WebSocket;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/dial/DialDevice;->sendMessage(Lcom/google/android/music/dial/WebSocket;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/music/dial/DialDevice;)Ljava/lang/StringBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/DialDevice;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mMessageBuffer:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/dial/DialDevice;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/DialDevice;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mCallbackMap:Ljava/util/Map;

    return-object v0
.end method

.method private callListenersOnError(Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Listener$Error;)V
    .locals 4
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "error"    # Lcom/google/android/music/dial/DialDevice$Listener$Error;

    .prologue
    .line 704
    iget-object v3, p0, Lcom/google/android/music/dial/DialDevice;->mListeners:Ljava/util/List;

    monitor-enter v3

    .line 705
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/dial/DialDevice;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/dial/DialDevice$Listener;

    .line 706
    .local v1, "listener":Lcom/google/android/music/dial/DialDevice$Listener;
    invoke-interface {v1, p1, p2}, Lcom/google/android/music/dial/DialDevice$Listener;->onError(Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Listener$Error;)V

    goto :goto_0

    .line 708
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/music/dial/DialDevice$Listener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 709
    return-void
.end method

.method private callListenersOnMessageReceived(Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)V
    .locals 4
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "message"    # Lcom/google/android/music/dial/model/GenericDialEvent;

    .prologue
    .line 689
    iget-object v3, p0, Lcom/google/android/music/dial/DialDevice;->mListeners:Ljava/util/List;

    monitor-enter v3

    .line 690
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/dial/DialDevice;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/dial/DialDevice$Listener;

    .line 691
    .local v1, "listener":Lcom/google/android/music/dial/DialDevice$Listener;
    invoke-interface {v1, p1, p2}, Lcom/google/android/music/dial/DialDevice$Listener;->onMessageReceived(Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)V

    goto :goto_0

    .line 693
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/music/dial/DialDevice$Listener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 694
    return-void
.end method

.method private createWebSocket(Landroid/net/Uri;ILcom/google/android/music/dial/WebSocket$Listener;)Lcom/google/android/music/dial/WebSocket;
    .locals 5
    .param p1, "appUrl"    # Landroid/net/Uri;
    .param p2, "connectionTimeout"    # I
    .param p3, "listener"    # Lcom/google/android/music/dial/WebSocket$Listener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 618
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/music/dial/Utils;->getHttpSchemeForApplicationUrl(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 622
    .local v0, "originUrl":Landroid/net/Uri;
    new-instance v1, Lcom/google/android/music/dial/WebSocket;

    iget-object v2, p0, Lcom/google/android/music/dial/DialDevice;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/music/dial/WebSocket;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    .local v1, "ws":Lcom/google/android/music/dial/WebSocket;
    invoke-virtual {v1, p3}, Lcom/google/android/music/dial/WebSocket;->setListener(Lcom/google/android/music/dial/WebSocket$Listener;)V

    .line 624
    invoke-virtual {v1, p1, p2}, Lcom/google/android/music/dial/WebSocket;->connect(Landroid/net/Uri;I)V

    .line 625
    return-object v1
.end method

.method private getSocketInternal()Lcom/google/android/music/dial/WebSocket;
    .locals 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;

    invoke-virtual {v0}, Lcom/google/android/music/dial/WebSocket;->isConnected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/DialDevice;->teardownSocket(Lcom/google/android/music/dial/WebSocket;)V

    .line 320
    invoke-direct {p0}, Lcom/google/android/music/dial/DialDevice;->setupSocket()Lcom/google/android/music/dial/WebSocket;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;

    .line 322
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/dial/Utils;->getConnectionHealthChecksEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 323
    sget-boolean v0, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v0, :cond_1

    .line 324
    const-string v0, "DialDevice"

    const-string v1, "Starting health checks on connection"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/dial/DialDevice;->stopReceiverPeriodicHealthCheck()V

    .line 329
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;

    if-eqz v0, :cond_2

    .line 330
    invoke-direct {p0}, Lcom/google/android/music/dial/DialDevice;->startReceiverPeriodicHealthCheck()V

    .line 338
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;

    return-object v0

    .line 333
    :cond_3
    sget-boolean v0, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v0, :cond_2

    .line 334
    const-string v0, "DialDevice"

    const-string v1, "Connection health checks are disabled"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handlePeriodicHealthCheck()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 749
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;

    if-eqz v1, :cond_8

    .line 750
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;

    invoke-virtual {v1}, Lcom/google/android/music/dial/WebSocket;->isReceiverAlive()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 751
    sget-boolean v1, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v1, :cond_0

    .line 752
    const-string v1, "DialDevice"

    const-string v2, "Receiver is alive."

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 755
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mContext:Landroid/content/Context;

    const-string v2, "Pong"

    invoke-static {v1, v2}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 757
    :cond_1
    iput v6, p0, Lcom/google/android/music/dial/DialDevice;->mFailedPings:I

    .line 758
    invoke-direct {p0}, Lcom/google/android/music/dial/DialDevice;->runHealthCheck()V

    .line 785
    :cond_2
    :goto_0
    return-void

    .line 760
    :cond_3
    iget v1, p0, Lcom/google/android/music/dial/DialDevice;->mFailedPings:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/music/dial/DialDevice;->mFailedPings:I

    .line 761
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/dial/Utils;->getConnectionMaxHealthCheckFailures(Landroid/content/Context;)I

    move-result v0

    .line 762
    .local v0, "maxFailuresAllowed":I
    sget-boolean v1, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v1, :cond_4

    .line 763
    const-string v1, "DialDevice"

    const-string v2, "Receiver pings failed %d/%d time(s)."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/music/dial/DialDevice;->mFailedPings:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    :cond_4
    iget v1, p0, Lcom/google/android/music/dial/DialDevice;->mFailedPings:I

    if-lt v1, v0, :cond_6

    .line 767
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 768
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mContext:Landroid/content/Context;

    const-string v2, "All pings failed"

    invoke-static {v1, v2}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 770
    :cond_5
    const-string v1, "DialDevice"

    const-string v2, "All receiver health checks failed.  Tearing down connection"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    invoke-virtual {p0}, Lcom/google/android/music/dial/DialDevice;->teardown()V

    .line 772
    iput v6, p0, Lcom/google/android/music/dial/DialDevice;->mFailedPings:I

    goto :goto_0

    .line 774
    :cond_6
    sget-boolean v1, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v1, :cond_7

    .line 775
    const-string v1, "DialDevice"

    const-string v2, "Ping failed.  Retrying receiver health check."

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    :cond_7
    invoke-direct {p0}, Lcom/google/android/music/dial/DialDevice;->runHealthCheck()V

    goto :goto_0

    .line 781
    .end local v0    # "maxFailuresAllowed":I
    :cond_8
    sget-boolean v1, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v1, :cond_2

    .line 782
    const-string v1, "DialDevice"

    const-string v2, "mSocket is null.  handlePeriodicHealthCheck no op."

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private runHealthCheck()V
    .locals 7

    .prologue
    .line 718
    iget-object v2, p0, Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;

    if-eqz v2, :cond_2

    .line 720
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;

    invoke-virtual {v2}, Lcom/google/android/music/dial/WebSocket;->writePing()V

    .line 721
    iget-object v2, p0, Lcom/google/android/music/dial/DialDevice;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 722
    iget-object v2, p0, Lcom/google/android/music/dial/DialDevice;->mContext:Landroid/content/Context;

    const-string v3, "Ping"

    invoke-static {v2, v3}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 724
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/dial/DialDevice;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/dial/Utils;->getConnectionHealthCheckIntervalMillis(Landroid/content/Context;)I

    move-result v1

    .line 725
    .local v1, "nextCheckMillis":I
    iget-object v2, p0, Lcom/google/android/music/dial/DialDevice;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    int-to-long v4, v1

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 726
    sget-boolean v2, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v2, :cond_1

    .line 727
    const-string v2, "DialDevice"

    const-string v3, "Wrote Ping.  Scheduled check for Pong in %d ms."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 740
    .end local v1    # "nextCheckMillis":I
    :cond_1
    :goto_0
    return-void

    .line 732
    :catch_0
    move-exception v0

    .line 733
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "DialDevice"

    const-string v3, "Error writing Ping to receiver."

    invoke-static {v2, v3, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 736
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    sget-boolean v2, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v2, :cond_1

    .line 737
    const-string v2, "DialDevice"

    const-string v3, "mSocket is null.  runHealthCheck no op."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private sendMessage(Lcom/google/android/music/dial/WebSocket;Ljava/lang/String;)Z
    .locals 4
    .param p1, "socket"    # Lcom/google/android/music/dial/WebSocket;
    .param p2, "payload"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 655
    :try_start_0
    invoke-virtual {p1, p2}, Lcom/google/android/music/dial/WebSocket;->sendMessage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 656
    const/4 v1, 0x1

    .line 671
    :cond_0
    :goto_0
    return v1

    .line 657
    :catch_0
    move-exception v0

    .line 663
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-boolean v2, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v2, :cond_0

    .line 664
    const-string v2, "DialDevice"

    const-string v3, "Channel has become disconnected"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 667
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 668
    .local v0, "e":Ljava/io/IOException;
    sget-boolean v2, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v2, :cond_0

    .line 669
    const-string v2, "DialDevice"

    const-string v3, "Error sending message on web socket"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setupSocket()Lcom/google/android/music/dial/WebSocket;
    .locals 12

    .prologue
    .line 511
    sget-boolean v6, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v6, :cond_0

    .line 512
    const-string v6, "DialDevice"

    const-string v7, "setupSocket %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    :cond_0
    iget-object v6, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    invoke-virtual {v6}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getApplicationUrl()Landroid/net/Uri;

    move-result-object v0

    .line 525
    .local v0, "appUrl":Landroid/net/Uri;
    iget-object v6, p0, Lcom/google/android/music/dial/DialDevice;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/music/dial/Utils;->getWebSocketConnectTimeoutMillis(Landroid/content/Context;)I

    move-result v2

    .line 527
    .local v2, "connectTimeoutMillis":I
    iget-object v6, p0, Lcom/google/android/music/dial/DialDevice;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/music/dial/Utils;->getWebSocketConnectRetryTimeoutMillis(Landroid/content/Context;)I

    move-result v1

    .line 529
    .local v1, "connectRetryTimeoutMillis":I
    const/4 v5, 0x0

    .line 531
    .local v5, "socket":Lcom/google/android/music/dial/WebSocket;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    const/4 v6, 0x3

    if-ge v4, v6, :cond_9

    .line 532
    sget-boolean v6, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v6, :cond_1

    .line 533
    const-string v6, "DialDevice"

    const-string v7, "Socket device: %s url: %s attempt: %d"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    :cond_1
    iget-object v7, p0, Lcom/google/android/music/dial/DialDevice;->mSocketLock:Ljava/lang/Object;

    monitor-enter v7

    .line 539
    const/4 v6, 0x0

    :try_start_0
    iput-boolean v6, p0, Lcom/google/android/music/dial/DialDevice;->mSocketConnectionStatusAvailable:Z

    .line 540
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/music/dial/DialDevice;->mSocketConnected:Z

    .line 541
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 543
    :try_start_1
    invoke-direct {p0, v0, v2, p0}, Lcom/google/android/music/dial/DialDevice;->createWebSocket(Landroid/net/Uri;ILcom/google/android/music/dial/WebSocket$Listener;)Lcom/google/android/music/dial/WebSocket;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    .line 551
    invoke-virtual {v5}, Lcom/google/android/music/dial/WebSocket;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 552
    sget-boolean v6, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v6, :cond_2

    .line 553
    const-string v6, "DialDevice"

    const-string v7, "Socket device: %s url: %s attempt: %d - already connected"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v6, v5

    .line 604
    :goto_1
    return-object v6

    .line 541
    :catchall_0
    move-exception v6

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .line 544
    :catch_0
    move-exception v3

    .line 545
    .local v3, "e":Ljava/io/IOException;
    const-string v6, "DialDevice"

    const-string v7, "Socket device: %s url: %s attempt: %d - connection error"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 531
    .end local v3    # "e":Ljava/io/IOException;
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 560
    :cond_3
    sget-boolean v6, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v6, :cond_4

    .line 561
    const-string v6, "DialDevice"

    const-string v7, "Socket device: %s url: %s attempt: %d - waiting for connection"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    :cond_4
    iget-object v7, p0, Lcom/google/android/music/dial/DialDevice;->mSocketLock:Ljava/lang/Object;

    monitor-enter v7

    .line 568
    :goto_3
    :try_start_3
    iget-boolean v6, p0, Lcom/google/android/music/dial/DialDevice;->mSocketConnectionStatusAvailable:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-nez v6, :cond_5

    .line 572
    :try_start_4
    iget-object v6, p0, Lcom/google/android/music/dial/DialDevice;->mSocketLock:Ljava/lang/Object;

    mul-int/lit8 v8, v2, 0x2

    int-to-long v8, v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_3

    .line 573
    :catch_1
    move-exception v6

    goto :goto_3

    .line 577
    :cond_5
    :try_start_5
    iget-boolean v6, p0, Lcom/google/android/music/dial/DialDevice;->mSocketConnected:Z

    if-eqz v6, :cond_7

    .line 578
    sget-boolean v6, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v6, :cond_6

    .line 579
    const-string v6, "DialDevice"

    const-string v8, "Socket device: %s url: %s attempt: %d - connected"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    :cond_6
    monitor-exit v7

    move-object v6, v5

    goto/16 :goto_1

    .line 586
    :cond_7
    sget-boolean v6, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v6, :cond_8

    .line 587
    const-string v6, "DialDevice"

    const-string v8, "Socket device: %s url: %s attempt: %d - connect failed.  Retrying."

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    :cond_8
    invoke-direct {p0, v5}, Lcom/google/android/music/dial/DialDevice;->teardownSocket(Lcom/google/android/music/dial/WebSocket;)V

    .line 594
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 597
    int-to-long v6, v1

    :try_start_6
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_2

    .line 598
    :catch_2
    move-exception v6

    goto/16 :goto_2

    .line 594
    :catchall_1
    move-exception v6

    :try_start_7
    monitor-exit v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v6

    .line 602
    :cond_9
    const-string v6, "DialDevice"

    const-string v7, "Socket device: %s url: %s - all attempts to connect failed"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    const/4 v6, 0x0

    goto/16 :goto_1
.end method

.method private startReceiverPeriodicHealthCheck()V
    .locals 1

    .prologue
    .line 342
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/dial/DialDevice;->mFailedPings:I

    .line 343
    invoke-direct {p0}, Lcom/google/android/music/dial/DialDevice;->runHealthCheck()V

    .line 344
    return-void
.end method

.method private stopReceiverPeriodicHealthCheck()V
    .locals 2

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 348
    return-void
.end method

.method private teardownSocket(Lcom/google/android/music/dial/WebSocket;)V
    .locals 6
    .param p1, "socket"    # Lcom/google/android/music/dial/WebSocket;

    .prologue
    .line 632
    sget-boolean v1, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v1, :cond_0

    .line 633
    const-string v1, "DialDevice"

    const-string v2, "teardownSocket %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    :cond_0
    if-eqz p1, :cond_2

    .line 639
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;

    if-ne p1, v1, :cond_1

    .line 640
    invoke-direct {p0}, Lcom/google/android/music/dial/DialDevice;->stopReceiverPeriodicHealthCheck()V

    .line 641
    sget-boolean v1, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v1, :cond_1

    .line 642
    const-string v1, "DialDevice"

    const-string v2, "Stopped health check on socket that is being disconnected"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/music/dial/WebSocket;->disconnect()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 651
    :cond_2
    :goto_0
    return-void

    .line 647
    :catch_0
    move-exception v0

    .line 648
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "DialDevice"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error closing web socket for route "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/dial/DialDevice;->mRouteId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/google/android/music/dial/DialDevice$Listener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/music/dial/DialDevice$Listener;

    .prologue
    .line 241
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mListeners:Ljava/util/List;

    monitor-enter v1

    .line 242
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    :cond_0
    monitor-exit v1

    .line 246
    return-void

    .line 245
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 145
    sget-boolean v0, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v0, :cond_0

    .line 146
    const-string v0, "DialDevice"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 154
    const-string v0, "DialDevice"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 150
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/music/dial/DialDevice;->handlePeriodicHealthCheck()V

    .line 151
    const/4 v0, 0x1

    goto :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public abstract handleMessage(Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)Z
.end method

.method public onConnected()V
    .locals 2

    .prologue
    .line 357
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mSocketLock:Ljava/lang/Object;

    monitor-enter v1

    .line 358
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/music/dial/DialDevice;->mSocketConnectionStatusAvailable:Z

    .line 359
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/dial/DialDevice;->mSocketConnected:Z

    .line 360
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mSocketLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 361
    monitor-exit v1

    .line 362
    return-void

    .line 361
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onConnectionFailed(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 368
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice;->mSocketLock:Ljava/lang/Object;

    monitor-enter v1

    .line 369
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/music/dial/DialDevice;->mSocketConnectionStatusAvailable:Z

    .line 370
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/dial/DialDevice;->mSocketConnected:Z

    .line 371
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mSocketLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 372
    monitor-exit v1

    .line 373
    return-void

    .line 372
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onContinuationMessageReceived(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "isFinal"    # Z

    .prologue
    .line 447
    sget-boolean v0, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v0, :cond_0

    .line 448
    const-string v0, "DialDevice"

    const-string v1, "socket onContinuationMessageReceived string %s isFinal: %b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    :cond_0
    return-void
.end method

.method public onContinuationMessageReceived([BZ)V
    .locals 5
    .param p1, "message"    # [B
    .param p2, "isFinal"    # Z

    .prologue
    .line 457
    sget-boolean v0, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v0, :cond_0

    .line 458
    const-string v0, "DialDevice"

    const-string v1, "socket onContinuationMessageReceived bytes %s isFinal: %b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    :cond_0
    const-string v0, "DialDevice"

    const-string v1, "Bytes not accepted on responses."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    return-void
.end method

.method public onDisconnected(II)V
    .locals 7
    .param p1, "error"    # I
    .param p2, "closeCode"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 468
    sget-boolean v0, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v0, :cond_0

    .line 469
    const-string v0, "DialDevice"

    const-string v1, "socket onDisconnected %s, error: %d, closeCode: %d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v3, v2, v5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    :cond_0
    const/16 v0, 0x3e8

    if-eq p2, v0, :cond_1

    if-nez p1, :cond_3

    .line 475
    :cond_1
    sget-boolean v0, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v0, :cond_2

    .line 476
    const-string v0, "DialDevice"

    const-string v1, "socket onDisconnected %s.  Normal disconnect.  Will not re-establish connection"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :cond_2
    :goto_0
    return-void

    .line 482
    :cond_3
    sget-boolean v0, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v0, :cond_4

    .line 483
    const-string v0, "DialDevice"

    const-string v1, "socket onDisconnected %s.  Abnormal disconnect. Re-establishing connection"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/music/dial/DialDevice$6;

    invoke-direct {v1, p0}, Lcom/google/android/music/dial/DialDevice$6;-><init>(Lcom/google/android/music/dial/DialDevice;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onMessageReceived(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "isFinal"    # Z

    .prologue
    .line 390
    sget-boolean v0, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v0, :cond_0

    .line 391
    const-string v0, "DialDevice"

    const-string v1, "socket onMessageReceived string %s isFinal: %b message: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/music/dial/DialDevice$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/music/dial/DialDevice$5;-><init>(Lcom/google/android/music/dial/DialDevice;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 433
    return-void
.end method

.method public onMessageReceived([BZ)V
    .locals 5
    .param p1, "message"    # [B
    .param p2, "isFinal"    # Z

    .prologue
    .line 437
    sget-boolean v0, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v0, :cond_0

    .line 438
    const-string v0, "DialDevice"

    const-string v1, "socket onMessageReceived bytes %s isFinal: %b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :cond_0
    const-string v0, "DialDevice"

    const-string v1, "Bytes not accepted on responses."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    return-void
.end method

.method public sendCommand(Lcom/google/android/music/dial/model/GenericDialCommand;Lcom/google/android/music/dial/DialDevice$Callback;)V
    .locals 2
    .param p1, "command"    # Lcom/google/android/music/dial/model/GenericDialCommand;
    .param p2, "callback"    # Lcom/google/android/music/dial/DialDevice$Callback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/dial/MalformedDialCommandException;
        }
    .end annotation

    .prologue
    .line 266
    invoke-virtual {p1}, Lcom/google/android/music/dial/model/GenericDialCommand;->validate()V

    .line 268
    iget-object v0, p1, Lcom/google/android/music/dial/model/GenericDialCommand;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    iget-object v0, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mCmdId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 269
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mCallbackMap:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/music/dial/model/GenericDialCommand;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    iget-object v1, v1, Lcom/google/android/music/dial/model/CommandHeaderJson;->mCmdId:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/dial/model/GenericDialCommand;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/music/dial/DialDevice;->sendMessage(Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Callback;)V

    .line 272
    return-void
.end method

.method public sendMessage(Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Callback;)V
    .locals 4
    .param p1, "payload"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/google/android/music/dial/DialDevice$Callback;

    .prologue
    .line 276
    sget-boolean v0, Lcom/google/android/music/dial/DialDevice;->LOGV:Z

    if-eqz v0, :cond_0

    .line 277
    const-string v0, "DialDevice"

    const-string v1, "sendMessage:\n%s\n"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    const-string v0, "DialDevice"

    const-string v1, "Invalid payload."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    :goto_0
    return-void

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/music/dial/DialDevice$4;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/music/dial/DialDevice$4;-><init>(Lcom/google/android/music/dial/DialDevice;Lcom/google/android/music/dial/DialDevice$Callback;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public setup()V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/music/dial/DialDevice$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/dial/DialDevice$1;-><init>(Lcom/google/android/music/dial/DialDevice;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 174
    return-void
.end method

.method public teardown()V
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/music/dial/DialDevice$3;

    invoke-direct {v1, p0}, Lcom/google/android/music/dial/DialDevice$3;-><init>(Lcom/google/android/music/dial/DialDevice;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 237
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-static {v0}, Lcom/google/android/music/dial/Utils;->quitHandlerThread(Landroid/os/HandlerThread;)V

    .line 238
    return-void
.end method

.method public update(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "groupName"    # Ljava/lang/String;
    .param p2, "appUrl"    # Ljava/lang/String;

    .prologue
    .line 186
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    const-string v0, "DialDevice"

    const-string v1, "groupName cannot be empty or null"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :goto_0
    return-void

    .line 190
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 191
    const-string v0, "DialDevice"

    const-string v1, "appUrl cannot be empty or null"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/music/dial/DialDevice$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/music/dial/DialDevice$2;-><init>(Lcom/google/android/music/dial/DialDevice;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
