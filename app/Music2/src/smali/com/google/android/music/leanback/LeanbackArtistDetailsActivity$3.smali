.class Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackArtistDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createLockerAlbumsRow(JLjava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Landroid/support/v17/leanback/widget/Presenter;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/support/v17/leanback/widget/Presenter;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;->val$context:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    return-void
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 224
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 225
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v0, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->populateDocumentFromAlbumCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    .line 226
    new-instance v1, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v1}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3$1;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;Lcom/google/android/music/ui/cardlib/model/Document;)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Lcom/google/android/music/leanback/Item$StringGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/leanback/LeanbackUtils;->getAlbumDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/leanback/Item$Builder;->id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v1

    return-object v1
.end method
