.class public Lcom/google/android/music/art/ArtRequest2;
.super Ljava/lang/Object;
.source "ArtRequest2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/ArtRequest2$StateTransitionTracker;
    }
.end annotation


# static fields
.field public static final FIELD_NAMES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final LOGV:Z


# instance fields
.field private mAliasedBitmap:Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

.field private final mAvailableItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/art/ArtResolver2$ArtUrl;",
            ">;"
        }
    .end annotation
.end field

.field private final mDescriptor:Lcom/google/android/music/art/ArtDescriptor;

.field private final mIsSynchronous:Z

.field private final mItemsLock:Ljava/lang/Object;

.field private mListener:Lcom/google/android/music/art/ArtResolver2$RequestListener;

.field private final mNeededItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/art/ArtResolver2$ArtUrl;",
            ">;"
        }
    .end annotation
.end field

.field private final mOriginatingHandler:Landroid/os/Handler;

.field private mPalette:Landroid/support/v7/graphics/Palette;

.field private final mPostProcessor:Lcom/google/android/music/art/ArtPostProcessor;

.field private mReferences:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mRenderedBitmap:Landroid/graphics/Bitmap;

.field private volatile mState:I

.field private final mStateChangeLock:Ljava/lang/Object;

.field private mTransitionTracker:Lcom/google/android/music/art/ArtRequest2$StateTransitionTracker;

.field private mUsedMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    sget-boolean v1, Lcom/google/android/music/art/ArtResolver2;->LOGV:Z

    sput-boolean v1, Lcom/google/android/music/art/ArtRequest2;->LOGV:Z

    .line 59
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    .line 60
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;"
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "NONE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "LOADING (Request Created)"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "LOADING (TypeHandler start)"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "LOADING, (Resolve start)"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "LOADING (Finished)"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "RENDERING (Load Task Created)"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    const/16 v1, 0x15

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "RENDERING (Load Task Start)"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "RENDERING (Rendering start)"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "FINISHED"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    const/16 v1, 0x28

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ERROR_GENERIC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    const/16 v1, 0x29

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ERROR_LOADING"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    const/16 v1, 0x2a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ERROR_RENDERING"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    const/16 v1, 0x3c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "CANCELLED"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    const/16 v1, 0x46

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "RELEASED"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-static {v0}, Lcom/google/common/collect/ImmutableMap;->copyOf(Ljava/util/Map;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v1

    sput-object v1, Lcom/google/android/music/art/ArtRequest2;->FIELD_NAMES:Ljava/util/Map;

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/art/ArtPostProcessor;Lcom/google/android/music/art/ArtDescriptor;Z)V
    .locals 2
    .param p1, "postProcessor"    # Lcom/google/android/music/art/ArtPostProcessor;
    .param p2, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;
    .param p3, "isSynchronous"    # Z

    .prologue
    const/4 v1, 0x0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mStateChangeLock:Ljava/lang/Object;

    .line 81
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    .line 92
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mItemsLock:Ljava/lang/Object;

    .line 93
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mNeededItems:Ljava/util/List;

    .line 95
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mAvailableItems:Ljava/util/List;

    .line 101
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mReferences:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mTransitionTracker:Lcom/google/android/music/art/ArtRequest2$StateTransitionTracker;

    .line 128
    iput-object p1, p0, Lcom/google/android/music/art/ArtRequest2;->mPostProcessor:Lcom/google/android/music/art/ArtPostProcessor;

    .line 129
    iput-object p2, p0, Lcom/google/android/music/art/ArtRequest2;->mDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    .line 130
    iput-boolean p3, p0, Lcom/google/android/music/art/ArtRequest2;->mIsSynchronous:Z

    .line 131
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mOriginatingHandler:Landroid/os/Handler;

    .line 132
    invoke-direct {p0, v1}, Lcom/google/android/music/art/ArtRequest2;->changeState(I)V

    .line 133
    return-void
.end method

.method private changeState(I)V
    .locals 2
    .param p1, "newState"    # I

    .prologue
    .line 159
    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest2;->mStateChangeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 160
    :try_start_0
    iget v0, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    if-ne p1, v0, :cond_0

    monitor-exit v1

    .line 173
    :goto_0
    return-void

    .line 161
    :cond_0
    iget v0, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    if-ge p1, v0, :cond_1

    .line 162
    monitor-exit v1

    goto :goto_0

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 171
    :cond_1
    :try_start_1
    iput p1, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    .line 172
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private getStateName(I)Ljava/lang/String;
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 526
    sget-object v1, Lcom/google/android/music/art/ArtRequest2;->FIELD_NAMES:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 527
    .local v0, "fieldName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 528
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 530
    :cond_0
    return-object v0
.end method

.method private shouldStopLoadingLocked()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 391
    iget v2, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    .line 392
    .local v2, "state":I
    const/16 v5, 0x14

    if-ne v2, v5, :cond_2

    move v0, v4

    .line 393
    .local v0, "alreadyRendering":Z
    :goto_0
    const/16 v5, 0x3c

    if-ne v2, v5, :cond_3

    move v1, v4

    .line 394
    .local v1, "cancelled":Z
    :goto_1
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    iget-object v5, p0, Lcom/google/android/music/art/ArtRequest2;->mNeededItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/music/art/ArtRequest2;->mAvailableItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/music/art/ArtRequest2;->mPostProcessor:Lcom/google/android/music/art/ArtPostProcessor;

    iget-object v7, p0, Lcom/google/android/music/art/ArtRequest2;->mDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    invoke-interface {v6, v7}, Lcom/google/android/music/art/ArtPostProcessor;->getMaxNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I

    move-result v6

    if-lt v5, v6, :cond_1

    :cond_0
    move v3, v4

    :cond_1
    return v3

    .end local v0    # "alreadyRendering":Z
    .end local v1    # "cancelled":Z
    :cond_2
    move v0, v3

    .line 392
    goto :goto_0

    .restart local v0    # "alreadyRendering":Z
    :cond_3
    move v1, v3

    .line 393
    goto :goto_1
.end method

.method private translateStringItemToArtUrl(Ljava/lang/String;)Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    .locals 6
    .param p1, "neededItem"    # Ljava/lang/String;

    .prologue
    .line 241
    instance-of v4, p1, Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 242
    iget-object v5, p0, Lcom/google/android/music/art/ArtRequest2;->mItemsLock:Ljava/lang/Object;

    monitor-enter v5

    .line 243
    :try_start_0
    iget-object v4, p0, Lcom/google/android/music/art/ArtRequest2;->mNeededItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 244
    .local v3, "o":Ljava/lang/Object;
    instance-of v4, v3, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    if-eqz v4, :cond_0

    .line 245
    move-object v0, v3

    check-cast v0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    move-object v1, v0

    .line 246
    .local v1, "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    invoke-virtual {v1}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 247
    monitor-exit v5

    .line 253
    .end local v1    # "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "o":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 251
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    monitor-exit v5

    .line 253
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 251
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method


# virtual methods
.method public addNeededItem(Lcom/google/android/music/art/ArtResolver2$ArtUrl;)V
    .locals 2
    .param p1, "neededItem"    # Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    .prologue
    .line 209
    iget v0, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Type handler should have started before adding items"

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 211
    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest2;->mItemsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 212
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/art/ArtRequest2;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit v1

    .line 215
    :goto_1
    return-void

    .line 209
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mNeededItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addNeededItems(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/music/art/ArtResolver2$ArtUrl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 218
    .local p1, "neededItems":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/art/ArtResolver2$ArtUrl;>;"
    iget v0, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Type handler should have started before adding items"

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 220
    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest2;->mItemsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 221
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/art/ArtRequest2;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit v1

    .line 224
    :goto_1
    return-void

    .line 218
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mNeededItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 223
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cancelRequest()V
    .locals 1

    .prologue
    .line 194
    const/16 v0, 0x3c

    invoke-direct {p0, v0}, Lcom/google/android/music/art/ArtRequest2;->changeState(I)V

    .line 195
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/art/ArtRequest2;->setListener(Lcom/google/android/music/art/ArtResolver2$RequestListener;)V

    .line 196
    return-void
.end method

.method public didRenderSuccessfully()Z
    .locals 2

    .prologue
    .line 203
    iget v0, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    .line 204
    .local v0, "state":I
    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    const/16 v1, 0x28

    if-gt v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method externalChangeState(I)V
    .locals 2
    .param p1, "newState"    # I

    .prologue
    .line 142
    sparse-switch p1, :sswitch_data_0

    .line 153
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "externalChangeState can only be used to change to certain substates of RENDERING, LOADING, or ERROR"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150
    :sswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtRequest2;->changeState(I)V

    .line 151
    return-void

    .line 142
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0xb -> :sswitch_0
        0xc -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x28 -> :sswitch_0
        0x29 -> :sswitch_0
    .end sparse-switch
.end method

.method public finishedLoadingSuccessfully()Z
    .locals 2

    .prologue
    .line 180
    iget v0, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    .line 181
    .local v0, "state":I
    const/16 v1, 0xd

    if-lt v0, v1, :cond_0

    const/16 v1, 0x28

    if-ge v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method generateOriginInfo(Lcom/google/android/music/art/ArtResolver2$ArtUrl;I)Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;
    .locals 3
    .param p1, "url"    # Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    .param p2, "source"    # I

    .prologue
    .line 547
    new-instance v0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;

    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest2;->mDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    iget-object v1, v1, Lcom/google/android/music/art/ArtDescriptor;->artType:Lcom/google/android/music/art/ArtType;

    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest2;->mDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    iget v2, v2, Lcom/google/android/music/art/ArtDescriptor;->sizeBucket:I

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;-><init>(Lcom/google/android/music/art/ArtResolver2$ArtUrl;ILcom/google/android/music/art/ArtType;I)V

    return-object v0
.end method

.method public getAvailableItems()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/art/ArtResolver2$ArtUrl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 352
    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest2;->mItemsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 353
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mAvailableItems:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 354
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getDescriptor()Lcom/google/android/music/art/ArtDescriptor;
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    return-object v0
.end method

.method public getListener()Lcom/google/android/music/art/ArtResolver2$RequestListener;
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mListener:Lcom/google/android/music/art/ArtResolver2$RequestListener;

    return-object v0
.end method

.method public getMinimumNeededItemCount()I
    .locals 2

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mPostProcessor:Lcom/google/android/music/art/ArtPostProcessor;

    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest2;->mDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    invoke-interface {v0, v1}, Lcom/google/android/music/art/ArtPostProcessor;->getMinNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I

    move-result v0

    return v0
.end method

.method public getNeededItems()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/art/ArtResolver2$ArtUrl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 343
    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest2;->mItemsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 344
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mNeededItems:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 345
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method getOriginatingHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mOriginatingHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getPalette()Landroid/support/v7/graphics/Palette;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mPalette:Landroid/support/v7/graphics/Palette;

    return-object v0
.end method

.method public getResultBitmap()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 455
    invoke-virtual {p0}, Lcom/google/android/music/art/ArtRequest2;->didRenderSuccessfully()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    iget v0, p0, Lcom/google/android/music/art/ArtRequest2;->mUsedMode:I

    packed-switch v0, :pswitch_data_0

    .line 462
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown rendering mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/art/ArtRequest2;->mUsedMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 458
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mRenderedBitmap:Landroid/graphics/Bitmap;

    .line 467
    :goto_0
    return-object v0

    .line 460
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest2;->mAliasedBitmap:Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    iget-object v0, v0, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 465
    :cond_0
    const-string v0, "ArtResolver"

    const-string v1, "ArtRequest2.getResultBitmap(): Asked for art before it was rendered."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    const/4 v0, 0x0

    goto :goto_0

    .line 456
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isCancelled()Z
    .locals 2

    .prologue
    .line 190
    iget v0, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    const/16 v1, 0x3c

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInFinalState()Z
    .locals 2

    .prologue
    .line 199
    iget v0, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSynchronous()Z
    .locals 1

    .prologue
    .line 443
    iget-boolean v0, p0, Lcom/google/android/music/art/ArtRequest2;->mIsSynchronous:Z

    return v0
.end method

.method public notifyItemAvailable(Lcom/google/android/music/art/ArtResolver2$ArtUrl;)V
    .locals 5
    .param p1, "neededItem"    # Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    .prologue
    .line 283
    iget v0, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    .line 284
    .local v0, "state":I
    const/16 v2, 0x14

    if-gt v0, v2, :cond_0

    const/16 v2, 0xc

    if-ge v0, v2, :cond_2

    .line 286
    :cond_0
    sget-boolean v2, Lcom/google/android/music/art/ArtRequest2;->LOGV:Z

    if-eqz v2, :cond_1

    .line 287
    const-string v2, "ArtResolver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notifyItemAvailable called out of expected state. Current state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Item change rejected"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    :cond_1
    :goto_0
    return-void

    .line 295
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest2;->mItemsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 296
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest2;->mNeededItems:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 297
    .local v1, "wasRemoved":Z
    if-eqz v1, :cond_3

    .line 298
    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest2;->mAvailableItems:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300
    :cond_3
    invoke-direct {p0}, Lcom/google/android/music/art/ArtRequest2;->shouldStopLoadingLocked()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 301
    const/16 v2, 0xd

    invoke-direct {p0, v2}, Lcom/google/android/music/art/ArtRequest2;->changeState(I)V

    .line 303
    :cond_4
    monitor-exit v3

    goto :goto_0

    .end local v1    # "wasRemoved":Z
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public notifyItemUnavailable(Ljava/lang/String;)V
    .locals 6
    .param p1, "neededItem"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x14

    .line 318
    iget v1, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    .line 319
    .local v1, "state":I
    if-eq v1, v2, :cond_0

    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    .line 320
    sget-boolean v2, Lcom/google/android/music/art/ArtRequest2;->LOGV:Z

    if-eqz v2, :cond_0

    .line 321
    const-string v2, "ArtResolver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notifyItemUnavailable called out of expected state. Current state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Item change rejected"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    :goto_0
    return-void

    .line 327
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtRequest2;->translateStringItemToArtUrl(Ljava/lang/String;)Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    move-result-object v0

    .line 328
    .local v0, "actualItem":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest2;->mItemsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 329
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest2;->mNeededItems:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 330
    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest2;->mNeededItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 331
    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest2;->mAvailableItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v4, p0, Lcom/google/android/music/art/ArtRequest2;->mPostProcessor:Lcom/google/android/music/art/ArtPostProcessor;

    iget-object v5, p0, Lcom/google/android/music/art/ArtRequest2;->mDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    invoke-interface {v4, v5}, Lcom/google/android/music/art/ArtPostProcessor;->getMinNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 332
    const/16 v2, 0x29

    invoke-direct {p0, v2}, Lcom/google/android/music/art/ArtRequest2;->changeState(I)V

    .line 336
    :cond_1
    :goto_1
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 334
    :cond_2
    const/16 v2, 0x14

    :try_start_1
    invoke-direct {p0, v2}, Lcom/google/android/music/art/ArtRequest2;->changeState(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public release()I
    .locals 5

    .prologue
    .line 497
    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest2;->mReferences:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 498
    .local v0, "countRemaining":I
    sget-boolean v2, Lcom/google/android/music/art/ArtRequest2;->LOGV:Z

    if-eqz v2, :cond_0

    .line 499
    const-string v2, "ArtRequest2"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "release countRemaining="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    :cond_0
    if-nez v0, :cond_3

    .line 502
    sget-boolean v2, Lcom/google/android/music/art/ArtRequest2;->LOGV:Z

    if-eqz v2, :cond_1

    .line 503
    const-string v2, "ArtRequest2"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "recycling request "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest2;->mAliasedBitmap:Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest2;->mAliasedBitmap:Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    invoke-virtual {v2}, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;->release()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 506
    invoke-static {}, Lcom/google/android/music/art/ArtResolver2;->getInstance()Lcom/google/android/music/art/ArtResolver2;

    move-result-object v1

    .line 507
    .local v1, "resolver":Lcom/google/android/music/art/ArtResolver2;
    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest2;->mAliasedBitmap:Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    invoke-virtual {v1, v2}, Lcom/google/android/music/art/ArtResolver2;->removeManagedBitmap(Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V

    .line 509
    .end local v1    # "resolver":Lcom/google/android/music/art/ArtResolver2;
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest2;->mRenderedBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    .line 510
    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest2;->mRenderedBitmap:Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/google/android/music/utils/AlbumArtUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 513
    :cond_3
    const/16 v2, 0x46

    invoke-direct {p0, v2}, Lcom/google/android/music/art/ArtRequest2;->changeState(I)V

    .line 514
    return v0
.end method

.method public renderImage([Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V
    .locals 9
    .param p1, "neededImages"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 405
    iget v3, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    const/16 v6, 0x15

    if-ne v3, v6, :cond_0

    move v3, v4

    :goto_0
    const-string v6, "ArtRequest2 must be in rendering state prior to calling renderImage. Current state is %s"

    new-array v7, v4, [Ljava/lang/Object;

    iget v8, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    invoke-direct {p0, v8}, Lcom/google/android/music/art/ArtRequest2;->getStateName(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-static {v3, v6, v7}, Lcom/google/android/common/base/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 408
    const/16 v3, 0x16

    invoke-direct {p0, v3}, Lcom/google/android/music/art/ArtRequest2;->changeState(I)V

    .line 409
    invoke-static {}, Lcom/google/android/music/art/ArtResolver2;->getInstance()Lcom/google/android/music/art/ArtResolver2;

    move-result-object v0

    .line 410
    .local v0, "resolver":Lcom/google/android/music/art/ArtResolver2;
    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest2;->mDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    invoke-virtual {v3}, Lcom/google/android/music/art/ArtDescriptor;->getBucketSize()I

    move-result v2

    .line 412
    .local v2, "width":I
    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest2;->mPostProcessor:Lcom/google/android/music/art/ArtPostProcessor;

    invoke-interface {v3, p0, p1}, Lcom/google/android/music/art/ArtPostProcessor;->getPostProcessingMode(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)I

    move-result v3

    iput v3, p0, Lcom/google/android/music/art/ArtRequest2;->mUsedMode:I

    .line 413
    iget v3, p0, Lcom/google/android/music/art/ArtRequest2;->mUsedMode:I

    packed-switch v3, :pswitch_data_0

    .line 427
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string v4, "Unknown mode requested"

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .end local v0    # "resolver":Lcom/google/android/music/art/ArtResolver2;
    .end local v2    # "width":I
    :cond_0
    move v3, v5

    .line 405
    goto :goto_0

    .line 415
    .restart local v0    # "resolver":Lcom/google/android/music/art/ArtResolver2;
    .restart local v2    # "width":I
    :pswitch_0
    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest2;->mDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    iget v3, v3, Lcom/google/android/music/art/ArtDescriptor;->aspectRatio:F

    iget-object v4, p0, Lcom/google/android/music/art/ArtRequest2;->mPostProcessor:Lcom/google/android/music/art/ArtPostProcessor;

    iget-object v5, p0, Lcom/google/android/music/art/ArtRequest2;->mDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    invoke-interface {v4, v5}, Lcom/google/android/music/art/ArtPostProcessor;->getConfig(Lcom/google/android/music/art/ArtDescriptor;)Landroid/graphics/Bitmap$Config;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/music/art/ArtResolver2;->getMutableBitmap(IFLandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/art/ArtRequest2;->mRenderedBitmap:Landroid/graphics/Bitmap;

    .line 417
    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest2;->mPostProcessor:Lcom/google/android/music/art/ArtPostProcessor;

    iget-object v4, p0, Lcom/google/android/music/art/ArtRequest2;->mRenderedBitmap:Landroid/graphics/Bitmap;

    invoke-interface {v3, p0, p1, v4}, Lcom/google/android/music/art/ArtPostProcessor;->renderPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;Landroid/graphics/Bitmap;)Z

    move-result v1

    .line 430
    .local v1, "success":Z
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/music/art/ArtRequest2;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_2

    .line 431
    if-eqz v1, :cond_5

    const/16 v3, 0x1e

    :goto_2
    invoke-direct {p0, v3}, Lcom/google/android/music/art/ArtRequest2;->changeState(I)V

    .line 433
    :cond_2
    sget-boolean v3, Lcom/google/android/music/art/ArtRequest2;->LOGV:Z

    if-eqz v3, :cond_3

    .line 434
    const-string v3, "ArtRequest2"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "finished rendering, new state="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    :cond_3
    return-void

    .line 420
    .end local v1    # "success":Z
    :pswitch_1
    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest2;->mPostProcessor:Lcom/google/android/music/art/ArtPostProcessor;

    invoke-interface {v3, p0, p1}, Lcom/google/android/music/art/ArtPostProcessor;->aliasPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/art/ArtRequest2;->mAliasedBitmap:Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .line 421
    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest2;->mAliasedBitmap:Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    if-eqz v3, :cond_4

    move v1, v4

    .line 422
    .restart local v1    # "success":Z
    :goto_3
    if-eqz v1, :cond_1

    .line 423
    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest2;->mAliasedBitmap:Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    invoke-virtual {v3}, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;->retain()V

    goto :goto_1

    .end local v1    # "success":Z
    :cond_4
    move v1, v5

    .line 421
    goto :goto_3

    .line 431
    .restart local v1    # "success":Z
    :cond_5
    const/16 v3, 0x2a

    goto :goto_2

    .line 413
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public retain()I
    .locals 4

    .prologue
    .line 486
    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest2;->mReferences:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 487
    .local v0, "count":I
    sget-boolean v1, Lcom/google/android/music/art/ArtRequest2;->LOGV:Z

    if-eqz v1, :cond_0

    .line 488
    const-string v1, "ArtRequest2"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "retain count="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    :cond_0
    return v0
.end method

.method public setListener(Lcom/google/android/music/art/ArtResolver2$RequestListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/music/art/ArtResolver2$RequestListener;

    .prologue
    .line 476
    iget-boolean v0, p0, Lcom/google/android/music/art/ArtRequest2;->mIsSynchronous:Z

    if-eqz v0, :cond_0

    .line 477
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot add listeners to a synchronous request"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 480
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/art/ArtRequest2;->mListener:Lcom/google/android/music/art/ArtResolver2$RequestListener;

    .line 481
    return-void
.end method

.method setPalette(Landroid/support/v7/graphics/Palette;)V
    .locals 0
    .param p1, "palette"    # Landroid/support/v7/graphics/Palette;

    .prologue
    .line 379
    iput-object p1, p0, Lcom/google/android/music/art/ArtRequest2;->mPalette:Landroid/support/v7/graphics/Palette;

    .line 380
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 519
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ArtRequest2{mState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/art/ArtRequest2;->mState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDescriptor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest2;->mDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
