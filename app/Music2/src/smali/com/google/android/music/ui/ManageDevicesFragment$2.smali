.class Lcom/google/android/music/ui/ManageDevicesFragment$2;
.super Ljava/lang/Object;
.source "ManageDevicesFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ManageDevicesFragment;->deauthorizeDevice(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mResponse:Z

.field final synthetic this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$deviceId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ManageDevicesFragment;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/google/android/music/ui/ManageDevicesFragment$2;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/ManageDevicesFragment$2;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/music/ui/ManageDevicesFragment$2;->val$deviceId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 4

    .prologue
    .line 168
    :try_start_0
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment$2;->val$context:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 169
    .local v0, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment$2;->val$deviceId:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/google/android/music/cloudclient/MusicCloud;->deleteUserDevice(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment$2;->mResponse:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 175
    .end local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :goto_0
    return-void

    .line 170
    :catch_0
    move-exception v1

    .line 171
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v2, "MusicDevices"

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 172
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 173
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "MusicDevices"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public taskCompleted()V
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$2;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$2;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # getter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$000(Lcom/google/android/music/ui/ManageDevicesFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$2;->mResponse:Z

    if-eqz v0, :cond_2

    .line 184
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$2;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->getUserDevices()V

    goto :goto_0

    .line 186
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$2;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/ManageDevicesFragment$2;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/ManageDevicesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0b032d

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
