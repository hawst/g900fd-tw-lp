.class Lcom/google/android/music/download/cache/CacheLocationManager$1$1;
.super Ljava/lang/Object;
.source "CacheLocationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/download/cache/CacheLocationManager$1;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/download/cache/CacheLocationManager$1;

.field final synthetic val$action:Ljava/lang/String;

.field final synthetic val$mountPoint:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/cache/CacheLocationManager$1;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$1$1;->this$1:Lcom/google/android/music/download/cache/CacheLocationManager$1;

    iput-object p2, p0, Lcom/google/android/music/download/cache/CacheLocationManager$1$1;->val$action:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/music/download/cache/CacheLocationManager$1$1;->val$mountPoint:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager$1$1;->this$1:Lcom/google/android/music/download/cache/CacheLocationManager$1;

    iget-object v0, v0, Lcom/google/android/music/download/cache/CacheLocationManager$1;->this$0:Lcom/google/android/music/download/cache/CacheLocationManager;

    # getter for: Lcom/google/android/music/download/cache/CacheLocationManager;->mApplicationContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/download/cache/CacheLocationManager;->access$000(Lcom/google/android/music/download/cache/CacheLocationManager;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isMainProcess(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager$1$1;->this$1:Lcom/google/android/music/download/cache/CacheLocationManager$1;

    iget-object v0, v0, Lcom/google/android/music/download/cache/CacheLocationManager$1;->this$0:Lcom/google/android/music/download/cache/CacheLocationManager;

    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$1$1;->val$action:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/download/cache/CacheLocationManager$1$1;->val$mountPoint:Landroid/net/Uri;

    # invokes: Lcom/google/android/music/download/cache/CacheLocationManager;->mainProcessInitialize(Ljava/lang/String;Landroid/net/Uri;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/music/download/cache/CacheLocationManager;->access$100(Lcom/google/android/music/download/cache/CacheLocationManager;Ljava/lang/String;Landroid/net/Uri;)V

    .line 85
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager$1$1;->this$1:Lcom/google/android/music/download/cache/CacheLocationManager$1;

    iget-object v0, v0, Lcom/google/android/music/download/cache/CacheLocationManager$1;->this$0:Lcom/google/android/music/download/cache/CacheLocationManager;

    # invokes: Lcom/google/android/music/download/cache/CacheLocationManager;->initialize()V
    invoke-static {v0}, Lcom/google/android/music/download/cache/CacheLocationManager;->access$200(Lcom/google/android/music/download/cache/CacheLocationManager;)V

    goto :goto_0
.end method
