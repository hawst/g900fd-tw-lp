.class public final Lcom/google/android/music/wear/WearMetadataSyncController;
.super Ljava/lang/Object;
.source "WearMetadataSyncController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/wear/WearMetadataSyncController$1;
    }
.end annotation


# static fields
.field private static final DATA_ITEM_PATHS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDataApiWrapper:Lcom/google/android/music/wear/util/DataApiWrapper;

.field private final mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 35
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "sync_enabled"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "collections"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "tracks"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/wear/WearMetadataSyncController;->DATA_ITEM_PATHS:Ljava/util/Set;

    return-void
.end method

.method constructor <init>(Lcom/google/android/music/wear/WearMusicDataProvider;Lcom/google/android/music/wear/util/DataApiWrapper;)V
    .locals 0
    .param p1, "musicDataProvider"    # Lcom/google/android/music/wear/WearMusicDataProvider;
    .param p2, "dataApiWrapper"    # Lcom/google/android/music/wear/util/DataApiWrapper;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/music/wear/WearMetadataSyncController;->mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

    .line 47
    iput-object p2, p0, Lcom/google/android/music/wear/WearMetadataSyncController;->mDataApiWrapper:Lcom/google/android/music/wear/util/DataApiWrapper;

    .line 48
    return-void
.end method

.method private static createPutRequests(Lcom/google/android/music/wear/WearMusicDataProvider;)Ljava/util/List;
    .locals 13
    .param p0, "musicDataProvider"    # Lcom/google/android/music/wear/WearMusicDataProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/wear/WearMusicDataProvider;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/wearable/PutDataMapRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 93
    .local v7, "syncedTrackIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v6, "putRequests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/wearable/PutDataMapRequest;>;"
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "Starting wear sync"

    aput-object v12, v10, v11

    invoke-static {v10}, Lcom/google/android/music/wear/WearMetadataSyncController;->debug([Ljava/lang/Object;)V

    .line 98
    invoke-static {}, Lcom/google/android/music/wear/WearMetadataSyncController;->getSyncEnabledPutRequest()Lcom/google/android/gms/wearable/PutDataMapRequest;

    move-result-object v10

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    invoke-interface {p0}, Lcom/google/android/music/wear/WearMusicDataProvider;->getSyncedMediaLists()Ljava/util/List;

    move-result-object v4

    .line 102
    .local v4, "mediaLists":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/wear/WearMediaList;>;"
    const/4 v5, 0x0

    .line 104
    .local v5, "numTracksAdded":I
    const/4 v0, 0x0

    .line 106
    .local v0, "collectionPriority":I
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v1, v10, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_2

    .line 107
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/wear/WearMediaList;

    .line 108
    .local v3, "list":Lcom/google/android/music/wear/WearMediaList;
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "Syncing list: "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v3, v10, v11

    invoke-static {v10}, Lcom/google/android/music/wear/WearMetadataSyncController;->debug([Ljava/lang/Object;)V

    .line 109
    invoke-interface {p0, v3}, Lcom/google/android/music/wear/WearMusicDataProvider;->getSyncedTracks(Lcom/google/android/music/wear/WearMediaList;)Ljava/util/List;

    move-result-object v9

    .line 110
    .local v9, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/wear/WearTrack;>;"
    invoke-static {v9}, Lcom/google/android/music/wear/WearMetadataSyncController;->getTrackIds(Ljava/util/List;)[J

    move-result-object v10

    invoke-static {v3, v10, v0}, Lcom/google/android/music/wear/WearMetadataSyncController;->getCollectionPutRequest(Lcom/google/android/music/wear/WearMediaList;[JI)Lcom/google/android/gms/wearable/PutDataMapRequest;

    move-result-object v10

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    add-int/lit8 v0, v0, 0x1

    .line 113
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/music/wear/WearTrack;

    .line 114
    .local v8, "track":Lcom/google/android/music/wear/WearTrack;
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "Found track: "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v8, v10, v11

    invoke-static {v10}, Lcom/google/android/music/wear/WearMetadataSyncController;->debug([Ljava/lang/Object;)V

    .line 115
    iget-wide v10, v8, Lcom/google/android/music/wear/WearTrack;->id:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 116
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "Adding data item for track: "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v8, v10, v11

    invoke-static {v10}, Lcom/google/android/music/wear/WearMetadataSyncController;->debug([Ljava/lang/Object;)V

    .line 117
    invoke-static {v8}, Lcom/google/android/music/wear/WearMetadataSyncController;->getTrackPutRequest(Lcom/google/android/music/wear/WearTrack;)Lcom/google/android/gms/wearable/PutDataMapRequest;

    move-result-object v10

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    add-int/lit8 v5, v5, 0x1

    .line 119
    const/16 v10, 0x3e8

    if-lt v5, v10, :cond_0

    .line 120
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "Hit MAX_TRACKS_TO_SYNC tracks, stopping iterating through tracks"

    aput-object v12, v10, v11

    invoke-static {v10}, Lcom/google/android/music/wear/WearMetadataSyncController;->debug([Ljava/lang/Object;)V

    .line 125
    .end local v8    # "track":Lcom/google/android/music/wear/WearTrack;
    :cond_1
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "Ended syncing list: "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v3, v10, v11

    invoke-static {v10}, Lcom/google/android/music/wear/WearMetadataSyncController;->debug([Ljava/lang/Object;)V

    .line 128
    const/16 v10, 0x3e8

    if-lt v5, v10, :cond_3

    .line 129
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "Hit MAX_TRACKS_TO_SYNC tracks, stopping iterating through lists"

    aput-object v12, v10, v11

    invoke-static {v10}, Lcom/google/android/music/wear/WearMetadataSyncController;->debug([Ljava/lang/Object;)V

    .line 134
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "list":Lcom/google/android/music/wear/WearMediaList;
    .end local v9    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/wear/WearTrack;>;"
    :cond_2
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "Wear sync finished"

    aput-object v12, v10, v11

    invoke-static {v10}, Lcom/google/android/music/wear/WearMetadataSyncController;->debug([Ljava/lang/Object;)V

    .line 135
    return-object v6

    .line 106
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "list":Lcom/google/android/music/wear/WearMediaList;
    .restart local v9    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/wear/WearTrack;>;"
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto/16 :goto_0
.end method

.method private static final varargs debug([Ljava/lang/Object;)V
    .locals 7
    .param p0, "parts"    # [Ljava/lang/Object;

    .prologue
    .line 225
    const-string v5, "MetadataSyncController"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 233
    :goto_0
    return-void

    .line 228
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 229
    .local v4, "sb":Ljava/lang/StringBuilder;
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/Object;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 230
    .local v3, "o":Ljava/lang/Object;
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 229
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 232
    .end local v3    # "o":Ljava/lang/Object;
    :cond_1
    const-string v5, "MetadataSyncController"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static getCollectionId(Lcom/google/android/music/wear/WearMediaList;)Ljava/lang/String;
    .locals 4
    .param p0, "list"    # Lcom/google/android/music/wear/WearMediaList;

    .prologue
    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/music/wear/WearMediaList;->type:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    invoke-virtual {v1}, Lcom/google/android/music/wear/WearMediaList$MediaListType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/wear/WearMediaList;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getCollectionPutRequest(Lcom/google/android/music/wear/WearMediaList;[JI)Lcom/google/android/gms/wearable/PutDataMapRequest;
    .locals 5
    .param p0, "list"    # Lcom/google/android/music/wear/WearMediaList;
    .param p1, "trackIds"    # [J
    .param p2, "priority"    # I

    .prologue
    .line 177
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/collections/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/music/wear/WearMetadataSyncController;->getCollectionId(Lcom/google/android/music/wear/WearMediaList;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 178
    .local v1, "path":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/gms/wearable/PutDataMapRequest;->create(Ljava/lang/String;)Lcom/google/android/gms/wearable/PutDataMapRequest;

    move-result-object v2

    .line 179
    .local v2, "request":Lcom/google/android/gms/wearable/PutDataMapRequest;
    invoke-virtual {v2}, Lcom/google/android/gms/wearable/PutDataMapRequest;->getDataMap()Lcom/google/android/gms/wearable/DataMap;

    move-result-object v0

    .line 180
    .local v0, "dataMap":Lcom/google/android/gms/wearable/DataMap;
    const-string v3, "name"

    iget-object v4, p0, Lcom/google/android/music/wear/WearMediaList;->title:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/wearable/DataMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v3, "priority"

    invoke-virtual {v0, v3, p2}, Lcom/google/android/gms/wearable/DataMap;->putInt(Ljava/lang/String;I)V

    .line 182
    const-string v3, "trackIds"

    invoke-virtual {v0, v3, p1}, Lcom/google/android/gms/wearable/DataMap;->putLongArray(Ljava/lang/String;[J)V

    .line 183
    const-string v3, "type"

    iget-object v4, p0, Lcom/google/android/music/wear/WearMediaList;->type:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    invoke-static {v4}, Lcom/google/android/music/wear/WearMetadataSyncController;->getTypeInt(Lcom/google/android/music/wear/WearMediaList$MediaListType;)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/wearable/DataMap;->putInt(Ljava/lang/String;I)V

    .line 184
    const-string v3, "downloadComplete"

    iget-boolean v4, p0, Lcom/google/android/music/wear/WearMediaList;->complete:Z

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/wearable/DataMap;->putBoolean(Ljava/lang/String;Z)V

    .line 185
    return-object v2
.end method

.method private static getSyncEnabledPutRequest()Lcom/google/android/gms/wearable/PutDataMapRequest;
    .locals 1

    .prologue
    .line 142
    const-string v0, "/sync_enabled"

    invoke-static {v0}, Lcom/google/android/gms/wearable/PutDataMapRequest;->create(Ljava/lang/String;)Lcom/google/android/gms/wearable/PutDataMapRequest;

    move-result-object v0

    return-object v0
.end method

.method private static final getTrackIds(Ljava/util/List;)[J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/wear/WearTrack;",
            ">;)[J"
        }
    .end annotation

    .prologue
    .line 236
    .local p0, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/wear/WearTrack;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    new-array v1, v2, [J

    .line 237
    .local v1, "trackIds":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 238
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/wear/WearTrack;

    iget-wide v2, v2, Lcom/google/android/music/wear/WearTrack;->id:J

    aput-wide v2, v1, v0

    .line 237
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 240
    :cond_0
    return-object v1
.end method

.method private static getTrackPutRequest(Lcom/google/android/music/wear/WearTrack;)Lcom/google/android/gms/wearable/PutDataMapRequest;
    .locals 6
    .param p0, "track"    # Lcom/google/android/music/wear/WearTrack;

    .prologue
    .line 149
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/tracks/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/wear/WearTrack;->id:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 150
    .local v1, "path":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/gms/wearable/PutDataMapRequest;->create(Ljava/lang/String;)Lcom/google/android/gms/wearable/PutDataMapRequest;

    move-result-object v0

    .line 151
    .local v0, "dataMap":Lcom/google/android/gms/wearable/PutDataMapRequest;
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/PutDataMapRequest;->getDataMap()Lcom/google/android/gms/wearable/DataMap;

    move-result-object v2

    const-string v3, "trackName"

    iget-object v4, p0, Lcom/google/android/music/wear/WearTrack;->title:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/wearable/DataMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iget-object v2, p0, Lcom/google/android/music/wear/WearTrack;->artistName:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 154
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/PutDataMapRequest;->getDataMap()Lcom/google/android/gms/wearable/DataMap;

    move-result-object v2

    const-string v3, "artist"

    iget-object v4, p0, Lcom/google/android/music/wear/WearTrack;->artistName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/wearable/DataMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_0
    iget-wide v2, p0, Lcom/google/android/music/wear/WearTrack;->artworkId:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 158
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/PutDataMapRequest;->getDataMap()Lcom/google/android/gms/wearable/DataMap;

    move-result-object v2

    const-string v3, "artworkId"

    iget-wide v4, p0, Lcom/google/android/music/wear/WearTrack;->artworkId:J

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/wearable/DataMap;->putLong(Ljava/lang/String;J)V

    .line 161
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/PutDataMapRequest;->getDataMap()Lcom/google/android/gms/wearable/DataMap;

    move-result-object v2

    const-string v3, "sizeBytes"

    iget-wide v4, p0, Lcom/google/android/music/wear/WearTrack;->sizeBytes:J

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/wearable/DataMap;->putLong(Ljava/lang/String;J)V

    .line 162
    return-object v0
.end method

.method private static getTypeInt(Lcom/google/android/music/wear/WearMediaList$MediaListType;)I
    .locals 3
    .param p0, "type"    # Lcom/google/android/music/wear/WearMediaList$MediaListType;

    .prologue
    .line 248
    sget-object v0, Lcom/google/android/music/wear/WearMetadataSyncController$1;->$SwitchMap$com$google$android$music$wear$WearMediaList$MediaListType:[I

    invoke-virtual {p0}, Lcom/google/android/music/wear/WearMediaList$MediaListType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 258
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported list type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 250
    :pswitch_0
    const/4 v0, 0x1

    .line 256
    :goto_0
    return v0

    .line 252
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 254
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 256
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 248
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private removeOtherDataItems(Ljava/util/Set;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "pathsWanted":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 195
    :try_start_0
    iget-object v8, p0, Lcom/google/android/music/wear/WearMetadataSyncController;->mDataApiWrapper:Lcom/google/android/music/wear/util/DataApiWrapper;

    const-wide/16 v10, 0x1

    sget-object v9, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v8, v10, v11, v9}, Lcom/google/android/music/wear/util/DataApiWrapper;->getAllDataItems(JLjava/util/concurrent/TimeUnit;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/music/wear/util/GmsApiException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    .line 208
    .local v1, "dataItems":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/Uri;Lcom/google/android/gms/wearable/DataMap;>;"
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 210
    .local v0, "dataItemUri":Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    .line 211
    .local v4, "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    if-lt v8, v6, :cond_2

    sget-object v8, Lcom/google/android/music/wear/WearMetadataSyncController;->DATA_ITEM_PATHS:Ljava/util/Set;

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p1, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    move v5, v6

    .line 214
    .local v5, "shouldRemove":Z
    :goto_1
    if-eqz v5, :cond_0

    .line 215
    iget-object v8, p0, Lcom/google/android/music/wear/WearMetadataSyncController;->mDataApiWrapper:Lcom/google/android/music/wear/util/DataApiWrapper;

    invoke-interface {v8, v0}, Lcom/google/android/music/wear/util/DataApiWrapper;->deleteDataItems(Landroid/net/Uri;)V

    .line 216
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const-string v9, "Deleting "

    aput-object v9, v8, v7

    aput-object v0, v8, v6

    invoke-static {v8}, Lcom/google/android/music/wear/WearMetadataSyncController;->debug([Ljava/lang/Object;)V

    goto :goto_0

    .line 196
    .end local v0    # "dataItemUri":Landroid/net/Uri;
    .end local v1    # "dataItems":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/Uri;Lcom/google/android/gms/wearable/DataMap;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v5    # "shouldRemove":Z
    :catch_0
    move-exception v2

    .line 197
    .local v2, "e":Ljava/lang/InterruptedException;
    const-string v6, "MetadataSyncController"

    const-string v7, "Interrupted while getting all data items"

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 198
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V

    .line 219
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_1
    :goto_2
    return-void

    .line 200
    :catch_1
    move-exception v2

    .line 201
    .local v2, "e":Ljava/util/concurrent/TimeoutException;
    const-string v6, "MetadataSyncController"

    const-string v7, "Timeout while getting all data items"

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 203
    .end local v2    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_2
    move-exception v2

    .line 204
    .local v2, "e":Lcom/google/android/music/wear/util/GmsApiException;
    const-string v6, "MetadataSyncController"

    const-string v7, "Exception while getting all data items"

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .end local v2    # "e":Lcom/google/android/music/wear/util/GmsApiException;
    .restart local v0    # "dataItemUri":Landroid/net/Uri;
    .restart local v1    # "dataItems":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/Uri;Lcom/google/android/gms/wearable/DataMap;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    move v5, v7

    .line 211
    goto :goto_1
.end method


# virtual methods
.method public updateDataItems()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 56
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 59
    .local v3, "wantedItemPaths":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/google/android/music/wear/WearMetadataSyncController;->mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

    invoke-interface {v4}, Lcom/google/android/music/wear/WearMusicDataProvider;->isWearSyncEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 60
    iget-object v4, p0, Lcom/google/android/music/wear/WearMetadataSyncController;->mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

    invoke-static {v4}, Lcom/google/android/music/wear/WearMetadataSyncController;->createPutRequests(Lcom/google/android/music/wear/WearMusicDataProvider;)Ljava/util/List;

    move-result-object v1

    .line 61
    .local v1, "putRequests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/wearable/PutDataMapRequest;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/wearable/PutDataMapRequest;

    .line 62
    .local v2, "request":Lcom/google/android/gms/wearable/PutDataMapRequest;
    new-array v4, v8, [Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Put "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/PutDataMapRequest;->getUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v4}, Lcom/google/android/music/wear/WearMetadataSyncController;->debug([Ljava/lang/Object;)V

    .line 63
    iget-object v4, p0, Lcom/google/android/music/wear/WearMetadataSyncController;->mDataApiWrapper:Lcom/google/android/music/wear/util/DataApiWrapper;

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/PutDataMapRequest;->asPutDataRequest()Lcom/google/android/gms/wearable/PutDataRequest;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/music/wear/util/DataApiWrapper;->putDataItem(Lcom/google/android/gms/wearable/PutDataRequest;)V

    .line 64
    invoke-virtual {v2}, Lcom/google/android/gms/wearable/PutDataMapRequest;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 67
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "putRequests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/wearable/PutDataMapRequest;>;"
    .end local v2    # "request":Lcom/google/android/gms/wearable/PutDataMapRequest;
    :cond_0
    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "Wear sync disabled"

    aput-object v5, v4, v7

    invoke-static {v4}, Lcom/google/android/music/wear/WearMetadataSyncController;->debug([Ljava/lang/Object;)V

    .line 72
    :cond_1
    invoke-direct {p0, v3}, Lcom/google/android/music/wear/WearMetadataSyncController;->removeOtherDataItems(Ljava/util/Set;)V

    .line 73
    return-void
.end method
