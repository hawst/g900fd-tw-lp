.class Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;
.super Landroid/widget/BaseAdapter;
.source "GenreQuizActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/quiz/GenreQuizActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GenreQuizAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/quiz/GenreQuizActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/music/quiz/GenreQuizActivity;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/quiz/GenreQuizActivity;Lcom/google/android/music/quiz/GenreQuizActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/quiz/GenreQuizActivity;
    .param p2, "x1"    # Lcom/google/android/music/quiz/GenreQuizActivity$1;

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;-><init>(Lcom/google/android/music/quiz/GenreQuizActivity;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    # getter for: Lcom/google/android/music/quiz/GenreQuizActivity;->mGenresList:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/quiz/GenreQuizActivity;->access$300(Lcom/google/android/music/quiz/GenreQuizActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/google/android/music/cloudclient/QuizGenreJson;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    # getter for: Lcom/google/android/music/quiz/GenreQuizActivity;->mGenresList:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/quiz/GenreQuizActivity;->access$300(Lcom/google/android/music/quiz/GenreQuizActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/QuizGenreJson;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 165
    invoke-virtual {p0, p1}, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;->getItem(I)Lcom/google/android/music/cloudclient/QuizGenreJson;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 181
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 186
    const/4 v1, 0x0

    .line 187
    .local v1, "vh":Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;
    if-nez p2, :cond_0

    .line 188
    iget-object v2, p0, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    invoke-virtual {v2}, Lcom/google/android/music/quiz/GenreQuizActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 189
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f040034

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 190
    new-instance v1, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;

    .end local v1    # "vh":Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;
    iget-object v2, p0, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    invoke-direct {v1, v2, v5}, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;-><init>(Lcom/google/android/music/quiz/GenreQuizActivity;Lcom/google/android/music/quiz/GenreQuizActivity$1;)V

    .line 191
    .restart local v1    # "vh":Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;
    const v2, 0x7f0e011a

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/widgets/ArtImageCheckbox;

    iput-object v2, v1, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    .line 192
    iget-object v2, v1, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    iget-object v3, p0, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    # getter for: Lcom/google/android/music/quiz/GenreQuizActivity;->mOnCheckedChangeListener:Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;
    invoke-static {v3}, Lcom/google/android/music/quiz/GenreQuizActivity;->access$500(Lcom/google/android/music/quiz/GenreQuizActivity;)Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setOnCheckedChangeListener(Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;)V

    .line 193
    iget-object v2, v1, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setCropToCircle(Z)V

    .line 194
    iget-object v2, v1, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    const/16 v3, 0x12f

    invoke-virtual {v2, v3}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setDefaultArtId(I)V

    .line 195
    const v2, 0x7f0e011b

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mName:Landroid/widget/TextView;

    .line 196
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 201
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;->getItem(I)Lcom/google/android/music/cloudclient/QuizGenreJson;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/music/cloudclient/QuizGenreJson;->mType:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mGenreId:Ljava/lang/String;

    .line 203
    iget-object v2, v1, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    invoke-virtual {p0, p1}, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;->getItem(I)Lcom/google/android/music/cloudclient/QuizGenreJson;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/widgets/ArtImageCheckbox;->bind(Lcom/google/android/music/cloudclient/QuizGenreJson;)V

    .line 204
    iget-object v2, v1, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    invoke-virtual {v2, v1}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setTag(Ljava/lang/Object;)V

    .line 205
    iget-object v2, v1, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    iget-object v3, p0, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    # getter for: Lcom/google/android/music/quiz/GenreQuizActivity;->mSelectedGenreIds:Ljava/util/HashSet;
    invoke-static {v3}, Lcom/google/android/music/quiz/GenreQuizActivity;->access$600(Lcom/google/android/music/quiz/GenreQuizActivity;)Ljava/util/HashSet;

    move-result-object v3

    iget-object v4, v1, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mGenreId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setChecked(Z)V

    .line 206
    iget-object v2, v1, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mName:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;->getItem(I)Lcom/google/android/music/cloudclient/QuizGenreJson;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/music/cloudclient/QuizGenreJson;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    new-instance v2, Lcom/google/android/music/quiz/GenreQuizActivity$OnGenreSelectedListner;

    iget-object v3, p0, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    invoke-direct {v2, v3, v5}, Lcom/google/android/music/quiz/GenreQuizActivity$OnGenreSelectedListner;-><init>(Lcom/google/android/music/quiz/GenreQuizActivity;Lcom/google/android/music/quiz/GenreQuizActivity$1;)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    return-object p2

    .line 198
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "vh":Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;
    check-cast v1, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;

    .restart local v1    # "vh":Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;
    goto :goto_0
.end method
