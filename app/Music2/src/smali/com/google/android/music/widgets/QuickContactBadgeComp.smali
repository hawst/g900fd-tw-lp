.class public Lcom/google/android/music/widgets/QuickContactBadgeComp;
.super Landroid/widget/QuickContactBadge;
.source "QuickContactBadgeComp.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentUrl:Ljava/lang/String;

.field private mQuickContactBadgeHeight:I

.field private mQuickContactBadgeWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/QuickContactBadge;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    iput-object p1, p0, Lcom/google/android/music/widgets/QuickContactBadgeComp;->mContext:Landroid/content/Context;

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 28
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0f00b5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/widgets/QuickContactBadgeComp;->mQuickContactBadgeWidth:I

    .line 29
    const v1, 0x7f0f00b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/widgets/QuickContactBadgeComp;->mQuickContactBadgeHeight:I

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/widgets/QuickContactBadgeComp;Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/widgets/QuickContactBadgeComp;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/widgets/QuickContactBadgeComp;->getPhoto(Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/widgets/QuickContactBadgeComp;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/widgets/QuickContactBadgeComp;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/music/widgets/QuickContactBadgeComp;->mCurrentUrl:Ljava/lang/String;

    return-object v0
.end method

.method private getPhoto(Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "allowResolve"    # Z

    .prologue
    const/4 v4, 0x0

    .line 80
    iget-object v0, p0, Lcom/google/android/music/widgets/QuickContactBadgeComp;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/music/widgets/QuickContactBadgeComp;->mQuickContactBadgeWidth:I

    iget v3, p0, Lcom/google/android/music/widgets/QuickContactBadgeComp;->mQuickContactBadgeHeight:I

    const/4 v8, 0x1

    move-object v1, p1

    move v5, v4

    move v6, p2

    move v7, v4

    invoke-static/range {v0 .. v8}, Lcom/google/android/music/utils/AlbumArtUtils;->getArtworkFromUrl(Landroid/content/Context;Ljava/lang/String;IIZZZZZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public setImageToDefault()V
    .locals 2

    .prologue
    .line 34
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 35
    const v0, 0x7f0200de

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/QuickContactBadgeComp;->setImageResource(I)V

    .line 39
    :goto_0
    return-void

    .line 37
    :cond_0
    invoke-super {p0}, Landroid/widget/QuickContactBadge;->setImageToDefault()V

    goto :goto_0
.end method

.method public declared-synchronized setImageUrl(Ljava/lang/String;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 43
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/widgets/QuickContactBadgeComp;->mCurrentUrl:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    :goto_0
    monitor-exit p0

    return-void

    .line 44
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/google/android/music/widgets/QuickContactBadgeComp;->mCurrentUrl:Ljava/lang/String;

    .line 45
    if-nez p1, :cond_1

    .line 46
    invoke-virtual {p0}, Lcom/google/android/music/widgets/QuickContactBadgeComp;->setImageToDefault()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 43
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 50
    :cond_1
    const/4 v1, 0x0

    :try_start_2
    invoke-direct {p0, p1, v1}, Lcom/google/android/music/widgets/QuickContactBadgeComp;->getPhoto(Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 51
    .local v0, "photo":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 52
    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/QuickContactBadgeComp;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 54
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/widgets/QuickContactBadgeComp;->setImageToDefault()V

    .line 55
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sUIBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/widgets/QuickContactBadgeComp$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/music/widgets/QuickContactBadgeComp$1;-><init>(Lcom/google/android/music/widgets/QuickContactBadgeComp;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsyncWithCallback(Lcom/google/android/music/utils/LoggableHandler;Lcom/google/android/music/utils/async/AsyncRunner;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
