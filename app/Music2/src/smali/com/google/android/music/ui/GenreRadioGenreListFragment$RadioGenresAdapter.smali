.class Lcom/google/android/music/ui/GenreRadioGenreListFragment$RadioGenresAdapter;
.super Lcom/google/android/music/ui/MediaListCursorAdapter;
.source "GenreRadioGenreListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/GenreRadioGenreListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RadioGenresAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/GenreRadioGenreListFragment;


# direct methods
.method protected constructor <init>(Lcom/google/android/music/ui/GenreRadioGenreListFragment;Lcom/google/android/music/ui/GenreRadioGenreListFragment;)V
    .locals 1
    .param p2, "musicFragment"    # Lcom/google/android/music/ui/GenreRadioGenreListFragment;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/music/ui/GenreRadioGenreListFragment$RadioGenresAdapter;->this$0:Lcom/google/android/music/ui/GenreRadioGenreListFragment;

    .line 98
    const v0, 0x1090003

    invoke-direct {p0, p2, v0}, Lcom/google/android/music/ui/MediaListCursorAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;I)V

    .line 99
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/music/ui/GenreRadioGenreListFragment;Lcom/google/android/music/ui/GenreRadioGenreListFragment;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "musicFragment"    # Lcom/google/android/music/ui/GenreRadioGenreListFragment;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/music/ui/GenreRadioGenreListFragment$RadioGenresAdapter;->this$0:Lcom/google/android/music/ui/GenreRadioGenreListFragment;

    .line 102
    const v0, 0x1090003

    invoke-direct {p0, p2, v0, p3}, Lcom/google/android/music/ui/MediaListCursorAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ILandroid/database/Cursor;)V

    .line 103
    return-void
.end method


# virtual methods
.method protected bindViewToLoadingItem(Landroid/view/View;Landroid/content/Context;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 126
    const v1, 0x1020014

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 127
    .local v0, "tv":Landroid/widget/TextView;
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    return-void
.end method

.method protected bindViewToMediaListItem(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;J)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "itemId"    # J

    .prologue
    .line 108
    const/4 v6, 0x1

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 109
    .local v3, "genreNautilusId":Ljava/lang/String;
    const/4 v6, 0x2

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 110
    .local v2, "genreName":Ljava/lang/String;
    const/4 v6, 0x3

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 111
    .local v5, "subgenreCount":I
    const/4 v6, 0x4

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "artUrls":Ljava/lang/String;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 114
    .local v1, "extra":Landroid/os/Bundle;
    const-string v6, "nautilusId"

    invoke-virtual {v1, v6, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v6, "name"

    invoke-virtual {v1, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v6, "subgenreCount"

    invoke-virtual {v1, v6, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117
    const-string v6, "artUrls"

    invoke-virtual {v1, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 120
    const v6, 0x1020014

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 121
    .local v4, "nameView":Landroid/widget/TextView;
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 132
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/MediaListCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 133
    .local v4, "view":Landroid/view/View;
    iget-object v5, p0, Lcom/google/android/music/ui/GenreRadioGenreListFragment$RadioGenresAdapter;->this$0:Lcom/google/android/music/ui/GenreRadioGenreListFragment;

    invoke-virtual {v5}, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f0138

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 134
    .local v1, "left":I
    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 135
    .local v2, "right":I
    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    .line 136
    .local v3, "top":I
    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    .line 137
    .local v0, "bottom":I
    invoke-virtual {v4, v1, v3, v2, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 138
    return-object v4
.end method
