.class public Lcom/google/android/music/leanback/LeanbackCardNowPlayingView;
.super Lcom/google/android/music/leanback/LeanbackNowPlayingView;
.source "LeanbackCardNowPlayingView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method


# virtual methods
.method protected getBarDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 38
    const v0, 0x7f0201c3

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected getBarRightMarginPx(Landroid/content/res/Resources;)I
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 33
    const v0, 0x7f0f0183

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method protected getBarsLeftMarginPx(Landroid/content/res/Resources;)I
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 23
    const/4 v0, 0x0

    return v0
.end method

.method protected getBarsTopMarginPx(Landroid/content/res/Resources;)I
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 28
    const/4 v0, 0x0

    return v0
.end method

.method protected getPlayIconDimensionsPx(Landroid/content/res/Resources;)I
    .locals 2
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/google/android/music/leanback/LeanbackCardNowPlayingView;->getBarRightMarginPx(Landroid/content/res/Resources;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    const v1, 0x7f0f0181

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v0, v1

    return v0
.end method
