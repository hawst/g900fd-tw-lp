.class public final enum Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;
.super Ljava/lang/Enum;
.source "PlayQueueFeeder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/mix/PlayQueueFeeder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PostProcessingAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

.field public static final enum CLEAR_REFRESH:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

.field public static final enum FEED:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

.field public static final enum FEED_REFRESH:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

.field public static final enum NEXT:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

.field public static final enum NONE:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

.field public static final enum OPEN:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 58
    new-instance v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    const-string v1, "OPEN"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->OPEN:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .line 59
    new-instance v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    const-string v1, "NEXT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->NEXT:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .line 60
    new-instance v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    const-string v1, "FEED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->FEED:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .line 61
    new-instance v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v6}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->NONE:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .line 62
    new-instance v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    const-string v1, "FEED_REFRESH"

    invoke-direct {v0, v1, v7}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->FEED_REFRESH:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .line 63
    new-instance v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    const-string v1, "CLEAR_REFRESH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->CLEAR_REFRESH:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .line 57
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    sget-object v1, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->OPEN:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->NEXT:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->FEED:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->NONE:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->FEED_REFRESH:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->CLEAR_REFRESH:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->$VALUES:[Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 57
    const-class v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->$VALUES:[Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    invoke-virtual {v0}, [Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    return-object v0
.end method
