.class Lcom/google/android/music/dial/DialDevice$2;
.super Ljava/lang/Object;
.source "DialDevice.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/dial/DialDevice;->update(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/dial/DialDevice;

.field final synthetic val$appUrl:Ljava/lang/String;

.field final synthetic val$groupName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/dial/DialDevice;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcom/google/android/music/dial/DialDevice$2;->this$0:Lcom/google/android/music/dial/DialDevice;

    iput-object p2, p0, Lcom/google/android/music/dial/DialDevice$2;->val$appUrl:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/music/dial/DialDevice$2;->val$groupName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 197
    iget-object v3, p0, Lcom/google/android/music/dial/DialDevice$2;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;
    invoke-static {v3}, Lcom/google/android/music/dial/DialDevice;->access$100(Lcom/google/android/music/dial/DialDevice;)Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getApplicationUrl()Landroid/net/Uri;

    move-result-object v0

    .line 198
    .local v0, "currentAppUrl":Landroid/net/Uri;
    iget-object v3, p0, Lcom/google/android/music/dial/DialDevice$2;->val$appUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 199
    .local v1, "newAppUrl":Landroid/net/Uri;
    iget-object v3, p0, Lcom/google/android/music/dial/DialDevice$2;->this$0:Lcom/google/android/music/dial/DialDevice;

    new-instance v4, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    iget-object v5, p0, Lcom/google/android/music/dial/DialDevice$2;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;
    invoke-static {v5}, Lcom/google/android/music/dial/DialDevice;->access$100(Lcom/google/android/music/dial/DialDevice;)Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;-><init>(Lcom/google/android/music/dial/RemoteDeviceInfo;)V

    iget-object v5, p0, Lcom/google/android/music/dial/DialDevice$2;->val$groupName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setGroupName(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setApplicationUrl(Landroid/net/Uri;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setIsGroupCoordinator(Z)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->build()Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v4

    # setter for: Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;
    invoke-static {v3, v4}, Lcom/google/android/music/dial/DialDevice;->access$102(Lcom/google/android/music/dial/DialDevice;Lcom/google/android/music/dial/RemoteDeviceInfo;)Lcom/google/android/music/dial/RemoteDeviceInfo;

    .line 206
    invoke-virtual {v1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 207
    # getter for: Lcom/google/android/music/dial/DialDevice;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/dial/DialDevice;->access$400()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 208
    const-string v3, "DialDevice"

    const-string v4, "Reconnecting since app URL changed from %s to %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v7

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/dial/DialDevice$2;->this$0:Lcom/google/android/music/dial/DialDevice;

    iget-object v4, p0, Lcom/google/android/music/dial/DialDevice$2;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;
    invoke-static {v4}, Lcom/google/android/music/dial/DialDevice;->access$500(Lcom/google/android/music/dial/DialDevice;)Lcom/google/android/music/dial/WebSocket;

    move-result-object v4

    # invokes: Lcom/google/android/music/dial/DialDevice;->teardownSocket(Lcom/google/android/music/dial/WebSocket;)V
    invoke-static {v3, v4}, Lcom/google/android/music/dial/DialDevice;->access$600(Lcom/google/android/music/dial/DialDevice;Lcom/google/android/music/dial/WebSocket;)V

    .line 212
    iget-object v3, p0, Lcom/google/android/music/dial/DialDevice$2;->this$0:Lcom/google/android/music/dial/DialDevice;

    const/4 v4, 0x0

    # setter for: Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;
    invoke-static {v3, v4}, Lcom/google/android/music/dial/DialDevice;->access$502(Lcom/google/android/music/dial/DialDevice;Lcom/google/android/music/dial/WebSocket;)Lcom/google/android/music/dial/WebSocket;

    .line 213
    iget-object v3, p0, Lcom/google/android/music/dial/DialDevice$2;->this$0:Lcom/google/android/music/dial/DialDevice;

    # invokes: Lcom/google/android/music/dial/DialDevice;->getSocketInternal()Lcom/google/android/music/dial/WebSocket;
    invoke-static {v3}, Lcom/google/android/music/dial/DialDevice;->access$000(Lcom/google/android/music/dial/DialDevice;)Lcom/google/android/music/dial/WebSocket;

    move-result-object v2

    .line 214
    .local v2, "socket":Lcom/google/android/music/dial/WebSocket;
    if-nez v2, :cond_1

    .line 215
    const-string v3, "DialDevice"

    const-string v4, "Socket connection failed for %s."

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/music/dial/DialDevice$2;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;
    invoke-static {v6}, Lcom/google/android/music/dial/DialDevice;->access$100(Lcom/google/android/music/dial/DialDevice;)Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    iget-object v3, p0, Lcom/google/android/music/dial/DialDevice$2;->this$0:Lcom/google/android/music/dial/DialDevice;

    iget-object v4, p0, Lcom/google/android/music/dial/DialDevice$2;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mRouteId:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/music/dial/DialDevice;->access$200(Lcom/google/android/music/dial/DialDevice;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/dial/DialDevice$Listener$Error;->SOCKET_CONNECT:Lcom/google/android/music/dial/DialDevice$Listener$Error;

    # invokes: Lcom/google/android/music/dial/DialDevice;->callListenersOnError(Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Listener$Error;)V
    invoke-static {v3, v4, v5}, Lcom/google/android/music/dial/DialDevice;->access$300(Lcom/google/android/music/dial/DialDevice;Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Listener$Error;)V

    .line 220
    .end local v2    # "socket":Lcom/google/android/music/dial/WebSocket;
    :cond_1
    return-void
.end method
