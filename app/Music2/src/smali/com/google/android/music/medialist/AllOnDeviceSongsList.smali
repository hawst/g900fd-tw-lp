.class public Lcom/google/android/music/medialist/AllOnDeviceSongsList;
.super Lcom/google/android/music/medialist/AllSongsList;
.source "AllOnDeviceSongsList.java"


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "sortOrder"    # I

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/medialist/AllSongsList;-><init>(IZ)V

    .line 18
    return-void
.end method


# virtual methods
.method public getArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 31
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/music/medialist/AllOnDeviceSongsList;->getSortOrder()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/google/android/music/medialist/AllSongsList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    .line 23
    .local v1, "parentUri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 24
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v2, "filter"

    sget-object v3, Lcom/google/android/music/store/MusicContent;->PARAM_FILTER_USER_AND_EXTERNAL_LOCAL_KEPT_AND_CACHED:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 26
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    return-object v2
.end method
