.class public Lcom/google/android/music/dial/model/PlayCommandJson;
.super Lcom/google/android/music/dial/model/GenericDialCommand;
.source "PlayCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/model/PlayCommandJson$PlayCommandBody;
    }
.end annotation


# instance fields
.field public mBody:Lcom/google/android/music/dial/model/PlayCommandJson$PlayCommandBody;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "body"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/music/dial/model/GenericDialCommand;-><init>()V

    .line 24
    iget-object v0, p0, Lcom/google/android/music/dial/model/PlayCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "playback:1"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mNamespace:Ljava/lang/String;

    .line 25
    iget-object v0, p0, Lcom/google/android/music/dial/model/PlayCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "play"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mCommand:Ljava/lang/String;

    .line 27
    new-instance v0, Lcom/google/android/music/dial/model/PlayCommandJson$PlayCommandBody;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/PlayCommandJson$PlayCommandBody;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/dial/model/PlayCommandJson;->mBody:Lcom/google/android/music/dial/model/PlayCommandJson$PlayCommandBody;

    .line 28
    return-void
.end method


# virtual methods
.method protected getBodyAsJsonString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/music/dial/model/PlayCommandJson;->mBody:Lcom/google/android/music/dial/model/PlayCommandJson$PlayCommandBody;

    invoke-virtual {v0}, Lcom/google/android/music/dial/model/PlayCommandJson$PlayCommandBody;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
