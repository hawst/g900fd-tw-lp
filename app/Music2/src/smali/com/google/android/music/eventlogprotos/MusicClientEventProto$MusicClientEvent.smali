.class public final Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
.super Lcom/google/protobuf/nano/MessageNano;
.source "MusicClientEventProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/eventlogprotos/MusicClientEventProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MusicClientEvent"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;


# instance fields
.field public downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

.field public navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

.field public obfuscatedGaiaId:Ljava/lang/Long;

.field public playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

.field public queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

.field public signupEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

.field public uploadEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$UploadEventInfo;

.field public userServiceLevel:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 368
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    sput-object v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 369
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 372
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    .line 375
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    .line 378
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    .line 381
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->signupEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    .line 384
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->userServiceLevel:I

    .line 389
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->uploadEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$UploadEventInfo;

    .line 392
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    .line 369
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 425
    const/4 v0, 0x0

    .line 426
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    if-eqz v1, :cond_0

    .line 427
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 430
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    if-eqz v1, :cond_1

    .line 431
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 434
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    if-eqz v1, :cond_2

    .line 435
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 438
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->signupEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    if-eqz v1, :cond_3

    .line 439
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->signupEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 442
    :cond_3
    iget v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->userServiceLevel:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_4

    .line 443
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->userServiceLevel:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 446
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->obfuscatedGaiaId:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 447
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->obfuscatedGaiaId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 450
    :cond_5
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->uploadEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$UploadEventInfo;

    if-eqz v1, :cond_6

    .line 451
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->uploadEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$UploadEventInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 454
    :cond_6
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    if-eqz v1, :cond_7

    .line 455
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 458
    :cond_7
    iput v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->cachedSize:I

    .line 459
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 467
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 468
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 472
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 473
    :sswitch_0
    return-object p0

    .line 478
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    if-nez v2, :cond_1

    .line 479
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    .line 481
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 485
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    if-nez v2, :cond_2

    .line 486
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    .line 488
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 492
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    if-nez v2, :cond_3

    .line 493
    new-instance v2, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    .line 495
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 499
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->signupEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    if-nez v2, :cond_4

    .line 500
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->signupEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    .line 502
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->signupEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 506
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 507
    .local v1, "temp":I
    if-eqz v1, :cond_5

    const/4 v2, 0x1

    if-eq v1, v2, :cond_5

    const/4 v2, 0x2

    if-eq v1, v2, :cond_5

    const/4 v2, 0x3

    if-ne v1, v2, :cond_6

    .line 511
    :cond_5
    iput v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->userServiceLevel:I

    goto :goto_0

    .line 513
    :cond_6
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->userServiceLevel:I

    goto :goto_0

    .line 518
    .end local v1    # "temp":I
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->obfuscatedGaiaId:Ljava/lang/Long;

    goto :goto_0

    .line 522
    :sswitch_7
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->uploadEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$UploadEventInfo;

    if-nez v2, :cond_7

    .line 523
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$UploadEventInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$UploadEventInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->uploadEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$UploadEventInfo;

    .line 525
    :cond_7
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->uploadEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$UploadEventInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 529
    :sswitch_8
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    if-nez v2, :cond_8

    .line 530
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    .line 532
    :cond_8
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 468
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 365
    invoke-virtual {p0, p1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    if-eqz v0, :cond_0

    .line 398
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    if-eqz v0, :cond_1

    .line 401
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 403
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    if-eqz v0, :cond_2

    .line 404
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 406
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->signupEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    if-eqz v0, :cond_3

    .line 407
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->signupEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 409
    :cond_3
    iget v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->userServiceLevel:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 410
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->userServiceLevel:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 412
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->obfuscatedGaiaId:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 413
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->obfuscatedGaiaId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 415
    :cond_5
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->uploadEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$UploadEventInfo;

    if-eqz v0, :cond_6

    .line 416
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->uploadEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$UploadEventInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 418
    :cond_6
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    if-eqz v0, :cond_7

    .line 419
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 421
    :cond_7
    return-void
.end method
