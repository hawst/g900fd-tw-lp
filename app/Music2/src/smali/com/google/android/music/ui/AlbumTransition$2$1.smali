.class Lcom/google/android/music/ui/AlbumTransition$2$1;
.super Ljava/lang/Object;
.source "AlbumTransition.java"

# interfaces
.implements Landroid/transition/Transition$TransitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/AlbumTransition$2;->onTransitionEnd(Landroid/transition/Transition;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/ui/AlbumTransition$2;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/AlbumTransition$2;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/google/android/music/ui/AlbumTransition$2$1;->this$1:Lcom/google/android/music/ui/AlbumTransition$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransitionCancel(Landroid/transition/Transition;)V
    .locals 0
    .param p1, "arg0"    # Landroid/transition/Transition;

    .prologue
    .line 403
    return-void
.end method

.method public onTransitionEnd(Landroid/transition/Transition;)V
    .locals 2
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/music/ui/AlbumTransition$2$1;->this$1:Lcom/google/android/music/ui/AlbumTransition$2;

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$2;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    iget-object v1, p0, Lcom/google/android/music/ui/AlbumTransition$2$1;->this$1:Lcom/google/android/music/ui/AlbumTransition$2;

    iget-object v1, v1, Lcom/google/android/music/ui/AlbumTransition$2;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mRootSharedElementId:I
    invoke-static {v1}, Lcom/google/android/music/ui/AlbumTransition;->access$1500(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v1

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->findViewById(I)Landroid/view/View;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/AlbumTransition;->access$900(Lcom/google/android/music/ui/AlbumTransition;I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/AlbumTransition$2$1;->this$1:Lcom/google/android/music/ui/AlbumTransition$2;

    iget-object v1, v1, Lcom/google/android/music/ui/AlbumTransition$2;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mEndingSharedElementColor:I
    invoke-static {v1}, Lcom/google/android/music/ui/AlbumTransition;->access$1400(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 400
    return-void
.end method

.method public onTransitionPause(Landroid/transition/Transition;)V
    .locals 0
    .param p1, "arg0"    # Landroid/transition/Transition;

    .prologue
    .line 395
    return-void
.end method

.method public onTransitionResume(Landroid/transition/Transition;)V
    .locals 0
    .param p1, "arg0"    # Landroid/transition/Transition;

    .prologue
    .line 392
    return-void
.end method

.method public onTransitionStart(Landroid/transition/Transition;)V
    .locals 0
    .param p1, "arg0"    # Landroid/transition/Transition;

    .prologue
    .line 389
    return-void
.end method
