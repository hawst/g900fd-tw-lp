.class Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$3;
.super Ljava/lang/Object;
.source "TutorialTryNautilusActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->addMoreInfoIcon()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$3;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 193
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$3;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    const-string v3, "accessibility"

    invoke-virtual {v2, v3}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 195
    .local v0, "am":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    .line 196
    .local v1, "isAccessibilityEnabled":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 197
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$3;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    # invokes: Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->launchMoreInfoLink(Landroid/content/Context;)V
    invoke-static {v2}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->access$500(Landroid/content/Context;)V

    .line 199
    :cond_0
    return-void

    .line 195
    .end local v1    # "isAccessibilityEnabled":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
