.class public Lcom/google/android/music/utils/ByteUtils;
.super Ljava/lang/Object;
.source "ByteUtils.java"


# direct methods
.method public static bytesEqual([BI[BII)Z
    .locals 4
    .param p0, "a"    # [B
    .param p1, "aOffset"    # I
    .param p2, "b"    # [B
    .param p3, "bOffset"    # I
    .param p4, "length"    # I

    .prologue
    .line 13
    const/4 v1, 0x1

    .line 14
    .local v1, "result":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p4, :cond_1

    .line 15
    add-int v2, p1, v0

    aget-byte v2, p0, v2

    add-int v3, p3, v0

    aget-byte v3, p2, v3

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_1
    and-int/2addr v1, v2

    .line 14
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 17
    :cond_1
    return v1
.end method
