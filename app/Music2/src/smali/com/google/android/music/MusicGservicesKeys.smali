.class public Lcom/google/android/music/MusicGservicesKeys;
.super Ljava/lang/Object;
.source "MusicGservicesKeys.java"


# static fields
.field public static final DEFAULT_MUSIC_ICING_NOTIFICATION_DELAY_MS:J

.field public static final DEFAULT_STORAGE_LOW_BROADCAST_TIMEOUT_MILLIS:J

.field public static MUSIC_CONFIG_SYNC_ALARM_MIN:Ljava/lang/String;

.field public static MUSIC_EXPIRATION_SYNC_ALARM_MINUTES:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 559
    const-string v0, "music_config_sync_alarm_min"

    sput-object v0, Lcom/google/android/music/MusicGservicesKeys;->MUSIC_CONFIG_SYNC_ALARM_MIN:Ljava/lang/String;

    .line 569
    const-string v0, "music_exp_sync_alarm_minutes"

    sput-object v0, Lcom/google/android/music/MusicGservicesKeys;->MUSIC_EXPIRATION_SYNC_ALARM_MINUTES:Ljava/lang/String;

    .line 967
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/music/MusicGservicesKeys;->DEFAULT_MUSIC_ICING_NOTIFICATION_DELAY_MS:J

    .line 1266
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/music/MusicGservicesKeys;->DEFAULT_STORAGE_LOW_BROADCAST_TIMEOUT_MILLIS:J

    return-void
.end method
