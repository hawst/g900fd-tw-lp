.class final Lcom/google/android/music/utils/MusicUtils$3;
.super Ljava/lang/Object;
.source "MusicUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/utils/MusicUtils;->moveQueueItem(Landroid/content/Context;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$fromPos:I

.field final synthetic val$isInCloudQueueMode:Z

.field final synthetic val$toPos:I


# direct methods
.method constructor <init>(Landroid/content/Context;ZII)V
    .locals 0

    .prologue
    .line 1558
    iput-object p1, p0, Lcom/google/android/music/utils/MusicUtils$3;->val$context:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/google/android/music/utils/MusicUtils$3;->val$isInCloudQueueMode:Z

    iput p3, p0, Lcom/google/android/music/utils/MusicUtils$3;->val$fromPos:I

    iput p4, p0, Lcom/google/android/music/utils/MusicUtils$3;->val$toPos:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1560
    iget-object v0, p0, Lcom/google/android/music/utils/MusicUtils$3;->val$context:Landroid/content/Context;

    iget-boolean v1, p0, Lcom/google/android/music/utils/MusicUtils$3;->val$isInCloudQueueMode:Z

    iget v2, p0, Lcom/google/android/music/utils/MusicUtils$3;->val$fromPos:I

    iget v3, p0, Lcom/google/android/music/utils/MusicUtils$3;->val$toPos:I

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/store/MusicContent$Queue;->moveItem(Landroid/content/Context;ZII)V

    .line 1562
    return-void
.end method
