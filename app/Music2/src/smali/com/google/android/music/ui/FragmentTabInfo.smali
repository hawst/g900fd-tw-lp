.class final Lcom/google/android/music/ui/FragmentTabInfo;
.super Ljava/lang/Object;
.source "FragmentTabInfo.java"


# instance fields
.field public final mFragmentInfo:Lcom/google/android/music/ui/FragmentInfo;

.field public final mOnlineOnly:Z

.field public final mPageWidth:F

.field public final mResId:I

.field public final mTag:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;FZ)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "resId"    # I
    .param p4, "fragmentArgs"    # Landroid/os/Bundle;
    .param p5, "viewWidth"    # F
    .param p6, "onlineOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Class",
            "<*>;",
            "Landroid/os/Bundle;",
            "FZ)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p3, "fragmentClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "invalid tag"

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 60
    iput-object p1, p0, Lcom/google/android/music/ui/FragmentTabInfo;->mTag:Ljava/lang/String;

    .line 61
    iput p2, p0, Lcom/google/android/music/ui/FragmentTabInfo;->mResId:I

    .line 62
    new-instance v0, Lcom/google/android/music/ui/FragmentInfo;

    invoke-direct {v0, p3, p4}, Lcom/google/android/music/ui/FragmentInfo;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/music/ui/FragmentTabInfo;->mFragmentInfo:Lcom/google/android/music/ui/FragmentInfo;

    .line 63
    iput p5, p0, Lcom/google/android/music/ui/FragmentTabInfo;->mPageWidth:F

    .line 64
    iput-boolean p6, p0, Lcom/google/android/music/ui/FragmentTabInfo;->mOnlineOnly:Z

    .line 65
    return-void

    .line 59
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;Z)V
    .locals 7
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "resId"    # I
    .param p4, "fragmentArgs"    # Landroid/os/Bundle;
    .param p5, "onlineOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Class",
            "<*>;",
            "Landroid/os/Bundle;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p3, "fragmentClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;FZ)V

    .line 44
    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILjava/lang/Class;Z)V
    .locals 6
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "resId"    # I
    .param p4, "onlineOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Class",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p3, "fragmentClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;Z)V

    .line 30
    return-void
.end method
