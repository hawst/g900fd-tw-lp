.class Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor$ArtLoaderImplFutureTask;
.super Ljava/util/concurrent/FutureTask;
.source "ArtLoaderImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArtLoaderImplFutureTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/FutureTask",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final mRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;Ljava/lang/Object;)V
    .locals 0
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 329
    .local p0, "this":Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor$ArtLoaderImplFutureTask;, "Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor$ArtLoaderImplFutureTask<TV;>;"
    .local p2, "result":Ljava/lang/Object;, "TV;"
    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 330
    iput-object p1, p0, Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor$ArtLoaderImplFutureTask;->mRunnable:Ljava/lang/Runnable;

    .line 331
    return-void
.end method


# virtual methods
.method public getRunnable()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 334
    .local p0, "this":Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor$ArtLoaderImplFutureTask;, "Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor$ArtLoaderImplFutureTask<TV;>;"
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor$ArtLoaderImplFutureTask;->mRunnable:Ljava/lang/Runnable;

    return-object v0
.end method
