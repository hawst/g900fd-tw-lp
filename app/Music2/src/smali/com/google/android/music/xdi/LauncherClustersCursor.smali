.class Lcom/google/android/music/xdi/LauncherClustersCursor;
.super Landroid/database/MatrixCursor;
.source "LauncherClustersCursor.java"


# static fields
.field private static final PROJECTION_ALBUMS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mImageHeight:I

.field private final mImageWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/LauncherClustersCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 28
    iput-object p1, p0, Lcom/google/android/music/xdi/LauncherClustersCursor;->mContext:Landroid/content/Context;

    .line 29
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/LauncherClustersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 31
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/LauncherClustersCursor;->mImageWidth:I

    .line 32
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/LauncherClustersCursor;->mImageHeight:I

    .line 34
    invoke-direct {p0}, Lcom/google/android/music/xdi/LauncherClustersCursor;->addRowsForMyMusic()V

    .line 35
    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "clusterId"    # J

    .prologue
    .line 39
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 40
    iput-object p1, p0, Lcom/google/android/music/xdi/LauncherClustersCursor;->mContext:Landroid/content/Context;

    .line 41
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/LauncherClustersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 43
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/LauncherClustersCursor;->mImageWidth:I

    .line 44
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/LauncherClustersCursor;->mImageHeight:I

    .line 45
    return-void
.end method

.method private addRowsForMyMusic()V
    .locals 15

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 51
    iget-object v0, p0, Lcom/google/android/music/xdi/LauncherClustersCursor;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/google/android/music/store/MusicContent$Albums;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/music/xdi/LauncherClustersCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move v7, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v9

    .line 56
    .local v9, "cursor":Landroid/database/Cursor;
    if-nez v9, :cond_0

    .line 79
    :goto_0
    return-void

    .line 61
    :cond_0
    const/4 v10, 0x1

    .line 62
    .local v10, "i":I
    const/4 v14, 0x2

    .line 64
    .local v14, "visibleCount":I
    :goto_1
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    if-gt v10, v0, :cond_1

    .line 65
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 66
    .local v12, "id":J
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/music/xdi/LauncherClustersCursor;->mImageWidth:I

    iget v2, p0, Lcom/google/android/music/xdi/LauncherClustersCursor;->mImageHeight:I

    invoke-static {v12, v13, v0, v1, v2}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v8

    .line 69
    .local v8, "artUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/music/xdi/LauncherClustersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v0}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v0

    new-array v11, v0, [Ljava/lang/Object;

    .line 70
    .local v11, "values":[Ljava/lang/Object;
    iget-object v0, p0, Lcom/google/android/music/xdi/LauncherClustersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "_id"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v11, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/music/xdi/LauncherClustersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "image_uri"

    invoke-virtual {v0, v11, v1, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 73
    invoke-virtual {p0, v11}, Lcom/google/android/music/xdi/LauncherClustersCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    add-int/lit8 v10, v10, 0x1

    .line 75
    goto :goto_1

    .line 77
    .end local v8    # "artUri":Landroid/net/Uri;
    .end local v11    # "values":[Ljava/lang/Object;
    .end local v12    # "id":J
    :cond_1
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method
