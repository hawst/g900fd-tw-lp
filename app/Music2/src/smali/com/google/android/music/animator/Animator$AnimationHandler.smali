.class Lcom/google/android/music/animator/Animator$AnimationHandler;
.super Landroid/os/Handler;
.source "Animator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/animator/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AnimationHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 276
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/animator/Animator$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/animator/Animator$1;

    .prologue
    .line 276
    invoke-direct {p0}, Lcom/google/android/music/animator/Animator$AnimationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 291
    const/4 v1, 0x1

    .line 292
    .local v1, "callAgain":Z
    iget v8, p1, Landroid/os/Message;->what:I

    packed-switch v8, :pswitch_data_0

    .line 355
    :cond_0
    :goto_0
    return-void

    .line 296
    :pswitch_0
    # getter for: Lcom/google/android/music/animator/Animator;->animations:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$000()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-gtz v8, :cond_1

    # getter for: Lcom/google/android/music/animator/Animator;->delayedAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$100()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_2

    .line 297
    :cond_1
    const/4 v1, 0x0

    .line 299
    :cond_2
    # getter for: Lcom/google/android/music/animator/Animator;->pendingAnimations:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$200()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 300
    .local v2, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v2, :cond_4

    .line 301
    # getter for: Lcom/google/android/music/animator/Animator;->pendingAnimations:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$200()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/animator/Animator;

    .line 303
    .local v0, "anim":Lcom/google/android/music/animator/Animator;
    # getter for: Lcom/google/android/music/animator/Animator;->mStartDelay:J
    invoke-static {v0}, Lcom/google/android/music/animator/Animator;->access$300(Lcom/google/android/music/animator/Animator;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_3

    .line 304
    # invokes: Lcom/google/android/music/animator/Animator;->startAnimation()V
    invoke-static {v0}, Lcom/google/android/music/animator/Animator;->access$400(Lcom/google/android/music/animator/Animator;)V

    .line 300
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 306
    :cond_3
    # getter for: Lcom/google/android/music/animator/Animator;->delayedAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$100()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 309
    .end local v0    # "anim":Lcom/google/android/music/animator/Animator;
    :cond_4
    # getter for: Lcom/google/android/music/animator/Animator;->pendingAnimations:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$200()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 314
    .end local v2    # "count":I
    .end local v3    # "i":I
    :pswitch_1
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    .line 318
    .local v4, "currentTime":J
    # getter for: Lcom/google/android/music/animator/Animator;->delayedAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$100()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 319
    .local v7, "numDelayedAnims":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    if-ge v3, v7, :cond_6

    .line 320
    # getter for: Lcom/google/android/music/animator/Animator;->delayedAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$100()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/animator/Animator;

    .line 321
    .restart local v0    # "anim":Lcom/google/android/music/animator/Animator;
    # invokes: Lcom/google/android/music/animator/Animator;->delayedAnimationFrame(J)Z
    invoke-static {v0, v4, v5}, Lcom/google/android/music/animator/Animator;->access$500(Lcom/google/android/music/animator/Animator;J)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 322
    # getter for: Lcom/google/android/music/animator/Animator;->readyAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$600()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 319
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 324
    .end local v0    # "anim":Lcom/google/android/music/animator/Animator;
    :cond_6
    # getter for: Lcom/google/android/music/animator/Animator;->readyAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$600()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_8

    .line 325
    const/4 v3, 0x0

    :goto_4
    # getter for: Lcom/google/android/music/animator/Animator;->readyAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$600()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v3, v8, :cond_7

    .line 326
    # getter for: Lcom/google/android/music/animator/Animator;->readyAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$600()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/animator/Animator;

    .line 327
    .restart local v0    # "anim":Lcom/google/android/music/animator/Animator;
    # invokes: Lcom/google/android/music/animator/Animator;->startAnimation()V
    invoke-static {v0}, Lcom/google/android/music/animator/Animator;->access$400(Lcom/google/android/music/animator/Animator;)V

    .line 328
    # getter for: Lcom/google/android/music/animator/Animator;->delayedAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$100()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 325
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 330
    .end local v0    # "anim":Lcom/google/android/music/animator/Animator;
    :cond_7
    # getter for: Lcom/google/android/music/animator/Animator;->readyAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$600()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 335
    :cond_8
    # getter for: Lcom/google/android/music/animator/Animator;->animations:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$000()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 336
    .local v6, "numAnims":I
    const/4 v3, 0x0

    :goto_5
    if-ge v3, v6, :cond_a

    .line 337
    # getter for: Lcom/google/android/music/animator/Animator;->animations:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$000()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/animator/Animator;

    .line 338
    .restart local v0    # "anim":Lcom/google/android/music/animator/Animator;
    # invokes: Lcom/google/android/music/animator/Animator;->animationFrame(J)Z
    invoke-static {v0, v4, v5}, Lcom/google/android/music/animator/Animator;->access$700(Lcom/google/android/music/animator/Animator;J)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 339
    # getter for: Lcom/google/android/music/animator/Animator;->endingAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$800()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 336
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 342
    .end local v0    # "anim":Lcom/google/android/music/animator/Animator;
    :cond_a
    # getter for: Lcom/google/android/music/animator/Animator;->endingAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$800()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_c

    .line 343
    const/4 v3, 0x0

    :goto_6
    # getter for: Lcom/google/android/music/animator/Animator;->endingAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$800()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v3, v8, :cond_b

    .line 344
    # getter for: Lcom/google/android/music/animator/Animator;->endingAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$800()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/music/animator/Animator;

    # invokes: Lcom/google/android/music/animator/Animator;->endAnimation()V
    invoke-static {v8}, Lcom/google/android/music/animator/Animator;->access$900(Lcom/google/android/music/animator/Animator;)V

    .line 343
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 346
    :cond_b
    # getter for: Lcom/google/android/music/animator/Animator;->endingAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$800()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 351
    :cond_c
    if-eqz v1, :cond_0

    # getter for: Lcom/google/android/music/animator/Animator;->animations:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$000()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_d

    # getter for: Lcom/google/android/music/animator/Animator;->delayedAnims:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$100()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 352
    :cond_d
    const/4 v8, 0x1

    # getter for: Lcom/google/android/music/animator/Animator;->mFrameDelay:J
    invoke-static {}, Lcom/google/android/music/animator/Animator;->access$1000()J

    move-result-wide v10

    invoke-virtual {p0, v8, v10, v11}, Lcom/google/android/music/animator/Animator$AnimationHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 292
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
