.class public Lcom/google/android/music/tutorial/TutorialConfirmNautilusActivity;
.super Lcom/google/android/music/tutorial/TutorialActivity;
.source "TutorialConfirmNautilusActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 17
    invoke-super {p0}, Lcom/google/android/music/tutorial/TutorialActivity;->isActivityDestroyed()Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 39
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0e0283

    if-ne v0, v1, :cond_0

    .line 41
    const/4 v0, 0x1

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isQuizEnabledOnSignUp()Z

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialConfirmNautilusActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/music/tutorial/TutorialUtils;->finishTutorialPermanently(Landroid/app/Activity;ZZLcom/google/android/music/preferences/MusicPreferences;)V

    .line 44
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x3

    .line 21
    invoke-super {p0, p1}, Lcom/google/android/music/tutorial/TutorialActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    const v3, 0x7f040101

    invoke-virtual {p0, v3}, Lcom/google/android/music/tutorial/TutorialConfirmNautilusActivity;->setContentView(I)V

    .line 24
    const v3, 0x7f0e01c5

    invoke-virtual {p0, v3}, Lcom/google/android/music/tutorial/TutorialConfirmNautilusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 25
    .local v2, "title":Landroid/widget/TextView;
    invoke-static {v2, v4}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 27
    const v3, 0x7f0e0251

    invoke-virtual {p0, v3}, Lcom/google/android/music/tutorial/TutorialConfirmNautilusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 28
    .local v0, "description":Landroid/widget/TextView;
    const/4 v3, 0x1

    invoke-static {v0, v3}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 30
    const v3, 0x7f0e0283

    invoke-virtual {p0, v3}, Lcom/google/android/music/tutorial/TutorialConfirmNautilusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 31
    .local v1, "getStartedBtn":Landroid/widget/Button;
    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    .line 32
    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    invoke-static {v1, v4}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 35
    return-void
.end method
