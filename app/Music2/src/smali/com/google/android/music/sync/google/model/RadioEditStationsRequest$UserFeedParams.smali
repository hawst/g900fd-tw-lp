.class public Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$UserFeedParams;
.super Lcom/google/api/client/json/GenericJson;
.source "RadioEditStationsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserFeedParams"
.end annotation


# instance fields
.field public mContentFilter:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "contentFilter"
    .end annotation
.end field

.field public mLanguageCode:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "languageCode"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
