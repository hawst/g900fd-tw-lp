.class Lcom/google/android/music/playback/MediaButtonIntentReceiver$MediaButtonReceiverHelper;
.super Ljava/lang/Object;
.source "MediaButtonIntentReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/MediaButtonIntentReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MediaButtonReceiverHelper"
.end annotation


# direct methods
.method static onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 63
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 64
    .local v7, "intentAction":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 65
    .local v3, "extras":Landroid/os/Bundle;
    if-eqz v3, :cond_0

    .line 66
    invoke-virtual {v3}, Landroid/os/Bundle;->size()I

    .line 68
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Received: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 69
    const-string v9, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 70
    const-string v9, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/view/KeyEvent;

    .line 73
    .local v2, "event":Landroid/view/KeyEvent;
    if-nez v2, :cond_2

    .line 152
    .end local v2    # "event":Landroid/view/KeyEvent;
    :cond_1
    :goto_0
    return-void

    .line 77
    .restart local v2    # "event":Landroid/view/KeyEvent;
    :cond_2
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    .line 78
    .local v8, "keycode":I
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 79
    .local v0, "action":I
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    .line 85
    .local v4, "eventtime":J
    const/4 v1, 0x0

    .line 86
    .local v1, "command":Ljava/lang/String;
    sparse-switch v8, :sswitch_data_0

    .line 108
    :goto_1
    if-eqz v1, :cond_1

    .line 109
    if-nez v0, :cond_6

    .line 110
    # getter for: Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mDown:Z
    invoke-static {}, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->access$100()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 111
    const-string v9, "togglepause"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "play"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_3
    # getter for: Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mLastClickTime:J
    invoke-static {}, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->access$200()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-eqz v9, :cond_1

    # getter for: Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mLastClickTime:J
    invoke-static {}, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->access$200()J

    move-result-wide v10

    sub-long v10, v4, v10

    const-wide/16 v12, 0x3e8

    cmp-long v9, v10, v12

    if-lez v9, :cond_1

    .line 115
    # getter for: Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->access$300()Landroid/os/Handler;

    move-result-object v9

    # getter for: Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->access$300()Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 88
    :sswitch_0
    const-string v1, "stop"

    .line 89
    goto :goto_1

    .line 92
    :sswitch_1
    const-string v1, "togglepause"

    .line 93
    goto :goto_1

    .line 95
    :sswitch_2
    const-string v1, "next"

    .line 96
    goto :goto_1

    .line 98
    :sswitch_3
    const-string v1, "previous"

    .line 99
    goto :goto_1

    .line 101
    :sswitch_4
    const-string v1, "pause"

    .line 102
    goto :goto_1

    .line 104
    :sswitch_5
    const-string v1, "play"

    goto :goto_1

    .line 118
    :cond_4
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v9

    if-nez v9, :cond_1

    .line 126
    new-instance v6, Landroid/content/Intent;

    const-class v9, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-direct {v6, p0, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 127
    .local v6, "i":Landroid/content/Intent;
    const-string v9, "com.android.music.musicservicecommand"

    invoke-virtual {v6, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 128
    const-string v9, "device"

    const-string v10, "any"

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 130
    const/16 v9, 0x4f

    if-ne v8, v9, :cond_5

    # getter for: Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mLastClickTime:J
    invoke-static {}, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->access$200()J

    move-result-wide v10

    sub-long v10, v4, v10

    const-wide/16 v12, 0x12c

    cmp-long v9, v10, v12

    if-gez v9, :cond_5

    .line 132
    const-string v9, "command"

    const-string v10, "next"

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    invoke-static {p0, v6}, Lcom/google/android/music/playback/MediaButtonIntentReceiver$MediaButtonReceiverHelper;->startService(Landroid/content/Context;Landroid/content/Intent;)V

    .line 135
    const-wide/16 v10, 0x0

    # setter for: Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mLastClickTime:J
    invoke-static {v10, v11}, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->access$202(J)J

    .line 143
    :goto_2
    const/4 v9, 0x0

    # setter for: Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mLaunched:Z
    invoke-static {v9}, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->access$002(Z)Z

    .line 144
    const/4 v9, 0x1

    # setter for: Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mDown:Z
    invoke-static {v9}, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->access$102(Z)Z

    goto/16 :goto_0

    .line 137
    :cond_5
    const-string v9, "command"

    invoke-virtual {v6, v9, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    const-string v9, "removeNotification"

    const/4 v10, 0x1

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 139
    invoke-static {p0, v6}, Lcom/google/android/music/playback/MediaButtonIntentReceiver$MediaButtonReceiverHelper;->startService(Landroid/content/Context;Landroid/content/Intent;)V

    .line 140
    # setter for: Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mLastClickTime:J
    invoke-static {v4, v5}, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->access$202(J)J

    goto :goto_2

    .line 147
    .end local v6    # "i":Landroid/content/Intent;
    :cond_6
    # getter for: Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->access$300()Landroid/os/Handler;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 148
    const/4 v9, 0x0

    # setter for: Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mDown:Z
    invoke-static {v9}, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->access$102(Z)Z

    goto/16 :goto_0

    .line 86
    nop

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_1
        0x55 -> :sswitch_1
        0x56 -> :sswitch_0
        0x57 -> :sswitch_2
        0x58 -> :sswitch_3
        0x7e -> :sswitch_5
        0x7f -> :sswitch_4
    .end sparse-switch
.end method

.method static startService(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x1

    .line 155
    sget-object v1, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 156
    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 157
    .local v0, "pmgr":Landroid/os/PowerManager;
    const-class v1, Lcom/google/android/music/playback/MediaButtonIntentReceiver;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    sput-object v1, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 160
    .end local v0    # "pmgr":Landroid/os/PowerManager;
    :cond_0
    sget-object v1, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 161
    sget-object v1, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->RELEASE_RECEIVER_LOCK:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 162
    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 163
    return-void
.end method
