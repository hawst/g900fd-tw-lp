.class public Lcom/google/android/music/browse/config/ClientConfigurationManager;
.super Ljava/lang/Object;
.source "ClientConfigurationManager.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final ns:Ljava/lang/String;


# instance fields
.field private mClients:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/music/browse/config/ClientConfiguration;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/android/music/browse/config/ClientConfigurationManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/browse/config/ClientConfigurationManager;->TAG:Ljava/lang/String;

    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/music/browse/config/ClientConfigurationManager;->ns:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/browse/config/ClientConfigurationManager;->mClients:Ljava/util/Map;

    .line 42
    :try_start_0
    const-string v1, "clients"

    invoke-static {p1, v1}, Lcom/google/android/music/browse/config/ClientConfigurationManager;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/music/browse/config/ClientConfigurationManager;->readConfigurations(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    .line 47
    :goto_0
    return-void

    .line 44
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    sget-object v1, Lcom/google/android/music/browse/config/ClientConfigurationManager;->TAG:Ljava/lang/String;

    const-string v2, "Error getting client configuration!"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 44
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private static final beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
    .locals 4
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "firstElementName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 132
    :cond_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .local v0, "type":I
    if-eq v0, v2, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 136
    :cond_1
    if-eq v0, v2, :cond_2

    .line 137
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "No start tag found"

    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 140
    :cond_2
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 141
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected start tag: found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 144
    :cond_3
    return-void
.end method

.method private readClientConfiguration(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/android/music/browse/config/ClientConfiguration;
    .locals 6
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    .line 74
    sget-object v3, Lcom/google/android/music/browse/config/ClientConfigurationManager;->ns:Ljava/lang/String;

    const-string v4, "client"

    invoke-interface {p1, v5, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 75
    new-instance v0, Lcom/google/android/music/browse/config/ClientConfiguration;

    invoke-direct {v0}, Lcom/google/android/music/browse/config/ClientConfiguration;-><init>()V

    .line 76
    .local v0, "client":Lcom/google/android/music/browse/config/ClientConfiguration;
    const/4 v3, 0x0

    const-string v4, "pkg"

    invoke-interface {p1, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/music/browse/config/ClientConfiguration;->setPackage(Ljava/lang/String;)V

    .line 77
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "client"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 78
    :cond_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 81
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, "name":Ljava/lang/String;
    const-string v3, "item"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 83
    invoke-virtual {v0}, Lcom/google/android/music/browse/config/ClientConfiguration;->getPackage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1, v3}, Lcom/google/android/music/browse/config/ClientConfigurationManager;->readItem(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    move-result-object v1

    .line 84
    .local v1, "item":Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
    invoke-virtual {v0, v1}, Lcom/google/android/music/browse/config/ClientConfiguration;->setRoot(Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;)V

    goto :goto_0

    .line 86
    .end local v1    # "item":Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/music/browse/config/ClientConfigurationManager;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 89
    .end local v2    # "name":Ljava/lang/String;
    :cond_3
    return-object v0
.end method

.method private readConfigurations(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 5
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    .line 55
    sget-object v2, Lcom/google/android/music/browse/config/ClientConfigurationManager;->ns:Ljava/lang/String;

    const-string v3, "clients"

    invoke-interface {p1, v4, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 56
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "clients"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 58
    :cond_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    if-ne v2, v4, :cond_0

    .line 61
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 63
    .local v1, "name":Ljava/lang/String;
    const-string v2, "client"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/music/browse/config/ClientConfigurationManager;->readClientConfiguration(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/android/music/browse/config/ClientConfiguration;

    move-result-object v0

    .line 65
    .local v0, "client":Lcom/google/android/music/browse/config/ClientConfiguration;
    iget-object v2, p0, Lcom/google/android/music/browse/config/ClientConfigurationManager;->mClients:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/music/browse/config/ClientConfiguration;->getPackage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 67
    .end local v0    # "client":Lcom/google/android/music/browse/config/ClientConfiguration;
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/music/browse/config/ClientConfigurationManager;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 70
    .end local v1    # "name":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method private readItem(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
    .locals 10
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2, "parentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 94
    sget-object v6, Lcom/google/android/music/browse/config/ClientConfigurationManager;->ns:Ljava/lang/String;

    const-string v7, "item"

    invoke-interface {p1, v9, v6, v7}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 95
    new-instance v3, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    invoke-direct {v3}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;-><init>()V

    .line 96
    .local v3, "item":Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
    const-string v6, "id"

    invoke-interface {p1, v8, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 97
    .local v2, "id":Ljava/lang/String;
    invoke-virtual {v3, v2}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->setId(Ljava/lang/String;)V

    .line 98
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->setFullId(Ljava/lang/String;)V

    .line 100
    invoke-static {p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    .line 101
    .local v0, "attributes":Landroid/util/AttributeSet;
    const-string v6, "display"

    const/4 v7, 0x0

    invoke-interface {v0, v8, v6, v7}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->setDisplay(I)V

    .line 102
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_1

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "item"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 103
    :cond_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v6

    if-ne v6, v9, :cond_0

    .line 106
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 107
    .local v5, "name":Ljava/lang/String;
    const-string v6, "content"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 108
    const-string v6, "type"

    invoke-interface {p1, v8, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 109
    .local v1, "content":Ljava/lang/String;
    const-string v6, "max"

    invoke-interface {p1, v8, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 110
    .local v4, "max":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 111
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v3, v1, v6}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->addContent(Ljava/lang/String;I)V

    goto :goto_0

    .line 113
    :cond_2
    const v6, 0x7fffffff

    invoke-virtual {v3, v1, v6}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->addContent(Ljava/lang/String;I)V

    goto :goto_0

    .line 115
    .end local v1    # "content":Ljava/lang/String;
    .end local v4    # "max":Ljava/lang/String;
    :cond_3
    const-string v6, "item"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 116
    invoke-virtual {v3}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->getFullId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p1, v6}, Lcom/google/android/music/browse/config/ClientConfigurationManager;->readItem(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->addItem(Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;)V

    goto :goto_0

    .line 118
    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/music/browse/config/ClientConfigurationManager;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 121
    .end local v5    # "name":Ljava/lang/String;
    :cond_5
    return-object v3
.end method

.method private skip(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 148
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 150
    :cond_0
    const/4 v0, 0x1

    .line 151
    .local v0, "depth":I
    :goto_0
    if-eqz v0, :cond_1

    .line 152
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 157
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    :pswitch_1
    add-int/lit8 v0, v0, -0x1

    .line 155
    goto :goto_0

    .line 161
    :cond_1
    return-void

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getConfiguration(Ljava/lang/String;)Lcom/google/android/music/browse/config/ClientConfiguration;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/browse/config/ClientConfigurationManager;->mClients:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/browse/config/ClientConfiguration;

    return-object v0
.end method
