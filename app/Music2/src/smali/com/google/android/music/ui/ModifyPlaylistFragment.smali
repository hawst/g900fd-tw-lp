.class public Lcom/google/android/music/ui/ModifyPlaylistFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "ModifyPlaylistFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/ModifyPlaylistFragment$5;,
        Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;
    }
.end annotation


# instance fields
.field private mIsCreate:Z

.field private mIsDismissed:Z

.field private mIsSideloaded:Z

.field private mNetworkConnected:Z

.field private mPlaylistDescriptionEditor:Landroid/widget/EditText;

.field private mPlaylistNameEditor:Landroid/widget/EditText;

.field private volatile mPlaylistSongList:Lcom/google/android/music/medialist/PlaylistSongList;

.field final mPositiveListener:Landroid/content/DialogInterface$OnClickListener;

.field private mShareState:I

.field private mShareSwitch:Landroid/widget/CompoundButton;

.field private mSongsToAdd:Lcom/google/android/music/medialist/SongList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 256
    new-instance v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/ModifyPlaylistFragment$2;-><init>(Lcom/google/android/music/ui/ModifyPlaylistFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPositiveListener:Landroid/content/DialogInterface$OnClickListener;

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Lcom/google/android/music/medialist/PlaylistSongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ModifyPlaylistFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistSongList:Lcom/google/android/music/medialist/PlaylistSongList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ModifyPlaylistFragment;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsDismissed:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ModifyPlaylistFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistNameEditor:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ModifyPlaylistFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistDescriptionEditor:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ModifyPlaylistFragment;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsCreate:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/ModifyPlaylistFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ModifyPlaylistFragment;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->handleCreate()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/ModifyPlaylistFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ModifyPlaylistFragment;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->handleEdit()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/ModifyPlaylistFragment;Landroid/content/Context;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ModifyPlaylistFragment;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->idForplaylist(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ModifyPlaylistFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mSongsToAdd:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Landroid/widget/CompoundButton;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ModifyPlaylistFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mShareSwitch:Landroid/widget/CompoundButton;

    return-object v0
.end method

.method private getShareSwitchState()Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;
    .locals 2

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsSideloaded:Z

    if-eqz v0, :cond_0

    .line 192
    sget-object v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->HIDDEN:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    .line 207
    :goto_0
    return-object v0

    .line 195
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mNetworkConnected:Z

    if-nez v0, :cond_2

    .line 196
    iget-boolean v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsCreate:Z

    if-eqz v0, :cond_1

    .line 197
    sget-object v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->DISABLED:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    goto :goto_0

    .line 199
    :cond_1
    sget-object v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->HIDDEN:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    goto :goto_0

    .line 204
    :cond_2
    iget v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mShareState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iget-boolean v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsCreate:Z

    if-nez v0, :cond_3

    .line 205
    sget-object v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->HIDDEN:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    goto :goto_0

    .line 207
    :cond_3
    sget-object v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->ENABLED:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    goto :goto_0
.end method

.method private handleCreate()V
    .locals 7

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistNameEditor:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 272
    .local v3, "name":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistDescriptionEditor:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 273
    .local v4, "description":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 275
    .local v2, "context":Landroid/content/Context;
    const/4 v6, 0x1

    .line 276
    .local v6, "shareState":I
    invoke-direct {p0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->getShareSwitchState()Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->ENABLED:Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    if-ne v0, v1, :cond_0

    .line 277
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mShareSwitch:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 278
    const/4 v6, 0x2

    .line 283
    :cond_0
    :goto_0
    move v5, v6

    .line 287
    .local v5, "shareStateArg":I
    if-eqz v3, :cond_1

    invoke-static {v3}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v0

    if-lez v0, :cond_1

    .line 289
    new-instance v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;-><init>(Lcom/google/android/music/ui/ModifyPlaylistFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 331
    :cond_1
    return-void

    .line 280
    .end local v5    # "shareStateArg":I
    :cond_2
    const/4 v6, 0x3

    goto :goto_0
.end method

.method private handleEdit()V
    .locals 8

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistNameEditor:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 335
    .local v3, "name":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistDescriptionEditor:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 336
    .local v4, "description":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mShareSwitch:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v6

    .line 337
    .local v6, "isShared":Z
    invoke-virtual {p0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 340
    .local v2, "context":Landroid/content/Context;
    const/4 v7, 0x1

    .line 341
    .local v7, "shareState":I
    iget-boolean v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mNetworkConnected:Z

    if-eqz v0, :cond_2

    if-eqz v6, :cond_0

    iget v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mShareState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    :cond_0
    if-nez v6, :cond_2

    iget v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mShareState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 345
    :cond_1
    if-eqz v6, :cond_3

    .line 346
    const/4 v7, 0x2

    .line 352
    :cond_2
    :goto_0
    move v5, v7

    .line 353
    .local v5, "shareStateArg":I
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 356
    new-instance v0, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;-><init>(Lcom/google/android/music/ui/ModifyPlaylistFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 379
    :goto_1
    return-void

    .line 348
    .end local v5    # "shareStateArg":I
    :cond_3
    const/4 v7, 0x3

    goto :goto_0

    .line 376
    .restart local v5    # "shareStateArg":I
    :cond_4
    const v0, 0x7f0b034e

    const/4 v1, 0x1

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method private idForplaylist(Landroid/content/Context;Ljava/lang/String;)I
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 241
    const/4 v7, -0x1

    .line 242
    .local v7, "id":I
    const/4 v6, 0x0

    .line 244
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-static {p2}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 247
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 251
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 253
    return v7

    .line 251
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private launchSetPlaylistNameTask()V
    .locals 2

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 213
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;-><init>(Lcom/google/android/music/ui/ModifyPlaylistFragment;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 238
    return-void
.end method

.method public static makeCreateArgs(Lcom/google/android/music/medialist/SongList;Z)Landroid/os/Bundle;
    .locals 3
    .param p0, "songlist"    # Lcom/google/android/music/medialist/SongList;
    .param p1, "isSideloaded"    # Z

    .prologue
    const/4 v2, 0x1

    .line 76
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 77
    .local v0, "result":Landroid/os/Bundle;
    if-eqz p0, :cond_0

    .line 78
    const-string v1, "addSongList"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 80
    :cond_0
    const-string v1, "isCreate"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 81
    const-string v1, "isSideloaded"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 82
    const-string v1, "isShared"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    return-object v0
.end method

.method public static makeEditArgs(Lcom/google/android/music/medialist/PlaylistSongList;IZ)Landroid/os/Bundle;
    .locals 3
    .param p0, "songlist"    # Lcom/google/android/music/medialist/PlaylistSongList;
    .param p1, "shareState"    # I
    .param p2, "isSideloaded"    # Z

    .prologue
    .line 99
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 100
    .local v0, "result":Landroid/os/Bundle;
    if-eqz p0, :cond_0

    .line 101
    const-string v1, "editSongList"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 104
    :cond_0
    const-string v1, "isCreate"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 105
    const-string v1, "isSideloaded"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 106
    const-string v1, "isShared"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 108
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 15
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 120
    .local v1, "args":Landroid/os/Bundle;
    const-string v12, "addSongList"

    invoke-virtual {v1, v12}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/google/android/music/medialist/SongList;

    iput-object v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mSongsToAdd:Lcom/google/android/music/medialist/SongList;

    .line 121
    const-string v12, "editSongList"

    invoke-virtual {v1, v12}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/google/android/music/medialist/PlaylistSongList;

    iput-object v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistSongList:Lcom/google/android/music/medialist/PlaylistSongList;

    .line 123
    const-string v12, "isCreate"

    invoke-virtual {v1, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    iput-boolean v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsCreate:Z

    .line 124
    const-string v12, "isSideloaded"

    invoke-virtual {v1, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    iput-boolean v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsSideloaded:Z

    .line 125
    const-string v12, "isShared"

    invoke-virtual {v1, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    iput v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mShareState:I

    .line 127
    new-instance v11, Landroid/content/ContextWrapper;

    invoke-virtual {p0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    invoke-direct {v11, v12}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 128
    .local v11, "wrapper":Landroid/content/ContextWrapper;
    const v12, 0x7f0a00b7

    invoke-virtual {v11, v12}, Landroid/content/ContextWrapper;->setTheme(I)V

    .line 129
    invoke-static {v11}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 130
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v12, 0x7f040029

    const/4 v13, 0x0

    invoke-virtual {v4, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 131
    .local v3, "childView":Landroid/view/View;
    const v12, 0x7f0e00fe

    invoke-virtual {v3, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/EditText;

    iput-object v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistNameEditor:Landroid/widget/EditText;

    .line 132
    const v12, 0x7f0e00ff

    invoke-virtual {v3, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/EditText;

    iput-object v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistDescriptionEditor:Landroid/widget/EditText;

    .line 134
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/music/ui/UIStateManager;->isNetworkConnected()Z

    move-result v12

    iput-boolean v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mNetworkConnected:Z

    .line 135
    invoke-direct {p0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->getShareSwitchState()Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;

    move-result-object v8

    .line 136
    .local v8, "switchState":Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;
    const v12, 0x7f0e0103

    invoke-virtual {v3, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/CompoundButton;

    iput-object v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mShareSwitch:Landroid/widget/CompoundButton;

    .line 137
    sget-object v12, Lcom/google/android/music/ui/ModifyPlaylistFragment$5;->$SwitchMap$com$google$android$music$ui$ModifyPlaylistFragment$ShareSwitchState:[I

    invoke-virtual {v8}, Lcom/google/android/music/ui/ModifyPlaylistFragment$ShareSwitchState;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_0

    .line 152
    new-instance v12, Ljava/lang/IllegalStateException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Unhandled share switch state: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 139
    :pswitch_0
    iget-object v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mShareSwitch:Landroid/widget/CompoundButton;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/CompoundButton;->setVisibility(I)V

    .line 156
    :goto_0
    iget-boolean v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsSideloaded:Z

    if-eqz v12, :cond_3

    .line 157
    const v12, 0x7f0e0100

    invoke-virtual {v3, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 158
    .local v6, "publicLayout":Landroid/view/View;
    const/16 v12, 0x8

    invoke-virtual {v6, v12}, Landroid/view/View;->setVisibility(I)V

    .line 169
    .end local v6    # "publicLayout":Landroid/view/View;
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 170
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 172
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 174
    .local v7, "res":Landroid/content/res/Resources;
    iget-boolean v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsCreate:Z

    if-eqz v12, :cond_5

    const v10, 0x7f0b005d

    .line 177
    .local v10, "titleId":I
    :goto_2
    iget-boolean v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsCreate:Z

    if-eqz v12, :cond_6

    const v5, 0x7f0b0348

    .line 179
    .local v5, "positiveButtonId":I
    :goto_3
    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v12

    invoke-virtual {v2, v12}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 180
    invoke-virtual {v7, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPositiveListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v12, v13}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 181
    const/high16 v12, 0x1040000

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v2, v12, v13}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 182
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 184
    iget-boolean v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsCreate:Z

    if-nez v12, :cond_1

    if-nez p1, :cond_1

    .line 185
    invoke-direct {p0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->launchSetPlaylistNameTask()V

    .line 187
    :cond_1
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v12

    return-object v12

    .line 142
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .end local v2    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v5    # "positiveButtonId":I
    .end local v7    # "res":Landroid/content/res/Resources;
    .end local v10    # "titleId":I
    :pswitch_1
    iget-object v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mShareSwitch:Landroid/widget/CompoundButton;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    goto :goto_0

    .line 145
    :pswitch_2
    iget v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mShareState:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_2

    .line 146
    iget-object v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mShareSwitch:Landroid/widget/CompoundButton;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    .line 148
    :cond_2
    iget-object v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mShareSwitch:Landroid/widget/CompoundButton;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    .line 159
    :cond_3
    iget-boolean v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mNetworkConnected:Z

    if-nez v12, :cond_0

    .line 160
    const v12, 0x7f0e0102

    invoke-virtual {v3, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 162
    .local v9, "textViewPublicDesc":Landroid/widget/TextView;
    iget-boolean v12, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsCreate:Z

    if-eqz v12, :cond_4

    .line 163
    const v12, 0x7f0b0344

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 165
    :cond_4
    const v12, 0x7f0b0345

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 174
    .end local v9    # "textViewPublicDesc":Landroid/widget/TextView;
    .restart local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .restart local v2    # "builder":Landroid/app/AlertDialog$Builder;
    .restart local v7    # "res":Landroid/content/res/Resources;
    :cond_5
    const v10, 0x7f0b005e

    goto :goto_2

    .line 177
    .restart local v10    # "titleId":I
    :cond_6
    const v5, 0x7f0b0349

    goto :goto_3

    .line 137
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 113
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsDismissed:Z

    .line 115
    return-void
.end method
