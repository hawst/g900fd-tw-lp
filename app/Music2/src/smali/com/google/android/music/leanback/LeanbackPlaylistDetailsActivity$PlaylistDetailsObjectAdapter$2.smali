.class Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$2;
.super Ljava/lang/Object;
.source "LeanbackPlaylistDetailsActivity.java"

# interfaces
.implements Lcom/google/android/music/leanback/bitmap/BitmapListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->recalculateSize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

.field final synthetic val$detailsOverviewRow:Landroid/support/v17/leanback/widget/DetailsOverviewRow;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;Landroid/support/v17/leanback/widget/DetailsOverviewRow;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$2;->this$1:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$2;->val$detailsOverviewRow:Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBitmapLoaded(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$2;->val$detailsOverviewRow:Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$2;->this$1:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->access$800(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->setImageBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$2;->this$1:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    const/4 v1, 0x0

    const/4 v2, 0x1

    # invokes: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->notifyItemRangeChanged(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->access$900(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;II)V

    .line 250
    return-void
.end method
