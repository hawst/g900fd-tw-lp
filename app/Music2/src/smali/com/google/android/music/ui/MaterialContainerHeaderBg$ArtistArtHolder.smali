.class Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;
.super Ljava/lang/Object;
.source "MaterialContainerHeaderBg.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/MaterialContainerHeaderBg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ArtistArtHolder"
.end annotation


# instance fields
.field private mImageView:Landroid/widget/ImageView;

.field private final mName:Ljava/lang/String;

.field private mRequestTime:J

.field private mRequestedUrl:Ljava/lang/String;

.field private mShowingDefaultArt:Z

.field final synthetic this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/MaterialContainerHeaderBg;Ljava/lang/String;)V
    .locals 1
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 494
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 487
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mShowingDefaultArt:Z

    .line 495
    iput-object p2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mName:Ljava/lang/String;

    .line 496
    return-void
.end method


# virtual methods
.method clearArtistArt()V
    .locals 3

    .prologue
    .line 528
    iget-boolean v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mShowingDefaultArt:Z

    if-eqz v1, :cond_0

    .line 534
    :goto_0
    return-void

    .line 529
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 530
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_1

    .line 531
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 533
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mImageView:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public getViewName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public onBitmapResult(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mRequestedUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 543
    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$600()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 544
    const-string v0, "MaterialHeaderBg"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ignoring stale result in onBitmapResult for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->getViewName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " requested: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mRequestedUrl:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    :cond_0
    :goto_0
    return-void

    .line 552
    :cond_1
    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$600()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 553
    const-string v0, "MaterialHeaderBg"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBitmapResult for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->getViewName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " time: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mRequestTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    :cond_2
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtClearedOnStop:Z
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$1200(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 559
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mShowingDefaultArt:Z

    .line 560
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method requestBitmap(Ljava/lang/String;)V
    .locals 2
    .param p1, "remoteUrl"    # Ljava/lang/String;

    .prologue
    .line 508
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mRequestedUrl:Ljava/lang/String;

    .line 509
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mRequestTime:J

    .line 511
    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->sBackgroundHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$1100()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder$1;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 525
    return-void
.end method

.method setImageView(Landroid/widget/ImageView;)V
    .locals 0
    .param p1, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 499
    invoke-static {p1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mImageView:Landroid/widget/ImageView;

    .line 501
    return-void
.end method

.method showDefaultArtistArt()V
    .locals 2

    .prologue
    .line 537
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mShowingDefaultArt:Z

    .line 538
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->mImageView:Landroid/widget/ImageView;

    const v1, 0x7f02017a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 539
    return-void
.end method
