.class public Lcom/google/android/music/utils/DbUtils;
.super Ljava/lang/Object;
.source "DbUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/utils/DbUtils$StringCursorHelper;,
        Lcom/google/android/music/utils/DbUtils$CursorHelper;
    }
.end annotation


# direct methods
.method public static final addAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;
    .locals 1
    .param p0, "existingConditions"    # Ljava/lang/StringBuilder;
    .param p1, "andCondition"    # Ljava/lang/String;

    .prologue
    .line 479
    const-string v0, "AND"

    invoke-static {p0, p1, v0}, Lcom/google/android/music/utils/DbUtils;->addCondition(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method private static addCondition(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;
    .locals 2
    .param p0, "existingConditions"    # Ljava/lang/StringBuilder;
    .param p1, "condition"    # Ljava/lang/String;
    .param p2, "operator"    # Ljava/lang/String;

    .prologue
    const/16 v1, 0x20

    .line 491
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 502
    :goto_0
    return-object p0

    .line 494
    :cond_0
    if-nez p0, :cond_1

    .line 495
    new-instance p0, Ljava/lang/StringBuilder;

    .end local p0    # "existingConditions":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    .line 497
    .restart local p0    # "existingConditions":Ljava/lang/StringBuilder;
    :cond_1
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 498
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 500
    :cond_2
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static addOrCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;
    .locals 1
    .param p0, "existingConditions"    # Ljava/lang/StringBuilder;
    .param p1, "orCondition"    # Ljava/lang/String;

    .prologue
    .line 484
    const-string v0, "OR"

    invoke-static {p0, p1, v0}, Lcom/google/android/music/utils/DbUtils;->addCondition(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static addRowToMatrixCursor(Landroid/database/MatrixCursor;Landroid/database/Cursor;)V
    .locals 5
    .param p0, "matrixCursor"    # Landroid/database/MatrixCursor;
    .param p1, "rowToAdd"    # Landroid/database/Cursor;

    .prologue
    .line 543
    invoke-virtual {p0}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v2

    .line 544
    .local v2, "rowBuilder":Landroid/database/MatrixCursor$RowBuilder;
    invoke-virtual {p0}, Landroid/database/MatrixCursor;->getColumnCount()I

    move-result v0

    .line 545
    .local v0, "columnCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 546
    invoke-interface {p1, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 547
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 545
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 553
    :cond_0
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 554
    .local v3, "value":Ljava/lang/String;
    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_1

    .line 557
    .end local v3    # "value":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public static appendIN(Ljava/lang/StringBuffer;Ljava/util/Collection;)Ljava/lang/StringBuffer;
    .locals 5
    .param p0, "buffer"    # Ljava/lang/StringBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/StringBuffer;"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "longs":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v4, "No values for IN operator"

    invoke-direct {v1, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 155
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v1, v1, 0x6

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->ensureCapacity(I)V

    .line 157
    const-string v1, " IN ("

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 158
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 159
    .local v2, "value":J
    invoke-virtual {p0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const/16 v4, 0x2c

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 162
    .end local v2    # "value":J
    :cond_1
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 163
    const-string v1, ") "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 165
    return-object p0
.end method

.method public static appendIN(Ljava/lang/StringBuilder;Ljava/util/Collection;)V
    .locals 5
    .param p0, "buffer"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 178
    .local p1, "longs":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 179
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v4, "No values for IN operator"

    invoke-direct {v1, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 182
    :cond_0
    const-string v1, " IN ("

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 184
    .local v2, "value":J
    invoke-virtual {p0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x2c

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 187
    .end local v2    # "value":J
    :cond_1
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 188
    const-string v1, ") "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    return-void
.end method

.method public static appendIN(Ljava/lang/StringBuilder;[I)V
    .locals 6
    .param p0, "buffer"    # Ljava/lang/StringBuilder;
    .param p1, "ints"    # [I

    .prologue
    .line 223
    if-eqz p1, :cond_0

    array-length v4, p1

    if-nez v4, :cond_1

    .line 224
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "No values for IN operator"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 227
    :cond_1
    const-string v4, " IN ("

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget v3, v0, v1

    .line 229
    .local v3, "value":I
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x2c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 228
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 232
    .end local v3    # "value":I
    :cond_2
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 233
    const-string v4, ") "

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    return-void
.end method

.method public static varargs appendIN(Ljava/lang/StringBuilder;[J)V
    .locals 7
    .param p0, "buffer"    # Ljava/lang/StringBuilder;
    .param p1, "longs"    # [J

    .prologue
    .line 209
    if-eqz p1, :cond_0

    array-length v3, p1

    if-nez v3, :cond_1

    .line 210
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v6, "No values for IN operator"

    invoke-direct {v3, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 213
    :cond_1
    const-string v3, " IN ("

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    move-object v0, p1

    .local v0, "arr$":[J
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-wide v4, v0, v1

    .line 215
    .local v4, "value":J
    invoke-virtual {p0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v6, 0x2c

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 214
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 218
    .end local v4    # "value":J
    :cond_2
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 219
    const-string v3, ") "

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    return-void
.end method

.method public static final escapeForLikeOperator(Ljava/lang/String;C)Ljava/lang/String;
    .locals 5
    .param p0, "valueToEscape"    # Ljava/lang/String;
    .param p1, "escapeChar"    # C

    .prologue
    .line 93
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 94
    .local v2, "inputLength":I
    new-instance v3, Ljava/lang/StringBuffer;

    add-int/lit8 v4, v2, 0xa

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 95
    .local v3, "result":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 96
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 97
    .local v0, "c":C
    sparse-switch v0, :sswitch_data_0

    .line 103
    if-ne v0, p1, :cond_0

    .line 104
    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 107
    :cond_0
    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 95
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 100
    :sswitch_0
    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 109
    .end local v0    # "c":C
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 97
    nop

    :sswitch_data_0
    .sparse-switch
        0x25 -> :sswitch_0
        0x5f -> :sswitch_0
    .end sparse-switch
.end method

.method public static findIndirectlyReferencedItem(JJILandroid/database/Cursor;III)I
    .locals 12
    .param p0, "refId"    # J
    .param p2, "objId"    # J
    .param p4, "lastKnownPosition"    # I
    .param p5, "cursor"    # Landroid/database/Cursor;
    .param p6, "refIdColumnIndex"    # I
    .param p7, "objIdColumnIndex"    # I
    .param p8, "maxRadiusToSearch"    # I

    .prologue
    .line 387
    invoke-interface/range {p5 .. p5}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 388
    .local v6, "newCount":I
    const/4 v9, 0x1

    if-ge v6, v9, :cond_1

    .line 389
    const/4 v5, -0x1

    .line 449
    :cond_0
    :goto_0
    return v5

    .line 392
    :cond_1
    move/from16 v7, p4

    .line 393
    .local v7, "position":I
    const/4 v3, 0x1

    .line 394
    .local v3, "forwardSearch":Z
    const/4 v2, 0x1

    .line 397
    .local v2, "backwardSearch":Z
    move/from16 v0, p4

    if-gt v6, v0, :cond_2

    .line 398
    const/4 v3, 0x0

    .line 399
    add-int/lit8 v7, v6, -0x1

    .line 402
    :cond_2
    const/4 v5, -0x1

    .line 404
    .local v5, "musicPositionMatch":I
    move-object/from16 v0, p5

    invoke-interface {v0, v7}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v9

    if-eqz v9, :cond_3

    move-object/from16 v0, p5

    move/from16 v1, p7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    cmp-long v9, v10, p2

    if-nez v9, :cond_3

    .line 405
    move v5, v7

    .line 406
    invoke-interface/range {p5 .. p6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    cmp-long v9, v10, p0

    if-eqz v9, :cond_0

    .line 412
    :cond_3
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_1
    move/from16 v0, p8

    if-gt v4, v0, :cond_0

    .line 413
    if-eqz v3, :cond_5

    .line 414
    add-int v8, v7, v4

    .line 415
    .local v8, "positionToTry":I
    move-object/from16 v0, p5

    invoke-interface {v0, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 416
    move-object/from16 v0, p5

    move/from16 v1, p7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    cmp-long v9, v10, p2

    if-nez v9, :cond_5

    .line 417
    invoke-interface/range {p5 .. p6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    cmp-long v9, v10, p0

    if-nez v9, :cond_4

    move v5, v8

    .line 418
    goto :goto_0

    .line 419
    :cond_4
    const/4 v9, -0x1

    if-ne v5, v9, :cond_5

    .line 420
    move v5, v8

    .line 431
    .end local v8    # "positionToTry":I
    :cond_5
    if-eqz v2, :cond_8

    .line 432
    sub-int v8, v7, v4

    .line 433
    .restart local v8    # "positionToTry":I
    move-object/from16 v0, p5

    invoke-interface {v0, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 434
    move-object/from16 v0, p5

    move/from16 v1, p7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    cmp-long v9, v10, p2

    if-nez v9, :cond_8

    .line 435
    invoke-interface/range {p5 .. p6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    cmp-long v9, v10, p0

    if-nez v9, :cond_7

    move v5, v8

    .line 436
    goto :goto_0

    .line 424
    :cond_6
    const/4 v3, 0x0

    .line 425
    if-nez v2, :cond_5

    goto :goto_0

    .line 437
    :cond_7
    const/4 v9, -0x1

    if-ne v5, v9, :cond_8

    .line 438
    move v5, v8

    .line 412
    .end local v8    # "positionToTry":I
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 442
    .restart local v8    # "positionToTry":I
    :cond_9
    const/4 v2, 0x0

    .line 443
    if-nez v3, :cond_8

    goto/16 :goto_0
.end method

.method public static findItemInCursor(JILandroid/database/Cursor;II)I
    .locals 8
    .param p0, "id"    # J
    .param p2, "lastKnownPosition"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "idColIndex"    # I
    .param p5, "maxRadiusToSearch"    # I

    .prologue
    const/4 v5, -0x1

    .line 311
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 312
    .local v3, "newCount":I
    const/4 v6, 0x1

    if-ge v3, v6, :cond_1

    move v4, v5

    .line 359
    :cond_0
    :goto_0
    return v4

    .line 316
    :cond_1
    move v4, p2

    .line 317
    .local v4, "position":I
    const/4 v1, 0x1

    .line 318
    .local v1, "forwardSearch":Z
    const/4 v0, 0x1

    .line 321
    .local v0, "backwardSearch":Z
    if-gt v3, p2, :cond_2

    .line 322
    const/4 v1, 0x0

    .line 323
    add-int/lit8 v4, v3, -0x1

    .line 327
    :cond_2
    invoke-interface {p3, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {p3, p4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    cmp-long v6, v6, p0

    if-eqz v6, :cond_0

    .line 332
    :cond_3
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_1
    if-gt v2, p5, :cond_8

    .line 333
    if-eqz v1, :cond_5

    .line 334
    add-int v6, v4, v2

    invoke-interface {p3, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 335
    invoke-interface {p3, p4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    cmp-long v6, v6, p0

    if-nez v6, :cond_5

    .line 336
    add-int/2addr v4, v2

    goto :goto_0

    .line 339
    :cond_4
    const/4 v1, 0x0

    .line 340
    if-nez v0, :cond_5

    move v4, v5

    .line 341
    goto :goto_0

    .line 346
    :cond_5
    if-eqz v0, :cond_7

    .line 347
    sub-int v6, v4, v2

    invoke-interface {p3, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 348
    invoke-interface {p3, p4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    cmp-long v6, v6, p0

    if-nez v6, :cond_7

    .line 349
    sub-int/2addr v4, v2

    goto :goto_0

    .line 352
    :cond_6
    const/4 v0, 0x0

    .line 353
    if-nez v1, :cond_7

    move v4, v5

    .line 354
    goto :goto_0

    .line 332
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_8
    move v4, v5

    .line 359
    goto :goto_0
.end method

.method public static formatProjection([Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 8
    .param p0, "projection"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 276
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    array-length v6, p0

    if-nez v6, :cond_1

    .line 279
    :cond_0
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Projection must not be empty"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 281
    :cond_1
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 282
    .local v1, "buffer":Ljava/lang/StringBuffer;
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_3

    aget-object v5, v0, v2

    .line 283
    .local v5, "s":Ljava/lang/String;
    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 284
    .local v4, "mapped":Ljava/lang/String;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 285
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 289
    :goto_1
    const/16 v6, 0x2c

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 282
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 287
    :cond_2
    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 292
    .end local v4    # "mapped":Ljava/lang/String;
    .end local v5    # "s":Ljava/lang/String;
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 293
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public static getNotInClause(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;
    .locals 3
    .param p0, "column"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 243
    .local p1, "excludeIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 246
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " NOT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    invoke-static {v0, p1}, Lcom/google/android/music/utils/DbUtils;->appendIN(Ljava/lang/StringBuilder;Ljava/util/Collection;)V

    .line 248
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 250
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public static getNullableLong(Landroid/database/Cursor;IJ)J
    .locals 2
    .param p0, "c"    # Landroid/database/Cursor;
    .param p1, "columnIndex"    # I
    .param p2, "defaultValue"    # J

    .prologue
    .line 514
    invoke-interface {p0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 517
    .end local p2    # "defaultValue":J
    :goto_0
    return-wide p2

    .restart local p2    # "defaultValue":J
    :cond_0
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide p2

    goto :goto_0
.end method

.method public static getStringNotInClause(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;
    .locals 3
    .param p0, "column"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 260
    .local p1, "excludeStrings":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 262
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 263
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " NOT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    invoke-static {v0, p1}, Lcom/google/android/music/utils/DbUtils;->stringAppendIN(Ljava/lang/StringBuilder;Ljava/util/Collection;)V

    .line 265
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 267
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public static injectColumnIntoProjection([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 8
    .param p0, "projection"    # [Ljava/lang/String;
    .param p1, "column"    # Ljava/lang/String;

    .prologue
    .line 568
    if-eqz p0, :cond_0

    array-length v7, p0

    if-eqz v7, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 569
    :cond_0
    const/4 p0, 0x0

    .line 588
    .end local p0    # "projection":[Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p0

    .line 572
    .restart local p0    # "projection":[Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    .line 573
    .local v2, "found":Z
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_3

    aget-object v1, v0, v4

    .line 574
    .local v1, "col":Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 575
    const/4 v2, 0x1

    .line 579
    .end local v1    # "col":Ljava/lang/String;
    :cond_3
    if-nez v2, :cond_1

    .line 581
    array-length v7, p0

    add-int/lit8 v7, v7, 0x1

    new-array v6, v7, [Ljava/lang/String;

    .line 582
    .local v6, "newProj":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    array-length v7, p0

    if-ge v3, v7, :cond_5

    .line 583
    aget-object v7, p0, v3

    aput-object v7, v6, v3

    .line 582
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 573
    .end local v3    # "i":I
    .end local v6    # "newProj":[Ljava/lang/String;
    .restart local v1    # "col":Ljava/lang/String;
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 585
    .end local v1    # "col":Ljava/lang/String;
    .restart local v3    # "i":I
    .restart local v6    # "newProj":[Ljava/lang/String;
    :cond_5
    array-length v7, v6

    add-int/lit8 v7, v7, -0x1

    aput-object p1, v6, v7

    .line 586
    move-object p0, v6

    goto :goto_0
.end method

.method public static final quoteStringValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "valueToQuote"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x27

    .line 123
    const/16 v4, 0x27

    .line 124
    .local v4, "quoteChar":C
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 125
    .local v3, "length":I
    new-instance v0, Ljava/lang/StringBuffer;

    add-int/lit8 v5, v3, 0x2

    add-int/lit8 v5, v5, 0xc

    invoke-direct {v0, v5}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 127
    .local v0, "buffer":Ljava/lang/StringBuffer;
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 128
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 129
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 130
    .local v1, "c":C
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 131
    if-ne v1, v6, :cond_0

    .line 132
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 128
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 135
    .end local v1    # "c":C
    :cond_1
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 136
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static stringAppendIN(Ljava/lang/StringBuilder;Ljava/util/Collection;)V
    .locals 4
    .param p0, "buffer"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 193
    .local p1, "strings":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 194
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "No values for IN operator"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 197
    :cond_0
    const-string v2, " IN ("

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 199
    .local v1, "value":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/utils/DbUtils;->quoteStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 202
    .end local v1    # "value":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 203
    const-string v2, ") "

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    return-void
.end method
