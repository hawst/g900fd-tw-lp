.class Lcom/google/android/music/KeepOnView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "KeepOnView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/KeepOnView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/KeepOnView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private available:Z

.field private checked:Z

.field private downloadFraction:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 669
    new-instance v0, Lcom/google/android/music/KeepOnView$SavedState$1;

    invoke-direct {v0}, Lcom/google/android/music/KeepOnView$SavedState$1;-><init>()V

    sput-object v0, Lcom/google/android/music/KeepOnView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 645
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 646
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/KeepOnView$SavedState;->checked:Z

    .line 647
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/KeepOnView$SavedState;->available:Z

    .line 648
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/music/KeepOnView$SavedState;->downloadFraction:F

    .line 649
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/music/KeepOnView$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/music/KeepOnView$1;

    .prologue
    .line 632
    invoke-direct {p0, p1}, Lcom/google/android/music/KeepOnView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;)V
    .locals 0
    .param p1, "superState"    # Landroid/os/Parcelable;

    .prologue
    .line 638
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 639
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/music/KeepOnView$SavedState;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView$SavedState;

    .prologue
    .line 632
    iget v0, p0, Lcom/google/android/music/KeepOnView$SavedState;->downloadFraction:F

    return v0
.end method

.method static synthetic access$1002(Lcom/google/android/music/KeepOnView$SavedState;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView$SavedState;
    .param p1, "x1"    # F

    .prologue
    .line 632
    iput p1, p0, Lcom/google/android/music/KeepOnView$SavedState;->downloadFraction:F

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/music/KeepOnView$SavedState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView$SavedState;

    .prologue
    .line 632
    iget-boolean v0, p0, Lcom/google/android/music/KeepOnView$SavedState;->checked:Z

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/music/KeepOnView$SavedState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView$SavedState;
    .param p1, "x1"    # Z

    .prologue
    .line 632
    iput-boolean p1, p0, Lcom/google/android/music/KeepOnView$SavedState;->checked:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/music/KeepOnView$SavedState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView$SavedState;

    .prologue
    .line 632
    iget-boolean v0, p0, Lcom/google/android/music/KeepOnView$SavedState;->available:Z

    return v0
.end method

.method static synthetic access$902(Lcom/google/android/music/KeepOnView$SavedState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView$SavedState;
    .param p1, "x1"    # Z

    .prologue
    .line 632
    iput-boolean p1, p0, Lcom/google/android/music/KeepOnView$SavedState;->available:Z

    return p1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 661
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SelectionCheckBox.SavedState{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " checked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/KeepOnView$SavedState;->checked:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " available="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/KeepOnView$SavedState;->available:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " downloadFraction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/KeepOnView$SavedState;->downloadFraction:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 653
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 654
    iget-boolean v0, p0, Lcom/google/android/music/KeepOnView$SavedState;->checked:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 655
    iget-boolean v0, p0, Lcom/google/android/music/KeepOnView$SavedState;->available:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 656
    iget v0, p0, Lcom/google/android/music/KeepOnView$SavedState;->downloadFraction:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 657
    return-void
.end method
