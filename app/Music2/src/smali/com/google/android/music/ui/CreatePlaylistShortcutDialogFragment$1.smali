.class Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment$1;
.super Ljava/lang/Object;
.source "CreatePlaylistShortcutDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment$1;->this$0:Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "itemId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment$1;->this$0:Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;

    invoke-virtual {v3, p3}, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;->getAdapterItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;

    .line 35
    .local v1, "item":Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 38
    .local v2, "shortcut":Landroid/content/Intent;
    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 39
    sget-object v3, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    const-string v4, "vnd.android.cursor.dir/vnd.google.music.playlist"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 40
    const/high16 v3, 0x4000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 41
    const-string v3, "playlist"

    iget-wide v4, v1, Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;->id:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 45
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 46
    const-string v3, "android.intent.extra.shortcut.NAME"

    iget-object v4, v1, Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    const-string v3, "android.intent.extra.shortcut.ICON_RESOURCE"

    iget-object v4, p0, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment$1;->this$0:Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;

    invoke-virtual {v4}, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f030001

    invoke-static {v4, v5}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 51
    iget-object v3, p0, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment$1;->this$0:Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;

    invoke-virtual {v3}, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v3, v4, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 52
    iget-object v3, p0, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment$1;->this$0:Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;

    invoke-virtual {v3}, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;->dismiss()V

    .line 53
    return-void
.end method
