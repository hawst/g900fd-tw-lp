.class Lcom/google/android/music/activities/CreateMixActivity$1$3;
.super Ljava/lang/Object;
.source "CreateMixActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/activities/CreateMixActivity$1;->onStart(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/activities/CreateMixActivity$1;

.field final synthetic val$mixName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/activities/CreateMixActivity$1;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/music/activities/CreateMixActivity$1$3;->this$1:Lcom/google/android/music/activities/CreateMixActivity$1;

    iput-object p2, p0, Lcom/google/android/music/activities/CreateMixActivity$1$3;->val$mixName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 98
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/android/music/activities/CreateMixActivity$1$3;->this$1:Lcom/google/android/music/activities/CreateMixActivity$1;

    iget-object v0, v0, Lcom/google/android/music/activities/CreateMixActivity$1;->this$0:Lcom/google/android/music/activities/CreateMixActivity;

    # getter for: Lcom/google/android/music/activities/CreateMixActivity;->mText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/music/activities/CreateMixActivity;->access$100(Lcom/google/android/music/activities/CreateMixActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/activities/CreateMixActivity$1$3;->this$1:Lcom/google/android/music/activities/CreateMixActivity$1;

    iget-object v1, v1, Lcom/google/android/music/activities/CreateMixActivity$1;->this$0:Lcom/google/android/music/activities/CreateMixActivity;

    invoke-virtual {v1}, Lcom/google/android/music/activities/CreateMixActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b01d1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/music/activities/CreateMixActivity$1$3;->val$mixName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    :goto_0
    return-void

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/activities/CreateMixActivity$1$3;->this$1:Lcom/google/android/music/activities/CreateMixActivity$1;

    iget-object v0, v0, Lcom/google/android/music/activities/CreateMixActivity$1;->this$0:Lcom/google/android/music/activities/CreateMixActivity;

    # getter for: Lcom/google/android/music/activities/CreateMixActivity;->mText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/music/activities/CreateMixActivity;->access$100(Lcom/google/android/music/activities/CreateMixActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/activities/CreateMixActivity$1$3;->this$1:Lcom/google/android/music/activities/CreateMixActivity$1;

    iget-object v1, v1, Lcom/google/android/music/activities/CreateMixActivity$1;->this$0:Lcom/google/android/music/activities/CreateMixActivity;

    invoke-virtual {v1}, Lcom/google/android/music/activities/CreateMixActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b01d3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/music/activities/CreateMixActivity$1$3;->val$mixName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
