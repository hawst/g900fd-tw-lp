.class Lcom/google/android/music/dial/DialMediaRouteProvider$3;
.super Ljava/lang/Object;
.source "DialMediaRouteProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/dial/DialMediaRouteProvider;->publishRoutes()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/dial/DialMediaRouteProvider;


# direct methods
.method constructor <init>(Lcom/google/android/music/dial/DialMediaRouteProvider;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/music/dial/DialMediaRouteProvider$3;->this$0:Lcom/google/android/music/dial/DialMediaRouteProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 228
    iget-object v5, p0, Lcom/google/android/music/dial/DialMediaRouteProvider$3;->this$0:Lcom/google/android/music/dial/DialMediaRouteProvider;

    # getter for: Lcom/google/android/music/dial/DialMediaRouteProvider;->mRemoteDeviceManager:Lcom/google/android/music/dial/RemoteDeviceManager;
    invoke-static {v5}, Lcom/google/android/music/dial/DialMediaRouteProvider;->access$300(Lcom/google/android/music/dial/DialMediaRouteProvider;)Lcom/google/android/music/dial/RemoteDeviceManager;

    move-result-object v5

    if-nez v5, :cond_0

    .line 245
    :goto_0
    return-void

    .line 231
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 232
    .local v4, "routes":Ljava/util/List;, "Ljava/util/List<Landroid/support/v7/media/MediaRouteDescriptor;>;"
    iget-object v5, p0, Lcom/google/android/music/dial/DialMediaRouteProvider$3;->this$0:Lcom/google/android/music/dial/DialMediaRouteProvider;

    # getter for: Lcom/google/android/music/dial/DialMediaRouteProvider;->mRemoteDeviceManager:Lcom/google/android/music/dial/RemoteDeviceManager;
    invoke-static {v5}, Lcom/google/android/music/dial/DialMediaRouteProvider;->access$300(Lcom/google/android/music/dial/DialMediaRouteProvider;)Lcom/google/android/music/dial/RemoteDeviceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/dial/RemoteDeviceManager;->getRemoteDeviceInfos()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/dial/RemoteDeviceInfo;

    .line 233
    .local v1, "deviceInfo":Lcom/google/android/music/dial/RemoteDeviceInfo;
    # getter for: Lcom/google/android/music/dial/DialMediaRouteProvider;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/dial/DialMediaRouteProvider;->access$000()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 234
    const-string v5, "DialMediaRouteProvider"

    const-string v6, "publishRoutes device: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    :cond_1
    iget-object v5, p0, Lcom/google/android/music/dial/DialMediaRouteProvider$3;->this$0:Lcom/google/android/music/dial/DialMediaRouteProvider;

    # invokes: Lcom/google/android/music/dial/DialMediaRouteProvider;->getRouteDescriptorForDevice(Lcom/google/android/music/dial/RemoteDeviceInfo;)Landroid/support/v7/media/MediaRouteDescriptor;
    invoke-static {v5, v1}, Lcom/google/android/music/dial/DialMediaRouteProvider;->access$400(Lcom/google/android/music/dial/DialMediaRouteProvider;Lcom/google/android/music/dial/RemoteDeviceInfo;)Landroid/support/v7/media/MediaRouteDescriptor;

    move-result-object v0

    .line 237
    .local v0, "descriptor":Landroid/support/v7/media/MediaRouteDescriptor;
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 240
    .end local v0    # "descriptor":Landroid/support/v7/media/MediaRouteDescriptor;
    .end local v1    # "deviceInfo":Lcom/google/android/music/dial/RemoteDeviceInfo;
    :cond_2
    new-instance v5, Landroid/support/v7/media/MediaRouteProviderDescriptor$Builder;

    invoke-direct {v5}, Landroid/support/v7/media/MediaRouteProviderDescriptor$Builder;-><init>()V

    invoke-virtual {v5, v4}, Landroid/support/v7/media/MediaRouteProviderDescriptor$Builder;->addRoutes(Ljava/util/Collection;)Landroid/support/v7/media/MediaRouteProviderDescriptor$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v7/media/MediaRouteProviderDescriptor$Builder;->build()Landroid/support/v7/media/MediaRouteProviderDescriptor;

    move-result-object v3

    .line 244
    .local v3, "providerDescriptor":Landroid/support/v7/media/MediaRouteProviderDescriptor;
    iget-object v5, p0, Lcom/google/android/music/dial/DialMediaRouteProvider$3;->this$0:Lcom/google/android/music/dial/DialMediaRouteProvider;

    invoke-virtual {v5, v3}, Lcom/google/android/music/dial/DialMediaRouteProvider;->setDescriptor(Landroid/support/v7/media/MediaRouteProviderDescriptor;)V

    goto :goto_0
.end method
