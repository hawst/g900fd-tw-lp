.class public interface abstract Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ServiceHooks"
.end annotation


# virtual methods
.method public abstract cancelAllStreamingTracks()V
.end method

.method public abstract cancelTryNext()V
.end method

.method public abstract markSongPlayed(Lcom/google/android/music/download/ContentIdentifier;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract notifyMediaRouteInvalidated()V
.end method

.method public abstract notifyOpenComplete()V
.end method

.method public abstract notifyOpenStarted()V
.end method

.method public abstract notifyPlayStateChanged()V
.end method

.method public abstract notifyPlaybackComplete()V
.end method

.method public abstract onCloudQueueAdditionalSenderConnected(Ljava/lang/String;)V
.end method

.method public abstract onCloudQueueFirstSenderConnected()V
.end method

.method public abstract onCloudQueueTrackChanged(Ljava/lang/String;)V
.end method

.method public abstract onDownloadProgress(Lcom/google/android/music/download/TrackDownloadProgress;)V
.end method

.method public abstract onVolumeChanged(I)V
.end method

.method public abstract reportTrackEnded()V
.end method

.method public abstract reportTrackPaused()V
.end method

.method public abstract reportTrackPlaying()V
.end method

.method public abstract streamTrack(Lcom/google/android/music/download/ContentIdentifier;JLcom/google/android/music/download/IDownloadProgressListener;ZZ)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/download/stream/DownloadRequestException;,
            Lcom/google/android/music/download/cache/OutOfSpaceException;
        }
    .end annotation
.end method
