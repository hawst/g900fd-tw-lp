.class public Lcom/google/android/music/ui/PlayRadioButton;
.super Lcom/google/android/music/ui/BaseActionButton;
.source "PlayRadioButton.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/ui/PlayRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Z)V

    .line 20
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "isNautilus"    # Z

    .prologue
    .line 23
    if-eqz p3, :cond_0

    const v0, 0x7f0b0233

    move v1, v0

    :goto_0
    if-eqz p3, :cond_1

    const v0, 0x7f02012f

    :goto_1
    invoke-direct {p0, p1, p2, v1, v0}, Lcom/google/android/music/ui/BaseActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 26
    return-void

    .line 23
    :cond_0
    const v0, 0x7f0b0235

    move v1, v0

    goto :goto_0

    :cond_1
    const v0, 0x7f020106

    goto :goto_1
.end method


# virtual methods
.method protected handleAction(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "medialist"    # Lcom/google/android/music/medialist/MediaList;

    .prologue
    .line 30
    check-cast p2, Lcom/google/android/music/medialist/SongList;

    .end local p2    # "medialist":Lcom/google/android/music/medialist/MediaList;
    invoke-static {p1, p2}, Lcom/google/android/music/utils/MusicUtils;->playRadio(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V

    .line 31
    return-void
.end method
