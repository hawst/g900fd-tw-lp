.class Lcom/google/android/music/AudioPreview$2;
.super Ljava/lang/Object;
.source "AudioPreview.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AudioPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/AudioPreview;


# direct methods
.method constructor <init>(Lcom/google/android/music/AudioPreview;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/google/android/music/AudioPreview$2;->this$0:Lcom/google/android/music/AudioPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 2
    .param p1, "focusChange"    # I

    .prologue
    const/4 v1, 0x0

    .line 254
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$2;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$300(Lcom/google/android/music/AudioPreview;)Lcom/google/android/music/AudioPreview$PreviewPlayer;

    move-result-object v0

    if-nez v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$2;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$400(Lcom/google/android/music/AudioPreview;)Landroid/media/AudioManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 280
    :goto_0
    return-void

    .line 260
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 279
    :cond_1
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$2;->this$0:Lcom/google/android/music/AudioPreview;

    # invokes: Lcom/google/android/music/AudioPreview;->updatePlayPause()V
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$700(Lcom/google/android/music/AudioPreview;)V

    goto :goto_0

    .line 262
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$2;->this$0:Lcom/google/android/music/AudioPreview;

    # setter for: Lcom/google/android/music/AudioPreview;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v1}, Lcom/google/android/music/AudioPreview;->access$502(Lcom/google/android/music/AudioPreview;Z)Z

    .line 263
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$2;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$300(Lcom/google/android/music/AudioPreview;)Lcom/google/android/music/AudioPreview$PreviewPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->pause()V

    goto :goto_1

    .line 267
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$2;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$300(Lcom/google/android/music/AudioPreview;)Lcom/google/android/music/AudioPreview$PreviewPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$2;->this$0:Lcom/google/android/music/AudioPreview;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/music/AudioPreview;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v1}, Lcom/google/android/music/AudioPreview;->access$502(Lcom/google/android/music/AudioPreview;Z)Z

    .line 269
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$2;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$300(Lcom/google/android/music/AudioPreview;)Lcom/google/android/music/AudioPreview$PreviewPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->pause()V

    goto :goto_1

    .line 273
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$2;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$500(Lcom/google/android/music/AudioPreview;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$2;->this$0:Lcom/google/android/music/AudioPreview;

    # setter for: Lcom/google/android/music/AudioPreview;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v1}, Lcom/google/android/music/AudioPreview;->access$502(Lcom/google/android/music/AudioPreview;Z)Z

    .line 275
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$2;->this$0:Lcom/google/android/music/AudioPreview;

    # invokes: Lcom/google/android/music/AudioPreview;->start()V
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$600(Lcom/google/android/music/AudioPreview;)V

    goto :goto_1

    .line 260
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
