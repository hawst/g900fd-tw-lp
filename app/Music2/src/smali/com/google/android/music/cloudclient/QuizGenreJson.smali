.class public Lcom/google/android/music/cloudclient/QuizGenreJson;
.super Lcom/google/api/client/json/GenericJson;
.source "QuizGenreJson.java"


# instance fields
.field public mColor:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "color"
    .end annotation
.end field

.field public mImageUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "genreArtRef"
    .end annotation
.end field

.field public mName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "name"
    .end annotation
.end field

.field public mType:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "type"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
