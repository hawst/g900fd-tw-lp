.class Lcom/google/android/music/activities/MusicSettingsActivity$2;
.super Ljava/lang/Object;
.source "MusicSettingsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/activities/MusicSettingsActivity;->showStreamQualityDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/activities/MusicSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/activities/MusicSettingsActivity;)V
    .locals 0

    .prologue
    .line 615
    iput-object p1, p0, Lcom/google/android/music/activities/MusicSettingsActivity$2;->this$0:Lcom/google/android/music/activities/MusicSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 618
    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity$2;->this$0:Lcom/google/android/music/activities/MusicSettingsActivity;

    # getter for: Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamQualityList:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/activities/MusicSettingsActivity;->access$100(Lcom/google/android/music/activities/MusicSettingsActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_1

    .line 624
    :cond_0
    :goto_0
    return-void

    .line 621
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity$2;->this$0:Lcom/google/android/music/activities/MusicSettingsActivity;

    # getter for: Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v0}, Lcom/google/android/music/activities/MusicSettingsActivity;->access$200(Lcom/google/android/music/activities/MusicSettingsActivity;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/music/preferences/MusicPreferences;->setStreamQuality(I)V

    .line 622
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity$2;->this$0:Lcom/google/android/music/activities/MusicSettingsActivity;

    # invokes: Lcom/google/android/music/activities/MusicSettingsActivity;->updateStreamQualitySummary()V
    invoke-static {v0}, Lcom/google/android/music/activities/MusicSettingsActivity;->access$300(Lcom/google/android/music/activities/MusicSettingsActivity;)V

    .line 623
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0
.end method
