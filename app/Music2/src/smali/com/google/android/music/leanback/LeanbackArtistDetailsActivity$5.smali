.class Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$5;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackArtistDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createRelatedArtistsRow(Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Landroid/support/v17/leanback/widget/Presenter;)V
    .locals 0
    .param p2, "x0"    # Landroid/support/v17/leanback/widget/Presenter;

    .prologue
    .line 293
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    invoke-direct {p0, p2}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    return-void
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 296
    new-instance v3, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v3}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 297
    .local v3, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v3, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->populateDocumentFromArtistCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    .line 298
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v2

    .line 299
    .local v2, "artworkUrl":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 300
    const/4 v2, 0x0

    .line 302
    :cond_0
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v1

    .line 303
    .local v1, "artistName":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistMetajamId()Ljava/lang/String;

    move-result-object v0

    .line 305
    .local v0, "artistMetajamId":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    invoke-static {v5, v0}, Lcom/google/android/music/leanback/LeanbackUtils;->getArtistDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 308
    .local v4, "intent":Landroid/content/Intent;
    new-instance v5, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v5}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v5, v1}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/google/android/music/leanback/Item$Builder;->wide(Z)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v5

    return-object v5
.end method
