.class public Lcom/google/android/music/net/NetworkMonitorServiceConnection;
.super Ljava/lang/Object;
.source "NetworkMonitorServiceConnection.java"


# instance fields
.field private final mDownloadabilityChangeListener:Lcom/google/android/music/net/IDownloadabilityChangeListener;

.field private final mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

.field private mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

.field private mRunOnServiceConnected:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mSafeServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

.field private final mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0, v0, v0, v0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;-><init>(Lcom/google/android/music/net/INetworkChangeListener;Lcom/google/android/music/net/IStreamabilityChangeListener;Lcom/google/android/music/net/IDownloadabilityChangeListener;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/net/INetworkChangeListener;)V
    .locals 1
    .param p1, "networkChangeListener"    # Lcom/google/android/music/net/INetworkChangeListener;

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;-><init>(Lcom/google/android/music/net/INetworkChangeListener;Lcom/google/android/music/net/IStreamabilityChangeListener;Lcom/google/android/music/net/IDownloadabilityChangeListener;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/net/INetworkChangeListener;Lcom/google/android/music/net/IStreamabilityChangeListener;Lcom/google/android/music/net/IDownloadabilityChangeListener;)V
    .locals 1
    .param p1, "networkChangeListener"    # Lcom/google/android/music/net/INetworkChangeListener;
    .param p2, "streamabilityChangeListener"    # Lcom/google/android/music/net/IStreamabilityChangeListener;
    .param p3, "downloadabilityChangeListener"    # Lcom/google/android/music/net/IDownloadabilityChangeListener;

    .prologue
    const/4 v0, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    .line 35
    iput-object v0, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mRunOnServiceConnected:Ljava/util/List;

    .line 37
    new-instance v0, Lcom/google/android/music/net/NetworkMonitorServiceConnection$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection$1;-><init>(Lcom/google/android/music/net/NetworkMonitorServiceConnection;)V

    iput-object v0, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mSafeServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    .line 83
    iput-object p1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

    .line 84
    iput-object p2, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

    .line 85
    iput-object p3, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mDownloadabilityChangeListener:Lcom/google/android/music/net/IDownloadabilityChangeListener;

    .line 86
    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/net/IStreamabilityChangeListener;)V
    .locals 1
    .param p1, "streamabilityChangeListener"    # Lcom/google/android/music/net/IStreamabilityChangeListener;

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0, v0, p1, v0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;-><init>(Lcom/google/android/music/net/INetworkChangeListener;Lcom/google/android/music/net/IStreamabilityChangeListener;Lcom/google/android/music/net/IDownloadabilityChangeListener;)V

    .line 72
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/music/net/NetworkMonitorServiceConnection;Lcom/google/android/music/net/INetworkMonitor;)Lcom/google/android/music/net/INetworkMonitor;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitorServiceConnection;
    .param p1, "x1"    # Lcom/google/android/music/net/INetworkMonitor;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/music/net/NetworkMonitorServiceConnection;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mRunOnServiceConnected:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/music/net/NetworkMonitorServiceConnection;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/net/NetworkMonitorServiceConnection;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mRunOnServiceConnected:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method public declared-synchronized addRunOnServiceConnected(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    if-eqz v0, :cond_0

    .line 90
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :goto_0
    monitor-exit p0

    return-void

    .line 93
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mRunOnServiceConnected:Ljava/util/List;

    if-nez v0, :cond_1

    .line 94
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mRunOnServiceConnected:Ljava/util/List;

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mRunOnServiceConnected:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bindToService(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mSafeServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/music/net/NetworkMonitor;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/music/utils/SafeServiceConnection;->bindService(Landroid/content/Context;Landroid/content/Intent;I)Z

    .line 102
    return-void
.end method

.method protected finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    if-eqz v0, :cond_0

    .line 173
    const-string v0, "NetworkMonitor"

    const-string v1, "NetworkMonitorServiceConnection not unbinded cleanly"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 175
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 176
    return-void
.end method

.method public getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    return-object v0
.end method

.method public hasConnectivity()Z
    .locals 5

    .prologue
    .line 155
    const/4 v1, 0x0

    .line 157
    .local v1, "hasConnectivity":Z
    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    .line 158
    .local v2, "networkMonitor":Lcom/google/android/music/net/INetworkMonitor;
    if-nez v2, :cond_0

    .line 159
    const-string v3, "NetworkMonitor"

    const-string v4, "Network service is not bound"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :goto_0
    return v1

    .line 162
    :cond_0
    :try_start_0
    invoke-interface {v2}, Lcom/google/android/music/net/INetworkMonitor;->isConnected()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "NetworkMonitor"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public declared-synchronized registerListeners()V
    .locals 3

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    if-nez v1, :cond_1

    .line 112
    const-string v1, "NetworkMonitor"

    const-string v2, "registerListeners: networkMonitor was never connected"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 116
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

    if-eqz v1, :cond_2

    .line 117
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

    invoke-interface {v1, v2}, Lcom/google/android/music/net/INetworkMonitor;->registerNetworkChangeListener(Lcom/google/android/music/net/INetworkChangeListener;)V

    .line 119
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

    if-eqz v1, :cond_3

    .line 120
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

    invoke-interface {v1, v2}, Lcom/google/android/music/net/INetworkMonitor;->registerStreamabilityChangeListener(Lcom/google/android/music/net/IStreamabilityChangeListener;)V

    .line 122
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mDownloadabilityChangeListener:Lcom/google/android/music/net/IDownloadabilityChangeListener;

    if-eqz v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mDownloadabilityChangeListener:Lcom/google/android/music/net/IDownloadabilityChangeListener;

    invoke-interface {v1, v2}, Lcom/google/android/music/net/INetworkMonitor;->registerDownloadabilityChangeListener(Lcom/google/android/music/net/IDownloadabilityChangeListener;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v1, "NetworkMonitor"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 111
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public unbindFromService(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->unregisterListeners()V

    .line 106
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mSafeServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    invoke-virtual {v0, p1}, Lcom/google/android/music/utils/SafeServiceConnection;->unbindService(Landroid/content/Context;)V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    .line 108
    return-void
.end method

.method public declared-synchronized unregisterListeners()V
    .locals 3

    .prologue
    .line 131
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    if-nez v1, :cond_1

    .line 132
    const-string v1, "NetworkMonitor"

    const-string v2, "unregisterListeners: networkMonitor was never connected"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 136
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

    if-eqz v1, :cond_2

    .line 137
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

    invoke-interface {v1, v2}, Lcom/google/android/music/net/INetworkMonitor;->unregisterNetworkChangeListener(Lcom/google/android/music/net/INetworkChangeListener;)V

    .line 139
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

    if-eqz v1, :cond_3

    .line 140
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

    invoke-interface {v1, v2}, Lcom/google/android/music/net/INetworkMonitor;->unregisterStreamabilityChangeListener(Lcom/google/android/music/net/IStreamabilityChangeListener;)V

    .line 142
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mDownloadabilityChangeListener:Lcom/google/android/music/net/IDownloadabilityChangeListener;

    if-eqz v1, :cond_0

    .line 143
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

    invoke-interface {v1, v2}, Lcom/google/android/music/net/INetworkMonitor;->unregisterStreamabilityChangeListener(Lcom/google/android/music/net/IStreamabilityChangeListener;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 145
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v1, "NetworkMonitor"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 131
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public waitForServiceConnection(J)Z
    .locals 3
    .param p1, "timeout"    # J

    .prologue
    .line 184
    monitor-enter p0

    .line 185
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 187
    :try_start_1
    invoke-virtual {p0, p1, p2}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 192
    :cond_0
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->mNetworkMonitor:Lcom/google/android/music/net/INetworkMonitor;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    monitor-exit p0

    return v1

    .line 188
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "NetworkMonitor"

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 193
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 192
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
