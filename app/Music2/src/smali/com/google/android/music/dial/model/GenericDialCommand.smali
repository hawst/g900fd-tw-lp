.class public abstract Lcom/google/android/music/dial/model/GenericDialCommand;
.super Lcom/google/api/client/json/GenericJson;
.source "GenericDialCommand.java"


# instance fields
.field public mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "header"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 18
    new-instance v0, Lcom/google/android/music/dial/model/CommandHeaderJson;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/CommandHeaderJson;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/dial/model/GenericDialCommand;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    .line 19
    return-void
.end method


# virtual methods
.method protected abstract getBodyAsJsonString()Ljava/lang/String;
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 35
    const-string v0, "{\"header\":%s,\"body\":%s}"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/music/dial/model/GenericDialCommand;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/music/dial/model/GenericDialCommand;->getBodyAsJsonString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public validate()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/dial/MalformedDialCommandException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/music/dial/model/GenericDialCommand;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/dial/model/GenericDialCommand;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    iget-object v0, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mNamespace:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 45
    :cond_0
    new-instance v0, Lcom/google/android/music/dial/MalformedDialCommandException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Command is missing required fields.  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/dial/MalformedDialCommandException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_1
    return-void
.end method
