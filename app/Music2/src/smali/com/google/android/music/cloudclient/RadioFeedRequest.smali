.class public Lcom/google/android/music/cloudclient/RadioFeedRequest;
.super Lcom/google/api/client/json/GenericJson;
.source "RadioFeedRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/RadioFeedRequest$MixRequest;,
        Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;
    }
.end annotation


# static fields
.field public static final EXPLICIT_CONTENT_FILTER_VALUE:I = 0x2

.field public static final NO_CONTENT_FILTER_VALUE:I = 0x1


# instance fields
.field public mContentFilter:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "contentFilter"
    .end annotation
.end field

.field public mMixes:Lcom/google/android/music/cloudclient/RadioFeedRequest$MixRequest;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "mixes"
    .end annotation
.end field

.field public mStations:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "stations"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/cloudclient/RadioFeedRequest;->mStations:Ljava/util/List;

    .line 62
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/music/cloudclient/RadioFeedRequest;->mContentFilter:I

    return-void
.end method

.method private static checkContentFilter(I)I
    .locals 3
    .param p0, "contentFilter"    # I

    .prologue
    .line 133
    invoke-static {p0}, Lcom/google/android/music/cloudclient/RadioFeedRequest;->validateContentFilter(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 134
    const-string v0, "RadioFeedRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid value for content filter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    const/4 p0, 0x1

    .line 137
    :cond_0
    return p0
.end method

.method public static serialize(III)[B
    .locals 4
    .param p0, "maxMixes"    # I
    .param p1, "maxEntries"    # I
    .param p2, "contentFilter"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    invoke-static {p2}, Lcom/google/android/music/cloudclient/RadioFeedRequest;->validateContentFilter(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 121
    const-string v1, "RadioFeedRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid value for content filter: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    const/4 p2, 0x1

    .line 124
    :cond_0
    new-instance v0, Lcom/google/android/music/cloudclient/RadioFeedRequest;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/RadioFeedRequest;-><init>()V

    .line 125
    .local v0, "request":Lcom/google/android/music/cloudclient/RadioFeedRequest;
    iput p2, v0, Lcom/google/android/music/cloudclient/RadioFeedRequest;->mContentFilter:I

    .line 126
    new-instance v1, Lcom/google/android/music/cloudclient/RadioFeedRequest$MixRequest;

    invoke-direct {v1}, Lcom/google/android/music/cloudclient/RadioFeedRequest$MixRequest;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/cloudclient/RadioFeedRequest;->mMixes:Lcom/google/android/music/cloudclient/RadioFeedRequest$MixRequest;

    .line 127
    iget-object v1, v0, Lcom/google/android/music/cloudclient/RadioFeedRequest;->mMixes:Lcom/google/android/music/cloudclient/RadioFeedRequest$MixRequest;

    iput p0, v1, Lcom/google/android/music/cloudclient/RadioFeedRequest$MixRequest;->mNumSeeds:I

    .line 128
    iget-object v1, v0, Lcom/google/android/music/cloudclient/RadioFeedRequest;->mMixes:Lcom/google/android/music/cloudclient/RadioFeedRequest$MixRequest;

    iput p1, v1, Lcom/google/android/music/cloudclient/RadioFeedRequest$MixRequest;->mNumEntries:I

    .line 129
    invoke-static {v0}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v1

    return-object v1
.end method

.method public static serialize(Ljava/lang/String;ILjava/util/List;I)[B
    .locals 3
    .param p0, "radioRemoteId"    # Ljava/lang/String;
    .param p1, "maxEntries"    # I
    .param p3, "contentFilter"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/MixTrackId;",
            ">;I)[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    .local p2, "recentlyPlayed":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    invoke-static {p3}, Lcom/google/android/music/cloudclient/RadioFeedRequest;->checkContentFilter(I)I

    move-result p3

    .line 70
    new-instance v1, Lcom/google/android/music/cloudclient/RadioFeedRequest;

    invoke-direct {v1}, Lcom/google/android/music/cloudclient/RadioFeedRequest;-><init>()V

    .line 71
    .local v1, "request":Lcom/google/android/music/cloudclient/RadioFeedRequest;
    iput p3, v1, Lcom/google/android/music/cloudclient/RadioFeedRequest;->mContentFilter:I

    .line 72
    new-instance v0, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;-><init>()V

    .line 73
    .local v0, "radioStationRequest":Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;
    iput-object p0, v0, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;->mRadioId:Ljava/lang/String;

    .line 74
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, v0, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;->mRecentlyPlayed:Ljava/util/List;

    .line 75
    iput p1, v0, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;->mNumEntries:I

    .line 76
    invoke-virtual {v0, p2}, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;->setRecentlyPlayedFromMixList(Ljava/util/List;)V

    .line 78
    iget-object v2, v1, Lcom/google/android/music/cloudclient/RadioFeedRequest;->mStations:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    invoke-static {v1}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v2

    return-object v2
.end method

.method public static serializeForArtistShuffle(Ljava/lang/String;II)[B
    .locals 3
    .param p0, "remoteSeedId"    # Ljava/lang/String;
    .param p1, "maxEntries"    # I
    .param p2, "contentFilter"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-static {p2}, Lcom/google/android/music/cloudclient/RadioFeedRequest;->checkContentFilter(I)I

    move-result p2

    .line 107
    new-instance v1, Lcom/google/android/music/cloudclient/RadioFeedRequest;

    invoke-direct {v1}, Lcom/google/android/music/cloudclient/RadioFeedRequest;-><init>()V

    .line 108
    .local v1, "request":Lcom/google/android/music/cloudclient/RadioFeedRequest;
    iput p2, v1, Lcom/google/android/music/cloudclient/RadioFeedRequest;->mContentFilter:I

    .line 109
    new-instance v0, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;-><init>()V

    .line 110
    .local v0, "radioStationRequest":Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;
    const/4 v2, 0x7

    invoke-static {p0, v2}, Lcom/google/android/music/sync/google/model/RadioSeed;->createRadioSeed(Ljava/lang/String;I)Lcom/google/android/music/sync/google/model/RadioSeed;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;->mSeed:Lcom/google/android/music/sync/google/model/RadioSeed;

    .line 112
    iput p1, v0, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;->mNumEntries:I

    .line 113
    iget-object v2, v1, Lcom/google/android/music/cloudclient/RadioFeedRequest;->mStations:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    invoke-static {v1}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v2

    return-object v2
.end method

.method public static serializeForLuckyRadio(ILjava/util/List;I)[B
    .locals 4
    .param p0, "maxEntries"    # I
    .param p2, "contentFilter"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/MixTrackId;",
            ">;I)[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "recentlyPlayed":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    invoke-static {p2}, Lcom/google/android/music/cloudclient/RadioFeedRequest;->checkContentFilter(I)I

    move-result p2

    .line 89
    new-instance v1, Lcom/google/android/music/cloudclient/RadioFeedRequest;

    invoke-direct {v1}, Lcom/google/android/music/cloudclient/RadioFeedRequest;-><init>()V

    .line 90
    .local v1, "request":Lcom/google/android/music/cloudclient/RadioFeedRequest;
    iput p2, v1, Lcom/google/android/music/cloudclient/RadioFeedRequest;->mContentFilter:I

    .line 91
    new-instance v0, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;-><init>()V

    .line 92
    .local v0, "radioStationRequest":Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;
    const/4 v2, 0x0

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/music/sync/google/model/RadioSeed;->createRadioSeed(Ljava/lang/String;I)Lcom/google/android/music/sync/google/model/RadioSeed;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;->mSeed:Lcom/google/android/music/sync/google/model/RadioSeed;

    .line 94
    iput p0, v0, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;->mNumEntries:I

    .line 95
    invoke-virtual {v0, p1}, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;->setRecentlyPlayedFromMixList(Ljava/util/List;)V

    .line 96
    iget-object v2, v1, Lcom/google/android/music/cloudclient/RadioFeedRequest;->mStations:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    invoke-static {v1}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v2

    return-object v2
.end method

.method private static validateContentFilter(I)Z
    .locals 2
    .param p0, "contentFilter"    # I

    .prologue
    const/4 v0, 0x1

    .line 142
    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
