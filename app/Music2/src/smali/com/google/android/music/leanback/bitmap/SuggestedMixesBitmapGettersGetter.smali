.class public Lcom/google/android/music/leanback/bitmap/SuggestedMixesBitmapGettersGetter;
.super Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;
.source "SuggestedMixesBitmapGettersGetter.java"


# instance fields
.field private final mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLandroid/os/Handler;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loaderManager"    # Landroid/support/v4/app/LoaderManager;
    .param p3, "loaderId"    # I
    .param p4, "seed"    # J
    .param p6, "handler"    # Landroid/os/Handler;

    .prologue
    .line 27
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    .line 28
    iput-object p6, p0, Lcom/google/android/music/leanback/bitmap/SuggestedMixesBitmapGettersGetter;->mHandler:Landroid/os/Handler;

    .line 29
    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/SuggestedMixesBitmapGettersGetter;->startLoading()V

    .line 30
    return-void
.end method


# virtual methods
.method protected addBitmapGetters(Landroid/database/Cursor;Ljava/util/ArrayList;)V
    .locals 18
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/leanback/bitmap/BitmapGetter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p2, "bitmapGettersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/leanback/bitmap/BitmapGetter;>;"
    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 46
    .local v4, "id":J
    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 48
    .local v6, "name":Ljava/lang/String;
    new-instance v3, Lcom/google/android/music/medialist/PlaylistSongList;

    const/16 v7, 0x32

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v3 .. v13}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 53
    .local v3, "songList":Lcom/google/android/music/medialist/SongList;
    new-instance v8, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/leanback/bitmap/SuggestedMixesBitmapGettersGetter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/music/leanback/bitmap/SuggestedMixesBitmapGettersGetter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/music/leanback/bitmap/SuggestedMixesBitmapGettersGetter;->mLoaderId:I

    add-int/lit8 v7, v7, 0x1

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v11

    add-int/2addr v11, v7

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/music/leanback/bitmap/SuggestedMixesBitmapGettersGetter;->mSeed:J

    const-wide/16 v16, 0x1

    add-long v12, v12, v16

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v7

    int-to-long v0, v7

    move-wide/from16 v16, v0

    add-long v12, v12, v16

    move-object v14, v3

    invoke-direct/range {v8 .. v14}, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLcom/google/android/music/medialist/SongList;)V

    .line 56
    .local v8, "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/music/leanback/bitmap/SuggestedMixesBitmapGettersGetter;->mHandler:Landroid/os/Handler;

    new-instance v9, Lcom/google/android/music/leanback/bitmap/SuggestedMixesBitmapGettersGetter$1;

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v8}, Lcom/google/android/music/leanback/bitmap/SuggestedMixesBitmapGettersGetter$1;-><init>(Lcom/google/android/music/leanback/bitmap/SuggestedMixesBitmapGettersGetter;Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;)V

    invoke-virtual {v7, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 62
    invoke-virtual {v8}, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;->getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    move-result-object v2

    .line 63
    .local v2, "bitmapGetters":[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    array-length v7, v2

    if-ge v15, v7, :cond_1

    .line 64
    aget-object v7, v2, v15

    if-eqz v7, :cond_0

    .line 65
    aget-object v7, v2, v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_0
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 69
    :cond_1
    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 70
    new-instance v7, Lcom/google/android/music/leanback/bitmap/DecodedExternalAlbumArtUriBitmapGetter;

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lcom/google/android/music/leanback/bitmap/DecodedExternalAlbumArtUriBitmapGetter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    :cond_2
    return-void
.end method

.method public bridge synthetic getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .locals 1

    .prologue
    .line 21
    invoke-super {p0}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    move-result-object v0

    return-object v0
.end method

.method protected getMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/music/medialist/RecommendedRadioList;

    invoke-direct {v0}, Lcom/google/android/music/medialist/RecommendedRadioList;-><init>()V

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/music/ui/RecommendedRadioFragment;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 21
    invoke-super {p0, p1, p2}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    return-void
.end method

.method public bridge synthetic startLoading()V
    .locals 0

    .prologue
    .line 21
    invoke-super {p0}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->startLoading()V

    return-void
.end method
