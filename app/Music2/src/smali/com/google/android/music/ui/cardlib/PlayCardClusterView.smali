.class public Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
.super Landroid/view/ViewGroup;
.source "PlayCardClusterView.java"


# instance fields
.field private mContent:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

.field private mHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

.field private final mHeaderContentOverlap:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f00f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mHeaderContentOverlap:I

    .line 40
    return-void
.end method


# virtual methods
.method public clearThumbnails()V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mContent:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->clearThumbnails()V

    .line 117
    return-void
.end method

.method public createContent()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mContent:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->createContent()V

    .line 100
    return-void
.end method

.method public getCardChild(I)Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    .locals 1
    .param p1, "cardIndex"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mContent:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    return-object v0
.end method

.method public hasCards()Z
    .locals 2

    .prologue
    .line 90
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mContent:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getChildCount()I

    move-result v0

    .line 91
    .local v0, "childCount":I
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mContent:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getMetadata()Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hideHeader()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setVisibility(I)V

    .line 104
    return-void
.end method

.method public inflateContent(Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;)V
    .locals 1
    .param p1, "cardHeap"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mContent:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->inflateContent(Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;)V

    .line 96
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 46
    const v0, 0x7f0e01ec

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mContent:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

    .line 47
    const v0, 0x7f0e01ed

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    .line 48
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v7, 0x0

    .line 139
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->getWidth()I

    move-result v3

    .line 140
    .local v3, "width":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->getPaddingTop()I

    move-result v2

    .line 141
    .local v2, "paddingTop":I
    move v0, v2

    .line 143
    .local v0, "contentY":I
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 144
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v1

    .line 145
    .local v1, "headerHeight":I
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    add-int v5, v2, v1

    invoke-virtual {v4, v7, v2, v3, v5}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->layout(IIII)V

    .line 148
    iget v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mHeaderContentOverlap:I

    sub-int v4, v1, v4

    add-int/2addr v0, v4

    .line 150
    .end local v1    # "headerHeight":I
    :cond_0
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mContent:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v4, v7, v0, v3, v5}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->layout(IIII)V

    .line 151
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 121
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 123
    .local v0, "availableWidth":I
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mContent:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->measure(II)V

    .line 124
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mContent:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->getPaddingBottom()I

    move-result v4

    add-int v2, v3, v4

    .line 126
    .local v2, "height":I
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_0

    .line 127
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->measure(II)V

    .line 128
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v1

    .line 131
    .local v1, "headerHeight":I
    iget v3, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mHeaderContentOverlap:I

    sub-int v3, v1, v3

    add-int/2addr v2, v3

    .line 134
    .end local v1    # "headerHeight":I
    :cond_0
    invoke-virtual {p0, v0, v2}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setMeasuredDimension(II)V

    .line 135
    return-void
.end method

.method public removeAllCards()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mContent:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->removeAllViews()V

    .line 83
    return-void
.end method

.method public setMetadata(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V
    .locals 2
    .param p1, "metadata"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
    .param p3, "cardsContextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;",
            ")V"
        }
    .end annotation

    .prologue
    .line 56
    .local p2, "clusterDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mContent:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->setMetadata(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;Landroid/view/View$OnClickListener;)V

    .line 57
    return-void
.end method

.method public setMetadata(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
    .param p3, "cardsContextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .param p4, "clickListener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 70
    .local p2, "clusterDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mContent:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->setMetadata(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;Landroid/view/View$OnClickListener;)V

    .line 71
    return-void
.end method

.method public setMoreClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setMoreButtonClickHandler(Landroid/view/View$OnClickListener;)V

    .line 113
    return-void
.end method

.method public showHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "titleMain"    # Ljava/lang/String;
    .param p2, "titleSecondary"    # Ljava/lang/String;
    .param p3, "more"    # Ljava/lang/String;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setVisibility(I)V

    .line 109
    return-void
.end method
