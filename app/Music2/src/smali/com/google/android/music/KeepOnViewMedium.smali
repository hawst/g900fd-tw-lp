.class public Lcom/google/android/music/KeepOnViewMedium;
.super Lcom/google/android/music/KeepOnView;
.source "KeepOnViewMedium.java"


# static fields
.field private static sResourcesInitialized:Z

.field private static sSharedResourcesRef:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Lcom/google/android/music/KeepOnView$SharedResources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/music/KeepOnViewMedium;->sResourcesInitialized:Z

    .line 21
    new-instance v0, Ljava/lang/ref/SoftReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/google/android/music/KeepOnViewMedium;->sSharedResourcesRef:Ljava/lang/ref/SoftReference;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/music/KeepOnViewMedium;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/KeepOnViewMedium;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/KeepOnView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method private computeSharedResources()Lcom/google/android/music/KeepOnView$SharedResources;
    .locals 6

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnViewMedium;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 50
    .local v4, "resources":Landroid/content/res/Resources;
    const v5, 0x7f020123

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 52
    .local v2, "normalOverlay":Landroid/graphics/Bitmap;
    const v5, 0x7f020125

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 54
    .local v3, "progressOverlay":Landroid/graphics/Bitmap;
    const v5, 0x7f020121

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 56
    .local v0, "completedOverlay":Landroid/graphics/Bitmap;
    const v5, 0x7f020064

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 58
    .local v1, "greyCircleBackground":Landroid/graphics/Bitmap;
    new-instance v5, Lcom/google/android/music/KeepOnView$SharedResources;

    invoke-direct {v5, v2, v3, v0, v1}, Lcom/google/android/music/KeepOnView$SharedResources;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    return-object v5
.end method

.method private setKeepOnResources(Lcom/google/android/music/KeepOnView$SharedResources;)V
    .locals 1
    .param p1, "sharedResources"    # Lcom/google/android/music/KeepOnView$SharedResources;

    .prologue
    .line 63
    new-instance v0, Ljava/lang/ref/SoftReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/google/android/music/KeepOnViewMedium;->sSharedResourcesRef:Ljava/lang/ref/SoftReference;

    .line 64
    return-void
.end method


# virtual methods
.method protected getKeepOnResources()Lcom/google/android/music/KeepOnView$SharedResources;
    .locals 2

    .prologue
    .line 38
    sget-boolean v1, Lcom/google/android/music/KeepOnViewMedium;->sResourcesInitialized:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/music/KeepOnViewMedium;->sSharedResourcesRef:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 39
    sget-object v1, Lcom/google/android/music/KeepOnViewMedium;->sSharedResourcesRef:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/KeepOnView$SharedResources;

    .line 44
    :goto_0
    return-object v1

    .line 41
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/KeepOnViewMedium;->computeSharedResources()Lcom/google/android/music/KeepOnView$SharedResources;

    move-result-object v0

    .line 42
    .local v0, "resources":Lcom/google/android/music/KeepOnView$SharedResources;
    invoke-direct {p0, v0}, Lcom/google/android/music/KeepOnViewMedium;->setKeepOnResources(Lcom/google/android/music/KeepOnView$SharedResources;)V

    .line 43
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/music/KeepOnViewMedium;->sResourcesInitialized:Z

    move-object v1, v0

    .line 44
    goto :goto_0
.end method

.method protected getProgressArcPadding(Landroid/content/Context;)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f00c1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method
