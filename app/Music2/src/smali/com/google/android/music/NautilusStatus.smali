.class public final enum Lcom/google/android/music/NautilusStatus;
.super Ljava/lang/Enum;
.source "NautilusStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/NautilusStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/NautilusStatus;

.field public static final enum GOT_NAUTILUS:Lcom/google/android/music/NautilusStatus;

.field public static final enum PURCHASE_AVAILABLE_NO_TRIAL:Lcom/google/android/music/NautilusStatus;

.field public static final enum TRIAL_AVAILABLE:Lcom/google/android/music/NautilusStatus;

.field public static final enum UNAVAILABLE:Lcom/google/android/music/NautilusStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15
    new-instance v0, Lcom/google/android/music/NautilusStatus;

    const-string v1, "UNAVAILABLE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/NautilusStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/NautilusStatus;->UNAVAILABLE:Lcom/google/android/music/NautilusStatus;

    .line 20
    new-instance v0, Lcom/google/android/music/NautilusStatus;

    const-string v1, "TRIAL_AVAILABLE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/NautilusStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/NautilusStatus;->TRIAL_AVAILABLE:Lcom/google/android/music/NautilusStatus;

    .line 26
    new-instance v0, Lcom/google/android/music/NautilusStatus;

    const-string v1, "PURCHASE_AVAILABLE_NO_TRIAL"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/NautilusStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/NautilusStatus;->PURCHASE_AVAILABLE_NO_TRIAL:Lcom/google/android/music/NautilusStatus;

    .line 32
    new-instance v0, Lcom/google/android/music/NautilusStatus;

    const-string v1, "GOT_NAUTILUS"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/NautilusStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/NautilusStatus;->GOT_NAUTILUS:Lcom/google/android/music/NautilusStatus;

    .line 10
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/music/NautilusStatus;

    sget-object v1, Lcom/google/android/music/NautilusStatus;->UNAVAILABLE:Lcom/google/android/music/NautilusStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/NautilusStatus;->TRIAL_AVAILABLE:Lcom/google/android/music/NautilusStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/NautilusStatus;->PURCHASE_AVAILABLE_NO_TRIAL:Lcom/google/android/music/NautilusStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/NautilusStatus;->GOT_NAUTILUS:Lcom/google/android/music/NautilusStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/NautilusStatus;->$VALUES:[Lcom/google/android/music/NautilusStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/NautilusStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/google/android/music/NautilusStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/NautilusStatus;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/NautilusStatus;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/google/android/music/NautilusStatus;->$VALUES:[Lcom/google/android/music/NautilusStatus;

    invoke-virtual {v0}, [Lcom/google/android/music/NautilusStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/NautilusStatus;

    return-object v0
.end method


# virtual methods
.method public isFreeTrialAvailable()Z
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/music/NautilusStatus;->TRIAL_AVAILABLE:Lcom/google/android/music/NautilusStatus;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNautilusEnabled()Z
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/music/NautilusStatus;->GOT_NAUTILUS:Lcom/google/android/music/NautilusStatus;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
