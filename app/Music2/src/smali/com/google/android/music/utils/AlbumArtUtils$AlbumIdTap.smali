.class Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;
.super Ljava/lang/Object;
.source "AlbumArtUtils.java"

# interfaces
.implements Lcom/google/android/music/download/artwork/AlbumIdSink;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/AlbumArtUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AlbumIdTap"
.end annotation


# instance fields
.field private mIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mTarget:Lcom/google/android/music/download/artwork/AlbumIdSink;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/artwork/AlbumIdSink;)V
    .locals 0
    .param p1, "target"    # Lcom/google/android/music/download/artwork/AlbumIdSink;

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    iput-object p1, p0, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;->mTarget:Lcom/google/android/music/download/artwork/AlbumIdSink;

    .line 170
    return-void
.end method


# virtual methods
.method public extractIds()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;->mIds:Ljava/util/Set;

    .line 190
    .local v0, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;->mIds:Ljava/util/Set;

    .line 191
    return-object v0
.end method

.method public report(Ljava/lang/Long;)V
    .locals 1
    .param p1, "albumId"    # Ljava/lang/Long;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;->mTarget:Lcom/google/android/music/download/artwork/AlbumIdSink;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;->mTarget:Lcom/google/android/music/download/artwork/AlbumIdSink;

    invoke-interface {v0, p1}, Lcom/google/android/music/download/artwork/AlbumIdSink;->report(Ljava/lang/Long;)V

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;->mIds:Ljava/util/Set;

    if-nez v0, :cond_1

    .line 178
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;->mIds:Ljava/util/Set;

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;->mIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 181
    return-void
.end method
