.class Lcom/google/android/music/ui/NowPlayingArtViewPager$NowPlayingArtTransformer;
.super Ljava/lang/Object;
.source "NowPlayingArtViewPager.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$PageTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingArtViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NowPlayingArtTransformer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingArtViewPager;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/NowPlayingArtViewPager;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingArtViewPager$NowPlayingArtTransformer;->this$0:Lcom/google/android/music/ui/NowPlayingArtViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/NowPlayingArtViewPager;Lcom/google/android/music/ui/NowPlayingArtViewPager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/NowPlayingArtViewPager;
    .param p2, "x1"    # Lcom/google/android/music/ui/NowPlayingArtViewPager$1;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/NowPlayingArtViewPager$NowPlayingArtTransformer;-><init>(Lcom/google/android/music/ui/NowPlayingArtViewPager;)V

    return-void
.end method


# virtual methods
.method public transformPage(Landroid/view/View;F)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # F

    .prologue
    const/4 v3, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 52
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 54
    .local v0, "pageWidth":I
    const/high16 v2, -0x40800000    # -1.0f

    cmpg-float v2, p2, v2

    if-gez v2, :cond_0

    .line 56
    invoke-virtual {p1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 82
    :goto_0
    return-void

    .line 58
    :cond_0
    cmpg-float v2, p2, v3

    if-gtz v2, :cond_1

    .line 60
    invoke-virtual {p1, v5}, Landroid/view/View;->setAlpha(F)V

    .line 61
    invoke-virtual {p1, v3}, Landroid/view/View;->setTranslationX(F)V

    .line 62
    invoke-virtual {p1, v5}, Landroid/view/View;->setScaleX(F)V

    .line 63
    invoke-virtual {p1, v5}, Landroid/view/View;->setScaleY(F)V

    goto :goto_0

    .line 65
    :cond_1
    cmpg-float v2, p2, v5

    if-gtz v2, :cond_2

    .line 67
    sub-float v2, v5, p2

    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 70
    int-to-float v2, v0

    neg-float v3, p2

    mul-float/2addr v2, v3

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 73
    # getter for: Lcom/google/android/music/ui/NowPlayingArtViewPager;->MIN_SCALE:F
    invoke-static {}, Lcom/google/android/music/ui/NowPlayingArtViewPager;->access$100()F

    move-result v2

    # getter for: Lcom/google/android/music/ui/NowPlayingArtViewPager;->MIN_SCALE:F
    invoke-static {}, Lcom/google/android/music/ui/NowPlayingArtViewPager;->access$100()F

    move-result v3

    sub-float v3, v5, v3

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v4

    sub-float v4, v5, v4

    mul-float/2addr v3, v4

    add-float v1, v2, v3

    .line 75
    .local v1, "scaleFactor":F
    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleX(F)V

    .line 76
    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    goto :goto_0

    .line 80
    .end local v1    # "scaleFactor":F
    :cond_2
    invoke-virtual {p1, v3}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method
