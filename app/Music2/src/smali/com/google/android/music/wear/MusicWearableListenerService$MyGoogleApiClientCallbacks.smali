.class final Lcom/google/android/music/wear/MusicWearableListenerService$MyGoogleApiClientCallbacks;
.super Ljava/lang/Object;
.source "MusicWearableListenerService.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/wear/MusicWearableListenerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyGoogleApiClientCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/wear/MusicWearableListenerService;


# direct methods
.method private constructor <init>(Lcom/google/android/music/wear/MusicWearableListenerService;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/music/wear/MusicWearableListenerService$MyGoogleApiClientCallbacks;->this$0:Lcom/google/android/music/wear/MusicWearableListenerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/wear/MusicWearableListenerService;Lcom/google/android/music/wear/MusicWearableListenerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/wear/MusicWearableListenerService;
    .param p2, "x1"    # Lcom/google/android/music/wear/MusicWearableListenerService$1;

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/google/android/music/wear/MusicWearableListenerService$MyGoogleApiClientCallbacks;-><init>(Lcom/google/android/music/wear/MusicWearableListenerService;)V

    return-void
.end method


# virtual methods
.method public onConnected(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "connectionHint"    # Landroid/os/Bundle;

    .prologue
    .line 101
    return-void
.end method

.method public onConnectionSuspended(I)V
    .locals 3
    .param p1, "cause"    # I

    .prologue
    .line 105
    const-string v0, "MusicWearableListenerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GoogleApiClient suspended: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    return-void
.end method
