.class Lcom/google/android/music/AudioPreview$ProgressRefresher;
.super Ljava/lang/Object;
.source "AudioPreview.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AudioPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProgressRefresher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/AudioPreview;


# direct methods
.method constructor <init>(Lcom/google/android/music/AudioPreview;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/google/android/music/AudioPreview$ProgressRefresher;->this$0:Lcom/google/android/music/AudioPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$ProgressRefresher;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$300(Lcom/google/android/music/AudioPreview;)Lcom/google/android/music/AudioPreview$PreviewPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/AudioPreview$ProgressRefresher;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mSeeking:Z
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$800(Lcom/google/android/music/AudioPreview;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/AudioPreview$ProgressRefresher;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mDuration:I
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$900(Lcom/google/android/music/AudioPreview;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$ProgressRefresher;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$1000(Lcom/google/android/music/AudioPreview;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/AudioPreview$ProgressRefresher;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;
    invoke-static {v1}, Lcom/google/android/music/AudioPreview;->access$300(Lcom/google/android/music/AudioPreview;)Lcom/google/android/music/AudioPreview$PreviewPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$ProgressRefresher;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$1100(Lcom/google/android/music/AudioPreview;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 313
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$ProgressRefresher;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$1100(Lcom/google/android/music/AudioPreview;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/AudioPreview$ProgressRefresher;

    iget-object v2, p0, Lcom/google/android/music/AudioPreview$ProgressRefresher;->this$0:Lcom/google/android/music/AudioPreview;

    invoke-direct {v1, v2}, Lcom/google/android/music/AudioPreview$ProgressRefresher;-><init>(Lcom/google/android/music/AudioPreview;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 314
    return-void
.end method
