.class Lcom/google/android/music/xdi/DetailAlbumCursor;
.super Landroid/database/MatrixCursor;
.source "DetailAlbumCursor.java"


# static fields
.field private static final PROJECTION_ALBUMS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mImageHeight:I

.field private final mImageWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "album_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "album_art"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/DetailAlbumCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "albumId"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 44
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mContext:Landroid/content/Context;

    .line 45
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 47
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mImageWidth:I

    .line 48
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mImageHeight:I

    .line 50
    invoke-direct {p0, p3}, Lcom/google/android/music/xdi/DetailAlbumCursor;->addAlbum(Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method private addAlbum(Ljava/lang/String;)V
    .locals 14
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 54
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mContext:Landroid/content/Context;

    sget-object v10, Lcom/google/android/music/xdi/DetailAlbumCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    invoke-static {v9, p1, v10}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumCursor(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 56
    .local v5, "cursor":Landroid/database/Cursor;
    if-nez v5, :cond_0

    .line 109
    :goto_0
    return-void

    .line 61
    :cond_0
    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 62
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v9}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v9

    new-array v8, v9, [Ljava/lang/Object;

    .line 63
    .local v8, "values":[Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v6

    .line 65
    .local v6, "isNautilusAlbum":Z
    const/4 v9, 0x0

    invoke-interface {v5, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 66
    .local v7, "name":Ljava/lang/String;
    const/4 v9, 0x2

    invoke-interface {v5, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 67
    .local v2, "artistId":J
    const/4 v4, 0x0

    .line 68
    .local v4, "artistMetajamId":Ljava/lang/String;
    const/4 v9, 0x3

    invoke-interface {v5, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_1

    .line 69
    const/4 v9, 0x3

    invoke-interface {v5, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 71
    :cond_1
    const/4 v0, 0x0

    .line 72
    .local v0, "artUriString":Ljava/lang/String;
    const/4 v1, 0x0

    .line 74
    .local v1, "artistArtUri":Ljava/lang/String;
    if-eqz v6, :cond_5

    .line 75
    const/4 v9, 0x1

    invoke-interface {v5, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_2

    .line 76
    const/4 v9, 0x1

    invoke-interface {v5, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 78
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 79
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultAlbumArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 86
    :cond_3
    :goto_1
    const-wide/16 v10, 0x0

    cmp-long v9, v2, v10

    if-lez v9, :cond_6

    .line 87
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mContext:Landroid/content/Context;

    invoke-static {v9, v2, v3}, Lcom/google/android/music/utils/MusicUtils;->getArtistArtUrl(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 94
    :goto_2
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v10, "_id"

    invoke-interface {v5}, Landroid/database/Cursor;->getPosition()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v8, v10, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 95
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v10, "foreground_image_uri"

    invoke-virtual {v9, v8, v10, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 97
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v10, "background_image_uri"

    invoke-virtual {v9, v8, v10, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 99
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v10, "badge_uri"

    iget-object v11, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultBadgeUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v8, v10, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 101
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v10, "color_hint"

    iget-object v11, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0c00a6

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v8, v10, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 104
    invoke-virtual {p0, v8}, Lcom/google/android/music/xdi/DetailAlbumCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    .end local v0    # "artUriString":Ljava/lang/String;
    .end local v1    # "artistArtUri":Ljava/lang/String;
    .end local v2    # "artistId":J
    .end local v4    # "artistMetajamId":Ljava/lang/String;
    .end local v6    # "isNautilusAlbum":Z
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "values":[Ljava/lang/Object;
    :cond_4
    invoke-static {v5}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 82
    .restart local v0    # "artUriString":Ljava/lang/String;
    .restart local v1    # "artistArtUri":Ljava/lang/String;
    .restart local v2    # "artistId":J
    .restart local v4    # "artistMetajamId":Ljava/lang/String;
    .restart local v6    # "isNautilusAlbum":Z
    .restart local v7    # "name":Ljava/lang/String;
    .restart local v8    # "values":[Ljava/lang/Object;
    :cond_5
    :try_start_1
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const/4 v9, 0x1

    iget v12, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mImageWidth:I

    iget v13, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mImageHeight:I

    invoke-static {v10, v11, v9, v12, v13}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 88
    :cond_6
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 89
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mContext:Landroid/content/Context;

    invoke-static {v9, v4}, Lcom/google/android/music/utils/MusicUtils;->getNautilusArtistArtUrl(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 91
    :cond_7
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumCursor;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistArtUri(Landroid/content/Context;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_2

    .line 107
    .end local v0    # "artUriString":Ljava/lang/String;
    .end local v1    # "artistArtUri":Ljava/lang/String;
    .end local v2    # "artistId":J
    .end local v4    # "artistMetajamId":Ljava/lang/String;
    .end local v6    # "isNautilusAlbum":Z
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "values":[Ljava/lang/Object;
    :catchall_0
    move-exception v9

    invoke-static {v5}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v9
.end method
