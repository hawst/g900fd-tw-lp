.class interface abstract Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;
.super Ljava/lang/Object;
.source "GetAccountOffersTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/tutorial/GetAccountOffersTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract onAccountDisabled(Landroid/accounts/Account;)V
.end method

.method public abstract onAccountInvalid(Landroid/accounts/Account;)V
.end method

.method public abstract onAccountOffersError(Landroid/accounts/Account;)V
.end method

.method public abstract onAccountOffersSuccess(Landroid/accounts/Account;Lcom/google/android/music/cloudclient/OffersResponseJson;)V
.end method
