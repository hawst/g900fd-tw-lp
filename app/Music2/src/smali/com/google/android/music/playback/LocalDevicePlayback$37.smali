.class synthetic Lcom/google/android/music/playback/LocalDevicePlayback$37;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$google$android$music$download$ContentIdentifier$Domain:[I

.field static final synthetic $SwitchMap$com$google$android$music$mix$PlayQueueFeeder$PostProcessingAction:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2739
    invoke-static {}, Lcom/google/android/music/download/ContentIdentifier$Domain;->values()[Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/music/playback/LocalDevicePlayback$37;->$SwitchMap$com$google$android$music$download$ContentIdentifier$Domain:[I

    :try_start_0
    sget-object v0, Lcom/google/android/music/playback/LocalDevicePlayback$37;->$SwitchMap$com$google$android$music$download$ContentIdentifier$Domain:[I

    sget-object v1, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-virtual {v1}, Lcom/google/android/music/download/ContentIdentifier$Domain;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_0
    :try_start_1
    sget-object v0, Lcom/google/android/music/playback/LocalDevicePlayback$37;->$SwitchMap$com$google$android$music$download$ContentIdentifier$Domain:[I

    sget-object v1, Lcom/google/android/music/download/ContentIdentifier$Domain;->NAUTILUS:Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-virtual {v1}, Lcom/google/android/music/download/ContentIdentifier$Domain;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    .line 445
    :goto_1
    invoke-static {}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->values()[Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/music/playback/LocalDevicePlayback$37;->$SwitchMap$com$google$android$music$mix$PlayQueueFeeder$PostProcessingAction:[I

    :try_start_2
    sget-object v0, Lcom/google/android/music/playback/LocalDevicePlayback$37;->$SwitchMap$com$google$android$music$mix$PlayQueueFeeder$PostProcessingAction:[I

    sget-object v1, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->OPEN:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    invoke-virtual {v1}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_2
    :try_start_3
    sget-object v0, Lcom/google/android/music/playback/LocalDevicePlayback$37;->$SwitchMap$com$google$android$music$mix$PlayQueueFeeder$PostProcessingAction:[I

    sget-object v1, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->NEXT:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    invoke-virtual {v1}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_3
    :try_start_4
    sget-object v0, Lcom/google/android/music/playback/LocalDevicePlayback$37;->$SwitchMap$com$google$android$music$mix$PlayQueueFeeder$PostProcessingAction:[I

    sget-object v1, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->FEED:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    invoke-virtual {v1}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_4
    :try_start_5
    sget-object v0, Lcom/google/android/music/playback/LocalDevicePlayback$37;->$SwitchMap$com$google$android$music$mix$PlayQueueFeeder$PostProcessingAction:[I

    sget-object v1, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->NONE:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    invoke-virtual {v1}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_5
    :try_start_6
    sget-object v0, Lcom/google/android/music/playback/LocalDevicePlayback$37;->$SwitchMap$com$google$android$music$mix$PlayQueueFeeder$PostProcessingAction:[I

    sget-object v1, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->FEED_REFRESH:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    invoke-virtual {v1}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_6
    :try_start_7
    sget-object v0, Lcom/google/android/music/playback/LocalDevicePlayback$37;->$SwitchMap$com$google$android$music$mix$PlayQueueFeeder$PostProcessingAction:[I

    sget-object v1, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->CLEAR_REFRESH:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    invoke-virtual {v1}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_7
    return-void

    :catch_0
    move-exception v0

    goto :goto_7

    :catch_1
    move-exception v0

    goto :goto_6

    :catch_2
    move-exception v0

    goto :goto_5

    :catch_3
    move-exception v0

    goto :goto_4

    :catch_4
    move-exception v0

    goto :goto_3

    :catch_5
    move-exception v0

    goto :goto_2

    .line 2739
    :catch_6
    move-exception v0

    goto :goto_1

    :catch_7
    move-exception v0

    goto :goto_0
.end method
