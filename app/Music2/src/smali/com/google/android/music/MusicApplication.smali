.class public Lcom/google/android/music/MusicApplication;
.super Landroid/app/Application;
.source "MusicApplication.java"


# instance fields
.field private mCastProviderFactory:Lcom/google/android/music/cast/CastMediaProviderFactory;

.field private mMediaRouter:Landroid/support/v7/media/MediaRouter;

.field mNewUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

.field mOldUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 205
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/MusicApplication;->mOldUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 206
    new-instance v0, Lcom/google/android/music/MusicApplication$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/MusicApplication$2;-><init>(Lcom/google/android/music/MusicApplication;)V

    iput-object v0, p0, Lcom/google/android/music/MusicApplication;->mNewUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 18

    .prologue
    .line 55
    invoke-super/range {p0 .. p0}, Landroid/app/Application;->onCreate()V

    .line 57
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 58
    .local v2, "appContext":Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->init(Landroid/content/Context;)V

    .line 59
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    invoke-static {v14}, Lcom/google/android/music/utils/ConfigUtils;->init(Landroid/content/ContentResolver;)V

    .line 60
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/utils/MusicUtils;->isUIProcess(Landroid/content/Context;)Z

    move-result v8

    .line 62
    .local v8, "isUIProcess":Z
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "music_use_gms_core_http_client"

    const/16 v16, 0x1

    invoke-static/range {v14 .. v16}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v13

    .line 67
    .local v13, "useGMSCoreHttpClient":Z
    const/4 v6, 0x0

    .line 71
    .local v6, "isGMSCoreInstalled":Z
    if-eqz v13, :cond_0

    invoke-static {v2}, Lcom/google/android/music/utils/SystemUtils;->isTv(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_0

    .line 73
    :try_start_0
    invoke-static {v2}, Lcom/google/android/gms/security/ProviderInstaller;->installIfNeeded(Landroid/content/Context;)V

    .line 74
    const/4 v6, 0x1

    .line 75
    const-string v14, "AndroidMusic"

    const-string v15, "GMSCore installation verified"

    invoke-static {v14, v15}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesRepairableException; {:try_start_0 .. :try_end_0} :catch_1

    .line 85
    :cond_0
    :goto_0
    invoke-static {v6}, Lcom/google/android/music/GoogleHttpClientFactory;->configure(Z)V

    .line 88
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v4

    .line 90
    .local v4, "eventLogger":Lcom/google/android/music/eventlog/MusicEventLogger;
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v10

    .line 91
    .local v10, "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v7, 0x0

    .line 92
    .local v7, "isLogFilesEnabled":Z
    invoke-virtual {v10}, Lcom/google/android/music/preferences/MusicPreferences;->getSelectedStorageVolumeId()Ljava/util/UUID;

    move-result-object v12

    .line 94
    .local v12, "selectedVolumeId":Ljava/util/UUID;
    :try_start_1
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    .line 95
    .local v11, "resolver":Landroid/content/ContentResolver;
    const-string v14, "music_debug_logs_enabled"

    const/4 v15, 0x0

    invoke-static {v11, v14, v15}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v5

    .line 97
    .local v5, "isDebugLogsAvailable":Z
    if-eqz v5, :cond_8

    .line 98
    invoke-virtual {v10}, Lcom/google/android/music/preferences/MusicPreferences;->isLogFilesEnabled()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    .line 103
    :goto_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 105
    if-eqz v7, :cond_2

    .line 106
    const/4 v14, 0x1

    invoke-static {v14}, Lcom/google/android/music/utils/DebugUtils;->setAutoLogAll(Z)V

    .line 108
    new-instance v9, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v14

    const-string v15, "music2_logs"

    invoke-direct {v9, v14, v15}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 109
    .local v9, "logsDir":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_1

    .line 110
    invoke-virtual {v9}, Ljava/io/File;->mkdir()Z

    .line 114
    :cond_1
    if-eqz v8, :cond_9

    .line 115
    const-string v14, "music_ui.log"

    invoke-static {v9, v14}, Lcom/google/android/music/log/Log;->configure(Ljava/io/File;Ljava/lang/String;)V

    .line 123
    .end local v9    # "logsDir":Ljava/io/File;
    :cond_2
    :goto_2
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    .line 125
    if-eqz v8, :cond_a

    .line 126
    invoke-static {}, Lcom/google/android/music/utils/AlbumArtUtils;->initBitmapPoolForUIProcess()V

    .line 127
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    .line 133
    :goto_3
    if-eqz v8, :cond_3

    .line 134
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/utils/MusicUtils;->openPlaylistCursor(Landroid/content/Context;)V

    .line 138
    :cond_3
    if-eqz v8, :cond_4

    .line 139
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/playperf/MeasurementServiceHelper;->init(Landroid/content/Context;)V

    .line 142
    :cond_4
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/music/MusicApplication;->mOldUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 143
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/MusicApplication;->mNewUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-static {v14}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 145
    if-eqz v8, :cond_5

    .line 146
    invoke-virtual {v4}, Lcom/google/android/music/eventlog/MusicEventLogger;->logUIStarted()V

    .line 149
    :cond_5
    if-nez v8, :cond_6

    .line 151
    sget-object v14, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v15, Lcom/google/android/music/MusicApplication$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/music/MusicApplication$1;-><init>(Lcom/google/android/music/MusicApplication;)V

    const-wide/16 v16, 0x1388

    invoke-virtual/range {v14 .. v17}, Lcom/google/android/music/utils/LoggableHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 164
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v15

    new-instance v16, Landroid/content/ComponentName;

    const-string v14, "com.android.music.MediaAppWidgetProvider"

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v14}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v17, 0x7f100007

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v14

    if-eqz v14, :cond_b

    const/4 v14, 0x1

    :goto_4
    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v15, v0, v14, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 173
    :cond_6
    invoke-virtual {v10}, Lcom/google/android/music/preferences/MusicPreferences;->isMediaRouteSupportEnabled()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 174
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isGingerbreadOrGreater()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 177
    invoke-static {v2}, Landroid/support/v7/media/MediaRouter;->getInstance(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/music/MusicApplication;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    .line 179
    invoke-static {v2}, Lcom/google/android/music/cast/CastUtils;->shouldUseCastV1(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 181
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/MusicApplication;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-static {v2, v14}, Lcom/google/android/music/cast/CastUtils;->disableCastV2Provider(Landroid/content/Context;Landroid/support/v7/media/MediaRouter;)V

    .line 186
    new-instance v14, Lcom/google/android/music/cast/CastMediaProviderFactory;

    invoke-direct {v14, v2, v7}, Lcom/google/android/music/cast/CastMediaProviderFactory;-><init>(Landroid/content/Context;Z)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/music/MusicApplication;->mCastProviderFactory:Lcom/google/android/music/cast/CastMediaProviderFactory;

    .line 189
    if-eqz v8, :cond_c

    .line 195
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/MusicApplication;->mCastProviderFactory:Lcom/google/android/music/cast/CastMediaProviderFactory;

    invoke-virtual {v14}, Lcom/google/android/music/cast/CastMediaProviderFactory;->registerMinimalMediaRouteProvider()V

    .line 203
    :cond_7
    :goto_5
    return-void

    .line 76
    .end local v4    # "eventLogger":Lcom/google/android/music/eventlog/MusicEventLogger;
    .end local v5    # "isDebugLogsAvailable":Z
    .end local v7    # "isLogFilesEnabled":Z
    .end local v10    # "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v11    # "resolver":Landroid/content/ContentResolver;
    .end local v12    # "selectedVolumeId":Ljava/util/UUID;
    :catch_0
    move-exception v3

    .line 77
    .local v3, "e":Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;
    const-string v14, "AndroidMusic"

    const-string v15, "GMSCore installation not available"

    invoke-static {v14, v15}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget v14, v3, Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;->errorCode:I

    invoke-static {v14, v2}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->showErrorNotification(ILandroid/content/Context;)V

    goto/16 :goto_0

    .line 79
    .end local v3    # "e":Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;
    :catch_1
    move-exception v3

    .line 80
    .local v3, "e":Lcom/google/android/gms/common/GooglePlayServicesRepairableException;
    const-string v14, "AndroidMusic"

    const-string v15, "GMSCore installation needs repair"

    invoke-static {v14, v15}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-virtual {v3}, Lcom/google/android/gms/common/GooglePlayServicesRepairableException;->getConnectionStatusCode()I

    move-result v14

    invoke-static {v14, v2}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->showErrorNotification(ILandroid/content/Context;)V

    goto/16 :goto_0

    .line 100
    .end local v3    # "e":Lcom/google/android/gms/common/GooglePlayServicesRepairableException;
    .restart local v4    # "eventLogger":Lcom/google/android/music/eventlog/MusicEventLogger;
    .restart local v5    # "isDebugLogsAvailable":Z
    .restart local v7    # "isLogFilesEnabled":Z
    .restart local v10    # "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    .restart local v11    # "resolver":Landroid/content/ContentResolver;
    .restart local v12    # "selectedVolumeId":Ljava/util/UUID;
    :cond_8
    const/4 v14, 0x0

    :try_start_2
    invoke-virtual {v10, v14}, Lcom/google/android/music/preferences/MusicPreferences;->setLogFilesEnable(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 103
    .end local v5    # "isDebugLogsAvailable":Z
    .end local v11    # "resolver":Landroid/content/ContentResolver;
    :catchall_0
    move-exception v14

    invoke-static/range {p0 .. p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v14

    .line 117
    .restart local v5    # "isDebugLogsAvailable":Z
    .restart local v9    # "logsDir":Ljava/io/File;
    .restart local v11    # "resolver":Landroid/content/ContentResolver;
    :cond_9
    const-string v14, "music_main.log"

    invoke-static {v9, v14}, Lcom/google/android/music/log/Log;->configure(Ljava/io/File;Ljava/lang/String;)V

    .line 118
    const-string v14, "com.google.android.music.pin"

    const-string v15, "music_pin.log"

    invoke-static {v14, v9, v15}, Lcom/google/android/music/log/Log;->configureLogFile(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 129
    .end local v9    # "logsDir":Ljava/io/File;
    :cond_a
    invoke-static {}, Lcom/google/android/music/utils/AlbumArtUtils;->initBitmapPoolForMainProcess()V

    .line 130
    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lcom/google/android/music/download/cache/StorageMigrationService;->resumeMigrationIfNeeded(Landroid/content/Context;Ljava/util/UUID;)V

    goto/16 :goto_3

    .line 164
    :cond_b
    const/4 v14, 0x2

    goto/16 :goto_4

    .line 198
    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/MusicApplication;->mCastProviderFactory:Lcom/google/android/music/cast/CastMediaProviderFactory;

    invoke-virtual {v14}, Lcom/google/android/music/cast/CastMediaProviderFactory;->registerMediaRouteProvider()V

    goto :goto_5
.end method

.method public onTerminate()V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/music/MusicApplication;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/google/android/music/MusicApplication;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-static {v0}, Lcom/google/android/music/cast/CastUtils;->cleaup(Landroid/support/v7/media/MediaRouter;)V

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/MusicApplication;->mCastProviderFactory:Lcom/google/android/music/cast/CastMediaProviderFactory;

    if-eqz v0, :cond_1

    .line 229
    iget-object v0, p0, Lcom/google/android/music/MusicApplication;->mCastProviderFactory:Lcom/google/android/music/cast/CastMediaProviderFactory;

    invoke-virtual {v0}, Lcom/google/android/music/cast/CastMediaProviderFactory;->destroy()V

    .line 230
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/MusicApplication;->mCastProviderFactory:Lcom/google/android/music/cast/CastMediaProviderFactory;

    .line 232
    :cond_1
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->closePlaylistCursor()V

    .line 233
    invoke-static {}, Lcom/google/android/music/eventlog/MusicEventLogger;->destroy()V

    .line 236
    invoke-static {p0}, Lcom/google/android/music/playperf/MeasurementServiceHelper;->destroy(Landroid/content/Context;)V

    .line 238
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 239
    return-void
.end method
