.class public Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;
.super Lcom/google/android/music/leanback/LeanbackDetailsActivity;
.source "LeanbackArtistDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;
    }
.end annotation


# static fields
.field private static final PROJECTION_ARTISTS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mItemPresenter:Lcom/google/android/music/leanback/ItemPresenter;

.field private mPlaySongsSongList:Lcom/google/android/music/medialist/SongList;

.field private mPlayTopSongsAdded:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->TAG:Ljava/lang/String;

    .line 62
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->PROJECTION_ARTISTS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackDetailsActivity;-><init>()V

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->mPlayTopSongsAdded:Z

    .line 356
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/Action;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createShuffleAllSongsAction(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/Action;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/Action;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createStartRadioAction(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/Action;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;JLjava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createLockerAlbumsRow(JLjava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createNautilusAlbumsRow(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createRelatedArtistsRow(Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Ljava/lang/String;Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/music/leanback/Item;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createSongList(Ljava/lang/String;Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->mPlaySongsSongList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->mPlayTopSongsAdded:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->mPlayTopSongsAdded:Z

    return p1
.end method

.method static synthetic access$202(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/medialist/SongList;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;
    .param p1, "x1"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->mPlaySongsSongList:Lcom/google/android/music/medialist/SongList;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;)Landroid/support/v17/leanback/widget/Action;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createPlayTopSongsAction()Landroid/support/v17/leanback/widget/Action;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Lcom/google/android/music/medialist/SongList;I)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;
    .param p1, "x1"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "x2"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getPlaySongsIntent(Lcom/google/android/music/medialist/SongList;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Landroid/support/v17/leanback/widget/ObjectAdapter;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getPlaySongsSongList(Landroid/support/v17/leanback/widget/ObjectAdapter;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    return-object v0
.end method

.method private createArtistDetails(Ljava/lang/String;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 4
    .param p1, "artistId"    # Ljava/lang/String;

    .prologue
    .line 113
    new-instance v0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Ljava/lang/String;)V

    .line 148
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getArtistDetailsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->PROJECTION_ARTISTS:[Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V

    .line 150
    return-object v0
.end method

.method private createLockerAlbumsRow(JLjava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 5
    .param p1, "artistId"    # J
    .param p3, "artistName"    # Ljava/lang/String;

    .prologue
    .line 219
    move-object v1, p0

    .line 220
    .local v1, "context":Landroid/content/Context;
    new-instance v0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->mItemPresenter:Lcom/google/android/music/leanback/ItemPresenter;

    invoke-direct {v0, p0, v2, v1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Landroid/support/v17/leanback/widget/Presenter;Landroid/content/Context;)V

    .line 247
    .local v0, "albumsAdapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/music/medialist/ArtistAlbumList;

    const/4 v4, 0x0

    invoke-direct {v3, p1, p2, p3, v4}, Lcom/google/android/music/medialist/ArtistAlbumList;-><init>(JLjava/lang/String;Z)V

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->ALBUM_COLUMNS:[Ljava/lang/String;

    invoke-virtual {p0, v2, v0, v3, v4}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 251
    const v2, 0x7f0b00b2

    invoke-virtual {p0, v2, v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createListRow(ILandroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v2

    return-object v2
.end method

.method private createNautilusAlbumsRow(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 4
    .param p1, "nautilusArtistId"    # Ljava/lang/String;
    .param p2, "artist"    # Ljava/lang/String;

    .prologue
    .line 255
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 257
    :cond_0
    const/4 v1, 0x0

    .line 284
    :goto_0
    return-object v1

    .line 260
    :cond_1
    new-instance v0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$4;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->mItemPresenter:Lcom/google/android/music/leanback/ItemPresenter;

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$4;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 281
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/music/medialist/NautilusArtistAlbumList;

    invoke-direct {v2, p1, p2}, Lcom/google/android/music/medialist/NautilusArtistAlbumList;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->ALBUM_COLUMNS:[Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 284
    const v1, 0x7f0b036c

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createListRow(Ljava/lang/String;Landroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v1

    goto :goto_0
.end method

.method private createPlayTopSongsAction()Landroid/support/v17/leanback/widget/Action;
    .locals 7

    .prologue
    .line 176
    new-instance v0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$2;

    const-wide/16 v2, -0x1

    const v1, 0x7f0b02d5

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v1, 0x7f0b02d7

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$2;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    return-object v0
.end method

.method private createRelatedArtistsRow(Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 4
    .param p1, "nautilusArtistId"    # Ljava/lang/String;

    .prologue
    .line 288
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 290
    :cond_0
    const/4 v1, 0x0

    .line 316
    :goto_0
    return-object v1

    .line 293
    :cond_1
    new-instance v0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$5;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->mItemPresenter:Lcom/google/android/music/leanback/ItemPresenter;

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$5;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 312
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    const/4 v1, 0x3

    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Artists;->getRelatedArtistsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->ARTIST_COLUMNS:[Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V

    .line 316
    const v1, 0x7f0b00b1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createListRow(ILandroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v1

    goto :goto_0
.end method

.method private createShuffleAllSongsAction(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/Action;
    .locals 7
    .param p1, "artistId"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 211
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getArtistSongList(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    .line 212
    .local v0, "songList":Lcom/google/android/music/medialist/SongList;
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;I)Landroid/content/Intent;

    move-result-object v6

    .line 214
    .local v6, "intent":Landroid/content/Intent;
    new-instance v1, Lcom/google/android/music/leanback/IntentAction;

    const-wide/16 v2, -0x1

    const v4, 0x7f0b0056

    invoke-virtual {p0, v4}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0b02d8

    invoke-virtual {p0, v5}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/leanback/IntentAction;-><init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    return-object v1
.end method

.method private createSongList(Ljava/lang/String;Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 4
    .param p1, "nautilusArtistId"    # Ljava/lang/String;
    .param p2, "item"    # Lcom/google/android/music/leanback/Item;

    .prologue
    .line 320
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 322
    :cond_0
    const/4 v0, 0x0

    .line 353
    :goto_0
    return-object v0

    .line 324
    :cond_1
    new-instance v0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;

    invoke-direct {v0, p0, p2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Lcom/google/android/music/leanback/Item;)V

    .line 344
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    new-instance v1, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$7;

    invoke-direct {v1, p0, v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$7;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 350
    const/4 v1, 0x4

    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Artists;->getTopSongsByArtistUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->SONG_COLUMNS:[Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method private createStartRadioAction(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/Action;
    .locals 8
    .param p1, "artistId"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 165
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getArtistSongList(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/SongList;

    move-result-object v7

    .line 166
    .local v7, "songList":Lcom/google/android/music/medialist/SongList;
    invoke-static {p0, v7}, Lcom/google/android/music/utils/MusicUtils;->getSongListRadioMixDescriptor(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v0

    .line 167
    .local v0, "descriptor":Lcom/google/android/music/mix/MixDescriptor;
    invoke-static {p0, v0}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/mix/MixDescriptor;)Landroid/content/Intent;

    move-result-object v6

    .line 169
    .local v6, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0b023a

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 172
    .local v4, "actionTitle":Ljava/lang/String;
    :goto_0
    new-instance v1, Lcom/google/android/music/leanback/IntentAction;

    const-wide/16 v2, -0x1

    const v5, 0x7f0b02d6

    invoke-virtual {p0, v5}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/leanback/IntentAction;-><init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    return-object v1

    .line 169
    .end local v4    # "actionTitle":Ljava/lang/String;
    :cond_0
    const v1, 0x7f0b023b

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private getArtistDetailsUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 7
    .param p1, "artistId"    # Ljava/lang/String;

    .prologue
    .line 96
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v3

    .line 98
    .local v3, "isNautilusId":Z
    if-eqz v3, :cond_0

    .line 99
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Artists;->getNautilusArtistsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 108
    :goto_0
    return-object v4

    .line 101
    :cond_0
    const-wide/16 v0, 0x0

    .line 103
    .local v0, "artistIdLong":J
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 108
    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v4

    goto :goto_0

    .line 104
    :catch_0
    move-exception v2

    .line 105
    .local v2, "e":Ljava/lang/NumberFormatException;
    sget-object v4, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error converting to long: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private getArtistSongList(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/SongList;
    .locals 7
    .param p1, "artistId"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 154
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v0

    .line 155
    .local v0, "isNautilusArtistId":Z
    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/music/medialist/NautilusArtistSongList;

    invoke-direct {v1, p1, p2}, Lcom/google/android/music/medialist/NautilusArtistSongList;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/music/medialist/ArtistSongList;

    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/medialist/ArtistSongList;-><init>(JLjava/lang/String;IZ)V

    goto :goto_0
.end method

.method private getPlaySongsIntent(Lcom/google/android/music/medialist/SongList;I)Landroid/content/Intent;
    .locals 1
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "offset"    # I

    .prologue
    .line 203
    if-nez p1, :cond_0

    .line 204
    const/4 v0, 0x0

    .line 206
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;II)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private getPlaySongsSongList(Landroid/support/v17/leanback/widget/ObjectAdapter;)Lcom/google/android/music/medialist/SongList;
    .locals 6
    .param p1, "songListObjectAdapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 186
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 187
    .local v2, "songDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 188
    invoke-virtual {p1, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/leanback/SongRow;

    invoke-virtual {v5}, Lcom/google/android/music/leanback/SongRow;->getDocument()Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 190
    :cond_0
    invoke-static {v2}, Lcom/google/android/music/xdi/XdiUtils;->extractTrackMetajamIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 191
    .local v4, "songIdsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    new-array v3, v5, [Ljava/lang/String;

    .line 192
    .local v3, "songIds":[Ljava/lang/String;
    invoke-interface {v4, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 193
    array-length v5, v3

    if-gtz v5, :cond_1

    .line 194
    const/4 v5, 0x0

    .line 199
    :goto_1
    return-object v5

    .line 197
    :cond_1
    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newUnknownContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 199
    .local v0, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v5, Lcom/google/android/music/medialist/NautilusSelectedSongList;

    invoke-direct {v5, v0, v3}, Lcom/google/android/music/medialist/NautilusSelectedSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;[Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method protected getAdapter(Landroid/support/v17/leanback/widget/PresenterSelector;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 7
    .param p1, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "artist_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 85
    .local v6, "artistId":Ljava/lang/String;
    invoke-direct {p0, v6}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createArtistDetails(Ljava/lang/String;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v3

    .line 86
    .local v3, "detailsObjectAdapter":Landroid/support/v17/leanback/widget/ObjectAdapter;
    new-instance v0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    new-instance v4, Lcom/google/android/music/leanback/SongTitleRow;

    const v1, 0x7f0b00b0

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/google/android/music/leanback/SongTitleRow;-><init>(Ljava/lang/String;)V

    new-instance v5, Lcom/google/android/music/leanback/SongFooterRow;

    invoke-direct {v5}, Lcom/google/android/music/leanback/SongFooterRow;-><init>()V

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Landroid/support/v17/leanback/widget/PresenterSelector;Landroid/support/v17/leanback/widget/ObjectAdapter;Lcom/google/android/music/leanback/SongTitleRow;Lcom/google/android/music/leanback/SongFooterRow;)V

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    new-instance v0, Lcom/google/android/music/leanback/ItemPresenter;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/ItemPresenter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->mItemPresenter:Lcom/google/android/music/leanback/ItemPresenter;

    .line 79
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/LeanbackDetailsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 80
    return-void
.end method

.method protected setTransitionInfo(Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;)V
    .locals 0
    .param p1, "detailsOverviewRowPresenter"    # Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;

    .prologue
    .line 93
    return-void
.end method
