.class public interface abstract Lcom/google/android/music/art/ArtLoader;
.super Ljava/lang/Object;
.source "ArtLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/ArtLoader$Listener;,
        Lcom/google/android/music/art/ArtLoader$DownloadMode;
    }
.end annotation


# virtual methods
.method public abstract getArtFileDescriptorAsync(Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;)V
.end method

.method public abstract getArtFileDescriptorAsync(Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;Landroid/os/Handler;)V
.end method

.method public abstract getArtFileDescriptorSync(Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;)Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation
.end method
