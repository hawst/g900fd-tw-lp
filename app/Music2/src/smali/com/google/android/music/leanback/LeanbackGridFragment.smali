.class public Lcom/google/android/music/leanback/LeanbackGridFragment;
.super Landroid/support/v17/leanback/app/VerticalGridFragment;
.source "LeanbackGridFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Landroid/support/v17/leanback/app/VerticalGridFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 13
    if-eqz p1, :cond_0

    .line 14
    new-instance v0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;-><init>()V

    .line 15
    .local v0, "presenter":Landroid/support/v17/leanback/widget/VerticalGridPresenter;
    const-string v1, "Rows"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->setNumberOfColumns(I)V

    .line 16
    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackGridFragment;->setGridPresenter(Landroid/support/v17/leanback/widget/VerticalGridPresenter;)V

    .line 18
    const-string v1, "Title"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackGridFragment;->setTitle(Ljava/lang/String;)V

    .line 20
    .end local v0    # "presenter":Landroid/support/v17/leanback/widget/VerticalGridPresenter;
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v17/leanback/app/VerticalGridFragment;->onCreate(Landroid/os/Bundle;)V

    .line 22
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackSearchOrbItemActivity;->getSearchOrbViewColors(Landroid/content/res/Resources;)Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackGridFragment;->setSearchAffordanceColors(Landroid/support/v17/leanback/widget/SearchOrbView$Colors;)V

    .line 24
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/support/v17/leanback/app/VerticalGridFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 29
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackGridFragment;->getGridPresenter()Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    move-result-object v0

    .line 31
    .local v0, "presenter":Landroid/support/v17/leanback/widget/VerticalGridPresenter;
    const-string v1, "Rows"

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->getNumberOfColumns()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 32
    const-string v1, "Title"

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackGridFragment;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    return-void
.end method
