.class public Lcom/google/android/music/DeleteConfirmationDialog;
.super Landroid/app/AlertDialog;
.source "DeleteConfirmationDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/DeleteConfirmationDialog$2;,
        Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;
    }
.end annotation


# instance fields
.field private mArtistName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mHasRemote:Z

.field private mItemTitle:Ljava/lang/String;

.field private mPrimaryId:J

.field private mSecondaryId:J

.field private mType:Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;JLjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;
    .param p3, "id"    # J
    .param p5, "itemTitle"    # Ljava/lang/CharSequence;
    .param p6, "artistName"    # Ljava/lang/CharSequence;
    .param p7, "hasRemote"    # Z

    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 55
    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v1, p2

    move-wide v2, p3

    move-object v6, p5

    move-object v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/music/DeleteConfirmationDialog;->init(Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;JJLjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/DeleteConfirmationDialog;)Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/DeleteConfirmationDialog;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mType:Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/DeleteConfirmationDialog;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/DeleteConfirmationDialog;

    .prologue
    .line 26
    iget-wide v0, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mPrimaryId:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/google/android/music/DeleteConfirmationDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/DeleteConfirmationDialog;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private init(Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;JJLjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 10
    .param p1, "type"    # Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;
    .param p2, "primaryId"    # J
    .param p4, "secondaryId"    # J
    .param p6, "itemTitle"    # Ljava/lang/CharSequence;
    .param p7, "artistName"    # Ljava/lang/CharSequence;
    .param p8, "hasRemote"    # Z

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/android/music/DeleteConfirmationDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mContext:Landroid/content/Context;

    .line 62
    iput-wide p2, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mPrimaryId:J

    .line 63
    const-wide/16 v6, -0x1

    cmp-long v5, p4, v6

    if-eqz v5, :cond_1

    .line 64
    iput-wide p4, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mSecondaryId:J

    .line 68
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mType:Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;

    .line 69
    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mHasRemote:Z

    .line 70
    invoke-interface/range {p6 .. p6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mItemTitle:Ljava/lang/String;

    .line 71
    invoke-interface/range {p7 .. p7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mArtistName:Ljava/lang/String;

    .line 73
    iget-object v5, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "music_enable_tracks_upsync_deletion"

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v2

    .line 77
    .local v2, "enableRemoteTrackDeletes":Z
    iget-boolean v5, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mHasRemote:Z

    if-eqz v5, :cond_2

    if-nez v2, :cond_2

    .line 78
    iget-object v5, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mContext:Landroid/content/Context;

    const v6, 0x7f0b01df

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mItemTitle:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/music/DeleteConfirmationDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 88
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/DeleteConfirmationDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 89
    .local v3, "r":Landroid/content/res/Resources;
    const/4 v5, -0x1

    const v6, 0x7f0b004e

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6, p0}, Lcom/google/android/music/DeleteConfirmationDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 91
    const/4 v6, -0x2

    const v5, 0x7f0b004f

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v5, 0x0

    check-cast v5, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v6, v7, v5}, Lcom/google/android/music/DeleteConfirmationDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 93
    return-void

    .line 65
    .end local v2    # "enableRemoteTrackDeletes":Z
    .end local v3    # "r":Landroid/content/res/Resources;
    :cond_1
    sget-object v5, Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;->ALBUM_BY_ARTIST:Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;

    if-ne p1, v5, :cond_0

    .line 66
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "secondary Id required for type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 79
    .restart local v2    # "enableRemoteTrackDeletes":Z
    :cond_2
    iget-boolean v5, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mHasRemote:Z

    if-eqz v5, :cond_3

    .line 80
    const v5, 0x7f0b01db

    invoke-virtual {p0, v5}, Lcom/google/android/music/DeleteConfirmationDialog;->setTitle(I)V

    .line 81
    const-string v5, "<b>%s<br/>%s</b><br/><br/>%s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mItemTitle:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mArtistName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-object v8, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mContext:Landroid/content/Context;

    const v9, 0x7f0b01dc

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    .line 83
    .local v4, "span":Landroid/text/Spanned;
    invoke-virtual {p0, v4}, Lcom/google/android/music/DeleteConfirmationDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 85
    .end local v4    # "span":Landroid/text/Spanned;
    :cond_3
    iget-object v5, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mContext:Landroid/content/Context;

    const v6, 0x7f0b01dd

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mItemTitle:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/music/DeleteConfirmationDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private performDelete()V
    .locals 3

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mType:Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;

    sget-object v1, Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;->SONG:Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mType:Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;

    sget-object v1, Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;->PLAYLIST:Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;

    if-eq v0, v1, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mContext:Landroid/content/Context;

    const-string v1, "TODO: Peform Delete"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 135
    :cond_0
    new-instance v0, Lcom/google/android/music/DeleteConfirmationDialog$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/DeleteConfirmationDialog$1;-><init>(Lcom/google/android/music/DeleteConfirmationDialog;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 149
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/music/DeleteConfirmationDialog;->performDelete()V

    .line 123
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 97
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    .line 98
    if-eqz p1, :cond_0

    .line 99
    const-string v0, "primaryId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 100
    .local v2, "primaryId":J
    const-string v0, "secondartId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 101
    .local v4, "secondaryId":J
    invoke-static {}, Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;->values()[Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;

    move-result-object v0

    const-string v9, "type"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    aget-object v1, v0, v9

    .line 102
    .local v1, "type":Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;
    const-string v0, "hasRemote"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 103
    .local v8, "hasRemote":Z
    const-string v0, "itemTitle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 104
    .local v6, "itemTitle":Ljava/lang/String;
    const-string v0, "artistName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .local v7, "artistName":Ljava/lang/String;
    move-object v0, p0

    .line 105
    invoke-direct/range {v0 .. v8}, Lcom/google/android/music/DeleteConfirmationDialog;->init(Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;JJLjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 107
    .end local v1    # "type":Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;
    .end local v2    # "primaryId":J
    .end local v4    # "secondaryId":J
    .end local v6    # "itemTitle":Ljava/lang/String;
    .end local v7    # "artistName":Ljava/lang/String;
    .end local v8    # "hasRemote":Z
    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 111
    invoke-super {p0}, Landroid/app/AlertDialog;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v0

    .line 112
    .local v0, "outcicle":Landroid/os/Bundle;
    const-string v1, "primaryId"

    iget-wide v2, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mPrimaryId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 113
    const-string v1, "secondartId"

    iget-wide v2, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mSecondaryId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 114
    const-string v1, "type"

    iget-object v2, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mType:Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;

    invoke-virtual {v2}, Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 115
    const-string v1, "hasRemote"

    iget-boolean v2, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mHasRemote:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 116
    const-string v1, "artistName"

    iget-object v2, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mArtistName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v1, "itemTitle"

    iget-object v2, p0, Lcom/google/android/music/DeleteConfirmationDialog;->mItemTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    return-object v0
.end method
