.class Lcom/google/android/music/quiz/ArtistQuizActivity$1;
.super Ljava/lang/Object;
.source "ArtistQuizActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/quiz/ArtistQuizActivity;->fetchArtistList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mLocalResponse:Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

.field final synthetic this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/quiz/ArtistQuizActivity;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 5

    .prologue
    .line 155
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mClientImpl:Lcom/google/android/music/cloudclient/MusicCloudImpl;
    invoke-static {v1}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$400(Lcom/google/android/music/quiz/ArtistQuizActivity;)Lcom/google/android/music/cloudclient/MusicCloudImpl;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mSelectedGenreIds:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$100(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mSelectedArtistIds:Ljava/util/HashSet;
    invoke-static {v3}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$200(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/HashSet;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mUnselectedArtistIds:Ljava/util/HashSet;
    invoke-static {v4}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$300(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->getUserQuizArtistList(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->mLocalResponse:Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    .line 158
    iget-object v1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->mLocalResponse:Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->mLocalResponse:Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;->mArtists:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->mLocalResponse:Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;->mArtists:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->mLocalResponse:Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;->mArtists:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/QuizArtistJson;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/QuizArtistJson;->mArtistId:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 161
    :cond_0
    const-string v1, "MusicQuiz"

    const-string v2, "Incomplete data"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 169
    :cond_1
    :goto_0
    return-void

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "MusicQuiz"

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 166
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 167
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "MusicQuiz"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-virtual {v0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-virtual {v0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->isActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->mLocalResponse:Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->mLocalResponse:Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;->mArtists:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 177
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    iget-object v1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->mLocalResponse:Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    # invokes: Lcom/google/android/music/quiz/ArtistQuizActivity;->prefetchArt(Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;)V
    invoke-static {v0, v1}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$500(Lcom/google/android/music/quiz/ArtistQuizActivity;Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;)V

    .line 178
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    iget-object v1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->mLocalResponse:Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    # invokes: Lcom/google/android/music/quiz/ArtistQuizActivity;->updateAdapter(Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;)V
    invoke-static {v0, v1}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$600(Lcom/google/android/music/quiz/ArtistQuizActivity;Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;)V

    .line 180
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/music/quiz/ArtistQuizActivity;->setLoadingProgressVisibility(I)V

    .line 181
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$1;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/music/quiz/ArtistQuizActivity;->enableButtons(Z)V
    invoke-static {v0, v1}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$700(Lcom/google/android/music/quiz/ArtistQuizActivity;Z)V

    goto :goto_0
.end method
