.class Lcom/google/android/music/dial/WebSocketMultiplexer$1;
.super Ljava/lang/Object;
.source "WebSocketMultiplexer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/dial/WebSocketMultiplexer;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/dial/WebSocketMultiplexer;


# direct methods
.method constructor <init>(Lcom/google/android/music/dial/WebSocketMultiplexer;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer$1;->this$0:Lcom/google/android/music/dial/WebSocketMultiplexer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 116
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer$1;->this$0:Lcom/google/android/music/dial/WebSocketMultiplexer;

    # getter for: Lcom/google/android/music/dial/WebSocketMultiplexer;->mStartupLatch:Ljava/util/concurrent/CountDownLatch;
    invoke-static {v1}, Lcom/google/android/music/dial/WebSocketMultiplexer;->access$000(Lcom/google/android/music/dial/WebSocketMultiplexer;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 117
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer$1;->this$0:Lcom/google/android/music/dial/WebSocketMultiplexer;

    # invokes: Lcom/google/android/music/dial/WebSocketMultiplexer;->selectorLoop()V
    invoke-static {v1}, Lcom/google/android/music/dial/WebSocketMultiplexer;->access$100(Lcom/google/android/music/dial/WebSocketMultiplexer;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    const-string v1, "WebSocketMultiplexer"

    const-string v2, "Selector loop thread exiting"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer$1;->this$0:Lcom/google/android/music/dial/WebSocketMultiplexer;

    # invokes: Lcom/google/android/music/dial/WebSocketMultiplexer;->disposeWakeLock()V
    invoke-static {v1}, Lcom/google/android/music/dial/WebSocketMultiplexer;->access$400(Lcom/google/android/music/dial/WebSocketMultiplexer;)V

    .line 125
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer$1;->this$0:Lcom/google/android/music/dial/WebSocketMultiplexer;

    # setter for: Lcom/google/android/music/dial/WebSocketMultiplexer;->mThread:Ljava/lang/Thread;
    invoke-static {v1, v4}, Lcom/google/android/music/dial/WebSocketMultiplexer;->access$502(Lcom/google/android/music/dial/WebSocketMultiplexer;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 127
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "t":Ljava/lang/Throwable;
    :try_start_1
    const-string v1, "WebSocketMultiplexer"

    const-string v2, "Unexpected throwable in selector loop"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 120
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer$1;->this$0:Lcom/google/android/music/dial/WebSocketMultiplexer;

    # setter for: Lcom/google/android/music/dial/WebSocketMultiplexer;->mSelectorThrowable:Ljava/lang/Throwable;
    invoke-static {v1, v0}, Lcom/google/android/music/dial/WebSocketMultiplexer;->access$202(Lcom/google/android/music/dial/WebSocketMultiplexer;Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 121
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer$1;->this$0:Lcom/google/android/music/dial/WebSocketMultiplexer;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/music/dial/WebSocketMultiplexer;->mAborted:Z
    invoke-static {v1, v2}, Lcom/google/android/music/dial/WebSocketMultiplexer;->access$302(Lcom/google/android/music/dial/WebSocketMultiplexer;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123
    const-string v1, "WebSocketMultiplexer"

    const-string v2, "Selector loop thread exiting"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer$1;->this$0:Lcom/google/android/music/dial/WebSocketMultiplexer;

    # invokes: Lcom/google/android/music/dial/WebSocketMultiplexer;->disposeWakeLock()V
    invoke-static {v1}, Lcom/google/android/music/dial/WebSocketMultiplexer;->access$400(Lcom/google/android/music/dial/WebSocketMultiplexer;)V

    .line 125
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer$1;->this$0:Lcom/google/android/music/dial/WebSocketMultiplexer;

    # setter for: Lcom/google/android/music/dial/WebSocketMultiplexer;->mThread:Ljava/lang/Thread;
    invoke-static {v1, v4}, Lcom/google/android/music/dial/WebSocketMultiplexer;->access$502(Lcom/google/android/music/dial/WebSocketMultiplexer;Ljava/lang/Thread;)Ljava/lang/Thread;

    goto :goto_0

    .line 123
    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    const-string v2, "WebSocketMultiplexer"

    const-string v3, "Selector loop thread exiting"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocketMultiplexer$1;->this$0:Lcom/google/android/music/dial/WebSocketMultiplexer;

    # invokes: Lcom/google/android/music/dial/WebSocketMultiplexer;->disposeWakeLock()V
    invoke-static {v2}, Lcom/google/android/music/dial/WebSocketMultiplexer;->access$400(Lcom/google/android/music/dial/WebSocketMultiplexer;)V

    .line 125
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocketMultiplexer$1;->this$0:Lcom/google/android/music/dial/WebSocketMultiplexer;

    # setter for: Lcom/google/android/music/dial/WebSocketMultiplexer;->mThread:Ljava/lang/Thread;
    invoke-static {v2, v4}, Lcom/google/android/music/dial/WebSocketMultiplexer;->access$502(Lcom/google/android/music/dial/WebSocketMultiplexer;Ljava/lang/Thread;)Ljava/lang/Thread;

    throw v1
.end method
