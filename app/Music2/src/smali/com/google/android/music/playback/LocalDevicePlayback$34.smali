.class Lcom/google/android/music/playback/LocalDevicePlayback$34;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->seek(J)J
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

.field final synthetic val$remoteSeekPos:J


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;J)V
    .locals 0

    .prologue
    .line 4712
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$34;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iput-wide p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$34;->val$remoteSeekPos:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 4715
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$34;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$34;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1100(Lcom/google/android/music/playback/LocalDevicePlayback;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/store/Store;->getCloudQueueItemIdForQueueItem(J)Ljava/lang/String;

    move-result-object v0

    .line 4717
    .local v0, "cloudQueueItemId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4718
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$34;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$200(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$34;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mCloudQueueVersion:I
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$10300(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$34;->val$remoteSeekPos:J

    invoke-interface {v1, v2, v0, v4, v5}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->seek(Ljava/lang/String;Ljava/lang/String;J)V

    .line 4723
    :goto_0
    return-void

    .line 4721
    :cond_0
    const-string v1, "LocalDevicePlayback"

    const-string v2, "Error getting current cloud queue item ID"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
