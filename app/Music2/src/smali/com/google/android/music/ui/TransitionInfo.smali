.class public abstract Lcom/google/android/music/ui/TransitionInfo;
.super Ljava/lang/Object;
.source "TransitionInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field private mSharedElementName:Ljava/lang/String;

.field private mTransitionType:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/ui/TransitionInfo;->mTransitionType:I

    .line 27
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/TransitionInfo;->setTransitionType(I)V

    .line 28
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/ui/TransitionInfo;->mTransitionType:I

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TransitionInfo;->mTransitionType:I

    .line 52
    return-void
.end method


# virtual methods
.method public getSharedElementName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/music/ui/TransitionInfo;->mSharedElementName:Ljava/lang/String;

    return-object v0
.end method

.method public getTransitionType()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/music/ui/TransitionInfo;->mTransitionType:I

    return v0
.end method

.method public setSharedElementName(Ljava/lang/String;)V
    .locals 0
    .param p1, "sharedElementName"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/music/ui/TransitionInfo;->mSharedElementName:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setTransitionType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/google/android/music/ui/TransitionInfo;->mTransitionType:I

    .line 36
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/music/ui/TransitionInfo;->mTransitionType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 48
    return-void
.end method
