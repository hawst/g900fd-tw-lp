.class public Lcom/google/android/music/ui/songza/TopSituationFragment;
.super Lcom/google/android/music/ui/BaseFragment;
.source "TopSituationFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/songza/TopSituationFragment$1;,
        Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;,
        Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;,
        Lcom/google/android/music/ui/songza/TopSituationFragment$ListConfigurator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/music/ui/BaseFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final SITUATION_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAdapter:Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mPlayHeaderListLayout:Lcom/google/android/music/ui/MusicPlayHeaderListLayout;

.field private mRootView:Landroid/view/View;

.field private mSituationArtView:Lcom/google/android/music/art/SimpleArtView;

.field private mSituationTitleView:Landroid/widget/TextView;

.field private mTopSituationArtUrl:Ljava/lang/String;

.field private mTopSituationDescription:Ljava/lang/String;

.field private mTopSituationId:Ljava/lang/String;

.field private mTopSituationTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "situation_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "situation_title"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/songza/TopSituationFragment;->SITUATION_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseFragment;-><init>()V

    .line 59
    iput-object v0, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationId:Ljava/lang/String;

    .line 60
    iput-object v0, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationTitle:Ljava/lang/String;

    .line 61
    iput-object v0, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationDescription:Ljava/lang/String;

    .line 62
    iput-object v0, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationArtUrl:Ljava/lang/String;

    .line 232
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/songza/TopSituationFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/songza/TopSituationFragment;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationDescription:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/songza/TopSituationFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/songza/TopSituationFragment;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationArtUrl:Ljava/lang/String;

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/ui/songza/TopSituationFragment;
    .locals 3
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "url"    # Ljava/lang/String;

    .prologue
    .line 82
    new-instance v1, Lcom/google/android/music/ui/songza/TopSituationFragment;

    invoke-direct {v1}, Lcom/google/android/music/ui/songza/TopSituationFragment;-><init>()V

    .line 83
    .local v1, "fragment":Lcom/google/android/music/ui/songza/TopSituationFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 84
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v2, "title"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v2, "description"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v2, "url"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/songza/TopSituationFragment;->setArguments(Landroid/os/Bundle;)V

    .line 89
    return-object v1
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 141
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/TopSituationFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    .line 143
    .local v0, "lm":Landroid/support/v4/app/LoaderManager;
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 144
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1, "i"    # I
    .param p2, "bundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 148
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/TopSituationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationId:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/music/store/MusicContent$Situations;->getSubSituationsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/songza/TopSituationFragment;->SITUATION_PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 95
    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/TopSituationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 96
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 97
    const-string v3, "id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationId:Ljava/lang/String;

    .line 98
    const-string v3, "title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationTitle:Ljava/lang/String;

    .line 99
    const-string v3, "description"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationDescription:Ljava/lang/String;

    .line 100
    const-string v3, "url"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationArtUrl:Ljava/lang/String;

    .line 103
    :cond_0
    const v3, 0x7f0400dd

    invoke-virtual {p1, v3, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mRootView:Landroid/view/View;

    .line 104
    iget-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mRootView:Landroid/view/View;

    const v4, 0x7f0e019d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;

    iput-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mPlayHeaderListLayout:Lcom/google/android/music/ui/MusicPlayHeaderListLayout;

    .line 106
    iget-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mPlayHeaderListLayout:Lcom/google/android/music/ui/MusicPlayHeaderListLayout;

    new-instance v4, Lcom/google/android/music/ui/songza/TopSituationFragment$ListConfigurator;

    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/TopSituationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/music/ui/songza/TopSituationFragment$ListConfigurator;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/TopSituationFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/TopSituationFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->configureForMusic(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 109
    iget-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mRootView:Landroid/view/View;

    const v4, 0x7f0e026c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mListView:Landroid/widget/ListView;

    .line 110
    iget-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, v8}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 112
    iget-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mRootView:Landroid/view/View;

    const v4, 0x7f0e0265

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/art/SimpleArtView;

    iput-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mSituationArtView:Lcom/google/android/music/art/SimpleArtView;

    .line 113
    iget-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationArtUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 114
    new-instance v2, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    iget-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationArtUrl:Ljava/lang/String;

    invoke-direct {v2, v3, v7}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;-><init>(Ljava/lang/String;Z)V

    .line 115
    .local v2, "url":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    new-instance v1, Lcom/google/android/music/art/SingleUrlArtDescriptor;

    sget-object v3, Lcom/google/android/music/art/ArtType;->SONGZA_SITUATION:Lcom/google/android/music/art/ArtType;

    const/4 v4, 0x3

    const/high16 v5, 0x3f100000    # 0.5625f

    invoke-direct {v1, v3, v4, v5, v2}, Lcom/google/android/music/art/SingleUrlArtDescriptor;-><init>(Lcom/google/android/music/art/ArtType;IFLcom/google/android/music/art/ArtResolver2$ArtUrl;)V

    .line 117
    .local v1, "artDescriptor":Lcom/google/android/music/art/SingleUrlArtDescriptor;
    iget-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mSituationArtView:Lcom/google/android/music/art/SimpleArtView;

    invoke-virtual {v3, v1, v7}, Lcom/google/android/music/art/SimpleArtView;->bind(Lcom/google/android/music/art/ArtDescriptor;Z)V

    .line 122
    .end local v1    # "artDescriptor":Lcom/google/android/music/art/SingleUrlArtDescriptor;
    .end local v2    # "url":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    :goto_0
    iget-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mRootView:Landroid/view/View;

    const v4, 0x7f0e0014

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mSituationTitleView:Landroid/widget/TextView;

    .line 123
    iget-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mSituationTitleView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationTitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    new-instance v3, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;

    invoke-direct {v3, p0, v7}, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;-><init>(Lcom/google/android/music/ui/songza/TopSituationFragment;Z)V

    iput-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mAdapter:Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;

    .line 126
    iget-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mListView:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mAdapter:Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 128
    iget-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mRootView:Landroid/view/View;

    return-object v3

    .line 119
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mSituationArtView:Lcom/google/android/music/art/SimpleArtView;

    const v4, 0x7f02017c

    invoke-virtual {v3, v4}, Lcom/google/android/music/art/SimpleArtView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mPlayHeaderListLayout:Lcom/google/android/music/ui/MusicPlayHeaderListLayout;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mPlayHeaderListLayout:Lcom/google/android/music/ui/MusicPlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->unregisterSharedPreferenceChangeListener()V

    .line 136
    :cond_0
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onDestroyView()V

    .line 137
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 155
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mAdapter:Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->swapCursor(Landroid/database/Cursor;)V

    .line 156
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 43
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/songza/TopSituationFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 160
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/music/ui/songza/TopSituationFragment;->mAdapter:Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->swapCursor(Landroid/database/Cursor;)V

    .line 161
    return-void
.end method
