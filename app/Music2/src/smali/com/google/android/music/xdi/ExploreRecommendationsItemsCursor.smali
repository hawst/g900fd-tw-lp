.class Lcom/google/android/music/xdi/ExploreRecommendationsItemsCursor;
.super Lcom/google/android/music/xdi/XdiCursor;
.source "ExploreRecommendationsItemsCursor.java"


# instance fields
.field private final mGroupType:I

.field private final mImageHeight:I

.field private final mImageWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;JI)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "groupId"    # J
    .param p5, "groupType"    # I

    .prologue
    .line 30
    invoke-static {p1, p3, p4, p5}, Lcom/google/android/music/xdi/ExploreRecommendationsItemsCursor;->getCursorForHeader(Landroid/content/Context;JI)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/xdi/XdiCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/database/Cursor;)V

    .line 31
    iput p5, p0, Lcom/google/android/music/xdi/ExploreRecommendationsItemsCursor;->mGroupType:I

    .line 32
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/ExploreRecommendationsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 33
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/ExploreRecommendationsItemsCursor;->mImageWidth:I

    .line 34
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/ExploreRecommendationsItemsCursor;->mImageHeight:I

    .line 35
    return-void
.end method

.method private static getCursorForHeader(Landroid/content/Context;JI)Landroid/database/Cursor;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "groupId"    # J
    .param p3, "groupType"    # I

    .prologue
    const/4 v3, 0x0

    .line 38
    invoke-static {p3}, Lcom/google/android/music/xdi/XdiUtils;->getClusterProjection(I)[Ljava/lang/String;

    move-result-object v2

    .line 40
    .local v2, "clusterProjection":[Ljava/lang/String;
    if-nez v2, :cond_0

    .line 41
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 44
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$Explore;->getRecommendedUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected extractDataForCurrentRow([Ljava/lang/Object;)Z
    .locals 7
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreRecommendationsItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreRecommendationsItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/xdi/ExploreRecommendationsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    iget v4, p0, Lcom/google/android/music/xdi/ExploreRecommendationsItemsCursor;->mGroupType:I

    iget v5, p0, Lcom/google/android/music/xdi/ExploreRecommendationsItemsCursor;->mImageWidth:I

    iget v6, p0, Lcom/google/android/music/xdi/ExploreRecommendationsItemsCursor;->mImageHeight:I

    move-object v3, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/music/xdi/XdiUtils;->extractDataForGroupType(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/xdi/ProjectionMap;[Ljava/lang/Object;III)Z

    move-result v0

    return v0
.end method
