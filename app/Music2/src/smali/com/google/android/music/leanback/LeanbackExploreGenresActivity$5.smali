.class Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackExploreGenresActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getNewReleasesAdapter(Ljava/lang/String;Ljava/lang/String;IJ)Landroid/support/v17/leanback/widget/CursorObjectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

.field final synthetic val$genreId:Ljava/lang/String;

.field final synthetic val$genreName:Ljava/lang/String;

.field final synthetic val$loaderId:I

.field final synthetic val$seed:J


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 1

    .prologue
    .line 264
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;->val$genreName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;->val$genreId:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;->val$loaderId:I

    iput-wide p5, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;->val$seed:J

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, 0x2

    const/4 v6, 0x1

    .line 267
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 268
    .local v8, "groupTitle":Ljava/lang/String;
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 270
    .local v7, "groupDescription":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    const v2, 0x7f0b036d

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;->val$genreName:Ljava/lang/String;

    aput-object v5, v3, v4

    aput-object v8, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;->val$genreId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/music/leanback/LeanbackUtils;->newExploreNewReleasesIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    .line 275
    .local v9, "intent":Landroid/content/Intent;
    new-instance v0, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v0}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v0, v8}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v10

    new-instance v0, Lcom/google/android/music/leanback/bitmap/ExploreNewReleasesBitmapGettersGetter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    invoke-virtual {v2}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    iget v3, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;->val$loaderId:I

    iget-wide v4, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;->val$seed:J

    iget-object v6, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;->val$genreId:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/leanback/bitmap/ExploreNewReleasesBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLjava/lang/String;)V

    invoke-virtual {v10, v0}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v0

    return-object v0
.end method
