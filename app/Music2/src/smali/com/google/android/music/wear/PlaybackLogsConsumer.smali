.class final Lcom/google/android/music/wear/PlaybackLogsConsumer;
.super Ljava/lang/Object;
.source "PlaybackLogsConsumer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/wear/PlaybackLogsConsumer$1;,
        Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;,
        Lcom/google/android/music/wear/PlaybackLogsConsumer$ConsumePlaybackLogsRunnable;
    }
.end annotation


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private final mClock:Lcom/google/android/music/wear/util/Clock;

.field private final mConsumePlaybackLogsRunnable:Lcom/google/android/music/wear/PlaybackLogsConsumer$ConsumePlaybackLogsRunnable;

.field private final mDataApiWrapper:Lcom/google/android/music/wear/util/DataApiWrapper;

.field private final mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;


# direct methods
.method constructor <init>(Lcom/google/android/music/wear/util/DataApiWrapper;Lcom/google/android/music/wear/WearMusicDataProvider;Ljava/util/concurrent/Executor;Lcom/google/android/music/wear/util/Clock;)V
    .locals 2
    .param p1, "dataApiWrapper"    # Lcom/google/android/music/wear/util/DataApiWrapper;
    .param p2, "musicDataProvider"    # Lcom/google/android/music/wear/WearMusicDataProvider;
    .param p3, "backgroundExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "clock"    # Lcom/google/android/music/wear/util/Clock;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Lcom/google/android/music/wear/PlaybackLogsConsumer$ConsumePlaybackLogsRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/wear/PlaybackLogsConsumer$ConsumePlaybackLogsRunnable;-><init>(Lcom/google/android/music/wear/PlaybackLogsConsumer;Lcom/google/android/music/wear/PlaybackLogsConsumer$1;)V

    iput-object v0, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer;->mConsumePlaybackLogsRunnable:Lcom/google/android/music/wear/PlaybackLogsConsumer$ConsumePlaybackLogsRunnable;

    .line 46
    const-string v0, "dataApiWrapper cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/wear/util/DataApiWrapper;

    iput-object v0, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer;->mDataApiWrapper:Lcom/google/android/music/wear/util/DataApiWrapper;

    .line 48
    const-string v0, "musicDataProvider cannot be null"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/wear/WearMusicDataProvider;

    iput-object v0, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer;->mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

    .line 50
    const-string v0, "backgroundExecutor cannot be null"

    invoke-static {p3, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    .line 52
    const-string v0, "clock cannot be null"

    invoke-static {p4, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/wear/util/Clock;

    iput-object v0, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer;->mClock:Lcom/google/android/music/wear/util/Clock;

    .line 53
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/music/wear/PlaybackLogsConsumer;Ljava/util/Map;Lcom/google/android/gms/wearable/DataMap;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/wear/PlaybackLogsConsumer;
    .param p1, "x1"    # Ljava/util/Map;
    .param p2, "x2"    # Lcom/google/android/gms/wearable/DataMap;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/wear/PlaybackLogsConsumer;->reportEvent(Ljava/util/Map;Lcom/google/android/gms/wearable/DataMap;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/wear/PlaybackLogsConsumer;)Lcom/google/android/music/wear/util/DataApiWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/wear/PlaybackLogsConsumer;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer;->mDataApiWrapper:Lcom/google/android/music/wear/util/DataApiWrapper;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/wear/PlaybackLogsConsumer;)Lcom/google/android/music/wear/WearMusicDataProvider;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/wear/PlaybackLogsConsumer;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer;->mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

    return-object v0
.end method

.method static synthetic access$800(Ljava/util/List;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 27
    invoke-static {p0}, Lcom/google/android/music/wear/PlaybackLogsConsumer;->buildCollectionIdToMediaListMap(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Landroid/net/Uri;)Z
    .locals 1
    .param p0, "x0"    # Landroid/net/Uri;

    .prologue
    .line 27
    invoke-static {p0}, Lcom/google/android/music/wear/PlaybackLogsConsumer;->isPlaybackLogEvent(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method private static buildCollectionIdToMediaListMap(Ljava/util/List;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/wear/WearMediaList;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/music/wear/WearMediaList;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    .local p0, "lists":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/wear/WearMediaList;>;"
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    .line 125
    .local v2, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/music/wear/WearMediaList;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/wear/WearMediaList;

    .line 126
    .local v1, "list":Lcom/google/android/music/wear/WearMediaList;
    invoke-static {v1}, Lcom/google/android/music/wear/WearMetadataSyncController;->getCollectionId(Lcom/google/android/music/wear/WearMediaList;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 128
    .end local v1    # "list":Lcom/google/android/music/wear/WearMediaList;
    :cond_0
    return-object v2
.end method

.method private isEventTimestampSane(J)Z
    .locals 7
    .param p1, "eventTimeMs"    # J

    .prologue
    .line 102
    iget-object v2, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer;->mClock:Lcom/google/android/music/wear/util/Clock;

    invoke-interface {v2}, Lcom/google/android/music/wear/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    .line 103
    .local v0, "currentTimeMs":J
    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x2

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sub-long v2, v0, v2

    cmp-long v2, p1, v2

    if-ltz v2, :cond_0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    add-long/2addr v2, v0

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isPlaybackLogEvent(Landroid/net/Uri;)Z
    .locals 4
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 111
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    .line 112
    .local v0, "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 115
    :goto_0
    return v1

    :cond_0
    const-string v2, "play_logs"

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method private reportEvent(Ljava/util/Map;Lcom/google/android/gms/wearable/DataMap;)V
    .locals 9
    .param p2, "dataMap"    # Lcom/google/android/gms/wearable/DataMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/music/wear/WearMediaList;",
            ">;",
            "Lcom/google/android/gms/wearable/DataMap;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "collectionIdToMediaListMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/music/wear/WearMediaList;>;"
    const/4 v8, 0x3

    .line 69
    invoke-static {p2}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->fromDataMap(Lcom/google/android/gms/wearable/DataMap;)Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;

    move-result-object v0

    .line 70
    .local v0, "event":Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;
    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->type:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$100(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;->COUNT_PLAYBACK:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    if-ne v1, v2, :cond_1

    .line 71
    iget-object v1, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer;->mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->trackId:J
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$200(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)J

    move-result-wide v2

    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->eventTimeMs:J
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$300(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)J

    move-result-wide v6

    invoke-interface {v1, v2, v3, v6, v7}, Lcom/google/android/music/wear/WearMusicDataProvider;->markSongPlayed(JJ)V

    .line 72
    const-string v1, "PlaybackLogsConsumer"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    const-string v1, "PlaybackLogsConsumer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Recorded a playback of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->trackId:J
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$200(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v3

    new-instance v5, Ljava/util/Date;

    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->eventTimeMs:J
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$300(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->type:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$100(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;->ANALYTICS_PLAYBACK:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    if-ne v1, v2, :cond_0

    .line 78
    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->eventTimeMs:J
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$300(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/music/wear/PlaybackLogsConsumer;->isEventTimestampSane(J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 79
    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->collectionId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$400(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/wear/WearMediaList;

    .line 80
    .local v4, "list":Lcom/google/android/music/wear/WearMediaList;
    iget-object v1, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer;->mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->trackId:J
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$200(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)J

    move-result-wide v2

    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->isExplicit:Z
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$500(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)Z

    move-result v5

    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->eventTimeMs:J
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$300(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)J

    move-result-wide v6

    invoke-interface/range {v1 .. v7}, Lcom/google/android/music/wear/WearMusicDataProvider;->logPlayEvent(JLcom/google/android/music/wear/WearMediaList;ZJ)V

    .line 82
    const-string v1, "PlaybackLogsConsumer"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    const-string v1, "PlaybackLogsConsumer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Logged play event of track "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->trackId:J
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$200(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (played as part of list: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", explicit: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->isExplicit:Z
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$500(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v3

    new-instance v5, Ljava/util/Date;

    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->eventTimeMs:J
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$300(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 89
    .end local v4    # "list":Lcom/google/android/music/wear/WearMediaList;
    :cond_2
    const-string v1, "PlaybackLogsConsumer"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    const-string v1, "PlaybackLogsConsumer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignoring stale play event which happened at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v3

    new-instance v5, Ljava/util/Date;

    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->eventTimeMs:J
    invoke-static {v0}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->access$300(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method


# virtual methods
.method public consumePlaybackLogs()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer;->mConsumePlaybackLogsRunnable:Lcom/google/android/music/wear/PlaybackLogsConsumer$ConsumePlaybackLogsRunnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 61
    return-void
.end method
