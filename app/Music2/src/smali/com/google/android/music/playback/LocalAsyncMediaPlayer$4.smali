.class Lcom/google/android/music/playback/LocalAsyncMediaPlayer$4;
.super Ljava/lang/Object;
.source "LocalAsyncMediaPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V
    .locals 0

    .prologue
    .line 936
    iput-object p1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$4;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 938
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$4;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$4;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$000(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/playback/CompatMediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/playback/CompatMediaPlayer;->getCurrentPosition()I

    move-result v1

    int-to-long v2, v1

    # setter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J
    invoke-static {v0, v2, v3}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$902(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;J)J

    .line 939
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$4;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$300(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/playback/StopWatch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/playback/StopWatch;->reset()V

    .line 940
    return-void
.end method
