.class public Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog;
.super Lcom/google/android/music/ui/dialogs/TwoButtonsDialog;
.source "DeleteDocumentDialog.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/music/ui/dialogs/TwoButtonsDialog;-><init>()V

    return-void
.end method


# virtual methods
.method public createArgs(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/os/Bundle;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 25
    invoke-virtual {p2}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v5, v8, :cond_0

    invoke-virtual {p2}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v8, :cond_1

    :cond_0
    move v5, v7

    :goto_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "invalid doc type: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 28
    invoke-virtual {p2}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 30
    .local v4, "title":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v8, :cond_2

    invoke-virtual {p2}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v5

    const/16 v8, 0x47

    if-ne v5, v8, :cond_2

    .line 32
    const v5, 0x7f0b00b6

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v4, v7, v6

    invoke-virtual {p1, v5, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 37
    .local v1, "message":Ljava/lang/String;
    :goto_1
    const v5, 0x104000a

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 38
    .local v2, "okLabel":Ljava/lang/String;
    const/high16 v5, 0x1040000

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "cancelLabel":Ljava/lang/String;
    invoke-super {p0, v1, v2, v0}, Lcom/google/android/music/ui/dialogs/TwoButtonsDialog;->createArgs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 41
    .local v3, "result":Landroid/os/Bundle;
    const-string v5, "deleteType"

    invoke-virtual {p2}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 42
    const-string v5, "deleteId"

    invoke-virtual {p2}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v6

    invoke-virtual {v3, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 43
    return-object v3

    .end local v0    # "cancelLabel":Ljava/lang/String;
    .end local v1    # "message":Ljava/lang/String;
    .end local v2    # "okLabel":Ljava/lang/String;
    .end local v3    # "result":Landroid/os/Bundle;
    .end local v4    # "title":Ljava/lang/String;
    :cond_1
    move v5, v6

    .line 25
    goto :goto_0

    .line 35
    .restart local v4    # "title":Ljava/lang/String;
    :cond_2
    const v5, 0x7f0b01de

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v4, v7, v6

    invoke-virtual {p1, v5, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "message":Ljava/lang/String;
    goto :goto_1
.end method

.method protected onOkClicked()V
    .locals 7

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 49
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "deleteType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 50
    .local v3, "docType":I
    const-string v1, "deleteId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 51
    .local v4, "itemId":J
    invoke-virtual {p0}, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 53
    .local v6, "resolver":Landroid/content/ContentResolver;
    new-instance v1, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog$1;

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog$1;-><init>(Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog;IJLandroid/content/ContentResolver;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 67
    return-void
.end method
