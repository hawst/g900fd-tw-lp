.class public Lcom/google/android/music/utils/PlaybackUtils;
.super Ljava/lang/Object;
.source "PlaybackUtils.java"


# direct methods
.method public static isPlayed(Landroid/content/Context;JJ)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "duration"    # J
    .param p3, "position"    # J

    .prologue
    .line 23
    const/4 v0, 0x0

    .line 24
    .local v0, "isPlayed":Z
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-lez v1, :cond_1

    .line 25
    long-to-float v1, p3

    long-to-float v2, p1

    const v3, 0x3f266666    # 0.65f

    mul-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-gez v1, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_mark_as_played_seconds"

    const-wide/16 v4, 0xa

    invoke-static {v1, v2, v4, v5}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    cmp-long v1, p3, v2

    if-ltz v1, :cond_1

    .line 30
    :cond_0
    const/4 v0, 0x1

    .line 33
    :cond_1
    return v0
.end method
