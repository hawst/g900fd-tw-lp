.class public Lcom/google/android/music/ui/PlayTabStrip;
.super Landroid/widget/LinearLayout;
.source "PlayTabStrip.java"


# instance fields
.field private final mFullUnderlinePaint:Landroid/graphics/Paint;

.field private final mFullUnderlineThickness:I

.field private mIndexForSelection:I

.field private final mSelectedUnderlinePaint:Landroid/graphics/Paint;

.field private final mSelectedUnderlineThickness:I

.field private mSelectionOffset:F

.field private final mSideSeparatorHeight:I

.field private final mSideSeparatorPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/PlayTabStrip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/PlayTabStrip;->setWillNotDraw(Z)V

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 45
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0f000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/ui/PlayTabStrip;->mFullUnderlineThickness:I

    .line 47
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/ui/PlayTabStrip;->mFullUnderlinePaint:Landroid/graphics/Paint;

    .line 48
    iget-object v1, p0, Lcom/google/android/music/ui/PlayTabStrip;->mFullUnderlinePaint:Landroid/graphics/Paint;

    const v2, 0x7f0c0025

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 50
    const v1, 0x7f0f00f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/ui/PlayTabStrip;->mSelectedUnderlineThickness:I

    .line 52
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/ui/PlayTabStrip;->mSelectedUnderlinePaint:Landroid/graphics/Paint;

    .line 54
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/ui/PlayTabStrip;->mSideSeparatorPaint:Landroid/graphics/Paint;

    .line 55
    iget-object v1, p0, Lcom/google/android/music/ui/PlayTabStrip;->mSideSeparatorPaint:Landroid/graphics/Paint;

    const v2, 0x7f0c0026

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 56
    iget-object v1, p0, Lcom/google/android/music/ui/PlayTabStrip;->mSideSeparatorPaint:Landroid/graphics/Paint;

    const v2, 0x7f0f00fb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 58
    const v1, 0x7f0f00fa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/ui/PlayTabStrip;->mSideSeparatorHeight:I

    .line 60
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 21
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 92
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/PlayTabStrip;->getHeight()I

    move-result v11

    .line 93
    .local v11, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/PlayTabStrip;->getChildCount()I

    move-result v8

    .line 96
    .local v8, "childCount":I
    if-lez v8, :cond_1

    .line 97
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/music/ui/PlayTabStrip;->mIndexForSelection:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v18

    .line 98
    .local v18, "selectedTitle":Landroid/view/View;
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getLeft()I

    move-result v16

    .line 99
    .local v16, "selectedLeft":I
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getRight()I

    move-result v17

    .line 100
    .local v17, "selectedRight":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/music/ui/PlayTabStrip;->mSelectionOffset:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/music/ui/PlayTabStrip;->mIndexForSelection:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/PlayTabStrip;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 103
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/music/ui/PlayTabStrip;->mIndexForSelection:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v15

    .line 104
    .local v15, "nextTitle":Landroid/view/View;
    invoke-virtual {v15}, Landroid/view/View;->getLeft()I

    move-result v13

    .line 105
    .local v13, "nextLeft":I
    invoke-virtual {v15}, Landroid/view/View;->getRight()I

    move-result v14

    .line 107
    .local v14, "nextRight":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/music/ui/PlayTabStrip;->mSelectionOffset:F

    int-to-float v2, v13

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/music/ui/PlayTabStrip;->mSelectionOffset:F

    sub-float/2addr v2, v3

    move/from16 v0, v16

    int-to-float v3, v0

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v0, v1

    move/from16 v16, v0

    .line 109
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/music/ui/PlayTabStrip;->mSelectionOffset:F

    int-to-float v2, v14

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/music/ui/PlayTabStrip;->mSelectionOffset:F

    sub-float/2addr v2, v3

    move/from16 v0, v17

    int-to-float v3, v0

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v0, v1

    move/from16 v17, v0

    .line 113
    .end local v13    # "nextLeft":I
    .end local v14    # "nextRight":I
    .end local v15    # "nextTitle":Landroid/view/View;
    :cond_0
    move/from16 v0, v16

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/music/ui/PlayTabStrip;->mSelectedUnderlineThickness:I

    sub-int v1, v11, v1

    int-to-float v3, v1

    move/from16 v0, v17

    int-to-float v4, v0

    int-to-float v5, v11

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/ui/PlayTabStrip;->mSelectedUnderlinePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 118
    .end local v16    # "selectedLeft":I
    .end local v17    # "selectedRight":I
    .end local v18    # "selectedTitle":Landroid/view/View;
    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/music/ui/PlayTabStrip;->mFullUnderlineThickness:I

    sub-int v1, v11, v1

    int-to-float v3, v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/PlayTabStrip;->getWidth()I

    move-result v1

    int-to-float v4, v1

    int-to-float v5, v11

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/ui/PlayTabStrip;->mFullUnderlinePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 122
    const/4 v12, 0x1

    .local v12, "i":I
    :goto_0
    if-ge v12, v8, :cond_2

    .line 123
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/android/music/ui/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 124
    .local v7, "child":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getPaddingTop()I

    move-result v10

    .line 125
    .local v10, "childPaddingTop":I
    invoke-virtual {v7}, Landroid/view/View;->getPaddingBottom()I

    move-result v9

    .line 126
    .local v9, "childPaddingBottom":I
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v1

    sub-int/2addr v1, v10

    sub-int/2addr v1, v9

    div-int/lit8 v1, v1, 0x2

    add-int v19, v1, v10

    .line 128
    .local v19, "separatorCenter":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/music/ui/PlayTabStrip;->mSideSeparatorHeight:I

    div-int/lit8 v1, v1, 0x2

    sub-int v20, v19, v1

    .line 129
    .local v20, "separatorTop":I
    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v2, v1

    move/from16 v0, v20

    int-to-float v3, v0

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v4, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/music/ui/PlayTabStrip;->mSideSeparatorHeight:I

    add-int v1, v1, v20

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/ui/PlayTabStrip;->mSideSeparatorPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 122
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 132
    .end local v7    # "child":Landroid/view/View;
    .end local v9    # "childPaddingBottom":I
    .end local v10    # "childPaddingTop":I
    .end local v19    # "separatorCenter":I
    .end local v20    # "separatorTop":I
    :cond_2
    return-void
.end method

.method onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/google/android/music/ui/PlayTabStrip;->mIndexForSelection:I

    .line 76
    iput p2, p0, Lcom/google/android/music/ui/PlayTabStrip;->mSelectionOffset:F

    .line 77
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlayTabStrip;->invalidate()V

    .line 78
    return-void
.end method

.method onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/google/android/music/ui/PlayTabStrip;->mIndexForSelection:I

    .line 86
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/ui/PlayTabStrip;->mSelectionOffset:F

    .line 87
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlayTabStrip;->invalidate()V

    .line 88
    return-void
.end method

.method public setSelectedIndicatorColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/PlayTabStrip;->mSelectedUnderlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 67
    return-void
.end method
