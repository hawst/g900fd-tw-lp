.class Lcom/google/android/music/xdi/ExploreHeadersCursor;
.super Landroid/database/MatrixCursor;
.source "ExploreHeadersCursor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mItemHeight:I

.field private final mItemWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 30
    iput-object p1, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mContext:Landroid/content/Context;

    .line 31
    new-instance v4, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v4, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v4, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 32
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthDp(Landroid/content/Context;)I

    move-result v4

    iput v4, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mItemWidth:I

    .line 33
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightDp(Landroid/content/Context;)I

    move-result v4

    iput v4, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mItemHeight:I

    .line 35
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v4, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_EXPLORE_HEADER_NAMES:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 36
    sget-object v4, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_EXPLORE_HEADER_IDS:[I

    aget v4, v4, v0

    int-to-long v2, v4

    .line 37
    .local v2, "id":J
    iget-object v4, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_EXPLORE_HEADER_NAMES:[I

    aget v5, v5, v0

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 38
    .local v1, "name":Ljava/lang/String;
    invoke-direct {p0, v2, v3, v1}, Lcom/google/android/music/xdi/ExploreHeadersCursor;->getColumnValuesForBrowseHeader(JLjava/lang/String;)[Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/music/xdi/ExploreHeadersCursor;->addRow([Ljava/lang/Object;)V

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 40
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "id":J
    :cond_0
    return-void
.end method

.method private getColumnValuesForBrowseHeader(JLjava/lang/String;)[Ljava/lang/Object;
    .locals 7
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 43
    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v1}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/Object;

    .line 45
    .local v0, "values":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 46
    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "name"

    invoke-virtual {v1, v0, v2, p3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 47
    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "display_name"

    invoke-virtual {v1, v0, v2, p3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 48
    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "background_image_uri"

    invoke-virtual {v1, v0, v2, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 49
    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "bg_image_uri"

    invoke-virtual {v1, v0, v2, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 50
    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "icon_uri"

    invoke-virtual {v1, v0, v2, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 51
    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "default_item_width"

    iget v3, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mItemWidth:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 52
    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "default_item_height"

    iget v3, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mItemHeight:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 53
    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "color_hint"

    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00a6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 55
    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "badge_uri"

    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultBadgeUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 57
    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "items_uri"

    invoke-virtual {v1, v0, v2, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 59
    return-object v0
.end method
