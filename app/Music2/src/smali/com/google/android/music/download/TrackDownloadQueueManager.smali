.class Lcom/google/android/music/download/TrackDownloadQueueManager;
.super Lcom/google/android/music/download/BaseDownloadQueueManager;
.source "TrackDownloadQueueManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/music/download/BaseDownloadQueueManager",
        "<",
        "Lcom/google/android/music/download/TrackDownloadRequest;",
        "Lcom/google/android/music/download/TrackDownloadTask;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/google/android/music/download/BaseDownloadQueueManager;-><init>(Landroid/content/Context;)V

    .line 11
    return-void
.end method


# virtual methods
.method protected bridge synthetic createDownloadProgress(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)Lcom/google/android/music/download/DownloadProgress;
    .locals 1
    .param p1, "x0"    # Lcom/google/android/music/download/DownloadRequest;
    .param p2, "x1"    # Lcom/google/android/music/download/DownloadState;

    .prologue
    .line 7
    check-cast p1, Lcom/google/android/music/download/TrackDownloadRequest;

    .end local p1    # "x0":Lcom/google/android/music/download/DownloadRequest;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/download/TrackDownloadQueueManager;->createDownloadProgress(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/DownloadState;)Lcom/google/android/music/download/DownloadProgress;

    move-result-object v0

    return-object v0
.end method

.method protected createDownloadProgress(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/DownloadState;)Lcom/google/android/music/download/DownloadProgress;
    .locals 4
    .param p1, "request"    # Lcom/google/android/music/download/TrackDownloadRequest;
    .param p2, "state"    # Lcom/google/android/music/download/DownloadState;

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-virtual {p1}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v2

    invoke-direct {v0, p1, p2, v2, v3}, Lcom/google/android/music/download/TrackDownloadProgress;-><init>(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/DownloadState;J)V

    return-object v0
.end method

.method protected bridge synthetic getDownloadTask(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/IDownloadProgressListener;)Lcom/google/android/music/download/DownloadTask;
    .locals 1
    .param p1, "x0"    # Lcom/google/android/music/download/DownloadRequest;
    .param p2, "x1"    # Lcom/google/android/music/download/IDownloadProgressListener;

    .prologue
    .line 7
    check-cast p1, Lcom/google/android/music/download/TrackDownloadRequest;

    .end local p1    # "x0":Lcom/google/android/music/download/DownloadRequest;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/download/TrackDownloadQueueManager;->getDownloadTask(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/IDownloadProgressListener;)Lcom/google/android/music/download/TrackDownloadTask;

    move-result-object v0

    return-object v0
.end method

.method protected getDownloadTask(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/IDownloadProgressListener;)Lcom/google/android/music/download/TrackDownloadTask;
    .locals 7
    .param p1, "request"    # Lcom/google/android/music/download/TrackDownloadRequest;
    .param p2, "progressListener"    # Lcom/google/android/music/download/IDownloadProgressListener;

    .prologue
    .line 16
    new-instance v0, Lcom/google/android/music/download/TrackDownloadTask;

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadQueueManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadQueueManager;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadQueueManager;->getHttpClient()Lcom/google/android/music/cloudclient/MusicHttpClient;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadQueueManager;->getNetworkMonitorServiceConnection()Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    move-result-object v6

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/download/TrackDownloadTask;-><init>(Landroid/content/Context;Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/IDownloadProgressListener;Lcom/google/android/music/preferences/MusicPreferences;Lcom/google/android/music/cloudclient/MusicHttpClient;Lcom/google/android/music/net/NetworkMonitorServiceConnection;)V

    return-object v0
.end method

.method protected getOwnerFromInt(I)Lcom/google/android/music/download/DownloadRequest$Owner;
    .locals 1
    .param p1, "ownerOrdinal"    # I

    .prologue
    .line 22
    invoke-static {}, Lcom/google/android/music/download/TrackOwner;->values()[Lcom/google/android/music/download/TrackOwner;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method
