.class public Lcom/google/android/music/art/ArtUtils;
.super Ljava/lang/Object;
.source "ArtUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/ArtUtils$1;,
        Lcom/google/android/music/art/ArtUtils$OverlayKey;,
        Lcom/google/android/music/art/ArtUtils$ArtHandler;
    }
.end annotation


# static fields
.field private static final sArtHandler:Lcom/google/android/music/art/ArtUtils$ArtHandler;

.field private static final sOverlayCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Lcom/google/android/music/art/ArtUtils$OverlayKey;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/music/art/ArtUtils$ArtHandler;

    invoke-direct {v0}, Lcom/google/android/music/art/ArtUtils$ArtHandler;-><init>()V

    sput-object v0, Lcom/google/android/music/art/ArtUtils;->sArtHandler:Lcom/google/android/music/art/ArtUtils$ArtHandler;

    .line 23
    new-instance v0, Landroid/support/v4/util/LruCache;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/google/android/music/art/ArtUtils;->sOverlayCache:Landroid/support/v4/util/LruCache;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    return-void
.end method

.method public static final getHandler()Lcom/google/android/music/art/ArtUtils$ArtHandler;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/music/art/ArtUtils;->sArtHandler:Lcom/google/android/music/art/ArtUtils$ArtHandler;

    return-object v0
.end method

.method public static getOverlay(Landroid/content/Context;IIZ)Landroid/graphics/drawable/BitmapDrawable;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "circleCrop"    # Z

    .prologue
    .line 66
    new-instance v2, Lcom/google/android/music/art/ArtUtils$OverlayKey;

    const/4 v6, 0x0

    invoke-direct {v2, p1, p2, p3, v6}, Lcom/google/android/music/art/ArtUtils$OverlayKey;-><init>(IIZLcom/google/android/music/art/ArtUtils$1;)V

    .line 67
    .local v2, "key":Lcom/google/android/music/art/ArtUtils$OverlayKey;
    sget-object v6, Lcom/google/android/music/art/ArtUtils;->sOverlayCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v6, v2}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    .line 69
    .local v5, "value":Landroid/graphics/Bitmap;
    if-nez v5, :cond_0

    .line 70
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 71
    .local v1, "handle":Ljava/lang/Object;
    invoke-static {p0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    .line 72
    .local v4, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v3, 0x0

    .line 74
    .local v3, "nautilusEnabled":Z
    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 76
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 79
    invoke-static {p0, p1, v3, p3}, Lcom/google/android/music/art/ArtRenderingUtils;->createRadioOverlay(Landroid/content/Context;IZZ)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 81
    sget-object v6, Lcom/google/android/music/art/ArtUtils;->sOverlayCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v6, v2, v5}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    .end local v1    # "handle":Ljava/lang/Object;
    .end local v3    # "nautilusEnabled":Z
    .end local v4    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_0
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 84
    .local v0, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    return-object v0

    .line 76
    .end local v0    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    .restart local v1    # "handle":Ljava/lang/Object;
    .restart local v3    # "nautilusEnabled":Z
    .restart local v4    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :catchall_0
    move-exception v6

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v6
.end method
