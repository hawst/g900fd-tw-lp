.class public Lcom/google/android/music/leanback/LeanbackAccountSwitcherActivity;
.super Lcom/google/android/music/leanback/LeanbackBrowseActivity;
.source "LeanbackAccountSwitcherActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getAccountsListRow([Landroid/accounts/Account;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 7
    .param p1, "accounts"    # [Landroid/accounts/Account;

    .prologue
    .line 51
    new-instance v1, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackAccountSwitcherActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 52
    .local v1, "accountAdapter":Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    move-object v2, p1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v0, v2, v3

    .line 54
    .local v0, "account":Landroid/accounts/Account;
    new-instance v5, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v5}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 52
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 56
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    const v5, 0x7f0b005c

    invoke-virtual {p0, v5}, Lcom/google/android/music/leanback/LeanbackAccountSwitcherActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/google/android/music/leanback/LeanbackAccountSwitcherActivity;->createListRow(Ljava/lang/String;Landroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v5

    return-object v5
.end method

.method protected getListRowsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 3

    .prologue
    .line 28
    new-instance v1, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    new-instance v2, Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-direct {v2}, Landroid/support/v17/leanback/widget/ListRowPresenter;-><init>()V

    invoke-direct {v1, v2}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 30
    .local v1, "listRowsAdapter":Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {p0}, Lcom/google/android/music/tutorial/SignupStatus;->getAllAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 31
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v2, v0

    if-nez v2, :cond_0

    .line 32
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackAccountSwitcherActivity;->getNoAccountListRow()Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 37
    :goto_0
    return-object v1

    .line 34
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackAccountSwitcherActivity;->getAccountsListRow([Landroid/accounts/Account;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected getNoAccountListRow()Landroid/support/v17/leanback/widget/ListRow;
    .locals 4

    .prologue
    .line 41
    new-instance v0, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackAccountSwitcherActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 42
    .local v0, "adapter":Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 43
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 45
    new-instance v2, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v2}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    const v3, 0x7f0b013d

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackAccountSwitcherActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 47
    const v2, 0x7f0b0120

    invoke-virtual {p0, v2}, Lcom/google/android/music/leanback/LeanbackAccountSwitcherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/music/leanback/LeanbackAccountSwitcherActivity;->createListRow(Ljava/lang/String;Landroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v2

    return-object v2
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 23
    invoke-super {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->onResume()V

    .line 24
    return-void
.end method
