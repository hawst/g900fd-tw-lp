.class public Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;
.super Ljava/lang/Object;
.source "MediaDescriptionCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/session/MediaDescriptionCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mDescription:Ljava/lang/CharSequence;

.field private mExtras:Landroid/os/Bundle;

.field private mIcon:Landroid/graphics/Bitmap;

.field private mIconUri:Landroid/net/Uri;

.field private mMediaId:Ljava/lang/String;

.field private mSubtitle:Ljava/lang/CharSequence;

.field private mTitle:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/music/playback/session/MediaDescriptionCompat;
    .locals 9

    .prologue
    .line 257
    new-instance v0, Lcom/google/android/music/playback/session/MediaDescriptionCompat;

    iget-object v1, p0, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;->mMediaId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;->mTitle:Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;->mSubtitle:Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;->mDescription:Ljava/lang/CharSequence;

    iget-object v5, p0, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;->mIcon:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;->mIconUri:Landroid/net/Uri;

    iget-object v7, p0, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;->mExtras:Landroid/os/Bundle;

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/music/playback/session/MediaDescriptionCompat;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/net/Uri;Landroid/os/Bundle;Lcom/google/android/music/playback/session/MediaDescriptionCompat$1;)V

    return-object v0
.end method

.method public setIconUri(Landroid/net/Uri;)Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;
    .locals 0
    .param p1, "iconUri"    # Landroid/net/Uri;

    .prologue
    .line 241
    iput-object p1, p0, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;->mIconUri:Landroid/net/Uri;

    .line 242
    return-object p0
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;
    .locals 0
    .param p1, "subtitle"    # Ljava/lang/CharSequence;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;->mSubtitle:Ljava/lang/CharSequence;

    .line 209
    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/music/playback/session/MediaDescriptionCompat$Builder;->mTitle:Ljava/lang/CharSequence;

    .line 198
    return-object p0
.end method
