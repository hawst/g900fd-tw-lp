.class public abstract Lcom/google/android/music/KeepOnView;
.super Landroid/widget/ImageView;
.source "KeepOnView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
.implements Lcom/google/android/music/animator/AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/KeepOnView$SavedState;,
        Lcom/google/android/music/KeepOnView$MyUpdateKeepOnStatusTask;,
        Lcom/google/android/music/KeepOnView$SharedResources;
    }
.end annotation


# static fields
.field protected static sArcColor:I

.field protected static sArcColorPaused:I


# instance fields
.field private mArcPaint:Landroid/graphics/Paint;

.field private mArcRect:Landroid/graphics/RectF;

.field private mDocumentType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field private mDownloadFraction:F

.field private mEnabled:Z

.field private mIsUnwindAnimationInProcess:Z

.field private mKeepOnObserver:Landroid/database/ContentObserver;

.field private final mNetworkChangeListener:Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;

.field private mPinned:Z

.field protected mProgressArcPadding:I

.field private mSongList:Lcom/google/android/music/medialist/SongList;

.field private mSongListName:Ljava/lang/String;

.field private mViewInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 106
    sput v0, Lcom/google/android/music/KeepOnView;->sArcColor:I

    .line 107
    sput v0, Lcom/google/android/music/KeepOnView;->sArcColorPaused:I

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    .line 151
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 104
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/KeepOnView;->mProgressArcPadding:I

    .line 126
    new-instance v0, Lcom/google/android/music/KeepOnView$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/KeepOnView$1;-><init>(Lcom/google/android/music/KeepOnView;)V

    iput-object v0, p0, Lcom/google/android/music/KeepOnView;->mNetworkChangeListener:Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;

    .line 155
    invoke-virtual {p0, p1}, Lcom/google/android/music/KeepOnView;->initResourceIds(Landroid/content/Context;)V

    .line 157
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/KeepOnView;->mArcRect:Landroid/graphics/RectF;

    .line 158
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/KeepOnView;->mArcPaint:Landroid/graphics/Paint;

    .line 159
    invoke-direct {p0}, Lcom/google/android/music/KeepOnView;->isAbleToDownload()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/music/KeepOnView;->setArtPaintColor(Z)V

    .line 160
    iget-object v0, p0, Lcom/google/android/music/KeepOnView;->mArcPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 162
    iput-boolean v2, p0, Lcom/google/android/music/KeepOnView;->mPinned:Z

    .line 163
    iput-boolean v2, p0, Lcom/google/android/music/KeepOnView;->mEnabled:Z

    .line 164
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/KeepOnView;->mDownloadFraction:F

    .line 165
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/KeepOnView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 167
    iput-boolean v2, p0, Lcom/google/android/music/KeepOnView;->mViewInitialized:Z

    .line 170
    iput-boolean v2, p0, Lcom/google/android/music/KeepOnView;->mIsUnwindAnimationInProcess:Z

    .line 172
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/KeepOnView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/music/KeepOnView;->setArtPaintColor(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/KeepOnView;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/music/KeepOnView;->mSongList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/music/KeepOnView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/music/KeepOnView;->handleOnKeepOnClicked()V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/music/KeepOnView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/music/KeepOnView;->mSongListName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/android/music/KeepOnView;Lcom/google/android/music/ui/cardlib/model/Document$Type;)Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView;
    .param p1, "x1"    # Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/music/KeepOnView;->mDocumentType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    return-object p1
.end method

.method private drawKeepOn(Landroid/graphics/Canvas;IILandroid/graphics/Paint;Landroid/graphics/RectF;ZZF)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paddingLeft"    # I
    .param p3, "paddingTop"    # I
    .param p4, "arcPaint"    # Landroid/graphics/Paint;
    .param p5, "arcRect"    # Landroid/graphics/RectF;
    .param p6, "isPinable"    # Z
    .param p7, "pinned"    # Z
    .param p8, "downloadFraction"    # F

    .prologue
    .line 437
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getKeepOnResources()Lcom/google/android/music/KeepOnView$SharedResources;

    move-result-object v7

    .line 438
    .local v7, "sharedRes":Lcom/google/android/music/KeepOnView$SharedResources;
    if-eqz p6, :cond_0

    .line 439
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v0, v0, p8

    float-to-double v0, v0

    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v0, v0, v4

    if-gtz v0, :cond_1

    .line 440
    # getter for: Lcom/google/android/music/KeepOnView$SharedResources;->sPinnedCompletedOverlay:Landroid/graphics/Bitmap;
    invoke-static {v7}, Lcom/google/android/music/KeepOnView$SharedResources;->access$500(Lcom/google/android/music/KeepOnView$SharedResources;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 448
    .local v6, "overlay":Landroid/graphics/Bitmap;
    :goto_0
    # getter for: Lcom/google/android/music/KeepOnView$SharedResources;->sGreyCircleBackground:Landroid/graphics/Bitmap;
    invoke-static {v7}, Lcom/google/android/music/KeepOnView$SharedResources;->access$700(Lcom/google/android/music/KeepOnView$SharedResources;)Landroid/graphics/Bitmap;

    move-result-object v0

    int-to-float v1, p2

    int-to-float v2, p3

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 452
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->isPinned()Z

    move-result v0

    if-eqz v0, :cond_4

    const/high16 v0, 0x41200000    # 10.0f

    const/high16 v1, 0x43b40000    # 360.0f

    mul-float v1, v1, p8

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 454
    .local v3, "sweepAngle":F
    :goto_1
    const/high16 v2, 0x43870000    # 270.0f

    const/4 v4, 0x1

    move-object v0, p1

    move-object v1, p5

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 455
    int-to-float v0, p2

    int-to-float v1, p3

    const/4 v2, 0x0

    invoke-virtual {p1, v6, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 457
    .end local v3    # "sweepAngle":F
    .end local v6    # "overlay":Landroid/graphics/Bitmap;
    :cond_0
    return-void

    .line 441
    :cond_1
    if-nez p7, :cond_2

    iget-boolean v0, p0, Lcom/google/android/music/KeepOnView;->mIsUnwindAnimationInProcess:Z

    if-eqz v0, :cond_3

    .line 442
    :cond_2
    # getter for: Lcom/google/android/music/KeepOnView$SharedResources;->sPinnedInProgressOverlay:Landroid/graphics/Bitmap;
    invoke-static {v7}, Lcom/google/android/music/KeepOnView$SharedResources;->access$600(Lcom/google/android/music/KeepOnView$SharedResources;)Landroid/graphics/Bitmap;

    move-result-object v6

    .restart local v6    # "overlay":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 444
    .end local v6    # "overlay":Landroid/graphics/Bitmap;
    :cond_3
    # getter for: Lcom/google/android/music/KeepOnView$SharedResources;->sUnpinnedOverlay:Landroid/graphics/Bitmap;
    invoke-static {v7}, Lcom/google/android/music/KeepOnView$SharedResources;->access$400(Lcom/google/android/music/KeepOnView$SharedResources;)Landroid/graphics/Bitmap;

    move-result-object v6

    .restart local v6    # "overlay":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 452
    :cond_4
    const/high16 v0, 0x43b40000    # 360.0f

    mul-float v3, p8, v0

    goto :goto_1
.end method

.method private getBodyText()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 530
    iget-object v3, p0, Lcom/google/android/music/KeepOnView;->mDocumentType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-nez v3, :cond_1

    .line 545
    :cond_0
    :goto_0
    return-object v2

    .line 532
    :cond_1
    const/high16 v1, -0x80000000

    .line 533
    .local v1, "resourceId":I
    iget-object v3, p0, Lcom/google/android/music/KeepOnView;->mDocumentType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_3

    .line 534
    const v1, 0x7f0b02df

    .line 542
    :cond_2
    :goto_1
    const/high16 v3, -0x80000000

    if-eq v1, v3, :cond_0

    .line 545
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 535
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/KeepOnView;->mDocumentType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_4

    .line 536
    const v1, 0x7f0b02e0

    goto :goto_1

    .line 537
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/KeepOnView;->mDocumentType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_2

    .line 538
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v0

    .line 539
    .local v0, "isNautilus":Z
    if-eqz v0, :cond_5

    const v1, 0x7f0b02e1

    :goto_2
    goto :goto_1

    :cond_5
    const v1, 0x7f0b02e2

    goto :goto_2
.end method

.method private handleOnKeepOnClicked()V
    .locals 2

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/music/KeepOnView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    if-eqz v0, :cond_0

    .line 551
    new-instance v0, Lcom/google/android/music/KeepOnView$7;

    invoke-direct {v0, p0}, Lcom/google/android/music/KeepOnView$7;-><init>(Lcom/google/android/music/KeepOnView;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 603
    :goto_0
    return-void

    .line 574
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/KeepOnView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-eqz v0, :cond_1

    .line 575
    new-instance v0, Lcom/google/android/music/KeepOnView$8;

    invoke-direct {v0, p0}, Lcom/google/android/music/KeepOnView$8;-><init>(Lcom/google/android/music/KeepOnView;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto :goto_0

    .line 600
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/KeepOnView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-static {v0, v1, p0, p0}, Lcom/google/android/music/activitymanagement/KeepOnManager;->toggleSonglistKeepOn(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;)V

    goto :goto_0
.end method

.method private isAbleToDownload()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 209
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/UIStateManager;->isWifiOrEthernetConnected()Z

    move-result v1

    .line 210
    .local v1, "highSpeedAvailable":Z
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/UIStateManager;->isMobileConnected()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isOfflineDLOnlyOnWifi()Z

    move-result v4

    if-nez v4, :cond_2

    move v0, v3

    .line 212
    .local v0, "downloadOnMobile":Z
    :goto_0
    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v3

    :cond_1
    return v2

    .end local v0    # "downloadOnMobile":Z
    :cond_2
    move v0, v2

    .line 210
    goto :goto_0
.end method

.method private setArtPaintColor(Z)V
    .locals 2
    .param p1, "connected"    # Z

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getDownloadFraction()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/music/KeepOnView;->mArcPaint:Landroid/graphics/Paint;

    sget v1, Lcom/google/android/music/KeepOnView;->sArcColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 186
    :goto_0
    return-void

    .line 181
    :cond_0
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isKeepOnDownloadingPaused()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p1, :cond_2

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/KeepOnView;->mArcPaint:Landroid/graphics/Paint;

    sget v1, Lcom/google/android/music/KeepOnView;->sArcColorPaused:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 184
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/KeepOnView;->mArcPaint:Landroid/graphics/Paint;

    sget v1, Lcom/google/android/music/KeepOnView;->sArcColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method

.method private setDocumentType()V
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/KeepOnView;->mDocumentType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 296
    new-instance v0, Lcom/google/android/music/KeepOnView$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/KeepOnView$4;-><init>(Lcom/google/android/music/KeepOnView;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 319
    return-void
.end method

.method private setSongListName()V
    .locals 1

    .prologue
    .line 268
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/KeepOnView;->mSongListName:Ljava/lang/String;

    .line 270
    new-instance v0, Lcom/google/android/music/KeepOnView$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/KeepOnView$3;-><init>(Lcom/google/android/music/KeepOnView;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 290
    return-void
.end method

.method private setViewInitilized()V
    .locals 1

    .prologue
    .line 386
    iget-boolean v0, p0, Lcom/google/android/music/KeepOnView;->mViewInitialized:Z

    if-nez v0, :cond_0

    .line 387
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/KeepOnView;->mViewInitialized:Z

    .line 389
    :cond_0
    return-void
.end method


# virtual methods
.method public getDownloadFraction()F
    .locals 1

    .prologue
    .line 357
    iget v0, p0, Lcom/google/android/music/KeepOnView;->mDownloadFraction:F

    return v0
.end method

.method protected abstract getKeepOnResources()Lcom/google/android/music/KeepOnView$SharedResources;
.end method

.method protected abstract getProgressArcPadding(Landroid/content/Context;)I
.end method

.method protected initResourceIds(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, -0x1

    .line 228
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 229
    .local v0, "resources":Landroid/content/res/Resources;
    sget v1, Lcom/google/android/music/KeepOnView;->sArcColor:I

    if-ne v1, v2, :cond_0

    .line 230
    const v1, 0x7f0c007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/music/KeepOnView;->sArcColor:I

    .line 232
    :cond_0
    sget v1, Lcom/google/android/music/KeepOnView;->sArcColorPaused:I

    if-ne v1, v2, :cond_1

    .line 233
    const v1, 0x7f0c007c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/music/KeepOnView;->sArcColorPaused:I

    .line 236
    :cond_1
    iget v1, p0, Lcom/google/android/music/KeepOnView;->mProgressArcPadding:I

    if-ne v1, v2, :cond_2

    .line 237
    invoke-virtual {p0, p1}, Lcom/google/android/music/KeepOnView;->getProgressArcPadding(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/KeepOnView;->mProgressArcPadding:I

    .line 239
    :cond_2
    return-void
.end method

.method public isPinnable()Z
    .locals 1

    .prologue
    .line 382
    iget-boolean v0, p0, Lcom/google/android/music/KeepOnView;->mEnabled:Z

    return v0
.end method

.method public isPinned()Z
    .locals 1

    .prologue
    .line 377
    iget-boolean v0, p0, Lcom/google/android/music/KeepOnView;->mPinned:Z

    return v0
.end method

.method public onAnimationUpdate(Lcom/google/android/music/animator/Animator;)V
    .locals 1
    .param p1, "animator"    # Lcom/google/android/music/animator/Animator;

    .prologue
    .line 393
    invoke-virtual {p1}, Lcom/google/android/music/animator/Animator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/google/android/music/KeepOnView;->mDownloadFraction:F

    .line 394
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->invalidate()V

    .line 395
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    .line 190
    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    .line 192
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/KeepOnView;->mNetworkChangeListener:Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->registerNetworkChangeListener(Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/music/KeepOnView;->mKeepOnObserver:Landroid/database/ContentObserver;

    if-nez v0, :cond_0

    .line 195
    new-instance v0, Lcom/google/android/music/KeepOnView$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/KeepOnView$2;-><init>(Lcom/google/android/music/KeepOnView;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/music/KeepOnView;->mKeepOnObserver:Landroid/database/ContentObserver;

    .line 203
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/store/MusicContent;->KEEP_ON_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/music/KeepOnView;->mKeepOnObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 206
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 685
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->onKeepOnViewClicked()V

    .line 686
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 217
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 218
    iget-object v0, p0, Lcom/google/android/music/KeepOnView;->mKeepOnObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/KeepOnView;->mKeepOnObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 220
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/KeepOnView;->mKeepOnObserver:Landroid/database/ContentObserver;

    .line 223
    :cond_0
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/KeepOnView;->mNetworkChangeListener:Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->unregisterNetworkChangeListener(Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;)V

    .line 224
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 403
    iget-boolean v0, p0, Lcom/google/android/music/KeepOnView;->mViewInitialized:Z

    if-eqz v0, :cond_0

    .line 404
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getPaddingLeft()I

    move-result v2

    .line 405
    .local v2, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getPaddingTop()I

    move-result v3

    .line 407
    .local v3, "paddingTop":I
    invoke-direct {p0}, Lcom/google/android/music/KeepOnView;->isAbleToDownload()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/music/KeepOnView;->setArtPaintColor(Z)V

    .line 408
    iget-object v4, p0, Lcom/google/android/music/KeepOnView;->mArcPaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/google/android/music/KeepOnView;->mArcRect:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->isPinnable()Z

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->isPinned()Z

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getDownloadFraction()F

    move-result v8

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/music/KeepOnView;->drawKeepOn(Landroid/graphics/Canvas;IILandroid/graphics/Paint;Landroid/graphics/RectF;ZZF)V

    .line 411
    .end local v2    # "paddingLeft":I
    .end local v3    # "paddingTop":I
    :cond_0
    return-void
.end method

.method public onKeepOnViewClicked()V
    .locals 12

    .prologue
    .line 481
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 482
    .local v2, "context":Landroid/content/Context;
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v7

    .line 483
    .local v7, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->getSelectedStorageVolumeId()Ljava/util/UUID;

    move-result-object v8

    .line 484
    .local v8, "volumeId":Ljava/util/UUID;
    if-eqz v8, :cond_0

    .line 485
    invoke-static {v2}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v9

    invoke-virtual {v9, v8}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownLocationByID(Ljava/util/UUID;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v5

    .line 487
    .local v5, "location":Lcom/google/android/music/download/cache/CacheLocation;
    if-nez v5, :cond_0

    .line 488
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/music/ui/SDCardDialogActivity;->showPinFailureDialog(Landroid/content/Context;)V

    .line 527
    .end local v5    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    :goto_0
    return-void

    .line 493
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->isPinned()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/music/preferences/MusicPreferences;->shouldShowUnpinDialog()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 495
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v0, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 496
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v9

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    .line 497
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v9, 0x7f040080

    const/4 v10, 0x0

    invoke-virtual {v4, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 499
    .local v3, "customView":Landroid/view/ViewGroup;
    const v9, 0x7f0e00ba

    invoke-virtual {v3, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 500
    .local v1, "checkBox":Landroid/widget/CheckBox;
    const v9, 0x7f0e0135

    invoke-virtual {v3, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 501
    .local v6, "messageView":Landroid/widget/TextView;
    invoke-direct {p0}, Lcom/google/android/music/KeepOnView;->getBodyText()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 502
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v10, 0x7f0b02e4

    new-instance v11, Lcom/google/android/music/KeepOnView$6;

    invoke-direct {v11, p0, v1}, Lcom/google/android/music/KeepOnView$6;-><init>(Lcom/google/android/music/KeepOnView;Landroid/widget/CheckBox;)V

    invoke-virtual {v9, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v10, 0x7f0b02e3

    new-instance v11, Lcom/google/android/music/KeepOnView$5;

    invoke-direct {v11, p0}, Lcom/google/android/music/KeepOnView$5;-><init>(Lcom/google/android/music/KeepOnView;)V

    invoke-virtual {v9, v10, v11}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 525
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v1    # "checkBox":Landroid/widget/CheckBox;
    .end local v3    # "customView":Landroid/view/ViewGroup;
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    .end local v6    # "messageView":Landroid/widget/TextView;
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/KeepOnView;->handleOnKeepOnClicked()V

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 13
    .param p1, "widthSpec"    # I
    .param p2, "heightSpec"    # I

    .prologue
    .line 416
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getPaddingLeft()I

    move-result v6

    .line 417
    .local v6, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getPaddingTop()I

    move-result v7

    .line 418
    .local v7, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getKeepOnResources()Lcom/google/android/music/KeepOnView$SharedResources;

    move-result-object v10

    # getter for: Lcom/google/android/music/KeepOnView$SharedResources;->sUnpinnedOverlay:Landroid/graphics/Bitmap;
    invoke-static {v10}, Lcom/google/android/music/KeepOnView$SharedResources;->access$400(Lcom/google/android/music/KeepOnView$SharedResources;)Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 419
    .local v5, "backgroundWidth":I
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getKeepOnResources()Lcom/google/android/music/KeepOnView$SharedResources;

    move-result-object v10

    # getter for: Lcom/google/android/music/KeepOnView$SharedResources;->sUnpinnedOverlay:Landroid/graphics/Bitmap;
    invoke-static {v10}, Lcom/google/android/music/KeepOnView$SharedResources;->access$400(Lcom/google/android/music/KeepOnView$SharedResources;)Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 421
    .local v4, "backgroundHeight":I
    iget v10, p0, Lcom/google/android/music/KeepOnView;->mProgressArcPadding:I

    add-int/2addr v10, v6

    int-to-float v1, v10

    .line 422
    .local v1, "arcLeft":F
    iget v10, p0, Lcom/google/android/music/KeepOnView;->mProgressArcPadding:I

    add-int/2addr v10, v7

    int-to-float v2, v10

    .line 423
    .local v2, "arcTop":F
    iget v10, p0, Lcom/google/android/music/KeepOnView;->mProgressArcPadding:I

    mul-int/lit8 v10, v10, 0x2

    sub-int v10, v5, v10

    int-to-float v3, v10

    .line 424
    .local v3, "arcWidth":F
    iget v10, p0, Lcom/google/android/music/KeepOnView;->mProgressArcPadding:I

    mul-int/lit8 v10, v10, 0x2

    sub-int v10, v4, v10

    int-to-float v0, v10

    .line 425
    .local v0, "arcHeight":F
    iget-object v10, p0, Lcom/google/android/music/KeepOnView;->mArcRect:Landroid/graphics/RectF;

    add-float v11, v1, v3

    add-float v12, v2, v0

    invoke-virtual {v10, v1, v2, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 427
    mul-int/lit8 v10, v6, 0x2

    add-int v9, v5, v10

    .line 428
    .local v9, "viewWidth":I
    mul-int/lit8 v10, v7, 0x2

    add-int v8, v4, v10

    .line 430
    .local v8, "viewHeight":I
    invoke-virtual {p0, v9, v8}, Lcom/google/android/music/KeepOnView;->setMeasuredDimension(II)V

    .line 431
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 472
    move-object v0, p1

    check-cast v0, Lcom/google/android/music/KeepOnView$SavedState;

    .line 473
    .local v0, "ss":Lcom/google/android/music/KeepOnView$SavedState;
    invoke-virtual {v0}, Lcom/google/android/music/KeepOnView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/ImageView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 474
    # getter for: Lcom/google/android/music/KeepOnView$SavedState;->checked:Z
    invoke-static {v0}, Lcom/google/android/music/KeepOnView$SavedState;->access$800(Lcom/google/android/music/KeepOnView$SavedState;)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/KeepOnView;->setPinned(Z)V

    .line 475
    # getter for: Lcom/google/android/music/KeepOnView$SavedState;->available:Z
    invoke-static {v0}, Lcom/google/android/music/KeepOnView$SavedState;->access$900(Lcom/google/android/music/KeepOnView$SavedState;)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/KeepOnView;->setPinnable(Z)V

    .line 476
    # getter for: Lcom/google/android/music/KeepOnView$SavedState;->downloadFraction:F
    invoke-static {v0}, Lcom/google/android/music/KeepOnView$SavedState;->access$1000(Lcom/google/android/music/KeepOnView$SavedState;)F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/KeepOnView;->setDownloadFraction(F)V

    .line 477
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->requestLayout()V

    .line 478
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 462
    invoke-super {p0}, Landroid/widget/ImageView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 463
    .local v1, "superState":Landroid/os/Parcelable;
    new-instance v0, Lcom/google/android/music/KeepOnView$SavedState;

    invoke-direct {v0, v1}, Lcom/google/android/music/KeepOnView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 464
    .local v0, "ss":Lcom/google/android/music/KeepOnView$SavedState;
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->isPinned()Z

    move-result v2

    # setter for: Lcom/google/android/music/KeepOnView$SavedState;->checked:Z
    invoke-static {v0, v2}, Lcom/google/android/music/KeepOnView$SavedState;->access$802(Lcom/google/android/music/KeepOnView$SavedState;Z)Z

    .line 465
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->isPinnable()Z

    move-result v2

    # setter for: Lcom/google/android/music/KeepOnView$SavedState;->available:Z
    invoke-static {v0, v2}, Lcom/google/android/music/KeepOnView$SavedState;->access$902(Lcom/google/android/music/KeepOnView$SavedState;Z)Z

    .line 466
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getDownloadFraction()F

    move-result v2

    # setter for: Lcom/google/android/music/KeepOnView$SavedState;->downloadFraction:F
    invoke-static {v0, v2}, Lcom/google/android/music/KeepOnView$SavedState;->access$1002(Lcom/google/android/music/KeepOnView$SavedState;F)F

    .line 467
    return-object v0
.end method

.method public setDownloadFraction(F)V
    .locals 0
    .param p1, "downloadFraction"    # F

    .prologue
    .line 351
    iput p1, p0, Lcom/google/android/music/KeepOnView;->mDownloadFraction:F

    .line 352
    invoke-direct {p0}, Lcom/google/android/music/KeepOnView;->setViewInitilized()V

    .line 353
    return-void
.end method

.method public setIsUnwindAnimationInProcess(Z)V
    .locals 0
    .param p1, "animationInProcess"    # Z

    .prologue
    .line 341
    iput-boolean p1, p0, Lcom/google/android/music/KeepOnView;->mIsUnwindAnimationInProcess:Z

    .line 342
    return-void
.end method

.method public setOnClick(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 259
    if-eqz p1, :cond_0

    .line 260
    invoke-virtual {p0, p0}, Lcom/google/android/music/KeepOnView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 264
    :goto_0
    return-void

    .line 262
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/KeepOnView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setPinnable(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 369
    iput-boolean p1, p0, Lcom/google/android/music/KeepOnView;->mEnabled:Z

    .line 371
    invoke-virtual {p0, p1}, Lcom/google/android/music/KeepOnView;->setEnabled(Z)V

    .line 372
    invoke-direct {p0}, Lcom/google/android/music/KeepOnView;->setViewInitilized()V

    .line 373
    return-void
.end method

.method public setPinned(Z)V
    .locals 0
    .param p1, "pinned"    # Z

    .prologue
    .line 362
    iput-boolean p1, p0, Lcom/google/android/music/KeepOnView;->mPinned:Z

    .line 363
    invoke-direct {p0}, Lcom/google/android/music/KeepOnView;->setViewInitilized()V

    .line 364
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->updateContentDescription()V

    .line 365
    return-void
.end method

.method public setSongList(Lcom/google/android/music/medialist/SongList;)V
    .locals 0
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 322
    iput-object p1, p0, Lcom/google/android/music/KeepOnView;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 323
    invoke-direct {p0}, Lcom/google/android/music/KeepOnView;->setSongListName()V

    .line 324
    invoke-direct {p0}, Lcom/google/android/music/KeepOnView;->setDocumentType()V

    .line 325
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->updateContainerOfflineStatus()V

    .line 326
    return-void
.end method

.method public updateContainerOfflineStatus()V
    .locals 1

    .prologue
    .line 607
    new-instance v0, Lcom/google/android/music/KeepOnView$MyUpdateKeepOnStatusTask;

    invoke-direct {v0, p0}, Lcom/google/android/music/KeepOnView$MyUpdateKeepOnStatusTask;-><init>(Lcom/google/android/music/KeepOnView;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 608
    return-void
.end method

.method protected updateContentDescription()V
    .locals 2

    .prologue
    .line 334
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->isPinned()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0241

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/music/KeepOnView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 337
    return-void

    .line 334
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0240

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
