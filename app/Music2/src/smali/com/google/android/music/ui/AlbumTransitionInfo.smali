.class public Lcom/google/android/music/ui/AlbumTransitionInfo;
.super Lcom/google/android/music/ui/TransitionInfo;
.source "AlbumTransitionInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/ui/AlbumTransitionInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mBackgroundColor:I

.field mPadding:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/google/android/music/ui/AlbumTransitionInfo$1;

    invoke-direct {v0}, Lcom/google/android/music/ui/AlbumTransitionInfo$1;-><init>()V

    sput-object v0, Lcom/google/android/music/ui/AlbumTransitionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/TransitionInfo;-><init>(I)V

    .line 23
    const-string v0, "card"

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AlbumTransitionInfo;->setSharedElementName(Ljava/lang/String;)V

    .line 24
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/TransitionInfo;-><init>(Landroid/os/Parcel;)V

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/AlbumTransitionInfo;->mBackgroundColor:I

    .line 68
    const-class v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/google/android/music/ui/AlbumTransitionInfo;->mPadding:Landroid/graphics/RectF;

    .line 69
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/music/ui/AlbumTransitionInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/music/ui/AlbumTransitionInfo$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/AlbumTransitionInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/android/music/ui/AlbumTransitionInfo;->mBackgroundColor:I

    return v0
.end method

.method public getSharedElementPadding()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/music/ui/AlbumTransitionInfo;->mPadding:Landroid/graphics/RectF;

    return-object v0
.end method

.method public setBackgroundColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/google/android/music/ui/AlbumTransitionInfo;->mBackgroundColor:I

    .line 32
    return-void
.end method

.method public setSharedElementPadding(Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "padding"    # Landroid/graphics/RectF;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/music/ui/AlbumTransitionInfo;->mPadding:Landroid/graphics/RectF;

    .line 40
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/TransitionInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 50
    iget v0, p0, Lcom/google/android/music/ui/AlbumTransitionInfo;->mBackgroundColor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 51
    iget-object v0, p0, Lcom/google/android/music/ui/AlbumTransitionInfo;->mPadding:Landroid/graphics/RectF;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 52
    return-void
.end method
