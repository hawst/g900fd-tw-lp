.class public Lcom/google/android/music/sync/google/MusicUpstreamSender;
.super Lcom/google/android/music/sync/common/UpstreamSender;
.source "MusicUpstreamSender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/sync/google/MusicUpstreamSender$1;
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mAccountHash:I

.field private final mClient:Lcom/google/android/music/sync/api/MusicApiClient;

.field private final mContext:Landroid/content/Context;

.field private final mEnableTrackStatsUpSync:Z

.field private final mProtocolState:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mStore:Lcom/google/android/music/store/Store;

.field private final mTag:Ljava/lang/String;

.field private final mUseVerboseLogging:Z

.field private final mValues:Landroid/content/ContentValues;


# direct methods
.method public constructor <init>(Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;ILandroid/content/Context;Ljava/util/Map;Ljava/lang/String;Lcom/google/android/music/sync/api/MusicApiClient;Z)V
    .locals 2
    .param p1, "queue"    # Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;
    .param p2, "upstreamPageSize"    # I
    .param p3, "context"    # Landroid/content/Context;
    .param p5, "logTag"    # Ljava/lang/String;
    .param p6, "client"    # Lcom/google/android/music/sync/api/MusicApiClient;
    .param p7, "enableTrackStatsUpSync"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;",
            "I",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/music/sync/api/MusicApiClient;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 97
    .local p4, "protocolState":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/music/sync/common/UpstreamSender;-><init>(Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;ILjava/lang/String;)V

    .line 98
    iput-object p5, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    .line 99
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->SYNC:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    .line 100
    iput-object p3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mContext:Landroid/content/Context;

    .line 101
    invoke-static {p3}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    .line 102
    iput-object p6, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    .line 103
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    .line 104
    iput-object p4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mProtocolState:Ljava/util/Map;

    .line 105
    const-class v0, Landroid/accounts/Account;

    const-string v1, "account"

    invoke-interface {p4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    .line 106
    const-class v0, Ljava/lang/Integer;

    const-string v1, "remote_account"

    invoke-interface {p4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccountHash:I

    .line 107
    iput-boolean p7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mEnableTrackStatsUpSync:Z

    .line 108
    return-void
.end method

.method private cleanupBatchMutations(Ljava/util/List;Ljava/util/List;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ConflictException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Landroid/accounts/AuthenticatorException;
        }
    .end annotation

    .prologue
    .line 191
    .local p1, "subBlock":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    .local p2, "batchMutateResponse":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;>;"
    const/4 v5, 0x0

    .line 192
    .local v5, "isTxnSuccessful":Z
    iget-object v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v11}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 193
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x0

    .line 194
    .local v2, "conflictCount":I
    const/4 v10, 0x0

    .line 196
    .local v10, "tooManyItemsCount":I
    :try_start_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v6

    .line 197
    .local v6, "listSize":I
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v11

    if-le v6, v11, :cond_0

    .line 198
    new-instance v11, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "A batch mutate response to the server contained "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " mutations, but "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " results were returned."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    .end local v6    # "listSize":I
    :catchall_0
    move-exception v11

    iget-object v12, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v12, v3, v5}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v11

    .line 202
    .restart local v6    # "listSize":I
    :cond_0
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v6, :cond_a

    .line 203
    :try_start_1
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;

    .line 204
    .local v7, "mutation":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;

    .line 205
    .local v8, "response":Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;
    iget-object v9, v8, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;->mResponseCode:Ljava/lang/String;

    .line 206
    .local v9, "responseCode":Ljava/lang/String;
    sget-object v11, Lcom/google/android/music/sync/google/MusicUpstreamSender$1;->$SwitchMap$com$google$android$music$sync$google$model$BatchMutateResponse$MutateResponse$ResponseCode:[I

    invoke-direct {p0, v9}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mapStringResponseCodeToEnum(Ljava/lang/String;)Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    .line 269
    :cond_1
    :goto_1
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->yieldIfContendedSafely()Z

    .line 202
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 208
    :pswitch_0
    add-int/lit8 v2, v2, 0x1

    .line 218
    :pswitch_1
    invoke-interface {v7}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->isInsert()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 219
    iget-object v11, v8, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;->mId:Ljava/lang/String;

    if-nez v11, :cond_2

    .line 220
    iget-object v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v12, "Insert response from server with null id.  Removing."

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    invoke-direct {p0, v7, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->deleteMutatedRow(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 224
    :cond_2
    iget-object v11, v8, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;->mId:Ljava/lang/String;

    invoke-interface {v7, v11}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->setRemoteId(Ljava/lang/String;)V

    .line 225
    invoke-direct {p0, v7, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupInsert(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 227
    :cond_3
    invoke-interface {v7}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->isUpdate()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 228
    invoke-direct {p0, v7, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupUpdate(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 229
    :cond_4
    invoke-interface {v7}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->isDeleted()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 230
    invoke-direct {p0, v7, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupDelete(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 237
    :pswitch_2
    add-int/lit8 v10, v10, 0x1

    .line 238
    invoke-interface {v7}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->isInsert()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 240
    invoke-direct {p0, v7, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->deleteMutatedRow(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 241
    :cond_5
    invoke-interface {v7}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->isUpdate()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 243
    iget-object v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v12, "TOO_MANY_ITEMS response code returned for an update. Likely a server side error."

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    invoke-direct {p0, v7, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupMutation(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 247
    :cond_6
    invoke-interface {v7}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->isDeleted()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 249
    iget-object v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v12, "TOO_MANY_ITEMS response code returned for a delete. Likely a server side error."

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    invoke-direct {p0, v7, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->deleteMutatedRow(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 255
    :pswitch_3
    iget-boolean v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v11, :cond_7

    .line 256
    iget-object v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v12, "Upstream sender received invalid request on item in batch mutate.  Restoring from server."

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    :cond_7
    invoke-interface {v7}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->isInsert()Z

    move-result v11

    if-nez v11, :cond_8

    invoke-interface {v7}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getRemoteId()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 261
    :cond_8
    invoke-direct {p0, v7, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->deleteMutatedRow(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_1

    .line 264
    :cond_9
    invoke-direct {p0, v7}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->restoreItemFromServer(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 271
    .end local v7    # "mutation":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    .end local v8    # "response":Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;
    .end local v9    # "responseCode":Ljava/lang/String;
    :cond_a
    const/4 v5, 0x1

    .line 273
    iget-object v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v11, v3, v5}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 275
    if-lez v10, :cond_b

    .line 276
    iget-object v12, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v11, 0x1

    if-ne v10, v11, :cond_c

    const-string v11, "1 entry"

    :goto_2
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, " rejected due to the constituent entity being too large."

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v12, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :cond_b
    if-eqz v2, :cond_d

    .line 280
    new-instance v1, Lcom/google/android/music/sync/api/ConflictException;

    invoke-direct {v1}, Lcom/google/android/music/sync/api/ConflictException;-><init>()V

    .line 281
    .local v1, "ce":Lcom/google/android/music/sync/api/ConflictException;
    invoke-virtual {v1, v2}, Lcom/google/android/music/sync/api/ConflictException;->setConflictCount(I)V

    .line 282
    throw v1

    .line 276
    .end local v1    # "ce":Lcom/google/android/music/sync/api/ConflictException;
    :cond_c
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, " entries"

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_2

    .line 284
    :cond_d
    return-void

    .line 206
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private cleanupDelete(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p1, "mutation"    # Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 383
    const/4 v1, 0x0

    .line 384
    .local v1, "tombstoneTable":Ljava/lang/String;
    const/4 v0, 0x0

    .line 385
    .local v0, "idColName":Ljava/lang/String;
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    if-eqz v2, :cond_2

    .line 386
    const-string v1, "LIST_TOMBSTONES"

    .line 387
    const-string v0, "Id"

    .line 398
    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v2, :cond_1

    .line 399
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v3, "Upstream sender: Removing playlist entity."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getLocalId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p2, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 403
    return-void

    .line 388
    :cond_2
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    if-eqz v2, :cond_3

    .line 389
    const-string v1, "LISTITEM_TOMBSTONES"

    .line 390
    const-string v0, "Id"

    goto :goto_0

    .line 391
    :cond_3
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/TrackTombstone;

    if-eqz v2, :cond_4

    .line 392
    const-string v1, "MUSIC_TOMBSTONES"

    .line 393
    const-string v0, "Id"

    goto :goto_0

    .line 394
    :cond_4
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    if-eqz v2, :cond_0

    .line 395
    const-string v1, "RADIO_STATION_TOMBSTONES"

    .line 396
    const-string v0, "Id"

    goto :goto_0
.end method

.method private cleanupInsert(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 9
    .param p1, "mutation"    # Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v8, 0x0

    .line 315
    const/4 v1, 0x0

    .line 316
    .local v1, "table":Ljava/lang/String;
    const/4 v0, 0x0

    .line 317
    .local v0, "idColName":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 318
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    if-eqz v2, :cond_2

    .line 319
    const-string v1, "LISTS"

    .line 320
    const-string v0, "Id"

    .line 321
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "_sync_dirty"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 322
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "SourceId"

    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getRemoteId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "SourceAccount"

    iget v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccountHash:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 350
    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v2, :cond_1

    .line 351
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v3, "Upstream sender: Undirtying inserted entity."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getLocalId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {p2, v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 355
    return-void

    .line 324
    :cond_2
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    if-eqz v2, :cond_3

    .line 325
    const-string v1, "LISTITEMS"

    .line 326
    const-string v0, "Id"

    .line 327
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "_sync_dirty"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 328
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "SourceId"

    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getRemoteId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "SourceAccount"

    iget v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccountHash:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 330
    :cond_3
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/Track;

    if-eqz v2, :cond_4

    .line 331
    const-string v1, "MUSIC"

    .line 332
    const-string v0, "Id"

    .line 333
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "_sync_dirty"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 334
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "SourceId"

    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getRemoteId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "SourceType"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 336
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "SourceAccount"

    iget v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccountHash:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 337
    :cond_4
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    if-eqz v2, :cond_5

    .line 338
    const-string v1, "RADIO_STATIONS"

    .line 339
    const-string v0, "Id"

    .line 340
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "_sync_dirty"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 341
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "SourceId"

    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getRemoteId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "SourceAccount"

    iget v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccountHash:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 343
    :cond_5
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    if-eqz v2, :cond_0

    .line 344
    const-string v1, "MAINSTAGE_BLACKLIST"

    .line 345
    const-string v0, "Id"

    .line 346
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "_sync_dirty"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 347
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "SourceId"

    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getRemoteId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "SourceAccount"

    iget v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccountHash:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0
.end method

.method private cleanupMutation(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "mutation"    # Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 305
    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->isDeleted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupDelete(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 312
    :goto_0
    return-void

    .line 307
    :cond_0
    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->isUpdate()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 308
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupUpdate(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 310
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupInsert(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method

.method private cleanupUpdate(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 9
    .param p1, "mutation"    # Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v8, 0x0

    .line 358
    const/4 v1, 0x0

    .line 359
    .local v1, "table":Ljava/lang/String;
    const/4 v0, 0x0

    .line 360
    .local v0, "idColName":Ljava/lang/String;
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    if-eqz v2, :cond_2

    .line 361
    const-string v1, "LISTS"

    .line 362
    const-string v0, "Id"

    .line 373
    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v2, :cond_1

    .line 374
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v3, "Upstream sender: Undirtying updated entity."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 377
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    const-string v3, "_sync_dirty"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 378
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mValues:Landroid/content/ContentValues;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getLocalId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {p2, v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 380
    return-void

    .line 363
    :cond_2
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    if-eqz v2, :cond_3

    .line 364
    const-string v1, "LISTITEMS"

    .line 365
    const-string v0, "Id"

    goto :goto_0

    .line 366
    :cond_3
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/Track;

    if-eqz v2, :cond_4

    .line 367
    const-string v1, "MUSIC"

    .line 368
    const-string v0, "Id"

    goto :goto_0

    .line 369
    :cond_4
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    if-eqz v2, :cond_0

    .line 370
    const-string v1, "RADIO_STATIONS"

    .line 371
    const-string v0, "Id"

    goto :goto_0
.end method

.method private deleteMutatedRow(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "mutation"    # Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 287
    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->isDeleted()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 289
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupDelete(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 302
    :goto_0
    return-void

    .line 291
    :cond_0
    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getLocalId()J

    move-result-wide v0

    .line 292
    .local v0, "localId":J
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    if-eqz v2, :cond_1

    .line 293
    invoke-static {p2, v0, v1}, Lcom/google/android/music/store/PlayList;->deleteById(Landroid/database/sqlite/SQLiteDatabase;J)I

    goto :goto_0

    .line 294
    :cond_1
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    if-eqz v2, :cond_2

    .line 295
    invoke-static {p2, v0, v1}, Lcom/google/android/music/store/PlayList$Item;->deleteById(Landroid/database/sqlite/SQLiteDatabase;J)V

    goto :goto_0

    .line 296
    :cond_2
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/Track;

    if-eqz v2, :cond_3

    .line 297
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mContext:Landroid/content/Context;

    invoke-static {v2, v0, v1}, Lcom/google/android/music/store/MusicFile;->deleteByLocalId(Landroid/content/Context;J)V

    goto :goto_0

    .line 299
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v3, "Attempting to undo a mutation of unknown type."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private deletePlaylist(J)V
    .locals 5
    .param p1, "localId"    # J

    .prologue
    const/4 v4, 0x1

    .line 924
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v2}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 925
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x0

    .line 927
    .local v1, "deleteStatement":Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    invoke-static {v0, p1, p2}, Lcom/google/android/music/store/PlayList;->deleteById(Landroid/database/sqlite/SQLiteDatabase;J)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 929
    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 930
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v2, v0, v4}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 932
    return-void

    .line 929
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 930
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0, v4}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v2
.end method

.method private deletePlaylistEntry(J)V
    .locals 5
    .param p1, "localId"    # J

    .prologue
    const/4 v4, 0x1

    .line 935
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v2}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 936
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x0

    .line 938
    .local v1, "deleteStatement":Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    invoke-static {v0, p1, p2}, Lcom/google/android/music/store/PlayList$Item;->deleteById(Landroid/database/sqlite/SQLiteDatabase;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 940
    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 941
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v2, v0, v4}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 943
    return-void

    .line 940
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 941
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0, v4}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v2
.end method

.method private deleteRadioStation(J)V
    .locals 5
    .param p1, "localId"    # J

    .prologue
    const/4 v4, 0x1

    .line 946
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v2}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 947
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x0

    .line 949
    .local v1, "deleteStatement":Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    invoke-static {v0, p1, p2}, Lcom/google/android/music/store/RadioStation;->deleteById(Landroid/database/sqlite/SQLiteDatabase;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 951
    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 952
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v2, v0, v4}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 954
    return-void

    .line 951
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 952
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0, v4}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v2
.end method

.method private fillInRemoteIdForTrack(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)Z
    .locals 6
    .param p1, "entry"    # Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    .prologue
    const/4 v3, 0x0

    .line 901
    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 903
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    new-instance v2, Lcom/google/android/music/store/MusicFile;

    invoke-direct {v2}, Lcom/google/android/music/store/MusicFile;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 905
    .local v2, "file":Lcom/google/android/music/store/MusicFile;
    :try_start_1
    iget-wide v4, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalMusicId:J

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/android/music/store/MusicFile;->load(Landroid/database/sqlite/SQLiteDatabase;J)V
    :try_end_1
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 911
    :try_start_2
    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getTrackType()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_0

    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getSourceType()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    .line 918
    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 920
    :goto_0
    return v3

    .line 906
    :catch_0
    move-exception v1

    .line 918
    .local v1, "e":Lcom/google/android/music/store/DataNotFoundException;
    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 915
    .end local v1    # "e":Lcom/google/android/music/store/DataNotFoundException;
    :cond_0
    :try_start_3
    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->setTrackId(Ljava/lang/String;)V

    .line 916
    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getSourceType()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->setServerSourceFromClientSourceType(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 918
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 920
    const/4 v3, 0x1

    goto :goto_0

    .line 918
    .end local v2    # "file":Lcom/google/android/music/store/MusicFile;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v3
.end method

.method private fillInRemoteIdOfParentPlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)Z
    .locals 4
    .param p1, "entry"    # Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    .prologue
    .line 879
    new-instance v1, Lcom/google/android/music/store/PlayList;

    invoke-direct {v1}, Lcom/google/android/music/store/PlayList;-><init>()V

    .line 880
    .local v1, "parentPlaylist":Lcom/google/android/music/store/PlayList;
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v2}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 882
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    iget-object v2, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mItem:Lcom/google/android/music/store/PlayList$Item;

    invoke-virtual {v2}, Lcom/google/android/music/store/PlayList$Item;->getListId()J

    move-result-wide v2

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/music/store/PlayList;->readPlayList(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;

    .line 883
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/music/store/PlayList;->getSourceId()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-nez v2, :cond_1

    .line 884
    :cond_0
    const/4 v2, 0x0

    .line 888
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 890
    :goto_0
    return v2

    .line 886
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/music/store/PlayList;->getSourceId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemotePlaylistId:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 888
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v2, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 890
    const/4 v2, 0x1

    goto :goto_0

    .line 888
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v2
.end method

.method private handleDeleteEntry(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)V
    .locals 6
    .param p1, "entry"    # Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ConflictException;
        }
    .end annotation

    .prologue
    .line 1282
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_0

    .line 1283
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Sending deleted playlist entry to cloud."

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1286
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    sget-object v5, Lcom/google/android/music/sync/api/MusicApiClient$OpType;->DELETE:Lcom/google/android/music/sync/api/MusicApiClient$OpType;

    invoke-interface {v3, v4, p1, v5}, Lcom/google/android/music/sync/api/MusicApiClient;->mutateItem(Landroid/accounts/Account;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Lcom/google/android/music/sync/api/MusicApiClient$OpType;)V
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    .line 1305
    :goto_0
    const/4 v2, 0x0

    .line 1306
    .local v2, "isTxnSuccessful":Z
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1308
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupDelete(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1309
    const/4 v2, 0x1

    .line 1311
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1313
    return-void

    .line 1287
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "isTxnSuccessful":Z
    :catch_0
    move-exception v1

    .line 1289
    .local v1, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Invalid data on playlist entry delete.  Skipping item."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1290
    .end local v1    # "e":Lcom/google/android/music/store/InvalidDataException;
    :catch_1
    move-exception v1

    .line 1291
    .local v1, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v3, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Http code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " on upstream playlist entry delete."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1293
    .end local v1    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_2
    move-exception v1

    .line 1294
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v4, "IO error on upstream playlist entry delete."

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1295
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 1296
    .local v1, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Server returned a bad request (400) error code for a playlist entry delete.  Ignoring."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1298
    .end local v1    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_4
    move-exception v1

    .line 1299
    .local v1, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Server returned a forbidden (401) error code for a playlist entry delete.  Ignoring."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1301
    .end local v1    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_5
    move-exception v1

    .line 1302
    .local v1, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Server returned a not found (404) error code for a playlist entry delete.  Ignoring."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1311
    .end local v1    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v2    # "isTxnSuccessful":Z
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method private handleDeletePlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;)V
    .locals 6
    .param p1, "playlist"    # Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ConflictException;
        }
    .end annotation

    .prologue
    .line 1246
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_0

    .line 1247
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Sending deleted playlist to cloud."

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1250
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    sget-object v5, Lcom/google/android/music/sync/api/MusicApiClient$OpType;->DELETE:Lcom/google/android/music/sync/api/MusicApiClient$OpType;

    invoke-interface {v3, v4, p1, v5}, Lcom/google/android/music/sync/api/MusicApiClient;->mutateItem(Landroid/accounts/Account;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Lcom/google/android/music/sync/api/MusicApiClient$OpType;)V
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    .line 1269
    :goto_0
    const/4 v2, 0x0

    .line 1270
    .local v2, "isTxnSuccessful":Z
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1272
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupDelete(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1273
    const/4 v2, 0x1

    .line 1275
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1277
    return-void

    .line 1251
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "isTxnSuccessful":Z
    :catch_0
    move-exception v1

    .line 1253
    .local v1, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Invalid data on playlist delete.  Skipping item."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1254
    .end local v1    # "e":Lcom/google/android/music/store/InvalidDataException;
    :catch_1
    move-exception v1

    .line 1255
    .local v1, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v3, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Http code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " on upstream playlist delete."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1257
    .end local v1    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_2
    move-exception v1

    .line 1258
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v4, "IO error on upstream playlist delete."

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1259
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 1260
    .local v1, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Server returned a bad request (400) error code for a playlist delete.  Ignoring."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1262
    .end local v1    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_4
    move-exception v1

    .line 1263
    .local v1, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Server returned a forbidden (401) error code for a playlist delete.  Ignoring."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1265
    .end local v1    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_5
    move-exception v1

    .line 1266
    .local v1, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Server returned a not found (404) error code for a playlist delete.  Ignoring."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1275
    .end local v1    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v2    # "isTxnSuccessful":Z
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method private handleDeleteRadioStation(Lcom/google/android/music/sync/google/model/SyncableRadioStation;)V
    .locals 10
    .param p1, "radioStation"    # Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ConflictException;
        }
    .end annotation

    .prologue
    .line 571
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v7, :cond_0

    .line 572
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Upstream sender: Sending deleted radio station to cloud."

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    :cond_0
    :try_start_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 576
    .local v5, "radioStations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncableRadioStation;>;"
    invoke-interface {v5, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 577
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v8, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    invoke-interface {v7, v8, v5}, Lcom/google/android/music/sync/api/MusicApiClient;->mutateItems(Landroid/accounts/Account;Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 578
    .local v4, "mutateResponses":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;

    .line 579
    .local v6, "response":Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;
    invoke-virtual {v6}, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;->throwExceptionFromResponseCode()V
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 582
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "mutateResponses":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;>;"
    .end local v5    # "radioStations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncableRadioStation;>;"
    .end local v6    # "response":Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;
    :catch_0
    move-exception v1

    .line 583
    .local v1, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Invalid data on radio station delete.  Skipping item."

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 599
    .end local v1    # "e":Lcom/google/android/music/store/InvalidDataException;
    :cond_1
    :goto_1
    const/4 v3, 0x0

    .line 600
    .local v3, "isTxnSuccessful":Z
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v7}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 602
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupDelete(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603
    const/4 v3, 0x1

    .line 605
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v7, v0, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 607
    return-void

    .line 584
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v3    # "isTxnSuccessful":Z
    :catch_1
    move-exception v1

    .line 585
    .local v1, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v7, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Http code "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " on upstream radio station delete."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v1}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 587
    .end local v1    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_2
    move-exception v1

    .line 588
    .local v1, "e":Ljava/io/IOException;
    new-instance v7, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v8, "IO error on upstream radio station delete."

    invoke-direct {v7, v8, v1}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 589
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 590
    .local v1, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Server returned a bad request (400) error code for a radio station delete.  Ignoring."

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 592
    .end local v1    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_4
    move-exception v1

    .line 593
    .local v1, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Server returned a forbidden (401) error code for a radio station delete.  Ignoring."

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 595
    .end local v1    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_5
    move-exception v1

    .line 596
    .local v1, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Server returned a not found (404) error code for a radio station delete.  Ignoring."

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 605
    .end local v1    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v3    # "isTxnSuccessful":Z
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v8, v0, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v7
.end method

.method private handleDeleteTrack(Lcom/google/android/music/sync/google/model/TrackTombstone;)V
    .locals 6
    .param p1, "trackTombstone"    # Lcom/google/android/music/sync/google/model/TrackTombstone;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ConflictException;
        }
    .end annotation

    .prologue
    .line 1318
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_0

    .line 1319
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Sending deleted track to cloud."

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1322
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    sget-object v5, Lcom/google/android/music/sync/api/MusicApiClient$OpType;->DELETE:Lcom/google/android/music/sync/api/MusicApiClient$OpType;

    invoke-interface {v3, v4, p1, v5}, Lcom/google/android/music/sync/api/MusicApiClient;->mutateItem(Landroid/accounts/Account;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Lcom/google/android/music/sync/api/MusicApiClient$OpType;)V
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    .line 1340
    :goto_0
    const/4 v2, 0x0

    .line 1341
    .local v2, "isTxnSuccessful":Z
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1343
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupDelete(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1344
    const/4 v2, 0x1

    .line 1346
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1348
    return-void

    .line 1323
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "isTxnSuccessful":Z
    :catch_0
    move-exception v1

    .line 1324
    .local v1, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Invalid data on track delete.  Skipping item."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1325
    .end local v1    # "e":Lcom/google/android/music/store/InvalidDataException;
    :catch_1
    move-exception v1

    .line 1326
    .local v1, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v3, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Http code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " on upstream track delete."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1328
    .end local v1    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_2
    move-exception v1

    .line 1329
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v4, "IO error on upstream track delete."

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1330
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 1331
    .local v1, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Server returned a bad request (400) error code for a track delete.  Ignoring."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1333
    .end local v1    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_4
    move-exception v1

    .line 1334
    .local v1, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Server returned a forbidden (401) error code for a track delete.  Ignoring."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1336
    .end local v1    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_5
    move-exception v1

    .line 1337
    .local v1, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Server returned a not found (404) error code for a track delete.  Ignoring."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1346
    .end local v1    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v2    # "isTxnSuccessful":Z
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method private handleInsertBlacklistItem(Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;)V
    .locals 10
    .param p1, "blacklistItem"    # Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/ConflictException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 1353
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v7, :cond_0

    .line 1354
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Upstream sender: Sending inserted blacklist item to cloud."

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1357
    :cond_0
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1358
    .local v0, "blacklistItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1359
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v8, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    invoke-interface {v7, v8, v0}, Lcom/google/android/music/sync/api/MusicApiClient;->mutateItems(Landroid/accounts/Account;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 1360
    .local v5, "mutateResponses":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;

    .line 1361
    .local v6, "response":Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;
    invoke-virtual {v6}, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;->throwExceptionFromResponseCode()V
    :try_end_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 1363
    .end local v0    # "blacklistItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "mutateResponses":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;>;"
    .end local v6    # "response":Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;
    :catch_0
    move-exception v2

    .line 1364
    .local v2, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v7, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Http code "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " on upstream blacklistitem insert."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v2}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 1366
    .end local v2    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_1
    move-exception v2

    .line 1367
    .local v2, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Invalid data on blacklistitem insert.  Skipping item."

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1384
    .end local v2    # "e":Lcom/google/android/music/store/InvalidDataException;
    :cond_1
    :goto_1
    const/4 v4, 0x0

    .line 1385
    .local v4, "isTxnSuccessful":Z
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v7}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1387
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-direct {p0, p1, v1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupInsert(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1388
    const/4 v4, 0x1

    .line 1390
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v7, v1, v4}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1392
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "isTxnSuccessful":Z
    :goto_2
    return-void

    .line 1368
    :catch_2
    move-exception v2

    .line 1369
    .local v2, "e":Ljava/io/IOException;
    new-instance v7, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v8, "IO error on upstream blacklistitem insert."

    invoke-direct {v7, v8, v2}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 1370
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 1371
    .local v2, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v7, :cond_1

    .line 1372
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Upstream sender: Server returned 400 on insert. Removing local copy."

    invoke-static {v7, v8, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 1375
    .end local v2    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_4
    move-exception v2

    .line 1376
    .local v2, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v7, :cond_1

    .line 1377
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Upstream sender: Server returned 403 on insert. Removing local copy."

    invoke-static {v7, v8, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 1380
    .end local v2    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_5
    move-exception v2

    .line 1381
    .local v2, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Unexpected 404 on blacklistitem insert.  Skipping."

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 1390
    .end local v2    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v4    # "isTxnSuccessful":Z
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v8, v1, v4}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v7
.end method

.method private handleInsertEntry(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)V
    .locals 6
    .param p1, "entry"    # Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ConflictException;
        }
    .end annotation

    .prologue
    .line 1076
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->fillInRemoteIdOfParentPlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1077
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_0

    .line 1078
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Upstream sender: Found an inserted playlist entry whose parent has no remote id.  Skipping it until the next sync."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1133
    :cond_0
    :goto_0
    return-void

    .line 1084
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->fillInRemoteIdForTrack(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1085
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_0

    .line 1086
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Upstream sender: Found an inserted playlist entry whose track has no remote id.  Skipping it until the next sync."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1091
    :cond_2
    const/4 v3, 0x0

    iput-object v3, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mAbsolutePosition:Ljava/lang/String;

    .line 1092
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_3

    .line 1093
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Sending inserted playlist entry to cloud."

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1096
    :cond_3
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    sget-object v5, Lcom/google/android/music/sync/api/MusicApiClient$OpType;->INSERT:Lcom/google/android/music/sync/api/MusicApiClient$OpType;

    invoke-interface {v3, v4, p1, v5}, Lcom/google/android/music/sync/api/MusicApiClient;->mutateItem(Landroid/accounts/Account;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Lcom/google/android/music/sync/api/MusicApiClient$OpType;)V
    :try_end_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    .line 1121
    :goto_1
    iget-object v3, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 1122
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "An inserted playlist entry was returned without a server id.  Skipping."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1097
    :catch_0
    move-exception v1

    .line 1098
    .local v1, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v3, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Http code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " on upstream playlist entry insert."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1100
    .end local v1    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_1
    move-exception v1

    .line 1102
    .local v1, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Invalid data on playlist entry insert.  Skipping item."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 1103
    .end local v1    # "e":Lcom/google/android/music/store/InvalidDataException;
    :catch_2
    move-exception v1

    .line 1104
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v4, "IO error on upstream playlist entry insert."

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1105
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 1106
    .local v1, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_4

    .line 1107
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Server returned 400 on insert. Removing local copy."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1110
    :cond_4
    iget-wide v4, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalId:J

    invoke-direct {p0, v4, v5}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->deletePlaylistEntry(J)V

    goto :goto_1

    .line 1111
    .end local v1    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_4
    move-exception v1

    .line 1112
    .local v1, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_5

    .line 1113
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Server returned 403 on insert. Removing local copy."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1116
    :cond_5
    iget-wide v4, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalId:J

    invoke-direct {p0, v4, v5}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->deletePlaylistEntry(J)V

    goto :goto_1

    .line 1117
    .end local v1    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_5
    move-exception v1

    .line 1118
    .local v1, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Unexpected 404 on playlist entry insert.  Skipping."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 1125
    .end local v1    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    :cond_6
    const/4 v2, 0x0

    .line 1126
    .local v2, "isTxnSuccessful":Z
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1128
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupInsert(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1129
    const/4 v2, 0x1

    .line 1131
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method private handleInsertPlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;)V
    .locals 6
    .param p1, "playlist"    # Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ConflictException;
        }
    .end annotation

    .prologue
    .line 1029
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_0

    .line 1030
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Sending inserted playlist to cloud."

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1033
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    sget-object v5, Lcom/google/android/music/sync/api/MusicApiClient$OpType;->INSERT:Lcom/google/android/music/sync/api/MusicApiClient$OpType;

    invoke-interface {v3, v4, p1, v5}, Lcom/google/android/music/sync/api/MusicApiClient;->mutateItem(Landroid/accounts/Account;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Lcom/google/android/music/sync/api/MusicApiClient$OpType;)V
    :try_end_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    .line 1058
    :goto_0
    iget-object v3, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1059
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "An inserted playlist was returned without a server id.  Skipping."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1070
    :goto_1
    return-void

    .line 1034
    :catch_0
    move-exception v1

    .line 1035
    .local v1, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v3, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Http code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " on upstream playlist insert."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1037
    .end local v1    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_1
    move-exception v1

    .line 1039
    .local v1, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Invalid data on playlist insert.  Skipping item."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1040
    .end local v1    # "e":Lcom/google/android/music/store/InvalidDataException;
    :catch_2
    move-exception v1

    .line 1041
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v4, "IO error on upstream playlist insert."

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1042
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 1043
    .local v1, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_1

    .line 1044
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Server returned 400 on insert. Removing local copy."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1047
    :cond_1
    iget-wide v4, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLocalId:J

    invoke-direct {p0, v4, v5}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->deletePlaylist(J)V

    goto :goto_0

    .line 1048
    .end local v1    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_4
    move-exception v1

    .line 1049
    .local v1, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_2

    .line 1050
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Server returned 403 on insert. Removing local copy."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1053
    :cond_2
    iget-wide v4, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLocalId:J

    invoke-direct {p0, v4, v5}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->deletePlaylist(J)V

    goto :goto_0

    .line 1054
    .end local v1    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_5
    move-exception v1

    .line 1055
    .local v1, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Unexpected 404 on playlist insert.  Skipping."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 1062
    .end local v1    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    :cond_3
    const/4 v2, 0x0

    .line 1063
    .local v2, "isTxnSuccessful":Z
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1065
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupInsert(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1066
    const/4 v2, 0x1

    .line 1068
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    goto :goto_1

    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method private handleInsertRadioStation(Lcom/google/android/music/sync/google/model/SyncableRadioStation;)V
    .locals 12
    .param p1, "radioStation"    # Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ConflictException;
        }
    .end annotation

    .prologue
    .line 661
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v7, :cond_0

    .line 662
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Upstream sender: Sending insert radio station to cloud."

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 665
    :cond_0
    :try_start_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 666
    .local v5, "radioStations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncableRadioStation;>;"
    invoke-interface {v5, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 667
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v8, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    invoke-interface {v7, v8, v5}, Lcom/google/android/music/sync/api/MusicApiClient;->mutateItems(Landroid/accounts/Account;Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 668
    .local v4, "mutateResponses":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;

    .line 669
    .local v6, "response":Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;
    invoke-virtual {v6}, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;->throwExceptionFromResponseCode()V
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 671
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "mutateResponses":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;>;"
    .end local v5    # "radioStations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncableRadioStation;>;"
    .end local v6    # "response":Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;
    :catch_0
    move-exception v1

    .line 672
    .local v1, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Invalid data on radio station insert.  Skipping item."

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 697
    .end local v1    # "e":Lcom/google/android/music/store/InvalidDataException;
    :cond_1
    :goto_1
    const/4 v3, 0x0

    .line 698
    .local v3, "isTxnSuccessful":Z
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v7}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 700
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupInsert(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 701
    const/4 v3, 0x1

    .line 703
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v7, v0, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 705
    return-void

    .line 673
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v3    # "isTxnSuccessful":Z
    :catch_1
    move-exception v1

    .line 674
    .local v1, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v7, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Http code "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " on upstream radio station insert. Removing local radio station"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v1}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 676
    .end local v1    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_2
    move-exception v1

    .line 677
    .local v1, "e":Ljava/io/IOException;
    new-instance v7, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v8, "IO error on upstream radio station insert."

    invoke-direct {v7, v8, v1}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 678
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 679
    .local v1, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v7, :cond_2

    .line 680
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Upstream sender received 400 on insert. Removing local radio station "

    invoke-static {v7, v8, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 683
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->getLocalId()J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->deleteRadioStation(J)V

    goto :goto_1

    .line 684
    .end local v1    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_4
    move-exception v1

    .line 685
    .local v1, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v7, :cond_3

    .line 686
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Upstream sender received 403 on insert. Removing local radio station "

    invoke-static {v7, v8, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 689
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->getLocalId()J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->deleteRadioStation(J)V

    goto :goto_1

    .line 690
    .end local v1    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_5
    move-exception v1

    .line 691
    .local v1, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v7, :cond_4

    .line 692
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Upstream sender received 404 on insert. Removing local radio station "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->getLocalId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 695
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->getLocalId()J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->deleteRadioStation(J)V

    goto/16 :goto_1

    .line 703
    .end local v1    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v3    # "isTxnSuccessful":Z
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v8, v0, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v7
.end method

.method private handleInsertTrack(Lcom/google/android/music/sync/google/model/Track;)V
    .locals 8
    .param p1, "track"    # Lcom/google/android/music/sync/google/model/Track;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ConflictException;
        }
    .end annotation

    .prologue
    .line 764
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_0

    .line 765
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Sending insert track to cloud."

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    sget-object v5, Lcom/google/android/music/sync/api/MusicApiClient$OpType;->INSERT:Lcom/google/android/music/sync/api/MusicApiClient$OpType;

    invoke-interface {v3, v4, p1, v5}, Lcom/google/android/music/sync/api/MusicApiClient;->mutateItem(Landroid/accounts/Account;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Lcom/google/android/music/sync/api/MusicApiClient$OpType;)V
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    .line 795
    :goto_0
    const/4 v2, 0x0

    .line 796
    .local v2, "isTxnSuccessful":Z
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 798
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupInsert(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 799
    const/4 v2, 0x1

    .line 801
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 803
    return-void

    .line 769
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "isTxnSuccessful":Z
    :catch_0
    move-exception v1

    .line 770
    .local v1, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Invalid data on track insert.  Skipping item."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 771
    .end local v1    # "e":Lcom/google/android/music/store/InvalidDataException;
    :catch_1
    move-exception v1

    .line 772
    .local v1, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v3, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Http code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " on upstream track insert. Removing local track"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 774
    .end local v1    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_2
    move-exception v1

    .line 775
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v4, "IO error on upstream track insert."

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 776
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 777
    .local v1, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_1

    .line 778
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender received 400 on insert. Removing local track "

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 781
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->getLocalId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/android/music/store/MusicFile;->deleteByLocalId(Landroid/content/Context;J)V

    goto :goto_0

    .line 782
    .end local v1    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_4
    move-exception v1

    .line 783
    .local v1, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_2

    .line 784
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender received 403 on insert. Removing local track "

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 787
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->getLocalId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/android/music/store/MusicFile;->deleteByLocalId(Landroid/content/Context;J)V

    goto :goto_0

    .line 788
    .end local v1    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_5
    move-exception v1

    .line 789
    .local v1, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_3

    .line 790
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Upstream sender received 404 on insert. Removing local track "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->getLocalId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 793
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->getLocalId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/android/music/store/MusicFile;->deleteByLocalId(Landroid/content/Context;J)V

    goto/16 :goto_0

    .line 801
    .end local v1    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v2    # "isTxnSuccessful":Z
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method private handleMutations(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/api/ConflictException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;
        }
    .end annotation

    .prologue
    .line 479
    .local p1, "subBlock":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->validateEntryParentsAndRemoteTrackIds(Ljava/util/List;)V

    .line 481
    const/4 v8, 0x0

    .line 482
    .local v8, "retryUnbatched":Z
    const/4 v5, 0x0

    .line 484
    .local v5, "mutateResponses":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;>;"
    :try_start_0
    iget-object v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v12, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    invoke-interface {v11, v12, p1}, Lcom/google/android/music/sync/api/MusicApiClient;->mutateItems(Landroid/accounts/Account;Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v5

    .line 504
    :goto_0
    if-nez v8, :cond_1

    .line 505
    invoke-direct {p0, p1, v5}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupBatchMutations(Ljava/util/List;Ljava/util/List;)V

    .line 566
    :cond_0
    return-void

    .line 485
    :catch_0
    move-exception v2

    .line 486
    .local v2, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v12, "At least one record was found with invalid data while handling a batched mutations."

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    const/4 v8, 0x1

    .line 503
    goto :goto_0

    .line 489
    .end local v2    # "e":Lcom/google/android/music/store/InvalidDataException;
    :catch_1
    move-exception v2

    .line 490
    .local v2, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v11, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Http code "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " on handling batched mutations."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12, v2}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v11

    .line 492
    .end local v2    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_2
    move-exception v2

    .line 493
    .local v2, "e":Ljava/io/IOException;
    new-instance v11, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v12, "IO error on handing batched mutations."

    invoke-direct {v11, v12, v2}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v11

    .line 494
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 495
    .local v2, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-object v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v12, "Bad-request returned while handling batched mutations."

    invoke-static {v11, v12, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 496
    const/4 v8, 0x1

    .line 503
    goto :goto_0

    .line 497
    .end local v2    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_4
    move-exception v2

    .line 498
    .local v2, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    iget-object v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v12, "Forbidden returned while handling batched mutations."

    invoke-static {v11, v12, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 499
    const/4 v8, 0x1

    .line 503
    goto :goto_0

    .line 500
    .end local v2    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_5
    move-exception v2

    .line 501
    .local v2, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    iget-object v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v12, "Not-found returned while handling batched mutations."

    invoke-static {v11, v12, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 502
    const/4 v8, 0x1

    goto :goto_0

    .line 509
    .end local v2    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    :cond_1
    iget-boolean v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v11, :cond_2

    .line 510
    iget-object v11, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v12, "Handling entity mutations individually due to one or more errors."

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    :cond_2
    const/4 v1, 0x0

    .line 513
    .local v1, "conflictCount":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_10

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 515
    .local v6, "mutation":Ljava/lang/Object;
    :try_start_1
    instance-of v11, v6, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    if-eqz v11, :cond_6

    .line 516
    const-class v11, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    invoke-virtual {v11, v6}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    .line 517
    .local v7, "playlist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    iget-boolean v11, v7, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mIsDeleted:Z

    if-eqz v11, :cond_4

    .line 518
    invoke-direct {p0, v7}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleDeletePlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;)V
    :try_end_1
    .catch Lcom/google/android/music/sync/api/ConflictException; {:try_start_1 .. :try_end_1} :catch_6

    goto :goto_1

    .line 556
    .end local v7    # "playlist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :catch_6
    move-exception v0

    .line 557
    .local v0, "ce":Lcom/google/android/music/sync/api/ConflictException;
    invoke-virtual {v0}, Lcom/google/android/music/sync/api/ConflictException;->getConflictCount()I

    move-result v11

    add-int/2addr v1, v11

    goto :goto_1

    .line 519
    .end local v0    # "ce":Lcom/google/android/music/sync/api/ConflictException;
    .restart local v7    # "playlist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :cond_4
    :try_start_2
    iget-object v11, v7, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    if-eqz v11, :cond_5

    .line 520
    invoke-direct {p0, v7}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleUpdatePlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;)V

    goto :goto_1

    .line 522
    :cond_5
    invoke-direct {p0, v7}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleInsertPlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;)V

    goto :goto_1

    .line 524
    .end local v7    # "playlist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :cond_6
    instance-of v11, v6, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    if-eqz v11, :cond_9

    .line 525
    const-class v11, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    invoke-virtual {v11, v6}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    .line 526
    .local v3, "entry":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    iget-boolean v11, v3, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mIsDeleted:Z

    if-eqz v11, :cond_7

    .line 527
    invoke-direct {p0, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleDeleteEntry(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)V

    goto :goto_1

    .line 528
    :cond_7
    iget-object v11, v3, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    if-eqz v11, :cond_8

    .line 529
    invoke-direct {p0, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleUpdateEntry(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)V

    goto :goto_1

    .line 531
    :cond_8
    invoke-direct {p0, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleInsertEntry(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)V

    goto :goto_1

    .line 533
    .end local v3    # "entry":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    :cond_9
    instance-of v11, v6, Lcom/google/android/music/sync/google/model/Track;

    if-eqz v11, :cond_b

    .line 534
    const-class v11, Lcom/google/android/music/sync/google/model/Track;

    invoke-virtual {v11, v6}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/music/sync/google/model/Track;

    .line 535
    .local v10, "track":Lcom/google/android/music/sync/google/model/Track;
    invoke-virtual {v10}, Lcom/google/android/music/sync/google/model/Track;->isInsert()Z

    move-result v11

    if-eqz v11, :cond_a

    .line 536
    invoke-direct {p0, v10}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleInsertTrack(Lcom/google/android/music/sync/google/model/Track;)V

    goto :goto_1

    .line 538
    :cond_a
    invoke-direct {p0, v10}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleUpdateTrack(Lcom/google/android/music/sync/google/model/Track;)V

    goto :goto_1

    .line 540
    .end local v10    # "track":Lcom/google/android/music/sync/google/model/Track;
    :cond_b
    instance-of v11, v6, Lcom/google/android/music/sync/google/model/TrackTombstone;

    if-eqz v11, :cond_c

    .line 541
    const-class v11, Lcom/google/android/music/sync/google/model/TrackTombstone;

    invoke-virtual {v11, v6}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/music/sync/google/model/TrackTombstone;

    .line 542
    .local v9, "tombstone":Lcom/google/android/music/sync/google/model/TrackTombstone;
    invoke-direct {p0, v9}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleDeleteTrack(Lcom/google/android/music/sync/google/model/TrackTombstone;)V

    goto :goto_1

    .line 543
    .end local v9    # "tombstone":Lcom/google/android/music/sync/google/model/TrackTombstone;
    :cond_c
    instance-of v11, v6, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    if-eqz v11, :cond_f

    .line 544
    const-class v11, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    invoke-virtual {v11, v6}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    .line 545
    .local v3, "entry":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    iget-boolean v11, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mIsDeleted:Z

    if-eqz v11, :cond_d

    .line 546
    invoke-direct {p0, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleDeleteRadioStation(Lcom/google/android/music/sync/google/model/SyncableRadioStation;)V

    goto/16 :goto_1

    .line 547
    :cond_d
    iget-object v11, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    if-eqz v11, :cond_e

    .line 548
    invoke-direct {p0, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleUpdateRadioStation(Lcom/google/android/music/sync/google/model/SyncableRadioStation;)V

    goto/16 :goto_1

    .line 550
    :cond_e
    invoke-direct {p0, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleInsertRadioStation(Lcom/google/android/music/sync/google/model/SyncableRadioStation;)V

    goto/16 :goto_1

    .line 552
    .end local v3    # "entry":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    :cond_f
    instance-of v11, v6, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    if-eqz v11, :cond_3

    .line 553
    const-class v11, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    invoke-virtual {v11, v6}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    .line 554
    .local v3, "entry":Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;
    invoke-direct {p0, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleInsertBlacklistItem(Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;)V
    :try_end_2
    .catch Lcom/google/android/music/sync/api/ConflictException; {:try_start_2 .. :try_end_2} :catch_6

    goto/16 :goto_1

    .line 560
    .end local v3    # "entry":Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;
    .end local v6    # "mutation":Ljava/lang/Object;
    :cond_10
    if-lez v1, :cond_0

    .line 561
    new-instance v0, Lcom/google/android/music/sync/api/ConflictException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " conflicts detected during individual mutations"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v0, v11}, Lcom/google/android/music/sync/api/ConflictException;-><init>(Ljava/lang/String;)V

    .line 563
    .restart local v0    # "ce":Lcom/google/android/music/sync/api/ConflictException;
    invoke-virtual {v0, v1}, Lcom/google/android/music/sync/api/ConflictException;->setConflictCount(I)V

    .line 564
    new-instance v11, Lcom/google/android/music/sync/api/ConflictException;

    invoke-direct {v11, v0}, Lcom/google/android/music/sync/api/ConflictException;-><init>(Ljava/lang/Throwable;)V

    throw v11
.end method

.method private handleUpdateEntry(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)V
    .locals 6
    .param p1, "entry"    # Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ConflictException;
        }
    .end annotation

    .prologue
    .line 1184
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->fillInRemoteIdOfParentPlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1185
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_0

    .line 1186
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Upstream sender: Found an updated playlist entry whose parent has no remote id.  Skipping it until the next sync."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1241
    :cond_0
    :goto_0
    return-void

    .line 1192
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->fillInRemoteIdForTrack(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1193
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_0

    .line 1194
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Upstream sender: Found an updated playlist entry whose track has no remote id.  Skipping it until the next sync."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1200
    :cond_2
    const/4 v3, 0x0

    iput-object v3, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mAbsolutePosition:Ljava/lang/String;

    .line 1201
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_3

    .line 1202
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Sending updated playlist entry to cloud."

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1205
    :cond_3
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    sget-object v5, Lcom/google/android/music/sync/api/MusicApiClient$OpType;->UPDATE:Lcom/google/android/music/sync/api/MusicApiClient$OpType;

    invoke-interface {v3, v4, p1, v5}, Lcom/google/android/music/sync/api/MusicApiClient;->mutateItem(Landroid/accounts/Account;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Lcom/google/android/music/sync/api/MusicApiClient$OpType;)V
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    .line 1233
    :goto_1
    const/4 v2, 0x0

    .line 1234
    .local v2, "isTxnSuccessful":Z
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1236
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupUpdate(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1237
    const/4 v2, 0x1

    .line 1239
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    goto :goto_0

    .line 1206
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "isTxnSuccessful":Z
    :catch_0
    move-exception v1

    .line 1208
    .local v1, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Invalid data on playlist entry update.  Skipping item."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 1209
    .end local v1    # "e":Lcom/google/android/music/store/InvalidDataException;
    :catch_1
    move-exception v1

    .line 1210
    .local v1, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v3, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Http code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " on upstream playlist entry update."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1212
    .end local v1    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_2
    move-exception v1

    .line 1213
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v4, "IO error on upstream playlist entry update."

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1214
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 1215
    .local v1, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_4

    .line 1216
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Server returned 400 on update. Restoring from server."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1219
    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->restoreItemFromServer(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)V

    goto :goto_1

    .line 1220
    .end local v1    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_4
    move-exception v1

    .line 1221
    .local v1, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_5

    .line 1222
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Server returned 403 on update. Restoring from server."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1225
    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->restoreItemFromServer(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)V

    goto :goto_1

    .line 1226
    .end local v1    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_5
    move-exception v1

    .line 1227
    .local v1, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_6

    .line 1228
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Server returned 404 on update. Removing local copy."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1231
    :cond_6
    iget-wide v4, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalId:J

    invoke-direct {p0, v4, v5}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->deletePlaylistEntry(J)V

    goto :goto_1

    .line 1239
    .end local v1    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v2    # "isTxnSuccessful":Z
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method private handleUpdatePlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;)V
    .locals 6
    .param p1, "playlist"    # Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ConflictException;
        }
    .end annotation

    .prologue
    .line 1138
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_0

    .line 1139
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Sending updated playlist to cloud."

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1142
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    sget-object v5, Lcom/google/android/music/sync/api/MusicApiClient$OpType;->UPDATE:Lcom/google/android/music/sync/api/MusicApiClient$OpType;

    invoke-interface {v3, v4, p1, v5}, Lcom/google/android/music/sync/api/MusicApiClient;->mutateItem(Landroid/accounts/Account;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Lcom/google/android/music/sync/api/MusicApiClient$OpType;)V
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    .line 1170
    :goto_0
    const/4 v2, 0x0

    .line 1171
    .local v2, "isTxnSuccessful":Z
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1173
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupUpdate(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1174
    const/4 v2, 0x1

    .line 1176
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1178
    return-void

    .line 1143
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "isTxnSuccessful":Z
    :catch_0
    move-exception v1

    .line 1145
    .local v1, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Invalid data on playlist update.  Skipping item."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1146
    .end local v1    # "e":Lcom/google/android/music/store/InvalidDataException;
    :catch_1
    move-exception v1

    .line 1147
    .local v1, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v3, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Http code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " on upstream playlist update."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1149
    .end local v1    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_2
    move-exception v1

    .line 1150
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v4, "IO error on upstream playlist update."

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1151
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 1152
    .local v1, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_1

    .line 1153
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Server returned 400 on update. Restoring from server."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1156
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->restoreItemFromServer(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)V

    goto :goto_0

    .line 1157
    .end local v1    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_4
    move-exception v1

    .line 1158
    .local v1, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_2

    .line 1159
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Server returned 403 on update. Restoring from server."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1162
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->restoreItemFromServer(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)V

    goto :goto_0

    .line 1163
    .end local v1    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_5
    move-exception v1

    .line 1164
    .local v1, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_3

    .line 1165
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Server returned 404 on update. Removing local copy."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1168
    :cond_3
    iget-wide v4, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLocalId:J

    invoke-direct {p0, v4, v5}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->deletePlaylist(J)V

    goto :goto_0

    .line 1176
    .end local v1    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v2    # "isTxnSuccessful":Z
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method private handleUpdateRadioStation(Lcom/google/android/music/sync/google/model/SyncableRadioStation;)V
    .locals 12
    .param p1, "radioStation"    # Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ConflictException;
        }
    .end annotation

    .prologue
    .line 612
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v7, :cond_0

    .line 613
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Upstream sender: Sending updated radio station to cloud."

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    :cond_0
    :try_start_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 617
    .local v5, "radioStations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncableRadioStation;>;"
    invoke-interface {v5, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 618
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v8, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    invoke-interface {v7, v8, v5}, Lcom/google/android/music/sync/api/MusicApiClient;->mutateItems(Landroid/accounts/Account;Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 619
    .local v4, "mutateResponses":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;

    .line 620
    .local v6, "response":Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;
    invoke-virtual {v6}, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;->throwExceptionFromResponseCode()V
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 622
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "mutateResponses":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;>;"
    .end local v5    # "radioStations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncableRadioStation;>;"
    .end local v6    # "response":Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;
    :catch_0
    move-exception v1

    .line 623
    .local v1, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Invalid data on radio station update.  Skipping item."

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 648
    .end local v1    # "e":Lcom/google/android/music/store/InvalidDataException;
    :cond_1
    :goto_1
    const/4 v3, 0x0

    .line 649
    .local v3, "isTxnSuccessful":Z
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v7}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 651
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupUpdate(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 652
    const/4 v3, 0x1

    .line 654
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v7, v0, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 656
    return-void

    .line 624
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v3    # "isTxnSuccessful":Z
    :catch_1
    move-exception v1

    .line 625
    .local v1, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v7, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Http code "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " on upstream radio station update."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v1}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 627
    .end local v1    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_2
    move-exception v1

    .line 628
    .local v1, "e":Ljava/io/IOException;
    new-instance v7, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v8, "IO error on upstream radio station update."

    invoke-direct {v7, v8, v1}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 629
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 630
    .local v1, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v7, :cond_2

    .line 631
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Upstream sender received 400 on update. Restoring from server."

    invoke-static {v7, v8, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 634
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->restoreItemFromServer(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)V

    goto :goto_1

    .line 635
    .end local v1    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_4
    move-exception v1

    .line 636
    .local v1, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v7, :cond_3

    .line 637
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v8, "Upstream sender received 403 on update. Restoring from server."

    invoke-static {v7, v8, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 640
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->restoreItemFromServer(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)V

    goto :goto_1

    .line 641
    .end local v1    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_5
    move-exception v1

    .line 642
    .local v1, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v7, :cond_4

    .line 643
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Upstream sender received 404 on update. Removing local radio station "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->getLocalId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 646
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->getLocalId()J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->deleteRadioStation(J)V

    goto/16 :goto_1

    .line 654
    .end local v1    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v3    # "isTxnSuccessful":Z
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v8, v0, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v7
.end method

.method private handleUpdateTrack(Lcom/google/android/music/sync/google/model/Track;)V
    .locals 8
    .param p1, "track"    # Lcom/google/android/music/sync/google/model/Track;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ConflictException;
        }
    .end annotation

    .prologue
    .line 710
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_0

    .line 711
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender: Sending updated track to cloud."

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    sget-object v5, Lcom/google/android/music/sync/api/MusicApiClient$OpType;->UPDATE:Lcom/google/android/music/sync/api/MusicApiClient$OpType;

    invoke-interface {v3, v4, p1, v5}, Lcom/google/android/music/sync/api/MusicApiClient;->mutateItem(Landroid/accounts/Account;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Lcom/google/android/music/sync/api/MusicApiClient$OpType;)V
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    .line 751
    :goto_0
    const/4 v2, 0x0

    .line 752
    .local v2, "isTxnSuccessful":Z
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 754
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->cleanupUpdate(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 755
    const/4 v2, 0x1

    .line 757
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 759
    return-void

    .line 715
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "isTxnSuccessful":Z
    :catch_0
    move-exception v1

    .line 716
    .local v1, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Invalid data on track update.  Skipping item."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 717
    .end local v1    # "e":Lcom/google/android/music/store/InvalidDataException;
    :catch_1
    move-exception v1

    .line 718
    .local v1, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v3, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Http code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " on upstream track update."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 720
    .end local v1    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_2
    move-exception v1

    .line 721
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v4, "IO error on upstream track update."

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 722
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 723
    .local v1, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_1

    .line 724
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender received 400 on update. Restoring from server."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 728
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->hasLockerId()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 729
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->restoreItemFromServer(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)V

    goto :goto_0

    .line 731
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->getLocalId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/android/music/store/MusicFile;->deleteByLocalId(Landroid/content/Context;J)V

    goto :goto_0

    .line 733
    .end local v1    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_4
    move-exception v1

    .line 734
    .local v1, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_3

    .line 735
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v4, "Upstream sender received 403 on update. Restoring from server."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 739
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->hasLockerId()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 740
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->restoreItemFromServer(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)V

    goto :goto_0

    .line 742
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->getLocalId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/android/music/store/MusicFile;->deleteByLocalId(Landroid/content/Context;J)V

    goto/16 :goto_0

    .line 744
    .end local v1    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_5
    move-exception v1

    .line 745
    .local v1, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v3, :cond_5

    .line 746
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Upstream sender received 404 on update. Removing local track "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->getLocalId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 749
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->getLocalId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/android/music/store/MusicFile;->deleteByLocalId(Landroid/content/Context;J)V

    goto/16 :goto_0

    .line 757
    .end local v1    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v2    # "isTxnSuccessful":Z
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method private mapStringResponseCodeToEnum(Ljava/lang/String;)Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;
    .locals 7
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/HardSyncException;
        }
    .end annotation

    .prologue
    .line 175
    invoke-static {}, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;->values()[Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 176
    .local v1, "code":Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;
    invoke-virtual {v1}, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 177
    return-object v1

    .line 175
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 180
    .end local v1    # "code":Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;
    :cond_1
    new-instance v4, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to map a batch-mutate response code string "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to a known enum value!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private reportTrackStats(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/music/sync/common/QueueableSyncEntity;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;
        }
    .end annotation

    .prologue
    .line 808
    .local p1, "entitySublist":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/music/sync/common/QueueableSyncEntity;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 809
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v3, "Track stats list is null or empty. Skip reporting."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 850
    :cond_1
    :goto_0
    return-void

    .line 813
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mEnableTrackStatsUpSync:Z

    if-nez v2, :cond_3

    .line 814
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v3, "Track stats up sync is disabled. This is a no-op."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 818
    :cond_3
    const/4 v0, 0x1

    .line 820
    .local v0, "cleanupTrackStats":Z
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    invoke-interface {v2, v3, p1}, Lcom/google/android/music/sync/api/MusicApiClient;->reportTrackStats(Landroid/accounts/Account;Ljava/util/List;)V

    .line 821
    iget-boolean v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    if-eqz v2, :cond_4

    .line 822
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Reported "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " TrackStats to the server."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/music/sync/api/ServiceUnavailableException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/google/android/music/sync/api/ConflictException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 846
    :cond_4
    if-eqz v0, :cond_1

    .line 847
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->resetTrackStats(Ljava/util/List;)V

    goto :goto_0

    .line 824
    :catch_0
    move-exception v1

    .line 825
    .local v1, "e":Lcom/google/android/music/sync/api/ServiceUnavailableException;
    const/4 v0, 0x0

    .line 828
    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 846
    .end local v1    # "e":Lcom/google/android/music/sync/api/ServiceUnavailableException;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_5

    .line 847
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->resetTrackStats(Ljava/util/List;)V

    :cond_5
    throw v2

    .line 829
    :catch_1
    move-exception v1

    .line 830
    .local v1, "e":Ljava/io/IOException;
    const/4 v0, 0x0

    .line 831
    :try_start_2
    new-instance v2, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v3, "IO error on reporting track stats."

    invoke-direct {v2, v3, v1}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 832
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 833
    .local v1, "e":Lcom/google/android/music/sync/api/BadRequestException;
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v3, "Bad request returned while reporting track stats."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 846
    if-eqz v0, :cond_1

    .line 847
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->resetTrackStats(Ljava/util/List;)V

    goto :goto_0

    .line 834
    .end local v1    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_3
    move-exception v1

    .line 835
    .local v1, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :try_start_3
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v3, "Forbidden returned while reporting track stats."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 846
    if-eqz v0, :cond_1

    .line 847
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->resetTrackStats(Ljava/util/List;)V

    goto :goto_0

    .line 836
    .end local v1    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_4
    move-exception v1

    .line 837
    .local v1, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    :try_start_4
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v3, "Not-found returned while reporting track stats."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 846
    if-eqz v0, :cond_1

    .line 847
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->resetTrackStats(Ljava/util/List;)V

    goto :goto_0

    .line 838
    .end local v1    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    :catch_5
    move-exception v1

    .line 839
    .local v1, "e":Lcom/google/android/music/sync/api/NotModifiedException;
    :try_start_5
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v3, "Not-modified returned while reporting track stats."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 846
    if-eqz v0, :cond_1

    .line 847
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->resetTrackStats(Ljava/util/List;)V

    goto/16 :goto_0

    .line 840
    .end local v1    # "e":Lcom/google/android/music/sync/api/NotModifiedException;
    :catch_6
    move-exception v1

    .line 841
    .local v1, "e":Lcom/google/android/music/sync/api/ConflictException;
    :try_start_6
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v3, "Conflicts returned while reporting track stats."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 846
    if-eqz v0, :cond_1

    .line 847
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->resetTrackStats(Ljava/util/List;)V

    goto/16 :goto_0

    .line 842
    .end local v1    # "e":Lcom/google/android/music/sync/api/ConflictException;
    :catch_7
    move-exception v1

    .line 843
    .local v1, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :try_start_7
    new-instance v2, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Http code "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " on reporting track stats."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method

.method private resetTrackStats(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/music/sync/common/QueueableSyncEntity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 853
    .local p1, "entitySublist":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/music/sync/common/QueueableSyncEntity;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 854
    .local v4, "trackIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v5

    .line 855
    .local v5, "trackRemoteIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/sync/common/QueueableSyncEntity;

    .line 856
    .local v3, "item":Lcom/google/android/music/sync/common/QueueableSyncEntity;
    const-class v6, Lcom/google/android/music/sync/google/model/TrackStat;

    invoke-virtual {v6, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/sync/google/model/TrackStat;

    invoke-virtual {v6}, Lcom/google/android/music/sync/google/model/TrackStat;->getLocalId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 857
    const-class v6, Lcom/google/android/music/sync/google/model/TrackStat;

    invoke-virtual {v6, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/sync/google/model/TrackStat;

    invoke-virtual {v6}, Lcom/google/android/music/sync/google/model/TrackStat;->getRemoteId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 860
    .end local v3    # "item":Lcom/google/android/music/sync/common/QueueableSyncEntity;
    :cond_0
    const/4 v2, 0x0

    .line 861
    .local v2, "isTxnSuccessful":Z
    const/4 v0, 0x0

    .line 863
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v6}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 864
    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicFile;->resetPlayCount(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)V

    .line 865
    invoke-static {v0, v5}, Lcom/google/android/music/store/Store;->removeEvents(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 866
    const/4 v2, 0x1

    .line 868
    if-eqz v0, :cond_1

    .line 869
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v6, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 872
    :cond_1
    return-void

    .line 868
    :catchall_0
    move-exception v6

    if-eqz v0, :cond_2

    .line 869
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v7, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    :cond_2
    throw v6
.end method

.method private restoreItemFromServer(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)V
    .locals 12
    .param p1, "clientItem"    # Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 964
    const/4 v11, 0x0

    .line 965
    .local v11, "serverItem":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    const/4 v10, 0x0

    .line 967
    .local v10, "justDeleteTheLocalCopy":Z
    :try_start_0
    instance-of v1, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    if-eqz v1, :cond_3

    .line 968
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getRemoteId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/google/android/music/sync/api/MusicApiClient;->getPlaylist(Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_6

    move-result-object v11

    .line 993
    :goto_0
    if-eqz v10, :cond_0

    .line 995
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "serverItem":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    check-cast v11, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_8

    .line 1002
    .restart local v11    # "serverItem":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    const/4 v1, 0x1

    invoke-interface {v11, v1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->setIsDeleted(Z)V

    .line 1003
    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getRemoteId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11, v1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->setRemoteId(Ljava/lang/String;)V

    .line 1007
    :cond_0
    const/4 v7, 0x0

    .line 1008
    .local v7, "blockMerger":Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1009
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v9, 0x0

    .line 1011
    .local v9, "isTxnSuccessful":Z
    :try_start_2
    new-instance v0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;

    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mProtocolState:Ljava/util/Map;

    iget-boolean v4, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mUseVerboseLogging:Z

    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccountHash:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;-><init>(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Map;ZLjava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1013
    .end local v7    # "blockMerger":Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;
    .local v0, "blockMerger":Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;
    :try_start_3
    invoke-virtual {v0, v11, p1}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->processMergeItem(Lcom/google/android/music/sync/common/QueueableSyncEntity;Lcom/google/android/music/sync/common/QueueableSyncEntity;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1014
    const/4 v9, 0x1

    .line 1016
    if-eqz v0, :cond_1

    .line 1017
    invoke-virtual {v0}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->safelyCloseStatements()V

    .line 1019
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v1, v2, v9}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1020
    if-eqz v0, :cond_2

    if-eqz v9, :cond_2

    .line 1021
    invoke-virtual {v0}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->cleanupLocallyCachedFiles()V

    .line 1024
    .end local v0    # "blockMerger":Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v9    # "isTxnSuccessful":Z
    :cond_2
    return-void

    .line 969
    :cond_3
    :try_start_4
    instance-of v1, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    if-eqz v1, :cond_4

    .line 970
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getRemoteId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/google/android/music/sync/api/MusicApiClient;->getPlaylistEntry(Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    move-result-object v11

    goto :goto_0

    .line 971
    :cond_4
    instance-of v1, p1, Lcom/google/android/music/sync/google/model/Track;

    if-eqz v1, :cond_2

    .line 972
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mClient:Lcom/google/android/music/sync/api/MusicApiClient;

    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mAccount:Landroid/accounts/Account;

    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getRemoteId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/google/android/music/sync/api/MusicApiClient;->getTrack(Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/Track;
    :try_end_4
    .catch Landroid/accounts/AuthenticatorException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_4 .. :try_end_4} :catch_6

    move-result-object v11

    goto :goto_0

    .line 976
    :catch_0
    move-exception v8

    .line 977
    .local v8, "e":Landroid/accounts/AuthenticatorException;
    new-instance v1, Landroid/accounts/AuthenticatorException;

    const-string v3, "Unable to restore item due to auth error."

    invoke-direct {v1, v3, v8}, Landroid/accounts/AuthenticatorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 978
    .end local v8    # "e":Landroid/accounts/AuthenticatorException;
    :catch_1
    move-exception v8

    .line 979
    .local v8, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    new-instance v1, Lcom/google/android/music/sync/common/HardSyncException;

    const-string v3, "Unable to restore item due to http error."

    invoke-direct {v1, v3, v8}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 980
    .end local v8    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    :catch_2
    move-exception v8

    .line 981
    .local v8, "e":Ljava/io/IOException;
    new-instance v1, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v3, "Unable to restore item due to io error."

    invoke-direct {v1, v3, v8}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 982
    .end local v8    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v8

    .line 983
    .local v8, "e":Lcom/google/android/music/sync/api/NotModifiedException;
    new-instance v1, Lcom/google/android/music/sync/common/HardSyncException;

    const-string v3, "Received not-modified in response to a restore."

    invoke-direct {v1, v3, v8}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 984
    .end local v8    # "e":Lcom/google/android/music/sync/api/NotModifiedException;
    :catch_4
    move-exception v8

    .line 986
    .local v8, "e":Lcom/google/android/music/sync/api/BadRequestException;
    const/4 v10, 0x1

    .line 992
    goto/16 :goto_0

    .line 987
    .end local v8    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    :catch_5
    move-exception v8

    .line 989
    .local v8, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    const/4 v10, 0x1

    .line 992
    goto/16 :goto_0

    .line 990
    .end local v8    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    :catch_6
    move-exception v8

    .line 991
    .local v8, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 996
    .end local v8    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    .end local v11    # "serverItem":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    :catch_7
    move-exception v8

    .line 998
    .local v8, "e":Ljava/lang/InstantiationException;
    new-instance v1, Lcom/google/android/music/sync/common/HardSyncException;

    invoke-direct {v1, v8}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 999
    .end local v8    # "e":Ljava/lang/InstantiationException;
    :catch_8
    move-exception v8

    .line 1000
    .local v8, "e":Ljava/lang/IllegalAccessException;
    new-instance v1, Lcom/google/android/music/sync/common/HardSyncException;

    invoke-direct {v1, v8}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1016
    .end local v8    # "e":Ljava/lang/IllegalAccessException;
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v7    # "blockMerger":Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;
    .restart local v9    # "isTxnSuccessful":Z
    .restart local v11    # "serverItem":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    :catchall_0
    move-exception v1

    move-object v0, v7

    .end local v7    # "blockMerger":Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;
    .restart local v0    # "blockMerger":Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;
    :goto_1
    if-eqz v0, :cond_5

    .line 1017
    invoke-virtual {v0}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->safelyCloseStatements()V

    .line 1019
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v2, v9}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1020
    if-eqz v0, :cond_6

    if-eqz v9, :cond_6

    .line 1021
    invoke-virtual {v0}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->cleanupLocallyCachedFiles()V

    :cond_6
    throw v1

    .line 1016
    :catchall_1
    move-exception v1

    goto :goto_1
.end method

.method private validateEntryParentsAndRemoteTrackIds(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 416
    .local p1, "subBlock":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    const/4 v5, 0x0

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    instance-of v5, v5, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    if-nez v5, :cond_1

    .line 461
    :cond_0
    return-void

    .line 421
    :cond_1
    const/4 v2, 0x0

    .line 422
    .local v2, "indexesToRemove":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 423
    .local v4, "listSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_7

    .line 424
    const-class v5, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    .line 425
    .local v0, "entry":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    iget-object v5, v0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemotePlaylistId:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 423
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 429
    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->fillInRemoteIdOfParentPlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 432
    if-nez v2, :cond_4

    .line 433
    new-instance v2, Ljava/util/LinkedList;

    .end local v2    # "indexesToRemove":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Integer;>;"
    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 435
    .restart local v2    # "indexesToRemove":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Integer;>;"
    :cond_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 438
    :cond_5
    invoke-direct {p0, v0}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->fillInRemoteIdForTrack(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 440
    if-nez v2, :cond_6

    .line 441
    new-instance v2, Ljava/util/LinkedList;

    .end local v2    # "indexesToRemove":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Integer;>;"
    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 443
    .restart local v2    # "indexesToRemove":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Integer;>;"
    :cond_6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 450
    .end local v0    # "entry":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    :cond_7
    if-eqz v2, :cond_0

    .line 452
    invoke-virtual {v2}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v3

    .line 453
    .local v3, "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Ljava/lang/Integer;>;"
    :goto_2
    invoke-interface {v3}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 454
    invoke-interface {v3}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {p1, v5}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_2
.end method


# virtual methods
.method protected processUpstreamEntityBlock(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/common/QueueableSyncEntity;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;,
            Lcom/google/android/music/sync/common/ConflictDetectedException;
        }
    .end annotation

    .prologue
    .line 127
    .local p1, "block":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/common/QueueableSyncEntity;>;"
    new-instance v8, Lcom/google/android/music/sync/google/ListPartitioner;

    invoke-direct {v8}, Lcom/google/android/music/sync/google/ListPartitioner;-><init>()V

    .line 129
    .local v8, "partitioner":Lcom/google/android/music/sync/google/ListPartitioner;, "Lcom/google/android/music/sync/google/ListPartitioner<Lcom/google/android/music/sync/common/QueueableSyncEntity;>;"
    const-class v9, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    invoke-virtual {v8, v9}, Lcom/google/android/music/sync/google/ListPartitioner;->addPartitioningClass(Ljava/lang/Class;)V

    .line 130
    const-class v9, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    invoke-virtual {v8, v9}, Lcom/google/android/music/sync/google/ListPartitioner;->addPartitioningClass(Ljava/lang/Class;)V

    .line 131
    const-class v9, Lcom/google/android/music/sync/google/model/Track;

    invoke-virtual {v8, v9}, Lcom/google/android/music/sync/google/ListPartitioner;->addPartitioningClass(Ljava/lang/Class;)V

    .line 132
    const-class v9, Lcom/google/android/music/sync/google/model/TrackStat;

    invoke-virtual {v8, v9}, Lcom/google/android/music/sync/google/ListPartitioner;->addPartitioningClass(Ljava/lang/Class;)V

    .line 133
    const-class v9, Lcom/google/android/music/sync/google/model/TrackTombstone;

    invoke-virtual {v8, v9}, Lcom/google/android/music/sync/google/ListPartitioner;->addPartitioningClass(Ljava/lang/Class;)V

    .line 134
    const-class v9, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    invoke-virtual {v8, v9}, Lcom/google/android/music/sync/google/ListPartitioner;->addPartitioningClass(Ljava/lang/Class;)V

    .line 135
    const-class v9, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    invoke-virtual {v8, v9}, Lcom/google/android/music/sync/google/ListPartitioner;->addPartitioningClass(Ljava/lang/Class;)V

    .line 136
    const/4 v7, 0x0

    .line 138
    .local v7, "listOfSubblocks":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<+Lcom/google/android/music/sync/common/QueueableSyncEntity;>;>;"
    :try_start_0
    invoke-virtual {v8, p1}, Lcom/google/android/music/sync/google/ListPartitioner;->partition(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 143
    const/4 v1, 0x0

    .line 144
    .local v1, "conflictCount":I
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 145
    .local v3, "entitySublist":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/music/sync/common/QueueableSyncEntity;>;"
    const/4 v9, 0x0

    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/sync/common/QueueableSyncEntity;

    .line 146
    .local v5, "firstItem":Lcom/google/android/music/sync/common/QueueableSyncEntity;
    instance-of v9, v5, Lcom/google/android/music/sync/google/model/TrackStat;

    if-eqz v9, :cond_0

    .line 147
    invoke-direct {p0, v3}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->reportTrackStats(Ljava/util/List;)V

    goto :goto_0

    .line 139
    .end local v1    # "conflictCount":I
    .end local v3    # "entitySublist":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/music/sync/common/QueueableSyncEntity;>;"
    .end local v5    # "firstItem":Lcom/google/android/music/sync/common/QueueableSyncEntity;
    .end local v6    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 140
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v10, "Unable to partition the client changes into syncable entities:"

    invoke-static {v9, v10, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 141
    new-instance v9, Lcom/google/android/music/sync/common/HardSyncException;

    invoke-direct {v9, v2}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/Throwable;)V

    throw v9

    .line 149
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v1    # "conflictCount":I
    .restart local v3    # "entitySublist":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/music/sync/common/QueueableSyncEntity;>;"
    .restart local v5    # "firstItem":Lcom/google/android/music/sync/common/QueueableSyncEntity;
    .restart local v6    # "i$":Ljava/util/Iterator;
    :cond_0
    instance-of v9, v5, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    if-eqz v9, :cond_1

    .line 153
    :try_start_1
    const-class v9, Ljava/util/List;

    invoke-virtual {v9, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    invoke-direct {p0, v9}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleMutations(Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 154
    :catch_1
    move-exception v2

    .line 156
    .local v2, "e":Ljava/lang/Exception;
    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicUpstreamSender;->mTag:Ljava/lang/String;

    const-string v10, "Exception when sending blacklist items"

    invoke-static {v9, v10, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 162
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_2
    const-class v9, Ljava/util/List;

    invoke-virtual {v9, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    invoke-direct {p0, v9}, Lcom/google/android/music/sync/google/MusicUpstreamSender;->handleMutations(Ljava/util/List;)V
    :try_end_2
    .catch Lcom/google/android/music/sync/api/ConflictException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 163
    :catch_2
    move-exception v0

    .line 164
    .local v0, "ce":Lcom/google/android/music/sync/api/ConflictException;
    invoke-virtual {v0}, Lcom/google/android/music/sync/api/ConflictException;->getConflictCount()I

    move-result v9

    add-int/2addr v1, v9

    goto :goto_0

    .line 167
    .end local v0    # "ce":Lcom/google/android/music/sync/api/ConflictException;
    .end local v3    # "entitySublist":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/music/sync/common/QueueableSyncEntity;>;"
    .end local v5    # "firstItem":Lcom/google/android/music/sync/common/QueueableSyncEntity;
    :cond_2
    if-eqz v1, :cond_3

    .line 168
    new-instance v4, Lcom/google/android/music/sync/common/ConflictDetectedException;

    invoke-direct {v4}, Lcom/google/android/music/sync/common/ConflictDetectedException;-><init>()V

    .line 169
    .local v4, "ex":Lcom/google/android/music/sync/common/ConflictDetectedException;
    invoke-virtual {v4, v1}, Lcom/google/android/music/sync/common/ConflictDetectedException;->setConflictCount(I)V

    .line 170
    throw v4

    .line 172
    .end local v4    # "ex":Lcom/google/android/music/sync/common/ConflictDetectedException;
    :cond_3
    return-void
.end method
