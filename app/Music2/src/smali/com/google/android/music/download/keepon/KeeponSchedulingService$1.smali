.class Lcom/google/android/music/download/keepon/KeeponSchedulingService$1;
.super Ljava/lang/Object;
.source "KeeponSchedulingService.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/keepon/KeeponSchedulingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/keepon/KeeponSchedulingService;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/keepon/KeeponSchedulingService;)V
    .locals 0

    .prologue
    .line 568
    iput-object p1, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService$1;->this$0:Lcom/google/android/music/download/keepon/KeeponSchedulingService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 5
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 572
    iget-object v3, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService$1;->this$0:Lcom/google/android/music/download/keepon/KeeponSchedulingService;

    invoke-virtual {v3}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->isDownloadingPaused()Z

    move-result v1

    .line 573
    .local v1, "isDownloadingPaused":Z
    iget-object v3, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService$1;->this$0:Lcom/google/android/music/download/keepon/KeeponSchedulingService;

    invoke-virtual {v3}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 574
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/music/download/keepon/KeeponSchedulingService;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 575
    .local v2, "serviceIntent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 576
    iget-object v3, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService$1;->this$0:Lcom/google/android/music/download/keepon/KeeponSchedulingService;

    iget-object v4, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService$1;->this$0:Lcom/google/android/music/download/keepon/KeeponSchedulingService;

    # getter for: Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mStartId:I
    invoke-static {v4}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->access$100(Lcom/google/android/music/download/keepon/KeeponSchedulingService;)I

    move-result v4

    # invokes: Lcom/google/android/music/download/keepon/KeeponSchedulingService;->sendCancelDownloadsMessage(I)V
    invoke-static {v3, v4}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->access$200(Lcom/google/android/music/download/keepon/KeeponSchedulingService;I)V

    .line 582
    :goto_0
    return-void

    .line 579
    :cond_0
    const-string v3, "com.google.android.music.download.keepon.KeeponSchedulingService.START_DOWNLOAD"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 580
    invoke-virtual {v0, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method
