.class Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;
.super Ljava/lang/Object;
.source "AsyncMixCreatorWorker.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/mix/AsyncMixCreatorWorker;->startMixCreation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mIsPublicPlaylist:Z

.field private mNumOfSongs:I

.field private mPlaylistName:Ljava/lang/String;

.field private mSourceAccount:I

.field final synthetic this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;


# direct methods
.method constructor <init>(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 188
    iput-object p1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    iput v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->mNumOfSongs:I

    .line 192
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->mPlaylistName:Ljava/lang/String;

    .line 193
    iput-boolean v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->mIsPublicPlaylist:Z

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 197
    iget-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mLocalId:J
    invoke-static {v0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$000(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 198
    iput-boolean v5, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->mIsPublicPlaylist:Z

    .line 224
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$100(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mLocalId:J
    invoke-static {v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$000(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)J

    move-result-wide v2

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/music/store/MusicContent$Playlists$Members;->getPlaylistItemsCount(Landroid/content/Context;JZ)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->mNumOfSongs:I

    .line 202
    const/4 v6, 0x0

    .line 206
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$100(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mLocalId:J
    invoke-static {v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$000(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "playlist_name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "SourceAccount"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 214
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 215
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 216
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->mPlaylistName:Ljava/lang/String;

    .line 218
    :cond_1
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->mSourceAccount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    :cond_2
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public taskCompleted()V
    .locals 6

    .prologue
    .line 228
    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$100(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_0

    .line 252
    :goto_0
    return-void

    .line 232
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->mIsPublicPlaylist:Z

    if-eqz v1, :cond_1

    .line 233
    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # invokes: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->createMix()V
    invoke-static {v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$200(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)V

    goto :goto_0

    .line 234
    :cond_1
    iget v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->mNumOfSongs:I

    if-nez v1, :cond_2

    .line 235
    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$100(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0322

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->mPlaylistName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "errorMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mCallback:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;
    invoke-static {v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$300(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;->onFailure(Ljava/lang/String;)V

    goto :goto_0

    .line 238
    .end local v0    # "errorMsg":Ljava/lang/String;
    :cond_2
    iget v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->mSourceAccount:I

    if-eqz v1, :cond_4

    .line 239
    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mRemoteSeedId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$400(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 240
    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    iget-object v2, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->mPlaylistName:Ljava/lang/String;

    # setter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mName:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$502(Lcom/google/android/music/mix/AsyncMixCreatorWorker;Ljava/lang/String;)Ljava/lang/String;

    .line 241
    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # invokes: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->createMix()V
    invoke-static {v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$200(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)V

    goto :goto_0

    .line 243
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$100(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b01d6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 245
    .restart local v0    # "errorMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mCallback:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;
    invoke-static {v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$300(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;->onFailure(Ljava/lang/String;)V

    goto :goto_0

    .line 248
    .end local v0    # "errorMsg":Ljava/lang/String;
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$100(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0321

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 250
    .restart local v0    # "errorMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mCallback:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;
    invoke-static {v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$300(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;->onFailure(Ljava/lang/String;)V

    goto :goto_0
.end method
