.class Lcom/google/android/music/art/ArtLoaderImpl$1;
.super Ljava/lang/Object;
.source "ArtLoaderImpl.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtLoaderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/Runnable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/art/ArtLoaderImpl;


# direct methods
.method constructor <init>(Lcom/google/android/music/art/ArtLoaderImpl;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/music/art/ArtLoaderImpl$1;->this$0:Lcom/google/android/music/art/ArtLoaderImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private extractRequestRunnable(Ljava/lang/Runnable;)Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 90
    check-cast p1, Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor$ArtLoaderImplFutureTask;

    .end local p1    # "runnable":Ljava/lang/Runnable;
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor$ArtLoaderImplFutureTask;->getRunnable()Ljava/lang/Runnable;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 67
    check-cast p1, Ljava/lang/Runnable;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Runnable;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/art/ArtLoaderImpl$1;->compare(Ljava/lang/Runnable;Ljava/lang/Runnable;)I

    move-result v0

    return v0
.end method

.method public compare(Ljava/lang/Runnable;Ljava/lang/Runnable;)I
    .locals 4
    .param p1, "lhs"    # Ljava/lang/Runnable;
    .param p2, "rhs"    # Ljava/lang/Runnable;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtLoaderImpl$1;->extractRequestRunnable(Ljava/lang/Runnable;)Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;

    move-result-object v0

    .line 71
    .local v0, "left":Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;
    invoke-direct {p0, p2}, Lcom/google/android/music/art/ArtLoaderImpl$1;->extractRequestRunnable(Ljava/lang/Runnable;)Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;

    move-result-object v1

    .line 77
    .local v1, "right":Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;
    # getter for: Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mListener:Lcom/google/android/music/art/ArtLoader$Listener;
    invoke-static {v0}, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->access$000(Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;)Lcom/google/android/music/art/ArtLoader$Listener;

    move-result-object v2

    if-nez v2, :cond_0

    # getter for: Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mListener:Lcom/google/android/music/art/ArtLoader$Listener;
    invoke-static {v1}, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->access$000(Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;)Lcom/google/android/music/art/ArtLoader$Listener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 79
    const/4 v2, 0x1

    .line 86
    :goto_0
    return v2

    .line 80
    :cond_0
    # getter for: Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mListener:Lcom/google/android/music/art/ArtLoader$Listener;
    invoke-static {v0}, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->access$000(Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;)Lcom/google/android/music/art/ArtLoader$Listener;

    move-result-object v2

    if-eqz v2, :cond_1

    # getter for: Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mListener:Lcom/google/android/music/art/ArtLoader$Listener;
    invoke-static {v1}, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->access$000(Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;)Lcom/google/android/music/art/ArtLoader$Listener;

    move-result-object v2

    if-nez v2, :cond_1

    .line 82
    const/4 v2, -0x1

    goto :goto_0

    .line 86
    :cond_1
    # getter for: Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mRequestNumber:I
    invoke-static {v1}, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->access$100(Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;)I

    move-result v2

    # getter for: Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mRequestNumber:I
    invoke-static {v0}, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->access$100(Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;)I

    move-result v3

    sub-int/2addr v2, v3

    goto :goto_0
.end method
