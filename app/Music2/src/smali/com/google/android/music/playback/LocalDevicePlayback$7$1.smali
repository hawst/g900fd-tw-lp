.class Lcom/google/android/music/playback/LocalDevicePlayback$7$1;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback$7;->onDownloadProgress(Lcom/google/android/music/download/TrackDownloadProgress;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

.field final synthetic val$downloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback$7;Lcom/google/android/music/download/TrackDownloadProgress;)V
    .locals 0

    .prologue
    .line 686
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iput-object p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$1;->val$downloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 689
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v3, v3, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v3, v3, Lcom/google/android/music/playback/LocalDevicePlayback;->mProgressListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 691
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 693
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v3, v3, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v3, v3, Lcom/google/android/music/playback/LocalDevicePlayback;->mProgressListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/download/IDownloadProgressListener;

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$1;->val$downloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-interface {v3, v4}, Lcom/google/android/music/download/IDownloadProgressListener;->onDownloadProgress(Lcom/google/android/music/download/DownloadProgress;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 691
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 695
    :catch_0
    move-exception v0

    .line 696
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v3, "LocalDevicePlayback"

    const-string v4, "Failed to call the download progress"

    invoke-static {v3, v4, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 700
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v4, v4, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v4, v4, Lcom/google/android/music/playback/LocalDevicePlayback;->mProgressListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    throw v3

    :cond_0
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v3, v3, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v3, v3, Lcom/google/android/music/playback/LocalDevicePlayback;->mProgressListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 702
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v3, v3, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/stream/StreamingClient;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 703
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v3, v3, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/stream/StreamingClient;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$1;->val$downloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-virtual {v3, v4}, Lcom/google/android/music/download/stream/StreamingClient;->handleDownloadProgress(Lcom/google/android/music/download/TrackDownloadProgress;)V

    .line 705
    :cond_1
    return-void
.end method
