.class Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter$1;
.super Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;
.source "LeanbackLauncherActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/ObjectAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;)V
    .locals 0

    .prologue
    .line 883
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    .prologue
    .line 886
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->access$1800(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;)V

    .line 887
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->notifyChanged()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->access$1900(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;)V

    .line 888
    return-void
.end method

.method public onItemRangeChanged(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 892
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mOffset:I
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->access$2000(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;)I

    move-result v1

    add-int/2addr v1, p1

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->notifyItemRangeChanged(II)V
    invoke-static {v0, v1, p2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->access$2100(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;II)V

    .line 893
    return-void
.end method

.method public onItemRangeInserted(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 897
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->access$1800(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;)V

    .line 898
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mOffset:I
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->access$2000(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;)I

    move-result v1

    add-int/2addr v1, p1

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->notifyItemRangeInserted(II)V
    invoke-static {v0, v1, p2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->access$2200(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;II)V

    .line 899
    return-void
.end method

.method public onItemRangeRemoved(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 903
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->access$1800(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;)V

    .line 904
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter$1;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mOffset:I
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->access$2000(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;)I

    move-result v1

    add-int/2addr v1, p1

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->notifyItemRangeRemoved(II)V
    invoke-static {v0, v1, p2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->access$2300(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;II)V

    .line 905
    return-void
.end method
