.class public Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;
.super Lcom/google/android/music/leanback/LeanbackDetailsActivity;
.source "LeanbackPlaylistDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mSongList:Lcom/google/android/music/medialist/SongList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackDetailsActivity;-><init>()V

    .line 163
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->mSongList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/medialist/SongList;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;
    .param p1, "x1"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->mSongList:Lcom/google/android/music/medialist/SongList;

    return-object p1
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;
    .param p1, "x1"    # Lcom/google/android/music/leanback/Item;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->createSongList(Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;JILjava/lang/String;Lcom/google/android/music/medialist/SongList;Ljava/lang/String;Ljava/lang/String;II)Landroid/support/v17/leanback/widget/Action;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;
    .param p1, "x1"    # J
    .param p3, "x2"    # I
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Lcom/google/android/music/medialist/SongList;
    .param p6, "x5"    # Ljava/lang/String;
    .param p7, "x6"    # Ljava/lang/String;
    .param p8, "x7"    # I
    .param p9, "x8"    # I

    .prologue
    .line 34
    invoke-direct/range {p0 .. p9}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->createPlayAction(JILjava/lang/String;Lcom/google/android/music/medialist/SongList;Ljava/lang/String;Ljava/lang/String;II)Landroid/support/v17/leanback/widget/Action;

    move-result-object v0

    return-object v0
.end method

.method private createPlayAction(JILjava/lang/String;Lcom/google/android/music/medialist/SongList;Ljava/lang/String;Ljava/lang/String;II)Landroid/support/v17/leanback/widget/Action;
    .locals 15
    .param p1, "playlistId"    # J
    .param p3, "playlistType"    # I
    .param p4, "playlistName"    # Ljava/lang/String;
    .param p5, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p6, "actionName"    # Ljava/lang/String;
    .param p7, "actionSubName"    # Ljava/lang/String;
    .param p8, "playMode"    # I
    .param p9, "actionId"    # I

    .prologue
    .line 126
    new-instance v3, Lcom/google/android/music/medialist/PlaylistSongList;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-wide/from16 v4, p1

    move-object/from16 v6, p4

    invoke-direct/range {v3 .. v13}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move/from16 v0, p8

    invoke-static {p0, v3, v0}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;I)Landroid/content/Intent;

    move-result-object v8

    .line 134
    .local v8, "intent":Landroid/content/Intent;
    move-object/from16 v0, p5

    invoke-static {p0, v0, v8}, Lcom/google/android/music/xdi/XdiUtils;->setSonglistInfoInIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Landroid/content/Intent;)V

    .line 135
    new-instance v3, Lcom/google/android/music/leanback/IntentAction;

    move/from16 v0, p9

    int-to-long v4, v0

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v3 .. v8}, Lcom/google/android/music/leanback/IntentAction;-><init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    return-object v3
.end method

.method private createPlaylistObjectAdapter(J)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 5
    .param p1, "playlistId"    # J

    .prologue
    .line 54
    move-object v1, p0

    .line 55
    .local v1, "context":Landroid/content/Context;
    new-instance v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;JLandroid/content/Context;)V

    .line 105
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v2

    .line 106
    .local v2, "uri":Landroid/net/Uri;
    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/music/xdi/MusicProjections;->PLAYLIST_COLUMNS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v0, v2, v4}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V

    .line 107
    return-object v0
.end method

.method private createSongList(Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 4
    .param p1, "item"    # Lcom/google/android/music/leanback/Item;

    .prologue
    .line 139
    new-instance v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$2;-><init>(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;Lcom/google/android/music/leanback/Item;)V

    .line 159
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->mSongList:Lcom/google/android/music/medialist/SongList;

    sget-object v3, Lcom/google/android/music/xdi/TrackAdapter;->TRACK_COLUMNS:[Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 160
    return-object v0
.end method


# virtual methods
.method protected getAdapter(Landroid/support/v17/leanback/widget/PresenterSelector;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 10
    .param p1, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "playlist_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    .line 48
    .local v8, "playlistId":J
    invoke-direct {p0, v8, v9}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->createPlaylistObjectAdapter(J)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v4

    .line 49
    .local v4, "detailsObjectAdapter":Landroid/support/v17/leanback/widget/ObjectAdapter;
    new-instance v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    new-instance v5, Lcom/google/android/music/leanback/SongTitleRow;

    const v1, 0x7f0b00a1

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/google/android/music/leanback/SongTitleRow;-><init>(Ljava/lang/String;)V

    new-instance v6, Lcom/google/android/music/leanback/SongFooterRow;

    invoke-direct {v6}, Lcom/google/android/music/leanback/SongFooterRow;-><init>()V

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;-><init>(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;Landroid/content/Context;Landroid/support/v17/leanback/widget/PresenterSelector;Landroid/support/v17/leanback/widget/ObjectAdapter;Lcom/google/android/music/leanback/SongTitleRow;Lcom/google/android/music/leanback/SongFooterRow;)V

    return-object v0
.end method
