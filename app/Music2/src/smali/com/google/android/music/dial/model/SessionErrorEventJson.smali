.class public Lcom/google/android/music/dial/model/SessionErrorEventJson;
.super Lcom/google/api/client/json/GenericJson;
.source "SessionErrorEventJson.java"


# instance fields
.field public mErrorCode:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "errorCode"
    .end annotation
.end field

.field public mReason:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "reason"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
