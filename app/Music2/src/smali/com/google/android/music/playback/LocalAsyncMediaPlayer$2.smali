.class Lcom/google/android/music/playback/LocalAsyncMediaPlayer$2;
.super Ljava/lang/Object;
.source "LocalAsyncMediaPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V
    .locals 0

    .prologue
    .line 886
    iput-object p1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$2;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 888
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncMediaPlayer OnCompletion "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 889
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$2;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    const/4 v1, 0x4

    # invokes: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$200(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;I)V

    .line 890
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$2;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$300(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/playback/StopWatch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/playback/StopWatch;->pause()V

    .line 891
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$2;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$400(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 892
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$2;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$500(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->reportTrackEnded()V

    .line 894
    :cond_0
    return-void
.end method
