.class Lcom/google/android/music/xdi/DetailArtistInfoCursor;
.super Landroid/database/MatrixCursor;
.source "DetailArtistInfoCursor.java"


# static fields
.field private static final PROJECTION_ARTISTS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "artist"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->PROJECTION_ARTISTS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "artistId"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 38
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->mContext:Landroid/content/Context;

    .line 39
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 41
    invoke-direct {p0, p3}, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->addArtist(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method private addArtist(Ljava/lang/String;)V
    .locals 7
    .param p1, "artistId"    # Ljava/lang/String;

    .prologue
    .line 45
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->PROJECTION_ARTISTS:[Ljava/lang/String;

    invoke-static {v4, p1, v5}, Lcom/google/android/music/xdi/XdiUtils;->getArtistCursor(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 47
    .local v1, "cursor":Landroid/database/Cursor;
    if-nez v1, :cond_0

    .line 78
    :goto_0
    return-void

    .line 52
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 53
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v4}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v4

    new-array v3, v4, [Ljava/lang/Object;

    .line 54
    .local v3, "values":[Ljava/lang/Object;
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 56
    .local v2, "name":Ljava/lang/String;
    sget-object v4, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "details/artists"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "section"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "artist"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "actions"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 64
    .local v0, "contentUri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "_id"

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "display_name"

    invoke-virtual {v4, v3, v5, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 66
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "display_subname"

    const/4 v6, 0x0

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 67
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "display_description"

    const/4 v6, 0x0

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 68
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "display_category"

    const/4 v6, 0x0

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 69
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "display_date"

    const/4 v6, 0x0

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 70
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "action_uri"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 73
    invoke-virtual {p0, v3}, Lcom/google/android/music/xdi/DetailArtistInfoCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    .end local v0    # "contentUri":Landroid/net/Uri;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "values":[Ljava/lang/Object;
    :cond_1
    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v4
.end method
