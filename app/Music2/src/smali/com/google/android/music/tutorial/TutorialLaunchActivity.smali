.class public Lcom/google/android/music/tutorial/TutorialLaunchActivity;
.super Lcom/google/android/music/tutorial/TutorialActivity;
.source "TutorialLaunchActivity.java"

# interfaces
.implements Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;


# instance fields
.field private mDatabaseUpdating:Landroid/widget/TextView;

.field private mGetSelectedAccountOffers:Lcom/google/android/music/tutorial/GetAccountOffersTask;

.field private mIsFirstCheckAccount:Z

.field private mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialActivity;-><init>()V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mIsFirstCheckAccount:Z

    .line 112
    new-instance v0, Lcom/google/android/music/tutorial/TutorialLaunchActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity$3;-><init>(Lcom/google/android/music/tutorial/TutorialLaunchActivity;)V

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/tutorial/TutorialLaunchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialLaunchActivity;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->initTutorial()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/tutorial/TutorialLaunchActivity;)Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialLaunchActivity;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/tutorial/TutorialLaunchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialLaunchActivity;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->checkAccount()V

    return-void
.end method

.method private cancelGetOffers()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mGetSelectedAccountOffers:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mGetSelectedAccountOffers:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    invoke-virtual {v0}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->cancel()V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mGetSelectedAccountOffers:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .line 143
    :cond_0
    return-void
.end method

.method private checkAccount()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 175
    iget-boolean v0, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mIsFirstCheckAccount:Z

    if-nez v0, :cond_0

    .line 207
    :goto_0
    return-void

    .line 178
    :cond_0
    iput-boolean v3, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mIsFirstCheckAccount:Z

    .line 180
    const/4 v7, 0x1

    .line 181
    .local v7, "showTutorial":Z
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "forceTutorial"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 182
    .local v6, "forceTutorial":Z
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "coupon"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 183
    .local v4, "coupon":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "couponType"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 184
    .local v5, "couponType":Ljava/lang/String;
    if-nez v6, :cond_2

    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->wasTutorialViewed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 186
    const/4 v7, 0x0

    .line 192
    :cond_1
    :goto_1
    if-nez v7, :cond_3

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 193
    invoke-static {p0}, Lcom/google/android/music/tutorial/TutorialUtils;->finishTutorialTemporarily(Landroid/app/Activity;)V

    goto :goto_0

    .line 187
    :cond_2
    invoke-static {p0}, Lcom/google/android/music/utils/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 188
    const/4 v7, 0x0

    goto :goto_1

    .line 197
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v2

    .line 198
    .local v2, "currentAccount":Landroid/accounts/Account;
    if-eqz v2, :cond_4

    .line 201
    new-instance v0, Lcom/google/android/music/tutorial/GetAccountOffersTask;

    move-object v1, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;-><init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mGetSelectedAccountOffers:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .line 203
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mGetSelectedAccountOffers:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    invoke-virtual {v0}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->run()V

    goto :goto_0

    .line 205
    :cond_4
    const/4 v0, 0x1

    invoke-static {p0, v0, v4, v5}, Lcom/google/android/music/tutorial/TutorialUtils;->openSelectAccountActivity(Landroid/app/Activity;ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initTutorial()V
    .locals 3

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/tutorial/TutorialUtils;->isDatabaseUpdated(Lcom/google/android/music/preferences/MusicPreferences;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->checkAccount()V

    .line 110
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mDatabaseUpdating:Landroid/widget/TextView;

    const v2, 0x7f0b02cf

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v1, v2}, Lcom/google/android/music/preferences/MusicPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 90
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 91
    .local v0, "appContext":Landroid/content/Context;
    new-instance v1, Lcom/google/android/music/tutorial/TutorialLaunchActivity$2;

    invoke-direct {v1, p0, v0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity$2;-><init>(Lcom/google/android/music/tutorial/TutorialLaunchActivity;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Lcom/google/android/music/tutorial/TutorialActivity;->isActivityDestroyed()Z

    move-result v0

    return v0
.end method

.method public onAccountDisabled(Landroid/accounts/Account;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v1, 0x0

    .line 163
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->setInvalidStreamingAccount(Landroid/accounts/Account;)V

    .line 164
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-static {p0, v1, v1, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->finishTutorialPermanently(Landroid/app/Activity;ZZLcom/google/android/music/preferences/MusicPreferences;)V

    .line 165
    return-void
.end method

.method public onAccountInvalid(Landroid/accounts/Account;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v1, 0x0

    .line 157
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->setInvalidStreamingAccount(Landroid/accounts/Account;)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-static {p0, v1, v1, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->finishTutorialPermanently(Landroid/app/Activity;ZZLcom/google/android/music/preferences/MusicPreferences;)V

    .line 159
    return-void
.end method

.method public onAccountOffersError(Landroid/accounts/Account;)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 152
    invoke-static {p0}, Lcom/google/android/music/tutorial/TutorialUtils;->finishTutorialTemporarily(Landroid/app/Activity;)V

    .line 153
    return-void
.end method

.method public onAccountOffersSuccess(Landroid/accounts/Account;Lcom/google/android/music/cloudclient/OffersResponseJson;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "availableOffers"    # Lcom/google/android/music/cloudclient/OffersResponseJson;

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-static {p2, p0, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->openTryNautilusOrFinishTutorial(Lcom/google/android/music/cloudclient/OffersResponseJson;Landroid/app/Activity;Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 148
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 55
    invoke-super {p0, p1}, Lcom/google/android/music/tutorial/TutorialActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/tutorial/TutorialLaunchActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity$1;-><init>(Lcom/google/android/music/tutorial/TutorialLaunchActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->runWithPreferenceService(Ljava/lang/Runnable;)V

    .line 75
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->requestWindowFeature(I)Z

    .line 76
    const v0, 0x7f040100

    invoke-virtual {p0, v0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->setContentView(I)V

    .line 77
    const v0, 0x7f0e0282

    invoke-virtual {p0, v0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mDatabaseUpdating:Landroid/widget/TextView;

    .line 78
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->cancelGetOffers()V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 135
    invoke-super {p0}, Lcom/google/android/music/tutorial/TutorialActivity;->onDestroy()V

    .line 136
    return-void
.end method
