.class public Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;
.super Ljava/lang/Object;
.source "NowPlayingWidgetUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "State"
.end annotation


# instance fields
.field public final isPlaying:Z

.field public final launchIntent:Landroid/app/PendingIntent;


# direct methods
.method constructor <init>(Landroid/content/Intent;Landroid/content/Context;I)V
    .locals 2
    .param p1, "musicState"    # Landroid/content/Intent;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layoutId"    # I

    .prologue
    .line 449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450
    invoke-static {p2}, Lcom/google/android/music/ui/AppNavigation;->getIntentToOpenAppWithPlaybackScreen(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;->launchIntent:Landroid/app/PendingIntent;

    .line 451
    const-string v0, "playing"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;->isPlaying:Z

    .line 453
    return-void
.end method
