.class public Lcom/google/android/music/KeepOnView$SharedResources;
.super Ljava/lang/Object;
.source "KeepOnView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/KeepOnView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "SharedResources"
.end annotation


# instance fields
.field private final sGreyCircleBackground:Landroid/graphics/Bitmap;

.field private final sPinnedCompletedOverlay:Landroid/graphics/Bitmap;

.field private final sPinnedInProgressOverlay:Landroid/graphics/Bitmap;

.field private final sUnpinnedOverlay:Landroid/graphics/Bitmap;


# direct methods
.method protected constructor <init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "unpinnedOverlay"    # Landroid/graphics/Bitmap;
    .param p2, "pinnedOverlay"    # Landroid/graphics/Bitmap;
    .param p3, "pinnedCompletedOverlay"    # Landroid/graphics/Bitmap;
    .param p4, "greyCircleBackground"    # Landroid/graphics/Bitmap;

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-object p1, p0, Lcom/google/android/music/KeepOnView$SharedResources;->sUnpinnedOverlay:Landroid/graphics/Bitmap;

    .line 120
    iput-object p2, p0, Lcom/google/android/music/KeepOnView$SharedResources;->sPinnedInProgressOverlay:Landroid/graphics/Bitmap;

    .line 121
    iput-object p3, p0, Lcom/google/android/music/KeepOnView$SharedResources;->sPinnedCompletedOverlay:Landroid/graphics/Bitmap;

    .line 122
    iput-object p4, p0, Lcom/google/android/music/KeepOnView$SharedResources;->sGreyCircleBackground:Landroid/graphics/Bitmap;

    .line 123
    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/KeepOnView$SharedResources;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView$SharedResources;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$SharedResources;->sUnpinnedOverlay:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/KeepOnView$SharedResources;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView$SharedResources;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$SharedResources;->sPinnedCompletedOverlay:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/KeepOnView$SharedResources;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView$SharedResources;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$SharedResources;->sPinnedInProgressOverlay:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/KeepOnView$SharedResources;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/KeepOnView$SharedResources;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$SharedResources;->sGreyCircleBackground:Landroid/graphics/Bitmap;

    return-object v0
.end method
