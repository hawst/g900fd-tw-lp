.class public Lcom/google/android/music/download/cache/LastUsedDeleteStrategy;
.super Ljava/lang/Object;
.source "LastUsedDeleteStrategy.java"

# interfaces
.implements Lcom/google/android/music/download/cache/DeletionStrategy;


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mFileSys:Lcom/google/android/music/download/cache/FileSystem;

.field private final mStore:Lcom/google/android/music/store/Store;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/cache/LastUsedDeleteStrategy;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/download/cache/FileSystem;Lcom/google/android/music/store/Store;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileSystem"    # Lcom/google/android/music/download/cache/FileSystem;
    .param p3, "store"    # Lcom/google/android/music/store/Store;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p3, p0, Lcom/google/android/music/download/cache/LastUsedDeleteStrategy;->mStore:Lcom/google/android/music/store/Store;

    .line 31
    iput-object p2, p0, Lcom/google/android/music/download/cache/LastUsedDeleteStrategy;->mFileSys:Lcom/google/android/music/download/cache/FileSystem;

    .line 32
    iput-object p1, p0, Lcom/google/android/music/download/cache/LastUsedDeleteStrategy;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method


# virtual methods
.method public createSpace(JLcom/google/android/music/download/cache/CacheLocation;Lcom/google/android/music/download/cache/FilteredFileDeleter;)Z
    .locals 15
    .param p1, "requiredSpace"    # J
    .param p3, "cacheLocation"    # Lcom/google/android/music/download/cache/CacheLocation;
    .param p4, "fileDeleter"    # Lcom/google/android/music/download/cache/FilteredFileDeleter;

    .prologue
    .line 37
    iget-object v10, p0, Lcom/google/android/music/download/cache/LastUsedDeleteStrategy;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/music/download/cache/CacheLocation;->getSchemaValueForStorageType()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/google/android/music/store/Store;->getLeastRecentlyUsedCacheFiles(I)Landroid/database/Cursor;

    move-result-object v1

    .line 38
    .local v1, "possibleDeletes":Landroid/database/Cursor;
    if-nez v1, :cond_0

    .line 39
    const/4 v10, 0x0

    .line 80
    :goto_0
    return v10

    .line 41
    :cond_0
    const-wide/16 v6, 0x0

    .line 43
    .local v6, "recoveredSize":J
    :cond_1
    :goto_1
    cmp-long v10, v6, p1

    if-gez v10, :cond_4

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 44
    const/4 v10, 0x0

    invoke-interface {v1, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 47
    .local v2, "deleteId":J
    const/4 v9, 0x0

    .line 48
    .local v9, "volUuid":Ljava/util/UUID;
    const/4 v10, 0x3

    invoke-interface {v1, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v10

    if-nez v10, :cond_2

    .line 49
    const/4 v10, 0x3

    invoke-interface {v1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 50
    .local v8, "volId":Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/music/utils/MusicUtils;->getUUIDFromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v9

    .line 52
    .end local v8    # "volId":Ljava/lang/String;
    :cond_2
    iget-object v10, p0, Lcom/google/android/music/download/cache/LastUsedDeleteStrategy;->mContext:Landroid/content/Context;

    const/4 v11, 0x1

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x2

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    invoke-static {v10, v11, v12, v9}, Lcom/google/android/music/download/cache/CacheUtils;->resolveMusicPath(Landroid/content/Context;Ljava/lang/String;ILjava/util/UUID;)Ljava/io/File;

    move-result-object v0

    .line 55
    .local v0, "deleteFile":Ljava/io/File;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 56
    iget-object v10, p0, Lcom/google/android/music/download/cache/LastUsedDeleteStrategy;->mFileSys:Lcom/google/android/music/download/cache/FileSystem;

    invoke-interface {v10, v0}, Lcom/google/android/music/download/cache/FileSystem;->getLength(Ljava/io/File;)J

    move-result-wide v4

    .line 57
    .local v4, "possibleRecoverSize":J
    const-wide/16 v10, 0x0

    cmp-long v10, v4, v10

    if-lez v10, :cond_1

    .line 58
    sget-boolean v10, Lcom/google/android/music/download/cache/LastUsedDeleteStrategy;->LOGV:Z

    if-eqz v10, :cond_3

    .line 59
    const-string v10, "MusicCache"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "About to delete local file for: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_3
    iget-object v10, p0, Lcom/google/android/music/download/cache/LastUsedDeleteStrategy;->mFileSys:Lcom/google/android/music/download/cache/FileSystem;

    invoke-interface {v10, v0}, Lcom/google/android/music/download/cache/FileSystem;->delete(Ljava/io/File;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 64
    iget-object v10, p0, Lcom/google/android/music/download/cache/LastUsedDeleteStrategy;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v10, v2, v3}, Lcom/google/android/music/store/Store;->removeFileLocation(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    add-long/2addr v6, v4

    goto :goto_1

    .line 71
    .end local v0    # "deleteFile":Ljava/io/File;
    .end local v2    # "deleteId":J
    .end local v4    # "possibleRecoverSize":J
    .end local v9    # "volUuid":Ljava/util/UUID;
    :cond_4
    cmp-long v10, v6, p1

    if-ltz v10, :cond_5

    .line 72
    const/4 v10, 0x1

    .line 80
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 75
    :cond_5
    :try_start_1
    const-string v10, "MusicCache"

    const-string v11, "Could not create enough space in %s requiredSpace=%d recoveredSize=%d"

    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object p3, v12, v13

    const/4 v13, 0x1

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    const/4 v10, 0x0

    .line 80
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catchall_0
    move-exception v10

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v10
.end method
