.class Lcom/google/android/music/ui/NowPlayingArtPageFragment$2;
.super Ljava/lang/Object;
.source "NowPlayingArtPageFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingArtPageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$2;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 124
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$2;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$600(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 125
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$2;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 126
    .local v1, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 127
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_1

    .line 128
    const-string v2, "NowPlayingArtPageFragment"

    const-string v3, "Missing account"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_0
    :goto_0
    return-void

    .line 131
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$2;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$2;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoId:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$600(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/music/youtube/YouTubeUtils;->startVideo(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method
