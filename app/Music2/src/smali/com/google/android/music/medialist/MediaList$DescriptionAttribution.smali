.class public Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;
.super Ljava/lang/Object;
.source "MediaList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/medialist/MediaList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DescriptionAttribution"
.end annotation


# instance fields
.field public final licenseTitle:Ljava/lang/String;

.field public final licenseUrl:Ljava/lang/String;

.field public final sourceTitle:Ljava/lang/String;

.field public final sourceUrl:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/medialist/MediaList;


# direct methods
.method public constructor <init>(Lcom/google/android/music/medialist/MediaList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "sourceTitle"    # Ljava/lang/String;
    .param p3, "sourceUrl"    # Ljava/lang/String;
    .param p4, "licenseTitle"    # Ljava/lang/String;
    .param p5, "licenseUrl"    # Ljava/lang/String;

    .prologue
    .line 430
    iput-object p1, p0, Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;->this$0:Lcom/google/android/music/medialist/MediaList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 431
    iput-object p2, p0, Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;->sourceTitle:Ljava/lang/String;

    .line 432
    iput-object p3, p0, Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;->sourceUrl:Ljava/lang/String;

    .line 433
    iput-object p4, p0, Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;->licenseTitle:Ljava/lang/String;

    .line 434
    iput-object p5, p0, Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;->licenseUrl:Ljava/lang/String;

    .line 435
    return-void
.end method
