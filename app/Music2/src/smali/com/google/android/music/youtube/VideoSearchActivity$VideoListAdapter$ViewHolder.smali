.class Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "VideoSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewHolder"
.end annotation


# instance fields
.field public final mAuthor:Landroid/widget/TextView;

.field public final mDetails:Landroid/widget/TextView;

.field public final mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

.field public final mTitle:Landroid/widget/TextView;

.field final synthetic this$1:Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;Landroid/view/View;)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;->this$1:Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    const v0, 0x7f0e028b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 119
    const v0, 0x7f0e00b8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    .line 120
    const v0, 0x7f0e028c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;->mAuthor:Landroid/widget/TextView;

    .line 121
    const v0, 0x7f0e028d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;->mDetails:Landroid/widget/TextView;

    .line 122
    return-void
.end method


# virtual methods
.method public show(Lcom/google/android/music/youtube/YouTubeVideo;)V
    .locals 8
    .param p1, "video"    # Lcom/google/android/music/youtube/YouTubeVideo;

    .prologue
    .line 125
    invoke-virtual {p1}, Lcom/google/android/music/youtube/YouTubeVideo;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v1

    .line 126
    .local v1, "thumbnailUrl":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 127
    iget-object v2, p0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v2, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalAlbumArt(Ljava/lang/String;)V

    .line 129
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/music/youtube/YouTubeVideo;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v2, p0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;->mAuthor:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/music/youtube/YouTubeVideo;->getAuthor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    invoke-virtual {p1}, Lcom/google/android/music/youtube/YouTubeVideo;->isHd()Z

    move-result v2

    if-eqz v2, :cond_1

    const v0, 0x7f0b0049

    .line 133
    .local v0, "resId":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;->mDetails:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;->this$1:Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;

    iget-object v3, v3, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    invoke-virtual {v3, v0}, Lcom/google/android/music/youtube/VideoSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter$ViewHolder;->this$1:Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;

    iget-object v6, v6, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    # getter for: Lcom/google/android/music/youtube/VideoSearchActivity;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/google/android/music/youtube/VideoSearchActivity;->access$000(Lcom/google/android/music/youtube/VideoSearchActivity;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/music/youtube/YouTubeVideo;->getDurationSeconds()I

    move-result v7

    invoke-static {v6, v7}, Lcom/google/android/music/youtube/YouTubeUtils;->formatDurationSeconds(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p1}, Lcom/google/android/music/youtube/YouTubeVideo;->getViewCount()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    return-void

    .line 131
    .end local v0    # "resId":I
    :cond_1
    const v0, 0x7f0b004a

    goto :goto_0
.end method
