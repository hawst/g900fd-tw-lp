.class Lcom/google/android/music/playback/RemoteControlClientCompat$2;
.super Ljava/lang/Object;
.source "RemoteControlClientCompat.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/RemoteControlClientCompat;->setOnGetPlaybackPositionListener(Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/RemoteControlClientCompat;

.field final synthetic val$l:Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/RemoteControlClientCompat;Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;)V
    .locals 0

    .prologue
    .line 427
    iput-object p1, p0, Lcom/google/android/music/playback/RemoteControlClientCompat$2;->this$0:Lcom/google/android/music/playback/RemoteControlClientCompat;

    iput-object p2, p0, Lcom/google/android/music/playback/RemoteControlClientCompat$2;->val$l:Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "method"    # Ljava/lang/reflect/Method;
    .param p3, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 431
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onGetPlaybackPosition"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/google/android/music/playback/RemoteControlClientCompat$2;->val$l:Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/google/android/music/playback/RemoteControlClientCompat$2;->val$l:Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;

    invoke-interface {v0}, Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;->onGetPlaybackPosition()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 438
    :goto_0
    return-object v0

    .line 436
    :cond_0
    const-string v0, "RemoteControlCompat"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected interface method call: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method
