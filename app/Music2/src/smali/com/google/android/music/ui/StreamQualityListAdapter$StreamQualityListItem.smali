.class public Lcom/google/android/music/ui/StreamQualityListAdapter$StreamQualityListItem;
.super Ljava/lang/Object;
.source "StreamQualityListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/StreamQualityListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StreamQualityListItem"
.end annotation


# instance fields
.field public final mStreamQuality:Ljava/lang/CharSequence;

.field public final mStreamQualityDescription:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "quality"    # Ljava/lang/String;
    .param p2, "qualityDescription"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/music/ui/StreamQualityListAdapter$StreamQualityListItem;->mStreamQuality:Ljava/lang/CharSequence;

    .line 31
    iput-object p2, p0, Lcom/google/android/music/ui/StreamQualityListAdapter$StreamQualityListItem;->mStreamQualityDescription:Ljava/lang/CharSequence;

    .line 32
    return-void
.end method
