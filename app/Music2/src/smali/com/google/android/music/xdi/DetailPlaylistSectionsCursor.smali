.class Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;
.super Landroid/database/MatrixCursor;
.source "DetailPlaylistSectionsCursor.java"


# static fields
.field private static final PROJECTION_PLAYLISTS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "playlist_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "playlist_owner_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->PROJECTION_PLAYLISTS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "playlistId"    # J

    .prologue
    .line 44
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 45
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->mContext:Landroid/content/Context;

    .line 46
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 48
    invoke-direct {p0, p3, p4}, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->addPlaylistSections(J)V

    .line 49
    return-void
.end method

.method private addPlaylistSection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;)V
    .locals 4
    .param p1, "sectionName"    # Ljava/lang/String;
    .param p2, "sectionPrimaryText"    # Ljava/lang/String;
    .param p3, "sectionSecondaryText"    # Ljava/lang/String;
    .param p4, "sectionType"    # I
    .param p5, "sectionId"    # I
    .param p6, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 112
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v1}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/Object;

    .line 114
    .local v0, "values":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "_id"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 115
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "name"

    invoke-virtual {v1, v0, v2, p1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 116
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "display_header"

    invoke-virtual {v1, v0, v2, p1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 118
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "display_name"

    invoke-virtual {v1, v0, v2, p2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 120
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "display_subname"

    invoke-virtual {v1, v0, v2, p3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 122
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "section_type"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 124
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "content_uri"

    invoke-virtual {p6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 127
    invoke-virtual {p0, v0}, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->addRow([Ljava/lang/Object;)V

    .line 128
    return-void
.end method

.method private addPlaylistSections(J)V
    .locals 17
    .param p1, "playlistId"    # J

    .prologue
    .line 52
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->PROJECTION_PLAYLISTS:[Ljava/lang/String;

    move-wide/from16 v0, p1

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/music/xdi/XdiUtils;->getPlaylistCursor(Landroid/content/Context;J[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 54
    .local v15, "cursor":Landroid/database/Cursor;
    if-nez v15, :cond_0

    .line 80
    :goto_0
    return-void

    .line 59
    :cond_0
    :try_start_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 60
    const/4 v2, 0x0

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 61
    .local v4, "name":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 62
    .local v16, "ownerName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 63
    .local v5, "ownerLabel":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 64
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->mContext:Landroid/content/Context;

    const v3, 0x7f0b0102

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v16, v6, v8

    invoke-virtual {v2, v3, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 67
    :cond_1
    const/4 v7, 0x0

    .line 68
    .local v7, "sectionId":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00fd

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x2

    add-int/lit8 v13, v7, 0x1

    .end local v7    # "sectionId":I
    .local v13, "sectionId":I
    const-string v2, "playlist"

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v2}, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->getContentUriForPlaylistDetailsSection(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->addPlaylistSection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;)V

    .line 72
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->mContext:Landroid/content/Context;

    const v3, 0x7f0b02d0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v12, 0x1

    add-int/lit8 v7, v13, 0x1

    .end local v13    # "sectionId":I
    .restart local v7    # "sectionId":I
    const-string v2, "tracks"

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v2}, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->getContentUriForPlaylistDetailsSection(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    move-object/from16 v8, p0

    move-object v10, v4

    move-object v11, v5

    invoke-direct/range {v8 .. v14}, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;->addPlaylistSection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "ownerLabel":Ljava/lang/String;
    .end local v7    # "sectionId":I
    .end local v16    # "ownerName":Ljava/lang/String;
    :cond_2
    invoke-static {v15}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-static {v15}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2
.end method

.method private static getContentUriForPlaylistDetailsSection(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "playlistId"    # J
    .param p2, "sectionPath"    # Ljava/lang/String;

    .prologue
    .line 90
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "details/playlists"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "section"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
