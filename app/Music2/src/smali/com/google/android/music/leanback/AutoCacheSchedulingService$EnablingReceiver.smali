.class public Lcom/google/android/music/leanback/AutoCacheSchedulingService$EnablingReceiver;
.super Landroid/support/v4/content/WakefulBroadcastReceiver;
.source "AutoCacheSchedulingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/AutoCacheSchedulingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EnablingReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 835
    invoke-direct {p0}, Landroid/support/v4/content/WakefulBroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 839
    # getter for: Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 840
    const-string v0, "MusicLeanback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EnablingReceiver action:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    :cond_0
    # invokes: Lcom/google/android/music/leanback/AutoCacheSchedulingService;->startWakefulService(Landroid/content/Context;Landroid/content/Intent;)V
    invoke-static {p1, p2}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->access$700(Landroid/content/Context;Landroid/content/Intent;)V

    .line 843
    return-void
.end method
