.class public Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;
.super Lcom/google/android/music/tutorial/TutorialActivity;
.source "TutorialTryNautilusActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;
    }
.end annotation


# static fields
.field public static final ICON_RESOURCE_ID:[I

.field public static final SUMMARY_RESOURCE_ID:[I

.field public static final TITLE_RESOURCE_ID:[I


# instance fields
.field private final AUTO_SLIDE_DELAY:I

.field private mAlbumWall:Landroid/widget/ImageView;

.field private mCurrentAnimation:Landroid/animation/ObjectAnimator;

.field private mCurrentPosition:I

.field private mHandler:Landroid/os/Handler;

.field private mIsAutoScroll:Z

.field private mIsYTAvailable:Z

.field private mOffer:Lcom/google/android/music/cloudclient/OfferJson;

.field private mOfferDescription:Landroid/widget/TextView;

.field private mPageIndicator:Lcom/google/android/music/widgets/PageIndicator;

.field private mPagerAdapter:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;

.field private mPanLeftOrTopListener:Landroid/animation/Animator$AnimatorListener;

.field private mPanRightOrBottomListener:Landroid/animation/Animator$AnimatorListener;

.field private mPanningLeftOrTop:Z

.field private mRunnable:Ljava/lang/Runnable;

.field private mStartPanningRunnable:Ljava/lang/Runnable;

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 56
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->TITLE_RESOURCE_ID:[I

    .line 61
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->SUMMARY_RESOURCE_ID:[I

    .line 66
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->ICON_RESOURCE_ID:[I

    return-void

    .line 56
    nop

    :array_0
    .array-data 4
        0x7f0b0122
        0x7f0b0124
        0x7f0b0126
        0x7f0b0128
    .end array-data

    .line 61
    :array_1
    .array-data 4
        0x7f0b0123
        0x7f0b0125
        0x7f0b0127
        0x7f0b0129
    .end array-data

    .line 66
    :array_2
    .array-data 4
        0x7f0200d1
        0x7f0200d2
        0x7f0200d3
        0x7f020176
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialActivity;-><init>()V

    .line 94
    iput v1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentPosition:I

    .line 96
    const/16 v0, 0xfa0

    iput v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->AUTO_SLIDE_DELAY:I

    .line 97
    iput-boolean v1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mIsAutoScroll:Z

    .line 103
    iput-boolean v1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPanningLeftOrTop:Z

    .line 108
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mHandler:Landroid/os/Handler;

    .line 109
    new-instance v0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$1;-><init>(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)V

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mRunnable:Ljava/lang/Runnable;

    .line 399
    new-instance v0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$5;-><init>(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)V

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mStartPanningRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .prologue
    .line 54
    iget v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentPosition:I

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentPosition:I

    return p1
.end method

.method static synthetic access$008(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .prologue
    .line 54
    iget v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentPosition:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentPosition:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPagerAdapter:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->stopPanning()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPanningLeftOrTop:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPanningLeftOrTop:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->panLeftOrTop()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->panRightOrBottom()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;J)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;
    .param p1, "x1"    # J

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->startPanningIfNeeded(J)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 54
    invoke-static {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->launchMoreInfoLink(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mIsAutoScroll:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->stopAutoScroll()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mIsYTAvailable:Z

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Lcom/google/android/music/widgets/PageIndicator;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPageIndicator:Lcom/google/android/music/widgets/PageIndicator;

    return-object v0
.end method

.method private addMoreInfoIcon()V
    .locals 8

    .prologue
    const/16 v6, 0x12

    .line 168
    new-instance v3, Landroid/text/SpannableString;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mOffer:Lcom/google/android/music/cloudclient/OfferJson;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/OfferJson;->mDescription:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "__#icn1#__"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 171
    .local v3, "spannableString":Landroid/text/SpannableString;
    new-instance v0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$2;-><init>(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)V

    .line 178
    .local v0, "clickableSpan":Landroid/text/style/ClickableSpan;
    new-instance v1, Lcom/google/android/music/ui/TutorialCardsFactory$ImageSpanHack;

    const v4, 0x7f020104

    const/4 v5, 0x1

    invoke-direct {v1, p0, v4, v5}, Lcom/google/android/music/ui/TutorialCardsFactory$ImageSpanHack;-><init>(Landroid/content/Context;II)V

    .line 180
    .local v1, "imageSpan":Landroid/text/style/ImageSpan;
    invoke-virtual {v3}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "__#icn1#__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 182
    .local v2, "imageSpanIndex":I
    const-string v4, "__#icn1#__"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v3, v1, v2, v4, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 184
    const-string v4, "__#icn1#__"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v3, v0, v2, v4, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 186
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mOfferDescription:Landroid/widget/TextView;

    sget-object v5, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v4, v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 187
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mOfferDescription:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 188
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mOfferDescription:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mOffer:Lcom/google/android/music/cloudclient/OfferJson;

    iget-object v6, v6, Lcom/google/android/music/cloudclient/OfferJson;->mDescription:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0146

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 190
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mOfferDescription:Landroid/widget/TextView;

    new-instance v5, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$3;

    invoke-direct {v5, p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$3;-><init>(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    return-void
.end method

.method private doPan(FFFLjava/lang/String;Landroid/animation/Animator$AnimatorListener;)V
    .locals 5
    .param p1, "currentPos"    # F
    .param p2, "targetPos"    # F
    .param p3, "fullTravel"    # F
    .param p4, "propToAnimate"    # Ljava/lang/String;
    .param p5, "listener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 524
    const v2, 0x46ea6000    # 30000.0f

    sub-float v3, p2, p1

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    div-float/2addr v3, p3

    mul-float/2addr v2, v3

    float-to-long v0, v2

    .line 527
    .local v0, "duration":J
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput p1, v3, v4

    const/4 v4, 0x1

    aput p2, v3, v4

    invoke-static {v2, p4, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    .line 528
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 529
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 530
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2, p5}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 531
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 532
    return-void
.end method

.method private static launchMoreInfoLink(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 208
    const-string v3, "https://support.google.com/googleplay/?p=music_subscription_more_info"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 209
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 210
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v3, "hl"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 212
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 213
    .local v1, "i":Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 214
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 215
    return-void
.end method

.method private panLeftOrTop()V
    .locals 6

    .prologue
    .line 476
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->isActivityDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 496
    :cond_0
    :goto_0
    return-void

    .line 483
    :cond_1
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 484
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTop()I

    move-result v0

    neg-int v0, v0

    int-to-float v2, v0

    .line 485
    .local v2, "targetPos":F
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getY()F

    move-result v0

    add-float v1, v0, v2

    .line 486
    .local v1, "currentPos":F
    const-string v4, "translationY"

    .line 487
    .local v4, "propToAnimate":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTop()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    int-to-float v3, v0

    .line 495
    .local v3, "fullTravel":F
    :goto_1
    iget-object v5, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPanLeftOrTopListener:Landroid/animation/Animator$AnimatorListener;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->doPan(FFFLjava/lang/String;Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 489
    .end local v1    # "currentPos":F
    .end local v2    # "targetPos":F
    .end local v3    # "fullTravel":F
    .end local v4    # "propToAnimate":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLeft()I

    move-result v0

    neg-int v0, v0

    int-to-float v2, v0

    .line 490
    .restart local v2    # "targetPos":F
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getX()F

    move-result v0

    add-float v1, v0, v2

    .line 491
    .restart local v1    # "currentPos":F
    const-string v4, "translationX"

    .line 492
    .restart local v4    # "propToAnimate":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLeft()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    int-to-float v3, v0

    .restart local v3    # "fullTravel":F
    goto :goto_1
.end method

.method private panRightOrBottom()V
    .locals 6

    .prologue
    .line 499
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->isActivityDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 519
    :cond_0
    :goto_0
    return-void

    .line 506
    :cond_1
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 507
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTop()I

    move-result v0

    int-to-float v2, v0

    .line 508
    .local v2, "targetPos":F
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getY()F

    move-result v0

    sub-float v1, v0, v2

    .line 509
    .local v1, "currentPos":F
    const-string v4, "translationY"

    .line 510
    .local v4, "propToAnimate":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTop()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    int-to-float v3, v0

    .line 518
    .local v3, "fullTravel":F
    :goto_1
    iget-object v5, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPanRightOrBottomListener:Landroid/animation/Animator$AnimatorListener;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->doPan(FFFLjava/lang/String;Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 512
    .end local v1    # "currentPos":F
    .end local v2    # "targetPos":F
    .end local v3    # "fullTravel":F
    .end local v4    # "propToAnimate":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLeft()I

    move-result v0

    int-to-float v2, v0

    .line 513
    .restart local v2    # "targetPos":F
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getX()F

    move-result v0

    sub-float v1, v0, v2

    .line 514
    .restart local v1    # "currentPos":F
    const-string v4, "translationX"

    .line 515
    .restart local v4    # "propToAnimate":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLeft()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    int-to-float v3, v0

    .restart local v3    # "fullTravel":F
    goto :goto_1
.end method

.method static putOfferIntoIntent(Lcom/google/android/music/cloudclient/OfferJson;ZLandroid/content/Intent;)V
    .locals 2
    .param p0, "offer"    # Lcom/google/android/music/cloudclient/OfferJson;
    .param p1, "isYTAvaiable"    # Z
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 125
    const-string v0, "offer"

    invoke-static {p0}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonString(Lcom/google/api/client/json/GenericJson;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    const-string v0, "YTAvailable"

    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 127
    return-void
.end method

.method private setupViewPager()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 235
    const v2, 0x7f0e0287

    invoke-virtual {p0, v2}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ViewPager;

    iput-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 238
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 239
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f01be

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 243
    .local v1, "margin":I
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    mul-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 245
    .end local v1    # "margin":I
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 247
    const v2, 0x7f0e0286

    invoke-virtual {p0, v2}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/widgets/PageIndicator;

    iput-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPageIndicator:Lcom/google/android/music/widgets/PageIndicator;

    .line 248
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPageIndicator:Lcom/google/android/music/widgets/PageIndicator;

    iget-boolean v3, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mIsYTAvailable:Z

    invoke-virtual {v2, v3}, Lcom/google/android/music/widgets/PageIndicator;->setVideoServiceAvailability(Z)V

    .line 250
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 251
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    new-instance v2, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;-><init>(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;Landroid/support/v4/app/FragmentManager;)V

    iput-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPagerAdapter:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;

    .line 252
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPagerAdapter:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 255
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v4}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 256
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPageIndicator:Lcom/google/android/music/widgets/PageIndicator;

    invoke-virtual {v2, v4}, Lcom/google/android/music/widgets/PageIndicator;->updateSelectedDrawable(I)V

    .line 258
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v3, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$4;

    invoke-direct {v3, p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$4;-><init>(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 267
    return-void
.end method

.method private startAutoScroll()V
    .locals 4

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 271
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mIsAutoScroll:Z

    .line 272
    return-void
.end method

.method private startPanningIfNeeded()V
    .locals 3

    .prologue
    .line 415
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, " music_enable_album_wall_panning"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->startPanningIfNeeded(J)V

    .line 420
    :cond_0
    return-void
.end method

.method private startPanningIfNeeded(J)V
    .locals 3
    .param p1, "delay"    # J

    .prologue
    .line 429
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isHoneycombOrGreater()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 431
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPanLeftOrTopListener:Landroid/animation/Animator$AnimatorListener;

    if-nez v0, :cond_0

    .line 432
    new-instance v0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$6;

    invoke-direct {v0, p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$6;-><init>(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)V

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPanLeftOrTopListener:Landroid/animation/Animator$AnimatorListener;

    .line 446
    new-instance v0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$7;

    invoke-direct {v0, p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$7;-><init>(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)V

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPanRightOrBottomListener:Landroid/animation/Animator$AnimatorListener;

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mStartPanningRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 463
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mStartPanningRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Landroid/widget/ImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 465
    :cond_1
    return-void
.end method

.method private stopAutoScroll()V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 277
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mIsAutoScroll:Z

    .line 279
    :cond_0
    return-void
.end method

.method private stopPanning()V
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    .line 470
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 471
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    .line 473
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Lcom/google/android/music/tutorial/TutorialActivity;->isActivityDestroyed()Z

    move-result v0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, -0x1

    .line 321
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/tutorial/TutorialActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 322
    const/4 v1, 0x1

    if-ne v1, p1, :cond_2

    .line 323
    if-ne p2, v2, :cond_1

    .line 324
    sget-object v0, Lcom/google/android/music/NautilusStatus;->GOT_NAUTILUS:Lcom/google/android/music/NautilusStatus;

    .line 325
    .local v0, "tempStatus":Lcom/google/android/music/NautilusStatus;
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/music/preferences/MusicPreferences;->setTempNautilusStatus(Lcom/google/android/music/NautilusStatus;)V

    .line 326
    invoke-static {p0}, Lcom/google/android/music/tutorial/TutorialUtils;->openConfirmNautilusActivity(Landroid/app/Activity;)V

    .line 339
    .end local v0    # "tempStatus":Lcom/google/android/music/NautilusStatus;
    :cond_0
    :goto_0
    return-void

    .line 328
    :cond_1
    const-string v1, "MusicTutorial"

    const-string v2, "purchase error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 330
    :cond_2
    const/4 v1, 0x2

    if-ne v1, p1, :cond_0

    .line 331
    if-ne p2, v2, :cond_3

    .line 332
    sget-object v0, Lcom/google/android/music/NautilusStatus;->GOT_NAUTILUS:Lcom/google/android/music/NautilusStatus;

    .line 333
    .restart local v0    # "tempStatus":Lcom/google/android/music/NautilusStatus;
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/music/preferences/MusicPreferences;->setTempNautilusStatus(Lcom/google/android/music/NautilusStatus;)V

    .line 334
    invoke-static {p0}, Lcom/google/android/music/tutorial/TutorialUtils;->openConfirmNautilusActivity(Landroid/app/Activity;)V

    goto :goto_0

    .line 336
    .end local v0    # "tempStatus":Lcom/google/android/music/NautilusStatus;
    :cond_3
    const-string v1, "MusicTutorial"

    const-string v2, "enable nautilus error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 283
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 284
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    if-nez v0, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 313
    const-string v2, "MusicTutorial"

    const-string v3, "Unexpected onClick()"

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 289
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mOffer:Lcom/google/android/music/cloudclient/OfferJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/OfferJson;->mStoreDocId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 290
    invoke-static {p0, v5, v5, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->finishTutorialPermanently(Landroid/app/Activity;ZZLcom/google/android/music/preferences/MusicPreferences;)V

    goto :goto_0

    .line 294
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mTracker:Lcom/google/android/music/eventlog/MusicEventLogger;

    const-string v3, "signUpInAppBilling"

    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/tutorial/TutorialUtils;->getEntryPointFromIntent(Landroid/content/Intent;)Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/music/eventlog/MusicEventLogger;->logSignupFlowPage(Ljava/lang/String;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)V

    .line 297
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mOffer:Lcom/google/android/music/cloudclient/OfferJson;

    iget-boolean v2, v2, Lcom/google/android/music/cloudclient/OfferJson;->mFopLess:Z

    if-eqz v2, :cond_3

    .line 299
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mOffer:Lcom/google/android/music/cloudclient/OfferJson;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/OfferJson;->mStoreDocId:Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/google/android/music/tutorial/TutorialUtils;->openEnableNautilusActivity(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_0

    .line 302
    :cond_3
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mOffer:Lcom/google/android/music/cloudclient/OfferJson;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/OfferJson;->mStoreDocId:Ljava/lang/String;

    invoke-static {p0, v0, v2, v3}, Lcom/google/android/music/purchase/Finsky;->startNautilusPurchaseActivityForResult(Landroid/app/Activity;Lcom/google/android/music/preferences/MusicPreferences;ILjava/lang/String;)Z

    move-result v1

    .line 304
    .local v1, "success":Z
    if-nez v1, :cond_0

    .line 305
    invoke-static {p0, v5, v5, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->finishTutorialPermanently(Landroid/app/Activity;ZZLcom/google/android/music/preferences/MusicPreferences;)V

    goto :goto_0

    .line 310
    .end local v1    # "success":Z
    :pswitch_1
    invoke-static {p0, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->openOtherWaysToPlayActivity(Landroid/app/Activity;Lcom/google/android/music/preferences/MusicPreferences;)V

    goto :goto_0

    .line 286
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e027b
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    .line 131
    invoke-super {p0, p1}, Lcom/google/android/music/tutorial/TutorialActivity;->onCreate(Landroid/os/Bundle;)V

    .line 132
    const v5, 0x7f040103

    invoke-virtual {p0, v5}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->setContentView(I)V

    .line 134
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 135
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "offer"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 136
    .local v2, "offerString":Ljava/lang/String;
    const-string v5, "YTAvailable"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mIsYTAvailable:Z

    .line 138
    :try_start_0
    const-class v5, Lcom/google/android/music/cloudclient/OfferJson;

    invoke-static {v5, v2}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonString(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/cloudclient/OfferJson;

    iput-object v5, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mOffer:Lcom/google/android/music/cloudclient/OfferJson;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    const v5, 0x7f0e027c

    invoke-virtual {p0, v5}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 146
    .local v4, "tryNautilusBtn":Landroid/widget/Button;
    iget-object v5, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mOffer:Lcom/google/android/music/cloudclient/OfferJson;

    iget-boolean v5, v5, Lcom/google/android/music/cloudclient/OfferJson;->mHasFreeTrialPeriod:Z

    if-eqz v5, :cond_0

    const v5, 0x7f0b0140

    :goto_0
    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    .line 148
    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 151
    const v5, 0x7f0e027b

    invoke-virtual {p0, v5}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 152
    .local v3, "skipBtn":Landroid/widget/Button;
    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    invoke-static {v3, v7}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 155
    const v5, 0x7f0e0285

    invoke-virtual {p0, v5}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mAlbumWall:Landroid/widget/ImageView;

    .line 157
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->setupViewPager()V

    .line 159
    const v5, 0x7f0e0251

    invoke-virtual {p0, v5}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mOfferDescription:Landroid/widget/TextView;

    .line 160
    iget-object v5, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mOfferDescription:Landroid/widget/TextView;

    invoke-static {v5, v7}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 161
    .end local v3    # "skipBtn":Landroid/widget/Button;
    .end local v4    # "tryNautilusBtn":Landroid/widget/Button;
    :goto_1
    return-void

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/io/IOException;
    invoke-static {p0}, Lcom/google/android/music/tutorial/TutorialUtils;->finishTutorialTemporarily(Landroid/app/Activity;)V

    .line 141
    const-string v5, "MusicTutorial"

    const-string v6, "Failure to parse offers json"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 146
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v4    # "tryNautilusBtn":Landroid/widget/Button;
    :cond_0
    const v5, 0x7f0b0142

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 229
    invoke-super {p0}, Lcom/google/android/music/tutorial/TutorialActivity;->onPause()V

    .line 230
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->stopAutoScroll()V

    .line 231
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->stopPanning()V

    .line 232
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 220
    invoke-super {p0}, Lcom/google/android/music/tutorial/TutorialActivity;->onResume()V

    .line 222
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->addMoreInfoIcon()V

    .line 223
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->startAutoScroll()V

    .line 224
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->startPanningIfNeeded()V

    .line 225
    return-void
.end method
