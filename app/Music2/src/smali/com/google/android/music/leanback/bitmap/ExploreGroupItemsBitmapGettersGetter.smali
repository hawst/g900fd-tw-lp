.class public Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;
.super Lcom/google/android/music/leanback/bitmap/UriBitmapGettersGetter;
.source "ExploreGroupItemsBitmapGettersGetter.java"


# instance fields
.field private final mGroupType:I

.field private final mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLandroid/net/Uri;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loaderManager"    # Landroid/support/v4/app/LoaderManager;
    .param p3, "loaderId"    # I
    .param p4, "seed"    # J
    .param p6, "uri"    # Landroid/net/Uri;
    .param p7, "groupType"    # I

    .prologue
    .line 23
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/leanback/bitmap/UriBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    .line 24
    iput-object p6, p0, Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;->mUri:Landroid/net/Uri;

    .line 25
    iput p7, p0, Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;->mGroupType:I

    .line 26
    return-void
.end method


# virtual methods
.method protected addBitmapGetters(Landroid/database/Cursor;Ljava/util/ArrayList;)V
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/leanback/bitmap/BitmapGetter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p2, "bitmapGettersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/leanback/bitmap/BitmapGetter;>;"
    iget-object v3, p0, Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;->mContext:Landroid/content/Context;

    iget v4, p0, Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;->mGroupType:I

    invoke-static {v3, p1, v4}, Lcom/google/android/music/leanback/LeanbackUtils;->extractItemForGroupType(Landroid/content/Context;Landroid/database/Cursor;I)Lcom/google/android/music/leanback/Item;

    move-result-object v2

    .line 41
    .local v2, "item":Lcom/google/android/music/leanback/Item;
    invoke-virtual {v2}, Lcom/google/android/music/leanback/Item;->getBitmapGettersGetter()Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 42
    invoke-virtual {v2}, Lcom/google/android/music/leanback/Item;->getBitmapGettersGetter()Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;->getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    move-result-object v0

    .line 43
    .local v0, "bitmapGetters":[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    .line 44
    aget-object v3, v0, v1

    if-eqz v3, :cond_0

    .line 45
    aget-object v3, v0, v1

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 49
    .end local v0    # "bitmapGetters":[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method public bridge synthetic getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .locals 1

    .prologue
    .line 16
    invoke-super {p0}, Lcom/google/android/music/leanback/bitmap/UriBitmapGettersGetter;->getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    move-result-object v0

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;->mGroupType:I

    invoke-static {v0}, Lcom/google/android/music/xdi/XdiUtils;->getClusterProjection(I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 16
    invoke-super {p0, p1, p2}, Lcom/google/android/music/leanback/bitmap/UriBitmapGettersGetter;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;

    .prologue
    .line 16
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/bitmap/UriBitmapGettersGetter;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    return-void
.end method

.method public bridge synthetic startLoading()V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0}, Lcom/google/android/music/leanback/bitmap/UriBitmapGettersGetter;->startLoading()V

    return-void
.end method
