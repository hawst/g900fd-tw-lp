.class Lcom/google/android/music/ui/SDCardSelectorFragment$1;
.super Ljava/lang/Object;
.source "SDCardSelectorFragment.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/SDCardSelectorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/music/download/cache/CacheLocation;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/SDCardSelectorFragment;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$1;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/music/download/cache/CacheLocation;Lcom/google/android/music/download/cache/CacheLocation;)I
    .locals 10
    .param p1, "a"    # Lcom/google/android/music/download/cache/CacheLocation;
    .param p2, "b"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 55
    iget-object v7, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$1;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;
    invoke-static {v7}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$000(Lcom/google/android/music/ui/SDCardSelectorFragment;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/google/android/music/download/cache/CacheLocationManager;->getVolumeInformation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;

    move-result-object v1

    .line 57
    .local v1, "volA":Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    iget-object v7, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$1;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;
    invoke-static {v7}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$000(Lcom/google/android/music/ui/SDCardSelectorFragment;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v7

    invoke-virtual {v7, p2}, Lcom/google/android/music/download/cache/CacheLocationManager;->getVolumeInformation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;

    move-result-object v6

    .line 59
    .local v6, "volB":Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    invoke-virtual {v1}, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->getFreeSpaceInMegaBytes()J

    move-result-wide v2

    .line 60
    .local v2, "mbA":J
    invoke-virtual {v6}, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->getFreeSpaceInMegaBytes()J

    move-result-wide v4

    .line 62
    .local v4, "mbB":J
    sub-long v8, v4, v2

    long-to-int v0, v8

    .line 63
    .local v0, "diff":I
    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 50
    check-cast p1, Lcom/google/android/music/download/cache/CacheLocation;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/music/download/cache/CacheLocation;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/SDCardSelectorFragment$1;->compare(Lcom/google/android/music/download/cache/CacheLocation;Lcom/google/android/music/download/cache/CacheLocation;)I

    move-result v0

    return v0
.end method
