.class Lcom/google/android/music/art/ArtResolver$11;
.super Ljava/lang/Object;
.source "ArtResolver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/art/ArtResolver;->getAlbumArtForUrl(Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/art/ArtResolver;

.field final synthetic val$edge:I

.field final synthetic val$request:Lcom/google/android/music/art/ArtRequest;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/art/ArtResolver;Ljava/lang/String;ILcom/google/android/music/art/ArtRequest;)V
    .locals 0

    .prologue
    .line 1036
    iput-object p1, p0, Lcom/google/android/music/art/ArtResolver$11;->this$0:Lcom/google/android/music/art/ArtResolver;

    iput-object p2, p0, Lcom/google/android/music/art/ArtResolver$11;->val$url:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/music/art/ArtResolver$11;->val$edge:I

    iput-object p4, p0, Lcom/google/android/music/art/ArtResolver$11;->val$request:Lcom/google/android/music/art/ArtRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1039
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver$11;->this$0:Lcom/google/android/music/art/ArtResolver;

    # getter for: Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/art/ArtResolver;->access$200(Lcom/google/android/music/art/ArtResolver;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver$11;->val$url:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/music/art/ArtResolver$11;->val$edge:I

    iget v4, p0, Lcom/google/android/music/art/ArtResolver$11;->val$edge:I

    const/4 v5, 0x1

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/music/utils/AlbumArtUtils;->getRealNonAlbumArt(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1041
    .local v0, "result":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver$11;->val$request:Lcom/google/android/music/art/ArtRequest;

    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver$11;->this$0:Lcom/google/android/music/art/ArtResolver;

    # getter for: Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/art/ArtResolver;->access$200(Lcom/google/android/music/art/ArtResolver;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver$11;->val$url:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/music/art/ArtRequest;->notifyImageLoaded(Landroid/content/Context;Ljava/lang/Object;Landroid/graphics/Bitmap;)Z

    .line 1042
    return-void
.end method
