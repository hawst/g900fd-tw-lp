.class public Lcom/google/android/music/dial/model/GroupCoordinatorChangedEventJson;
.super Lcom/google/api/client/json/GenericJson;
.source "GroupCoordinatorChangedEventJson.java"


# instance fields
.field public mGroupId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "groupId"
    .end annotation
.end field

.field public mGroupName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "groupName"
    .end annotation
.end field

.field public mGroupStatus:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "groupStatus"
    .end annotation
.end field

.field public mWebsocketUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "websocketUrl"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
