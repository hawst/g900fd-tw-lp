.class Lcom/google/android/music/medialist/NautilusAlbumSongList$1;
.super Ljava/lang/Object;
.source "NautilusAlbumSongList.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/medialist/NautilusAlbumSongList;->toggleOfflineCaching(Landroid/content/Context;Lcom/google/android/music/activitymanagement/KeepOnManager;Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field albumId:J

.field final synthetic this$0:Lcom/google/android/music/medialist/NautilusAlbumSongList;

.field final synthetic val$animListener:Lcom/google/android/music/animator/AnimatorUpdateListener;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$keepOnManager:Lcom/google/android/music/activitymanagement/KeepOnManager;

.field final synthetic val$keepOnState:Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;


# direct methods
.method constructor <init>(Lcom/google/android/music/medialist/NautilusAlbumSongList;Landroid/content/Context;Lcom/google/android/music/activitymanagement/KeepOnManager;Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;)V
    .locals 2

    .prologue
    .line 298
    iput-object p1, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;->this$0:Lcom/google/android/music/medialist/NautilusAlbumSongList;

    iput-object p2, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;->val$keepOnManager:Lcom/google/android/music/activitymanagement/KeepOnManager;

    iput-object p4, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;->val$keepOnState:Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;

    iput-object p5, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;->val$animListener:Lcom/google/android/music/animator/AnimatorUpdateListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;->albumId:J

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;->this$0:Lcom/google/android/music/medialist/NautilusAlbumSongList;

    iget-object v1, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;->val$context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getAlbumId(Landroid/content/Context;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;->albumId:J

    .line 303
    return-void
.end method

.method public taskCompleted()V
    .locals 7

    .prologue
    .line 307
    iget-object v1, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;->val$keepOnManager:Lcom/google/android/music/activitymanagement/KeepOnManager;

    iget-wide v2, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;->albumId:J

    iget-object v4, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;->val$keepOnState:Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;

    iget-object v5, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;->val$animListener:Lcom/google/android/music/animator/AnimatorUpdateListener;

    iget-object v6, p0, Lcom/google/android/music/medialist/NautilusAlbumSongList$1;->val$context:Landroid/content/Context;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/activitymanagement/KeepOnManager;->toggleAlbumKeepOn(JLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Landroid/content/Context;)V

    .line 309
    return-void
.end method
