.class Lcom/google/android/music/xdi/InstantMixesHeadersCursor;
.super Landroid/database/MatrixCursor;
.source "InstantMixesHeadersCursor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mItemHeight:I

.field private final mItemWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 27
    iput-object p1, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mContext:Landroid/content/Context;

    .line 28
    new-instance v1, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v1, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 29
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthDp(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mItemWidth:I

    .line 30
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightDp(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mItemHeight:I

    .line 32
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_INSTANT_MIXES_HEADER_NAMES:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 33
    invoke-direct {p0, v0}, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->addBrowseHeader(I)V

    .line 32
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_0
    return-void
.end method

.method private addBrowseHeader(I)V
    .locals 8
    .param p1, "row"    # I

    .prologue
    .line 38
    iget-object v4, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v4}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v4

    new-array v3, v4, [Ljava/lang/Object;

    .line 39
    .local v3, "values":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_INSTANT_MIXES_HEADER_NAMES:[I

    aget v5, v5, p1

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 40
    .local v2, "name":Ljava/lang/String;
    sget-object v4, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_INSTANT_MIXES_HEADER_IDS:[I

    aget v4, v4, p1

    int-to-long v0, v4

    .line 42
    .local v0, "id":J
    iget-object v4, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 43
    iget-object v4, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "name"

    invoke-virtual {v4, v3, v5, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 44
    iget-object v4, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "display_name"

    invoke-virtual {v4, v3, v5, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 45
    iget-object v4, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "default_item_width"

    iget v6, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mItemWidth:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 46
    iget-object v4, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "default_item_height"

    iget v6, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mItemHeight:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 47
    iget-object v4, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "color_hint"

    iget-object v6, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c00a6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 49
    iget-object v4, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "badge_uri"

    iget-object v6, p0, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultBadgeUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 52
    invoke-virtual {p0, v3}, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;->addRow([Ljava/lang/Object;)V

    .line 53
    return-void
.end method
