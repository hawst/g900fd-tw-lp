.class public Lcom/google/android/music/activities/MusicSettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "MusicSettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/activities/MusicSettingsActivity$6;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private mAccountSettingsScreen:Landroid/preference/PreferenceScreen;

.field private mAccountTypeScreen:Landroid/preference/PreferenceScreen;

.field private mAutoCache:Landroid/preference/CheckBoxPreference;

.field private mCachedStreamed:Landroid/preference/CheckBoxPreference;

.field private mClearCache:Landroid/preference/PreferenceScreen;

.field private mContentFilter:Landroid/preference/CheckBoxPreference;

.field private mDebugLogs:Landroid/preference/CheckBoxPreference;

.field private mDeveloperCategory:Landroid/preference/PreferenceCategory;

.field private mDownloadOnlyOnWifi:Landroid/preference/CheckBoxPreference;

.field private mDownloadQueueScreen:Landroid/preference/PreferenceScreen;

.field private mDownloadingCategory:Landroid/preference/PreferenceCategory;

.field private mEqualizerScreen:Landroid/preference/PreferenceScreen;

.field private mGeneralCategory:Landroid/preference/PreferenceCategory;

.field private mIsExternalEmulated:Z

.field private mIsWifiOnly:Z

.field private mManageDeviceScreen:Landroid/preference/PreferenceScreen;

.field private mManageNautilusScreen:Landroid/preference/PreferenceScreen;

.field private mMaximumDeviceNumber:I

.field private mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private mNautilusStatus:Lcom/google/android/music/NautilusStatus;

.field private mRefreshScreen:Landroid/preference/PreferenceScreen;

.field private mSecondaryExternalEnabled:Z

.field private mSelectedAccount:Landroid/accounts/Account;

.field private mStorageBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mStorageLocationScreen:Landroid/preference/PreferenceScreen;

.field private mStreamOnlyOnWifi:Landroid/preference/CheckBoxPreference;

.field private mStreamQuality:Landroid/preference/PreferenceScreen;

.field private mStreamQualityList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/StreamQualityListAdapter$StreamQualityListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mStreamingCategory:Landroid/preference/PreferenceCategory;

.field private mVersion:Landroid/preference/PreferenceScreen;

.field private mWearSyncEnabled:Landroid/preference/CheckBoxPreference;

.field private final sStorageBroadcastFilter:Landroid/content/IntentFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/activities/MusicSettingsActivity;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 97
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMaximumDeviceNumber:I

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mIsWifiOnly:Z

    .line 121
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->sStorageBroadcastFilter:Landroid/content/IntentFilter;

    .line 123
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->sStorageBroadcastFilter:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.music.download.cache.CacheLocationManager.LocationsChanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 125
    new-instance v0, Lcom/google/android/music/activities/MusicSettingsActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/activities/MusicSettingsActivity$1;-><init>(Lcom/google/android/music/activities/MusicSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStorageBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/activities/MusicSettingsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/activities/MusicSettingsActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->refreshUI()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/activities/MusicSettingsActivity;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/MusicSettingsActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamQualityList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/activities/MusicSettingsActivity;)Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/MusicSettingsActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/activities/MusicSettingsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/activities/MusicSettingsActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->updateStreamQualitySummary()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/activities/MusicSettingsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/activities/MusicSettingsActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->killOurProcesses()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/activities/MusicSettingsActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/activities/MusicSettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/music/activities/MusicSettingsActivity;->handleEnableDebugLogs(Z)V

    return-void
.end method

.method private getIsWifiOnly()Z
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 401
    iget-object v5, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->isTvMusic()Z

    move-result v5

    if-nez v5, :cond_0

    .line 404
    :try_start_0
    const-string v4, "android.os.SystemProperties"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 405
    .local v2, "systemProperties":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v4, "get"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 406
    .local v1, "getMethod":Ljava/lang/reflect/Method;
    const-string v4, "wifi-only"

    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "ro.carrier"

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 413
    .end local v1    # "getMethod":Ljava/lang/reflect/Method;
    .end local v2    # "systemProperties":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    return v3

    .line 407
    :catch_0
    move-exception v0

    .line 408
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "MusicSettings"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error trying to access SystemProperties to check if wifi-only: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    move v3, v4

    .line 413
    goto :goto_0
.end method

.method private handleEnableDebugLogs(Z)V
    .locals 6
    .param p1, "actionConfirmed"    # Z

    .prologue
    .line 732
    if-nez p1, :cond_1

    .line 734
    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDebugLogs:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    .line 735
    .local v0, "value":Z
    iget-object v2, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDebugLogs:Landroid/preference/CheckBoxPreference;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 751
    .end local v0    # "value":Z
    :goto_1
    return-void

    .line 735
    .restart local v0    # "value":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 739
    .end local v0    # "value":Z
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    iget-object v2, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDebugLogs:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/preferences/MusicPreferences;->setLogFilesEnable(Z)V

    .line 746
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/activities/MusicSettingsActivity$3;

    invoke-direct {v2, p0}, Lcom/google/android/music/activities/MusicSettingsActivity$3;-><init>(Lcom/google/android/music/activities/MusicSettingsActivity;)V

    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/music/utils/LoggableHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method private hideStreamingPreferences()V
    .locals 3

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 371
    .local v0, "root":Landroid/preference/PreferenceScreen;
    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadingCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 372
    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamingCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 373
    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mRefreshScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 374
    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mContentFilter:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 376
    return-void
.end method

.method private initStreamQualityList()V
    .locals 6

    .prologue
    .line 632
    new-instance v1, Lcom/google/android/music/ui/StreamQualityListAdapter$StreamQualityListItem;

    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0069

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b006c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/google/android/music/ui/StreamQualityListAdapter$StreamQualityListItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    .local v1, "item_low":Lcom/google/android/music/ui/StreamQualityListAdapter$StreamQualityListItem;
    new-instance v2, Lcom/google/android/music/ui/StreamQualityListAdapter$StreamQualityListItem;

    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b006a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/android/music/ui/StreamQualityListAdapter$StreamQualityListItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    .local v2, "item_normal":Lcom/google/android/music/ui/StreamQualityListAdapter$StreamQualityListItem;
    new-instance v0, Lcom/google/android/music/ui/StreamQualityListAdapter$StreamQualityListItem;

    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b006b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b006d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Lcom/google/android/music/ui/StreamQualityListAdapter$StreamQualityListItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    .local v0, "item_high":Lcom/google/android/music/ui/StreamQualityListAdapter$StreamQualityListItem;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamQualityList:Ljava/util/List;

    .line 641
    iget-object v3, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamQualityList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 642
    iget-object v3, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamQualityList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 643
    iget-object v3, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamQualityList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 644
    return-void
.end method

.method private killOurProcesses()V
    .locals 12

    .prologue
    const/4 v11, -0x1

    .line 706
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 707
    .local v5, "packageName":Ljava/lang/String;
    const-string v9, "activity"

    invoke-virtual {p0, v9}, Lcom/google/android/music/activities/MusicSettingsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 708
    .local v0, "activityManger":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    .line 709
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    const/4 v7, -0x1

    .line 710
    .local v7, "uiPid":I
    const/4 v3, -0x1

    .line 711
    .local v3, "mainPid":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":main"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 712
    .local v4, "mainProcessName":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":ui"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 713
    .local v8, "uiProcessName":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 714
    .local v6, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v9, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 715
    iget v3, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    .line 719
    :cond_1
    :goto_0
    if-eq v7, v11, :cond_0

    if-eq v3, v11, :cond_0

    .line 723
    .end local v6    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_2
    if-eq v3, v11, :cond_3

    .line 724
    invoke-static {v3}, Landroid/os/Process;->killProcess(I)V

    .line 726
    :cond_3
    if-eq v7, v11, :cond_4

    .line 727
    invoke-static {v7}, Landroid/os/Process;->killProcess(I)V

    .line 729
    :cond_4
    return-void

    .line 716
    .restart local v6    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_5
    iget-object v9, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 717
    iget v7, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    goto :goto_0
.end method

.method private manageNautilusStatus()V
    .locals 2

    .prologue
    .line 454
    sget-object v0, Lcom/google/android/music/activities/MusicSettingsActivity$6;->$SwitchMap$com$google$android$music$NautilusStatus:[I

    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mNautilusStatus:Lcom/google/android/music/NautilusStatus;

    invoke-virtual {v1}, Lcom/google/android/music/NautilusStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 475
    const-string v0, "MusicSettings"

    const-string v1, "invalid all access status"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    :cond_0
    :goto_0
    return-void

    .line 456
    :pswitch_0
    sget-object v0, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->SETTINGS:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-static {p0, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnDemand(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)Z

    goto :goto_0

    .line 459
    :pswitch_1
    sget-object v0, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->SETTINGS:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-static {p0, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnDemand(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)Z

    goto :goto_0

    .line 470
    :pswitch_2
    invoke-static {p0}, Lcom/google/android/music/purchase/Finsky;->startCancelNautilusActivity(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 471
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->finish()V

    goto :goto_0

    .line 454
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private refreshNautilusUI()V
    .locals 3

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getNautilusStatus()Lcom/google/android/music/NautilusStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mNautilusStatus:Lcom/google/android/music/NautilusStatus;

    .line 485
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mSelectedAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mNautilusStatus:Lcom/google/android/music/NautilusStatus;

    sget-object v1, Lcom/google/android/music/NautilusStatus;->UNAVAILABLE:Lcom/google/android/music/NautilusStatus;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-static {p0, v0}, Lcom/google/android/music/purchase/Finsky;->isDirectPurchaseAvailable(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 487
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAccountTypeScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 488
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mManageNautilusScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 551
    :goto_0
    return-void

    .line 493
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0294

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_2

    .line 495
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAccountTypeScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 497
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0299

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_3

    .line 499
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mManageNautilusScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 502
    :cond_3
    sget-object v0, Lcom/google/android/music/activities/MusicSettingsActivity$6;->$SwitchMap$com$google$android$music$NautilusStatus:[I

    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mNautilusStatus:Lcom/google/android/music/NautilusStatus;

    invoke-virtual {v1}, Lcom/google/android/music/NautilusStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 549
    const-string v0, "MusicSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid nautilus status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mNautilusStatus:Lcom/google/android/music/NautilusStatus;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 504
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAccountTypeScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 505
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mManageNautilusScreen:Landroid/preference/PreferenceScreen;

    const v1, 0x7f0b029b

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    .line 507
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mManageNautilusScreen:Landroid/preference/PreferenceScreen;

    const v1, 0x7f0b029c

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    goto :goto_0

    .line 511
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAccountTypeScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 512
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mManageNautilusScreen:Landroid/preference/PreferenceScreen;

    const v1, 0x7f0b029d

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    .line 514
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mManageNautilusScreen:Landroid/preference/PreferenceScreen;

    const v1, 0x7f0b029e

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    goto/16 :goto_0

    .line 536
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAccountTypeScreen:Landroid/preference/PreferenceScreen;

    const v1, 0x7f0b0297

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    .line 538
    const v0, 0x7f0b0298

    invoke-direct {p0, v0}, Lcom/google/android/music/activities/MusicSettingsActivity;->setBillDate(I)V

    .line 540
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-static {p0, v0}, Lcom/google/android/music/purchase/Finsky;->doesSupportNautilusCancelation(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 541
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mManageNautilusScreen:Landroid/preference/PreferenceScreen;

    const v1, 0x7f0b02a1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    .line 542
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mManageNautilusScreen:Landroid/preference/PreferenceScreen;

    const v1, 0x7f0b02a2

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    goto/16 :goto_0

    .line 545
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mManageNautilusScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    .line 502
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private refreshUI()V
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 566
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mSelectedAccount:Landroid/accounts/Account;

    .line 568
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mSelectedAccount:Landroid/accounts/Account;

    if-eqz v4, :cond_1

    .line 569
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->showStreamingPreferences()V

    .line 574
    :goto_0
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->refreshNautilusUI()V

    .line 576
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getSeletectedAccountForDisplay()Ljava/lang/String;

    move-result-object v0

    .line 577
    .local v0, "accountName":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 578
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAccountSettingsScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0062

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 585
    :goto_1
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mCachedStreamed:Landroid/preference/CheckBoxPreference;

    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isCachedStreamingMusicEnabled()Z

    move-result v7

    invoke-virtual {v4, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 586
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAutoCache:Landroid/preference/CheckBoxPreference;

    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isAutoCachingEnabled()Z

    move-result v7

    invoke-virtual {v4, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 587
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamOnlyOnWifi:Landroid/preference/CheckBoxPreference;

    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isStreamOnlyOnWifi()Z

    move-result v7

    invoke-virtual {v4, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 588
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadOnlyOnWifi:Landroid/preference/CheckBoxPreference;

    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isOfflineDLOnlyOnWifi()Z

    move-result v7

    invoke-virtual {v4, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 589
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mWearSyncEnabled:Landroid/preference/CheckBoxPreference;

    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isWearSyncEnabled()Z

    move-result v7

    invoke-virtual {v4, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 590
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDebugLogs:Landroid/preference/CheckBoxPreference;

    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isLogFilesEnabled()Z

    move-result v7

    invoke-virtual {v4, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 591
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mContentFilter:Landroid/preference/CheckBoxPreference;

    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getContentFilter()I

    move-result v4

    const/4 v8, 0x2

    if-ne v4, v8, :cond_3

    move v4, v5

    :goto_2
    invoke-virtual {v7, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 595
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->updateSecondaryExternalSetting()V

    .line 597
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isMediaRouteSupportEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 598
    invoke-static {p0}, Landroid/support/v7/media/MediaRouter;->getInstance(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v2

    .line 599
    .local v2, "mediaRouter":Landroid/support/v7/media/MediaRouter;
    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v3

    .line 600
    .local v3, "routeInfo":Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-virtual {v3}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getPlaybackType()I

    move-result v4

    if-nez v4, :cond_4

    move v1, v5

    .line 602
    .local v1, "isLocal":Z
    :goto_3
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mEqualizerScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 603
    iget-object v5, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mEqualizerScreen:Landroid/preference/PreferenceScreen;

    if-eqz v1, :cond_5

    const v4, 0x7f0b0272

    :goto_4
    invoke-virtual {v5, v4}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    .line 607
    .end local v1    # "isLocal":Z
    .end local v2    # "mediaRouter":Landroid/support/v7/media/MediaRouter;
    .end local v3    # "routeInfo":Landroid/support/v7/media/MediaRouter$RouteInfo;
    :cond_0
    return-void

    .line 571
    .end local v0    # "accountName":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->hideStreamingPreferences()V

    goto/16 :goto_0

    .line 581
    .restart local v0    # "accountName":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAccountSettingsScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0063

    new-array v9, v5, [Ljava/lang/Object;

    aput-object v0, v9, v6

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_3
    move v4, v6

    .line 591
    goto :goto_2

    .restart local v2    # "mediaRouter":Landroid/support/v7/media/MediaRouter;
    .restart local v3    # "routeInfo":Landroid/support/v7/media/MediaRouter$RouteInfo;
    :cond_4
    move v1, v6

    .line 600
    goto :goto_3

    .line 603
    .restart local v1    # "isLocal":Z
    :cond_5
    const v4, 0x7f0b0273

    goto :goto_4
.end method

.method private setBillDate(I)V
    .locals 7
    .param p1, "summaryResId"    # I

    .prologue
    .line 554
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->getNautilusExpirationTimeInMillisec()J

    move-result-wide v2

    .line 555
    .local v2, "nautilisExp":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 556
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 557
    .local v0, "date":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v4, p1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 558
    .local v1, "summary":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAccountTypeScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 562
    .end local v0    # "date":Ljava/lang/String;
    .end local v1    # "summary":Ljava/lang/String;
    :goto_0
    return-void

    .line 560
    :cond_0
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAccountTypeScreen:Landroid/preference/PreferenceScreen;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private showStreamQualityDialog()V
    .locals 6

    .prologue
    .line 610
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamQuality()I

    move-result v2

    .line 611
    .local v2, "checkedItem":I
    new-instance v0, Lcom/google/android/music/ui/StreamQualityListAdapter;

    const v4, 0x7f0400db

    iget-object v5, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamQualityList:Ljava/util/List;

    invoke-direct {v0, p0, v4, v5, v2}, Lcom/google/android/music/ui/StreamQualityListAdapter;-><init>(Landroid/content/Context;ILjava/util/List;I)V

    .line 613
    .local v0, "adapter":Lcom/google/android/music/ui/StreamQualityListAdapter;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 614
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v4, 0x7f0b0068

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/google/android/music/activities/MusicSettingsActivity$2;

    invoke-direct {v5, p0}, Lcom/google/android/music/activities/MusicSettingsActivity$2;-><init>(Lcom/google/android/music/activities/MusicSettingsActivity;)V

    invoke-virtual {v4, v0, v2, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 626
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 627
    .local v3, "d":Landroid/app/Dialog;
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 628
    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    .line 629
    return-void
.end method

.method private showStreamingPreferences()V
    .locals 3

    .prologue
    .line 380
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 381
    .local v0, "root":Landroid/preference/PreferenceScreen;
    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadingCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 382
    iget-boolean v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mIsWifiOnly:Z

    if-nez v1, :cond_0

    .line 383
    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamingCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 385
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mRefreshScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 386
    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mContentFilter:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 388
    return-void
.end method

.method private updateSecondaryExternalSetting()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 320
    iget-boolean v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mSecondaryExternalEnabled:Z

    if-eqz v6, :cond_0

    iget-boolean v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mIsExternalEmulated:Z

    if-eqz v6, :cond_0

    .line 321
    invoke-static {p0}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v0

    .line 322
    .local v0, "clm":Lcom/google/android/music/download/cache/CacheLocationManager;
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->getSelectedStorageVolumeId()Ljava/util/UUID;

    move-result-object v5

    .line 323
    .local v5, "volumeId":Ljava/util/UUID;
    if-eqz v5, :cond_2

    .line 324
    invoke-virtual {v0, v5}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownLocationByID(Ljava/util/UUID;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v3

    .line 326
    .local v3, "location":Lcom/google/android/music/download/cache/CacheLocation;
    invoke-static {v5, p0}, Lcom/google/android/music/download/cache/CacheUtils;->isVolumeMounted(Ljava/util/UUID;Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 329
    invoke-virtual {v0, v3}, Lcom/google/android/music/download/cache/CacheLocationManager;->getStorageMetadata(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    move-result-object v4

    .line 331
    .local v4, "metadata":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStorageLocationScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v4, v8, v3}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->getDescription(ZLcom/google/android/music/download/cache/CacheLocation;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 356
    .end local v0    # "clm":Lcom/google/android/music/download/cache/CacheLocationManager;
    .end local v3    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    .end local v4    # "metadata":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    .end local v5    # "volumeId":Ljava/util/UUID;
    :cond_0
    :goto_0
    return-void

    .line 335
    .restart local v0    # "clm":Lcom/google/android/music/download/cache/CacheLocationManager;
    .restart local v3    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    .restart local v5    # "volumeId":Ljava/util/UUID;
    :cond_1
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStorageLocationScreen:Landroid/preference/PreferenceScreen;

    const v7, 0x7f0b02be

    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    goto :goto_0

    .line 342
    .end local v3    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/music/download/cache/CacheLocationManager;->deviceHasExternalStorage()Z

    move-result v1

    .line 343
    .local v1, "externalCapable":Z
    invoke-virtual {v0}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownUsableLocations()Ljava/util/Collection;

    move-result-object v2

    .line 344
    .local v2, "knownLocations":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/download/cache/CacheLocation;>;"
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStorageLocationScreen:Landroid/preference/PreferenceScreen;

    const v7, 0x7f0b02b4

    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    .line 345
    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v6

    if-le v6, v9, :cond_3

    .line 347
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStorageLocationScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v6, v9}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    goto :goto_0

    .line 351
    :cond_3
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStorageLocationScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v6, v8}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    goto :goto_0
.end method

.method private updateStreamQualitySummary()V
    .locals 2

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isLowStreamQuality()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamQuality:Landroid/preference/PreferenceScreen;

    const v1, 0x7f0b0069

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    .line 366
    :goto_0
    return-void

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isHighStreamQuality()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamQuality:Landroid/preference/PreferenceScreen;

    const v1, 0x7f0b006b

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    goto :goto_0

    .line 364
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamQuality:Landroid/preference/PreferenceScreen;

    const v1, 0x7f0b006a

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    goto :goto_0
.end method

.method private updateVersion()V
    .locals 5

    .prologue
    .line 419
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 420
    .local v1, "info":Landroid/content/pm/PackageInfo;
    iget-object v2, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mVersion:Landroid/preference/PreferenceScreen;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "v"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 424
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 421
    :catch_0
    move-exception v0

    .line 422
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "MusicSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Package not found (to retrieve version number)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected isValidFragment(Ljava/lang/String;)Z
    .locals 1
    .param p1, "fragmentName"    # Ljava/lang/String;

    .prologue
    .line 803
    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 135
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 138
    .local v3, "incoming":Landroid/content/Intent;
    const-string v7, ":android:show_fragment"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 139
    const-string v7, ":android:show_fragment"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 141
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 143
    const v7, 0x7f080004

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->addPreferencesFromResource(I)V

    .line 145
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0266

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceCategory;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    .line 147
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0267

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceCategory;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadingCategory:Landroid/preference/PreferenceCategory;

    .line 149
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0268

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceCategory;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamingCategory:Landroid/preference/PreferenceCategory;

    .line 151
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0269

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceCategory;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDeveloperCategory:Landroid/preference/PreferenceCategory;

    .line 154
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0332

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceScreen;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mRefreshScreen:Landroid/preference/PreferenceScreen;

    .line 156
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0294

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceScreen;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAccountTypeScreen:Landroid/preference/PreferenceScreen;

    .line 158
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0299

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceScreen;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mManageNautilusScreen:Landroid/preference/PreferenceScreen;

    .line 161
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 163
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0064

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/CheckBoxPreference;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamOnlyOnWifi:Landroid/preference/CheckBoxPreference;

    .line 166
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0067

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceScreen;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamQuality:Landroid/preference/PreferenceScreen;

    .line 168
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->updateStreamQualitySummary()V

    .line 169
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->initStreamQualityList()V

    .line 171
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b007a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/CheckBoxPreference;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mCachedStreamed:Landroid/preference/CheckBoxPreference;

    .line 174
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b007d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/CheckBoxPreference;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAutoCache:Landroid/preference/CheckBoxPreference;

    .line 177
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0291

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceScreen;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mClearCache:Landroid/preference/PreferenceScreen;

    .line 180
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0077

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/CheckBoxPreference;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadOnlyOnWifi:Landroid/preference/CheckBoxPreference;

    .line 183
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0088

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/CheckBoxPreference;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mWearSyncEnabled:Landroid/preference/CheckBoxPreference;

    .line 186
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0060

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceScreen;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAccountSettingsScreen:Landroid/preference/PreferenceScreen;

    .line 191
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0080

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceScreen;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadQueueScreen:Landroid/preference/PreferenceScreen;

    .line 194
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isManageDownloadsEnabled()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 195
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadQueueScreen:Landroid/preference/PreferenceScreen;

    const v8, 0x7f0b0083

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    .line 196
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadQueueScreen:Landroid/preference/PreferenceScreen;

    const v8, 0x7f0b0084

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    .line 199
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0085

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceScreen;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStorageLocationScreen:Landroid/preference/PreferenceScreen;

    .line 202
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b006f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/CheckBoxPreference;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDebugLogs:Landroid/preference/CheckBoxPreference;

    .line 205
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0074

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/CheckBoxPreference;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mContentFilter:Landroid/preference/CheckBoxPreference;

    .line 208
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v7

    if-nez v7, :cond_2

    .line 209
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mContentFilter:Landroid/preference/CheckBoxPreference;

    const v8, 0x7f0b0076

    invoke-virtual {v7, v8}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    .line 212
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0270

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceScreen;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mEqualizerScreen:Landroid/preference/PreferenceScreen;

    .line 214
    new-instance v2, Landroid/content/Intent;

    const-string v7, "android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 215
    .local v2, "i":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v7, v2, v12}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    .line 216
    .local v4, "info":Landroid/content/pm/ResolveInfo;
    if-eqz v4, :cond_3

    iget-object v7, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-boolean v7, v7, Landroid/content/pm/ActivityInfo;->exported:Z

    if-nez v7, :cond_4

    .line 217
    :cond_3
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    iget-object v8, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mEqualizerScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 220
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0323

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceScreen;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mManageDeviceScreen:Landroid/preference/PreferenceScreen;

    .line 223
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mManageDeviceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0325

    new-array v10, v13, [Ljava/lang/Object;

    iget v11, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMaximumDeviceNumber:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 226
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamOnlyOnWifi:Landroid/preference/CheckBoxPreference;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamQuality:Landroid/preference/PreferenceScreen;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mCachedStreamed:Landroid/preference/CheckBoxPreference;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAutoCache:Landroid/preference/CheckBoxPreference;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadOnlyOnWifi:Landroid/preference/CheckBoxPreference;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAccountSettingsScreen:Landroid/preference/PreferenceScreen;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadQueueScreen:Landroid/preference/PreferenceScreen;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDebugLogs:Landroid/preference/CheckBoxPreference;

    if-nez v7, :cond_6

    .line 234
    :cond_5
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Could not find the preference screens"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 237
    :cond_6
    const-string v7, "music_version_key"

    invoke-virtual {p0, v7}, Lcom/google/android/music/activities/MusicSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceScreen;

    iput-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mVersion:Landroid/preference/PreferenceScreen;

    .line 239
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->updateVersion()V

    .line 241
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isHoneycombOrGreater()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 242
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 243
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_7

    .line 244
    invoke-virtual {v0, v13}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 248
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    :cond_7
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getIsWifiOnly()Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mIsWifiOnly:Z

    .line 250
    iget-boolean v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mIsWifiOnly:Z

    if-eqz v7, :cond_8

    .line 251
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamingCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 252
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadingCategory:Landroid/preference/PreferenceCategory;

    iget-object v8, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadOnlyOnWifi:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 253
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Could not remove the streaming over wifi preferences"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 259
    :cond_8
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isCachingFeatureAvailable()Z

    move-result v7

    if-nez v7, :cond_a

    .line 260
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadingCategory:Landroid/preference/PreferenceCategory;

    iget-object v8, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mCachedStreamed:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 261
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Could not remove the caching preferences"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 264
    :cond_9
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadingCategory:Landroid/preference/PreferenceCategory;

    iget-object v8, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAutoCache:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 265
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Could not remove the autocaching preference"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 270
    :cond_a
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isAutoCachingAvailable()Z

    move-result v7

    if-nez v7, :cond_b

    .line 271
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadingCategory:Landroid/preference/PreferenceCategory;

    iget-object v8, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAutoCache:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 272
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Could not remove the autocaching preference"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 277
    :cond_b
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isOfflineFeatureAvailable()Z

    move-result v7

    if-nez v7, :cond_c

    .line 278
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadingCategory:Landroid/preference/PreferenceCategory;

    iget-object v8, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadQueueScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    move-result v7

    if-nez v7, :cond_c

    .line 279
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Could not remove the offline preferences"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 284
    :cond_c
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isWearSyncAvailable()Z

    move-result v7

    if-nez v7, :cond_d

    .line 285
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadingCategory:Landroid/preference/PreferenceCategory;

    iget-object v8, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mWearSyncEnabled:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    move-result v7

    if-nez v7, :cond_d

    .line 286
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Could not remove the wear sync preferences"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 291
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 292
    .local v6, "resolver":Landroid/content/ContentResolver;
    const-string v7, "music_debug_logs_enabled"

    invoke-static {v6, v7, v12}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v5

    .line 294
    .local v5, "isDebugLogsAvailable":Z
    if-nez v5, :cond_f

    .line 295
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDeveloperCategory:Landroid/preference/PreferenceCategory;

    iget-object v8, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDebugLogs:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    move-result v7

    if-nez v7, :cond_e

    .line 296
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Could not remove the debug logs preferences"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 299
    :cond_e
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v7, v12}, Lcom/google/android/music/preferences/MusicPreferences;->setLogFilesEnable(Z)V

    .line 301
    :cond_f
    const-string v7, "music_enable_secondary_sdcards"

    invoke-static {v6, v7, v13}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mSecondaryExternalEnabled:Z

    .line 308
    invoke-static {}, Lcom/google/android/music/utils/PostFroyoUtils$EnvironmentCompat;->isExternalStorageEmulated()Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mIsExternalEmulated:Z

    .line 310
    invoke-static {p0}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/music/download/cache/CacheLocationManager;->deviceHasExternalStorage()Z

    move-result v1

    .line 311
    .local v1, "hasExternal":Z
    iget-boolean v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mSecondaryExternalEnabled:Z

    if-eqz v7, :cond_10

    iget-boolean v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mIsExternalEmulated:Z

    if-eqz v7, :cond_10

    if-nez v1, :cond_11

    .line 312
    :cond_10
    iget-object v7, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadingCategory:Landroid/preference/PreferenceCategory;

    iget-object v8, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStorageLocationScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    move-result v7

    if-nez v7, :cond_11

    .line 313
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Could not remove the download location preference"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 317
    :cond_11
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 755
    if-eqz p1, :cond_0

    .line 756
    const/4 v1, 0x0

    .line 779
    :goto_0
    return-object v1

    .line 759
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 760
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDebugLogs:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 761
    const v1, 0x7f0b0072

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 765
    :goto_1
    const v1, 0x7f0b004f

    new-instance v2, Lcom/google/android/music/activities/MusicSettingsActivity$4;

    invoke-direct {v2, p0}, Lcom/google/android/music/activities/MusicSettingsActivity$4;-><init>(Lcom/google/android/music/activities/MusicSettingsActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 772
    const v1, 0x7f0b004e

    new-instance v2, Lcom/google/android/music/activities/MusicSettingsActivity$5;

    invoke-direct {v2, p0}, Lcom/google/android/music/activities/MusicSettingsActivity$5;-><init>(Lcom/google/android/music/activities/MusicSettingsActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 779
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    .line 763
    :cond_1
    const v1, 0x7f0b0073

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 447
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 448
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 449
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 392
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isHoneycombOrGreater()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 393
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->finish()V

    .line 394
    const/4 v0, 0x1

    .line 397
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 428
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 429
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 430
    .local v0, "localBroadcastManager":Landroid/support/v4/content/LocalBroadcastManager;
    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStorageBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 431
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "objValue"    # Ljava/lang/Object;

    .prologue
    .line 783
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->refreshUI()V

    .line 785
    const/4 v0, 0x1

    return v0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 8
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 649
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAccountSettingsScreen:Landroid/preference/PreferenceScreen;

    if-ne p2, v6, :cond_2

    .line 650
    invoke-static {p0, v4}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialToChooseAccount(Landroid/app/Activity;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 651
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->finish()V

    :cond_0
    :goto_0
    move v4, v5

    .line 702
    :cond_1
    return v4

    .line 653
    :cond_2
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mCachedStreamed:Landroid/preference/CheckBoxPreference;

    if-ne p2, v6, :cond_3

    .line 654
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mCachedStreamed:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v6

    invoke-virtual {v4, v6}, Lcom/google/android/music/preferences/MusicPreferences;->setCachedStreamingMusicEnabled(Z)V

    goto :goto_0

    .line 655
    :cond_3
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAutoCache:Landroid/preference/CheckBoxPreference;

    if-ne p2, v6, :cond_4

    .line 656
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mAutoCache:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v6

    invoke-virtual {v4, v6}, Lcom/google/android/music/preferences/MusicPreferences;->setAutoCachingEnabled(Z)V

    goto :goto_0

    .line 657
    :cond_4
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamOnlyOnWifi:Landroid/preference/CheckBoxPreference;

    if-ne p2, v6, :cond_5

    .line 658
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamOnlyOnWifi:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v6

    invoke-virtual {v4, v6}, Lcom/google/android/music/preferences/MusicPreferences;->setStreamOnlyOnWifi(Z)V

    goto :goto_0

    .line 659
    :cond_5
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStreamQuality:Landroid/preference/PreferenceScreen;

    if-ne p2, v6, :cond_6

    .line 660
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->showStreamQualityDialog()V

    goto :goto_0

    .line 661
    :cond_6
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStorageLocationScreen:Landroid/preference/PreferenceScreen;

    if-ne p2, v6, :cond_7

    .line 662
    invoke-static {p0}, Lcom/google/android/music/ui/SDCardDialogActivity;->showSelectionDialog(Landroid/content/Context;)V

    goto :goto_0

    .line 663
    :cond_7
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadOnlyOnWifi:Landroid/preference/CheckBoxPreference;

    if-ne p2, v6, :cond_8

    .line 664
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadOnlyOnWifi:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v6

    invoke-virtual {v4, v6}, Lcom/google/android/music/preferences/MusicPreferences;->setOffineDLOnlyOnWifi(Z)V

    goto :goto_0

    .line 665
    :cond_8
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDebugLogs:Landroid/preference/CheckBoxPreference;

    if-ne p2, v6, :cond_9

    .line 666
    invoke-virtual {p0, v4}, Lcom/google/android/music/activities/MusicSettingsActivity;->showDialog(I)V

    goto :goto_0

    .line 667
    :cond_9
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mWearSyncEnabled:Landroid/preference/CheckBoxPreference;

    if-ne p2, v6, :cond_a

    .line 668
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mWearSyncEnabled:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v6

    invoke-virtual {v4, v6}, Lcom/google/android/music/preferences/MusicPreferences;->setWearSyncEnabled(Z)V

    goto :goto_0

    .line 669
    :cond_a
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mEqualizerScreen:Landroid/preference/PreferenceScreen;

    if-ne p2, v6, :cond_c

    .line 670
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 671
    .local v2, "i":Landroid/content/Intent;
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getAudioSessionId()I

    move-result v0

    .line 672
    .local v0, "audioSession":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_b

    .line 673
    const-string v4, "android.media.extra.AUDIO_SESSION"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 677
    :goto_1
    const/16 v4, 0x1a

    invoke-virtual {p0, v2, v4}, Lcom/google/android/music/activities/MusicSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 675
    :cond_b
    const-string v4, "MusicSettings"

    const-string v6, "Failed to get valid audio session id"

    invoke-static {v4, v6}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 678
    .end local v0    # "audioSession":I
    .end local v2    # "i":Landroid/content/Intent;
    :cond_c
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mContentFilter:Landroid/preference/CheckBoxPreference;

    if-ne p2, v6, :cond_e

    .line 679
    const/4 v1, 0x1

    .line 680
    .local v1, "filterValue":I
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mContentFilter:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 681
    const/4 v1, 0x2

    .line 683
    :cond_d
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v4, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setContentFilter(I)V

    goto/16 :goto_0

    .line 684
    .end local v1    # "filterValue":I
    :cond_e
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mManageNautilusScreen:Landroid/preference/PreferenceScreen;

    if-ne p2, v6, :cond_f

    .line 685
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->manageNautilusStatus()V

    goto/16 :goto_0

    .line 686
    :cond_f
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mClearCache:Landroid/preference/PreferenceScreen;

    if-ne p2, v6, :cond_10

    .line 688
    new-instance v3, Landroid/content/Intent;

    const-class v6, Lcom/google/android/music/download/cache/TrackCacheService;

    invoke-direct {v3, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 689
    .local v3, "serviceIntent":Landroid/content/Intent;
    const-string v6, "com.google.android.music.download.cache.CacheService.CLEAR_CACHE"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 690
    invoke-virtual {p0, v3}, Lcom/google/android/music/activities/MusicSettingsActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 691
    invoke-virtual {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0357

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 693
    .end local v3    # "serviceIntent":Landroid/content/Intent;
    :cond_10
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mDownloadQueueScreen:Landroid/preference/PreferenceScreen;

    if-ne p2, v6, :cond_11

    .line 694
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/google/android/music/ui/DownloadContainerActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 695
    .restart local v2    # "i":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/google/android/music/activities/MusicSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 696
    .end local v2    # "i":Landroid/content/Intent;
    :cond_11
    iget-object v6, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mRefreshScreen:Landroid/preference/PreferenceScreen;

    if-ne p2, v6, :cond_1

    .line 697
    iget-object v4, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-static {v4, v5}, Lcom/google/android/music/utils/MusicUtils;->requestSync(Lcom/google/android/music/preferences/MusicPreferences;Z)V

    .line 698
    const v4, 0x7f0b0059

    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 435
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 436
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 437
    .local v0, "localBroadcastManager":Landroid/support/v4/content/LocalBroadcastManager;
    iget-object v1, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->mStorageBroadcastReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/google/android/music/activities/MusicSettingsActivity;->sStorageBroadcastFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 438
    invoke-direct {p0}, Lcom/google/android/music/activities/MusicSettingsActivity;->refreshUI()V

    .line 439
    return-void
.end method
