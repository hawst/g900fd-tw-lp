.class public Lcom/google/android/music/ui/DownloadContainerFragment;
.super Lcom/google/android/music/ui/BaseListFragment;
.source "DownloadContainerFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/DownloadContainerFragment$2;,
        Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;,
        Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;,
        Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;,
        Lcom/google/android/music/ui/DownloadContainerFragment$onContainerRowClickListener;,
        Lcom/google/android/music/ui/DownloadContainerFragment$onDownloadButtonClickListener;,
        Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/music/ui/BaseListFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final QUERY_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

.field private mButtonContainer:Landroid/view/ViewGroup;

.field private mDownloadButton:Landroid/widget/TextView;

.field private mHeaderIcon:Landroid/widget/ImageView;

.field private mListBackingData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final mProcessedData:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mUnavailableCardOpacity:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 77
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "container_type"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "subtitle"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "subtype"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "artUrls"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "remoteId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/DownloadContainerFragment;->QUERY_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseListFragment;-><init>()V

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    .line 101
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mProcessedData:Ljava/util/LinkedHashSet;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mListBackingData:Ljava/util/List;

    .line 150
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/DownloadContainerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/DownloadContainerFragment;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->updateHeaderButton()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/DownloadContainerFragment;)Ljava/util/LinkedHashSet;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/DownloadContainerFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mProcessedData:Ljava/util/LinkedHashSet;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/DownloadContainerFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/DownloadContainerFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mListBackingData:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/music/ui/DownloadContainerFragment;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/DownloadContainerFragment;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mListBackingData:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/DownloadContainerFragment;)Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/DownloadContainerFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/DownloadContainerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/DownloadContainerFragment;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->updateButtonAndMenuVisibility()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/DownloadContainerFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/DownloadContainerFragment;

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mUnavailableCardOpacity:F

    return v0
.end method

.method private addManageDownloadButton()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 193
    invoke-direct {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->showMenuInActionBar()Z

    move-result v3

    if-nez v3, :cond_0

    .line 194
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 195
    .local v1, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    .line 196
    .local v2, "lv":Landroid/widget/ListView;
    const v3, 0x7f040065

    invoke-virtual {v1, v3, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mButtonContainer:Landroid/view/ViewGroup;

    .line 198
    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mButtonContainer:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 199
    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    .line 201
    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mButtonContainer:Landroid/view/ViewGroup;

    const v4, 0x7f0e011e

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 204
    .local v0, "buttonParent":Landroid/view/ViewGroup;
    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mButtonContainer:Landroid/view/ViewGroup;

    const v4, 0x7f0e0120

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mDownloadButton:Landroid/widget/TextView;

    .line 205
    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mButtonContainer:Landroid/view/ViewGroup;

    const v4, 0x7f0e011f

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mHeaderIcon:Landroid/widget/ImageView;

    .line 206
    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mHeaderIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 207
    new-instance v3, Lcom/google/android/music/ui/DownloadContainerFragment$onDownloadButtonClickListener;

    invoke-direct {v3, p0}, Lcom/google/android/music/ui/DownloadContainerFragment$onDownloadButtonClickListener;-><init>(Lcom/google/android/music/ui/DownloadContainerFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    invoke-direct {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->updateHeaderButton()V

    .line 210
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 211
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/music/utils/ViewUtils;->setWidthToShortestEdge(Landroid/content/Context;Landroid/view/ViewGroup;)I

    .line 214
    .end local v0    # "buttonParent":Landroid/view/ViewGroup;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "lv":Landroid/widget/ListView;
    :cond_0
    return-void
.end method

.method private applyDifferences(Landroid/util/Pair;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 438
    .local p1, "differences":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/List<Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;>;Ljava/util/List<Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;>;>;"
    iget-object v3, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/util/List;

    .line 439
    .local v3, "toBeRemoved":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;>;"
    iget-object v2, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    .line 440
    .local v2, "toBeAdded":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;

    .line 441
    .local v0, "entry":Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->setRemoving(Z)V

    goto :goto_0

    .line 444
    .end local v0    # "entry":Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;
    :cond_0
    new-instance v4, Lcom/google/android/music/ui/DownloadContainerFragment$1;

    invoke-direct {v4, p0, v3}, Lcom/google/android/music/ui/DownloadContainerFragment$1;-><init>(Lcom/google/android/music/ui/DownloadContainerFragment;Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const/16 v6, 0xbb8

    invoke-static {v4, v5, v6}, Lcom/google/android/music/utils/MusicUtils;->runDelayedOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;I)Landroid/os/Handler;

    .line 454
    iget-object v4, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mProcessedData:Ljava/util/LinkedHashSet;

    invoke-virtual {v4, v2}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 462
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 463
    iget-object v4, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    invoke-virtual {v4}, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->notifyDataSetInvalidated()V

    .line 468
    :cond_1
    :goto_1
    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mProcessedData:Ljava/util/LinkedHashSet;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v4, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mListBackingData:Ljava/util/List;

    .line 469
    return-void

    .line 464
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 465
    iget-object v4, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    invoke-virtual {v4}, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->notifyDataSetChanged()V

    goto :goto_1
.end method

.method private calculateDifferences(Ljava/util/LinkedHashSet;)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 422
    .local p1, "newEntries":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 423
    .local v1, "onlyInOld":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 424
    .local v0, "onlyInNew":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;>;"
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 426
    .local v2, "working":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;>;"
    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mProcessedData:Ljava/util/LinkedHashSet;

    invoke-virtual {v2, v3}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 427
    invoke-virtual {v2, p1}, Ljava/util/LinkedHashSet;->removeAll(Ljava/util/Collection;)Z

    .line 428
    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 430
    invoke-virtual {v2}, Ljava/util/LinkedHashSet;->clear()V

    .line 431
    invoke-virtual {v2, p1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 432
    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mProcessedData:Ljava/util/LinkedHashSet;

    invoke-virtual {v2, v3}, Ljava/util/LinkedHashSet;->removeAll(Ljava/util/Collection;)Z

    .line 433
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 434
    new-instance v3, Landroid/util/Pair;

    invoke-direct {v3, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3
.end method

.method private loadFromCursor(Landroid/database/Cursor;)Ljava/util/LinkedHashSet;
    .locals 12
    .param p1, "c"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 377
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 378
    .local v0, "result":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;>;"
    if-nez p1, :cond_1

    .line 417
    :cond_0
    return-object v0

    .line 383
    :cond_1
    const/4 v10, -0x1

    invoke-interface {p1, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 384
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 385
    const/4 v1, 0x0

    .line 386
    .local v1, "item":Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;
    const/4 v10, 0x0

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    invoke-static {v10}, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->fromDbValue(I)Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    move-result-object v7

    .line 387
    .local v7, "type":Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;
    const/4 v10, 0x1

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 388
    .local v2, "id":J
    const/4 v10, 0x2

    invoke-interface {p1, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v5, 0x0

    .line 389
    .local v5, "title":Ljava/lang/String;
    :goto_1
    const/4 v10, 0x4

    invoke-interface {p1, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v10

    if-eqz v10, :cond_3

    const/high16 v4, -0x80000000

    .line 390
    .local v4, "subType":I
    :goto_2
    const/4 v10, 0x5

    invoke-interface {p1, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v10

    if-eqz v10, :cond_4

    const/4 v8, 0x0

    .line 391
    .local v8, "urls":Ljava/lang/String;
    :goto_3
    const/4 v6, 0x0

    .line 392
    .local v6, "subtitle":Ljava/lang/String;
    const/4 v10, 0x6

    invoke-interface {p1, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v10

    if-eqz v10, :cond_5

    const/4 v9, 0x0

    .line 394
    .local v9, "remoteId":Ljava/lang/String;
    :goto_4
    sget-object v10, Lcom/google/android/music/ui/DownloadContainerFragment$2;->$SwitchMap$com$google$android$music$ui$DownloadContainerFragment$ContainerType:[I

    invoke-virtual {v7}, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 413
    :goto_5
    new-instance v1, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;

    .end local v1    # "item":Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;
    invoke-direct/range {v1 .. v9}, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;-><init>(JILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    .restart local v1    # "item":Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;
    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 388
    .end local v4    # "subType":I
    .end local v5    # "title":Ljava/lang/String;
    .end local v6    # "subtitle":Ljava/lang/String;
    .end local v8    # "urls":Ljava/lang/String;
    .end local v9    # "remoteId":Ljava/lang/String;
    :cond_2
    const/4 v10, 0x2

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 389
    .restart local v5    # "title":Ljava/lang/String;
    :cond_3
    const/4 v10, 0x4

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    goto :goto_2

    .line 390
    .restart local v4    # "subType":I
    :cond_4
    const/4 v10, 0x5

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_3

    .line 392
    .restart local v6    # "subtitle":Ljava/lang/String;
    .restart local v8    # "urls":Ljava/lang/String;
    :cond_5
    const/4 v10, 0x6

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_4

    .line 396
    .restart local v9    # "remoteId":Ljava/lang/String;
    :pswitch_0
    const/4 v10, 0x3

    invoke-interface {p1, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v6, 0x0

    .line 397
    :goto_6
    goto :goto_5

    .line 396
    :cond_6
    const/4 v10, 0x3

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_6

    .line 399
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b02cb

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 401
    goto :goto_5

    .line 403
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b02cb

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 405
    goto :goto_5

    .line 407
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v10

    if-eqz v10, :cond_7

    const v10, 0x7f0b02cc

    :goto_7
    invoke-virtual {v11, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_5

    :cond_7
    const v10, 0x7f0b02cd

    goto :goto_7

    .line 394
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private showMenuInActionBar()Z
    .locals 1

    .prologue
    .line 243
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v0

    return v0
.end method

.method private updateButtonAndMenuVisibility()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 227
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    invoke-virtual {v0}, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/DownloadContainerFragment;->setEmptyScreenVisible(Z)V

    .line 229
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mButtonContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 230
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    invoke-virtual {v0}, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_5

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mButtonContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 237
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->showMenuInActionBar()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 238
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    .line 240
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 227
    goto :goto_0

    .line 233
    :cond_5
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mButtonContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method

.method private updateHeaderButton()V
    .locals 2

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isKeepOnDownloadingPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mDownloadButton:Landroid/widget/TextView;

    const v1, 0x7f0b02c9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 219
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mHeaderIcon:Landroid/widget/ImageView;

    const v1, 0x7f02014b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 224
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mDownloadButton:Landroid/widget/TextView;

    const v1, 0x7f0b02c7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 222
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mHeaderIcon:Landroid/widget/ImageView;

    const v1, 0x7f02012b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private updateList(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 369
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/DownloadContainerFragment;->loadFromCursor(Landroid/database/Cursor;)Ljava/util/LinkedHashSet;

    move-result-object v1

    .line 370
    .local v1, "newEntries":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;>;"
    invoke-direct {p0, v1}, Lcom/google/android/music/ui/DownloadContainerFragment;->calculateDifferences(Ljava/util/LinkedHashSet;)Landroid/util/Pair;

    move-result-object v0

    .line 371
    .local v0, "delta":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/List<Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;>;Ljava/util/List<Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;>;>;"
    invoke-direct {p0, v0}, Lcom/google/android/music/ui/DownloadContainerFragment;->applyDifferences(Landroid/util/Pair;)V

    .line 372
    iget-object v2, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    invoke-virtual {v2}, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->notifyDataSetChanged()V

    .line 373
    return-void
.end method


# virtual methods
.method protected initEmptyScreen()V
    .locals 1

    .prologue
    .line 185
    invoke-super {p0}, Lcom/google/android/music/ui/BaseListFragment;->initEmptyScreen()V

    .line 186
    const v0, 0x7f0b02a4

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/DownloadContainerFragment;->setEmptyScreenText(I)V

    .line 187
    const v0, 0x7f02017f

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/DownloadContainerFragment;->setEmptyImageView(I)V

    .line 188
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 154
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 155
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/DownloadContainerFragment;->setHasOptionsMenu(Z)V

    .line 156
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mUnavailableCardOpacity:F

    .line 158
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 3
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289
    new-instance v0, Lcom/google/android/music/ui/KeepOnDownloadCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/ui/DownloadContainerFragment;->QUERY_PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/KeepOnDownloadCursorLoader;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    const v2, 0x7f0e02a0

    .line 248
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    if-nez v0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->showMenuInActionBar()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isSideDrawerOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    const/high16 v0, 0x7f140000

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 254
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isKeepOnDownloadingPaused()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 255
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0b02ca

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 256
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02014a

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 261
    :goto_1
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    invoke-virtual {v0}, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 262
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 258
    :cond_2
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0b02c8

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 259
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02012a

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/DownloadContainerFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 180
    invoke-super {p0}, Lcom/google/android/music/ui/BaseListFragment;->onDestroyView()V

    .line 181
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 294
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 295
    invoke-direct {p0, p2}, Lcom/google/android/music/ui/DownloadContainerFragment;->updateList(Landroid/database/Cursor;)V

    .line 296
    invoke-direct {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->updateButtonAndMenuVisibility()V

    .line 298
    :cond_0
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 42
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/DownloadContainerFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 302
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mListBackingData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 303
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mProcessedData:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    .line 304
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    invoke-virtual {v0}, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->notifyDataSetChanged()V

    .line 305
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 268
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 275
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    :goto_0
    return v2

    .line 270
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isKeepOnDownloadingPaused()Z

    move-result v0

    .line 271
    .local v0, "isDownloadPaused":Z
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    if-nez v0, :cond_0

    move v1, v2

    :goto_1
    invoke-virtual {v3, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setKeepOnDownloadPaused(Z)V

    .line 272
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->supportInvalidateOptionsMenu()V

    goto :goto_0

    .line 271
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 268
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e02a0
        :pswitch_0
    .end packed-switch
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 162
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/BaseListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 164
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 165
    .local v0, "listView":Landroid/widget/ListView;
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 166
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    .line 168
    invoke-direct {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->addManageDownloadButton()V

    .line 169
    new-instance v2, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    invoke-direct {v2, p0, v4}, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;-><init>(Lcom/google/android/music/ui/DownloadContainerFragment;Lcom/google/android/music/ui/DownloadContainerFragment$1;)V

    iput-object v2, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    .line 170
    iget-object v2, p0, Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/DownloadContainerFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 172
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    .line 173
    .local v1, "lm":Landroid/support/v4/app/LoaderManager;
    invoke-virtual {v1, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 175
    return-void
.end method
