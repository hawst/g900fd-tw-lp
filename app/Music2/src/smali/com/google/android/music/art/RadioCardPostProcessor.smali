.class public Lcom/google/android/music/art/RadioCardPostProcessor;
.super Ljava/lang/Object;
.source "RadioCardPostProcessor.java"

# interfaces
.implements Lcom/google/android/music/art/ArtPostProcessor;


# static fields
.field private static final sInitializationLock:Ljava/lang/Object;

.field private static sInstance:Lcom/google/android/music/art/RadioCardPostProcessor;


# instance fields
.field private mSingleImageCropProcessor:Lcom/google/android/music/art/SingleImageCropPostProcessor;

.field private mTwoByTwoCompositor:Lcom/google/android/music/art/MultiImageCompositePostProcessor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/music/art/RadioCardPostProcessor;->sInitializationLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {}, Lcom/google/android/music/art/SingleImageCropPostProcessor;->getInstance()Lcom/google/android/music/art/SingleImageCropPostProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/art/RadioCardPostProcessor;->mSingleImageCropProcessor:Lcom/google/android/music/art/SingleImageCropPostProcessor;

    .line 23
    invoke-static {v1, v1}, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->getInstance(II)Lcom/google/android/music/art/MultiImageCompositePostProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/art/RadioCardPostProcessor;->mTwoByTwoCompositor:Lcom/google/android/music/art/MultiImageCompositePostProcessor;

    .line 26
    return-void
.end method

.method public static getInstance()Lcom/google/android/music/art/RadioCardPostProcessor;
    .locals 2

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/music/art/RadioCardPostProcessor;->sInstance:Lcom/google/android/music/art/RadioCardPostProcessor;

    if-nez v0, :cond_1

    .line 30
    sget-object v1, Lcom/google/android/music/art/RadioCardPostProcessor;->sInitializationLock:Ljava/lang/Object;

    monitor-enter v1

    .line 31
    :try_start_0
    sget-object v0, Lcom/google/android/music/art/RadioCardPostProcessor;->sInstance:Lcom/google/android/music/art/RadioCardPostProcessor;

    if-nez v0, :cond_0

    .line 32
    new-instance v0, Lcom/google/android/music/art/RadioCardPostProcessor;

    invoke-direct {v0}, Lcom/google/android/music/art/RadioCardPostProcessor;-><init>()V

    sput-object v0, Lcom/google/android/music/art/RadioCardPostProcessor;->sInstance:Lcom/google/android/music/art/RadioCardPostProcessor;

    .line 34
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    sget-object v0, Lcom/google/android/music/art/RadioCardPostProcessor;->sInstance:Lcom/google/android/music/art/RadioCardPostProcessor;

    return-object v0

    .line 34
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public aliasPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .locals 2
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "images"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .prologue
    .line 65
    const-string v0, "images must not be null"

    invoke-static {p2, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    const/4 v0, 0x0

    aget-object v0, p2, v0

    const-string v1, "images must contain at least one image"

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    array-length v0, p2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 68
    iget-object v0, p0, Lcom/google/android/music/art/RadioCardPostProcessor;->mSingleImageCropProcessor:Lcom/google/android/music/art/SingleImageCropPostProcessor;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/art/SingleImageCropPostProcessor;->aliasPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    move-result-object v0

    .line 70
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/art/RadioCardPostProcessor;->mTwoByTwoCompositor:Lcom/google/android/music/art/MultiImageCompositePostProcessor;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->aliasPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getConfig(Lcom/google/android/music/art/ArtDescriptor;)Landroid/graphics/Bitmap$Config;
    .locals 1
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 86
    sget-object v0, Lcom/google/android/music/art/ArtResolver2;->DEFAULT_CONFIG:Landroid/graphics/Bitmap$Config;

    return-object v0
.end method

.method public getMaxNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I
    .locals 1
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 76
    const/4 v0, 0x4

    return v0
.end method

.method public getMinNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I
    .locals 1
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method public getPostProcessingMode(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)I
    .locals 2
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "images"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .prologue
    .line 41
    invoke-static {p2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    array-length v0, p2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/google/android/music/art/RadioCardPostProcessor;->mSingleImageCropProcessor:Lcom/google/android/music/art/SingleImageCropPostProcessor;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/art/SingleImageCropPostProcessor;->getPostProcessingMode(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)I

    move-result v0

    .line 45
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/art/RadioCardPostProcessor;->mTwoByTwoCompositor:Lcom/google/android/music/art/MultiImageCompositePostProcessor;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->getPostProcessingMode(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)I

    move-result v0

    goto :goto_0
.end method

.method public renderPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;Landroid/graphics/Bitmap;)Z
    .locals 2
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "images"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .param p3, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 52
    const-string v0, "images must not be null"

    invoke-static {p2, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v0, "result must not be null"

    invoke-static {p3, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const/4 v0, 0x0

    aget-object v0, p2, v0

    const-string v1, "images must contain at least one image"

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    array-length v0, p2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/android/music/art/RadioCardPostProcessor;->mSingleImageCropProcessor:Lcom/google/android/music/art/SingleImageCropPostProcessor;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/music/art/SingleImageCropPostProcessor;->renderPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;Landroid/graphics/Bitmap;)Z

    move-result v0

    .line 58
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/art/RadioCardPostProcessor;->mTwoByTwoCompositor:Lcom/google/android/music/art/MultiImageCompositePostProcessor;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->renderPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;Landroid/graphics/Bitmap;)Z

    move-result v0

    goto :goto_0
.end method
