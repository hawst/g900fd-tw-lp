.class public Lcom/google/android/music/io/ChunkedInputStreamAdapter;
.super Ljava/io/InputStream;
.source "ChunkedInputStreamAdapter.java"


# instance fields
.field final mBuffer:[B

.field mBufferEnd:I

.field final mChunkSize:I

.field final mInput:Lcom/google/android/music/io/ChunkedInputStream;

.field mPosition:I


# direct methods
.method public constructor <init>(Lcom/google/android/music/io/ChunkedInputStream;)V
    .locals 2
    .param p1, "chunkedInputStream"    # Lcom/google/android/music/io/ChunkedInputStream;

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mInput:Lcom/google/android/music/io/ChunkedInputStream;

    .line 16
    iget-object v0, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mInput:Lcom/google/android/music/io/ChunkedInputStream;

    invoke-interface {v0}, Lcom/google/android/music/io/ChunkedInputStream;->getChunkSize()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mChunkSize:I

    .line 17
    iget v0, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mChunkSize:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBuffer:[B

    .line 18
    iput v1, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    .line 19
    iput v1, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBufferEnd:I

    .line 20
    return-void
.end method

.method private fillBuffer()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 140
    iput v4, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    .line 141
    iget-object v1, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mInput:Lcom/google/android/music/io/ChunkedInputStream;

    iget-object v2, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBuffer:[B

    iget v3, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mChunkSize:I

    invoke-interface {v1, v2, v4, v3}, Lcom/google/android/music/io/ChunkedInputStream;->read([BII)I

    move-result v0

    .line 142
    .local v0, "bytesRead":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 143
    iput v4, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBufferEnd:I

    .line 147
    :goto_0
    return-void

    .line 145
    :cond_0
    iput v0, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBufferEnd:I

    goto :goto_0
.end method


# virtual methods
.method public available()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    iget v0, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBufferEnd:I

    iget v1, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mInput:Lcom/google/android/music/io/ChunkedInputStream;

    invoke-interface {v1}, Lcom/google/android/music/io/ChunkedInputStream;->availableBytes()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mInput:Lcom/google/android/music/io/ChunkedInputStream;

    invoke-interface {v0}, Lcom/google/android/music/io/ChunkedInputStream;->close()V

    .line 137
    return-void
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    iget v0, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    iget v1, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBufferEnd:I

    if-ne v0, v1, :cond_0

    .line 25
    invoke-direct {p0}, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->fillBuffer()V

    .line 27
    :cond_0
    iget v0, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    iget v1, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBufferEnd:I

    if-ne v0, v1, :cond_1

    .line 28
    const/4 v0, -0x1

    .line 30
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBuffer:[B

    iget v1, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public read([B)I
    .locals 2
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 6
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 42
    const/4 v4, 0x1

    if-ge p3, v4, :cond_1

    const/4 v2, 0x0

    .line 89
    :cond_0
    :goto_0
    return v2

    .line 44
    :cond_1
    const/4 v2, 0x0

    .line 48
    .local v2, "totalBytesRead":I
    iget v4, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBufferEnd:I

    iget v5, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    sub-int/2addr v4, v5

    invoke-static {p3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 49
    .local v1, "bytesToCopy":I
    iget-object v4, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBuffer:[B

    iget v5, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    invoke-static {v4, v5, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 50
    iget v4, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    add-int/2addr v4, v1

    iput v4, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    .line 51
    add-int/2addr p2, v1

    .line 52
    add-int/2addr v2, v1

    .line 53
    sub-int/2addr p3, v1

    .line 58
    :goto_1
    iget v4, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mChunkSize:I

    if-lt p3, v4, :cond_3

    .line 59
    iget-object v4, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mInput:Lcom/google/android/music/io/ChunkedInputStream;

    iget v5, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mChunkSize:I

    invoke-interface {v4, p1, p2, v5}, Lcom/google/android/music/io/ChunkedInputStream;->read([BII)I

    move-result v0

    .line 60
    .local v0, "bytesReadThisChunk":I
    if-ne v0, v3, :cond_2

    .line 61
    if-nez v2, :cond_0

    move v2, v3

    .line 62
    goto :goto_0

    .line 66
    :cond_2
    add-int/2addr p2, v0

    .line 67
    add-int/2addr v2, v0

    .line 68
    sub-int/2addr p3, v0

    .line 69
    goto :goto_1

    .line 73
    .end local v0    # "bytesReadThisChunk":I
    :cond_3
    if-lez p3, :cond_0

    .line 74
    invoke-direct {p0}, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->fillBuffer()V

    .line 75
    iget v4, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    iget v5, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBufferEnd:I

    if-ne v4, v5, :cond_4

    .line 76
    if-nez v2, :cond_0

    move v2, v3

    .line 77
    goto :goto_0

    .line 80
    :cond_4
    iget v3, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBufferEnd:I

    iget v4, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    sub-int/2addr v3, v4

    invoke-static {p3, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 81
    iget-object v3, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBuffer:[B

    iget v4, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    invoke-static {v3, v4, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 82
    iget v3, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    .line 83
    add-int/2addr p2, v1

    .line 84
    add-int/2addr v2, v1

    .line 85
    sub-int/2addr p3, v1

    goto :goto_0
.end method

.method public skip(J)J
    .locals 17
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    const-wide/16 v10, 0x0

    .line 95
    .local v10, "totalSkipped":J
    const-wide/16 v14, 0x0

    cmp-long v14, p1, v14

    if-lez v14, :cond_1

    .line 98
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBufferEnd:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    sub-int/2addr v14, v15

    int-to-long v14, v14

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v14, v15}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 99
    .local v4, "bytesToSkip":J
    add-long/2addr v10, v4

    .line 100
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    int-to-long v14, v14

    add-long/2addr v14, v4

    long-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    .line 101
    sub-long p1, p1, v4

    .line 105
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mChunkSize:I

    int-to-long v14, v14

    div-long v8, p1, v14

    .line 106
    .local v8, "chunksToSkip":J
    const-wide/16 v14, 0x0

    cmp-long v14, v8, v14

    if-lez v14, :cond_0

    .line 107
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mChunkSize:I

    int-to-long v14, v14

    mul-long v4, v8, v14

    .line 108
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mInput:Lcom/google/android/music/io/ChunkedInputStream;

    invoke-interface {v14, v8, v9}, Lcom/google/android/music/io/ChunkedInputStream;->skipChunks(J)J

    move-result-wide v6

    .line 109
    .local v6, "chunksSkipped":J
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mChunkSize:I

    int-to-long v14, v14

    mul-long v2, v6, v14

    .line 110
    .local v2, "bytesSkipped":J
    add-long/2addr v10, v2

    .line 111
    sub-long p1, p1, v2

    .line 112
    cmp-long v14, v2, v4

    if-eqz v14, :cond_0

    move-wide v12, v10

    .line 126
    .end local v2    # "bytesSkipped":J
    .end local v4    # "bytesToSkip":J
    .end local v6    # "chunksSkipped":J
    .end local v8    # "chunksToSkip":J
    .end local v10    # "totalSkipped":J
    .local v12, "totalSkipped":J
    :goto_0
    return-wide v12

    .line 118
    .end local v12    # "totalSkipped":J
    .restart local v4    # "bytesToSkip":J
    .restart local v8    # "chunksToSkip":J
    .restart local v10    # "totalSkipped":J
    :cond_0
    const-wide/16 v14, 0x0

    cmp-long v14, p1, v14

    if-lez v14, :cond_1

    .line 119
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->fillBuffer()V

    .line 120
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mBufferEnd:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    sub-int/2addr v14, v15

    int-to-long v14, v14

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v14, v15}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 121
    add-long/2addr v10, v4

    .line 122
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    int-to-long v14, v14

    add-long/2addr v14, v4

    long-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->mPosition:I

    .line 123
    sub-long p1, p1, v4

    .end local v4    # "bytesToSkip":J
    .end local v8    # "chunksToSkip":J
    :cond_1
    move-wide v12, v10

    .line 126
    .end local v10    # "totalSkipped":J
    .restart local v12    # "totalSkipped":J
    goto :goto_0
.end method
