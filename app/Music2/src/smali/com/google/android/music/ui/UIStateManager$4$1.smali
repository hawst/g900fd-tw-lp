.class Lcom/google/android/music/ui/UIStateManager$4$1;
.super Ljava/lang/Object;
.source "UIStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/UIStateManager$4;->onNetworkChanged(ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/ui/UIStateManager$4;

.field final synthetic val$mobileConnected:Z

.field final synthetic val$wifiOrEthernetConnected:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/UIStateManager$4;ZZ)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/google/android/music/ui/UIStateManager$4$1;->this$1:Lcom/google/android/music/ui/UIStateManager$4;

    iput-boolean p2, p0, Lcom/google/android/music/ui/UIStateManager$4$1;->val$mobileConnected:Z

    iput-boolean p3, p0, Lcom/google/android/music/ui/UIStateManager$4$1;->val$wifiOrEthernetConnected:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$4$1;->this$1:Lcom/google/android/music/ui/UIStateManager$4;

    iget-object v0, v0, Lcom/google/android/music/ui/UIStateManager$4;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # getter for: Lcom/google/android/music/ui/UIStateManager;->mIsMobileConnected:Z
    invoke-static {v0}, Lcom/google/android/music/ui/UIStateManager;->access$700(Lcom/google/android/music/ui/UIStateManager;)Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/music/ui/UIStateManager$4$1;->val$mobileConnected:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$4$1;->this$1:Lcom/google/android/music/ui/UIStateManager$4;

    iget-object v0, v0, Lcom/google/android/music/ui/UIStateManager$4;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # getter for: Lcom/google/android/music/ui/UIStateManager;->mIsWifiOrEthernetConnected:Z
    invoke-static {v0}, Lcom/google/android/music/ui/UIStateManager;->access$800(Lcom/google/android/music/ui/UIStateManager;)Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/music/ui/UIStateManager$4$1;->val$wifiOrEthernetConnected:Z

    if-eq v0, v1, :cond_1

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$4$1;->this$1:Lcom/google/android/music/ui/UIStateManager$4;

    iget-object v0, v0, Lcom/google/android/music/ui/UIStateManager$4;->this$0:Lcom/google/android/music/ui/UIStateManager;

    iget-boolean v1, p0, Lcom/google/android/music/ui/UIStateManager$4$1;->val$mobileConnected:Z

    # setter for: Lcom/google/android/music/ui/UIStateManager;->mIsMobileConnected:Z
    invoke-static {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->access$702(Lcom/google/android/music/ui/UIStateManager;Z)Z

    .line 182
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$4$1;->this$1:Lcom/google/android/music/ui/UIStateManager$4;

    iget-object v0, v0, Lcom/google/android/music/ui/UIStateManager$4;->this$0:Lcom/google/android/music/ui/UIStateManager;

    iget-boolean v1, p0, Lcom/google/android/music/ui/UIStateManager$4$1;->val$wifiOrEthernetConnected:Z

    # setter for: Lcom/google/android/music/ui/UIStateManager;->mIsWifiOrEthernetConnected:Z
    invoke-static {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->access$802(Lcom/google/android/music/ui/UIStateManager;Z)Z

    .line 183
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$4$1;->this$1:Lcom/google/android/music/ui/UIStateManager$4;

    iget-object v0, v0, Lcom/google/android/music/ui/UIStateManager$4;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # invokes: Lcom/google/android/music/ui/UIStateManager;->notifyNetworkChanged()V
    invoke-static {v0}, Lcom/google/android/music/ui/UIStateManager;->access$900(Lcom/google/android/music/ui/UIStateManager;)V

    .line 185
    :cond_1
    return-void
.end method
