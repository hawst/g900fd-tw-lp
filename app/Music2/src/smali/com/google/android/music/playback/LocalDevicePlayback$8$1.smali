.class Lcom/google/android/music/playback/LocalDevicePlayback$8$1;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback$8;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback$8;)V
    .locals 0

    .prologue
    .line 914
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 917
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2900(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v2, v2, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->getAudioIdAndListItemId(I)Landroid/util/Pair;

    move-result-object v0

    .line 919
    .local v0, "audioIdAndListItemId":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/Long;>;"
    if-eqz v0, :cond_0

    .line 920
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v2, v1, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/music/download/ContentIdentifier;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->refreshCursor(Lcom/google/android/music/download/ContentIdentifier;)Z
    invoke-static {v2, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3000(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 921
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v2, v1, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J
    invoke-static {v2, v4, v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1102(Lcom/google/android/music/playback/LocalDevicePlayback;J)J

    .line 924
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/AsyncMediaPlayer;->start()V

    .line 925
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const-string v2, "com.android.music.metachanged"

    invoke-virtual {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V

    .line 926
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->updateDescriptorAndQueue(Z)V
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3100(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    .line 927
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->setNextTrack()V
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$700(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 928
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->logTrackStartEvent(Z)V
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3900(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    .line 930
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    sget-object v2, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->FEED:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->feedQueueIfNeeded(Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)Z
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4000(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)Z

    .line 931
    return-void
.end method
