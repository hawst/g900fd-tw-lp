.class public Lcom/google/android/music/dial/model/GenericDialEvent;
.super Lcom/google/api/client/json/GenericJson;
.source "GenericDialEvent.java"


# instance fields
.field public mBody:Lcom/google/api/client/json/GenericJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "body"
    .end annotation
.end field

.field public mGroupCoordinatorChangedEventJson:Lcom/google/android/music/dial/model/GroupCoordinatorChangedEventJson;

.field public mGroupVolumeEventJson:Lcom/google/android/music/dial/model/GroupVolumeEventJson;

.field public mHeader:Lcom/google/android/music/dial/model/EventHeaderJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "header"
    .end annotation
.end field

.field public mPlaybackErrorEventJson:Lcom/google/android/music/dial/model/PlaybackErrorEventJson;

.field public mPlaybackStatusEventJson:Lcom/google/android/music/dial/model/PlaybackStatusEventJson;

.field public mSessionErrorEventJson:Lcom/google/android/music/dial/model/SessionErrorEventJson;

.field public mSessionStatusEventJson:Lcom/google/android/music/dial/model/SessionStatusEventJson;

.field public mSubscribeResponseJson:Lcom/google/android/music/dial/model/SubscribeResponseJson;

.field public mSuccessResponseJson:Lcom/google/android/music/dial/model/SuccessResponseJson;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method


# virtual methods
.method public validate()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/dial/MalformedDialEventException;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/music/dial/model/GenericDialEvent;->mHeader:Lcom/google/android/music/dial/model/EventHeaderJson;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/dial/model/GenericDialEvent;->mHeader:Lcom/google/android/music/dial/model/EventHeaderJson;

    iget-object v0, v0, Lcom/google/android/music/dial/model/EventHeaderJson;->mNamespace:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/dial/model/GenericDialEvent;->mBody:Lcom/google/api/client/json/GenericJson;

    if-nez v0, :cond_1

    .line 39
    :cond_0
    new-instance v0, Lcom/google/android/music/dial/MalformedDialEventException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Event is missing required fields.  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/dial/MalformedDialEventException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_1
    return-void
.end method
