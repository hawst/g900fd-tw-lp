.class public Lcom/google/android/music/cloudclient/GetSituationsResponse;
.super Lcom/google/api/client/json/GenericJson;
.source "GetSituationsResponse.java"


# instance fields
.field public mPrimaryHeader:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "primaryHeader"
    .end annotation
.end field

.field public mSituations:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "situations"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/SituationJson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/cloudclient/GetSituationsResponse;->mSituations:Ljava/util/List;

    return-void
.end method
