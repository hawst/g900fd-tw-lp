.class public Lcom/google/android/music/ui/TrackContainerActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "TrackContainerActivity.java"


# instance fields
.field private mTransition:Lcom/google/android/music/ui/SharedElementTransition;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    return-void
.end method

.method public static final buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 286
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/music/ui/TrackContainerActivity;->buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static final buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 294
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/ui/TrackContainerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 299
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "data"

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 301
    const-string v1, "medialist"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 302
    if-eqz p2, :cond_0

    const-string v1, "document"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 303
    :cond_0
    return-object v0
.end method

.method private makeTrackContainerFragment(Landroid/content/Intent;)Lcom/google/android/music/ui/BaseTrackListFragment;
    .locals 26
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 93
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    .line 94
    .local v6, "action":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v25

    .line 95
    .local v25, "type":Ljava/lang/String;
    const-string v10, "highlightTrackSongId"

    const-wide/16 v12, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v22

    .line 97
    .local v22, "highlightTrackSongId":J
    const-string v10, "highlightTrackMetajamId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 100
    .local v20, "highlightTrackMetajamId":Ljava/lang/String;
    const-string v10, "android.intent.action.VIEW"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 101
    const/4 v7, 0x0

    .line 102
    .local v7, "list":Lcom/google/android/music/medialist/SongList;
    const-string v10, "vnd.android.cursor.dir/vnd.google.music.playlist"

    move-object/from16 v0, v25

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "vnd.android.cursor.dir/playlist"

    move-object/from16 v0, v25

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 106
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    const-string v12, "playlist"

    invoke-virtual {v10, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 107
    .local v24, "strPlaylistId":Ljava/lang/String;
    if-eqz v24, :cond_1

    .line 108
    invoke-static/range {v24 .. v24}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 112
    .local v8, "playlistId":J
    :goto_0
    invoke-static {v8, v9}, Lcom/google/android/music/store/MusicContent$AutoPlaylists;->isAutoPlaylistId(J)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 113
    const/4 v10, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/TrackContainerActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v12

    invoke-static {v8, v9, v10, v12}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;

    move-result-object v7

    .line 120
    :goto_1
    const/4 v10, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, v20

    invoke-static {v7, v10, v0, v1, v2}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->newInstance(Lcom/google/android/music/medialist/MediaList;Lcom/google/android/music/ui/cardlib/model/Document;JLjava/lang/String;)Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    move-result-object v19

    .line 134
    .end local v7    # "list":Lcom/google/android/music/medialist/SongList;
    .end local v8    # "playlistId":J
    .end local v24    # "strPlaylistId":Ljava/lang/String;
    :goto_2
    return-object v19

    .line 110
    .restart local v7    # "list":Lcom/google/android/music/medialist/SongList;
    .restart local v24    # "strPlaylistId":Ljava/lang/String;
    :cond_1
    const-string v10, "playlistId"

    const-wide/16 v12, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    .restart local v8    # "playlistId":J
    goto :goto_0

    .line 115
    :cond_2
    const/4 v11, 0x0

    .line 117
    .local v11, "playlistType":I
    new-instance v7, Lcom/google/android/music/medialist/PlaylistSongList;

    .end local v7    # "list":Lcom/google/android/music/medialist/SongList;
    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1

    invoke-direct/range {v7 .. v17}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .restart local v7    # "list":Lcom/google/android/music/medialist/SongList;
    goto :goto_1

    .line 123
    .end local v8    # "playlistId":J
    .end local v11    # "playlistType":I
    .end local v24    # "strPlaylistId":Ljava/lang/String;
    :cond_3
    const-string v10, "TrackContainerActivity"

    const-string v12, "ACTION_VIEW with unregistered type requested. Finishing early."

    invoke-static {v10, v12}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    const/16 v19, 0x0

    goto :goto_2

    .line 128
    .end local v7    # "list":Lcom/google/android/music/medialist/SongList;
    :cond_4
    const-string v10, "medialist"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v21

    check-cast v21, Lcom/google/android/music/medialist/MediaList;

    .line 129
    .local v21, "mediaList":Lcom/google/android/music/medialist/MediaList;
    const-string v10, "document"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v18

    check-cast v18, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 130
    .local v18, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    move-object/from16 v0, v21

    move-object/from16 v1, v18

    move-wide/from16 v2, v22

    move-object/from16 v4, v20

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->newInstance(Lcom/google/android/music/medialist/MediaList;Lcom/google/android/music/ui/cardlib/model/Document;JLjava/lang/String;)Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    move-result-object v19

    .local v19, "fragment":Lcom/google/android/music/ui/BaseTrackListFragment;
    goto :goto_2
.end method

.method public static final showAlbum(Landroid/content/Context;JLcom/google/android/music/ui/cardlib/model/Document;ZJLandroid/view/View;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J
    .param p3, "albumDoc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p4, "shouldFilter"    # Z
    .param p5, "highlightTrackId"    # J
    .param p7, "view"    # Landroid/view/View;

    .prologue
    .line 191
    if-eqz p3, :cond_0

    .line 192
    invoke-virtual {p3, p0, p4}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;Z)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    .line 197
    .local v1, "songlist":Lcom/google/android/music/medialist/SongList;
    :goto_0
    invoke-static {p0, v1, p3}, Lcom/google/android/music/ui/TrackContainerActivity;->buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/content/Intent;

    move-result-object v0

    .line 198
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "highlightTrackSongId"

    invoke-virtual {v0, v2, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 199
    invoke-static {p0, v0, p7}, Lcom/google/android/music/ui/SharedElementTransition;->startActivity(Landroid/content/Context;Landroid/content/Intent;Landroid/view/View;)V

    .line 200
    return-void

    .line 194
    .end local v0    # "i":Landroid/content/Intent;
    .end local v1    # "songlist":Lcom/google/android/music/medialist/SongList;
    :cond_0
    new-instance v1, Lcom/google/android/music/medialist/AlbumSongList;

    invoke-direct {v1, p1, p2, p4}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    .restart local v1    # "songlist":Lcom/google/android/music/medialist/SongList;
    goto :goto_0
.end method

.method public static final showAlbum(Landroid/content/Context;JLcom/google/android/music/ui/cardlib/model/Document;ZLandroid/view/View;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J
    .param p3, "albumDoc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p4, "shouldFilter"    # Z
    .param p5, "view"    # Landroid/view/View;

    .prologue
    .line 163
    if-eqz p3, :cond_0

    .line 164
    invoke-virtual {p3, p0, p4}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;Z)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    .line 169
    .local v1, "songlist":Lcom/google/android/music/medialist/SongList;
    :goto_0
    invoke-static {p0, v1, p3}, Lcom/google/android/music/ui/TrackContainerActivity;->buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/content/Intent;

    move-result-object v0

    .line 170
    .local v0, "i":Landroid/content/Intent;
    invoke-static {p0, v0, p5}, Lcom/google/android/music/ui/SharedElementTransition;->startActivity(Landroid/content/Context;Landroid/content/Intent;Landroid/view/View;)V

    .line 171
    return-void

    .line 166
    .end local v0    # "i":Landroid/content/Intent;
    .end local v1    # "songlist":Lcom/google/android/music/medialist/SongList;
    :cond_0
    new-instance v1, Lcom/google/android/music/medialist/AlbumSongList;

    invoke-direct {v1, p1, p2, p4}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    .restart local v1    # "songlist":Lcom/google/android/music/medialist/SongList;
    goto :goto_0
.end method

.method public static final showNautilusAlbum(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumNautilusId"    # Ljava/lang/String;
    .param p2, "albumDoc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 217
    if-eqz p2, :cond_0

    .line 218
    invoke-virtual {p2, p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    .line 223
    .local v1, "songlist":Lcom/google/android/music/medialist/SongList;
    :goto_0
    invoke-static {p0, v1, p2}, Lcom/google/android/music/ui/TrackContainerActivity;->buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/content/Intent;

    move-result-object v0

    .line 224
    .local v0, "i":Landroid/content/Intent;
    invoke-static {p0, v0, p3}, Lcom/google/android/music/ui/SharedElementTransition;->startActivity(Landroid/content/Context;Landroid/content/Intent;Landroid/view/View;)V

    .line 225
    return-void

    .line 220
    .end local v0    # "i":Landroid/content/Intent;
    .end local v1    # "songlist":Lcom/google/android/music/medialist/SongList;
    :cond_0
    new-instance v1, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    invoke-direct {v1, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;-><init>(Ljava/lang/String;)V

    .restart local v1    # "songlist":Lcom/google/android/music/medialist/SongList;
    goto :goto_0
.end method

.method public static final showNautilusAlbum(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;Landroid/view/View;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumNautilusId"    # Ljava/lang/String;
    .param p2, "albumDoc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p3, "highlightTrackNautilusId"    # Ljava/lang/String;
    .param p4, "view"    # Landroid/view/View;

    .prologue
    .line 244
    if-eqz p2, :cond_0

    .line 245
    invoke-virtual {p2, p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    .line 250
    .local v1, "songlist":Lcom/google/android/music/medialist/SongList;
    :goto_0
    invoke-static {p0, v1, p2}, Lcom/google/android/music/ui/TrackContainerActivity;->buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/content/Intent;

    move-result-object v0

    .line 251
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "highlightTrackMetajamId"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 252
    invoke-static {p0, v0, p4}, Lcom/google/android/music/ui/SharedElementTransition;->startActivity(Landroid/content/Context;Landroid/content/Intent;Landroid/view/View;)V

    .line 253
    return-void

    .line 247
    .end local v0    # "i":Landroid/content/Intent;
    .end local v1    # "songlist":Lcom/google/android/music/medialist/SongList;
    :cond_0
    new-instance v1, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    invoke-direct {v1, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;-><init>(Ljava/lang/String;)V

    .restart local v1    # "songlist":Lcom/google/android/music/medialist/SongList;
    goto :goto_0
.end method

.method public static final showPlaylist(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "list"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 262
    if-eqz p1, :cond_0

    .line 263
    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/google/android/music/ui/TrackContainerActivity;->buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/content/Intent;

    move-result-object v0

    .line 264
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 266
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public static final showPlaylist(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistDoc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 276
    if-eqz p1, :cond_0

    .line 277
    invoke-virtual {p1, p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    invoke-static {p0, v1, p1}, Lcom/google/android/music/ui/TrackContainerActivity;->buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/content/Intent;

    move-result-object v0

    .line 278
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 280
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const/4 v1, 0x0

    .line 54
    .local v1, "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v4

    if-nez v4, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 56
    .local v2, "intent":Landroid/content/Intent;
    invoke-direct {p0, v2}, Lcom/google/android/music/ui/TrackContainerActivity;->makeTrackContainerFragment(Landroid/content/Intent;)Lcom/google/android/music/ui/BaseTrackListFragment;

    move-result-object v1

    .line 57
    if-eqz v1, :cond_2

    .line 58
    const/4 v4, 0x0

    invoke-virtual {p0, v1, v4}, Lcom/google/android/music/ui/TrackContainerActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    .line 65
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 66
    .restart local v2    # "intent":Landroid/content/Intent;
    if-eqz v2, :cond_1

    .line 67
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 68
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 69
    const-string v4, "transitionInfo"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/ui/TransitionInfo;

    .line 72
    .local v3, "tInfo":Lcom/google/android/music/ui/TransitionInfo;
    if-eqz v3, :cond_1

    .line 73
    invoke-static {v3, p0}, Lcom/google/android/music/ui/SharedElementTransition;->createFromInfo(Lcom/google/android/music/ui/TransitionInfo;Lcom/google/android/music/ui/BaseActivity;)Lcom/google/android/music/ui/SharedElementTransition;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/ui/TrackContainerActivity;->mTransition:Lcom/google/android/music/ui/SharedElementTransition;

    .line 74
    iget-object v4, p0, Lcom/google/android/music/ui/TrackContainerActivity;->mTransition:Lcom/google/android/music/ui/SharedElementTransition;

    invoke-virtual {v4}, Lcom/google/android/music/ui/SharedElementTransition;->setup()V

    .line 78
    .end local v0    # "extras":Landroid/os/Bundle;
    .end local v3    # "tInfo":Lcom/google/android/music/ui/TransitionInfo;
    :cond_1
    return-void

    .line 60
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerActivity;->finish()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerActivity;->mTransition:Lcom/google/android/music/ui/SharedElementTransition;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerActivity;->mTransition:Lcom/google/android/music/ui/SharedElementTransition;

    invoke-virtual {v0}, Lcom/google/android/music/ui/SharedElementTransition;->destroy()V

    .line 88
    :cond_0
    invoke-super {p0}, Lcom/google/android/music/ui/BaseActivity;->onDestroy()V

    .line 89
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 139
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/TrackContainerActivity;->makeTrackContainerFragment(Landroid/content/Intent;)Lcom/google/android/music/ui/BaseTrackListFragment;

    move-result-object v0

    .line 140
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_0

    .line 141
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/TrackContainerActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    .line 145
    :goto_0
    return-void

    .line 143
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerActivity;->finish()V

    goto :goto_0
.end method

.method protected usesActionBarOverlay()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    return v0
.end method
