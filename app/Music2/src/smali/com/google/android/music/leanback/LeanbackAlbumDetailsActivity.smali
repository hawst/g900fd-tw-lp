.class public Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;
.super Lcom/google/android/music/leanback/LeanbackDetailsActivity;
.source "LeanbackAlbumDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;
    }
.end annotation


# static fields
.field private static final PROJECTION_ALBUMS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->TAG:Ljava/lang/String;

    .line 54
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "album_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "album_art"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "album_artist"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "album_year"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SongCount"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->PROJECTION_ALBUMS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackDetailsActivity;-><init>()V

    .line 287
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/support/v17/leanback/widget/Action;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # I
    .param p5, "x5"    # I

    .prologue
    .line 43
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->createPlayAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/support/v17/leanback/widget/Action;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Ljava/lang/String;Ljava/lang/String;I)Landroid/support/v17/leanback/widget/Action;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->createStartRadioAction(Ljava/lang/String;Ljava/lang/String;I)Landroid/support/v17/leanback/widget/Action;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Ljava/lang/String;Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/music/leanback/Item;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->createSongList(Ljava/lang/String;Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Ljava/lang/String;Ljava/lang/String;I)Landroid/support/v17/leanback/widget/Action;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->createGoToArtistAction(Ljava/lang/String;Ljava/lang/String;I)Landroid/support/v17/leanback/widget/Action;

    move-result-object v0

    return-object v0
.end method

.method private createAlbumDetails(Ljava/lang/String;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 4
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 126
    new-instance v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;-><init>(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Ljava/lang/String;)V

    .line 221
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    const/4 v1, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getAlbumDetailsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->PROJECTION_ALBUMS:[Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V

    .line 222
    return-object v0
.end method

.method private createGoToArtistAction(Ljava/lang/String;Ljava/lang/String;I)Landroid/support/v17/leanback/widget/Action;
    .locals 7
    .param p1, "artistId"    # Ljava/lang/String;
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "actionId"    # I

    .prologue
    .line 267
    new-instance v1, Lcom/google/android/music/leanback/IntentAction;

    int-to-long v2, p3

    const/4 v5, 0x0

    invoke-static {p0, p1}, Lcom/google/android/music/leanback/LeanbackUtils;->getArtistDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/leanback/IntentAction;-><init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    return-object v1
.end method

.method private createPlayAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/support/v17/leanback/widget/Action;
    .locals 7
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "actionSubName"    # Ljava/lang/String;
    .param p4, "playMode"    # I
    .param p5, "actionId"    # I

    .prologue
    .line 245
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getAlbumSongList(Ljava/lang/String;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    invoke-static {p0, v0, p4}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;I)Landroid/content/Intent;

    move-result-object v6

    .line 246
    .local v6, "intent":Landroid/content/Intent;
    new-instance v1, Lcom/google/android/music/leanback/IntentAction;

    int-to-long v2, p5

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/leanback/IntentAction;-><init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    return-object v1
.end method

.method private createSongList(Ljava/lang/String;Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 4
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "item"    # Lcom/google/android/music/leanback/Item;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getSongListForAlbum(Ljava/lang/String;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    .line 84
    .local v1, "songList":Lcom/google/android/music/medialist/SongList;
    new-instance v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$1;

    invoke-direct {v0, p0, v1, p2}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/leanback/Item;)V

    .line 105
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/music/xdi/TrackAdapter;->TRACK_COLUMNS:[Ljava/lang/String;

    invoke-virtual {p0, v2, v0, v1, v3}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 106
    return-object v0
.end method

.method private createStartRadioAction(Ljava/lang/String;Ljava/lang/String;I)Landroid/support/v17/leanback/widget/Action;
    .locals 7
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "albumName"    # Ljava/lang/String;
    .param p3, "actionId"    # I

    .prologue
    .line 257
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getAlbumSongList(Ljava/lang/String;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/music/utils/MusicUtils;->getSongListRadioMixDescriptor(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v0

    .line 259
    .local v0, "descriptor":Lcom/google/android/music/mix/MixDescriptor;
    invoke-static {p0, v0}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/mix/MixDescriptor;)Landroid/content/Intent;

    move-result-object v6

    .line 260
    .local v6, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0b023a

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 263
    .local v4, "actionTitle":Ljava/lang/String;
    :goto_0
    new-instance v1, Lcom/google/android/music/leanback/IntentAction;

    int-to-long v2, p3

    const v5, 0x7f0b02da

    invoke-virtual {p0, v5}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/leanback/IntentAction;-><init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    return-object v1

    .line 260
    .end local v4    # "actionTitle":Ljava/lang/String;
    :cond_0
    const v1, 0x7f0b023b

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private getAlbumDetailsUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 7
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 110
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v3

    .line 111
    .local v3, "isNautilusId":Z
    if-eqz v3, :cond_0

    .line 112
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Albums;->getNautilusAlbumsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 121
    :goto_0
    return-object v4

    .line 114
    :cond_0
    const-wide/16 v0, 0x0

    .line 116
    .local v0, "albumIdLong":J
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 121
    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v4

    goto :goto_0

    .line 117
    :catch_0
    move-exception v2

    .line 118
    .local v2, "e":Ljava/lang/NumberFormatException;
    sget-object v4, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error converting to long: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private getAlbumSongList(Ljava/lang/String;)Lcom/google/android/music/medialist/SongList;
    .locals 4
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 226
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    new-instance v0, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    invoke-direct {v0, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;-><init>(Ljava/lang/String;)V

    .line 229
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/music/medialist/AlbumSongList;

    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    goto :goto_0
.end method

.method private getSongListForAlbum(Ljava/lang/String;)Lcom/google/android/music/medialist/SongList;
    .locals 7
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 272
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v2

    .line 273
    .local v2, "isNautilusAlbum":Z
    if-eqz v2, :cond_0

    .line 274
    new-instance v3, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    invoke-direct {v3, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;-><init>(Ljava/lang/String;)V

    .line 283
    :goto_0
    return-object v3

    .line 278
    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 283
    .local v0, "albumIdLong":Ljava/lang/Long;
    new-instance v3, Lcom/google/android/music/medialist/AlbumSongList;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    goto :goto_0

    .line 279
    .end local v0    # "albumIdLong":Ljava/lang/Long;
    :catch_0
    move-exception v1

    .line 280
    .local v1, "e":Ljava/lang/NumberFormatException;
    sget-object v3, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error converting album ID to long: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected getAdapter(Landroid/support/v17/leanback/widget/PresenterSelector;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 8
    .param p1, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "album_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 77
    .local v7, "albumId":Ljava/lang/String;
    invoke-direct {p0, v7}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->createAlbumDetails(Ljava/lang/String;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v4

    .line 78
    .local v4, "detailsObjectAdapter":Landroid/support/v17/leanback/widget/ObjectAdapter;
    new-instance v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    new-instance v5, Lcom/google/android/music/leanback/SongTitleRow;

    const v1, 0x7f0b00a1

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/google/android/music/leanback/SongTitleRow;-><init>(Ljava/lang/String;)V

    new-instance v6, Lcom/google/android/music/leanback/SongFooterRow;

    invoke-direct {v6}, Lcom/google/android/music/leanback/SongFooterRow;-><init>()V

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;-><init>(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Landroid/content/Context;Landroid/support/v17/leanback/widget/PresenterSelector;Landroid/support/v17/leanback/widget/ObjectAdapter;Lcom/google/android/music/leanback/SongTitleRow;Lcom/google/android/music/leanback/SongFooterRow;)V

    return-object v0
.end method
