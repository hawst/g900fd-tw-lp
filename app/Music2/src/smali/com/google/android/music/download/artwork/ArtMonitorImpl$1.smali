.class Lcom/google/android/music/download/artwork/ArtMonitorImpl$1;
.super Lcom/google/android/music/utils/SafeServiceConnection;
.source "ArtMonitorImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/artwork/ArtMonitorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/artwork/ArtMonitorImpl;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl$1;->this$0:Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    invoke-direct {p0}, Lcom/google/android/music/utils/SafeServiceConnection;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 66
    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl$1;->this$0:Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    # getter for: Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->access$000(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 67
    :try_start_0
    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    .line 68
    .local v1, "outgoingMessenger":Landroid/os/Messenger;
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 69
    .local v0, "message":Landroid/os/Message;
    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl$1;->this$0:Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    # getter for: Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mIncomingMessenger:Landroid/os/Messenger;
    invoke-static {v2}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->access$100(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)Landroid/os/Messenger;

    move-result-object v2

    iput-object v2, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 72
    # getter for: Lcom/google/android/music/download/artwork/ArtMonitorImpl;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->access$200()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    const-string v2, "ArtMonitor"

    const-string v4, "ArtMonitor finished binding"

    invoke-static {v2, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v3

    .line 79
    return-void

    .line 78
    .end local v0    # "message":Landroid/os/Message;
    .end local v1    # "outgoingMessenger":Landroid/os/Messenger;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 75
    .restart local v0    # "message":Landroid/os/Message;
    .restart local v1    # "outgoingMessenger":Landroid/os/Messenger;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 83
    # getter for: Lcom/google/android/music/download/artwork/ArtMonitorImpl;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->access$200()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    const-string v0, "ArtMonitor"

    const-string v1, "ArtMonitor finished unbinding"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :cond_0
    return-void
.end method
