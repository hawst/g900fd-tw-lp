.class public Lcom/google/android/music/download/stream/PrefetchedContentCache;
.super Ljava/lang/Object;
.source "PrefetchedContentCache.java"


# instance fields
.field private final mContentMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/download/TrackDownloadRequest;",
            "Lcom/google/android/music/download/stream/StreamingContent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/stream/PrefetchedContentCache;->mContentMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public findContent(Lcom/google/android/music/download/TrackDownloadRequest;)Lcom/google/android/music/download/stream/StreamingContent;
    .locals 1
    .param p1, "req"    # Lcom/google/android/music/download/TrackDownloadRequest;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/music/download/stream/PrefetchedContentCache;->mContentMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/stream/StreamingContent;

    return-object v0
.end method

.method public pruneNotMatching([Lcom/google/android/music/download/ContentIdentifier;)Ljava/util/List;
    .locals 7
    .param p1, "ids"    # [Lcom/google/android/music/download/ContentIdentifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/music/download/ContentIdentifier;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/stream/StreamingContent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 42
    .local v3, "pruned":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/stream/StreamingContent;>;"
    if-nez p1, :cond_1

    .line 43
    iget-object v5, p0, Lcom/google/android/music/download/stream/PrefetchedContentCache;->mContentMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 44
    iget-object v5, p0, Lcom/google/android/music/download/stream/PrefetchedContentCache;->mContentMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    .line 57
    :cond_0
    return-object v3

    .line 46
    :cond_1
    new-instance v1, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 48
    .local v1, "filterSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/music/download/ContentIdentifier;>;"
    iget-object v5, p0, Lcom/google/android/music/download/stream/PrefetchedContentCache;->mContentMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackDownloadRequest;

    .line 49
    .local v4, "req":Lcom/google/android/music/download/TrackDownloadRequest;
    invoke-virtual {v4}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 50
    iget-object v5, p0, Lcom/google/android/music/download/stream/PrefetchedContentCache;->mContentMap:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 53
    .end local v4    # "req":Lcom/google/android/music/download/TrackDownloadRequest;
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/stream/StreamingContent;

    .line 54
    .local v0, "content":Lcom/google/android/music/download/stream/StreamingContent;
    iget-object v5, p0, Lcom/google/android/music/download/stream/PrefetchedContentCache;->mContentMap:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingContent;->getDownloadRequest()Lcom/google/android/music/download/TrackDownloadRequest;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public shouldFilter(Ljava/lang/String;)Z
    .locals 3
    .param p1, "fullFilePath"    # Ljava/lang/String;

    .prologue
    .line 75
    iget-object v2, p0, Lcom/google/android/music/download/stream/PrefetchedContentCache;->mContentMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/stream/StreamingContent;

    .line 76
    .local v0, "content":Lcom/google/android/music/download/stream/StreamingContent;
    invoke-virtual {v0, p1}, Lcom/google/android/music/download/stream/StreamingContent;->shouldFilter(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 77
    const/4 v2, 0x1

    .line 80
    .end local v0    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/music/download/stream/PrefetchedContentCache;->mContentMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public store(Lcom/google/android/music/download/stream/StreamingContent;)V
    .locals 2
    .param p1, "content"    # Lcom/google/android/music/download/stream/StreamingContent;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/music/download/stream/PrefetchedContentCache;->mContentMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/music/download/stream/StreamingContent;->getDownloadRequest()Lcom/google/android/music/download/TrackDownloadRequest;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    return-void
.end method
