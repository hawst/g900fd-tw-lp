.class Lcom/google/android/music/ui/NowPlayingScreenFragment$11;
.super Ljava/lang/Object;
.source "NowPlayingScreenFragment.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingScreenFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0

    .prologue
    .line 1096
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$11;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "classname"    # Landroid/content/ComponentName;
    .param p2, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 1098
    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->DEBUG_QUEUE:Z
    invoke-static {}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1800()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1099
    const-string v2, "NowPlayingFragment"

    const-string v3, "Service connected"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1102
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$11;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-static {p2}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v3

    # setter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v2, v3}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1702(Lcom/google/android/music/ui/NowPlayingScreenFragment;Lcom/google/android/music/playback/IMusicPlaybackService;)Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 1103
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$11;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->refreshPlaybackControls()V
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    .line 1104
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$11;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateTrackInfo()V
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2000(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    .line 1105
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$11;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->refreshNow()J
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2100(Lcom/google/android/music/ui/NowPlayingScreenFragment;)J

    move-result-wide v0

    .line 1106
    .local v0, "next":J
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$11;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->queueNextRefresh(J)V
    invoke-static {v2, v0, v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2200(Lcom/google/android/music/ui/NowPlayingScreenFragment;J)V

    .line 1108
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$11;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->setupPlayQueue()V
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2300(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    .line 1109
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "classname"    # Landroid/content/ComponentName;

    .prologue
    .line 1111
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$11;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1702(Lcom/google/android/music/ui/NowPlayingScreenFragment;Lcom/google/android/music/playback/IMusicPlaybackService;)Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 1112
    return-void
.end method
