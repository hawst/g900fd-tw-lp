.class public Lcom/google/android/music/ui/QueuePlayingFromHeaderView;
.super Landroid/widget/RelativeLayout;
.source "QueuePlayingFromHeaderView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/music/art/ArtRequest$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/QueuePlayingFromHeaderView$7;,
        Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueuePlayingFromHeaderListener;,
        Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/RelativeLayout;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/music/art/ArtRequest$Listener",
        "<",
        "Lcom/google/android/music/art/ContainerMixDescriptorPair;",
        ">;"
    }
.end annotation


# static fields
.field private static final ALBUM_COLUMNS:[Ljava/lang/String;

.field private static final ARTIST_SHUFFLE_COLUMNS:[Ljava/lang/String;

.field private static final LOGV:Z

.field private static final RADIO_COLUMNS:[Ljava/lang/String;


# instance fields
.field private mBackground:Landroid/graphics/Bitmap;

.field private mBackgroundNeedsUpdate:Z

.field private mBackgroundView:Landroid/widget/ImageView;

.field private mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field private mContainerInfoView:Landroid/widget/TextView;

.field private mGradient:Landroid/graphics/drawable/ShapeDrawable;

.field private mIsAttached:Z

.field private mIsRadio:Z

.field private mListener:Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueuePlayingFromHeaderListener;

.field private mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

.field private mOverlayView:Landroid/view/View;

.field private mPlayingFromBoldText:Ljava/lang/String;

.field private mPlayingFromDescription:Ljava/lang/String;

.field private mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private mRefreshButton:Landroid/widget/ImageView;

.field private mShade:Landroid/graphics/drawable/Drawable;

.field private mShowingDefault:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 79
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ALBUM_ART:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    .line 84
    new-array v0, v2, [Ljava/lang/String;

    const-string v3, "radio_name"

    aput-object v3, v0, v1

    sput-object v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->RADIO_COLUMNS:[Ljava/lang/String;

    .line 87
    new-array v0, v2, [Ljava/lang/String;

    const-string v3, "album_artist"

    aput-object v3, v0, v1

    sput-object v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->ALBUM_COLUMNS:[Ljava/lang/String;

    .line 90
    new-array v0, v2, [Ljava/lang/String;

    const-string v2, "artist"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->ARTIST_SHUFFLE_COLUMNS:[Ljava/lang/String;

    return-void

    :cond_1
    move v0, v1

    .line 79
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 134
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 107
    iput-boolean v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mShowingDefault:Z

    .line 108
    iput-boolean v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackgroundNeedsUpdate:Z

    .line 109
    iput-boolean v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mIsAttached:Z

    .line 111
    new-instance v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$1;-><init>(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;)V

    iput-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 135
    invoke-direct {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->init()V

    .line 136
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 129
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 107
    iput-boolean v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mShowingDefault:Z

    .line 108
    iput-boolean v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackgroundNeedsUpdate:Z

    .line 109
    iput-boolean v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mIsAttached:Z

    .line 111
    new-instance v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$1;-><init>(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;)V

    iput-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 130
    invoke-direct {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->init()V

    .line 131
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateRefreshButtonVisibility()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackground:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 76
    sget-boolean v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    return-object v0
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->ALBUM_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->RADIO_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getRadioTitleText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->ARTIST_SHUFFLE_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mIsAttached:Z

    return v0
.end method

.method private doAlbumArtUpdate(Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 334
    .local v0, "appContext":Landroid/content/Context;
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setArtImageOrDefault(Lcom/google/android/music/art/ArtRequest;)V

    .line 336
    new-instance v1, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;-><init>(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;Lcom/google/android/music/art/ContainerArtRequest;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 366
    return-void
.end method

.method private doArtistShuffleUpdate(Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 414
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 415
    .local v0, "appContext":Landroid/content/Context;
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setArtImageOrDefault(Lcom/google/android/music/art/ArtRequest;)V

    .line 416
    new-instance v1, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$5;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$5;-><init>(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;Lcom/google/android/music/art/ContainerArtRequest;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 446
    return-void
.end method

.method private doDefaultArtUpdate(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "boldText"    # Ljava/lang/String;
    .param p2, "normalText"    # Ljava/lang/String;

    .prologue
    .line 449
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x65

    invoke-static {v1, v2}, Lcom/google/android/music/art/StaticArt;->getArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 451
    .local v0, "bmp":Landroid/graphics/Bitmap;
    invoke-direct {p0, v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setArtImageOrDefault(Landroid/graphics/Bitmap;)V

    .line 452
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateBackground()V

    .line 454
    return-void
.end method

.method private doIFLUpdate()V
    .locals 6

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b02ed

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 371
    .local v0, "n":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0x66

    invoke-static {v3, v4}, Lcom/google/android/music/art/StaticArt;->getArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 373
    .local v1, "staticImg":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getHeight()I

    move-result v5

    invoke-static {v3, v1, v4, v5}, Lcom/google/android/music/art/QueueArtRenderer;->tileDefaultArtBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 375
    .local v2, "tiledBmp":Landroid/graphics/Bitmap;
    invoke-direct {p0, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setArtImageOrDefault(Landroid/graphics/Bitmap;)V

    .line 376
    invoke-direct {p0, v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getRadioTitleText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateBackground()V

    .line 378
    return-void
.end method

.method private doLocalRadioArtUpdate(Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 384
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 385
    .local v0, "appContext":Landroid/content/Context;
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setArtImageOrDefault(Lcom/google/android/music/art/ArtRequest;)V

    .line 386
    new-instance v1, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$4;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$4;-><init>(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;Lcom/google/android/music/art/ContainerArtRequest;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 411
    return-void
.end method

.method private getRadioTitleText(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "stationName"    # Ljava/lang/String;

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 315
    .local v2, "res":Landroid/content/res/Resources;
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 316
    .local v1, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v0

    .line 317
    .local v0, "nautilus":Z
    if-eqz v0, :cond_0

    const v3, 0x7f0b02eb

    :goto_0
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    return-object v3

    :cond_0
    const v3, 0x7f0b02ec

    goto :goto_0
.end method

.method private getSearchTitleText(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 324
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0b02f0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private handleHeaderClick()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 602
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    if-nez v1, :cond_0

    .line 603
    const-string v1, "QueueHeaderView"

    const-string v2, "Queue header tapped while container descriptor is null, bailing"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    :goto_0
    return-void

    .line 606
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v3

    .line 607
    .local v3, "type":Lcom/google/android/music/store/ContainerDescriptor$Type;
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    move-result-wide v4

    .line 608
    .local v4, "id":J
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v6

    .line 609
    .local v6, "extId":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v8

    .line 610
    .local v8, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 612
    .local v7, "context":Landroid/content/Context;
    sget-object v1, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$7;->$SwitchMap$com$google$android$music$store$ContainerDescriptor$Type:[I

    invoke-virtual {v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 620
    :pswitch_1
    new-instance v1, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$6;-><init>(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto :goto_0

    .line 653
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4, v5, v8, v9}, Lcom/google/android/music/ui/ArtistPageActivity;->showArtist(Landroid/content/Context;JLjava/lang/String;Z)V

    goto :goto_0

    .line 658
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v6, v8}, Lcom/google/android/music/ui/ArtistPageActivity;->showNautilusArtist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 661
    :pswitch_4
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/music/ui/SearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 662
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "android.intent.action.SEARCH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 663
    const-string v1, "query"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 664
    const-string v1, "createNewRadioStation"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 665
    const-string v1, "surpressKeyboard"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 666
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 667
    invoke-virtual {v7, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 612
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private init()V
    .locals 14

    .prologue
    const/4 v12, 0x1

    .line 139
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 140
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v11, 0x7f0400c9

    invoke-virtual {v2, v11, p0, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 142
    const v11, 0x7f0e0240

    invoke-virtual {p0, v11}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerInfoView:Landroid/widget/TextView;

    .line 143
    const v11, 0x7f0e023f

    invoke-virtual {p0, v11}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 144
    .local v4, "playingFromTextHeader":Landroid/widget/TextView;
    invoke-static {v4, v12}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 145
    const v11, 0x7f0e023e

    invoke-virtual {p0, v11}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    iput-object v11, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mRefreshButton:Landroid/widget/ImageView;

    .line 146
    invoke-direct {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->initRefreshButton()V

    .line 148
    const v11, 0x7f0e00b5

    invoke-virtual {p0, v11}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    iput-object v11, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackgroundView:Landroid/widget/ImageView;

    .line 149
    const v11, 0x7f0e023d

    invoke-virtual {p0, v11}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mOverlayView:Landroid/view/View;

    .line 151
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 152
    .local v5, "res":Landroid/content/res/Resources;
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0c00a8

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 153
    .local v9, "transparentColor":I
    const/high16 v0, -0x1000000

    .line 154
    .local v0, "blackColor":I
    new-instance v11, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v11, v9}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v11, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mShade:Landroid/graphics/drawable/Drawable;

    .line 155
    new-instance v11, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v12, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v12}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {v11, v12}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    iput-object v11, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mGradient:Landroid/graphics/drawable/ShapeDrawable;

    .line 164
    new-instance v7, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$2;

    invoke-direct {v7, p0, v9}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$2;-><init>(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;I)V

    .line 181
    .local v7, "sf":Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;
    iget-object v11, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mGradient:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v11, v7}, Landroid/graphics/drawable/ShapeDrawable;->setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V

    .line 182
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v11

    const-string v12, "window"

    invoke-virtual {v11, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/WindowManager;

    .line 184
    .local v10, "wm":Landroid/view/WindowManager;
    invoke-interface {v10}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 185
    .local v1, "display":Landroid/view/Display;
    invoke-static {v1}, Lcom/google/android/music/utils/ViewUtils;->getScreenSortedDimensions(Landroid/view/Display;)Landroid/util/Pair;

    move-result-object v8

    .line 186
    .local v8, "sizes":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v11, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iget-object v11, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    sub-int v3, v12, v11

    .line 187
    .local v3, "left":I
    iget-object v11, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    add-int v6, v3, v11

    .line 188
    .local v6, "right":I
    iget-object v11, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mGradient:Landroid/graphics/drawable/ShapeDrawable;

    const/4 v12, 0x0

    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getHeight()I

    move-result v13

    invoke-virtual {v11, v3, v12, v6, v13}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 189
    const/high16 v11, -0x1000000

    invoke-virtual {p0, v11}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setBackgroundColor(I)V

    .line 190
    invoke-virtual {p0, p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    return-void
.end method

.method private initRefreshButton()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 194
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 195
    .local v4, "res":Landroid/content/res/Resources;
    new-instance v5, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v5}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 196
    .local v5, "stateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    const v8, 0x7f020071

    invoke-static {v4, v8}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 198
    .local v1, "fullOpacity":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 200
    .local v7, "transparent":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 201
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 202
    .local v3, "p":Landroid/graphics/Paint;
    const/16 v8, 0xcc

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 203
    invoke-virtual {v0, v1, v11, v11, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 204
    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v6, v4, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 205
    .local v6, "touchedState":Landroid/graphics/drawable/BitmapDrawable;
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, v4, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 206
    .local v2, "otherStates":Landroid/graphics/drawable/BitmapDrawable;
    const/4 v8, 0x1

    new-array v8, v8, [I

    const/4 v9, 0x0

    const v10, 0x10100a7

    aput v10, v8, v9

    invoke-virtual {v5, v8, v6}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 207
    sget-object v8, Landroid/util/StateSet;->WILD_CARD:[I

    invoke-virtual {v5, v8, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 208
    iget-object v8, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mRefreshButton:Landroid/widget/ImageView;

    invoke-virtual {v8, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 209
    iget-object v8, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mRefreshButton:Landroid/widget/ImageView;

    invoke-virtual {v8, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    invoke-direct {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateRefreshButtonVisibility()V

    .line 211
    return-void
.end method

.method private setArtImageOrDefault(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "artImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 482
    if-eqz p1, :cond_0

    .line 483
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackground:Landroid/graphics/Bitmap;

    .line 489
    :goto_0
    return-void

    .line 486
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x65

    invoke-static {v0, v1}, Lcom/google/android/music/art/StaticArt;->getArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackground:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private setArtImageOrDefault(Lcom/google/android/music/art/ArtRequest;)V
    .locals 9
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest;

    .prologue
    .line 462
    sget-boolean v6, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    if-eqz v6, :cond_0

    .line 463
    const-string v6, "QueueHeaderView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setArtImageOrDefault request="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "window"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 467
    .local v5, "wm":Landroid/view/WindowManager;
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 468
    .local v0, "display":Landroid/view/Display;
    invoke-static {v0}, Lcom/google/android/music/utils/ViewUtils;->getScreenSortedDimensions(Landroid/view/Display;)Landroid/util/Pair;

    move-result-object v3

    .line 469
    .local v3, "sizes":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 471
    .local v1, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    iget-object v6, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 472
    .local v4, "width":I
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isXLargeScreen()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 474
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getWidth()I

    move-result v4

    .line 476
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p1, Lcom/google/android/music/art/ArtRequest;->mImages:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getHeight()I

    move-result v8

    invoke-static {v6, v7, v4, v8}, Lcom/google/android/music/art/QueueArtRenderer;->makeArtForBitmaps(Landroid/content/Context;Ljava/util/List;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 478
    .local v2, "result":Landroid/graphics/Bitmap;
    invoke-direct {p0, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setArtImageOrDefault(Landroid/graphics/Bitmap;)V

    .line 479
    return-void
.end method

.method private setIsRadio(Z)V
    .locals 9
    .param p1, "isRadio"    # Z

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 492
    if-eqz p1, :cond_2

    .line 493
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    .line 494
    .local v3, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    const/16 v5, 0x67

    :goto_0
    invoke-static {v6, v5}, Lcom/google/android/music/art/StaticArt;->getArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 496
    .local v2, "img":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 497
    .local v0, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    iget-object v5, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerInfoView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getLineHeight()I

    move-result v1

    .line 498
    .local v1, "height":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    int-to-float v6, v1

    mul-float/2addr v5, v6

    float-to-int v4, v5

    .line 500
    .local v4, "width":I
    invoke-virtual {v0, v7, v7, v4, v1}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 501
    iget-object v5, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerInfoView:Landroid/widget/TextView;

    invoke-virtual {v5, v0, v8, v8, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 502
    sget-boolean v5, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    if-eqz v5, :cond_0

    .line 503
    const-string v5, "QueueHeaderView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setIsRadio: Radio mode, height="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", width="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    .end local v0    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v1    # "height":I
    .end local v2    # "img":Landroid/graphics/Bitmap;
    .end local v3    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v4    # "width":I
    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mRefreshButton:Landroid/widget/ImageView;

    invoke-virtual {v5, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 509
    iput-boolean p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mIsRadio:Z

    .line 510
    invoke-direct {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateRefreshButtonVisibility()V

    .line 511
    return-void

    .line 494
    .restart local v3    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_1
    const/16 v5, 0x68

    goto :goto_0

    .line 506
    .end local v3    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_2
    iget-object v5, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerInfoView:Landroid/widget/TextView;

    invoke-virtual {v5, v7, v7, v7, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1
.end method

.method private setupClickHandling()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 273
    sget-object v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$7;->$SwitchMap$com$google$android$music$store$ContainerDescriptor$Type:[I

    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 305
    :pswitch_0
    sget-boolean v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    if-eqz v0, :cond_0

    .line 306
    const-string v0, "QueueHeaderView"

    const-string v1, "Disabling click handling"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setClickable(Z)V

    .line 310
    :goto_0
    return-void

    .line 278
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    if-eqz v0, :cond_2

    .line 279
    sget-boolean v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    if-eqz v0, :cond_1

    .line 280
    const-string v0, "QueueHeaderView"

    const-string v1, "Disabling click handling"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setClickable(Z)V

    goto :goto_0

    .line 297
    :cond_2
    :pswitch_2
    sget-boolean v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    if-eqz v0, :cond_3

    .line 298
    const-string v0, "QueueHeaderView"

    const-string v1, "Enabling click handling"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setClickable(Z)V

    goto :goto_0

    .line 273
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private updateHeaderViews(Lcom/google/android/music/art/ContainerArtRequest;Z)V
    .locals 9
    .param p1, "request"    # Lcom/google/android/music/art/ContainerArtRequest;
    .param p2, "removeListener"    # Z

    .prologue
    const v8, 0x7f0b02ef

    const/4 v7, 0x0

    .line 718
    sget-boolean v4, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    if-eqz v4, :cond_0

    .line 719
    const-string v4, "QueueHeaderView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateHeaderViews request="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", removeListener="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    :cond_0
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertMainThread()V

    .line 724
    const/4 v0, 0x0

    .line 725
    .local v0, "boldText":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 726
    .local v3, "res":Landroid/content/res/Resources;
    invoke-virtual {p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v1

    .line 727
    .local v1, "container":Lcom/google/android/music/store/ContainerDescriptor;
    sget-object v4, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$7;->$SwitchMap$com$google$android$music$store$ContainerDescriptor$Type:[I

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/store/ContainerDescriptor$Type;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 791
    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v7}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->doDefaultArtUpdate(Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p1, p0}, Lcom/google/android/music/art/ContainerArtRequest;->removeListener(Lcom/google/android/music/art/ArtRequest$Listener;)V

    .line 794
    :cond_1
    return-void

    .line 730
    :pswitch_0
    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 731
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setArtImageOrDefault(Lcom/google/android/music/art/ArtRequest;)V

    .line 732
    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v4}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateBackground()V

    goto :goto_0

    .line 736
    :pswitch_1
    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 737
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setArtImageOrDefault(Lcom/google/android/music/art/ArtRequest;)V

    .line 738
    invoke-direct {p0, v0, v7}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateBackground()V

    goto :goto_0

    .line 743
    :pswitch_2
    const v4, 0x7f0b02ee

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 744
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setArtImageOrDefault(Lcom/google/android/music/art/ArtRequest;)V

    .line 745
    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v4}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateBackground()V

    goto :goto_0

    .line 749
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setArtImageOrDefault(Lcom/google/android/music/art/ArtRequest;)V

    .line 750
    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getSearchTitleText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v7}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateBackground()V

    goto :goto_0

    .line 755
    :pswitch_4
    const v4, 0x7f0b02f1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 756
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setArtImageOrDefault(Lcom/google/android/music/art/ArtRequest;)V

    .line 757
    invoke-direct {p0, v0, v7}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateBackground()V

    goto :goto_0

    .line 761
    :pswitch_5
    const v4, 0x7f0b00bc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 762
    const v4, 0x7f0b02f2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 763
    .local v2, "normalText":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setArtImageOrDefault(Lcom/google/android/music/art/ArtRequest;)V

    .line 764
    invoke-direct {p0, v0, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateBackground()V

    goto :goto_0

    .line 773
    .end local v2    # "normalText":Ljava/lang/String;
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setArtImageOrDefault(Lcom/google/android/music/art/ArtRequest;)V

    .line 774
    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v7}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateBackground()V

    goto/16 :goto_0

    .line 778
    :pswitch_7
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->doArtistShuffleUpdate(Lcom/google/android/music/art/ContainerArtRequest;)V

    goto/16 :goto_0

    .line 782
    :pswitch_8
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->doAlbumArtUpdate(Lcom/google/android/music/art/ContainerArtRequest;)V

    goto/16 :goto_0

    .line 785
    :pswitch_9
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->doLocalRadioArtUpdate(Lcom/google/android/music/art/ContainerArtRequest;)V

    goto/16 :goto_0

    .line 788
    :pswitch_a
    invoke-direct {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->doIFLUpdate()V

    goto/16 :goto_0

    .line 727
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_a
        :pswitch_6
        :pswitch_9
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_8
        :pswitch_8
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private updateInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "boldText"    # Ljava/lang/String;
    .param p2, "normalText"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x20

    const/16 v7, 0x12

    .line 520
    if-nez p1, :cond_0

    .line 521
    iget-object v5, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerInfoView:Landroid/widget/TextView;

    const v6, 0x7f0b00c5

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 545
    :goto_0
    return-void

    .line 524
    :cond_0
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 525
    .local v1, "builder":Landroid/text/SpannableStringBuilder;
    invoke-virtual {v1, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 526
    if-eqz p2, :cond_1

    .line 527
    invoke-virtual {v1, v8}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 528
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b00d9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 529
    invoke-virtual {v1, v8}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 530
    invoke-virtual {v1, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 532
    :cond_1
    invoke-static {}, Lcom/google/android/music/utils/TypefaceUtil;->getRobotoLightTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    .line 533
    .local v4, "robotoLightTypeface":Landroid/graphics/Typeface;
    invoke-static {}, Lcom/google/android/music/utils/TypefaceUtil;->getRobotoBoldTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    .line 535
    .local v3, "robotoBoldTypeface":Landroid/graphics/Typeface;
    if-eqz v3, :cond_2

    if-eqz v4, :cond_2

    .line 536
    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-virtual {v3}, Landroid/graphics/Typeface;->getStyle()I

    move-result v5

    invoke-direct {v0, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 537
    .local v0, "boldSpan":Landroid/text/style/StyleSpan;
    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v1, v0, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 538
    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-virtual {v4}, Landroid/graphics/Typeface;->getStyle()I

    move-result v5

    invoke-direct {v2, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 539
    .local v2, "regularSpan":Landroid/text/style/StyleSpan;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    invoke-virtual {v1, v2, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 542
    .end local v0    # "boldSpan":Landroid/text/style/StyleSpan;
    .end local v2    # "regularSpan":Landroid/text/style/StyleSpan;
    :cond_2
    iget-object v5, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerInfoView:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 543
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mPlayingFromBoldText:Ljava/lang/String;

    .line 544
    iput-object p2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mPlayingFromDescription:Ljava/lang/String;

    goto :goto_0
.end method

.method private updateRefreshButtonVisibility()V
    .locals 4

    .prologue
    .line 576
    iget-boolean v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mIsRadio:Z

    .line 577
    .local v0, "isVisible":Z
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 578
    .local v1, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 579
    const/4 v0, 0x0

    .line 581
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mRefreshButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 582
    return-void

    .line 581
    :cond_1
    const/16 v2, 0x8

    goto :goto_0
.end method


# virtual methods
.method public lockQueueHeader()V
    .locals 3

    .prologue
    .line 682
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f05001b

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 684
    .local v0, "rotation":Landroid/view/animation/Animation;
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 685
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mRefreshButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 686
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mRefreshButton:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 687
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    .line 813
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 814
    sget-boolean v1, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    if-eqz v1, :cond_0

    .line 815
    const-string v1, "QueueHeaderView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onAttachedToWindow mBackgroundNeedsUpdate="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackgroundNeedsUpdate:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mIsAttached:Z

    .line 819
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 820
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 823
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    iget-object v2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->update(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/store/ContainerDescriptor;)V

    .line 824
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 590
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mListener:Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueuePlayingFromHeaderListener;

    if-nez v0, :cond_0

    .line 599
    :goto_0
    return-void

    .line 591
    :cond_0
    if-ne p1, p0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    if-eqz v0, :cond_1

    .line 592
    invoke-direct {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->handleHeaderClick()V

    goto :goto_0

    .line 594
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 596
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mListener:Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueuePlayingFromHeaderListener;

    invoke-interface {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueuePlayingFromHeaderListener;->onRefreshClicked()V

    goto :goto_0

    .line 594
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e023e
        :pswitch_0
    .end packed-switch
.end method

.method protected onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 828
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 829
    sget-boolean v1, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    if-eqz v1, :cond_0

    .line 830
    const-string v1, "QueueHeaderView"

    const-string v2, "onDetachedFromWindow"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mIsAttached:Z

    .line 833
    iput-object v3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackground:Landroid/graphics/Bitmap;

    .line 834
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackgroundView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 835
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackgroundNeedsUpdate:Z

    .line 836
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 837
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 838
    return-void
.end method

.method public onLoadError(Lcom/google/android/music/art/ArtRequest;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest",
            "<",
            "Lcom/google/android/music/art/ContainerMixDescriptorPair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 801
    .local p1, "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<Lcom/google/android/music/art/ContainerMixDescriptorPair;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->onLoadFinished(Lcom/google/android/music/art/ArtRequest;)V

    .line 802
    return-void
.end method

.method public onLoadFinished(Lcom/google/android/music/art/ArtRequest;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest",
            "<",
            "Lcom/google/android/music/art/ContainerMixDescriptorPair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 700
    .local p1, "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<Lcom/google/android/music/art/ContainerMixDescriptorPair;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mShowingDefault:Z

    .line 701
    check-cast p1, Lcom/google/android/music/art/ContainerArtRequest;

    .end local p1    # "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<Lcom/google/android/music/art/ContainerMixDescriptorPair;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateHeaderViews(Lcom/google/android/music/art/ContainerArtRequest;Z)V

    .line 702
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 859
    sget-boolean v1, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    if-eqz v1, :cond_0

    .line 860
    const-string v1, "QueueHeaderView"

    const-string v2, "onRestoreInstanceState"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 862
    :cond_0
    instance-of v1, p1, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;

    if-eqz v1, :cond_2

    move-object v0, p1

    .line 863
    check-cast v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;

    .line 864
    .local v0, "ss":Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;
    invoke-virtual {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 865
    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mBackground:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->access$1000(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackground:Landroid/graphics/Bitmap;

    .line 866
    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;
    invoke-static {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->access$1100(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    .line 867
    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;
    invoke-static {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->access$1300(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 868
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackground:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 869
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateBackground()V

    .line 871
    :cond_1
    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mIsRadio:Z
    invoke-static {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->access$1500(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setIsRadio(Z)V

    .line 872
    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mPlayingFromBoldText:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->access$1400(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mPlayingFromDescription:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->access$1200(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    .end local v0    # "ss":Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;
    :goto_0
    return-void

    .line 874
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 843
    sget-boolean v2, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    if-eqz v2, :cond_0

    .line 844
    const-string v2, "QueueHeaderView"

    const-string v3, "onSaveInstanceState"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    :cond_0
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 847
    .local v0, "parcel":Landroid/os/Parcelable;
    new-instance v1, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;

    invoke-direct {v1, v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;-><init>(Landroid/os/Parcelable;)V

    .line 848
    .local v1, "ss":Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;
    iget-object v2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackground:Landroid/graphics/Bitmap;

    # setter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mBackground:Landroid/graphics/Bitmap;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->access$1002(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 849
    iget-object v2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    # setter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->access$1102(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;Lcom/google/android/music/mix/MixDescriptor;)Lcom/google/android/music/mix/MixDescriptor;

    .line 850
    iget-object v2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mPlayingFromDescription:Ljava/lang/String;

    # setter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mPlayingFromDescription:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->access$1202(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;Ljava/lang/String;)Ljava/lang/String;

    .line 851
    iget-object v2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    # setter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->access$1302(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;Lcom/google/android/music/store/ContainerDescriptor;)Lcom/google/android/music/store/ContainerDescriptor;

    .line 852
    iget-object v2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mPlayingFromBoldText:Ljava/lang/String;

    # setter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mPlayingFromBoldText:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->access$1402(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;Ljava/lang/String;)Ljava/lang/String;

    .line 853
    iget-boolean v2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mIsRadio:Z

    # setter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mIsRadio:Z
    invoke-static {v1, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->access$1502(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;Z)Z

    .line 854
    return-object v1
.end method

.method protected onSizeChanged(IIII)V
    .locals 5
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 880
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    .line 881
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->UNKNOWN:Lcom/google/android/music/store/ContainerDescriptor$Type;

    if-eq v1, v2, :cond_1

    .line 884
    sget-boolean v1, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    if-eqz v1, :cond_0

    .line 885
    const-string v1, "QueueHeaderView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSizeChanged: remaking artwork for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->isValid()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 889
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/art/ArtResolver;->getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    iget-object v3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    sget-object v4, Lcom/google/android/music/art/ArtType;->QUEUE_HEADER:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/music/art/ArtResolver;->getArt(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/art/ArtType;)Lcom/google/android/music/art/ContainerArtRequest;

    move-result-object v0

    .line 891
    .local v0, "request":Lcom/google/android/music/art/ArtRequest;
    invoke-virtual {v0, p0}, Lcom/google/android/music/art/ArtRequest;->addListener(Lcom/google/android/music/art/ArtRequest$Listener;)V

    .line 896
    .end local v0    # "request":Lcom/google/android/music/art/ArtRequest;
    :cond_1
    :goto_0
    return-void

    .line 893
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->doDefaultArtUpdate(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setQueuePlayingFromHeaderListener(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueuePlayingFromHeaderListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueuePlayingFromHeaderListener;

    .prologue
    .line 585
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mListener:Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueuePlayingFromHeaderListener;

    .line 586
    return-void
.end method

.method public showDefaultArt(Lcom/google/android/music/art/ArtRequest;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest",
            "<",
            "Lcom/google/android/music/art/ContainerMixDescriptorPair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 806
    .local p1, "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<Lcom/google/android/music/art/ContainerMixDescriptorPair;>;"
    iget-boolean v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mShowingDefault:Z

    if-eqz v0, :cond_0

    .line 809
    :goto_0
    return-void

    .line 807
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mShowingDefault:Z

    move-object v0, p1

    .line 808
    check-cast v0, Lcom/google/android/music/art/ContainerArtRequest;

    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest;->isDoneLoading()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateHeaderViews(Lcom/google/android/music/art/ContainerArtRequest;Z)V

    goto :goto_0
.end method

.method public unlockQueueHeader()V
    .locals 3

    .prologue
    .line 693
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mRefreshButton:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 694
    .local v0, "anim":Landroid/view/animation/Animation;
    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 695
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mRefreshButton:Landroid/widget/ImageView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 696
    return-void
.end method

.method public update(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/store/ContainerDescriptor;)V
    .locals 9
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p2, "container"    # Lcom/google/android/music/store/ContainerDescriptor;

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 221
    sget-boolean v5, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    if-eqz v5, :cond_0

    .line 222
    const-string v5, "QueueHeaderView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "update mix="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", container="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_0
    iget-boolean v5, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mIsAttached:Z

    if-nez v5, :cond_2

    .line 227
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    .line 228
    iput-object p2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 270
    :cond_1
    :goto_0
    return-void

    .line 232
    :cond_2
    if-eqz p2, :cond_1

    .line 235
    iget-boolean v5, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackgroundNeedsUpdate:Z

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {p2, v5}, Lcom/google/android/music/store/ContainerDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 238
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/art/ArtResolver;->getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver;

    move-result-object v3

    .line 239
    .local v3, "resolver":Lcom/google/android/music/art/ArtResolver;
    invoke-virtual {p2}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v1

    .line 240
    .local v1, "name":Ljava/lang/String;
    const/4 v0, 0x0

    .line 241
    .local v0, "isRadio":Z
    sget-object v5, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$7;->$SwitchMap$com$google$android$music$store$ContainerDescriptor$Type:[I

    invoke-virtual {p2}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/store/ContainerDescriptor$Type;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 257
    :goto_1
    invoke-virtual {p2}, Lcom/google/android/music/store/ContainerDescriptor;->isValid()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 258
    sget-object v5, Lcom/google/android/music/art/ArtType;->QUEUE_HEADER:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v3, p2, p1, v5}, Lcom/google/android/music/art/ArtResolver;->getArt(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/art/ArtType;)Lcom/google/android/music/art/ContainerArtRequest;

    move-result-object v2

    .line 260
    .local v2, "request":Lcom/google/android/music/art/ContainerArtRequest;
    invoke-virtual {v2, p0}, Lcom/google/android/music/art/ContainerArtRequest;->addListener(Lcom/google/android/music/art/ArtRequest$Listener;)V

    .line 265
    .end local v2    # "request":Lcom/google/android/music/art/ContainerArtRequest;
    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setIsRadio(Z)V

    .line 266
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    .line 267
    iput-object p2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 268
    invoke-direct {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->setupClickHandling()V

    .line 269
    iput-boolean v4, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackgroundNeedsUpdate:Z

    goto :goto_0

    .line 243
    :pswitch_0
    const-string v5, "QueueHeaderView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown container type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    invoke-direct {p0, v1, v8}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->doDefaultArtUpdate(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 247
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->doIFLUpdate()V

    .line 248
    const/4 v0, 0x1

    .line 249
    goto :goto_2

    .line 252
    :pswitch_2
    if-eqz p1, :cond_4

    const/4 v0, 0x1

    :goto_3
    goto :goto_1

    :cond_4
    move v0, v4

    goto :goto_3

    .line 262
    :cond_5
    invoke-virtual {p2}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, v8}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->doDefaultArtUpdate(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 241
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public updateBackground()V
    .locals 6

    .prologue
    .line 552
    iget-object v3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackground:Landroid/graphics/Bitmap;

    if-nez v3, :cond_0

    .line 553
    const-string v3, "QueueHeaderView"

    const-string v4, "updateBackground called with mBackground set to null. This shouldn\'t happen."

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v1

    .line 558
    .local v1, "isLandscape":Z
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    .line 559
    .local v2, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    if-eqz v1, :cond_3

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isXLargeScreen()Z

    move-result v3

    if-nez v3, :cond_3

    .line 560
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mGradient:Landroid/graphics/drawable/ShapeDrawable;

    .line 564
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    sget-boolean v3, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z

    if-eqz v3, :cond_1

    .line 565
    const-string v3, "QueueHeaderView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateBackground: isLandscape="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "drawable="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", width="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackground:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_2

    .line 569
    iget-object v3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackgroundView:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 571
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mOverlayView:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 572
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->invalidate()V

    .line 573
    return-void

    .line 562
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mShade:Landroid/graphics/drawable/Drawable;

    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    goto :goto_0
.end method
