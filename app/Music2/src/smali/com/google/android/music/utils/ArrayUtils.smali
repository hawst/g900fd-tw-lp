.class public Lcom/google/android/music/utils/ArrayUtils;
.super Ljava/lang/Object;
.source "ArrayUtils.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public static varargs combine([[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([[TT;)[TT;"
        }
    .end annotation

    .prologue
    .local p0, "arrays":[[Ljava/lang/Object;, "[[TT;"
    const/4 v4, 0x0

    .line 12
    if-eqz p0, :cond_0

    array-length v9, p0

    if-nez v9, :cond_1

    .line 39
    :cond_0
    return-object v4

    .line 16
    :cond_1
    const/4 v8, 0x0

    .line 17
    .local v8, "totalSize":I
    const/4 v2, 0x0

    .line 18
    .local v2, "componentType":Ljava/lang/Class;
    move-object v0, p0

    .local v0, "arr$":[[Ljava/lang/Object;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_3

    aget-object v1, v0, v5

    .line 19
    .local v1, "array":[Ljava/lang/Object;, "[TT;"
    if-eqz v1, :cond_2

    .line 20
    array-length v9, v1

    add-int/2addr v8, v9

    .line 21
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v2

    .line 18
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 24
    .end local v1    # "array":[Ljava/lang/Object;, "[TT;"
    :cond_3
    if-eqz v8, :cond_0

    if-eqz v2, :cond_0

    .line 28
    invoke-static {v2, v8}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Object;

    move-object v4, v9

    check-cast v4, [Ljava/lang/Object;

    .line 31
    .local v4, "finalArray":[Ljava/lang/Object;, "[TT;"
    const/4 v3, 0x0

    .line 32
    .local v3, "currentPos":I
    move-object v0, p0

    array-length v6, v0

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v6, :cond_0

    aget-object v1, v0, v5

    .line 33
    .restart local v1    # "array":[Ljava/lang/Object;, "[TT;"
    if-eqz v1, :cond_4

    .line 34
    array-length v7, v1

    .line 35
    .local v7, "length":I
    const/4 v9, 0x0

    invoke-static {v1, v9, v4, v3, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 36
    array-length v9, v1

    add-int/2addr v3, v9

    .line 32
    .end local v7    # "length":I
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method
