.class public final Lcom/google/android/music/playback/LocalDevicePlayback;
.super Lcom/google/android/music/playback/DevicePlayback;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/playback/LocalDevicePlayback$37;,
        Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;,
        Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;,
        Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;,
        Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;,
        Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    }
.end annotation


# static fields
.field private static volatile mDisableGaplessOverride:Ljava/lang/Boolean;


# instance fields
.field private final LOGV:Z

.field private mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioId:Lcom/google/android/music/download/ContentIdentifier;

.field private mAudioManager:Landroid/media/AudioManager;

.field private volatile mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

.field private final mCacheServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

.field private final mCastSessionManager:Lcom/google/android/music/cast/CastSessionManager;

.field private final mCastSessionManagerCallback:Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;

.field private final mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

.field private mCloudQueueSessionLock:Ljava/lang/Object;

.field private mCloudQueueVersion:I

.field private volatile mConnectedMediaRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

.field private volatile mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field private mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field mCursorCols:[Ljava/lang/String;

.field private final mDeleteFilter:Lcom/google/android/music/download/cache/IDeleteFilter;

.field private mDownloadManagerSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

.field private volatile mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;

.field private final mDynamicContainersObserver:Landroid/database/ContentObserver;

.field private mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

.field private mFuture:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mHistory:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mIsStreamingEnabled:Z

.field private volatile mIsSupposedToBePlaying:Z

.field private mIsSwappingToCloudQueue:Z

.field private volatile mLastUserExplicitPlayTime:J

.field private volatile mLastUserInteractionTime:J

.field private mListItemId:J

.field private mMediaList:Lcom/google/android/music/medialist/SongList;

.field private mMediaMountedCount:I

.field private mMediaRouter:Landroid/support/v7/media/MediaRouter;

.field private final mMediaRouterCallback:Landroid/support/v7/media/MediaRouter$Callback;

.field private mMediaplayerHandler:Landroid/os/Handler;

.field private volatile mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

.field private volatile mMusicCastMediaRouterCallback:Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;

.field private final mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

.field private mNextAudioId:Lcom/google/android/music/download/ContentIdentifier;

.field private mNextPlayPos:I

.field private mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

.field private volatile mOpenFailedCounter:I

.field private volatile mPausedByTransientLossOfFocus:Z

.field private final mPendingMediaButtonSeekCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

.field private mPlayPos:I

.field private volatile mPlayQueueFeeder:Lcom/google/android/music/mix/PlayQueueFeeder;

.field private final mPlayQueueFeederListener:Lcom/google/android/music/mix/PlayQueueFeederListener;

.field private mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

.field private volatile mPrequeueItems:Z

.field private volatile mPreviewPlaybackInfo:Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;

.field final mProgressListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/google/android/music/download/IDownloadProgressListener;",
            ">;"
        }
    .end annotation
.end field

.field private mQueueIsSaveable:Z

.field private final mRand:Lcom/google/android/music/StrictShuffler;

.field private mReloadedQueueSeekPos:J

.field private mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

.field private mRepeatMode:I

.field private mSavedAudioId:Lcom/google/android/music/download/ContentIdentifier;

.field private volatile mSelectedRouteId:Ljava/lang/String;

.field mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

.field private mSharedPreviewPlayListener:Landroid/content/BroadcastReceiver;

.field private mShuffleMode:I

.field private final mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

.field private volatile mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

.field private final mStreamingClientLock:Ljava/lang/Object;

.field private mUnmountReceiver:Landroid/content/BroadcastReceiver;

.field private volatile mUseLocalPlayer:Z

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWasCloudQueuePlaybackRequested:Z

.field private final mWiFiManagerLock:Ljava/lang/Object;

.field private mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDisableGaplessOverride:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService;)V
    .locals 9
    .param p1, "service"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    const/4 v8, -0x1

    const-wide/16 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1551
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/DevicePlayback;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    .line 140
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "artist"

    aput-object v1, v0, v5

    const/4 v1, 0x2

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "AlbumArtistId"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "AlbumArtist"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "artistSort"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "is_podcast"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "bookmark"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "hasRemote"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "hasLocal"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Rating"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "SourceId"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SourceAccount"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "StoreId"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "domainParam"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Size"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "Vid"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "Nid"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "VThumbnailUrl"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCursorCols:[Ljava/lang/String;

    .line 239
    iput v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    .line 240
    iput v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    .line 244
    iput v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaMountedCount:I

    .line 245
    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    .line 247
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    .line 248
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mHistory:Ljava/util/LinkedList;

    .line 249
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    .line 250
    iput v8, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    .line 251
    iput v8, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayPos:I

    .line 254
    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    .line 256
    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mSavedAudioId:Lcom/google/android/music/download/ContentIdentifier;

    .line 257
    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextAudioId:Lcom/google/android/music/download/ContentIdentifier;

    .line 258
    iput-wide v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J

    .line 260
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->PLAYBACK_SERVICE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    .line 261
    new-instance v0, Lcom/google/android/music/StrictShuffler;

    invoke-direct {v0}, Lcom/google/android/music/StrictShuffler;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRand:Lcom/google/android/music/StrictShuffler;

    .line 262
    iput v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mOpenFailedCounter:I

    .line 264
    iput-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPausedByTransientLossOfFocus:Z

    .line 266
    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    .line 268
    iput-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    .line 269
    iput-boolean v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mQueueIsSaveable:Z

    .line 270
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mReloadedQueueSeekPos:J

    .line 273
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClientLock:Ljava/lang/Object;

    .line 283
    iput-wide v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    .line 284
    iput-wide v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserExplicitPlayTime:J

    .line 289
    iput-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsStreamingEnabled:Z

    .line 291
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWiFiManagerLock:Ljava/lang/Object;

    .line 296
    iput-boolean v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUseLocalPlayer:Z

    .line 297
    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mSelectedRouteId:Ljava/lang/String;

    .line 298
    iput-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPrequeueItems:Z

    .line 302
    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 304
    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMusicCastMediaRouterCallback:Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;

    .line 306
    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    .line 307
    iput v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCloudQueueVersion:I

    .line 308
    iput-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWasCloudQueuePlaybackRequested:Z

    .line 309
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCloudQueueSessionLock:Ljava/lang/Object;

    .line 310
    iput-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSwappingToCloudQueue:Z

    .line 312
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPendingMediaButtonSeekCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 314
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$1;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouterCallback:Landroid/support/v7/media/MediaRouter$Callback;

    .line 341
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$2;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCastSessionManagerCallback:Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;

    .line 371
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$3;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

    .line 390
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$4;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDeleteFilter:Lcom/google/android/music/download/cache/IDeleteFilter;

    .line 411
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mProgressListeners:Landroid/os/RemoteCallbackList;

    .line 414
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$5;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$5;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mSharedPreviewPlayListener:Landroid/content/BroadcastReceiver;

    .line 439
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$6;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$6;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayQueueFeederListener:Lcom/google/android/music/mix/PlayQueueFeederListener;

    .line 618
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$7;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$7;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    .line 872
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$8;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$8;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    .line 1484
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$9;

    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback$9;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDynamicContainersObserver:Landroid/database/ContentObserver;

    .line 1501
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$10;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$10;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 1508
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$11;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$11;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDownloadManagerSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    .line 1532
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$12;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$12;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCacheServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    .line 1552
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    .line 1553
    new-instance v0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

    invoke-direct {v0, v1}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;-><init>(Lcom/google/android/music/net/IStreamabilityChangeListener;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .line 1557
    invoke-static {p1}, Lcom/google/android/music/net/NetworkMonitor;->initIsStreamingEnabled(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsStreamingEnabled:Z

    .line 1559
    new-instance v0, Lcom/google/android/music/cast/CastTokenClientImpl;

    invoke-direct {v0, p1}, Lcom/google/android/music/cast/CastTokenClientImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    .line 1560
    new-instance v0, Lcom/google/android/music/cast/CastSessionManager;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCastSessionManagerCallback:Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;

    invoke-direct {v0, p1, v1}, Lcom/google/android/music/cast/CastSessionManager;-><init>(Landroid/content/Context;Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCastSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    .line 1561
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/playback/LocalDevicePlayback;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onSongChanged()V

    return-void
.end method

.method static synthetic access$10000(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->stop(Z)V

    return-void
.end method

.method static synthetic access$10100(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;
    .param p2, "x2"    # I

    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/playback/LocalDevicePlayback;->feedQueueIfNeeded(Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$10200(Lcom/google/android/music/playback/LocalDevicePlayback;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/playback/LocalDevicePlayback;->nextImplSync(ZI)V

    return-void
.end method

.method static synthetic access$10300(Lcom/google/android/music/playback/LocalDevicePlayback;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCloudQueueVersion:I

    return v0
.end method

.method static synthetic access$10400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/support/v7/media/MediaRouter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    return-object v0
.end method

.method static synthetic access$10502(Lcom/google/android/music/playback/LocalDevicePlayback;Landroid/support/v7/media/MediaRouter$RouteInfo;)Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mConnectedMediaRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    return-object p1
.end method

.method static synthetic access$10600(Lcom/google/android/music/playback/LocalDevicePlayback;Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->addRemotePlaybackClient(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$10700(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/cast/CastSessionManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCastSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    return-object v0
.end method

.method static synthetic access$10802(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPrequeueItems:Z

    return p1
.end method

.method static synthetic access$10900(Lcom/google/android/music/playback/LocalDevicePlayback;J)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # J

    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/playback/LocalDevicePlayback;->resumePlaybackAsync(J)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/music/playback/LocalDevicePlayback;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J

    return-wide v0
.end method

.method static synthetic access$1102(Lcom/google/android/music/playback/LocalDevicePlayback;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # J

    .prologue
    .line 133
    iput-wide p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J

    return-wide p1
.end method

.method static synthetic access$1200(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;IJ)Lcom/google/android/music/store/PlayQueueAddResult;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "x2"    # I
    .param p3, "x3"    # J

    .prologue
    .line 133
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/music/playback/LocalDevicePlayback;->queue(Lcom/google/android/music/medialist/SongList;IJ)Lcom/google/android/music/store/PlayQueueAddResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->handleMediaButtonSeek(Z)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/mix/MixGenerationState;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onQueueChanged()V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onOpenStarted()V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/music/playback/LocalDevicePlayback;)[Lcom/google/android/music/download/ContentIdentifier;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getNextSongs()[Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/music/playback/LocalDevicePlayback;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextAudioId:Lcom/google/android/music/download/ContentIdentifier;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/ContentIdentifier;)Lcom/google/android/music/download/ContentIdentifier;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextAudioId:Lcom/google/android/music/download/ContentIdentifier;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/ContentIdentifier;)Lcom/google/android/music/download/ContentIdentifier;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/google/android/music/playback/LocalDevicePlayback;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUseLocalPlayer:Z

    return v0
.end method

.method static synthetic access$2600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/google/android/music/playback/LocalDevicePlayback;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserExplicitPlayTime:J

    return-wide v0
.end method

.method static synthetic access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    return v0
.end method

.method static synthetic access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # I

    .prologue
    .line 133
    iput p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    return p1
.end method

.method static synthetic access$2900(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMusicCastMediaRouterCallback:Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/ContentIdentifier;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->refreshCursor(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->updateDescriptorAndQueue(Z)V

    return-void
.end method

.method static synthetic access$3200(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCloudQueueSessionLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->resetCurrentSongMetaDataCursor()V

    return-void
.end method

.method static synthetic access$3400(Lcom/google/android/music/playback/LocalDevicePlayback;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWasCloudQueuePlaybackRequested:Z

    return v0
.end method

.method static synthetic access$3402(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWasCloudQueuePlaybackRequested:Z

    return p1
.end method

.method static synthetic access$3500(Lcom/google/android/music/playback/LocalDevicePlayback;ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z
    .param p5, "x5"    # J
    .param p7, "x6"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    .line 133
    invoke-direct/range {p0 .. p7}, Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndNext(ZZZZJLcom/google/android/music/download/ContentIdentifier;)V

    return-void
.end method

.method static synthetic access$3602(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSwappingToCloudQueue:Z

    return p1
.end method

.method static synthetic access$3700(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->next(Z)V

    return-void
.end method

.method static synthetic access$3800(Lcom/google/android/music/playback/LocalDevicePlayback;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayPos:I

    return v0
.end method

.method static synthetic access$3802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # I

    .prologue
    .line 133
    iput p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayPos:I

    return p1
.end method

.method static synthetic access$3900(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->logTrackStartEvent(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->feedQueueIfNeeded(Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/playback/AsyncMediaPlayer;)Lcom/google/android/music/playback/AsyncMediaPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/playback/AsyncMediaPlayer;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    return-object p1
.end method

.method static synthetic access$4100(Lcom/google/android/music/playback/LocalDevicePlayback;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    return v0
.end method

.method static synthetic access$4200(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndPlay(Z)V

    return-void
.end method

.method static synthetic access$4300(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->pause(Z)V

    return-void
.end method

.method static synthetic access$4400(Lcom/google/android/music/playback/LocalDevicePlayback;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPausedByTransientLossOfFocus:Z

    return v0
.end method

.method static synthetic access$4402(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPausedByTransientLossOfFocus:Z

    return p1
.end method

.method static synthetic access$4500(Lcom/google/android/music/playback/LocalDevicePlayback;ZLjava/lang/String;ZJ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z
    .param p4, "x4"    # J

    .prologue
    .line 133
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/playback/LocalDevicePlayback;->setMediaRouteImpl(ZLjava/lang/String;ZJ)V

    return-void
.end method

.method static synthetic access$4600(Lcom/google/android/music/playback/LocalDevicePlayback;Ljava/lang/String;D)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # D

    .prologue
    .line 133
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/playback/LocalDevicePlayback;->setMediaRouteVolumeImpl(Ljava/lang/String;D)V

    return-void
.end method

.method static synthetic access$4700(Lcom/google/android/music/playback/LocalDevicePlayback;Ljava/lang/String;D)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # D

    .prologue
    .line 133
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/playback/LocalDevicePlayback;->updateMediaRouteVolumeImpl(Ljava/lang/String;D)V

    return-void
.end method

.method static synthetic access$4800(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->selectDefaultMediaRouteImpl()V

    return-void
.end method

.method static synthetic access$4900(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "x2"    # Landroid/content/Context;
    .param p3, "x3"    # [Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/music/playback/LocalDevicePlayback;->getSyncMediaCursor(Lcom/google/android/music/medialist/SongList;Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/playback/LocalDevicePlayback;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsStreamingEnabled:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsStreamingEnabled:Z

    return p1
.end method

.method static synthetic access$5100(Lcom/google/android/music/playback/LocalDevicePlayback;Landroid/content/Context;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->isInCloudQueueMode(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5200(Lcom/google/android/music/playback/LocalDevicePlayback;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    return v0
.end method

.method static synthetic access$5302(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/IDownloadQueueManager;)Lcom/google/android/music/download/IDownloadQueueManager;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/download/IDownloadQueueManager;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;

    return-object p1
.end method

.method static synthetic access$5400(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->tryCreatingStreamingSchedulingClient()V

    return-void
.end method

.method static synthetic access$5500(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/cache/ICacheManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    return-object v0
.end method

.method static synthetic access$5502(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/cache/ICacheManager;)Lcom/google/android/music/download/cache/ICacheManager;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/download/cache/ICacheManager;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    return-object p1
.end method

.method static synthetic access$5600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/cache/IDeleteFilter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDeleteFilter:Lcom/google/android/music/download/cache/IDeleteFilter;

    return-object v0
.end method

.method static synthetic access$5700()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDisableGaplessOverride:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$5702(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Ljava/lang/Boolean;

    .prologue
    .line 133
    sput-object p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDisableGaplessOverride:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$5800(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->reloadQueue()V

    return-void
.end method

.method static synthetic access$5900(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->saveQueue(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    return-object v0
.end method

.method static synthetic access$6002(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mQueueIsSaveable:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/playback/AsyncMediaPlayer;)Lcom/google/android/music/playback/AsyncMediaPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/playback/AsyncMediaPlayer;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    return-object p1
.end method

.method static synthetic access$6100(Lcom/google/android/music/playback/LocalDevicePlayback;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->closeExternalStorageFiles(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$6208(Lcom/google/android/music/playback/LocalDevicePlayback;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaMountedCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaMountedCount:I

    return v0
.end method

.method static synthetic access$6300(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6500(Lcom/google/android/music/playback/LocalDevicePlayback;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->canStream()Z

    move-result v0

    return v0
.end method

.method static synthetic access$6600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/mix/PlayQueueFeeder;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayQueueFeeder:Lcom/google/android/music/mix/PlayQueueFeeder;

    return-object v0
.end method

.method static synthetic access$6700(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayQueue(Lcom/google/android/music/medialist/SongList;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$6800(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->clearMediaList()V

    return-void
.end method

.method static synthetic access$6900(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->isSuggestedMix(Lcom/google/android/music/medialist/SongList;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->setNextTrack()V

    return-void
.end method

.method static synthetic access$7000(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->enableInfiniteMixModeForSuggestedMix(Lcom/google/android/music/medialist/SongList;)V

    return-void
.end method

.method static synthetic access$7100(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->updateLastTimeSuggestedMixPlayed()V

    return-void
.end method

.method static synthetic access$7200(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->isRadio(Lcom/google/android/music/medialist/SongList;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$7300(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->disableInfiniteMixMode()V

    return-void
.end method

.method static synthetic access$7400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mSavedAudioId:Lcom/google/android/music/download/ContentIdentifier;

    return-object v0
.end method

.method static synthetic access$7500(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;ZIZ)Lcom/google/android/music/store/PlayQueueAddResult;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "x2"    # Z
    .param p3, "x3"    # I
    .param p4, "x4"    # Z

    .prologue
    .line 133
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/music/playback/LocalDevicePlayback;->caqPlay(Lcom/google/android/music/medialist/SongList;ZIZ)Lcom/google/android/music/store/PlayQueueAddResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->createPlayQueueSongList()Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7700(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->setMediaList(Lcom/google/android/music/medialist/SongList;)V

    return-void
.end method

.method static synthetic access$7800(Lcom/google/android/music/playback/LocalDevicePlayback;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mOpenFailedCounter:I

    return v0
.end method

.method static synthetic access$7802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # I

    .prologue
    .line 133
    iput p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mOpenFailedCounter:I

    return p1
.end method

.method static synthetic access$7808(Lcom/google/android/music/playback/LocalDevicePlayback;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mOpenFailedCounter:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mOpenFailedCounter:I

    return v0
.end method

.method static synthetic access$7900(Lcom/google/android/music/playback/LocalDevicePlayback;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mReloadedQueueSeekPos:J

    return-wide v0
.end method

.method static synthetic access$7902(Lcom/google/android/music/playback/LocalDevicePlayback;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # J

    .prologue
    .line 133
    iput-wide p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mReloadedQueueSeekPos:J

    return-wide p1
.end method

.method static synthetic access$800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/stream/StreamingClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

    return-object v0
.end method

.method static synthetic access$8000(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->requestCreateRadioStationSync()V

    return-void
.end method

.method static synthetic access$8100(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$8200(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mHistory:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$8300(Lcom/google/android/music/playback/LocalDevicePlayback;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->shouldPlayInRandomOrder()Z

    move-result v0

    return v0
.end method

.method static synthetic access$8400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/StrictShuffler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRand:Lcom/google/android/music/StrictShuffler;

    return-object v0
.end method

.method static synthetic access$8500(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->fillShuffleList()V

    return-void
.end method

.method static synthetic access$8600(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->dumpPastPresentAndFuture()V

    return-void
.end method

.method static synthetic access$8700(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->saveBookmarkIfNeeded()V

    return-void
.end method

.method static synthetic access$8800(Lcom/google/android/music/playback/LocalDevicePlayback;ZZZZLcom/google/android/music/download/ContentIdentifier;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z
    .param p5, "x5"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    .line 133
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndPrepareToPlaySync(ZZZZLcom/google/android/music/download/ContentIdentifier;)V

    return-void
.end method

.method static synthetic access$8900(Lcom/google/android/music/playback/LocalDevicePlayback;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isUsingPlayQueue()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/music/playback/LocalDevicePlayback;Ljava/lang/String;IJ)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # J

    .prologue
    .line 133
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/music/playback/LocalDevicePlayback;->setPreviewValues(Ljava/lang/String;IJ)V

    return-void
.end method

.method static synthetic access$9000(Lcom/google/android/music/playback/LocalDevicePlayback;ZZII)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 133
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/music/playback/LocalDevicePlayback;->getNextPlayPosition(ZZII)I

    move-result v0

    return v0
.end method

.method static synthetic access$9100(Lcom/google/android/music/playback/LocalDevicePlayback;IZZ)Landroid/util/Pair;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 133
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/playback/LocalDevicePlayback;->findPlayableSong(IZZ)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9200(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->invalidateNextPlayer()V

    return-void
.end method

.method static synthetic access$9300(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->createPlayer()Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9400(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/ContentIdentifier;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->duration(Lcom/google/android/music/download/ContentIdentifier;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$9500(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->tryNext()V

    return-void
.end method

.method static synthetic access$9600(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onPlaybackFailure()V

    return-void
.end method

.method static synthetic access$9700(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "x2"    # I

    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/playback/LocalDevicePlayback;->playNextSync(Lcom/google/android/music/medialist/SongList;I)V

    return-void
.end method

.method static synthetic access$9800(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;
    .param p1, "x1"    # Z

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->pauseSync(Z)V

    return-void
.end method

.method static synthetic access$9900(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPendingMediaButtonSeekCount:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private acquireWiFiLock()V
    .locals 6

    .prologue
    .line 5390
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->hasWiFiConnection()Z

    move-result v0

    .line 5391
    .local v0, "hasWiFiConnection":Z
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWiFiManagerLock:Ljava/lang/Object;

    monitor-enter v4

    .line 5392
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    if-nez v3, :cond_0

    if-eqz v0, :cond_0

    .line 5393
    const/4 v1, 0x1

    .line 5395
    .local v1, "wifiLockMode":I
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v5, "wifi"

    invoke-virtual {v3, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 5398
    .local v2, "wifiManager":Landroid/net/wifi/WifiManager;
    const-string v3, "LocalDevicePlayback"

    invoke-virtual {v2, v1, v3}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 5399
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 5400
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 5401
    const-string v3, "LocalDevicePlayback"

    const-string v5, "acquireWiFiLock"

    invoke-static {v3, v5}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 5403
    .end local v1    # "wifiLockMode":I
    .end local v2    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_0
    monitor-exit v4

    .line 5404
    return-void

    .line 5403
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private addCastRemotePlaybackClient(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 9
    .param p1, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    const/4 v8, 0x0

    .line 5150
    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast/CastDevice;->getFromBundle(Landroid/os/Bundle;)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v3

    .line 5154
    .local v3, "castDevice":Lcom/google/android/gms/cast/CastDevice;
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/cast/CastUtils;->useCastV2Api(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v3, :cond_1

    .line 5155
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->getStreamingAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v7

    .line 5157
    .local v7, "streamingAccount":Landroid/accounts/Account;
    if-eqz v7, :cond_0

    .line 5158
    new-instance v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/cast/CastUtils;->getCastV2AppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;-><init>(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;Lcom/google/android/gms/cast/CastDevice;Lcom/google/android/music/cast/CastTokenClient;Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    .line 5165
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    iget-object v1, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->joinSession(Ljava/lang/String;)V

    .line 5170
    :goto_0
    iput-boolean v8, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPrequeueItems:Z

    .line 5171
    const/4 v0, 0x1

    .line 5173
    .end local v7    # "streamingAccount":Landroid/accounts/Account;
    :goto_1
    return v0

    .line 5167
    .restart local v7    # "streamingAccount":Landroid/accounts/Account;
    :cond_0
    const-string v0, "LocalDevicePlayback"

    const-string v1, "Receiver selected but no streaming account found.  Cannot cast."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .end local v7    # "streamingAccount":Landroid/accounts/Account;
    :cond_1
    move v0, v8

    .line 5173
    goto :goto_1
.end method

.method private addDialRemotePlaybackClient(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 8
    .param p1, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    const/4 v7, 0x0

    .line 5185
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->isDialMediaRouteSupportEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p1}, Lcom/google/android/music/cast/CastUtils;->isDialRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5187
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/music/dial/DialDeviceFactory;->fromRoute(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)Lcom/google/android/music/dial/DialDevice;

    move-result-object v3

    .line 5189
    .local v3, "dialDevice":Lcom/google/android/music/dial/DialDevice;
    if-eqz v3, :cond_2

    .line 5190
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->getStreamingAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v6

    .line 5192
    .local v6, "streamingAccount":Landroid/accounts/Account;
    if-eqz v6, :cond_1

    .line 5193
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/dial/DialRemotePlaybackClient;-><init>(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;Lcom/google/android/music/dial/DialDevice;Lcom/google/android/music/cast/CastTokenClient;Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;)V

    .line 5195
    .local v0, "client":Lcom/google/android/music/dial/DialRemotePlaybackClient;
    invoke-virtual {v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->init()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5196
    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    .line 5197
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    iget-object v2, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->joinSession(Ljava/lang/String;)V

    .line 5207
    iput-boolean v7, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPrequeueItems:Z

    .line 5208
    const/4 v1, 0x1

    .line 5211
    .end local v0    # "client":Lcom/google/android/music/dial/DialRemotePlaybackClient;
    .end local v3    # "dialDevice":Lcom/google/android/music/dial/DialDevice;
    .end local v6    # "streamingAccount":Landroid/accounts/Account;
    :goto_0
    return v1

    .line 5199
    .restart local v0    # "client":Lcom/google/android/music/dial/DialRemotePlaybackClient;
    .restart local v3    # "dialDevice":Lcom/google/android/music/dial/DialDevice;
    .restart local v6    # "streamingAccount":Landroid/accounts/Account;
    :cond_0
    const-string v1, "LocalDevicePlayback"

    const-string v2, "Error initializing DialRemotePlaybackClient."

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v7

    .line 5200
    goto :goto_0

    .line 5203
    .end local v0    # "client":Lcom/google/android/music/dial/DialRemotePlaybackClient;
    :cond_1
    const-string v1, "LocalDevicePlayback"

    const-string v2, "Receiver selected but no streaming account found.  Cannot cast."

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v7

    .line 5205
    goto :goto_0

    .end local v3    # "dialDevice":Lcom/google/android/music/dial/DialDevice;
    .end local v6    # "streamingAccount":Landroid/accounts/Account;
    :cond_2
    move v1, v7

    .line 5211
    goto :goto_0
.end method

.method private addRemotePlaybackClient(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 1
    .param p1, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 5138
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->addCastRemotePlaybackClient(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->addDialRemotePlaybackClient(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private addToDatabase(Lcom/google/android/music/medialist/ExternalSongList;)Lcom/google/android/music/medialist/SongList;
    .locals 4
    .param p1, "externalSongList"    # Lcom/google/android/music/medialist/ExternalSongList;

    .prologue
    .line 2912
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Lcom/google/android/music/medialist/ExternalSongList;->addToStore(Landroid/content/Context;Z)[J

    move-result-object v1

    .line 2913
    .local v1, "localIds":[J
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    .line 2914
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/android/music/medialist/ExternalSongList;->getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 2915
    .local v0, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v2, Lcom/google/android/music/medialist/SelectedSongList;

    invoke-direct {v2, v0, v1}, Lcom/google/android/music/medialist/SelectedSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;[J)V

    .line 2917
    .end local v0    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private canStream()Z
    .locals 1

    .prologue
    .line 5454
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsStreamingEnabled:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isDownloadedOnlyMode()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private caqPlay(Lcom/google/android/music/medialist/SongList;ZIZ)Lcom/google/android/music/store/PlayQueueAddResult;
    .locals 10
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "shuffleList"    # Z
    .param p3, "playPosition"    # I
    .param p4, "shouldClearQueue"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 2771
    invoke-static {p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->isQueueable(Lcom/google/android/music/medialist/SongList;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2818
    :cond_0
    :goto_0
    return-object v7

    .line 2775
    :cond_1
    instance-of v4, p1, Lcom/google/android/music/medialist/ExternalSongList;

    if-eqz v4, :cond_2

    .line 2776
    check-cast p1, Lcom/google/android/music/medialist/ExternalSongList;

    .end local p1    # "medialist":Lcom/google/android/music/medialist/SongList;
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->addToDatabase(Lcom/google/android/music/medialist/ExternalSongList;)Lcom/google/android/music/medialist/SongList;

    move-result-object p1

    .line 2777
    .restart local p1    # "medialist":Lcom/google/android/music/medialist/SongList;
    if-eqz p1, :cond_0

    .line 2782
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    new-array v8, v3, [Ljava/lang/String;

    const-string v9, "audio_id"

    aput-object v9, v8, v5

    invoke-direct {p0, p1, v4, v8, v7}, Lcom/google/android/music/playback/LocalDevicePlayback;->getSyncMediaCursor(Lcom/google/android/music/medialist/SongList;Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;

    move-result-object v2

    .line 2788
    .local v2, "cursorToAppend":Landroid/database/Cursor;
    if-eqz v2, :cond_0

    .line 2793
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    .line 2794
    .local v0, "store":Lcom/google/android/music/store/Store;
    iget v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    if-ne v4, v3, :cond_3

    move v5, v3

    .line 2795
    .local v5, "sprinkle":Z
    :cond_3
    const/4 v4, -0x1

    if-ne p3, v4, :cond_4

    .line 2796
    iget v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    if-ne v4, v3, :cond_7

    .line 2797
    invoke-direct {p0, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->getRandomPosition(Landroid/database/Cursor;)I

    move-result p3

    .line 2803
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->isInCloudQueueMode(Landroid/content/Context;)Z

    move-result v6

    .line 2804
    .local v6, "isInCloudQueueMode":Z
    if-eqz p4, :cond_5

    .line 2805
    invoke-virtual {v0, v6}, Lcom/google/android/music/store/Store;->caqClearQueue(Z)Z

    .line 2808
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/music/medialist/SongList;->getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v1

    .local v1, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    move v3, p2

    move v4, p3

    .line 2809
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/music/store/Store;->caqPlay(Lcom/google/android/music/store/ContainerDescriptor;Landroid/database/Cursor;ZIZZ)Lcom/google/android/music/store/PlayQueueAddResult;

    move-result-object v7

    .line 2812
    .local v7, "result":Lcom/google/android/music/store/PlayQueueAddResult;
    if-eqz v6, :cond_6

    invoke-virtual {v7}, Lcom/google/android/music/store/PlayQueueAddResult;->getAddedSize()I

    move-result v3

    if-lez v3, :cond_6

    .line 2813
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/cloudclient/CloudQueueManager;->setCloudQueue(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2818
    :cond_6
    invoke-static {v2}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .line 2799
    .end local v1    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v6    # "isInCloudQueueMode":Z
    .end local v7    # "result":Lcom/google/android/music/store/PlayQueueAddResult;
    :cond_7
    const/4 p3, 0x0

    goto :goto_1

    .line 2818
    .end local v0    # "store":Lcom/google/android/music/store/Store;
    .end local v5    # "sprinkle":Z
    :catchall_0
    move-exception v3

    invoke-static {v2}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v3
.end method

.method private clearCursor()V
    .locals 4

    .prologue
    .line 1854
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v2

    .line 1855
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 1856
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 1857
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 1859
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v3, Lcom/google/android/music/playback/LocalDevicePlayback$19;

    invoke-direct {v3, p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback$19;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;Landroid/database/Cursor;)V

    invoke-static {v1, v3}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 1866
    :cond_0
    monitor-exit v2

    .line 1867
    return-void

    .line 1866
    .end local v0    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private clearMediaList()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2667
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-virtual {v0}, Lcom/google/android/music/utils/LoggableHandler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Not called on sBackendServiceWorker"

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 2670
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->clearCursor()V

    .line 2672
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mSavedAudioId:Lcom/google/android/music/download/ContentIdentifier;

    .line 2673
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v0, v2}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->setMediaList(Lcom/google/android/music/medialist/SongList;)V

    .line 2674
    iput-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    .line 2675
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onQueueChanged()V

    .line 2676
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onSongChanged()V

    .line 2677
    return-void
.end method

.method private closeExternalStorageFiles(Ljava/lang/String;)V
    .locals 1
    .param p1, "storagePath"    # Ljava/lang/String;

    .prologue
    .line 1808
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->stop(Z)V

    .line 1809
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onQueueChanged()V

    .line 1810
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onSongChanged()V

    .line 1811
    return-void
.end method

.method private createPlayQueueSongList()Lcom/google/android/music/medialist/SongList;
    .locals 1

    .prologue
    .line 2657
    new-instance v0, Lcom/google/android/music/medialist/CaqPlayQueueSongList;

    invoke-direct {v0}, Lcom/google/android/music/medialist/CaqPlayQueueSongList;-><init>()V

    return-object v0
.end method

.method private createPlayer()Lcom/google/android/music/playback/AsyncMediaPlayer;
    .locals 6

    .prologue
    .line 5018
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUseLocalPlayer:Z

    if-eqz v0, :cond_0

    .line 5019
    new-instance v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;-><init>(Landroid/content/Context;Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;)V

    .line 5021
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    iget-boolean v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPrequeueItems:Z

    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCastSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;-><init>(Landroid/content/Context;Lcom/google/android/music/cast/CastTokenClient;Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;ZLcom/google/android/music/cast/CastSessionManager;)V

    goto :goto_0
.end method

.method private disableInfiniteMixMode()V
    .locals 1

    .prologue
    .line 2295
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    if-nez v0, :cond_0

    .line 2300
    :goto_0
    return-void

    .line 2298
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    .line 2299
    const-string v0, "com.google.android.music.mix.playbackmodechanged"

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doGetDuration(Landroid/database/Cursor;)J
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 4623
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v1}, Lcom/google/android/music/medialist/SongList;->isSharedDomain()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4624
    const-string v1, "SourceId"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4626
    .local v0, "remoteUrl":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getPreviewPlayDurationForUrl(Ljava/lang/String;)J

    move-result-wide v2

    .line 4628
    .end local v0    # "remoteUrl":Ljava/lang/String;
    :goto_0
    return-wide v2

    :cond_0
    const/16 v1, 0xa

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    goto :goto_0
.end method

.method private dumpPastPresentAndFuture()V
    .locals 0

    .prologue
    .line 3038
    return-void
.end method

.method private duration(Lcom/google/android/music/download/ContentIdentifier;)J
    .locals 6
    .param p1, "songId"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    const-wide/16 v2, -0x1

    .line 4601
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    if-nez v1, :cond_0

    .line 4614
    :goto_0
    return-wide v2

    .line 4605
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCursorCols:[Ljava/lang/String;

    invoke-virtual {v1, v4, p1, v5}, Lcom/google/android/music/medialist/SongList;->getSongCursor(Landroid/content/Context;Lcom/google/android/music/download/ContentIdentifier;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 4606
    .local v0, "cursor":Landroid/database/Cursor;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Queried ID "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 4609
    if-eqz v0, :cond_1

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4610
    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->doGetDuration(Landroid/database/Cursor;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 4614
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v1
.end method

.method private enableInfiniteMixMode(Lcom/google/android/music/mix/MixDescriptor;)V
    .locals 4
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    const/4 v2, 0x0

    .line 2280
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    .line 2281
    .local v0, "oldState":Lcom/google/android/music/mix/MixGenerationState;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/mix/MixGenerationState;->getMix()Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/music/mix/MixDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2292
    :goto_0
    return-void

    .line 2284
    :cond_0
    iput v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    .line 2285
    iput v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    .line 2286
    new-instance v1, Lcom/google/android/music/mix/MixGenerationState;

    invoke-direct {v1, p1}, Lcom/google/android/music/mix/MixGenerationState;-><init>(Lcom/google/android/music/mix/MixDescriptor;)V

    iput-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    .line 2289
    const-string v1, "LocalDevicePlayback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enabled mix "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/music/mix/MixDescriptor;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2291
    const-string v1, "com.google.android.music.mix.playbackmodechanged"

    invoke-virtual {p0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private enableInfiniteMixModeForSuggestedMix(Lcom/google/android/music/medialist/SongList;)V
    .locals 8
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 2303
    move-object v0, p1

    check-cast v0, Lcom/google/android/music/medialist/PlaylistSongList;

    .line 2304
    .local v0, "mix":Lcom/google/android/music/medialist/PlaylistSongList;
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/store/Store;->getFirstItemId(J)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v7

    .line 2306
    .local v7, "mixSeedId":Lcom/google/android/music/download/ContentIdentifier;
    if-nez v7, :cond_0

    .line 2307
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Failed to get seed for suggested mix"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2310
    :cond_0
    new-instance v1, Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {v7}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v2

    sget-object v4, Lcom/google/android/music/mix/MixDescriptor$Type;->TRACK_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/google/android/music/medialist/SongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/mix/MixDescriptor;-><init>(JLcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->enableInfiniteMixMode(Lcom/google/android/music/mix/MixDescriptor;)V

    .line 2312
    return-void
.end method

.method private endSession()V
    .locals 1

    .prologue
    .line 5500
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5501
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->tearDownRemotePlaybackClient()V

    .line 5505
    :goto_0
    return-void

    .line 5503
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCastSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v0}, Lcom/google/android/music/cast/CastSessionManager;->endSession()V

    goto :goto_0
.end method

.method private feedQueueIfNeeded(Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)Z
    .locals 1
    .param p1, "action"    # Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .prologue
    .line 2227
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->feedQueueIfNeeded(Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;I)Z

    move-result v0

    return v0
.end method

.method private feedQueueIfNeeded(Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;I)Z
    .locals 8
    .param p1, "action"    # Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;
    .param p2, "count"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2237
    iget-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v3, :cond_0

    .line 2238
    const-string v3, "LocalDevicePlayback"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "feedQueueIfNeeded: action="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2240
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-virtual {v4}, Lcom/google/android/music/utils/LoggableHandler;->getLooper()Landroid/os/Looper;

    move-result-object v4

    if-eq v3, v4, :cond_1

    .line 2241
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "feedQueueIfNeeded must run on the AsyncWorkers.sBackendServiceWorker handler"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2244
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    if-eqz v3, :cond_7

    .line 2245
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "music_min_infinite_mix_tail_size"

    const/4 v5, 0x5

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 2249
    .local v0, "minQueueTailSize":I
    sget-object v3, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->NEXT:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    if-ne p1, v3, :cond_2

    .line 2252
    add-int/2addr v0, p2

    .line 2254
    :cond_2
    if-gez v0, :cond_3

    .line 2255
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Negative queue tail size"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2257
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v3, :cond_4

    .line 2258
    const-string v3, "LocalDevicePlayback"

    const-string v4, "feedQueueIfNeeded: mPlayPos=%d mPlayList.length=%d minQueueTailSize=%d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    iget-object v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v6}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2262
    :cond_4
    iget v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    iget v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v4}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v4

    sub-int/2addr v4, v0

    if-lt v3, v4, :cond_7

    .line 2263
    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->requestMoreContentSync(Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    .line 2264
    iget-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v3, :cond_6

    .line 2265
    const-string v3, "LocalDevicePlayback"

    const-string v4, "feedQueueIfNeeded: updated the queue mPlayPos=%d minQueueTailSize=%d"

    new-array v5, v7, [Ljava/lang/Object;

    iget v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2276
    .end local v0    # "minQueueTailSize":I
    :cond_6
    :goto_0
    return v1

    .line 2272
    :cond_7
    iget-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v3, :cond_8

    .line 2273
    const-string v3, "LocalDevicePlayback"

    const-string v4, "feedQueueIfNeeded: didn\'t update the queue mPlayPos=%d"

    new-array v1, v1, [Ljava/lang/Object;

    iget v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    move v1, v2

    .line 2276
    goto :goto_0
.end method

.method private fillShuffleList()V
    .locals 14

    .prologue
    .line 2975
    iget-object v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v10}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v5

    .line 2976
    .local v5, "playlistLength":I
    const/16 v10, 0xc8

    if-le v5, v10, :cond_2

    const/4 v3, 0x1

    .line 2978
    .local v3, "infiniteShuffle":Z
    :goto_0
    const/16 v10, 0xc8

    invoke-static {v10, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 2979
    .local v4, "numToAdd":I
    iget-object v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v10

    if-eqz v10, :cond_0

    .line 2980
    iget-object v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v10

    sub-int/2addr v4, v10

    .line 2983
    :cond_0
    iget v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    const/4 v11, -0x1

    if-eq v10, v11, :cond_1

    .line 2984
    add-int/lit8 v4, v4, -0x1

    .line 2987
    :cond_1
    if-gtz v4, :cond_3

    .line 3028
    :goto_1
    return-void

    .line 2976
    .end local v3    # "infiniteShuffle":Z
    .end local v4    # "numToAdd":I
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 2991
    .restart local v3    # "infiniteShuffle":Z
    .restart local v4    # "numToAdd":I
    :cond_3
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v8

    .line 2993
    .local v8, "ratedDownSongs":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v4, :cond_a

    .line 2995
    iget-object v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRand:Lcom/google/android/music/StrictShuffler;

    invoke-virtual {v10, v5}, Lcom/google/android/music/StrictShuffler;->nextInt(I)I

    move-result v6

    .line 2997
    .local v6, "possiblePosition":I
    :try_start_0
    iget-object v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v10, v6}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->get(I)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v2

    .line 2998
    .local v2, "id":Lcom/google/android/music/download/ContentIdentifier;
    if-eqz v2, :cond_9

    .line 2999
    const/4 v9, 0x0

    .line 3000
    .local v9, "songRating":I
    invoke-virtual {v2}, Lcom/google/android/music/download/ContentIdentifier;->isDefaultDomain()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 3001
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v10

    invoke-virtual {v2}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Lcom/google/android/music/store/Store;->getRating(J)I

    move-result v9

    .line 3007
    :goto_3
    const/4 v10, 0x1

    if-eq v9, v10, :cond_4

    const/4 v10, 0x1

    if-eq v9, v10, :cond_4

    const/4 v10, 0x2

    if-ne v9, v10, :cond_7

    :cond_4
    const/4 v7, 0x1

    .line 3010
    .local v7, "ratedDown":Z
    :goto_4
    if-eqz v7, :cond_8

    .line 3011
    if-nez v3, :cond_5

    .line 3012
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 2993
    .end local v2    # "id":Lcom/google/android/music/download/ContentIdentifier;
    .end local v7    # "ratedDown":Z
    .end local v9    # "songRating":I
    :cond_5
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3003
    .restart local v2    # "id":Lcom/google/android/music/download/ContentIdentifier;
    .restart local v9    # "songRating":I
    :cond_6
    const-string v10, "LocalDevicePlayback"

    const-string v11, "Only Default domain songs are valid for StoreService.getRating"

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 3020
    .end local v2    # "id":Lcom/google/android/music/download/ContentIdentifier;
    .end local v9    # "songRating":I
    :catch_0
    move-exception v0

    .line 3021
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v10, "LocalDevicePlayback"

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 3007
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    .restart local v2    # "id":Lcom/google/android/music/download/ContentIdentifier;
    .restart local v9    # "songRating":I
    :cond_7
    const/4 v7, 0x0

    goto :goto_4

    .line 3015
    .restart local v7    # "ratedDown":Z
    :cond_8
    :try_start_1
    iget-object v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 3018
    .end local v7    # "ratedDown":Z
    .end local v9    # "songRating":I
    :cond_9
    const-string v10, "LocalDevicePlayback"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Could not get id for position: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5

    .line 3024
    .end local v2    # "id":Lcom/google/android/music/download/ContentIdentifier;
    .end local v6    # "possiblePosition":I
    :cond_a
    iget-boolean v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v10, :cond_b

    invoke-virtual {v8}, Ljava/util/LinkedList;->size()I

    move-result v10

    if-eqz v10, :cond_b

    .line 3025
    const-string v10, "LocalDevicePlayback"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Rated down songs added to end of shuffle list: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3027
    :cond_b
    iget-object v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    invoke-virtual {v10, v8}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1
.end method

.method private findPlayableSong(IZZ)Landroid/util/Pair;
    .locals 14
    .param p1, "currentPlayPos"    # I
    .param p2, "fromUserAction"    # Z
    .param p3, "reverse"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZZ)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/music/store/MusicFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3446
    const/4 v10, -0x1

    if-ne p1, v10, :cond_0

    .line 3448
    const/4 v10, 0x0

    .line 3548
    :goto_0
    return-object v10

    .line 3451
    :cond_0
    const/4 v0, 0x0

    .line 3452
    .local v0, "audioId":Lcom/google/android/music/download/ContentIdentifier;
    const/4 v5, 0x0

    .line 3453
    .local v5, "preferredMusicFile":Lcom/google/android/music/store/MusicFile;
    move v6, p1

    .line 3454
    .local v6, "requestedPos":I
    const-wide/16 v8, 0x0

    .line 3455
    .local v8, "tryCount":J
    const/4 v4, 0x0

    .line 3458
    .local v4, "playableSongFound":Z
    :goto_1
    const/4 v7, 0x0

    .line 3460
    .local v7, "shouldStream":Z
    iget-object v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v10, p1}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->getAudioIdAndListItemId(I)Landroid/util/Pair;

    move-result-object v1

    .line 3462
    .local v1, "audioIdAndListItemId":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/Long;>;"
    if-nez v1, :cond_2

    .line 3464
    iget-boolean v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v10, :cond_1

    .line 3465
    const-string v10, "LocalDevicePlayback"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Failed to get audioId for position: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3467
    :cond_1
    const/4 v10, 0x0

    goto :goto_0

    .line 3470
    :cond_2
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local v0    # "audioId":Lcom/google/android/music/download/ContentIdentifier;
    check-cast v0, Lcom/google/android/music/download/ContentIdentifier;

    .line 3476
    .restart local v0    # "audioId":Lcom/google/android/music/download/ContentIdentifier;
    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier;->isCacheable()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 3478
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v11

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayingLocally()Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v10, 0x0

    :goto_2
    invoke-virtual {v11, v0, v10}, Lcom/google/android/music/store/Store;->getPreferredMusicId(Lcom/google/android/music/download/ContentIdentifier;I)Lcom/google/android/music/download/ContentIdentifier;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3486
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v12

    invoke-static {v10, v11, v12, v13}, Lcom/google/android/music/store/MusicFile;->getSummaryMusicFile(Lcom/google/android/music/store/Store;Lcom/google/android/music/store/MusicFile;J)Lcom/google/android/music/store/MusicFile;
    :try_end_1
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    .line 3493
    invoke-virtual {v5}, Lcom/google/android/music/store/MusicFile;->getLocalCopyType()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    .line 3516
    :goto_3
    if-eqz v7, :cond_3

    if-eqz v7, :cond_a

    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->canStream()Z

    move-result v10

    if-eqz v10, :cond_a

    :cond_3
    const/4 v4, 0x1

    .line 3518
    :goto_4
    if-eqz v4, :cond_b

    .line 3536
    :cond_4
    :goto_5
    if-eqz v4, :cond_f

    .line 3537
    if-eq v6, p1, :cond_5

    .line 3538
    const-string v10, "LocalDevicePlayback"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "findNextPlayable: requested="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", found="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3540
    if-eqz p2, :cond_5

    iget-object v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v10}, Lcom/google/android/music/playback/MusicPlaybackService;->isUIVisible()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 3541
    iget-object v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    iget-object v11, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    const v12, 0x7f0b00e8

    invoke-virtual {v11, v12}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    invoke-static {v10, v11, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    .line 3546
    :cond_5
    new-instance v10, Landroid/util/Pair;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-direct {v10, v11, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3478
    :cond_6
    const/4 v10, 0x1

    goto :goto_2

    .line 3480
    :catch_0
    move-exception v2

    .line 3481
    .local v2, "e":Ljava/io/FileNotFoundException;
    const-string v10, "LocalDevicePlayback"

    const-string v11, "Failed to dedupe the track"

    invoke-static {v10, v11, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3482
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 3488
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v2

    .line 3489
    .local v2, "e":Lcom/google/android/music/store/DataNotFoundException;
    const-string v10, "LocalDevicePlayback"

    const-string v11, "Failed to load track data: "

    invoke-static {v10, v11, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3490
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 3495
    .end local v2    # "e":Lcom/google/android/music/store/DataNotFoundException;
    :sswitch_0
    const/4 v7, 0x0

    .line 3496
    goto :goto_3

    .line 3502
    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10, v5}, Lcom/google/android/music/download/cache/CacheUtils;->resolveMusicPath(Landroid/content/Context;Lcom/google/android/music/store/MusicFile;)Ljava/io/File;

    move-result-object v3

    .line 3503
    .local v3, "location":Ljava/io/File;
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_8

    :cond_7
    const/4 v7, 0x1

    .line 3504
    :goto_6
    goto/16 :goto_3

    .line 3503
    :cond_8
    const/4 v7, 0x0

    goto :goto_6

    .line 3508
    .end local v3    # "location":Ljava/io/File;
    :sswitch_2
    const/4 v7, 0x1

    goto/16 :goto_3

    .line 3513
    :cond_9
    const/4 v7, 0x1

    goto/16 :goto_3

    .line 3516
    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 3521
    :cond_b
    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iget-object v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v10}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v10

    int-to-long v10, v10

    cmp-long v10, v8, v10

    if-ltz v10, :cond_c

    .line 3523
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onPlaybackFailure()V

    goto/16 :goto_5

    .line 3525
    :cond_c
    if-nez p2, :cond_d

    if-nez p3, :cond_d

    iget v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    if-nez v10, :cond_d

    iget-object v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v10}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ge p1, v10, :cond_4

    .line 3533
    :cond_d
    const/4 v11, 0x1

    const/4 v12, 0x0

    if-eqz p3, :cond_e

    const/4 v10, -0x1

    :goto_7
    invoke-direct {p0, v11, v12, p1, v10}, Lcom/google/android/music/playback/LocalDevicePlayback;->getNextPlayPosition(ZZII)I

    move-result p1

    .line 3534
    goto/16 :goto_1

    .line 3533
    :cond_e
    const/4 v10, 0x1

    goto :goto_7

    .line 3548
    :cond_f
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 3493
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x64 -> :sswitch_1
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_0
    .end sparse-switch
.end method

.method private getNextPlayPosition(ZZII)I
    .locals 9
    .param p1, "forceNext"    # Z
    .param p2, "peak"    # Z
    .param p3, "playPos"    # I
    .param p4, "seeks"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v5, -0x1

    .line 4100
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-virtual {v7}, Lcom/google/android/music/utils/LoggableHandler;->getLooper()Landroid/os/Looper;

    move-result-object v7

    if-eq v6, v7, :cond_0

    .line 4101
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "getNextPlayPosition must run on the AsyncWorkers.sBackendServiceWorker handler"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 4104
    :cond_0
    iget-object v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v6}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v6

    if-gtz v6, :cond_1

    .line 4105
    const-string v4, "LocalDevicePlayback"

    const-string v6, "No play queue"

    invoke-static {v4, v6}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 4204
    :goto_0
    return v5

    .line 4109
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->shouldPlayInRandomOrder()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 4110
    iget-object v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    monitor-enter v6

    .line 4111
    :try_start_0
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v4}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v4

    const/16 v7, 0xc8

    if-le v4, v7, :cond_2

    .line 4112
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->fillShuffleList()V

    .line 4115
    :cond_2
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-nez v4, :cond_5

    .line 4117
    iget v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    if-nez v4, :cond_3

    if-nez p1, :cond_3

    .line 4119
    monitor-exit v6

    goto :goto_0

    .line 4147
    :catchall_0
    move-exception v4

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 4120
    :cond_3
    :try_start_1
    iget v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    const/4 v7, 0x2

    if-eq v4, v7, :cond_4

    if-eqz p1, :cond_6

    .line 4122
    :cond_4
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->fillShuffleList()V

    .line 4137
    :cond_5
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-nez v4, :cond_9

    .line 4138
    const-string v4, "LocalDevicePlayback"

    const-string v7, "Failed to fill future in getNextPlayPosition()"

    new-instance v8, Ljava/lang/Throwable;

    invoke-direct {v8}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v4, v7, v8}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4140
    monitor-exit v6

    goto :goto_0

    .line 4123
    :cond_6
    iget v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    if-ne v4, v1, :cond_8

    .line 4124
    if-ltz p3, :cond_7

    .line 4125
    monitor-exit v6

    move v5, p3

    goto :goto_0

    .line 4127
    :cond_7
    const-string v4, "LocalDevicePlayback"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "called getNext in repeat current mode, but mPlayPos wasn\'t valid: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/Throwable;

    invoke-direct {v8}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v4, v7, v8}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4129
    monitor-exit v6

    goto :goto_0

    .line 4132
    :cond_8
    const-string v4, "LocalDevicePlayback"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Shouldn\'t be here, repeat mode is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/Throwable;

    invoke-direct {v8}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v4, v7, v8}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4134
    monitor-exit v6

    goto/16 :goto_0

    .line 4142
    :cond_9
    if-eqz p2, :cond_a

    .line 4143
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    monitor-exit v6

    goto/16 :goto_0

    .line 4145
    :cond_a
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 4149
    :cond_b
    add-int v6, p3, p4

    iget-object v7, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v7}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v7

    rem-int v3, v6, v7

    .line 4150
    .local v3, "targetPosition":I
    if-lez p4, :cond_11

    .line 4151
    iget-object v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v6}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v6

    sub-int/2addr v6, p4

    if-lt p3, v6, :cond_c

    .line 4152
    .local v1, "atTheEndOfPlaylist":Z
    :goto_1
    iget v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    packed-switch v4, :pswitch_data_0

    .line 4174
    const-string v4, "LocalDevicePlayback"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown repeat mode: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/Throwable;

    invoke-direct {v7}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v4, v6, v7}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .end local v1    # "atTheEndOfPlaylist":Z
    :cond_c
    move v1, v4

    .line 4151
    goto :goto_1

    .line 4154
    .restart local v1    # "atTheEndOfPlaylist":Z
    :pswitch_0
    if-ltz p3, :cond_e

    .line 4155
    if-eqz p1, :cond_d

    move v5, v3

    .line 4156
    goto/16 :goto_0

    :cond_d
    move v5, p3

    .line 4158
    goto/16 :goto_0

    .line 4161
    :cond_e
    const-string v4, "LocalDevicePlayback"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "called getNext in repeat current mode, but mPlayPos wasn\'t valid: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/Throwable;

    invoke-direct {v7}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v4, v6, v7}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 4166
    :pswitch_1
    if-eqz v1, :cond_10

    .line 4167
    if-eqz p1, :cond_f

    .end local v3    # "targetPosition":I
    :goto_2
    move v5, v3

    goto/16 :goto_0

    .restart local v3    # "targetPosition":I
    :cond_f
    move v3, v5

    goto :goto_2

    :cond_10
    move v5, v3

    .line 4169
    goto/16 :goto_0

    :pswitch_2
    move v5, v3

    .line 4172
    goto/16 :goto_0

    .line 4178
    .end local v1    # "atTheEndOfPlaylist":Z
    :cond_11
    add-int v6, p3, p4

    if-gtz v6, :cond_12

    move v0, v1

    .line 4179
    .local v0, "atTheBeginningOfPlaylist":Z
    :goto_3
    if-ltz v3, :cond_13

    move v2, v3

    .line 4181
    .local v2, "endPosition":I
    :goto_4
    iget v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    packed-switch v4, :pswitch_data_1

    .line 4203
    const-string v4, "LocalDevicePlayback"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown repeat mode: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/Throwable;

    invoke-direct {v7}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v4, v6, v7}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .end local v0    # "atTheBeginningOfPlaylist":Z
    .end local v2    # "endPosition":I
    :cond_12
    move v0, v4

    .line 4178
    goto :goto_3

    .line 4179
    .restart local v0    # "atTheBeginningOfPlaylist":Z
    :cond_13
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v4}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v4

    add-int v2, v3, v4

    goto :goto_4

    .line 4183
    .restart local v2    # "endPosition":I
    :pswitch_3
    if-ltz p3, :cond_15

    .line 4184
    if-eqz p1, :cond_14

    move v5, v3

    .line 4185
    goto/16 :goto_0

    :cond_14
    move v5, p3

    .line 4187
    goto/16 :goto_0

    .line 4190
    :cond_15
    const-string v4, "LocalDevicePlayback"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "called getNext in repeat current mode, but mPlayPos wasn\'t valid: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/Throwable;

    invoke-direct {v7}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v4, v6, v7}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :pswitch_4
    move v5, v2

    .line 4195
    goto/16 :goto_0

    .line 4197
    :pswitch_5
    if-eqz v0, :cond_17

    .line 4198
    if-eqz p1, :cond_16

    .end local v2    # "endPosition":I
    :goto_5
    move v5, v2

    goto/16 :goto_0

    .restart local v2    # "endPosition":I
    :cond_16
    move v2, v5

    goto :goto_5

    :cond_17
    move v5, v2

    .line 4200
    goto/16 :goto_0

    .line 4152
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 4181
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getNextSongs()[Lcom/google/android/music/download/ContentIdentifier;
    .locals 8

    .prologue
    .line 2376
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "music_playlist_prefetch_count"

    const/4 v7, 0x5

    invoke-static {v5, v6, v7}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 2382
    .local v4, "songCount":I
    iget-object v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    monitor-enter v6

    .line 2383
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v2

    .line 2384
    .local v2, "length":I
    if-lez v2, :cond_2

    .line 2385
    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v5

    new-array v3, v5, [Lcom/google/android/music/download/ContentIdentifier;

    .line 2386
    .local v3, "ret":[Lcom/google/android/music/download/ContentIdentifier;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v3

    if-ge v1, v5, :cond_1

    .line 2387
    iget-object v7, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    invoke-virtual {v5, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v7, v5}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->get(I)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v5

    aput-object v5, v3, v1

    .line 2388
    aget-object v5, v3, v1

    invoke-virtual {v5}, Lcom/google/android/music/download/ContentIdentifier;->getDomain()Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v5

    sget-object v7, Lcom/google/android/music/download/ContentIdentifier$Domain;->SHARED:Lcom/google/android/music/download/ContentIdentifier$Domain;

    if-ne v5, v7, :cond_0

    .line 2391
    const/4 v5, 0x0

    aput-object v5, v3, v1

    .line 2386
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2395
    :cond_1
    invoke-direct {p0, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->stripNullContentIdentifiers([Lcom/google/android/music/download/ContentIdentifier;)[Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v5

    monitor-exit v6

    .line 2414
    :goto_1
    return-object v5

    .line 2397
    .end local v1    # "i":I
    .end local v3    # "ret":[Lcom/google/android/music/download/ContentIdentifier;
    :cond_2
    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v5}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v5

    if-ge v5, v4, :cond_5

    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v5}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v2

    .line 2399
    :goto_2
    new-array v3, v2, [Lcom/google/android/music/download/ContentIdentifier;

    .line 2400
    .restart local v3    # "ret":[Lcom/google/android/music/download/ContentIdentifier;
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    .line 2401
    .local v0, "currentPos":I
    if-gez v0, :cond_3

    .line 2404
    const/4 v0, 0x0

    .line 2406
    :cond_3
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    if-ge v1, v2, :cond_6

    .line 2407
    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v5}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v5

    if-lt v0, v5, :cond_4

    .line 2408
    const/4 v0, 0x0

    .line 2410
    :cond_4
    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v5, v0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->get(I)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v5

    aput-object v5, v3, v1

    .line 2411
    add-int/lit8 v0, v0, 0x1

    .line 2406
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .end local v0    # "currentPos":I
    .end local v1    # "i":I
    .end local v3    # "ret":[Lcom/google/android/music/download/ContentIdentifier;
    :cond_5
    move v2, v4

    .line 2397
    goto :goto_2

    .line 2414
    .restart local v0    # "currentPos":I
    .restart local v1    # "i":I
    .restart local v3    # "ret":[Lcom/google/android/music/download/ContentIdentifier;
    :cond_6
    invoke-direct {p0, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->stripNullContentIdentifiers([Lcom/google/android/music/download/ContentIdentifier;)[Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v5

    monitor-exit v6

    goto :goto_1

    .line 2415
    .end local v0    # "currentPos":I
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "ret":[Lcom/google/android/music/download/ContentIdentifier;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method private getPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 1566
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Music"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private getPreviewPlayDurationForUrl(Ljava/lang/String;)J
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 4667
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPreviewPlaybackInfo:Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;

    .line 4668
    .local v0, "info":Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;
    if-eqz v0, :cond_0

    .line 4669
    iget-object v1, v0, Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;->mPreviewUrl:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;->mPreviewUrl:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4670
    iget-wide v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;->mPreviewDuration:J

    .line 4673
    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method private getPreviewPlayTypeForUrl(Ljava/lang/String;)I
    .locals 6
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 4654
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPreviewPlaybackInfo:Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;

    .line 4655
    .local v0, "info":Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;
    if-eqz v0, :cond_1

    .line 4656
    iget-object v1, v0, Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;->mPreviewUrl:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;->mPreviewUrl:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4657
    iget-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v1, :cond_0

    .line 4658
    const-string v1, "LocalDevicePlayback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Use preview duration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;->mPreviewDuration:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4660
    :cond_0
    iget v1, v0, Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;->mPreviewPlayType:I

    .line 4663
    :goto_0
    return v1

    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private getRandomPosition(Landroid/database/Cursor;)I
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 2824
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 2825
    .local v1, "size":I
    if-nez v1, :cond_0

    .line 2826
    const/4 v2, 0x0

    .line 2829
    :goto_0
    return v2

    .line 2828
    :cond_0
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 2829
    .local v0, "random":Ljava/util/Random;
    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    goto :goto_0
.end method

.method private getSyncMediaCursor(Lcom/google/android/music/medialist/SongList;Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;
    .locals 11
    .param p1, "mediaList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cols"    # [Ljava/lang/String;
    .param p4, "filter"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 5544
    invoke-virtual {p1, p2}, Lcom/google/android/music/medialist/SongList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v10

    .line 5545
    .local v10, "uri":Landroid/net/Uri;
    if-nez v10, :cond_0

    .line 5562
    :goto_0
    return-object v3

    .line 5549
    :cond_0
    invoke-virtual {v10}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    .line 5551
    .local v8, "builder":Landroid/net/Uri$Builder;
    const-string v0, "isInCloudQueueMode"

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->isInCloudQueueMode(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 5555
    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/music/medialist/SongList;->getShouldFilter()Z

    move-result v6

    invoke-virtual {p1}, Lcom/google/android/music/medialist/SongList;->mayIncludeExternalContent()Z

    move-result v7

    move-object v0, p2

    move-object v2, p3

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v9

    .line 5558
    .local v9, "cursor":Landroid/database/Cursor;
    if-nez v9, :cond_1

    .line 5559
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Null cursor when querying uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5562
    :cond_1
    new-instance v3, Lcom/google/android/music/medialist/MediaList$MediaCursor;

    invoke-direct {v3, v9}, Lcom/google/android/music/medialist/MediaList$MediaCursor;-><init>(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method private handleMediaButtonSeek(Z)V
    .locals 2
    .param p1, "force"    # Z

    .prologue
    .line 3940
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPendingMediaButtonSeekCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-nez v0, :cond_1

    .line 3941
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v0, :cond_0

    .line 3942
    const-string v0, "LocalDevicePlayback"

    const-string v1, "No more pending media button events."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 4022
    :cond_0
    :goto_0
    return-void

    .line 3950
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 3951
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$32;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback$32;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private hasWiFiConnection()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 5381
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v3}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;

    move-result-object v1

    .line 5382
    .local v1, "monitor":Lcom/google/android/music/net/INetworkMonitor;
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/android/music/net/INetworkMonitor;->hasWifiConnection()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 5386
    .end local v1    # "monitor":Lcom/google/android/music/net/INetworkMonitor;
    :cond_0
    :goto_0
    return v2

    .line 5383
    :catch_0
    move-exception v0

    .line 5384
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "LocalDevicePlayback"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private invalidateNextPlayer()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3400
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3401
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/music/playback/AsyncMediaPlayer;->setNextPlayer(Lcom/google/android/music/playback/AsyncMediaPlayer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3405
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-eqz v0, :cond_2

    .line 3406
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

    if-eqz v0, :cond_1

    .line 3407
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingClient;->cancelNextStream()V

    .line 3409
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->release()V

    .line 3410
    iput-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    .line 3411
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayPos:I

    .line 3412
    iput-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextAudioId:Lcom/google/android/music/download/ContentIdentifier;

    .line 3414
    :cond_2
    return-void

    .line 3403
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private isDownloadedOnlyMode()Z
    .locals 4

    .prologue
    .line 5458
    const/4 v0, 0x0

    .line 5459
    .local v0, "isDownloadedOnly":Z
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 5460
    .local v1, "prefOwner":Ljava/lang/Object;
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    .line 5463
    .local v2, "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 5465
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 5467
    return v0

    .line 5465
    :catchall_0
    move-exception v3

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v3
.end method

.method private isGaplessEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 3385
    sget-object v1, Lcom/google/android/music/playback/LocalDevicePlayback;->mDisableGaplessOverride:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/music/playback/LocalDevicePlayback;->mDisableGaplessOverride:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/music/playback/LocalDevicePlayback;->mDisableGaplessOverride:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_gapless_enabled"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isInCloudQueueMode(Landroid/content/Context;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 5513
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 5514
    .local v3, "resolver":Landroid/content/ContentResolver;
    const-string v5, "music_enable_chromecast_cloud_queue"

    invoke-static {v3, v5, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 5518
    .local v0, "isCastCloudQueueEnabled":Z
    const-string v5, "music_enable_dial_cloud_queue"

    invoke-static {v3, v5, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    .line 5522
    .local v1, "isDialCloudQueueEnabled":Z
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mConnectedMediaRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 5524
    .local v4, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    if-nez v4, :cond_1

    .line 5525
    iget-boolean v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v5, :cond_0

    .line 5526
    const-string v5, "LocalDevicePlayback"

    const-string v6, "Not in Cloud Queue mode because no route is selected."

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5539
    :cond_0
    :goto_0
    return v2

    .line 5531
    :cond_1
    if-eqz v0, :cond_2

    invoke-static {v4}, Lcom/google/android/music/cast/CastUtils;->isCastV2Route(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    if-eqz v1, :cond_4

    invoke-static {v4}, Lcom/google/android/music/cast/CastUtils;->isDialRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    const/4 v2, 0x1

    .line 5535
    .local v2, "isInCloudQueueMode":Z
    :cond_4
    iget-boolean v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v5, :cond_0

    .line 5536
    const-string v5, "LocalDevicePlayback"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Route is selected, are we in Cloud Queue Mode? "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isPlayQueue(Lcom/google/android/music/medialist/SongList;)Z
    .locals 1
    .param p1, "list"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 2724
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/google/android/music/medialist/CaqPlayQueueSongList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isQueueable(Lcom/google/android/music/medialist/SongList;)Z
    .locals 2
    .param p0, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 2739
    sget-object v0, Lcom/google/android/music/playback/LocalDevicePlayback$37;->$SwitchMap$com$google$android$music$download$ContentIdentifier$Domain:[I

    invoke-virtual {p0}, Lcom/google/android/music/medialist/SongList;->getDomain()Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/download/ContentIdentifier$Domain;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2744
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2742
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 2739
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private isRadio(Lcom/google/android/music/medialist/SongList;)Z
    .locals 2
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 2327
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/music/medialist/SongList;->getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 2328
    .local v0, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 2329
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor$Type;->isRadio()Z

    move-result v1

    goto :goto_0
.end method

.method private isSuggestedMix(Lcom/google/android/music/medialist/SongList;)Z
    .locals 4
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 2315
    const/4 v1, 0x0

    .line 2316
    .local v1, "result":Z
    instance-of v2, p1, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 2317
    check-cast v0, Lcom/google/android/music/medialist/PlaylistSongList;

    .line 2318
    .local v0, "playlistSongList":Lcom/google/android/music/medialist/PlaylistSongList;
    invoke-virtual {v0}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistType()I

    move-result v2

    const/16 v3, 0x32

    if-ne v2, v3, :cond_0

    .line 2320
    const/4 v1, 0x1

    .line 2323
    .end local v0    # "playlistSongList":Lcom/google/android/music/medialist/PlaylistSongList;
    :cond_0
    return v1
.end method

.method private isUsingPlayQueue()Z
    .locals 1

    .prologue
    .line 2728
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayQueue(Lcom/google/android/music/medialist/SongList;)Z

    move-result v0

    return v0
.end method

.method private loadCurrent()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 3045
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->isValid()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    .line 3059
    :goto_0
    return v1

    .line 3049
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    iget v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-virtual {v1, v3}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->getAudioIdAndListItemId(I)Landroid/util/Pair;

    move-result-object v0

    .line 3051
    .local v0, "audioIdAndListItemId":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/Long;>;"
    if-nez v0, :cond_1

    move v1, v2

    .line 3052
    goto :goto_0

    .line 3055
    :cond_1
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/music/download/ContentIdentifier;

    invoke-direct {p0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->refreshCursor(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3056
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J

    .line 3057
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    move v1, v2

    .line 3059
    goto :goto_0
.end method

.method private loadFullMusicFile(Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/store/MusicFile;
    .locals 6
    .param p1, "partialMusicFile"    # Lcom/google/android/music/store/MusicFile;

    .prologue
    .line 5477
    new-instance v2, Lcom/google/android/music/store/MusicFile;

    invoke-direct {v2}, Lcom/google/android/music/store/MusicFile;-><init>()V

    .line 5478
    .local v2, "fullMusicFile":Lcom/google/android/music/store/MusicFile;
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    .line 5479
    .local v3, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 5482
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/music/store/MusicFile;->getLocalId()J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/android/music/store/MusicFile;->load(Landroid/database/sqlite/SQLiteDatabase;J)V
    :try_end_0
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5486
    invoke-virtual {v3, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 5489
    .end local v2    # "fullMusicFile":Lcom/google/android/music/store/MusicFile;
    :goto_0
    return-object v2

    .line 5483
    .restart local v2    # "fullMusicFile":Lcom/google/android/music/store/MusicFile;
    :catch_0
    move-exception v1

    .line 5484
    .local v1, "e":Lcom/google/android/music/store/DataNotFoundException;
    const/4 v2, 0x0

    .line 5486
    .end local v2    # "fullMusicFile":Lcom/google/android/music/store/MusicFile;
    invoke-virtual {v3, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .end local v1    # "e":Lcom/google/android/music/store/DataNotFoundException;
    .restart local v2    # "fullMusicFile":Lcom/google/android/music/store/MusicFile;
    :catchall_0
    move-exception v4

    invoke-virtual {v3, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v4
.end method

.method private logTrackStartEvent(Z)V
    .locals 4
    .param p1, "fromUserAction"    # Z

    .prologue
    .line 3614
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    if-eqz v1, :cond_1

    .line 3615
    const/4 v0, 0x1

    .line 3616
    .local v0, "eventType":I
    if-eqz p1, :cond_0

    .line 3617
    const/4 v0, 0x2

    .line 3619
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/music/store/Store;->storeEvent(Lcom/google/android/music/download/ContentIdentifier;ILcom/google/android/music/store/ContainerDescriptor;)V

    .line 3621
    .end local v0    # "eventType":I
    :cond_1
    return-void
.end method

.method private next(Z)V
    .locals 3
    .param p1, "force"    # Z

    .prologue
    .line 3933
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPendingMediaButtonSeekCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 3934
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "next: force="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", currentPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pendingMediaButtonSeekCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPendingMediaButtonSeekCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3936
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->handleMediaButtonSeek(Z)V

    .line 3937
    return-void
.end method

.method private nextImplSync(ZI)V
    .locals 6
    .param p1, "force"    # Z
    .param p2, "nextPlayPos"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4061
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 4065
    iput-boolean v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    .line 4066
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onPlaybackComplete()V

    .line 4067
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onPlayStateChanged()V

    .line 4086
    :goto_0
    return-void

    .line 4073
    :cond_0
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    if-ltz v0, :cond_1

    .line 4074
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mHistory:Ljava/util/LinkedList;

    iget v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 4075
    :goto_1
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mHistory:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0x14

    if-le v0, v1, :cond_1

    .line 4076
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mHistory:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    goto :goto_1

    .line 4080
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->saveBookmarkIfNeeded()V

    .line 4081
    invoke-direct {p0, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->stop(Z)V

    .line 4084
    iput p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    .line 4085
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndPrepareToPlaySync(ZZZZLcom/google/android/music/download/ContentIdentifier;)V

    goto :goto_0
.end method

.method private onOpenStarted()V
    .locals 1

    .prologue
    .line 4805
    const-string v0, "com.android.music.asyncopenstart"

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V

    .line 4806
    return-void
.end method

.method private onPlaybackFailure()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4784
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

    if-eqz v0, :cond_0

    .line 4785
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingClient;->currenStreamingPlayEnded()V

    .line 4787
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    .line 4788
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyFailure()V

    .line 4789
    invoke-direct {p0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->saveQueue(Z)V

    .line 4790
    return-void
.end method

.method private onQueueChanged()V
    .locals 1

    .prologue
    .line 4793
    const-string v0, "com.android.music.queuechanged"

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V

    .line 4794
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->playlistLoading()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4795
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->saveQueue(Z)V

    .line 4797
    :cond_0
    return-void
.end method

.method private onSongChanged()V
    .locals 1

    .prologue
    .line 4800
    const-string v0, "com.android.music.metachanged"

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V

    .line 4801
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->saveQueue(Z)V

    .line 4802
    return-void
.end method

.method private open(Lcom/google/android/music/download/ContentIdentifier;ZZJLcom/google/android/music/store/MusicFile;)V
    .locals 18
    .param p1, "songId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "fromUserAction"    # Z
    .param p3, "playOnSuccess"    # Z
    .param p4, "position"    # J
    .param p6, "preferredMusicFile"    # Lcom/google/android/music/store/MusicFile;

    .prologue
    .line 3558
    const-string v6, "LocalDevicePlayback"

    const-string v7, "open: id=%s, pos=%s, playPos=%s, fromUser=%s, track=%s"

    const/4 v4, 0x5

    new-array v8, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v8, v4

    const/4 v4, 0x1

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v4, 0x3

    invoke-static/range {p2 .. p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v9, 0x4

    if-eqz p6, :cond_1

    invoke-virtual/range {p6 .. p6}, Lcom/google/android/music/store/MusicFile;->getTitle()Ljava/lang/String;

    move-result-object v4

    :goto_0
    aput-object v4, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3562
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->updateDescriptorAndQueue(Z)V

    .line 3565
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-object/from16 v16, v0

    .line 3566
    .local v16, "oldPlayer":Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-interface/range {v16 .. v16}, Lcom/google/android/music/playback/AsyncMediaPlayer;->getAudioSessionId()I

    move-result v15

    .line 3567
    .local v15, "fxsession":I
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->invalidateNextPlayer()V

    .line 3568
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->createPlayer()Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    .line 3569
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v4, v15}, Lcom/google/android/music/playback/AsyncMediaPlayer;->setAudioSessionId(I)V

    .line 3570
    invoke-interface/range {v16 .. v16}, Lcom/google/android/music/playback/AsyncMediaPlayer;->release()V

    .line 3576
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->logTrackStartEvent(Z)V

    .line 3578
    move-object/from16 v5, p1

    .line 3579
    .local v5, "preferredSongId":Lcom/google/android/music/download/ContentIdentifier;
    if-eqz p6, :cond_0

    .line 3580
    new-instance v5, Lcom/google/android/music/download/ContentIdentifier;

    .end local v5    # "preferredSongId":Lcom/google/android/music/download/ContentIdentifier;
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/music/store/MusicFile;->getLocalId()J

    move-result-wide v6

    invoke-virtual/range {p6 .. p6}, Lcom/google/android/music/store/MusicFile;->getDomain()Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v4

    invoke-direct {v5, v6, v7, v4}, Lcom/google/android/music/download/ContentIdentifier;-><init>(JLcom/google/android/music/download/ContentIdentifier$Domain;)V

    .line 3583
    .restart local v5    # "preferredSongId":Lcom/google/android/music/download/ContentIdentifier;
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->duration()J

    move-result-wide v8

    const/4 v10, 0x1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    new-instance v14, Lcom/google/android/music/playback/LocalDevicePlayback$27;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    invoke-direct {v14, v0, v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback$27;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/ContentIdentifier;Z)V

    move-wide/from16 v6, p4

    move/from16 v11, p2

    move-object/from16 v12, p6

    invoke-interface/range {v4 .. v14}, Lcom/google/android/music/playback/AsyncMediaPlayer;->setDataSource(Lcom/google/android/music/download/ContentIdentifier;JJZZLcom/google/android/music/store/MusicFile;Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;)V

    .line 3611
    return-void

    .line 3558
    .end local v5    # "preferredSongId":Lcom/google/android/music/download/ContentIdentifier;
    .end local v15    # "fxsession":I
    .end local v16    # "oldPlayer":Lcom/google/android/music/playback/AsyncMediaPlayer;
    :cond_1
    const-string v4, "external"

    goto :goto_0
.end method

.method private open(Lcom/google/android/music/medialist/SongList;ZIZ)V
    .locals 7
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "shuffleList"    # Z
    .param p3, "position"    # I
    .param p4, "play"    # Z

    .prologue
    .line 2441
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v0, :cond_0

    .line 2442
    const-string v0, "LocalDevicePlayback"

    const-string v1, "open: medialist=%s position=%d play=%b "

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2445
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    .line 2446
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserExplicitPlayTime:J

    .line 2448
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 2449
    sget-object v6, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$22;

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move v4, p2

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/playback/LocalDevicePlayback$22;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;IZZ)V

    invoke-static {v6, v0}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 2558
    return-void
.end method

.method private openCurrentAndNext(ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    .locals 19
    .param p1, "fromUserAction"    # Z
    .param p2, "playOnSuccess"    # Z
    .param p3, "willPlay"    # Z
    .param p4, "reverse"    # Z
    .param p5, "positionMs"    # J
    .param p7, "audioIdWhenRadioRequested"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    .line 3137
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v4, :cond_0

    .line 3138
    const-string v4, "LocalDevicePlayback"

    const-string v5, "openCurrentAndNext"

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3141
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 3143
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->resetCurrentSongMetaDataCursor()V

    .line 3145
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v4}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->isValid()Z

    move-result v4

    if-nez v4, :cond_2

    .line 3146
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->stop(Z)V

    .line 3147
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onOpenComplete()V

    .line 3228
    :cond_1
    :goto_0
    return-void

    .line 3151
    :cond_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p4

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->findPlayableSong(IZZ)Landroid/util/Pair;

    move-result-object v15

    .line 3154
    .local v15, "playPosAndMusicFile":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Lcom/google/android/music/store/MusicFile;>;"
    if-nez v15, :cond_3

    .line 3155
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->stop(Z)V

    .line 3157
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    .line 3158
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onPlaybackComplete()V

    .line 3159
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onPlayStateChanged()V

    goto :goto_0

    .line 3163
    :cond_3
    iget-object v4, v15, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    .line 3164
    iget-object v14, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v14, Lcom/google/android/music/store/MusicFile;

    .line 3166
    .local v14, "preferredMusicFile":Lcom/google/android/music/store/MusicFile;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->loadCurrent()Z

    move-result v4

    if-nez v4, :cond_4

    .line 3167
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onOpenComplete()V

    goto :goto_0

    .line 3171
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    move-object/from16 v0, p7

    invoke-virtual {v4, v0}, Lcom/google/android/music/download/ContentIdentifier;->equals(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v4

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    if-eqz v4, :cond_6

    .line 3225
    :cond_5
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z

    move-result v4

    if-nez v4, :cond_1

    .line 3226
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->setNextTrack()V

    goto :goto_0

    .line 3174
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayingLocally()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 3176
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v4}, Lcom/google/android/music/playback/AsyncMediaPlayer;->stop()V

    .line 3178
    :cond_7
    if-nez p3, :cond_8

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    if-eqz v4, :cond_8

    .line 3179
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    .line 3180
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onPlayStateChanged()V

    .line 3182
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 3183
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-eqz v4, :cond_9

    .line 3184
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v4}, Lcom/google/android/music/playback/AsyncMediaPlayer;->stop()V

    .line 3185
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->releasePlayers()V

    .line 3187
    :cond_9
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->updateDescriptorAndQueue(Z)V

    .line 3188
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/music/playback/LocalDevicePlayback;->loadFullMusicFile(Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/store/MusicFile;

    move-result-object v6

    .line 3189
    .local v6, "fullMusicFile":Lcom/google/android/music/store/MusicFile;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v18

    .line 3190
    .local v18, "store":Lcom/google/android/music/store/Store;
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Lcom/google/android/music/store/Store;->getCloudQueueItemIdForQueueItem(J)Ljava/lang/String;

    move-result-object v7

    .line 3192
    .local v7, "cloudQueueItemId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    invoke-interface {v4}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->isQueueLoaded()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 3193
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v4, :cond_a

    .line 3194
    const-string v4, "LocalDevicePlayback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Calling skipToItem with item ID: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3197
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/store/Store;->getCurrentCloudQueueVersion()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    move-wide/from16 v8, p5

    move/from16 v10, p2

    invoke-interface/range {v4 .. v10}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->skipToItem(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V

    .line 3218
    .end local v6    # "fullMusicFile":Lcom/google/android/music/store/MusicFile;
    .end local v7    # "cloudQueueItemId":Ljava/lang/String;
    .end local v18    # "store":Lcom/google/android/music/store/Store;
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPodcast()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 3219
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getBookmark()J

    move-result-wide v16

    .line 3222
    .local v16, "bookmark":J
    const-wide/16 v4, 0x1388

    sub-long v4, v16, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->seek(J)J

    goto/16 :goto_1

    .line 3204
    .end local v16    # "bookmark":J
    .restart local v6    # "fullMusicFile":Lcom/google/android/music/store/MusicFile;
    .restart local v7    # "cloudQueueItemId":Ljava/lang/String;
    .restart local v18    # "store":Lcom/google/android/music/store/Store;
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v4, :cond_c

    .line 3205
    const-string v4, "LocalDevicePlayback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Calling loadCloudQueue with base item ID: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3208
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/cast/CastUtils;->getCloudQueueUrlForReceiver(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    move-wide/from16 v8, p5

    move/from16 v10, p2

    invoke-interface/range {v4 .. v10}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->loadCloudQueue(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V

    goto :goto_2

    .line 3216
    .end local v6    # "fullMusicFile":Lcom/google/android/music/store/MusicFile;
    .end local v7    # "cloudQueueItemId":Ljava/lang/String;
    .end local v18    # "store":Lcom/google/android/music/store/Store;
    :cond_d
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    move-object/from16 v8, p0

    move/from16 v10, p1

    move/from16 v11, p2

    move-wide/from16 v12, p5

    invoke-direct/range {v8 .. v14}, Lcom/google/android/music/playback/LocalDevicePlayback;->open(Lcom/google/android/music/download/ContentIdentifier;ZZJLcom/google/android/music/store/MusicFile;)V

    goto :goto_2
.end method

.method private openCurrentAndPlay(Z)V
    .locals 2
    .param p1, "fromUserAction"    # Z

    .prologue
    .line 2681
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 2682
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$25;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback$25;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 2688
    return-void
.end method

.method private openCurrentAndPrepareToPlaySync(ZZZZLcom/google/android/music/download/ContentIdentifier;)V
    .locals 9
    .param p1, "fromUserAction"    # Z
    .param p2, "asyncWakeLockAquired"    # Z
    .param p3, "startPlaying"    # Z
    .param p4, "reverse"    # Z
    .param p5, "audioIdWhenRadioRequested"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    const/4 v0, 0x0

    .line 2694
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v1

    if-nez v1, :cond_1

    .line 2717
    if-eqz p2, :cond_0

    .line 2718
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2721
    :cond_0
    :goto_0
    return-void

    .line 2698
    :cond_1
    :try_start_1
    iget v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    if-gez v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 2700
    .local v0, "autoSelect":Z
    :cond_3
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->shouldPlayInRandomOrder()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2701
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2702
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->fillShuffleList()V

    .line 2703
    if-eqz v0, :cond_5

    .line 2704
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-nez v1, :cond_4

    .line 2705
    const-string v1, "LocalDevicePlayback"

    const-string v3, "Failed to fill future in openCurrentAndPlaySync()"

    invoke-static {v1, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2706
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2717
    if-eqz p2, :cond_0

    .line 2718
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 2708
    :cond_4
    :try_start_3
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    .line 2710
    :cond_5
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2714
    :cond_6
    :goto_1
    const-wide/16 v6, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p3

    move v4, p3

    move v5, p4

    move-object v8, p5

    :try_start_4
    invoke-direct/range {v1 .. v8}, Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndNext(ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2717
    if-eqz p2, :cond_0

    .line 2718
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 2710
    :catchall_0
    move-exception v1

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2717
    .end local v0    # "autoSelect":Z
    :catchall_1
    move-exception v1

    if-eqz p2, :cond_7

    .line 2718
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_7
    throw v1

    .line 2711
    .restart local v0    # "autoSelect":Z
    :cond_8
    if-eqz v0, :cond_6

    .line 2712
    const/4 v1, 0x0

    :try_start_7
    iput v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1
.end method

.method private pause(Z)V
    .locals 3
    .param p1, "transientPause"    # Z

    .prologue
    .line 3820
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pause: transient="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", currentPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3822
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 3823
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$31;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback$31;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 3833
    return-void
.end method

.method private pauseSync(Z)V
    .locals 5
    .param p1, "transientPause"    # Z

    .prologue
    const/4 v4, 0x0

    .line 3836
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getAudioId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v0

    .line 3837
    .local v0, "songId":Lcom/google/android/music/download/ContentIdentifier;
    if-eqz v0, :cond_0

    .line 3838
    const v1, 0x12112

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v4}, Lcom/google/android/music/playback/AsyncMediaPlayer;->getRemoteSongId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 3840
    const-string v1, "LocalDevicePlayback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Event logging MUSIC_PAUSE_PLAYBACK_REQUESTED: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v3}, Lcom/google/android/music/playback/AsyncMediaPlayer;->getRemoteSongId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3844
    :cond_0
    monitor-enter p0

    .line 3845
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 3846
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 3847
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3848
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3849
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    invoke-interface {v1}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->pause()V

    .line 3853
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    .line 3854
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onPlayStateChanged()V

    .line 3855
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->saveBookmarkIfNeeded()V

    .line 3856
    iput-boolean p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPausedByTransientLossOfFocus:Z

    .line 3866
    :cond_1
    :goto_1
    monitor-exit p0

    .line 3867
    return-void

    .line 3851
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v1}, Lcom/google/android/music/playback/AsyncMediaPlayer;->pause()V

    goto :goto_0

    .line 3866
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 3857
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPreparing()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 3859
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->stopSync()V

    goto :goto_1

    .line 3861
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPausedByTransientLossOfFocus:Z

    if-eqz v1, :cond_1

    if-nez p1, :cond_1

    .line 3863
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPausedByTransientLossOfFocus:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private playNextSync(Lcom/google/android/music/medialist/SongList;I)V
    .locals 17
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "queuePlayPos"    # I

    .prologue
    .line 2833
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v6, :cond_0

    .line 2834
    const-string v6, "LocalDevicePlayback"

    const-string v9, "playNextSync: songList=%s fromPlayPos=%s mListItemId=%s"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v10, v11

    const/4 v11, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2837
    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->isQueueable(Lcom/google/android/music/medialist/SongList;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2838
    const-string v6, "LocalDevicePlayback"

    const-string v9, "Play next not applicable - bailing"

    invoke-static {v6, v9}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 2909
    :goto_0
    return-void

    .line 2842
    :cond_1
    move-object/from16 v0, p1

    instance-of v6, v0, Lcom/google/android/music/medialist/ExternalSongList;

    if-eqz v6, :cond_2

    .line 2843
    check-cast p1, Lcom/google/android/music/medialist/ExternalSongList;

    .end local p1    # "songList":Lcom/google/android/music/medialist/SongList;
    invoke-direct/range {p0 .. p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->addToDatabase(Lcom/google/android/music/medialist/ExternalSongList;)Lcom/google/android/music/medialist/SongList;

    move-result-object p1

    .line 2844
    .restart local p1    # "songList":Lcom/google/android/music/medialist/SongList;
    if-nez p1, :cond_2

    .line 2845
    const-string v6, "LocalDevicePlayback"

    const-string v9, "Play next failed to add tracks to the database"

    invoke-static {v6, v9}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2850
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "audio_id"

    aput-object v11, v9, v10

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v9, v10}, Lcom/google/android/music/playback/LocalDevicePlayback;->getSyncMediaCursor(Lcom/google/android/music/medialist/SongList;Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;

    move-result-object v4

    .line 2856
    .local v4, "cursorToAppend":Landroid/database/Cursor;
    if-nez v4, :cond_3

    .line 2857
    const-string v6, "LocalDevicePlayback"

    const-string v9, "Play next failed to get tracks"

    invoke-static {v6, v9}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2862
    :cond_3
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v2

    .line 2863
    .local v2, "store":Lcom/google/android/music/store/Store;
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    const/4 v9, 0x1

    if-ne v6, v9, :cond_7

    const/4 v5, 0x1

    .line 2864
    .local v5, "shuffleContainer":Z
    :goto_1
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    const/4 v9, -0x1

    if-ne v6, v9, :cond_4

    .line 2865
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    const/4 v9, 0x1

    if-ne v6, v9, :cond_8

    .line 2866
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->getRandomPosition(Landroid/database/Cursor;)I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    .line 2872
    :cond_4
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/music/medialist/SongList;->getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v3

    .line 2873
    .local v3, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/music/playback/LocalDevicePlayback;->isInCloudQueueMode(Landroid/content/Context;)Z

    move-result v7

    .line 2874
    .local v7, "isInCloudQueueMode":Z
    const/4 v6, -0x1

    move/from16 v0, p2

    if-eq v0, v6, :cond_9

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    move/from16 v0, p2

    if-eq v0, v6, :cond_9

    .line 2875
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    sget-object v9, Lcom/google/android/music/store/QueueUtils$MoveMode;->AFTER:Lcom/google/android/music/store/QueueUtils$MoveMode;

    move/from16 v0, p2

    invoke-virtual {v2, v0, v6, v9, v7}, Lcom/google/android/music/store/Store;->caqMoveItem(IILcom/google/android/music/store/QueueUtils$MoveMode;Z)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2877
    if-eqz v7, :cond_5

    .line 2878
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v6

    move/from16 v0, p2

    int-to-long v10, v0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    int-to-long v12, v9

    invoke-static {v6, v10, v11, v12, v13}, Lcom/google/android/music/cloudclient/CloudQueueManager;->updateCloudQueueVersionForUpdatedItems(Landroid/content/Context;JJ)V

    .line 2881
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/music/store/QueueUtils;->notifyChange(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2907
    :cond_6
    :goto_3
    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 2863
    .end local v3    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v5    # "shuffleContainer":Z
    .end local v7    # "isInCloudQueueMode":Z
    :cond_7
    const/4 v5, 0x0

    goto :goto_1

    .line 2868
    .restart local v5    # "shuffleContainer":Z
    :cond_8
    const/4 v6, 0x0

    :try_start_1
    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 2907
    .end local v2    # "store":Lcom/google/android/music/store/Store;
    .end local v5    # "shuffleContainer":Z
    :catchall_0
    move-exception v6

    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v6

    .line 2884
    .restart local v2    # "store":Lcom/google/android/music/store/Store;
    .restart local v3    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .restart local v5    # "shuffleContainer":Z
    .restart local v7    # "isInCloudQueueMode":Z
    :cond_9
    :try_start_2
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/music/store/Store;->caqPlayNext(Lcom/google/android/music/store/ContainerDescriptor;Landroid/database/Cursor;ZIZ)Lcom/google/android/music/store/PlayQueueAddResult;

    move-result-object v8

    .line 2888
    .local v8, "result":Lcom/google/android/music/store/PlayQueueAddResult;
    if-eqz v7, :cond_a

    invoke-virtual {v8}, Lcom/google/android/music/store/PlayQueueAddResult;->getAddedSize()I

    move-result v6

    if-lez v6, :cond_a

    .line 2889
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/music/cloudclient/CloudQueueManager;->updateCloudQueue(Landroid/content/Context;)V

    .line 2892
    :cond_a
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    if-nez v6, :cond_b

    invoke-virtual {v8}, Lcom/google/android/music/store/PlayQueueAddResult;->getAddedSize()I

    move-result v6

    if-lez v6, :cond_b

    .line 2894
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->createPlayQueueSongList()Lcom/google/android/music/medialist/SongList;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/music/playback/LocalDevicePlayback;->setMediaList(Lcom/google/android/music/medialist/SongList;)V

    .line 2895
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v6, v9}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->setMediaList(Lcom/google/android/music/medialist/SongList;)V

    .line 2896
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    .line 2897
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onQueueChanged()V

    .line 2898
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/music/store/QueueUtils;->notifyChange(Landroid/content/Context;)V

    .line 2899
    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    move-object/from16 v9, p0

    invoke-direct/range {v9 .. v16}, Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndNext(ZZZZJLcom/google/android/music/download/ContentIdentifier;)V

    goto :goto_3

    .line 2901
    :cond_b
    invoke-virtual {v8}, Lcom/google/android/music/store/PlayQueueAddResult;->getAddedSize()I

    move-result v6

    if-lez v6, :cond_6

    .line 2902
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/music/store/QueueUtils;->notifyChange(Landroid/content/Context;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method private queue(Lcom/google/android/music/medialist/SongList;IJ)Lcom/google/android/music/store/PlayQueueAddResult;
    .locals 15
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "state"    # I
    .param p3, "currentListItemId"    # J

    .prologue
    .line 2921
    iget-boolean v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v5, :cond_0

    .line 2922
    const-string v5, "LocalDevicePlayback"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "queue: currentListItemId="

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p3

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2924
    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->isQueueable(Lcom/google/android/music/medialist/SongList;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2925
    const/4 v9, 0x0

    .line 2969
    :goto_0
    return-object v9

    .line 2928
    :cond_1
    move-object/from16 v0, p1

    instance-of v5, v0, Lcom/google/android/music/medialist/ExternalSongList;

    if-eqz v5, :cond_2

    .line 2929
    check-cast p1, Lcom/google/android/music/medialist/ExternalSongList;

    .end local p1    # "medialist":Lcom/google/android/music/medialist/SongList;
    invoke-direct/range {p0 .. p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->addToDatabase(Lcom/google/android/music/medialist/ExternalSongList;)Lcom/google/android/music/medialist/SongList;

    move-result-object p1

    .line 2930
    .restart local p1    # "medialist":Lcom/google/android/music/medialist/SongList;
    if-nez p1, :cond_2

    .line 2931
    const/4 v9, 0x0

    goto :goto_0

    .line 2935
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v12, 0x0

    const-string v13, "audio_id"

    aput-object v13, v6, v12

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v5, v6, v12}, Lcom/google/android/music/playback/LocalDevicePlayback;->getSyncMediaCursor(Lcom/google/android/music/medialist/SongList;Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;

    move-result-object v4

    .line 2940
    .local v4, "cursorToAppend":Landroid/database/Cursor;
    if-nez v4, :cond_3

    .line 2941
    const/4 v9, 0x0

    goto :goto_0

    .line 2945
    :cond_3
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v2

    .line 2946
    .local v2, "store":Lcom/google/android/music/store/Store;
    iget v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_6

    const/4 v7, 0x1

    .line 2948
    .local v7, "sprinkle":Z
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->isInCloudQueueMode(Landroid/content/Context;)Z

    move-result v8

    .line 2949
    .local v8, "isInCloudQueueMode":Z
    invoke-virtual {v2}, Lcom/google/android/music/store/Store;->getCurrentCloudQueueVersion()J

    move-result-wide v10

    .line 2951
    .local v10, "version":J
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/music/medialist/SongList;->getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v3

    .line 2952
    .local v3, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    iget v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    move/from16 v5, p2

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/music/store/Store;->caqQueue(Lcom/google/android/music/store/ContainerDescriptor;Landroid/database/Cursor;IIZZ)Lcom/google/android/music/store/PlayQueueAddResult;

    move-result-object v9

    .line 2955
    .local v9, "addResult":Lcom/google/android/music/store/PlayQueueAddResult;
    if-eqz v9, :cond_5

    invoke-virtual {v9}, Lcom/google/android/music/store/PlayQueueAddResult;->getAddedSize()I

    move-result v5

    if-lez v5, :cond_5

    .line 2956
    if-eqz v8, :cond_4

    .line 2957
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/cloudclient/CloudQueueManager;->updateCloudQueue(Landroid/content/Context;)V

    .line 2960
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v9}, Lcom/google/android/music/store/PlayQueueAddResult;->getAddedSize()I

    move-result v6

    invoke-virtual {v9}, Lcom/google/android/music/store/PlayQueueAddResult;->getRequestedSize()I

    move-result v12

    const/16 v13, 0x3e8

    invoke-static {v5, v6, v12, v13}, Lcom/google/android/music/utils/MusicUtils;->showSongsToAddToQueue(Landroid/content/Context;III)V

    .line 2964
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/store/QueueUtils;->notifyChange(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2969
    :cond_5
    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .line 2946
    .end local v3    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v7    # "sprinkle":Z
    .end local v8    # "isInCloudQueueMode":Z
    .end local v9    # "addResult":Lcom/google/android/music/store/PlayQueueAddResult;
    .end local v10    # "version":J
    :cond_6
    const/4 v7, 0x0

    goto :goto_1

    .line 2969
    .end local v2    # "store":Lcom/google/android/music/store/Store;
    :catchall_0
    move-exception v5

    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v5
.end method

.method private refreshCursor(Lcom/google/android/music/download/ContentIdentifier;)Z
    .locals 6
    .param p1, "songId"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    const/4 v1, 0x0

    .line 3065
    if-nez p1, :cond_1

    .line 3066
    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 3067
    const-string v2, "LocalDevicePlayback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid audio identifier:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 3093
    :cond_0
    :goto_0
    return v1

    .line 3071
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    if-eqz v2, :cond_0

    .line 3075
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCursorCols:[Ljava/lang/String;

    invoke-virtual {v2, v3, p1, v4}, Lcom/google/android/music/medialist/SongList;->getSongCursor(Landroid/content/Context;Lcom/google/android/music/download/ContentIdentifier;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 3076
    .local v0, "cursor":Landroid/database/Cursor;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Queried ID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 3078
    const/4 v1, 0x0

    .line 3079
    .local v1, "success":Z
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v3

    .line 3080
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    invoke-static {v2}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 3081
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 3083
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3084
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 3085
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    .line 3086
    const/4 v1, 0x1

    .line 3091
    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3092
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onSongChanged()V

    goto :goto_0

    .line 3088
    :cond_2
    :try_start_1
    const-string v2, "LocalDevicePlayback"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "refreshCursor("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") failed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3089
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_1

    .line 3091
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private final registerExternalStorageListener()V
    .locals 3

    .prologue
    .line 1821
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 1822
    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$18;

    invoke-direct {v1, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$18;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    iput-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    .line 1844
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1845
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1846
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1847
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1848
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1850
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private releasePlayers()V
    .locals 1

    .prologue
    .line 3420
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-eqz v0, :cond_0

    .line 3421
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->release()V

    .line 3424
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

    if-eqz v0, :cond_1

    .line 3425
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingClient;->cancelNextStream()V

    .line 3427
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-eqz v0, :cond_2

    .line 3428
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->release()V

    .line 3430
    :cond_2
    return-void
.end method

.method private releaseWiFiLock()V
    .locals 3

    .prologue
    .line 5407
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWiFiManagerLock:Ljava/lang/Object;

    monitor-enter v1

    .line 5408
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_0

    .line 5409
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 5410
    const-string v0, "LocalDevicePlayback"

    const-string v2, "releaseWiFiLock"

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 5411
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 5413
    :cond_0
    monitor-exit v1

    .line 5414
    return-void

    .line 5413
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private reloadQueue()V
    .locals 3

    .prologue
    .line 2193
    const/4 v1, 0x1

    :try_start_0
    invoke-direct {p0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->reloadQueue(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2198
    :goto_0
    return-void

    .line 2194
    :catch_0
    move-exception v0

    .line 2195
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "LocalDevicePlayback"

    const-string v2, "Failed to load the queue, resetting"

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2196
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->resetQueue()V

    goto :goto_0
.end method

.method private reloadQueue(Z)V
    .locals 38
    .param p1, "stopPlayback"    # Z

    .prologue
    .line 2004
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertNotMainThread()V

    .line 2005
    const-wide/16 v36, -0x1

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/playback/LocalDevicePlayback;->mReloadedQueueSeekPos:J

    .line 2006
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v25

    .line 2007
    .local v25, "preferences":Landroid/content/SharedPreferences;
    const-string v11, "medialist"

    const/4 v13, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v11, v13}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 2008
    .local v20, "embryo":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "reloading queue from: "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 2009
    if-eqz v20, :cond_1

    .line 2010
    invoke-static/range {v20 .. v20}, Lcom/google/android/music/medialist/MediaList;->thaw(Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList;

    move-result-object v23

    .line 2011
    .local v23, "list":Lcom/google/android/music/medialist/MediaList;
    if-eqz v23, :cond_0

    move-object/from16 v0, v23

    instance-of v11, v0, Lcom/google/android/music/medialist/SongList;

    if-nez v11, :cond_3

    .line 2012
    :cond_0
    const-string v13, "LocalDevicePlayback"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "Saved media list must be a SongList, but got "

    move-object/from16 v0, v36

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    if-nez v23, :cond_2

    const-string v11, "<null>"

    :goto_0
    move-object/from16 v0, v36

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v36, " instead"

    move-object/from16 v0, v36

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v13, v11}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2014
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "error thawing: "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 2015
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->resetQueue()V

    .line 2189
    .end local v23    # "list":Lcom/google/android/music/medialist/MediaList;
    :cond_1
    :goto_1
    return-void

    .line 2012
    .restart local v23    # "list":Lcom/google/android/music/medialist/MediaList;
    :cond_2
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    .line 2021
    :cond_3
    const-string v11, "lastUserInteract"

    const-wide/16 v36, 0x0

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-interface {v0, v11, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v36

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    .line 2022
    const-string v11, "lastUserExplicitPlay"

    const-wide/16 v36, 0x0

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-interface {v0, v11, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v36

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserExplicitPlayTime:J

    .line 2024
    const-string v11, "repeatMode"

    const/4 v13, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v11, v13}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v26

    .line 2025
    .local v26, "repmode":I
    const/4 v11, 0x2

    move/from16 v0, v26

    if-eq v0, v11, :cond_4

    const/4 v11, 0x1

    move/from16 v0, v26

    if-eq v0, v11, :cond_4

    .line 2027
    const/16 v26, 0x0

    .line 2029
    :cond_4
    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    .line 2031
    const-string v11, "shufflemode"

    const/4 v13, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v11, v13}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v35

    .line 2032
    .local v35, "shufmode":I
    const/4 v11, 0x1

    move/from16 v0, v35

    if-eq v0, v11, :cond_5

    .line 2033
    const/16 v35, 0x0

    .line 2035
    :cond_5
    move/from16 v0, v35

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    .line 2037
    const-string v11, "infiniteMixSeedId"

    const/4 v13, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v11, v13}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 2038
    .local v12, "restoredMixSeed":Ljava/lang/String;
    const-string v11, "infiniteMixName"

    const-string v13, ""

    move-object/from16 v0, v25

    invoke-interface {v0, v11, v13}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2039
    .local v9, "restoreMixName":Ljava/lang/String;
    const-string v11, "infiniteMixArtLocation"

    const/4 v13, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v11, v13}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2041
    .local v10, "restoreMixArtLocation":Ljava/lang/String;
    const-string v11, "infiniteMixType"

    const/4 v13, -0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v11, v13}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v32

    .line 2043
    .local v32, "restoredMixTypeOrdinal":I
    const-string v11, "infiniteMixSeedTypeLocal"

    const/4 v13, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v11, v13}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v21

    .line 2045
    .local v21, "isSeedIdLocal":Z
    const/16 v24, 0x0

    .line 2046
    .local v24, "mix":Lcom/google/android/music/mix/MixDescriptor;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_c

    const/4 v11, -0x1

    move/from16 v0, v32

    if-eq v0, v11, :cond_c

    .line 2047
    invoke-static {}, Lcom/google/android/music/mix/MixDescriptor$Type;->values()[Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v11

    aget-object v8, v11, v32

    .line 2048
    .local v8, "mixType":Lcom/google/android/music/mix/MixDescriptor$Type;
    if-eqz v21, :cond_b

    .line 2050
    invoke-static {v12}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 2051
    .local v6, "localId":J
    const-wide/16 v36, -0x1

    cmp-long v11, v6, v36

    if-eqz v11, :cond_9

    .line 2053
    :try_start_0
    new-instance v5, Lcom/google/android/music/mix/MixDescriptor;

    invoke-direct/range {v5 .. v10}, Lcom/google/android/music/mix/MixDescriptor;-><init>(JLcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2073
    .end local v6    # "localId":J
    .end local v8    # "mixType":Lcom/google/android/music/mix/MixDescriptor$Type;
    .end local v24    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    .local v5, "mix":Lcom/google/android/music/mix/MixDescriptor;
    :goto_2
    if-eqz v5, :cond_7

    .line 2074
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v11, :cond_6

    .line 2075
    const-string v11, "LocalDevicePlayback"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "Restored mix: "

    move-object/from16 v0, v36

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v13}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2077
    :cond_6
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->enableInfiniteMixMode(Lcom/google/android/music/mix/MixDescriptor;)V

    .line 2080
    :cond_7
    const-string v11, "containerType"

    const/4 v13, -0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v11, v13}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 2081
    .local v4, "containerType":I
    const-string v11, "containerId"

    const-wide/16 v36, -0x1

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-interface {v0, v11, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    .line 2082
    .local v14, "containerId":J
    const-string v11, "containerName"

    const/4 v13, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v11, v13}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 2083
    .local v16, "containerName":Ljava/lang/String;
    const-string v11, "containerExtId"

    const/4 v13, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v11, v13}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 2084
    .local v17, "containerExtId":Ljava/lang/String;
    const-string v11, "containerExtData"

    const/4 v13, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v11, v13}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 2085
    .local v18, "containerExtData":Ljava/lang/String;
    const/4 v11, -0x1

    if-eq v4, v11, :cond_8

    const-wide/16 v36, -0x1

    cmp-long v11, v14, v36

    if-eqz v11, :cond_8

    .line 2086
    invoke-static {v4}, Lcom/google/android/music/store/ContainerDescriptor$Type;->fromDBValue(I)Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v13

    invoke-static/range {v13 .. v18}, Lcom/google/android/music/store/ContainerDescriptor;->newUnvalidatedDescriptor(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 2091
    :cond_8
    check-cast v23, Lcom/google/android/music/medialist/SongList;

    .end local v23    # "list":Lcom/google/android/music/medialist/MediaList;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->setMediaList(Lcom/google/android/music/medialist/SongList;)V

    .line 2092
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v11, v13}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->setMediaList(Lcom/google/android/music/medialist/SongList;)V

    .line 2093
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v11}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v11

    if-gtz v11, :cond_d

    .line 2095
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Empty playlist "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v13}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 2096
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->resetQueue()V

    goto/16 :goto_1

    .line 2056
    .end local v4    # "containerType":I
    .end local v5    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    .end local v14    # "containerId":J
    .end local v16    # "containerName":Ljava/lang/String;
    .end local v17    # "containerExtId":Ljava/lang/String;
    .end local v18    # "containerExtData":Ljava/lang/String;
    .restart local v6    # "localId":J
    .restart local v8    # "mixType":Lcom/google/android/music/mix/MixDescriptor$Type;
    .restart local v23    # "list":Lcom/google/android/music/medialist/MediaList;
    .restart local v24    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    :catch_0
    move-exception v19

    .line 2057
    .local v19, "e":Ljava/lang/NumberFormatException;
    const-string v11, "LocalDevicePlayback"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "Failed to parse out the long from local seed id: "

    move-object/from16 v0, v36

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v13}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v5, v24

    .line 2059
    .end local v24    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v5    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    goto/16 :goto_2

    .line 2061
    .end local v5    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    .end local v19    # "e":Ljava/lang/NumberFormatException;
    .restart local v24    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    :cond_9
    const-string v11, "LocalDevicePlayback"

    const-string v13, "Failed to load mix"

    invoke-static {v11, v13}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .end local v6    # "localId":J
    .end local v8    # "mixType":Lcom/google/android/music/mix/MixDescriptor$Type;
    :cond_a
    move-object/from16 v5, v24

    .end local v24    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v5    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    goto/16 :goto_2

    .line 2064
    .end local v5    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v8    # "mixType":Lcom/google/android/music/mix/MixDescriptor$Type;
    .restart local v24    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    :cond_b
    new-instance v5, Lcom/google/android/music/mix/MixDescriptor;

    const/16 v16, 0x0

    move-object v11, v5

    move-object v13, v8

    move-object v14, v9

    move-object v15, v10

    invoke-direct/range {v11 .. v16}, Lcom/google/android/music/mix/MixDescriptor;-><init>(Ljava/lang/String;Lcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;Z)V

    .end local v24    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v5    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    goto/16 :goto_2

    .line 2068
    .end local v5    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    .end local v8    # "mixType":Lcom/google/android/music/mix/MixDescriptor$Type;
    .restart local v24    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    :cond_c
    sget-object v11, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v11}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v11

    move/from16 v0, v32

    if-ne v0, v11, :cond_a

    .line 2069
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/music/mix/MixDescriptor;->getLuckyRadio(Landroid/content/Context;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v5

    .end local v24    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v5    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    goto/16 :goto_2

    .line 2100
    .end local v23    # "list":Lcom/google/android/music/medialist/MediaList;
    .restart local v4    # "containerType":I
    .restart local v14    # "containerId":J
    .restart local v16    # "containerName":Ljava/lang/String;
    .restart local v17    # "containerExtId":Ljava/lang/String;
    .restart local v18    # "containerExtData":Ljava/lang/String;
    :cond_d
    const-string v11, "curpos"

    const/4 v13, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v11, v13}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v33

    .line 2101
    .local v33, "restoredPosition":I
    const-string v11, "curAudioId"

    const-wide/16 v36, 0x0

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-interface {v0, v11, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v28

    .line 2102
    .local v28, "restoredAudioId":J
    const-string v11, "curDomainId"

    const/4 v13, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v11, v13}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 2103
    .local v27, "restoredDomain":Ljava/lang/String;
    const-string v11, "curListItemId"

    const-wide/16 v36, 0x0

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-interface {v0, v11, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v30

    .line 2104
    .local v30, "restoredListItemId":J
    const-wide/16 v36, 0x0

    cmp-long v11, v28, v36

    if-eqz v11, :cond_e

    if-eqz v27, :cond_e

    .line 2107
    move/from16 v0, v33

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    .line 2108
    new-instance v11, Lcom/google/android/music/download/ContentIdentifier;

    invoke-static/range {v27 .. v27}, Lcom/google/android/music/download/ContentIdentifier$Domain;->valueOf(Ljava/lang/String;)Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v13

    move-wide/from16 v0, v28

    invoke-direct {v11, v0, v1, v13}, Lcom/google/android/music/download/ContentIdentifier;-><init>(JLcom/google/android/music/download/ContentIdentifier$Domain;)V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    .line 2110
    move-wide/from16 v0, v30

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J

    .line 2112
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->refreshCurrentSongPosition()Z
    invoke-static {v11}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->access$6400(Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;)Z

    move-result v11

    if-eqz v11, :cond_16

    .line 2113
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    move/from16 v0, v33

    if-eq v0, v11, :cond_e

    .line 2114
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Current song postion changed from "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, v33

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, " to "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 2116
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    move/from16 v33, v0

    .line 2124
    :cond_e
    :goto_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v11}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v22

    .line 2125
    .local v22, "length":I
    if-ltz v33, :cond_f

    move/from16 v0, v33

    move/from16 v1, v22

    if-lt v0, v1, :cond_10

    .line 2126
    :cond_f
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "position out of range: "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, v33

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, "/"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 2127
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    const/4 v13, 0x2

    if-ne v11, v13, :cond_17

    .line 2129
    const/16 v33, 0x0

    .line 2136
    :cond_10
    :goto_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    if-eqz v11, :cond_11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayQueue(Lcom/google/android/music/medialist/SongList;)Z

    move-result v11

    if-nez v11, :cond_11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v11}, Lcom/google/android/music/medialist/SongList;->isDefaultDomain()Z

    move-result v11

    if-eqz v11, :cond_11

    .line 2138
    const-string v11, "LocalDevicePlayback"

    const-string v13, "Upgrading to queue"

    invoke-static {v11, v13}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2140
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    const/4 v13, 0x0

    const/16 v36, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v33

    move/from16 v2, v36

    invoke-direct {v0, v11, v13, v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->caqPlay(Lcom/google/android/music/medialist/SongList;ZIZ)Lcom/google/android/music/store/PlayQueueAddResult;

    move-result-object v34

    .line 2141
    .local v34, "result":Lcom/google/android/music/store/PlayQueueAddResult;
    if-eqz v34, :cond_18

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/music/store/PlayQueueAddResult;->getAddedSize()I

    move-result v11

    if-lez v11, :cond_18

    .line 2142
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->createPlayQueueSongList()Lcom/google/android/music/medialist/SongList;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/music/playback/LocalDevicePlayback;->setMediaList(Lcom/google/android/music/medialist/SongList;)V

    .line 2143
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v11, v13}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->setMediaList(Lcom/google/android/music/medialist/SongList;)V

    .line 2144
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/music/store/QueueUtils;->notifyChange(Landroid/content/Context;)V

    .line 2152
    .end local v34    # "result":Lcom/google/android/music/store/PlayQueueAddResult;
    :cond_11
    :goto_5
    move/from16 v0, v33

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    .line 2153
    const-string v11, "seekpos"

    const-wide/16 v36, 0x0

    move-object/from16 v0, v25

    move-wide/from16 v1, v36

    invoke-interface {v0, v11, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v36

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/playback/LocalDevicePlayback;->mReloadedQueueSeekPos:J

    .line 2165
    const/16 v11, 0x14

    move-object/from16 v0, p0

    iput v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mOpenFailedCounter:I

    .line 2167
    if-eqz p1, :cond_12

    .line 2168
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/music/playback/LocalDevicePlayback;->stop(Z)V

    .line 2170
    :cond_12
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->loadCurrent()Z

    move-result v11

    if-nez v11, :cond_13

    .line 2171
    const-wide/16 v36, 0x0

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/playback/LocalDevicePlayback;->mReloadedQueueSeekPos:J

    .line 2173
    :cond_13
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->shouldPlayInRandomOrder()Z

    move-result v11

    if-eqz v11, :cond_15

    .line 2174
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRand:Lcom/google/android/music/StrictShuffler;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v13}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/google/android/music/StrictShuffler;->setHistorySize(I)V

    .line 2175
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    if-ltz v11, :cond_14

    .line 2176
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRand:Lcom/google/android/music/StrictShuffler;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-virtual {v11, v13}, Lcom/google/android/music/StrictShuffler;->injectHistoricalValue(I)V

    .line 2178
    :cond_14
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;

    monitor-enter v13

    .line 2179
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->fillShuffleList()V

    .line 2180
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2181
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->dumpPastPresentAndFuture()V

    .line 2184
    :cond_15
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onQueueChanged()V

    .line 2185
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "queue reloaded with length "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v13}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, ", shuffle mode "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, ", playpos "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, ", id "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 2119
    .end local v22    # "length":I
    :cond_16
    const-string v11, "Could not locate current song when restoring queue."

    invoke-static {v11}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 2132
    .restart local v22    # "length":I
    :cond_17
    add-int/lit8 v33, v22, -0x1

    goto/16 :goto_4

    .line 2147
    .restart local v34    # "result":Lcom/google/android/music/store/PlayQueueAddResult;
    :cond_18
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    .line 2148
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->setMediaList(Lcom/google/android/music/medialist/SongList;)V

    goto/16 :goto_5

    .line 2180
    .end local v34    # "result":Lcom/google/android/music/store/PlayQueueAddResult;
    :catchall_0
    move-exception v11

    :try_start_2
    monitor-exit v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v11
.end method

.method private requestCreateRadioStationSync()V
    .locals 4

    .prologue
    .line 4025
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    .line 4026
    .local v0, "state":Lcom/google/android/music/mix/MixGenerationState;
    if-nez v0, :cond_0

    .line 4027
    const-string v1, "LocalDevicePlayback"

    const-string v2, "Missing mix state"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4035
    :goto_0
    return-void

    .line 4031
    :cond_0
    sget-object v1, Lcom/google/android/music/mix/MixGenerationState$State;->NOT_STARTED:Lcom/google/android/music/mix/MixGenerationState$State;

    invoke-virtual {v0, v1}, Lcom/google/android/music/mix/MixGenerationState;->setState(Lcom/google/android/music/mix/MixGenerationState$State;)V

    .line 4033
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayQueueFeeder:Lcom/google/android/music/mix/PlayQueueFeeder;

    invoke-virtual {v0}, Lcom/google/android/music/mix/MixGenerationState;->getMix()Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->NONE:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/mix/PlayQueueFeeder;->requestCreateRadioStation(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    goto :goto_0
.end method

.method private requestMoreContentSync(Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V
    .locals 6
    .param p1, "postAction"    # Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .prologue
    .line 4038
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    .line 4039
    .local v2, "state":Lcom/google/android/music/mix/MixGenerationState;
    if-nez v2, :cond_0

    .line 4040
    const-string v3, "LocalDevicePlayback"

    const-string v4, "Missing mix state"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 4058
    :goto_0
    return-void

    .line 4044
    :cond_0
    sget-object v3, Lcom/google/android/music/mix/MixGenerationState$State;->NOT_STARTED:Lcom/google/android/music/mix/MixGenerationState$State;

    invoke-virtual {v2, v3}, Lcom/google/android/music/mix/MixGenerationState;->setState(Lcom/google/android/music/mix/MixGenerationState$State;)V

    .line 4046
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "music_infinite_mix_recently_played_size"

    const/16 v5, 0x32

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 4050
    .local v1, "recentlyPlayedSize":I
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v3, v1}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->getTailTracks(I)Ljava/util/List;

    move-result-object v0

    .line 4051
    .local v0, "recentlyPlayed":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    iget-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v3, :cond_1

    .line 4052
    const-string v3, "LocalDevicePlayback"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "requestMoreContentSync: recentlyPlayed="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->listToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4056
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayQueueFeeder:Lcom/google/android/music/mix/PlayQueueFeeder;

    invoke-virtual {v2}, Lcom/google/android/music/mix/MixGenerationState;->getMix()Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->canStream()Z

    move-result v5

    invoke-virtual {v3, v4, v0, v5, p1}, Lcom/google/android/music/mix/PlayQueueFeeder;->requestContent(Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List;ZLcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    goto :goto_0
.end method

.method private resetCurrentSongMetaDataCursor()V
    .locals 3

    .prologue
    .line 3791
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v1

    .line 3792
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3793
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 3794
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 3796
    :cond_0
    monitor-exit v1

    .line 3797
    return-void

    .line 3796
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private resetQueue()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1994
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertNotMainThread()V

    .line 1995
    iput-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    .line 1996
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->setMediaList(Lcom/google/android/music/medialist/SongList;)V

    .line 1997
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1998
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 1999
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2000
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onQueueChanged()V

    .line 2001
    return-void
.end method

.method private resumePlaybackAsync(J)V
    .locals 3
    .param p1, "position"    # J

    .prologue
    .line 5108
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 5110
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$35;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/music/playback/LocalDevicePlayback$35;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;J)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 5128
    return-void
.end method

.method private saveBookmarkIfNeeded()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x2710

    .line 4212
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPodcast()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 4213
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->position()J

    move-result-wide v4

    .line 4214
    .local v4, "pos":J
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getBookmark()J

    move-result-wide v0

    .line 4215
    .local v0, "bookmark":J
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->duration()J

    move-result-wide v2

    .line 4216
    .local v2, "duration":J
    cmp-long v8, v4, v0

    if-gez v8, :cond_0

    add-long v8, v4, v10

    cmp-long v8, v8, v0

    if-gtz v8, :cond_1

    :cond_0
    cmp-long v8, v4, v0

    if-lez v8, :cond_2

    sub-long v8, v4, v10

    cmp-long v8, v8, v0

    if-gez v8, :cond_2

    .line 4238
    .end local v0    # "bookmark":J
    .end local v2    # "duration":J
    .end local v4    # "pos":J
    :cond_1
    :goto_0
    return-void

    .line 4222
    .restart local v0    # "bookmark":J
    .restart local v2    # "duration":J
    .restart local v4    # "pos":J
    :cond_2
    const-wide/16 v8, 0x3a98

    cmp-long v8, v4, v8

    if-ltz v8, :cond_3

    add-long v8, v4, v10

    cmp-long v8, v8, v2

    if-lez v8, :cond_4

    .line 4224
    :cond_3
    const-wide/16 v4, 0x0

    .line 4228
    :cond_4
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 4229
    .local v7, "values":Landroid/content/ContentValues;
    const-string v8, "bookmark"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4230
    sget-object v9, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v8, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/database/Cursor;

    const/4 v10, 0x0

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 4233
    .local v6, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v6, v7, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4236
    .end local v0    # "bookmark":J
    .end local v2    # "duration":J
    .end local v4    # "pos":J
    .end local v6    # "uri":Landroid/net/Uri;
    .end local v7    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v8

    goto :goto_0
.end method

.method private saveQueue(Z)V
    .locals 42
    .param p1, "full"    # Z

    .prologue
    .line 1870
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mQueueIsSaveable:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    if-nez v2, :cond_1

    .line 1991
    :cond_0
    :goto_0
    return-void

    .line 1876
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    .line 1877
    .local v5, "mediaList":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    if-nez v2, :cond_3

    const-wide/16 v13, 0x0

    .line 1878
    .local v13, "curAudioId":J
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    if-nez v2, :cond_4

    const/4 v15, 0x0

    .line 1879
    .local v15, "curDomain":Lcom/google/android/music/download/ContentIdentifier$Domain;
    :goto_2
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J

    move-wide/from16 v16, v0

    .line 1880
    .local v16, "curListItemId":J
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getAlbumId()J

    move-result-wide v7

    .line 1881
    .local v7, "curAlbumId":J
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getArtistName()Ljava/lang/String;

    move-result-object v9

    .line 1882
    .local v9, "curArtist":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getAlbumName()Ljava/lang/String;

    move-result-object v10

    .line 1883
    .local v10, "curAlbum":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getTrackName()Ljava/lang/String;

    move-result-object v12

    .line 1884
    .local v12, "curTitle":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getRating()I

    move-result v11

    .line 1885
    .local v11, "rating":I
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    .line 1886
    .local v6, "playPos":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    move/from16 v24, v0

    .line 1887
    .local v24, "repeatMode":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    move/from16 v25, v0

    .line 1888
    .local v25, "shuffleMode":I
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    move-wide/from16 v20, v0

    .line 1889
    .local v20, "lastUserInteractionTime":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserExplicitPlayTime:J

    move-wide/from16 v22, v0

    .line 1896
    .local v22, "lastUserExplicitPlayTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    move-object/from16 v38, v0

    .line 1897
    .local v38, "state":Lcom/google/android/music/mix/MixGenerationState;
    if-eqz v38, :cond_7

    .line 1898
    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/mix/MixGenerationState;->getMix()Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v37

    .line 1899
    .local v37, "mix":Lcom/google/android/music/mix/MixDescriptor;
    invoke-virtual/range {v37 .. v37}, Lcom/google/android/music/mix/MixDescriptor;->hasRemoteSeedId()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual/range {v37 .. v37}, Lcom/google/android/music/mix/MixDescriptor;->getType()Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    if-ne v2, v3, :cond_5

    .line 1900
    :cond_2
    invoke-virtual/range {v37 .. v37}, Lcom/google/android/music/mix/MixDescriptor;->getRemoteSeedId()Ljava/lang/String;

    move-result-object v26

    .line 1901
    .local v26, "mixSeed":Ljava/lang/String;
    const/16 v27, 0x0

    .line 1910
    .local v27, "isSeedIdLocal":Z
    :goto_3
    invoke-virtual/range {v37 .. v37}, Lcom/google/android/music/mix/MixDescriptor;->getName()Ljava/lang/String;

    move-result-object v28

    .line 1911
    .local v28, "mixName":Ljava/lang/String;
    invoke-virtual/range {v37 .. v37}, Lcom/google/android/music/mix/MixDescriptor;->getArtLocation()Ljava/lang/String;

    move-result-object v29

    .line 1912
    .local v29, "mixArtLocation":Ljava/lang/String;
    invoke-virtual/range {v37 .. v37}, Lcom/google/android/music/mix/MixDescriptor;->getType()Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v30

    .line 1926
    .end local v37    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    .local v30, "mixType":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    if-eqz v2, :cond_8

    .line 1927
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor$Type;->getDBValue()I

    move-result v31

    .line 1928
    .local v31, "containerType":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    move-result-wide v32

    .line 1929
    .local v32, "containerId":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v34

    .line 1930
    .local v34, "containerName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v35

    .line 1931
    .local v35, "containerExtId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->getExtData()Ljava/lang/String;

    move-result-object v36

    .line 1941
    .local v36, "containerExtData":Ljava/lang/String;
    :goto_5
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mReloadedQueueSeekPos:J

    const-wide/16 v40, 0x0

    cmp-long v2, v2, v40

    if-ltz v2, :cond_9

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mReloadedQueueSeekPos:J

    move-wide/from16 v18, v0

    .line 1944
    .local v18, "playerPosition":J
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1945
    sget-object v39, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/playback/LocalDevicePlayback$20;

    move-object/from16 v3, p0

    move/from16 v4, p1

    invoke-direct/range {v2 .. v36}, Lcom/google/android/music/playback/LocalDevicePlayback$20;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;ZLcom/google/android/music/medialist/SongList;IJLjava/lang/String;Ljava/lang/String;ILjava/lang/String;JLcom/google/android/music/download/ContentIdentifier$Domain;JJJJIILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;IIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v39

    invoke-static {v0, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 1877
    .end local v6    # "playPos":I
    .end local v7    # "curAlbumId":J
    .end local v9    # "curArtist":Ljava/lang/String;
    .end local v10    # "curAlbum":Ljava/lang/String;
    .end local v11    # "rating":I
    .end local v12    # "curTitle":Ljava/lang/String;
    .end local v13    # "curAudioId":J
    .end local v15    # "curDomain":Lcom/google/android/music/download/ContentIdentifier$Domain;
    .end local v16    # "curListItemId":J
    .end local v18    # "playerPosition":J
    .end local v20    # "lastUserInteractionTime":J
    .end local v22    # "lastUserExplicitPlayTime":J
    .end local v24    # "repeatMode":I
    .end local v25    # "shuffleMode":I
    .end local v26    # "mixSeed":Ljava/lang/String;
    .end local v27    # "isSeedIdLocal":Z
    .end local v28    # "mixName":Ljava/lang/String;
    .end local v29    # "mixArtLocation":Ljava/lang/String;
    .end local v30    # "mixType":I
    .end local v31    # "containerType":I
    .end local v32    # "containerId":J
    .end local v34    # "containerName":Ljava/lang/String;
    .end local v35    # "containerExtId":Ljava/lang/String;
    .end local v36    # "containerExtData":Ljava/lang/String;
    .end local v38    # "state":Lcom/google/android/music/mix/MixGenerationState;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v2}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v13

    goto/16 :goto_1

    .line 1878
    .restart local v13    # "curAudioId":J
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v2}, Lcom/google/android/music/download/ContentIdentifier;->getDomain()Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v15

    goto/16 :goto_2

    .line 1903
    .restart local v6    # "playPos":I
    .restart local v7    # "curAlbumId":J
    .restart local v9    # "curArtist":Ljava/lang/String;
    .restart local v10    # "curAlbum":Ljava/lang/String;
    .restart local v11    # "rating":I
    .restart local v12    # "curTitle":Ljava/lang/String;
    .restart local v15    # "curDomain":Lcom/google/android/music/download/ContentIdentifier$Domain;
    .restart local v16    # "curListItemId":J
    .restart local v20    # "lastUserInteractionTime":J
    .restart local v22    # "lastUserExplicitPlayTime":J
    .restart local v24    # "repeatMode":I
    .restart local v25    # "shuffleMode":I
    .restart local v37    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v38    # "state":Lcom/google/android/music/mix/MixGenerationState;
    :cond_5
    invoke-virtual/range {v37 .. v37}, Lcom/google/android/music/mix/MixDescriptor;->getType()Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/mix/MixDescriptor$Type;->RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    if-ne v2, v3, :cond_6

    .line 1904
    invoke-virtual/range {v37 .. v37}, Lcom/google/android/music/mix/MixDescriptor;->getLocalRadioId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v26

    .line 1908
    .restart local v26    # "mixSeed":Ljava/lang/String;
    :goto_7
    const/16 v27, 0x1

    .restart local v27    # "isSeedIdLocal":Z
    goto/16 :goto_3

    .line 1906
    .end local v26    # "mixSeed":Ljava/lang/String;
    .end local v27    # "isSeedIdLocal":Z
    :cond_6
    invoke-virtual/range {v37 .. v37}, Lcom/google/android/music/mix/MixDescriptor;->getLocalSeedId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v26

    .restart local v26    # "mixSeed":Ljava/lang/String;
    goto :goto_7

    .line 1914
    .end local v26    # "mixSeed":Ljava/lang/String;
    .end local v37    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    :cond_7
    const-string v26, ""

    .line 1915
    .restart local v26    # "mixSeed":Ljava/lang/String;
    const/16 v27, 0x0

    .line 1916
    .restart local v27    # "isSeedIdLocal":Z
    const-string v28, ""

    .line 1917
    .restart local v28    # "mixName":Ljava/lang/String;
    const/16 v29, 0x0

    .line 1918
    .restart local v29    # "mixArtLocation":Ljava/lang/String;
    const/16 v30, -0x1

    .restart local v30    # "mixType":I
    goto/16 :goto_4

    .line 1933
    :cond_8
    const/16 v31, -0x1

    .line 1934
    .restart local v31    # "containerType":I
    const-wide/16 v32, -0x1

    .line 1935
    .restart local v32    # "containerId":J
    const-string v34, ""

    .line 1936
    .restart local v34    # "containerName":Ljava/lang/String;
    const-string v35, ""

    .line 1937
    .restart local v35    # "containerExtId":Ljava/lang/String;
    const-string v36, ""

    .restart local v36    # "containerExtData":Ljava/lang/String;
    goto :goto_5

    .line 1941
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v2}, Lcom/google/android/music/playback/AsyncMediaPlayer;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v2}, Lcom/google/android/music/playback/AsyncMediaPlayer;->position()J

    move-result-wide v18

    goto :goto_6

    :cond_a
    const-wide/16 v18, 0x0

    goto :goto_6
.end method

.method private selectDefaultMediaRouteImpl()V
    .locals 3

    .prologue
    .line 5046
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-nez v1, :cond_0

    .line 5047
    const-string v1, "LocalDevicePlayback"

    const-string v2, "selectDefaultMediaRouteImpl called but the MediaRouter is null"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 5053
    :goto_0
    return-void

    .line 5050
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter;->getDefaultRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    .line 5051
    .local v0, "defaultRoute":Landroid/support/v7/media/MediaRouter$RouteInfo;
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v1, v0}, Landroid/support/v7/media/MediaRouter;->selectRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 5052
    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mConnectedMediaRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    goto :goto_0
.end method

.method private setMediaList(Lcom/google/android/music/medialist/SongList;)V
    .locals 1
    .param p1, "newList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 2333
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v0}, Lcom/google/android/music/medialist/SongList;->isDefaultDomain()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

    if-eqz v0, :cond_0

    .line 2335
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingClient;->clearPrefetchedCache()V

    .line 2337
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    .line 2338
    return-void
.end method

.method private setMediaRouteImpl(ZLjava/lang/String;ZJ)V
    .locals 20
    .param p1, "localRoute"    # Z
    .param p2, "routeId"    # Ljava/lang/String;
    .param p3, "autoPlay"    # Z
    .param p4, "position"    # J

    .prologue
    .line 5217
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/cast/CastUtils;->getCastV2Selector(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    .line 5218
    .local v14, "castV2Selector":Ljava/lang/String;
    new-instance v5, Landroid/support/v7/media/MediaRouteSelector$Builder;

    invoke-direct {v5}, Landroid/support/v7/media/MediaRouteSelector$Builder;-><init>()V

    const-string v6, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v5, v6}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addControlCategory(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    move-result-object v5

    const-string v6, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v5, v6}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addControlCategory(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    move-result-object v5

    invoke-virtual {v5, v14}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addControlCategory(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v7/media/MediaRouteSelector$Builder;->build()Landroid/support/v7/media/MediaRouteSelector;

    move-result-object v17

    .line 5223
    .local v17, "selector":Landroid/support/v7/media/MediaRouteSelector;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-nez v5, :cond_1

    .line 5224
    const-string v5, "LocalDevicePlayback"

    const-string v6, "setMediaRouteImpl called but the MediaRouter is null"

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 5327
    :cond_0
    return-void

    .line 5228
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "music_enable_cast_prequeue"

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v8

    .line 5236
    .local v8, "isPrequeueEnabled":Z
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouterCallback:Landroid/support/v7/media/MediaRouter$Callback;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0, v6}, Landroid/support/v7/media/MediaRouter;->addCallback(Landroid/support/v7/media/MediaRouteSelector;Landroid/support/v7/media/MediaRouter$Callback;)V

    .line 5238
    new-instance v4, Lcom/google/android/music/playback/LocalDevicePlayback$36;

    move-object/from16 v5, p0

    move-object/from16 v6, p2

    move/from16 v7, p1

    move/from16 v9, p3

    move-wide/from16 v10, p4

    invoke-direct/range {v4 .. v11}, Lcom/google/android/music/playback/LocalDevicePlayback$36;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;Ljava/lang/String;ZZZJ)V

    .line 5286
    .local v4, "callback":Landroid/support/v7/media/MediaRouter$Callback;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    const/4 v6, 0x4

    move-object/from16 v0, v17

    invoke-virtual {v5, v0, v4, v6}, Landroid/support/v7/media/MediaRouter;->addCallback(Landroid/support/v7/media/MediaRouteSelector;Landroid/support/v7/media/MediaRouter$Callback;I)V

    .line 5288
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v5}, Landroid/support/v7/media/MediaRouter;->getRoutes()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 5289
    .local v16, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v5, :cond_3

    .line 5290
    const-string v5, "LocalDevicePlayback"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "existing route= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5293
    :cond_3
    invoke-virtual/range {v16 .. v16}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 5294
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v5, v4}, Landroid/support/v7/media/MediaRouter;->removeCallback(Landroid/support/v7/media/MediaRouter$Callback;)V

    .line 5295
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/support/v7/media/MediaRouter;->selectRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 5296
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/playback/LocalDevicePlayback;->mConnectedMediaRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 5298
    if-eqz p3, :cond_4

    .line 5299
    move-object/from16 v0, p0

    move-wide/from16 v1, p4

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->resumePlaybackAsync(J)V

    .line 5302
    :cond_4
    if-nez p1, :cond_6

    .line 5303
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v5}, Lcom/google/android/music/playback/MusicPlaybackService;->getMediaSession()Lcom/google/android/music/playback/session/MediaSessionCompat;

    move-result-object v18

    .line 5304
    .local v18, "session":Lcom/google/android/music/playback/session/MediaSessionCompat;
    if-eqz v18, :cond_5

    .line 5305
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/playback/session/MediaSessionCompat;->getMediaSession()Ljava/lang/Object;

    move-result-object v12

    .line 5306
    .local v12, "actualMediaSession":Ljava/lang/Object;
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/playback/session/MediaSessionCompat;->getRemoteControlClient()Ljava/lang/Object;

    move-result-object v13

    .line 5307
    .local v13, "actualRcc":Ljava/lang/Object;
    if-eqz v12, :cond_7

    .line 5308
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v5, v12}, Landroid/support/v7/media/MediaRouter;->setMediaSession(Ljava/lang/Object;)V

    .line 5313
    .end local v12    # "actualMediaSession":Ljava/lang/Object;
    .end local v13    # "actualRcc":Ljava/lang/Object;
    :cond_5
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCastSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/google/android/music/cast/CastSessionManager;->startSession(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 5315
    if-eqz v8, :cond_8

    .line 5316
    invoke-static/range {v16 .. v16}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isActionEnqueueSupported(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v5

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPrequeueItems:Z

    .line 5322
    .end local v18    # "session":Lcom/google/android/music/playback/session/MediaSessionCompat;
    :cond_6
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v5, :cond_2

    .line 5323
    const-string v5, "LocalDevicePlayback"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Selected route from existing routes: route="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5309
    .restart local v12    # "actualMediaSession":Ljava/lang/Object;
    .restart local v13    # "actualRcc":Ljava/lang/Object;
    .restart local v18    # "session":Lcom/google/android/music/playback/session/MediaSessionCompat;
    :cond_7
    if-eqz v13, :cond_5

    .line 5310
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v5, v13}, Landroid/support/v7/media/MediaRouter;->addRemoteControlClient(Ljava/lang/Object;)V

    goto :goto_1

    .line 5318
    .end local v12    # "actualMediaSession":Ljava/lang/Object;
    .end local v13    # "actualRcc":Ljava/lang/Object;
    :cond_8
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPrequeueItems:Z

    goto :goto_2
.end method

.method private setMediaRouteVolumeImpl(Ljava/lang/String;D)V
    .locals 6
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "volume"    # D

    .prologue
    .line 5336
    iget-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v1, :cond_0

    .line 5337
    const-string v1, "LocalDevicePlayback"

    const-string v2, "setMediaRouteVolumeImpl: routedId=%s volume=%s "

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5340
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5341
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    invoke-interface {v1, p2, p3}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->setVolume(D)V

    .line 5349
    :cond_1
    :goto_0
    return-void

    .line 5342
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v1, :cond_1

    .line 5343
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    .line 5344
    .local v0, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/music/cast/CastUtils;->isRemoteControlIntentRoute(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5346
    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getVolumeMax()I

    move-result v1

    int-to-double v2, v1

    mul-double/2addr v2, p2

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->requestSetVolume(I)V

    goto :goto_0
.end method

.method private setNextTrack()V
    .locals 4

    .prologue
    .line 3231
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayingLocally()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3232
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isGaplessEnabled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 3381
    :cond_0
    :goto_0
    return-void

    .line 3237
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPrequeueItems:Z

    if-nez v1, :cond_2

    .line 3238
    iget-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v1, :cond_0

    .line 3239
    const-string v1, "LocalDevicePlayback"

    const-string v2, "setNextTrack: Bailing as prequeueing is not supported"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3243
    :cond_2
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->hasWiFiConnection()Z

    move-result v1

    if-nez v1, :cond_3

    .line 3244
    iget-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v1, :cond_0

    .line 3245
    const-string v1, "LocalDevicePlayback"

    const-string v2, "setNextTrack: Bailing as there is no WiFi"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3251
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_gapless_enabled_pinned"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 3256
    .local v0, "isGaplessEnabledForCached":Z
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/playback/LocalDevicePlayback$26;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback$26;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private setPreviewValues(Ljava/lang/String;IJ)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "duration"    # J

    .prologue
    .line 4650
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;-><init>(Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPreviewPlaybackInfo:Lcom/google/android/music/playback/LocalDevicePlayback$PreviewPlaybackInfo;

    .line 4651
    return-void
.end method

.method private shouldPlayInRandomOrder()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2755
    iget v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    if-ne v1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isUsingPlayQueue()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private stop(Z)V
    .locals 3
    .param p1, "willPlay"    # Z

    .prologue
    .line 3771
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop: willPlay="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", currentPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3775
    if-eqz p1, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayingLocally()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3776
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3777
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    invoke-interface {v0}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->pause()V

    .line 3783
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->resetCurrentSongMetaDataCursor()V

    .line 3784
    if-nez p1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    if-eqz v0, :cond_2

    .line 3785
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    .line 3786
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onPlayStateChanged()V

    .line 3788
    :cond_2
    return-void

    .line 3779
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->stop()V

    goto :goto_0
.end method

.method private stripNullContentIdentifiers([Lcom/google/android/music/download/ContentIdentifier;)[Lcom/google/android/music/download/ContentIdentifier;
    .locals 6
    .param p1, "ret"    # [Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    .line 2347
    const/4 v0, 0x0

    .line 2348
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, p1

    if-ge v1, v5, :cond_1

    .line 2349
    aget-object v5, p1, v1

    if-eqz v5, :cond_0

    .line 2350
    add-int/lit8 v0, v0, 0x1

    .line 2348
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2353
    :cond_1
    array-length v5, p1

    if-ne v0, v5, :cond_2

    .line 2367
    .end local p1    # "ret":[Lcom/google/android/music/download/ContentIdentifier;
    :goto_1
    return-object p1

    .line 2356
    .restart local p1    # "ret":[Lcom/google/android/music/download/ContentIdentifier;
    :cond_2
    if-gtz v0, :cond_3

    .line 2357
    const/4 p1, 0x0

    goto :goto_1

    .line 2360
    :cond_3
    const/4 v2, 0x0

    .line 2361
    .local v2, "newIndex":I
    new-array v4, v0, [Lcom/google/android/music/download/ContentIdentifier;

    .line 2362
    .local v4, "newRet":[Lcom/google/android/music/download/ContentIdentifier;
    const/4 v1, 0x0

    :goto_2
    array-length v5, p1

    if-ge v1, v5, :cond_5

    .line 2363
    aget-object v5, p1, v1

    if-eqz v5, :cond_4

    .line 2364
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "newIndex":I
    .local v3, "newIndex":I
    aget-object v5, p1, v1

    aput-object v5, v4, v2

    move v2, v3

    .line 2362
    .end local v3    # "newIndex":I
    .restart local v2    # "newIndex":I
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    move-object p1, v4

    .line 2367
    goto :goto_1
.end method

.method private tearDownRemotePlaybackClient()V
    .locals 1

    .prologue
    .line 5493
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    if-eqz v0, :cond_0

    .line 5494
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    invoke-interface {v0}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->leaveSession()V

    .line 5495
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    .line 5497
    :cond_0
    return-void
.end method

.method private tryCreatingStreamingSchedulingClient()V
    .locals 5

    .prologue
    .line 1783
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClientLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1784
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v0, :cond_1

    .line 1785
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;

    if-nez v0, :cond_0

    .line 1786
    const-string v0, "LocalDevicePlayback"

    const-string v2, "Download queue manager is null"

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1787
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    if-nez v0, :cond_1

    .line 1788
    const-string v0, "LocalDevicePlayback"

    const-string v2, "Cache manager is null"

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1791
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    if-eqz v0, :cond_2

    .line 1794
    new-instance v0, Lcom/google/android/music/download/stream/StreamingClient;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/music/download/stream/StreamingClient;-><init>(Landroid/content/Context;Lcom/google/android/music/download/IDownloadQueueManager;Lcom/google/android/music/download/cache/ICacheManager;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

    .line 1796
    const-string v0, "LocalDevicePlayback"

    const-string v2, "Streaming client created."

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1798
    :cond_2
    monitor-exit v1

    .line 1799
    return-void

    .line 1798
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private tryNext()V
    .locals 4

    .prologue
    .line 3641
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v0, :cond_0

    .line 3642
    const-string v0, "LocalDevicePlayback"

    const-string v1, "tryNext"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3646
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 3647
    return-void
.end method

.method private updateDescriptorAndQueue(Z)V
    .locals 3
    .param p1, "shouldUpdate"    # Z

    .prologue
    .line 3101
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isUsingPlayQueue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3102
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->isInCloudQueueMode(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/music/store/Store;->caqGetContainerAndUpdatePlayState(IZZ)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 3105
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    if-eqz v0, :cond_1

    .line 3106
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v0, :cond_0

    .line 3107
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateDescriptorAndQueue: mContainerDescriptor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3116
    :cond_0
    :goto_0
    return-void

    .line 3111
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v0, :cond_0

    .line 3112
    const-string v0, "LocalDevicePlayback"

    const-string v1, "updateDescriptorAndQueue: unknown container"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateLastTimeSuggestedMixPlayed()V
    .locals 4

    .prologue
    .line 2561
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 2562
    .local v1, "ref":Ljava/lang/Object;
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 2564
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/preferences/MusicPreferences;->setLastTimeSuggestedMixPlayedMillis(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2566
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 2568
    return-void

    .line 2566
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method private updateMediaRouteVolumeImpl(Ljava/lang/String;D)V
    .locals 6
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "deltaRatio"    # D

    .prologue
    .line 5359
    iget-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v1, :cond_0

    .line 5360
    const-string v1, "LocalDevicePlayback"

    const-string v2, "updateMediaRouteVolumeImpl: routedId=%s deltaRatio=%s "

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5363
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v1, :cond_1

    .line 5364
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    .line 5365
    .local v0, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/music/cast/CastUtils;->isRemoteControlIntentRoute(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5367
    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getVolumeMax()I

    move-result v1

    int-to-double v2, v1

    mul-double/2addr v2, p2

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->requestUpdateVolume(I)V

    .line 5370
    .end local v0    # "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    :cond_1
    return-void
.end method

.method private useRemotePlaybackClient()Z
    .locals 1

    .prologue
    .line 5471
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/cast/CastUtils;->useCastV2Api(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isDialMediaRouteSupportEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addDownloadProgressListener(Lcom/google/android/music/download/ContentIdentifier;Lcom/google/android/music/download/IDownloadProgressListener;)Z
    .locals 3
    .param p1, "songId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "listener"    # Lcom/google/android/music/download/IDownloadProgressListener;

    .prologue
    .line 4759
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v0, :cond_0

    .line 4760
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addDownloadProgressListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4762
    :cond_0
    if-eqz p2, :cond_1

    .line 4763
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mProgressListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p2}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    move-result v0

    .line 4765
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancelMix()V
    .locals 1

    .prologue
    .line 5013
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->disableInfiniteMixMode()V

    .line 5014
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayQueueFeeder:Lcom/google/android/music/mix/PlayQueueFeeder;

    invoke-virtual {v0}, Lcom/google/android/music/mix/PlayQueueFeeder;->cancelMix()V

    .line 5015
    return-void
.end method

.method public clearQueue()V
    .locals 2

    .prologue
    .line 2622
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->disableInfiniteMixMode()V

    .line 2625
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 2626
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$24;

    invoke-direct {v1, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$24;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 2654
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "writer"    # Ljava/io/PrintWriter;

    .prologue
    .line 4777
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IsPlayingLocally: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayingLocally()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 4778
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncPlayer IsPlaying: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->isPlaying()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 4780
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GaplessEnabled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isGaplessEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 4781
    return-void

    .line 4778
    :cond_0
    const-string v0, "no async player"

    goto :goto_0
.end method

.method public duration()J
    .locals 5

    .prologue
    .line 4586
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v4

    .line 4588
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 4589
    const-wide/16 v2, -0x1

    monitor-exit v4

    .line 4591
    :goto_0
    return-wide v2

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->doGetDuration(Landroid/database/Cursor;)J

    move-result-wide v2

    monitor-exit v4

    goto :goto_0

    .line 4592
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getAlbumArtUrl(J)Ljava/lang/String;
    .locals 1
    .param p1, "albumId"    # J

    .prologue
    .line 4894
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAlbumId()J
    .locals 5

    .prologue
    .line 4463
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v4

    .line 4465
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 4466
    const-wide/16 v2, -0x1

    monitor-exit v4

    .line 4468
    :goto_0
    return-wide v2

    :cond_0
    const-string v1, "album_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    monitor-exit v4

    goto :goto_0

    .line 4470
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getAlbumName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 4451
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v2

    .line 4453
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 4454
    const/4 v1, 0x0

    monitor-exit v2

    .line 4456
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "album"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    monitor-exit v2

    goto :goto_0

    .line 4458
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getArtistId()J
    .locals 5

    .prologue
    .line 4414
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v4

    .line 4416
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 4417
    const-wide/16 v2, -0x1

    monitor-exit v4

    .line 4419
    :goto_0
    return-wide v2

    :cond_0
    const-string v1, "AlbumArtistId"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    monitor-exit v4

    goto :goto_0

    .line 4422
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 4402
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v2

    .line 4404
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 4405
    const/4 v1, 0x0

    monitor-exit v2

    .line 4407
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "artist"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    monitor-exit v2

    goto :goto_0

    .line 4409
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getAudioId()Lcom/google/android/music/download/ContentIdentifier;
    .locals 1

    .prologue
    .line 4338
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4339
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    .line 4341
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAudioSessionId()I
    .locals 1

    .prologue
    .line 4751
    monitor-enter p0

    .line 4752
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->getAudioSessionId()I

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 4753
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getBookmark()J
    .locals 5

    .prologue
    .line 4571
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v4

    .line 4573
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 4574
    const-wide/16 v2, 0x0

    monitor-exit v4

    .line 4576
    :goto_0
    return-wide v2

    :cond_0
    const/16 v1, 0x9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    monitor-exit v4

    goto :goto_0

    .line 4577
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getConnectedMediaRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 1

    .prologue
    .line 5508
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mConnectedMediaRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 5509
    .local v0, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    return-object v0
.end method

.method public getCurrentTrackInfo()Lcom/google/android/music/playback/TrackInfo;
    .locals 26

    .prologue
    .line 4914
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getAudioId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v3

    .line 4915
    .local v3, "songId":Lcom/google/android/music/download/ContentIdentifier;
    if-nez v3, :cond_0

    .line 4916
    const/4 v2, 0x0

    .line 4968
    :goto_0
    return-object v2

    .line 4918
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    move-object/from16 v25, v0

    monitor-enter v25

    .line 4920
    :try_start_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/database/Cursor;

    .local v20, "cursor":Landroid/database/Cursor;
    if-nez v20, :cond_1

    .line 4921
    const/4 v2, 0x0

    monitor-exit v25

    goto :goto_0

    .line 4971
    .end local v20    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v9

    monitor-exit v25
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v9

    .line 4923
    .restart local v20    # "cursor":Landroid/database/Cursor;
    :cond_1
    :try_start_1
    const-string v9, "title"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 4925
    .local v4, "trackName":Ljava/lang/String;
    const-string v9, "artist"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 4927
    .local v5, "artistName":Ljava/lang/String;
    const-string v9, "album"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 4929
    .local v6, "albumName":Ljava/lang/String;
    const-string v9, "album_id"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 4931
    .local v7, "albumId":J
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->doGetDuration(Landroid/database/Cursor;)J

    move-result-wide v10

    .line 4933
    .local v10, "duration":J
    const-string v9, "Nid"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v23

    .line 4935
    .local v23, "trackMetajamIdIdx":I
    const/16 v18, 0x0

    .line 4936
    .local v18, "trackMetajamId":Ljava/lang/String;
    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_2

    .line 4937
    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 4939
    :cond_2
    const/4 v13, 0x1

    .line 4940
    .local v13, "supportRating":Z
    const/4 v12, 0x0

    .line 4941
    .local v12, "externalAlbumArtUrl":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v9

    instance-of v9, v9, Lcom/google/android/music/medialist/ExternalSongList;

    if-eqz v9, :cond_3

    .line 4943
    const/4 v13, 0x0

    .line 4944
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v9

    check-cast v9, Lcom/google/android/music/medialist/ExternalSongList;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/google/android/music/medialist/ExternalSongList;->getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    .line 4947
    :cond_3
    const-string v9, "Rating"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 4950
    .local v14, "rating":I
    const-string v9, "SourceId"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 4952
    .local v21, "remoteUrl":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->getPreviewPlayTypeForUrl(Ljava/lang/String;)I

    move-result v15

    .line 4954
    .local v15, "reviewPlayType":I
    const-string v9, "Vid"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v24

    .line 4955
    .local v24, "videoIdx":I
    const/16 v17, 0x0

    .line 4956
    .local v17, "videoId":Ljava/lang/String;
    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_4

    .line 4957
    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 4960
    :cond_4
    const-string v9, "VThumbnailUrl"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    .line 4961
    .local v22, "thumbnailIdx":I
    const/16 v19, 0x0

    .line 4962
    .local v19, "videoThumbnailUrl":Ljava/lang/String;
    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_5

    .line 4963
    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 4965
    :cond_5
    new-instance v2, Lcom/google/android/music/playback/TrackInfo;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    move-object/from16 v16, v0

    invoke-direct/range {v2 .. v19}, Lcom/google/android/music/playback/TrackInfo;-><init>(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZJLjava/lang/String;ZIILcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4968
    .local v2, "result":Lcom/google/android/music/playback/TrackInfo;
    monitor-exit v25
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public getErrorType()I
    .locals 1

    .prologue
    .line 4865
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->getErrorType()I

    move-result v0

    goto :goto_0
.end method

.method public getLastUserInteractionTime()J
    .locals 2

    .prologue
    .line 4899
    iget-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    return-wide v0
.end method

.method public getMediaList()Lcom/google/android/music/medialist/SongList;
    .locals 1

    .prologue
    .line 4889
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method public getMixState()Lcom/google/android/music/mix/MixGenerationState;
    .locals 1

    .prologue
    .line 4997
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    return-object v0
.end method

.method public getPlaybackState()Lcom/google/android/music/playback/PlaybackState;
    .locals 23

    .prologue
    .line 4978
    :try_start_0
    new-instance v2, Lcom/google/android/music/playback/PlaybackState;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getQueuePosition()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getQueueSize()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlaying()Z

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isCurrentSongLoaded()Z

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->position()J

    move-result-wide v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getShuffleMode()I

    move-result v10

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getRepeatMode()I

    move-result v11

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPreparing()Z

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isStreaming()Z

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isInErrorState()Z

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getErrorType()I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isStreamingFullyBuffered()Z

    move-result v16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isInFatalErrorState()Z

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->hasValidPlaylist()Z

    move-result v18

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->playlistLoading()Z

    move-result v19

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayingLocally()Z

    move-result v20

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isInfiniteMixMode()Z

    move-result v21

    invoke-direct/range {v2 .. v21}, Lcom/google/android/music/playback/PlaybackState;-><init>(IIZZLcom/google/android/music/medialist/SongList;JIIZZZIZZZZZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 4984
    :catch_0
    move-exception v22

    .line 4985
    .local v22, "e":Ljava/lang/Exception;
    const-string v2, "LocalDevicePlayback"

    const-string v3, "Failed to create PlaybackState: "

    move-object/from16 v0, v22

    invoke-static {v2, v3, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4986
    new-instance v2, Ljava/lang/RuntimeException;

    move-object/from16 v0, v22

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public getPreviewPlayType()I
    .locals 4

    .prologue
    .line 4636
    const/4 v1, 0x0

    .line 4637
    .local v1, "remoteUrl":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v3

    .line 4639
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 4640
    const/4 v2, -0x1

    monitor-exit v3

    .line 4646
    :goto_0
    return v2

    .line 4643
    :cond_0
    const-string v2, "SourceId"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 4645
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4646
    invoke-direct {p0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->getPreviewPlayTypeForUrl(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 4645
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public getQueuePosition()I
    .locals 3

    .prologue
    .line 4351
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v0, :cond_0

    .line 4352
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getQueuePosition: pos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4354
    :cond_0
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    return v0
.end method

.method public getQueueSize()I
    .locals 1

    .prologue
    .line 4390
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v0

    goto :goto_0
.end method

.method public getRating()I
    .locals 3

    .prologue
    .line 4492
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v2

    .line 4494
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 4495
    const/4 v1, 0x0

    monitor-exit v2

    .line 4497
    :goto_0
    return v1

    :cond_0
    const-string v1, "Rating"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    monitor-exit v2

    goto :goto_0

    .line 4499
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getRepeatMode()I
    .locals 1

    .prologue
    .line 4330
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    return v0
.end method

.method public getSelectedMediaRouteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5027
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mSelectedRouteId:Ljava/lang/String;

    return-object v0
.end method

.method public getShuffleMode()I
    .locals 1

    .prologue
    .line 4302
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    return v0
.end method

.method public getSongStoreId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 4439
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v2

    .line 4441
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 4442
    const/4 v1, 0x0

    monitor-exit v2

    .line 4444
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "StoreId"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    monitor-exit v2

    goto :goto_0

    .line 4446
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getSortableAlbumArtistName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 4427
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v2

    .line 4429
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 4430
    const/4 v1, 0x0

    monitor-exit v2

    .line 4432
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "artistSort"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    monitor-exit v2

    goto :goto_0

    .line 4434
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getState()Lcom/google/android/music/playback/DevicePlayback$State;
    .locals 2

    .prologue
    .line 1769
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    if-eqz v0, :cond_0

    .line 1770
    sget-object v0, Lcom/google/android/music/playback/DevicePlayback$State;->PLAYING:Lcom/google/android/music/playback/DevicePlayback$State;

    .line 1778
    :goto_0
    return-object v0

    .line 1771
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPausedByTransientLossOfFocus:Z

    if-eqz v0, :cond_1

    .line 1772
    sget-object v0, Lcom/google/android/music/playback/DevicePlayback$State;->TRANSIENT_PAUSE:Lcom/google/android/music/playback/DevicePlayback$State;

    goto :goto_0

    .line 1773
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1774
    sget-object v0, Lcom/google/android/music/playback/DevicePlayback$State;->SWITCHING_TRACKS:Lcom/google/android/music/playback/DevicePlayback$State;

    goto :goto_0

    .line 1775
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->isValid()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 1776
    sget-object v0, Lcom/google/android/music/playback/DevicePlayback$State;->PAUSED:Lcom/google/android/music/playback/DevicePlayback$State;

    goto :goto_0

    .line 1778
    :cond_3
    sget-object v0, Lcom/google/android/music/playback/DevicePlayback$State;->NO_PLAYLIST:Lcom/google/android/music/playback/DevicePlayback$State;

    goto :goto_0
.end method

.method public getTrackName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 4475
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v2

    .line 4477
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 4478
    const/4 v1, 0x0

    monitor-exit v2

    .line 4480
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "title"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    monitor-exit v2

    goto :goto_0

    .line 4482
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public hasLocal()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4560
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v2

    .line 4562
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 4563
    monitor-exit v2

    .line 4565
    :goto_0
    return v1

    :cond_0
    const/16 v3, 0xc

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_1

    const/4 v1, 0x1

    :cond_1
    monitor-exit v2

    goto :goto_0

    .line 4566
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public hasRemote()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4549
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v2

    .line 4551
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 4552
    monitor-exit v2

    .line 4554
    :goto_0
    return v1

    :cond_0
    const/16 v3, 0xb

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_1

    const/4 v1, 0x1

    :cond_1
    monitor-exit v2

    goto :goto_0

    .line 4555
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public hasValidPlaylist()Z
    .locals 1

    .prologue
    .line 4876
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCurrentSongLoaded()Z
    .locals 2

    .prologue
    .line 4395
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v1

    .line 4396
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4397
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isInErrorState()Z
    .locals 1

    .prologue
    .line 4860
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->isInErrorState()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInFatalErrorState()Z
    .locals 1

    .prologue
    .line 4870
    const/4 v0, 0x0

    return v0
.end method

.method public isInfiniteMixMode()Z
    .locals 1

    .prologue
    .line 4992
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 3877
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    return v0
.end method

.method public isPlayingLocally()Z
    .locals 1

    .prologue
    .line 4855
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUseLocalPlayer:Z

    return v0
.end method

.method public isPodcast()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4538
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v2

    .line 4540
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCurrentSongMetaDataCursor:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 4541
    monitor-exit v2

    .line 4543
    :goto_0
    return v1

    :cond_0
    const/16 v3, 0x8

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_1

    const/4 v1, 0x1

    :cond_1
    monitor-exit v2

    goto :goto_0

    .line 4544
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isPreparing()Z
    .locals 1

    .prologue
    .line 4839
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4840
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    invoke-interface {v0}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->isPreparing()Z

    move-result v0

    .line 4842
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->isPreparing()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStreaming()Z
    .locals 1

    .prologue
    .line 4847
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4848
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    invoke-interface {v0}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->isStreaming()Z

    move-result v0

    .line 4850
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->isStreaming()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStreamingFullyBuffered()Z
    .locals 1

    .prologue
    .line 4904
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

    if-eqz v0, :cond_0

    .line 4905
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingClient;->isCurrentStreamingFullyBuffered()Z

    move-result v0

    .line 4907
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 3917
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    .line 3918
    .local v0, "audioId":Lcom/google/android/music/download/ContentIdentifier;
    iget-object v10, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    .line 3919
    .local v10, "player":Lcom/google/android/music/playback/AsyncMediaPlayer;
    if-eqz v0, :cond_0

    if-eqz v10, :cond_0

    .line 3920
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUseLocalPlayer:Z

    if-nez v2, :cond_1

    move v2, v11

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-interface {v10}, Lcom/google/android/music/playback/AsyncMediaPlayer;->position()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserExplicitPlayTime:J

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/music/eventlog/MusicEventLogger;->logSkipForwardEventAsync(ZJLcom/google/android/music/store/ContainerDescriptor;JJ)V

    .line 3927
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    .line 3928
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserExplicitPlayTime:J

    .line 3929
    invoke-direct {p0, v11}, Lcom/google/android/music/playback/LocalDevicePlayback;->next(Z)V

    .line 3930
    return-void

    .line 3920
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 1572
    invoke-super {p0}, Lcom/google/android/music/playback/DevicePlayback;->onCreate()V

    .line 1574
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    .line 1576
    .local v2, "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isMediaRouteSupportEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1577
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-static {v4}, Landroid/support/v7/media/MediaRouter;->getInstance(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1580
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 1583
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    .line 1585
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "audio"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioManager:Landroid/media/AudioManager;

    .line 1587
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCacheServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    new-instance v6, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v7

    const-class v8, Lcom/google/android/music/download/cache/TrackCacheService;

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v4, v5, v6, v9}, Lcom/google/android/music/utils/SafeServiceConnection;->bindService(Landroid/content/Context;Landroid/content/Intent;I)Z

    .line 1590
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDownloadManagerSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    new-instance v6, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v7

    const-class v8, Lcom/google/android/music/download/TrackDownloadQueueService;

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v4, v5, v6, v9}, Lcom/google/android/music/utils/SafeServiceConnection;->bindService(Landroid/content/Context;Landroid/content/Intent;I)Z

    .line 1593
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->bindToService(Landroid/content/Context;)V

    .line 1596
    sget-object v4, Lcom/google/android/music/playback/LocalDevicePlayback;->mDisableGaplessOverride:Ljava/lang/Boolean;

    if-nez v4, :cond_1

    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isGaplessEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "music_disable_gapless_lpaplayer"

    invoke-static {v4, v5, v9}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1602
    sget-object v4, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v5, Lcom/google/android/music/playback/LocalDevicePlayback$13;

    invoke-direct {v5, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$13;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    invoke-static {v4, v5}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 1628
    :cond_1
    sget-object v4, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v5, Lcom/google/android/music/playback/LocalDevicePlayback$14;

    invoke-direct {v5, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$14;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    invoke-static {v4, v5}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 1636
    invoke-static {}, Lcom/google/android/music/utils/PostFroyoUtils$EnvironmentCompat;->isExternalStorageEmulated()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1637
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->registerExternalStorageListener()V

    .line 1643
    :cond_2
    new-instance v0, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    const-class v5, Lcom/google/android/music/download/cache/TrackCacheService;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1644
    .local v0, "cacheIntent":Landroid/content/Intent;
    const-string v4, "com.google.android.music.download.cache.CacheService.CLEAR_ORPHANED"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1645
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v4, v0}, Lcom/google/android/music/playback/MusicPlaybackService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1648
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->createPlayer()Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    .line 1650
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 1651
    .local v3, "sharedFilter":Landroid/content/IntentFilter;
    const-string v4, "com.android.music.sharedpreviewmetadataupdate"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1652
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mSharedPreviewPlayListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1654
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    const-string v5, "power"

    invoke-virtual {v4, v5}, Lcom/google/android/music/playback/MusicPlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 1655
    .local v1, "pm":Landroid/os/PowerManager;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".mWakeLock"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v9, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 1657
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 1658
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".mAsyncWakeLock"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v9, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 1661
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1668
    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v4, v9}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->setPlaylistLoading(Z)V

    .line 1669
    sget-object v4, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v5, Lcom/google/android/music/playback/LocalDevicePlayback$15;

    invoke-direct {v5, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$15;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    invoke-static {v4, v5}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 1683
    new-instance v4, Lcom/google/android/music/mix/PlayQueueFeeder;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayQueueFeederListener:Lcom/google/android/music/mix/PlayQueueFeederListener;

    invoke-direct {v4, v5, v6}, Lcom/google/android/music/mix/PlayQueueFeeder;-><init>(Landroid/content/Context;Lcom/google/android/music/mix/PlayQueueFeederListener;)V

    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayQueueFeeder:Lcom/google/android/music/mix/PlayQueueFeeder;

    .line 1685
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/store/MusicContent$Playlists;->CONTENT_URI:Landroid/net/Uri;

    iget-object v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDynamicContainersObserver:Landroid/database/ContentObserver;

    invoke-virtual {v4, v5, v9, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1688
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/store/MusicContent$AutoPlaylists;->CONTENT_URI:Landroid/net/Uri;

    iget-object v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDynamicContainersObserver:Landroid/database/ContentObserver;

    invoke-virtual {v4, v5, v9, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1691
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/store/MusicContent$Albums;->CONTENT_URI:Landroid/net/Uri;

    iget-object v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDynamicContainersObserver:Landroid/database/ContentObserver;

    invoke-virtual {v4, v5, v9, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1694
    return-void

    .line 1580
    .end local v0    # "cacheIntent":Landroid/content/Intent;
    .end local v1    # "pm":Landroid/os/PowerManager;
    .end local v3    # "sharedFilter":Landroid/content/IntentFilter;
    :catchall_0
    move-exception v4

    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v4
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1699
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDynamicContainersObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1701
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayQueueFeeder:Lcom/google/android/music/mix/PlayQueueFeeder;

    invoke-virtual {v2}, Lcom/google/android/music/mix/PlayQueueFeeder;->onDestroy()V

    .line 1703
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 1705
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->releaseWiFiLock()V

    .line 1707
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCastSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v2}, Lcom/google/android/music/cast/CastSessionManager;->onDestroy()V

    .line 1709
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    invoke-interface {v2}, Lcom/google/android/music/cast/CastTokenClient;->release()V

    .line 1712
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.media.action.CLOSE_AUDIO_EFFECT_CONTROL_SESSION"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1713
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "android.media.extra.AUDIO_SESSION"

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getAudioSessionId()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1714
    const-string v2, "android.media.extra.PACKAGE_NAME"

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1715
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1716
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->releasePlayers()V

    .line 1717
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1719
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->clearCursor()V

    .line 1722
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1723
    sget-object v2, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v3, Lcom/google/android/music/playback/LocalDevicePlayback$16;

    invoke-direct {v3, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$16;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    invoke-static {v2, v3}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 1734
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mSharedPreviewPlayListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1736
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_0

    .line 1737
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1738
    iput-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    .line 1740
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1741
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mDownloadManagerSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/utils/SafeServiceConnection;->unbindService(Landroid/content/Context;)V

    .line 1742
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCacheServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/utils/SafeServiceConnection;->unbindService(Landroid/content/Context;)V

    .line 1743
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->unbindFromService(Landroid/content/Context;)V

    .line 1745
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;

    .line 1746
    .local v1, "streamingClient":Lcom/google/android/music/download/stream/StreamingClient;
    if-eqz v1, :cond_1

    .line 1747
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1748
    sget-object v2, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v3, Lcom/google/android/music/playback/LocalDevicePlayback$17;

    invoke-direct {v3, p0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback$17;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/stream/StreamingClient;)V

    invoke-static {v2, v3}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 1759
    :cond_1
    invoke-super {p0}, Lcom/google/android/music/playback/DevicePlayback;->onDestroy()V

    .line 1760
    return-void
.end method

.method protected onMediaRouteInvalidated()V
    .locals 1

    .prologue
    .line 4823
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->saveQueue(Z)V

    .line 4824
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->selectDefaultMediaRoute()V

    .line 4825
    const-string v0, "com.android.music.mediarouteinvalidated"

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V

    .line 4826
    return-void
.end method

.method protected onOpenComplete()V
    .locals 1

    .prologue
    .line 4809
    const-string v0, "com.android.music.asyncopencomplete"

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V

    .line 4810
    return-void
.end method

.method protected onPlayStateChanged()V
    .locals 1

    .prologue
    .line 4813
    const-string v0, "com.android.music.playstatechanged"

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V

    .line 4814
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->saveQueue(Z)V

    .line 4815
    return-void
.end method

.method protected onPlaybackComplete()V
    .locals 1

    .prologue
    .line 4818
    const-string v0, "com.android.music.playbackcomplete"

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V

    .line 4819
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->saveQueue(Z)V

    .line 4820
    return-void
.end method

.method public open(Lcom/google/android/music/medialist/SongList;IZ)V
    .locals 1
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "position"    # I
    .param p3, "play"    # Z

    .prologue
    .line 2435
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/music/playback/LocalDevicePlayback;->open(Lcom/google/android/music/medialist/SongList;ZIZ)V

    .line 2436
    return-void
.end method

.method public openAndQueue(Lcom/google/android/music/medialist/SongList;I)V
    .locals 2
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "position"    # I

    .prologue
    .line 2573
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v0, :cond_0

    .line 2574
    const-string v0, "LocalDevicePlayback"

    const-string v1, "openAndQueue"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2577
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 2578
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$23;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback$23;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 2618
    return-void
.end method

.method public openMix(Lcom/google/android/music/mix/MixDescriptor;)V
    .locals 5
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    .line 5002
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->enableInfiniteMixMode(Lcom/google/android/music/mix/MixDescriptor;)V

    .line 5003
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v0, :cond_0

    .line 5004
    const-string v0, "LocalDevicePlayback"

    const-string v1, "openMix: mMixGenerationState=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5007
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayQueueFeeder:Lcom/google/android/music/mix/PlayQueueFeeder;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->canStream()Z

    move-result v2

    sget-object v3, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->OPEN:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/music/mix/PlayQueueFeeder;->requestContent(Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List;ZLcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    .line 5009
    return-void
.end method

.method public pause()V
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 3805
    iget-object v8, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    .line 3806
    .local v8, "audioId":Lcom/google/android/music/download/ContentIdentifier;
    if-eqz v8, :cond_0

    .line 3807
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUseLocalPlayer:Z

    if-nez v1, :cond_1

    move v1, v7

    :goto_0
    invoke-virtual {v8}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    iget-wide v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserExplicitPlayTime:J

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/music/eventlog/MusicEventLogger;->logStopEventAsync(ZJLcom/google/android/music/store/ContainerDescriptor;JZ)V

    .line 3814
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    .line 3815
    invoke-direct {p0, v9}, Lcom/google/android/music/playback/LocalDevicePlayback;->pause(Z)V

    .line 3816
    return-void

    :cond_1
    move v1, v9

    .line 3807
    goto :goto_0
.end method

.method public play()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 3654
    const-string v1, "LocalDevicePlayback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "play: currentPos="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3656
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    .line 3657
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserExplicitPlayTime:J

    .line 3658
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3, v0}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 3661
    .local v0, "receivedAudioFocus":Z
    :goto_0
    if-nez v0, :cond_1

    .line 3662
    const-string v1, "LocalDevicePlayback"

    const-string v2, "play() could not obtain audio focus."

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3731
    :goto_1
    return-void

    .line 3658
    .end local v0    # "receivedAudioFocus":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3666
    .restart local v0    # "receivedAudioFocus":Z
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 3667
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/playback/LocalDevicePlayback$29;

    invoke-direct {v2, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$29;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public playNext(Lcom/google/android/music/medialist/SongList;I)V
    .locals 2
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "queuePlayPos"    # I

    .prologue
    .line 3625
    const-string v0, "LocalDevicePlayback"

    const-string v1, "playNext"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3627
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 3628
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$28;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/music/playback/LocalDevicePlayback$28;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;I)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 3637
    return-void
.end method

.method public playlistLoading()Z
    .locals 1

    .prologue
    .line 4881
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    if-nez v0, :cond_0

    .line 4882
    const/4 v0, 0x0

    .line 4884
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->playlistLoading()Z

    move-result v0

    goto :goto_0
.end method

.method public position()J
    .locals 2

    .prologue
    .line 4683
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    invoke-interface {v0}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->isQueueLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4684
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    invoke-interface {v0}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->getPosition()J

    move-result-wide v0

    .line 4688
    :goto_0
    return-wide v0

    .line 4685
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-eqz v0, :cond_1

    .line 4686
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->position()J

    move-result-wide v0

    goto :goto_0

    .line 4688
    :cond_1
    iget-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mReloadedQueueSeekPos:J

    goto :goto_0
.end method

.method public prev()V
    .locals 3

    .prologue
    .line 3905
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    .line 3906
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserExplicitPlayTime:J

    .line 3908
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPendingMediaButtonSeekCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 3909
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prev: currentPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pendingMediaButtonSeekCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPendingMediaButtonSeekCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3911
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->handleMediaButtonSeek(Z)V

    .line 3912
    return-void
.end method

.method public refreshRadio()V
    .locals 3

    .prologue
    .line 2202
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    .line 2203
    .local v0, "state":Lcom/google/android/music/mix/MixGenerationState;
    if-eqz v0, :cond_0

    .line 2204
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 2205
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/playback/LocalDevicePlayback$21;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback$21;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/mix/MixGenerationState;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 2224
    :cond_0
    return-void
.end method

.method public registerMusicCastMediaRouterCallback(Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;

    .prologue
    .line 5373
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v0, :cond_0

    .line 5374
    const-string v0, "LocalDevicePlayback"

    const-string v1, "registerMusicCastMediaRouterCallback"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5376
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMusicCastMediaRouterCallback:Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;

    .line 5377
    return-void
.end method

.method public removeDownloadProgressListener(Lcom/google/android/music/download/IDownloadProgressListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/music/download/IDownloadProgressListener;

    .prologue
    .line 4770
    if-eqz p1, :cond_0

    .line 4771
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mProgressListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 4773
    :cond_0
    return-void
.end method

.method protected saveState()V
    .locals 1

    .prologue
    .line 1764
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->saveQueue(Z)V

    .line 1765
    return-void
.end method

.method public seek(J)J
    .locals 17
    .param p1, "pos"    # J

    .prologue
    .line 4698
    const-string v2, "LocalDevicePlayback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "seek: pos="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 4700
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    .line 4701
    .local v10, "audioId":Lcom/google/android/music/download/ContentIdentifier;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    .line 4702
    .local v11, "player":Lcom/google/android/music/playback/AsyncMediaPlayer;
    if-eqz v10, :cond_0

    if-eqz v11, :cond_0

    .line 4703
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUseLocalPlayer:Z

    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    invoke-virtual {v10}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v4

    invoke-interface {v11}, Lcom/google/android/music/playback/AsyncMediaPlayer;->position()J

    move-result-wide v6

    move-wide/from16 v8, p1

    invoke-virtual/range {v2 .. v9}, Lcom/google/android/music/eventlog/MusicEventLogger;->logSeekEventAsync(ZJJJ)V

    .line 4709
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    .line 4710
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 4711
    move-wide/from16 v12, p1

    .line 4712
    .local v12, "remoteSeekPos":J
    sget-object v2, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v3, Lcom/google/android/music/playback/LocalDevicePlayback$34;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v12, v13}, Lcom/google/android/music/playback/LocalDevicePlayback$34;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;J)V

    invoke-static {v2, v3}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 4746
    .end local v12    # "remoteSeekPos":J
    :cond_1
    const-wide/16 v14, -0x1

    :goto_1
    return-wide v14

    .line 4703
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 4725
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v2}, Lcom/google/android/music/playback/AsyncMediaPlayer;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4726
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-gez v2, :cond_4

    const-wide/16 p1, 0x0

    .line 4727
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v2}, Lcom/google/android/music/playback/AsyncMediaPlayer;->duration()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-ltz v2, :cond_5

    .line 4728
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->next(Z)V

    .line 4729
    const-wide/16 v14, -0x1

    goto :goto_1

    .line 4731
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayingLocally()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 4736
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->invalidateNextPlayer()V

    .line 4737
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-wide/from16 v0, p1

    invoke-interface {v2, v0, v1}, Lcom/google/android/music/playback/AsyncMediaPlayer;->seek(J)J

    move-result-wide v14

    .line 4738
    .local v14, "res":J
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->setNextTrack()V

    goto :goto_1

    .line 4741
    .end local v14    # "res":J
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-wide/from16 v0, p1

    invoke-interface {v2, v0, v1}, Lcom/google/android/music/playback/AsyncMediaPlayer;->seek(J)J

    move-result-wide v14

    goto :goto_1
.end method

.method public selectDefaultMediaRoute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5031
    iget-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUseLocalPlayer:Z

    if-nez v1, :cond_0

    .line 5032
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->stop()V

    .line 5035
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->tearDownRemotePlaybackClient()V

    .line 5037
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUseLocalPlayer:Z

    .line 5038
    iput-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mSelectedRouteId:Ljava/lang/String;

    .line 5039
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCastSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v1, v2}, Lcom/google/android/music/cast/CastSessionManager;->setSessionId(Ljava/lang/String;)V

    .line 5040
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->releaseWiFiLock()V

    .line 5041
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    const/16 v2, 0xc

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 5042
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 5043
    return-void
.end method

.method public setMediaRoute(ZLjava/lang/String;)V
    .locals 9
    .param p1, "localRoute"    # Z
    .param p2, "routeId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 5065
    iget-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUseLocalPlayer:Z

    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mSelectedRouteId:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mSelectedRouteId:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5105
    :cond_1
    :goto_0
    return-void

    .line 5070
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->position()J

    move-result-wide v4

    .line 5076
    .local v4, "position":J
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_4

    if-nez p1, :cond_4

    const/4 v3, 0x1

    .line 5077
    .local v3, "autoPlay":Z
    :goto_1
    if-nez v3, :cond_3

    .line 5078
    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->saveQueue(Z)V

    .line 5079
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mSelectedRouteId:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 5082
    iput-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z

    .line 5083
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onPlayStateChanged()V

    .line 5088
    :goto_2
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->releasePlayers()V

    .line 5091
    :cond_3
    iput-boolean p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUseLocalPlayer:Z

    .line 5092
    if-eqz p1, :cond_6

    .line 5093
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mSelectedRouteId:Ljava/lang/String;

    .line 5094
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->endSession()V

    .line 5095
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->releaseWiFiLock()V

    .line 5102
    :goto_3
    iget-object v7, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    const/16 v8, 0x9

    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;

    move v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;-><init>(ZLjava/lang/String;ZJ)V

    invoke-static {v7, v8, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    .line 5104
    .local v6, "message":Landroid/os/Message;
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .end local v3    # "autoPlay":Z
    .end local v6    # "message":Landroid/os/Message;
    :cond_4
    move v3, v0

    .line 5076
    goto :goto_1

    .line 5086
    .restart local v3    # "autoPlay":Z
    :cond_5
    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->stop(Z)V

    goto :goto_2

    .line 5097
    :cond_6
    iput-object p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mSelectedRouteId:Ljava/lang/String;

    .line 5098
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->endSession()V

    .line 5099
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->acquireWiFiLock()V

    goto :goto_3
.end method

.method public setMediaRouteVolume(Ljava/lang/String;D)V
    .locals 4
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "volume"    # D

    .prologue
    .line 5330
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    const/16 v2, 0xa

    new-instance v3, Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;

    invoke-direct {v3, p1, p2, p3}, Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;-><init>(Ljava/lang/String;D)V

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 5332
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 5333
    return-void
.end method

.method public setQueuePosition(I)V
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 4365
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v0, :cond_0

    .line 4366
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setQueuePosition: pos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4368
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    .line 4369
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    if-ne v0, p1, :cond_1

    .line 4386
    :goto_0
    return-void

    .line 4372
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mCloudQueueSessionLock:Ljava/lang/Object;

    monitor-enter v1

    .line 4373
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSwappingToCloudQueue:Z

    if-eqz v0, :cond_3

    .line 4374
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z

    if-eqz v0, :cond_2

    .line 4375
    const-string v0, "LocalDevicePlayback"

    const-string v2, "Suppressing queue position change request while switching to cloud queue mode"

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 4378
    :cond_2
    monitor-exit v1

    goto :goto_0

    .line 4380
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4381
    monitor-enter p0

    .line 4382
    const/4 v0, 0x1

    :try_start_2
    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->stop(Z)V

    .line 4383
    iput p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    .line 4384
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndPlay(Z)V

    .line 4385
    monitor-exit p0

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public setRating(I)V
    .locals 7
    .param p1, "rating"    # I

    .prologue
    const/4 v6, 0x1

    .line 4504
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    .line 4505
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getAudioId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v0

    .line 4506
    .local v0, "songId":Lcom/google/android/music/download/ContentIdentifier;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier;->isDefaultDomain()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier;->isNautilusDomain()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4507
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v2

    invoke-static {v1, v2, v3, p1}, Lcom/google/android/music/store/MusicContent$XAudio;->setRating(Landroid/content/ContentResolver;JI)V

    .line 4515
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/ContentIdentifier;->equals(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4516
    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->refreshCursor(Lcom/google/android/music/download/ContentIdentifier;)Z

    .line 4519
    :cond_1
    if-ne p1, v6, :cond_3

    .line 4522
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->THUMBS_UP_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->isInCloudQueueMode(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/store/Store;->caqIsContainerBroken(IZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4526
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->next()V

    .line 4530
    :cond_3
    if-eqz v0, :cond_4

    .line 4531
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v4

    iget-boolean v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mUseLocalPlayer:Z

    if-nez v2, :cond_5

    :goto_0
    move v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/eventlog/MusicEventLogger;->logSongRating(ILcom/google/android/music/store/ContainerDescriptor;JZ)V

    .line 4534
    :cond_4
    return-void

    .line 4531
    :cond_5
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public setRepeatMode(I)V
    .locals 2
    .param p1, "repeatmode"    # I

    .prologue
    .line 4307
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    if-eqz v0, :cond_1

    .line 4326
    :cond_0
    :goto_0
    return-void

    .line 4310
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    .line 4311
    monitor-enter p0

    .line 4312
    :try_start_0
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    if-ne p1, v0, :cond_2

    .line 4313
    monitor-exit p0

    goto :goto_0

    .line 4320
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 4315
    :cond_2
    :try_start_1
    iput p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    .line 4316
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->saveQueue(Z)V

    .line 4317
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    if-eqz v0, :cond_3

    .line 4318
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->setNextTrack()V

    .line 4320
    :cond_3
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4321
    const-string v0, "com.google.android.music.repeatmodechanged"

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V

    .line 4322
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isInCloudQueueMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4323
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I

    invoke-virtual {v0, v1}, Lcom/google/android/music/store/Store;->setCloudQueueRepeatMode(I)V

    .line 4324
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/cloudclient/CloudQueueManager;->setRepeatShuffleMode(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public setShuffleMode(I)V
    .locals 2
    .param p1, "shufflemode"    # I

    .prologue
    .line 4242
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;

    if-eqz v0, :cond_0

    .line 4298
    :goto_0
    return-void

    .line 4245
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    .line 4246
    monitor-enter p0

    .line 4247
    :try_start_0
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 4248
    monitor-exit p0

    goto :goto_0

    .line 4296
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 4250
    :cond_1
    :try_start_1
    iput p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I

    .line 4252
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$33;

    invoke-direct {v1, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$33;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 4296
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4297
    const-string v0, "com.google.android.music.shufflemodechanged"

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public shuffleAll()V
    .locals 2

    .prologue
    .line 2420
    new-instance v0, Lcom/google/android/music/medialist/AllSongsList;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/google/android/music/medialist/AllSongsList;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->shuffleSongs(Lcom/google/android/music/medialist/SongList;)V

    .line 2421
    return-void
.end method

.method public shuffleOnDevice()V
    .locals 2

    .prologue
    .line 2425
    new-instance v0, Lcom/google/android/music/medialist/AllOnDeviceSongsList;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/google/android/music/medialist/AllOnDeviceSongsList;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->shuffleSongs(Lcom/google/android/music/medialist/SongList;)V

    .line 2426
    return-void
.end method

.method public shuffleSongs(Lcom/google/android/music/medialist/SongList;)V
    .locals 2
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    const/4 v1, 0x1

    .line 2430
    const/4 v0, -0x1

    invoke-direct {p0, p1, v1, v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->open(Lcom/google/android/music/medialist/SongList;ZIZ)V

    .line 2431
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 3735
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserInteractionTime:J

    .line 3736
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 3737
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$30;

    invoke-direct {v1, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$30;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 3747
    return-void
.end method

.method public stopSync()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 3750
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getAudioId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v0

    .line 3751
    .local v0, "songId":Lcom/google/android/music/download/ContentIdentifier;
    if-eqz v0, :cond_0

    .line 3752
    const v1, 0x12111

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v4}, Lcom/google/android/music/playback/AsyncMediaPlayer;->getRemoteSongId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 3754
    const-string v1, "LocalDevicePlayback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Event logging MUSIC_STOP_PLAYBACK_REQUESTED: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-interface {v3}, Lcom/google/android/music/playback/AsyncMediaPlayer;->getRemoteSongId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3758
    :cond_0
    invoke-direct {p0, v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->stop(Z)V

    .line 3759
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->releasePlayers()V

    .line 3760
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onPlayStateChanged()V

    .line 3762
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback;->loadCurrent()Z

    .line 3763
    return-void
.end method

.method public supportsRating()Z
    .locals 1

    .prologue
    .line 4487
    const/4 v0, 0x1

    return v0
.end method

.method public updateMediaRouteVolume(Ljava/lang/String;D)V
    .locals 4
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "deltaRatio"    # D

    .prologue
    .line 5352
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    const/16 v2, 0xb

    new-instance v3, Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;

    invoke-direct {v3, p1, p2, p3}, Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;-><init>(Ljava/lang/String;D)V

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 5354
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 5356
    return-void
.end method
