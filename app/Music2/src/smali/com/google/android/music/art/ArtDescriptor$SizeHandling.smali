.class public final enum Lcom/google/android/music/art/ArtDescriptor$SizeHandling;
.super Ljava/lang/Enum;
.source "ArtDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SizeHandling"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/art/ArtDescriptor$SizeHandling;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

.field public static final enum EXACT:Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

.field public static final enum SLOPPY:Lcom/google/android/music/art/ArtDescriptor$SizeHandling;


# instance fields
.field private final mMaxPercent:F

.field private final mMinPercent:F


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 67
    new-instance v0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    const-string v1, "SLOPPY"

    const v2, 0x3f333333    # 0.7f

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;-><init>(Ljava/lang/String;IFF)V

    sput-object v0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->SLOPPY:Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    .line 71
    new-instance v0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    const-string v1, "EXACT"

    invoke-direct {v0, v1, v6, v4, v4}, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;-><init>(Ljava/lang/String;IFF)V

    sput-object v0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->EXACT:Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    .line 63
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    sget-object v1, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->SLOPPY:Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->EXACT:Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->$VALUES:[Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IFF)V
    .locals 0
    .param p3, "minPercent"    # F
    .param p4, "maxPercent"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 78
    iput p3, p0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->mMinPercent:F

    .line 79
    iput p4, p0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->mMaxPercent:F

    .line 80
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/art/ArtDescriptor$SizeHandling;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    const-class v0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/art/ArtDescriptor$SizeHandling;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->$VALUES:[Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    invoke-virtual {v0}, [Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    return-object v0
.end method


# virtual methods
.method public checkSizeAcceptable(FF)Z
    .locals 6
    .param p1, "requested"    # F
    .param p2, "given"    # F

    .prologue
    .line 90
    iget v3, p0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->mMaxPercent:F

    mul-float/2addr v3, p1

    float-to-int v0, v3

    .line 91
    .local v0, "max":I
    iget v3, p0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->mMinPercent:F

    mul-float/2addr v3, p1

    float-to-int v1, v3

    .line 92
    .local v1, "min":I
    const-string v3, "ArtResolver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Size requested: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", allowable range=("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    div-float v2, p2, p1

    .line 96
    .local v2, "ratio":F
    iget v3, p0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->mMinPercent:F

    cmpl-float v3, v2, v3

    if-lez v3, :cond_0

    iget v3, p0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->mMaxPercent:F

    cmpg-float v3, v2, v3

    if-gtz v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method
