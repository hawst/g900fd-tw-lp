.class public final Lcom/google/android/music/playback/session/RatingCompat;
.super Ljava/lang/Object;
.source "RatingCompat.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/playback/session/RatingCompat;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mRatingObj:Ljava/lang/Object;

.field private final mRatingStyle:I

.field private final mRatingValue:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    new-instance v0, Lcom/google/android/music/playback/session/RatingCompat$1;

    invoke-direct {v0}, Lcom/google/android/music/playback/session/RatingCompat$1;-><init>()V

    sput-object v0, Lcom/google/android/music/playback/session/RatingCompat;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(IF)V
    .locals 0
    .param p1, "ratingStyle"    # I
    .param p2, "rating"    # F

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput p1, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingStyle:I

    .line 69
    iput p2, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingValue:F

    .line 70
    return-void
.end method

.method synthetic constructor <init>(IFLcom/google/android/music/playback/session/RatingCompat$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # F
    .param p3, "x2"    # Lcom/google/android/music/playback/session/RatingCompat$1;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/playback/session/RatingCompat;-><init>(IF)V

    return-void
.end method

.method public static fromRating(Ljava/lang/Object;)Lcom/google/android/music/playback/session/RatingCompat;
    .locals 4
    .param p0, "ratingObj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 288
    if-eqz p0, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-ge v2, v3, :cond_1

    .line 318
    :cond_0
    :goto_0
    return-object v0

    .line 292
    :cond_1
    invoke-static {p0}, Lcom/google/android/music/playback/session/RatingCompatApi19;->getRatingStyle(Ljava/lang/Object;)I

    move-result v1

    .line 294
    .local v1, "ratingStyle":I
    invoke-static {p0}, Lcom/google/android/music/playback/session/RatingCompatApi19;->isRated(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 295
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 297
    :pswitch_0
    invoke-static {p0}, Lcom/google/android/music/playback/session/RatingCompatApi19;->hasHeart(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Lcom/google/android/music/playback/session/RatingCompat;->newHeartRating(Z)Lcom/google/android/music/playback/session/RatingCompat;

    move-result-object v0

    .line 317
    .local v0, "rating":Lcom/google/android/music/playback/session/RatingCompat;
    :goto_1
    iput-object p0, v0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingObj:Ljava/lang/Object;

    goto :goto_0

    .line 300
    .end local v0    # "rating":Lcom/google/android/music/playback/session/RatingCompat;
    :pswitch_1
    invoke-static {p0}, Lcom/google/android/music/playback/session/RatingCompatApi19;->isThumbUp(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Lcom/google/android/music/playback/session/RatingCompat;->newThumbRating(Z)Lcom/google/android/music/playback/session/RatingCompat;

    move-result-object v0

    .line 301
    .restart local v0    # "rating":Lcom/google/android/music/playback/session/RatingCompat;
    goto :goto_1

    .line 305
    .end local v0    # "rating":Lcom/google/android/music/playback/session/RatingCompat;
    :pswitch_2
    invoke-static {p0}, Lcom/google/android/music/playback/session/RatingCompatApi19;->getStarRating(Ljava/lang/Object;)F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/music/playback/session/RatingCompat;->newStarRating(IF)Lcom/google/android/music/playback/session/RatingCompat;

    move-result-object v0

    .line 307
    .restart local v0    # "rating":Lcom/google/android/music/playback/session/RatingCompat;
    goto :goto_1

    .line 309
    .end local v0    # "rating":Lcom/google/android/music/playback/session/RatingCompat;
    :pswitch_3
    invoke-static {p0}, Lcom/google/android/music/playback/session/RatingCompatApi19;->getPercentRating(Ljava/lang/Object;)F

    move-result v2

    invoke-static {v2}, Lcom/google/android/music/playback/session/RatingCompat;->newPercentageRating(F)Lcom/google/android/music/playback/session/RatingCompat;

    move-result-object v0

    .line 310
    .restart local v0    # "rating":Lcom/google/android/music/playback/session/RatingCompat;
    goto :goto_1

    .line 315
    .end local v0    # "rating":Lcom/google/android/music/playback/session/RatingCompat;
    :cond_2
    invoke-static {v1}, Lcom/google/android/music/playback/session/RatingCompat;->newUnratedRating(I)Lcom/google/android/music/playback/session/RatingCompat;

    move-result-object v0

    .restart local v0    # "rating":Lcom/google/android/music/playback/session/RatingCompat;
    goto :goto_1

    .line 295
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static newHeartRating(Z)Lcom/google/android/music/playback/session/RatingCompat;
    .locals 3
    .param p0, "hasHeart"    # Z

    .prologue
    .line 138
    new-instance v1, Lcom/google/android/music/playback/session/RatingCompat;

    const/4 v2, 0x1

    if-eqz p0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/google/android/music/playback/session/RatingCompat;-><init>(IF)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newPercentageRating(F)Lcom/google/android/music/playback/session/RatingCompat;
    .locals 2
    .param p0, "percent"    # F

    .prologue
    .line 195
    const/4 v0, 0x0

    cmpg-float v0, p0, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x42c80000    # 100.0f

    cmpl-float v0, p0, v0

    if-lez v0, :cond_1

    .line 196
    :cond_0
    const-string v0, "Rating"

    const-string v1, "Invalid percentage-based rating value"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const/4 v0, 0x0

    .line 199
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/music/playback/session/RatingCompat;

    const/4 v1, 0x6

    invoke-direct {v0, v1, p0}, Lcom/google/android/music/playback/session/RatingCompat;-><init>(IF)V

    goto :goto_0
.end method

.method public static newStarRating(IF)Lcom/google/android/music/playback/session/RatingCompat;
    .locals 5
    .param p0, "starRatingStyle"    # I
    .param p1, "starRating"    # F

    .prologue
    const/4 v1, 0x0

    .line 165
    const/high16 v0, -0x40800000    # -1.0f

    .line 166
    .local v0, "maxRating":F
    packed-switch p0, :pswitch_data_0

    .line 177
    const-string v2, "Rating"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid rating style ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") for a star rating"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :goto_0
    return-object v1

    .line 168
    :pswitch_0
    const/high16 v0, 0x40400000    # 3.0f

    .line 180
    :goto_1
    const/4 v2, 0x0

    cmpg-float v2, p1, v2

    if-ltz v2, :cond_0

    cmpl-float v2, p1, v0

    if-lez v2, :cond_1

    .line 181
    :cond_0
    const-string v2, "Rating"

    const-string v3, "Trying to set out of range star-based rating"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 171
    :pswitch_1
    const/high16 v0, 0x40800000    # 4.0f

    .line 172
    goto :goto_1

    .line 174
    :pswitch_2
    const/high16 v0, 0x40a00000    # 5.0f

    .line 175
    goto :goto_1

    .line 184
    :cond_1
    new-instance v1, Lcom/google/android/music/playback/session/RatingCompat;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/playback/session/RatingCompat;-><init>(IF)V

    goto :goto_0

    .line 166
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static newThumbRating(Z)Lcom/google/android/music/playback/session/RatingCompat;
    .locals 3
    .param p0, "thumbIsUp"    # Z

    .prologue
    .line 149
    new-instance v1, Lcom/google/android/music/playback/session/RatingCompat;

    const/4 v2, 0x2

    if-eqz p0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/google/android/music/playback/session/RatingCompat;-><init>(IF)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newUnratedRating(I)Lcom/google/android/music/playback/session/RatingCompat;
    .locals 2
    .param p0, "ratingStyle"    # I

    .prologue
    .line 117
    packed-switch p0, :pswitch_data_0

    .line 126
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 124
    :pswitch_0
    new-instance v0, Lcom/google/android/music/playback/session/RatingCompat;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/playback/session/RatingCompat;-><init>(IF)V

    goto :goto_0

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingStyle:I

    return v0
.end method

.method public getPercentRating()F
    .locals 2

    .prologue
    .line 271
    iget v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingStyle:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/playback/session/RatingCompat;->isRated()Z

    move-result v0

    if-nez v0, :cond_1

    .line 272
    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    .line 274
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingValue:F

    goto :goto_0
.end method

.method public getRating()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingObj:Ljava/lang/Object;

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_1

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingObj:Ljava/lang/Object;

    .line 355
    :goto_0
    return-object v0

    .line 334
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/playback/session/RatingCompat;->isRated()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 335
    iget v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingStyle:I

    packed-switch v0, :pswitch_data_0

    .line 350
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 337
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/playback/session/RatingCompat;->hasHeart()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/music/playback/session/RatingCompatApi19;->newHeartRating(Z)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingObj:Ljava/lang/Object;

    .line 355
    :goto_2
    iget-object v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingObj:Ljava/lang/Object;

    goto :goto_0

    .line 340
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/music/playback/session/RatingCompat;->isThumbUp()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/music/playback/session/RatingCompatApi19;->newThumbRating(Z)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingObj:Ljava/lang/Object;

    goto :goto_2

    .line 345
    :pswitch_2
    iget v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingStyle:I

    invoke-virtual {p0}, Lcom/google/android/music/playback/session/RatingCompat;->getStarRating()F

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/music/playback/session/RatingCompatApi19;->newStarRating(IF)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingObj:Ljava/lang/Object;

    goto :goto_2

    .line 348
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/music/playback/session/RatingCompat;->getPercentRating()F

    move-result v0

    invoke-static {v0}, Lcom/google/android/music/playback/session/RatingCompatApi19;->newPercentageRating(F)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingObj:Ljava/lang/Object;

    goto :goto_1

    .line 353
    :cond_2
    iget v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingStyle:I

    invoke-static {v0}, Lcom/google/android/music/playback/session/RatingCompatApi19;->newUnratedRating(I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingObj:Ljava/lang/Object;

    goto :goto_2

    .line 335
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getRatingStyle()I
    .locals 1

    .prologue
    .line 218
    iget v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingStyle:I

    return v0
.end method

.method public getStarRating()F
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingStyle:I

    packed-switch v0, :pswitch_data_0

    .line 261
    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    :goto_0
    return v0

    .line 257
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/playback/session/RatingCompat;->isRated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    iget v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingValue:F

    goto :goto_0

    .line 253
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public hasHeart()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 227
    iget v2, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingStyle:I

    if-eq v2, v0, :cond_0

    .line 230
    :goto_0
    return v1

    :cond_0
    iget v2, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingValue:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public isRated()Z
    .locals 2

    .prologue
    .line 208
    iget v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingValue:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isThumbUp()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 240
    iget v1, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingStyle:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    .line 243
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingValue:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Rating:style="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingStyle:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " rating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingValue:F

    const/4 v2, 0x0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    const-string v0, "unrated"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingValue:F

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 85
    iget v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingStyle:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    iget v0, p0, Lcom/google/android/music/playback/session/RatingCompat;->mRatingValue:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 87
    return-void
.end method
