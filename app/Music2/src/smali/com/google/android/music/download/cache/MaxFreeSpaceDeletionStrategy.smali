.class public Lcom/google/android/music/download/cache/MaxFreeSpaceDeletionStrategy;
.super Ljava/lang/Object;
.source "MaxFreeSpaceDeletionStrategy.java"

# interfaces
.implements Lcom/google/android/music/download/cache/DeletionStrategy;


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mFileSys:Lcom/google/android/music/download/cache/FileSystem;

.field private final mStore:Lcom/google/android/music/store/Store;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/cache/MaxFreeSpaceDeletionStrategy;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/download/cache/FileSystem;Lcom/google/android/music/store/Store;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileSys"    # Lcom/google/android/music/download/cache/FileSystem;
    .param p3, "store"    # Lcom/google/android/music/store/Store;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p2, p0, Lcom/google/android/music/download/cache/MaxFreeSpaceDeletionStrategy;->mFileSys:Lcom/google/android/music/download/cache/FileSystem;

    .line 31
    iput-object p3, p0, Lcom/google/android/music/download/cache/MaxFreeSpaceDeletionStrategy;->mStore:Lcom/google/android/music/store/Store;

    .line 32
    iput-object p1, p0, Lcom/google/android/music/download/cache/MaxFreeSpaceDeletionStrategy;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method


# virtual methods
.method public createSpace(JLcom/google/android/music/download/cache/CacheLocation;Lcom/google/android/music/download/cache/FilteredFileDeleter;)Z
    .locals 11
    .param p1, "space"    # J
    .param p3, "cacheLocation"    # Lcom/google/android/music/download/cache/CacheLocation;
    .param p4, "fileDeleter"    # Lcom/google/android/music/download/cache/FilteredFileDeleter;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 38
    const/4 v0, 0x0

    .line 41
    .local v0, "deletable":Landroid/database/Cursor;
    :try_start_0
    iget-object v8, p0, Lcom/google/android/music/download/cache/MaxFreeSpaceDeletionStrategy;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {p3}, Lcom/google/android/music/download/cache/CacheLocation;->getSchemaValueForStorageType()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/google/android/music/store/Store;->getDeletableFiles(I)Landroid/database/Cursor;

    move-result-object v0

    .line 42
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 43
    const/4 v8, 0x0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 44
    .local v2, "id":J
    const/4 v5, 0x0

    .line 45
    .local v5, "volUuid":Ljava/util/UUID;
    const/4 v8, 0x3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-nez v8, :cond_1

    .line 46
    const/4 v8, 0x3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 47
    .local v4, "volId":Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/music/utils/MusicUtils;->getUUIDFromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v5

    .line 49
    .end local v4    # "volId":Ljava/lang/String;
    :cond_1
    iget-object v8, p0, Lcom/google/android/music/download/cache/MaxFreeSpaceDeletionStrategy;->mContext:Landroid/content/Context;

    const/4 v9, 0x1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    invoke-static {v8, v9, v10, v5}, Lcom/google/android/music/download/cache/CacheUtils;->resolveMusicPath(Landroid/content/Context;Ljava/lang/String;ILjava/util/UUID;)Ljava/io/File;

    move-result-object v1

    .line 51
    .local v1, "file":Ljava/io/File;
    iget-object v8, p0, Lcom/google/android/music/download/cache/MaxFreeSpaceDeletionStrategy;->mFileSys:Lcom/google/android/music/download/cache/FileSystem;

    invoke-interface {v8, v1}, Lcom/google/android/music/download/cache/FileSystem;->delete(Ljava/io/File;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 52
    iget-object v8, p0, Lcom/google/android/music/download/cache/MaxFreeSpaceDeletionStrategy;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v8, v2, v3}, Lcom/google/android/music/store/Store;->removeFileLocation(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 56
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "id":J
    .end local v5    # "volUuid":Ljava/util/UUID;
    :catchall_0
    move-exception v6

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v6

    :cond_2
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 59
    iget-object v8, p0, Lcom/google/android/music/download/cache/MaxFreeSpaceDeletionStrategy;->mFileSys:Lcom/google/android/music/download/cache/FileSystem;

    invoke-virtual {p3}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/google/android/music/download/cache/FileSystem;->getFreeSpace(Ljava/io/File;)J

    move-result-wide v8

    cmp-long v8, p1, v8

    if-gez v8, :cond_3

    :goto_1
    return v6

    :cond_3
    move v6, v7

    goto :goto_1
.end method
