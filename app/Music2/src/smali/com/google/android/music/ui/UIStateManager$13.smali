.class Lcom/google/android/music/ui/UIStateManager$13;
.super Ljava/lang/Object;
.source "UIStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/UIStateManager;->refreshAccountStatus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/UIStateManager;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/UIStateManager;)V
    .locals 0

    .prologue
    .line 657
    iput-object p1, p0, Lcom/google/android/music/ui/UIStateManager$13;->this$0:Lcom/google/android/music/ui/UIStateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 660
    iget-object v4, p0, Lcom/google/android/music/ui/UIStateManager$13;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # getter for: Lcom/google/android/music/ui/UIStateManager;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v4}, Lcom/google/android/music/ui/UIStateManager;->access$1400(Lcom/google/android/music/ui/UIStateManager;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getNautilusStatus()Lcom/google/android/music/NautilusStatus;

    move-result-object v1

    .line 661
    .local v1, "currentStatus":Lcom/google/android/music/NautilusStatus;
    iget-object v4, p0, Lcom/google/android/music/ui/UIStateManager$13;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # getter for: Lcom/google/android/music/ui/UIStateManager;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v4}, Lcom/google/android/music/ui/UIStateManager;->access$1400(Lcom/google/android/music/ui/UIStateManager;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 662
    .local v0, "currentAccount":Landroid/accounts/Account;
    iget-object v4, p0, Lcom/google/android/music/ui/UIStateManager$13;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # getter for: Lcom/google/android/music/ui/UIStateManager;->mCachedNautilusStatus:Lcom/google/android/music/NautilusStatus;
    invoke-static {v4}, Lcom/google/android/music/ui/UIStateManager;->access$1500(Lcom/google/android/music/ui/UIStateManager;)Lcom/google/android/music/NautilusStatus;

    move-result-object v4

    if-ne v4, v1, :cond_0

    iget-object v4, p0, Lcom/google/android/music/ui/UIStateManager$13;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # getter for: Lcom/google/android/music/ui/UIStateManager;->mCachedSelectedAccount:Landroid/accounts/Account;
    invoke-static {v4}, Lcom/google/android/music/ui/UIStateManager;->access$1600(Lcom/google/android/music/ui/UIStateManager;)Landroid/accounts/Account;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/music/utils/MusicUtils;->safeEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 665
    :cond_0
    # getter for: Lcom/google/android/music/ui/UIStateManager;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->access$000()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 666
    const-string v4, "UIStateManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Nautilus status changed "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/ui/UIStateManager$13;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # getter for: Lcom/google/android/music/ui/UIStateManager;->mCachedNautilusStatus:Lcom/google/android/music/NautilusStatus;
    invoke-static {v6}, Lcom/google/android/music/ui/UIStateManager;->access$1500(Lcom/google/android/music/ui/UIStateManager;)Lcom/google/android/music/NautilusStatus;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/ui/UIStateManager$13;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # setter for: Lcom/google/android/music/ui/UIStateManager;->mCachedNautilusStatus:Lcom/google/android/music/NautilusStatus;
    invoke-static {v4, v1}, Lcom/google/android/music/ui/UIStateManager;->access$1502(Lcom/google/android/music/ui/UIStateManager;Lcom/google/android/music/NautilusStatus;)Lcom/google/android/music/NautilusStatus;

    .line 670
    iget-object v4, p0, Lcom/google/android/music/ui/UIStateManager$13;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # setter for: Lcom/google/android/music/ui/UIStateManager;->mCachedSelectedAccount:Landroid/accounts/Account;
    invoke-static {v4, v0}, Lcom/google/android/music/ui/UIStateManager;->access$1602(Lcom/google/android/music/ui/UIStateManager;Landroid/accounts/Account;)Landroid/accounts/Account;

    .line 672
    iget-object v4, p0, Lcom/google/android/music/ui/UIStateManager$13;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # getter for: Lcom/google/android/music/ui/UIStateManager;->mStateChangeListeners:Ljava/util/List;
    invoke-static {v4}, Lcom/google/android/music/ui/UIStateManager;->access$1700(Lcom/google/android/music/ui/UIStateManager;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    .line 673
    .local v3, "listener":Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;
    iget-object v4, p0, Lcom/google/android/music/ui/UIStateManager$13;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # getter for: Lcom/google/android/music/ui/UIStateManager;->mCachedSelectedAccount:Landroid/accounts/Account;
    invoke-static {v4}, Lcom/google/android/music/ui/UIStateManager;->access$1600(Lcom/google/android/music/ui/UIStateManager;)Landroid/accounts/Account;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/ui/UIStateManager$13;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # getter for: Lcom/google/android/music/ui/UIStateManager;->mCachedNautilusStatus:Lcom/google/android/music/NautilusStatus;
    invoke-static {v5}, Lcom/google/android/music/ui/UIStateManager;->access$1500(Lcom/google/android/music/ui/UIStateManager;)Lcom/google/android/music/NautilusStatus;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;->onAccountStatusUpdate(Landroid/accounts/Account;Lcom/google/android/music/NautilusStatus;)V

    goto :goto_0

    .line 677
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "listener":Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;
    :cond_2
    return-void
.end method
