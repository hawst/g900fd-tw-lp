.class Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;
.super Ljava/lang/Object;
.source "StorageMigrationService.java"

# interfaces
.implements Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/cache/StorageMigrationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ServiceCallbacks"
.end annotation


# instance fields
.field private mCallbackHandler:Landroid/os/Handler;

.field private mService:Lcom/google/android/music/download/cache/StorageMigrationService;

.field private final mStartId:I


# direct methods
.method constructor <init>(Lcom/google/android/music/download/cache/StorageMigrationService;I)V
    .locals 1
    .param p1, "service"    # Lcom/google/android/music/download/cache/StorageMigrationService;
    .param p2, "startId"    # I

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;->mService:Lcom/google/android/music/download/cache/StorageMigrationService;

    .line 62
    iput p2, p0, Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;->mStartId:I

    .line 64
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;->mCallbackHandler:Landroid/os/Handler;

    .line 65
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;)Lcom/google/android/music/download/cache/StorageMigrationService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;->mService:Lcom/google/android/music/download/cache/StorageMigrationService;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;Lcom/google/android/music/download/cache/StorageMigrationService;)Lcom/google/android/music/download/cache/StorageMigrationService;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;
    .param p1, "x1"    # Lcom/google/android/music/download/cache/StorageMigrationService;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;->mService:Lcom/google/android/music/download/cache/StorageMigrationService;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;

    .prologue
    .line 54
    iget v0, p0, Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;->mStartId:I

    return v0
.end method


# virtual methods
.method public onMigrationFinished()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;->mCallbackHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks$1;-><init>(Lcom/google/android/music/download/cache/StorageMigrationService$ServiceCallbacks;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 83
    return-void
.end method
