.class public final Lcom/google/android/music/ui/FragmentInfo;
.super Ljava/lang/Object;
.source "FragmentInfo.java"


# instance fields
.field private final mArgs:Landroid/os/Bundle;

.field private final mFragmentClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "fragmentArgs"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "fragmentClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/music/ui/FragmentInfo;->mFragmentClass:Ljava/lang/Class;

    .line 32
    iput-object p2, p0, Lcom/google/android/music/ui/FragmentInfo;->mArgs:Landroid/os/Bundle;

    .line 33
    return-void
.end method


# virtual methods
.method public instantiate(Landroid/content/Context;)Landroid/support/v4/app/Fragment;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    iget-object v1, p0, Lcom/google/android/music/ui/FragmentInfo;->mFragmentClass:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/FragmentInfo;->mArgs:Landroid/os/Bundle;

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 42
    .local v0, "result":Landroid/support/v4/app/Fragment;
    return-object v0
.end method
