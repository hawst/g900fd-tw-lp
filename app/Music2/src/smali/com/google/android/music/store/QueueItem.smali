.class public Lcom/google/android/music/store/QueueItem;
.super Ljava/lang/Object;
.source "QueueItem.java"


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private mCloudQueueId:Ljava/lang/String;

.field private mId:J

.field private mItemOrder:J

.field private mItemUnshuffledlOrder:J

.field private mMusicId:J

.field private mQueueContainerId:J

.field private mState:I

.field private mVersion:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 74
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "MusicId"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "QueueContainerId"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "State"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "ItemOrder"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "ItemUnshuffledOrder"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "CloudQueueVersion"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CloudQueueId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/QueueItem;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static compileInsertStatement(Landroid/database/sqlite/SQLiteDatabase;Z)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "isInCloudQueueMode"    # Z

    .prologue
    .line 192
    invoke-static {p1}, Lcom/google/android/music/store/QueueItem;->getInsertStatement(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method public static compileUpdateStatement(Landroid/database/sqlite/SQLiteDatabase;Z)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "isInCloudQueueMode"    # Z

    .prologue
    .line 197
    invoke-static {p1}, Lcom/google/android/music/store/QueueItem;->getUpdateStatement(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method private static final getInsertStatement(Z)Ljava/lang/String;
    .locals 3
    .param p0, "isInCloudQueueMode"    # Z

    .prologue
    .line 22
    if-eqz p0, :cond_0

    const-string v0, "CLOUD_QUEUE_ITEMS"

    .line 24
    .local v0, "tableName":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insert into "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MusicId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "QueueContainerId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "State"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ItemOrder"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ItemUnshuffledOrder"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "CloudQueueVersion"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "CloudQueueId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") values (?,?,?,?,?,?,?)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 22
    .end local v0    # "tableName":Ljava/lang/String;
    :cond_0
    const-string v0, "QUEUE_ITEMS"

    goto :goto_0
.end method

.method private static final getUpdateStatement(Z)Ljava/lang/String;
    .locals 3
    .param p0, "isInCloudQueueMode"    # Z

    .prologue
    .line 36
    if-eqz p0, :cond_0

    const-string v0, "CLOUD_QUEUE_ITEMS"

    .line 38
    .local v0, "tableName":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "update "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " set "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MusicId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "QueueContainerId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "State"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ItemOrder"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ItemUnshuffledOrder"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "CloudQueueVersion"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "CloudQueueId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " where "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 36
    .end local v0    # "tableName":Ljava/lang/String;
    :cond_0
    const-string v0, "QUEUE_ITEMS"

    goto/16 :goto_0
.end method

.method private prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 4
    .param p1, "statement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    .line 206
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 207
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mMusicId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 208
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mQueueContainerId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 209
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/music/store/QueueItem;->mState:I

    int-to-long v2, v1

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 210
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mItemOrder:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 211
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mItemUnshuffledlOrder:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 212
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mVersion:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 213
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/music/store/QueueItem;->mCloudQueueId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 214
    return-void
.end method

.method public static updateQueueItemsToVersion(Landroid/database/sqlite/SQLiteDatabase;JZ)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "version"    # J
    .param p3, "isInCloudQueueMode"    # Z

    .prologue
    .line 252
    if-eqz p3, :cond_0

    const-string v0, "CLOUD_QUEUE_ITEMS"

    .line 254
    .local v0, "tableName":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "update "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " set "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "CloudQueueVersion"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    .line 256
    .local v1, "updateStatement":Landroid/database/sqlite/SQLiteStatement;
    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 257
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 258
    return-void

    .line 252
    .end local v0    # "tableName":Ljava/lang/String;
    .end local v1    # "updateStatement":Landroid/database/sqlite/SQLiteStatement;
    :cond_0
    const-string v0, "QUEUE_ITEMS"

    goto :goto_0
.end method


# virtual methods
.method public generateNewCloudQueueId()V
    .locals 1

    .prologue
    .line 187
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/QueueItem;->mCloudQueueId:Ljava/lang/String;

    .line 188
    return-void
.end method

.method public getItemOrder()J
    .locals 2

    .prologue
    .line 155
    iget-wide v0, p0, Lcom/google/android/music/store/QueueItem;->mItemOrder:J

    return-wide v0
.end method

.method public getItemUnshuffledOrder()J
    .locals 2

    .prologue
    .line 163
    iget-wide v0, p0, Lcom/google/android/music/store/QueueItem;->mItemUnshuffledlOrder:J

    return-wide v0
.end method

.method public getMusicId()J
    .locals 2

    .prologue
    .line 131
    iget-wide v0, p0, Lcom/google/android/music/store/QueueItem;->mMusicId:J

    return-wide v0
.end method

.method public insert(Landroid/database/sqlite/SQLiteStatement;)J
    .locals 6
    .param p1, "insertStatement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    .line 217
    iget-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 218
    new-instance v2, Lcom/google/android/music/store/InvalidDataException;

    const-string v3, "The local id for queue item must not be set for an insert."

    invoke-direct {v2, v3}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 222
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/store/QueueItem;->prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V

    .line 223
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v0

    .line 224
    .local v0, "insertedId":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 225
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Failed to insert into queue items"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 227
    :cond_1
    iput-wide v0, p0, Lcom/google/android/music/store/QueueItem;->mId:J

    .line 230
    iget-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mId:J

    return-wide v2
.end method

.method public reset()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 95
    iput-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mId:J

    .line 96
    iput-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mMusicId:J

    .line 97
    iput-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mQueueContainerId:J

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/store/QueueItem;->mState:I

    .line 99
    iput-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mItemOrder:J

    .line 100
    iput-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mItemUnshuffledlOrder:J

    .line 101
    iput-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mVersion:J

    .line 102
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/store/QueueItem;->mCloudQueueId:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public setCloudQueueId(Ljava/lang/String;)V
    .locals 0
    .param p1, "cloudQueueId"    # Ljava/lang/String;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/android/music/store/QueueItem;->mCloudQueueId:Ljava/lang/String;

    .line 184
    return-void
.end method

.method public setId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 127
    iput-wide p1, p0, Lcom/google/android/music/store/QueueItem;->mId:J

    .line 128
    return-void
.end method

.method public setItemOrder(J)V
    .locals 1
    .param p1, "order"    # J

    .prologue
    .line 159
    iput-wide p1, p0, Lcom/google/android/music/store/QueueItem;->mItemOrder:J

    .line 160
    return-void
.end method

.method public setItemUnshuffledOrder(J)V
    .locals 1
    .param p1, "order"    # J

    .prologue
    .line 167
    iput-wide p1, p0, Lcom/google/android/music/store/QueueItem;->mItemUnshuffledlOrder:J

    .line 168
    return-void
.end method

.method public setMusicId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 135
    iput-wide p1, p0, Lcom/google/android/music/store/QueueItem;->mMusicId:J

    .line 136
    return-void
.end method

.method public setQueueContainerId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 143
    iput-wide p1, p0, Lcom/google/android/music/store/QueueItem;->mQueueContainerId:J

    .line 144
    return-void
.end method

.method public setState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 147
    iput p1, p0, Lcom/google/android/music/store/QueueItem;->mState:I

    .line 148
    return-void
.end method

.method public setVersion(J)V
    .locals 1
    .param p1, "version"    # J

    .prologue
    .line 175
    iput-wide p1, p0, Lcom/google/android/music/store/QueueItem;->mVersion:J

    .line 176
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 108
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    const-string v1, "mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    const-string v1, "mMusicId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mMusicId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    const-string v1, "mQueueContainerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mQueueContainerId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string v1, "mState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/QueueItem;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    const-string v1, "mItemOrder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mItemOrder:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string v1, "mItemUnshuffledlOrder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mItemUnshuffledlOrder:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    const-string v1, "mVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mVersion:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    const-string v1, "mCloudQueueId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/QueueItem;->mCloudQueueId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public update(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 4
    .param p1, "updateStatement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    .line 234
    iget-wide v0, p0, Lcom/google/android/music/store/QueueItem;->mId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 235
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Queue item cannot be updated before it\'s created"

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 238
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/store/QueueItem;->prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V

    .line 239
    const/16 v0, 0x8

    iget-wide v2, p0, Lcom/google/android/music/store/QueueItem;->mId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 240
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 241
    return-void
.end method
