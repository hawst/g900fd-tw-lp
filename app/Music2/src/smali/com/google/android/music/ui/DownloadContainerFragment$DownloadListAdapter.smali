.class Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;
.super Landroid/widget/BaseAdapter;
.source "DownloadContainerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/DownloadContainerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/DownloadContainerFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/DownloadContainerFragment;)V
    .locals 0

    .prologue
    .line 471
    iput-object p1, p0, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/DownloadContainerFragment;Lcom/google/android/music/ui/DownloadContainerFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/DownloadContainerFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/DownloadContainerFragment$1;

    .prologue
    .line 471
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;-><init>(Lcom/google/android/music/ui/DownloadContainerFragment;)V

    return-void
.end method

.method private newView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 546
    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    invoke-virtual {v3, v5}, Lcom/google/android/music/ui/DownloadContainerFragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 547
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f04002c

    invoke-virtual {v0, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 549
    .local v1, "vg":Landroid/view/ViewGroup;
    new-instance v2, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;

    invoke-direct {v2, v5}, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;-><init>(Lcom/google/android/music/ui/DownloadContainerFragment$1;)V

    .line 550
    .local v2, "vh":Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;
    const v3, 0x7f0e007c

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v3, v2, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->art:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 551
    const v3, 0x7f0e00d3

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->title:Landroid/widget/TextView;

    .line 552
    const v3, 0x7f0e00d4

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->subtitle:Landroid/widget/TextView;

    .line 553
    const v3, 0x7f0e010e

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/KeepOnView;

    iput-object v3, v2, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->keepOn:Lcom/google/android/music/KeepOnView;

    .line 554
    iget-object v3, v2, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->keepOn:Lcom/google/android/music/KeepOnView;

    invoke-virtual {v3, v4}, Lcom/google/android/music/KeepOnView;->setVisibility(I)V

    .line 555
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 556
    return-object v1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    # getter for: Lcom/google/android/music/ui/DownloadContainerFragment;->mListBackingData:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/ui/DownloadContainerFragment;->access$300(Lcom/google/android/music/ui/DownloadContainerFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    # getter for: Lcom/google/android/music/ui/DownloadContainerFragment;->mListBackingData:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/ui/DownloadContainerFragment;->access$300(Lcom/google/android/music/ui/DownloadContainerFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 471
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->getItem(I)Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 485
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->getItem(I)Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;

    move-result-object v0

    .line 486
    .local v0, "entry":Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;
    iget-wide v2, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->id:J

    return-wide v2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 20
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 491
    if-nez p2, :cond_0

    .line 492
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->newView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 495
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;

    .line 496
    .local v19, "vh":Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->getItem(I)Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;

    move-result-object v18

    .line 497
    .local v18, "entry":Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;
    move-object/from16 v0, v18

    iget-boolean v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->isRemoving:Z

    if-eqz v4, :cond_1

    .line 498
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    # getter for: Lcom/google/android/music/ui/DownloadContainerFragment;->mUnavailableCardOpacity:F
    invoke-static {v4}, Lcom/google/android/music/ui/DownloadContainerFragment;->access$600(Lcom/google/android/music/ui/DownloadContainerFragment;)F

    move-result v4

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 503
    :goto_0
    move-object/from16 v0, v18

    iget-wide v8, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->id:J

    move-object/from16 v0, v19

    iput-wide v8, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->id:J

    .line 504
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->containerType:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    move-object/from16 v0, v19

    iput-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->containerType:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    .line 505
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->title:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 506
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->subtitle:Landroid/widget/TextView;

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 508
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->containerType:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    sget-object v6, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->ALBUM:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    if-ne v4, v6, :cond_2

    .line 510
    new-instance v3, Lcom/google/android/music/medialist/AlbumSongList;

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->id:J

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->title:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JLjava/lang/String;Ljava/lang/String;Z)V

    .line 512
    .local v3, "albumSongList":Lcom/google/android/music/medialist/AlbumSongList;
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->art:Lcom/google/android/music/AsyncAlbumArtImageView;

    move-object/from16 v0, v18

    iget-wide v8, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->id:J

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->title:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v10, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    invoke-virtual {v4, v8, v9, v6, v10}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumId(JLjava/lang/String;Ljava/lang/String;)V

    .line 513
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->keepOn:Lcom/google/android/music/KeepOnView;

    invoke-virtual {v4, v3}, Lcom/google/android/music/KeepOnView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    .line 539
    .end local v3    # "albumSongList":Lcom/google/android/music/medialist/AlbumSongList;
    :goto_1
    new-instance v4, Lcom/google/android/music/ui/DownloadContainerFragment$onContainerRowClickListener;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    invoke-direct {v4, v6}, Lcom/google/android/music/ui/DownloadContainerFragment$onContainerRowClickListener;-><init>(Lcom/google/android/music/ui/DownloadContainerFragment;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 540
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->keepOn:Lcom/google/android/music/KeepOnView;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/google/android/music/KeepOnView;->setOnClick(Z)V

    .line 542
    return-object p2

    .line 500
    :cond_1
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    goto :goto_0

    .line 514
    :cond_2
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->containerType:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    sget-object v6, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->RADIO_STATION:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    if-ne v4, v6, :cond_4

    .line 515
    new-instance v5, Lcom/google/android/music/medialist/RadioStationSongList;

    move-object/from16 v0, v18

    iget-wide v6, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->id:J

    move-object/from16 v0, v18

    iget-object v8, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->remoteId:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v9, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->title:Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v5 .. v11}, Lcom/google/android/music/medialist/RadioStationSongList;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    .local v5, "songList":Lcom/google/android/music/medialist/RadioStationSongList;
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->artUrls:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 518
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->art:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v4}, Lcom/google/android/music/AsyncAlbumArtImageView;->showDefaultArtwork()V

    .line 524
    :goto_2
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->keepOn:Lcom/google/android/music/KeepOnView;

    invoke-virtual {v4, v5}, Lcom/google/android/music/KeepOnView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    goto :goto_1

    .line 521
    :cond_3
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->art:Lcom/google/android/music/AsyncAlbumArtImageView;

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->artUrls:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalArtRadio(Ljava/lang/String;)V

    goto :goto_2

    .line 526
    .end local v5    # "songList":Lcom/google/android/music/medialist/RadioStationSongList;
    :cond_4
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->art:Lcom/google/android/music/AsyncAlbumArtImageView;

    move-object/from16 v0, v18

    iget-wide v8, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->id:J

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->title:Ljava/lang/String;

    move-object/from16 v0, v18

    iget v10, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->listType:I

    invoke-virtual {v4, v8, v9, v6, v10}, Lcom/google/android/music/AsyncAlbumArtImageView;->setPlaylistAlbumArt(JLjava/lang/String;I)V

    .line 527
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->containerType:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    sget-object v6, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->USER_PLAYLIST:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    if-ne v4, v6, :cond_5

    .line 528
    new-instance v7, Lcom/google/android/music/medialist/PlaylistSongList;

    move-object/from16 v0, v18

    iget-wide v8, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->id:J

    move-object/from16 v0, v18

    iget-object v10, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->title:Ljava/lang/String;

    move-object/from16 v0, v18

    iget v11, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->listType:I

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1

    invoke-direct/range {v7 .. v17}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 530
    .local v7, "playlistSongList":Lcom/google/android/music/medialist/PlaylistSongList;
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->keepOn:Lcom/google/android/music/KeepOnView;

    invoke-virtual {v4, v7}, Lcom/google/android/music/KeepOnView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    goto/16 :goto_1

    .line 533
    .end local v7    # "playlistSongList":Lcom/google/android/music/medialist/PlaylistSongList;
    :cond_5
    move-object/from16 v0, v18

    iget-wide v8, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->id:J

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    invoke-static {v8, v9, v4, v6}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;

    move-result-object v2

    .line 535
    .local v2, "autoPlaylist":Lcom/google/android/music/medialist/AutoPlaylistSongList;
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->keepOn:Lcom/google/android/music/KeepOnView;

    invoke-virtual {v4, v2}, Lcom/google/android/music/KeepOnView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    goto/16 :goto_1
.end method
