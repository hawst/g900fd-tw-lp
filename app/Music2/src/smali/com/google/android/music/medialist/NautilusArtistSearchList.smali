.class public Lcom/google/android/music/medialist/NautilusArtistSearchList;
.super Lcom/google/android/music/medialist/NautilusArtistList;
.source "NautilusArtistSearchList.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/medialist/NautilusArtistSearchList;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mSearchString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/music/medialist/NautilusArtistSearchList$1;

    invoke-direct {v0}, Lcom/google/android/music/medialist/NautilusArtistSearchList$1;-><init>()V

    sput-object v0, Lcom/google/android/music/medialist/NautilusArtistSearchList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "searchString"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/music/medialist/NautilusArtistList;-><init>()V

    .line 22
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid search: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 25
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/medialist/NautilusArtistSearchList;->mSearchString:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public getArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/medialist/NautilusArtistSearchList;->mSearchString:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusArtistSearchList;->mSearchString:Ljava/lang/String;

    const-string v1, "artist"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Search;->getSearchUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusArtistSearchList;->mSearchString:Ljava/lang/String;

    return-object v0
.end method

.method public getNautilusId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusArtistSearchList;->mSearchString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    return-void
.end method
