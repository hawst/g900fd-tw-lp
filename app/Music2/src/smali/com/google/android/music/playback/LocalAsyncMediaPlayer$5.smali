.class Lcom/google/android/music/playback/LocalAsyncMediaPlayer$5;
.super Ljava/lang/Object;
.source "LocalAsyncMediaPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V
    .locals 0

    .prologue
    .line 943
    iput-object p1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$5;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1, "arg0"    # Landroid/media/MediaPlayer;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    const/4 v0, 0x1

    .line 946
    packed-switch p2, :pswitch_data_0

    .line 954
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 948
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$5;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->onWaitForBuffer()V
    invoke-static {v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$1000(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V

    goto :goto_0

    .line 951
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$5;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->onResumeFromBuffer()V
    invoke-static {v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$1100(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V

    goto :goto_0

    .line 946
    :pswitch_data_0
    .packed-switch 0x2bd
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
