.class Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;
.super Ljava/lang/Object;
.source "MediaSessionCompat.java"

# interfaces
.implements Lcom/google/android/music/playback/session/MediaSessionCompatApi21$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StubApi21"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;


# direct methods
.method private constructor <init>(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;)V
    .locals 0

    .prologue
    .line 412
    iput-object p1, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;Lcom/google/android/music/playback/session/MediaSessionCompat$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;
    .param p2, "x1"    # Lcom/google/android/music/playback/session/MediaSessionCompat$1;

    .prologue
    .line 412
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;-><init>(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;)V

    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
    .locals 1
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "cb"    # Landroid/os/ResultReceiver;

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onCommand(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V

    .line 417
    return-void
.end method

.method public onCustomAction(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onCustomAction(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 447
    return-void
.end method

.method public onFastForward()V
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onFastForward()V

    .line 467
    return-void
.end method

.method public onMediaButtonEvent(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "mediaButtonIntent"    # Landroid/content/Intent;

    .prologue
    .line 421
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onMediaButtonEvent(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onPause()V

    .line 452
    return-void
.end method

.method public onPlay()V
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onPlay()V

    .line 427
    return-void
.end method

.method public onPlayFromMediaId(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onPlayFromMediaId(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 432
    return-void
.end method

.method public onPlayFromSearch(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onPlayFromSearch(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 442
    return-void
.end method

.method public onRewind()V
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onRewind()V

    .line 472
    return-void
.end method

.method public onSeekTo(J)V
    .locals 1
    .param p1, "pos"    # J

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onSeekTo(J)V

    .line 482
    return-void
.end method

.method public onSetRating(Ljava/lang/Object;)V
    .locals 2
    .param p1, "ratingObj"    # Ljava/lang/Object;

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-static {p1}, Lcom/google/android/music/playback/session/RatingCompat;->fromRating(Ljava/lang/Object;)Lcom/google/android/music/playback/session/RatingCompat;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onSetRating(Lcom/google/android/music/playback/session/RatingCompat;)V

    .line 487
    return-void
.end method

.method public onSkipToNext()V
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onSkipToNext()V

    .line 457
    return-void
.end method

.method public onSkipToPrevious()V
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onSkipToPrevious()V

    .line 462
    return-void
.end method

.method public onSkipToQueueItem(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onSkipToQueueItem(J)V

    .line 437
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;->this$0:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onStop()V

    .line 477
    return-void
.end method
