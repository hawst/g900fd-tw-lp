.class Lcom/google/android/music/download/artwork/ArtDownloadService$IncomingMessengerHandler;
.super Landroid/os/Handler;
.source "ArtDownloadService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/artwork/ArtDownloadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IncomingMessengerHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/artwork/ArtDownloadService;


# direct methods
.method private constructor <init>(Lcom/google/android/music/download/artwork/ArtDownloadService;)V
    .locals 0

    .prologue
    .line 696
    iput-object p1, p0, Lcom/google/android/music/download/artwork/ArtDownloadService$IncomingMessengerHandler;->this$0:Lcom/google/android/music/download/artwork/ArtDownloadService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/download/artwork/ArtDownloadService;Lcom/google/android/music/download/artwork/ArtDownloadService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/download/artwork/ArtDownloadService;
    .param p2, "x1"    # Lcom/google/android/music/download/artwork/ArtDownloadService$1;

    .prologue
    .line 696
    invoke-direct {p0, p1}, Lcom/google/android/music/download/artwork/ArtDownloadService$IncomingMessengerHandler;-><init>(Lcom/google/android/music/download/artwork/ArtDownloadService;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 700
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 708
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "got what="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 702
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService$IncomingMessengerHandler;->this$0:Lcom/google/android/music/download/artwork/ArtDownloadService;

    # getter for: Lcom/google/android/music/download/artwork/ArtDownloadService;->mClients:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->access$400(Lcom/google/android/music/download/artwork/ArtDownloadService;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 710
    :goto_0
    return-void

    .line 705
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService$IncomingMessengerHandler;->this$0:Lcom/google/android/music/download/artwork/ArtDownloadService;

    # getter for: Lcom/google/android/music/download/artwork/ArtDownloadService;->mClients:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->access$400(Lcom/google/android/music/download/artwork/ArtDownloadService;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 700
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
