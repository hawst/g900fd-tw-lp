.class Lcom/google/android/music/ui/UIStateManager$6;
.super Lcom/google/android/music/utils/SafeServiceConnection;
.source "UIStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/UIStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/UIStateManager;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/UIStateManager;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/google/android/music/ui/UIStateManager$6;->this$0:Lcom/google/android/music/ui/UIStateManager;

    invoke-direct {p0}, Lcom/google/android/music/utils/SafeServiceConnection;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$6;->this$0:Lcom/google/android/music/ui/UIStateManager;

    invoke-static {p2}, Lcom/google/android/music/store/IStoreService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/store/IStoreService;

    move-result-object v1

    # setter for: Lcom/google/android/music/ui/UIStateManager;->mStoreService:Lcom/google/android/music/store/IStoreService;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->access$1002(Lcom/google/android/music/ui/UIStateManager;Lcom/google/android/music/store/IStoreService;)Lcom/google/android/music/store/IStoreService;

    .line 206
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$6;->this$0:Lcom/google/android/music/ui/UIStateManager;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/ui/UIStateManager;->mStoreService:Lcom/google/android/music/store/IStoreService;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->access$1002(Lcom/google/android/music/ui/UIStateManager;Lcom/google/android/music/store/IStoreService;)Lcom/google/android/music/store/IStoreService;

    .line 211
    return-void
.end method
