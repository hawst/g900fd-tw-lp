.class Lcom/google/android/music/playback/LocalDevicePlayback$13;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0

    .prologue
    .line 1602
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$13;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1615
    :try_start_0
    const-string v1, "lpa.decode"

    invoke-static {v1}, Lcom/google/android/music/utils/SystemUtils;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mDisableGaplessOverride:Ljava/lang/Boolean;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5702(Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 1617
    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mDisableGaplessOverride:Ljava/lang/Boolean;
    invoke-static {}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5700()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1618
    const-string v1, "LocalDevicePlayback"

    const-string v2, "LPAPlayer detected. Disabling gapless."

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1623
    :cond_0
    :goto_0
    return-void

    .line 1620
    :catch_0
    move-exception v0

    .line 1621
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "LocalDevicePlayback"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
