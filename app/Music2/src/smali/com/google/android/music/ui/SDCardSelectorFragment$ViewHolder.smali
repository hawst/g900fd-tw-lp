.class Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;
.super Ljava/lang/Object;
.source "SDCardSelectorFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/SDCardSelectorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewHolder"
.end annotation


# instance fields
.field mDescriptionView:Landroid/widget/TextView;

.field mRadioButton:Landroid/widget/RadioButton;

.field private mRootView:Landroid/view/View;

.field mSizeView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/SDCardSelectorFragment;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/SDCardSelectorFragment;Lcom/google/android/music/ui/SDCardSelectorFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/SDCardSelectorFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/SDCardSelectorFragment$1;

    .prologue
    .line 231
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;-><init>(Lcom/google/android/music/ui/SDCardSelectorFragment;)V

    return-void
.end method


# virtual methods
.method bindBaseViews(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 257
    iput-object p1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->mRootView:Landroid/view/View;

    .line 258
    const v0, 0x1020014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->mDescriptionView:Landroid/widget/TextView;

    .line 259
    const v0, 0x1020015

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->mSizeView:Landroid/widget/TextView;

    .line 260
    const v0, 0x7f0e00bc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    .line 261
    return-void
.end method

.method createView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 249
    const v1, 0x7f0400db

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 251
    .local v0, "v":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->bindBaseViews(Landroid/view/View;)V

    .line 252
    invoke-virtual {v0, p0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 253
    return-object v0
.end method

.method formatDescriptionLine(Lcom/google/android/music/download/cache/CacheLocation;)Ljava/lang/String;
    .locals 2
    .param p1, "location"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 264
    iget-object v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;
    invoke-static {v1}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$000(Lcom/google/android/music/ui/SDCardSelectorFragment;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/music/download/cache/CacheLocationManager;->getStorageMetadata(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    move-result-object v0

    .line 266
    .local v0, "meta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    instance-of v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->getDescription(ZLcom/google/android/music/download/cache/CacheLocation;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method formatStorageSize(Lcom/google/android/music/download/cache/CacheLocation;)Ljava/lang/String;
    .locals 7
    .param p1, "loc"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 270
    iget-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;
    invoke-static {v2}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$000(Lcom/google/android/music/ui/SDCardSelectorFragment;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/music/download/cache/CacheLocationManager;->getVolumeInformation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;

    move-result-object v1

    .line 272
    .local v1, "volInfo":Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    invoke-virtual {v1}, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->getFreeSpace()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mUnitStrings:[Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$1200(Lcom/google/android/music/ui/SDCardSelectorFragment;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/music/utils/IOUtils;->readableFileSize(J[Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 274
    .local v0, "free":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    const v3, 0x7f0b02b6

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method setDescription(Ljava/lang/String;)V
    .locals 1
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->mDescriptionView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    return-void
.end method

.method showFake()V
    .locals 10

    .prologue
    .line 279
    iget-object v4, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->mRootView:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mUnavailableOpacity:F
    invoke-static {v5}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$1300(Lcom/google/android/music/ui/SDCardSelectorFragment;)F

    move-result v5

    invoke-static {v4, v5}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 280
    iget-object v4, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mShowMissingCard:Z
    invoke-static {v4}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$500(Lcom/google/android/music/ui/SDCardSelectorFragment;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 281
    iget-object v4, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    invoke-virtual {v4}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 282
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getSelectedStorageVolumeCapacity()J

    move-result-wide v2

    .line 283
    .local v2, "total":J
    iget-object v4, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mUnitStrings:[Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$1200(Lcom/google/android/music/ui/SDCardSelectorFragment;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/music/utils/IOUtils;->readableFileSize(J[Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 284
    .local v1, "totalSpace":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->mDescriptionView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    const v6, 0x7f0b02bf

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    iget-object v4, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->mSizeView:Landroid/widget/TextView;

    const v5, 0x7f0b02c1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 291
    .end local v0    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v1    # "totalSpace":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "total":J
    :goto_0
    iget-object v4, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    iget-object v5, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mShowMissingCard:Z
    invoke-static {v5}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$500(Lcom/google/android/music/ui/SDCardSelectorFragment;)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 292
    return-void

    .line 288
    :cond_0
    iget-object v4, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->mDescriptionView:Landroid/widget/TextView;

    const v5, 0x7f0b02c0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 289
    iget-object v4, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->mSizeView:Landroid/widget/TextView;

    const v5, 0x7f0b02c2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method update(Lcom/google/android/music/download/cache/CacheLocation;)V
    .locals 2
    .param p1, "location"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->mRootView:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 239
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->formatDescriptionLine(Lcom/google/android/music/download/cache/CacheLocation;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->setDescription(Ljava/lang/String;)V

    .line 240
    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->mSizeView:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->formatStorageSize(Lcom/google/android/music/download/cache/CacheLocation;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    iget-object v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mShowMissingCard:Z
    invoke-static {v0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$500(Lcom/google/android/music/ui/SDCardSelectorFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mCurrentLocation:Lcom/google/android/music/download/cache/CacheLocation;
    invoke-static {v0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$1100(Lcom/google/android/music/ui/SDCardSelectorFragment;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/download/cache/CacheLocation;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 242
    return-void

    .line 241
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
