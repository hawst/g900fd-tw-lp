.class Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SelectedRoute"
.end annotation


# instance fields
.field public mAutoPlay:Z

.field public mLocalRoute:Z

.field public mPosition:J

.field public mRouteId:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;ZJ)V
    .locals 0
    .param p1, "localRoute"    # Z
    .param p2, "routeId"    # Ljava/lang/String;
    .param p3, "autoPlay"    # Z
    .param p4, "position"    # J

    .prologue
    .line 5435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5436
    iput-boolean p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;->mLocalRoute:Z

    .line 5437
    iput-object p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;->mRouteId:Ljava/lang/String;

    .line 5438
    iput-boolean p3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;->mAutoPlay:Z

    .line 5439
    iput-wide p4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;->mPosition:J

    .line 5440
    return-void
.end method
