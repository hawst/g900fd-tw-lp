.class Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener$1;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;)V
    .locals 0

    .prologue
    .line 1522
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener$1;->this$1:Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 1527
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener$1;->this$1:Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;

    iget-object v0, v0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->turnOffDownloadedOnlyMode()V

    .line 1529
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener$1;->this$1:Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;

    iget-object v0, v0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener$1;->this$1:Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;

    iget-object v0, v0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1531
    :cond_0
    :goto_0
    return-void

    .line 1530
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener$1;->this$1:Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;

    iget-object v0, v0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;->this$0:Lcom/google/android/music/ui/BaseActivity;

    # invokes: Lcom/google/android/music/ui/BaseActivity;->updateDownloadStripVisibility()V
    invoke-static {v0}, Lcom/google/android/music/ui/BaseActivity;->access$1400(Lcom/google/android/music/ui/BaseActivity;)V

    goto :goto_0
.end method
