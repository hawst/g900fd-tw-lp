.class public abstract Lcom/google/android/music/playback/DevicePlayback;
.super Ljava/lang/Object;
.source "DevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/playback/DevicePlayback$1;,
        Lcom/google/android/music/playback/DevicePlayback$State;
    }
.end annotation


# instance fields
.field protected final mService:Lcom/google/android/music/playback/MusicPlaybackService;


# direct methods
.method protected constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService;)V
    .locals 0
    .param p1, "service"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/music/playback/DevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    .line 30
    return-void
.end method


# virtual methods
.method public abstract cancelMix()V
.end method

.method public abstract clearQueue()V
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 0
    .param p1, "writer"    # Ljava/io/PrintWriter;

    .prologue
    .line 93
    return-void
.end method

.method public abstract duration()J
.end method

.method public abstract getAlbumArtUrl(J)Ljava/lang/String;
.end method

.method public abstract getAlbumId()J
.end method

.method public abstract getAlbumName()Ljava/lang/String;
.end method

.method public abstract getArtistId()J
.end method

.method public abstract getArtistName()Ljava/lang/String;
.end method

.method public abstract getAudioId()Lcom/google/android/music/download/ContentIdentifier;
.end method

.method public abstract getAudioSessionId()I
.end method

.method protected final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/playback/DevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    return-object v0
.end method

.method public abstract getCurrentTrackInfo()Lcom/google/android/music/playback/TrackInfo;
.end method

.method public abstract getErrorType()I
.end method

.method public abstract getLastUserInteractionTime()J
.end method

.method public abstract getMediaList()Lcom/google/android/music/medialist/SongList;
.end method

.method public abstract getMixState()Lcom/google/android/music/mix/MixGenerationState;
.end method

.method public abstract getPlaybackState()Lcom/google/android/music/playback/PlaybackState;
.end method

.method public abstract getPreviewPlayType()I
.end method

.method public abstract getQueuePosition()I
.end method

.method public abstract getQueueSize()I
.end method

.method public abstract getRating()I
.end method

.method public abstract getRepeatMode()I
.end method

.method public abstract getShuffleMode()I
.end method

.method public abstract getSongStoreId()Ljava/lang/String;
.end method

.method public abstract getSortableAlbumArtistName()Ljava/lang/String;
.end method

.method public abstract getState()Lcom/google/android/music/playback/DevicePlayback$State;
.end method

.method public abstract getTrackName()Ljava/lang/String;
.end method

.method public abstract hasLocal()Z
.end method

.method public abstract hasRemote()Z
.end method

.method public abstract hasValidPlaylist()Z
.end method

.method public abstract isCurrentSongLoaded()Z
.end method

.method public abstract isInErrorState()Z
.end method

.method public abstract isInFatalErrorState()Z
.end method

.method public abstract isInfiniteMixMode()Z
.end method

.method public abstract isPlaying()Z
.end method

.method public abstract isPlayingLocally()Z
.end method

.method public abstract isPreparing()Z
.end method

.method public abstract isStreaming()Z
.end method

.method public abstract isStreamingFullyBuffered()Z
.end method

.method public abstract next()V
.end method

.method protected final notifyChange(Ljava/lang/String;)V
    .locals 1
    .param p1, "what"    # Ljava/lang/String;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/playback/DevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/google/android/music/playback/DevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1, p0}, Lcom/google/android/music/playback/MusicPlaybackService;->notifyChange(Ljava/lang/String;Lcom/google/android/music/playback/DevicePlayback;)V

    .line 40
    :cond_0
    return-void
.end method

.method protected notifyFailure()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/playback/DevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/android/music/playback/DevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p0}, Lcom/google/android/music/playback/MusicPlaybackService;->notifyFailure(Lcom/google/android/music/playback/DevicePlayback;)V

    .line 46
    :cond_0
    return-void
.end method

.method protected onCreate()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public abstract open(Lcom/google/android/music/medialist/SongList;IZ)V
.end method

.method public abstract openAndQueue(Lcom/google/android/music/medialist/SongList;I)V
.end method

.method public abstract openMix(Lcom/google/android/music/mix/MixDescriptor;)V
.end method

.method public abstract pause()V
.end method

.method public abstract play()V
.end method

.method public abstract playNext(Lcom/google/android/music/medialist/SongList;I)V
.end method

.method public abstract playlistLoading()Z
.end method

.method public abstract position()J
.end method

.method public abstract prev()V
.end method

.method public abstract refreshRadio()V
.end method

.method protected saveState()V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public abstract seek(J)J
.end method

.method public abstract setQueuePosition(I)V
.end method

.method public abstract setRating(I)V
.end method

.method public abstract setRepeatMode(I)V
.end method

.method public abstract setShuffleMode(I)V
.end method

.method public abstract shuffleAll()V
.end method

.method public abstract shuffleOnDevice()V
.end method

.method public abstract shuffleSongs(Lcom/google/android/music/medialist/SongList;)V
.end method

.method public abstract stop()V
.end method

.method public abstract supportsRating()Z
.end method
