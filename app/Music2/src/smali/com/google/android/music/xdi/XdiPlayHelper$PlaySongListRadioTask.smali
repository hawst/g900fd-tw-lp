.class Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;
.super Landroid/os/AsyncTask;
.source "XdiPlayHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/xdi/XdiPlayHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlaySongListRadioTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/google/android/music/medialist/SongList;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/music/mix/MixDescriptor;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/xdi/XdiPlayHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/music/xdi/XdiPlayHelper;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;->this$0:Lcom/google/android/music/xdi/XdiPlayHelper;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/xdi/XdiPlayHelper;Lcom/google/android/music/xdi/XdiPlayHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/xdi/XdiPlayHelper;
    .param p2, "x1"    # Lcom/google/android/music/xdi/XdiPlayHelper$1;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;-><init>(Lcom/google/android/music/xdi/XdiPlayHelper;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 70
    check-cast p1, [Lcom/google/android/music/medialist/SongList;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;->doInBackground([Lcom/google/android/music/medialist/SongList;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/google/android/music/medialist/SongList;)Ljava/util/List;
    .locals 4
    .param p1, "params"    # [Lcom/google/android/music/medialist/SongList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/music/medialist/SongList;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/mix/MixDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 73
    array-length v0, p1

    if-lez v0, :cond_0

    .line 75
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/music/mix/MixDescriptor;

    iget-object v1, p0, Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;->this$0:Lcom/google/android/music/xdi/XdiPlayHelper;

    # getter for: Lcom/google/android/music/xdi/XdiPlayHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/xdi/XdiPlayHelper;->access$000(Lcom/google/android/music/xdi/XdiPlayHelper;)Landroid/content/Context;

    move-result-object v1

    aget-object v2, p1, v3

    invoke-static {v1, v2}, Lcom/google/android/music/utils/MusicUtils;->getSongListRadioMixDescriptor(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/music/ui/cardlib/utils/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    .line 78
    :cond_0
    const-string v0, "MusicXdi"

    const-string v1, "Empty SongList."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 70
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/mix/MixDescriptor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "descriptors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/mix/MixDescriptor;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 87
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/mix/MixDescriptor;

    .line 89
    .local v0, "descriptor":Lcom/google/android/music/mix/MixDescriptor;
    const-string v2, "MusicXdi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "playing radio.  mix: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/music/mix/MixDescriptor;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v2, p0, Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;->this$0:Lcom/google/android/music/xdi/XdiPlayHelper;

    # invokes: Lcom/google/android/music/xdi/XdiPlayHelper;->playRadio(Lcom/google/android/music/mix/MixDescriptor;)Z
    invoke-static {v2, v0}, Lcom/google/android/music/xdi/XdiPlayHelper;->access$100(Lcom/google/android/music/xdi/XdiPlayHelper;Lcom/google/android/music/mix/MixDescriptor;)Z

    move-result v1

    .line 96
    .end local v0    # "descriptor":Lcom/google/android/music/mix/MixDescriptor;
    .local v1, "status":Z
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;->this$0:Lcom/google/android/music/xdi/XdiPlayHelper;

    invoke-virtual {v2, v1}, Lcom/google/android/music/xdi/XdiPlayHelper;->callCompletionListener(Z)V

    .line 97
    return-void

    .line 93
    .end local v1    # "status":Z
    :cond_0
    const-string v2, "MusicXdi"

    const-string v3, "Invalid MixDescriptors for SongList."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const/4 v1, 0x0

    .restart local v1    # "status":Z
    goto :goto_0
.end method
