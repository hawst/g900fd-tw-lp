.class public final enum Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
.super Ljava/lang/Enum;
.source "MusicSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Feed"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

.field public static final enum BLACKLIST_ITEMS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

.field public static final enum CLOUD_QUEUE:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

.field public static final enum CONFIG:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

.field public static final enum CONFIG_ALARM:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

.field public static final enum PLAYLISTS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

.field public static final enum PLENTRIES:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

.field public static final enum RADIO_STATIONS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

.field public static final enum SOUND_SEARCH:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

.field public static final enum TRACKS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 205
    new-instance v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    const-string v1, "TRACKS"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->TRACKS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    .line 206
    new-instance v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    const-string v1, "PLAYLISTS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->PLAYLISTS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    .line 207
    new-instance v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    const-string v1, "PLENTRIES"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->PLENTRIES:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    .line 208
    new-instance v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    const-string v1, "RADIO_STATIONS"

    invoke-direct {v0, v1, v6}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->RADIO_STATIONS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    .line 209
    new-instance v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    const-string v1, "CONFIG"

    invoke-direct {v0, v1, v7}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->CONFIG:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    .line 210
    new-instance v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    const-string v1, "CONFIG_ALARM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->CONFIG_ALARM:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    .line 211
    new-instance v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    const-string v1, "SOUND_SEARCH"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->SOUND_SEARCH:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    .line 212
    new-instance v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    const-string v1, "BLACKLIST_ITEMS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->BLACKLIST_ITEMS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    .line 213
    new-instance v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    const-string v1, "CLOUD_QUEUE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->CLOUD_QUEUE:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    .line 204
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    sget-object v1, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->TRACKS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->PLAYLISTS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->PLENTRIES:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->RADIO_STATIONS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->CONFIG:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->CONFIG_ALARM:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->SOUND_SEARCH:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->BLACKLIST_ITEMS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->CLOUD_QUEUE:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->$VALUES:[Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 204
    const-class v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    .locals 1

    .prologue
    .line 204
    sget-object v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->$VALUES:[Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    invoke-virtual {v0}, [Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    return-object v0
.end method
