.class public final enum Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;
.super Ljava/lang/Enum;
.source "RemoteDeviceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/RemoteDeviceManager$Listener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScanError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

.field public static final enum NO_NETWORK_INTERFACES_FOUND:Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

.field public static final enum SSDP_CLIENT_SCAN_ERROR:Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

.field public static final enum SSDP_CLIENT_START_SCAN_ERROR:Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 103
    new-instance v0, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    const-string v1, "NO_NETWORK_INTERFACES_FOUND"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;->NO_NETWORK_INTERFACES_FOUND:Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    .line 104
    new-instance v0, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    const-string v1, "SSDP_CLIENT_START_SCAN_ERROR"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;->SSDP_CLIENT_START_SCAN_ERROR:Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    .line 105
    new-instance v0, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    const-string v1, "SSDP_CLIENT_SCAN_ERROR"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;->SSDP_CLIENT_SCAN_ERROR:Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    .line 102
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    sget-object v1, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;->NO_NETWORK_INTERFACES_FOUND:Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;->SSDP_CLIENT_START_SCAN_ERROR:Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;->SSDP_CLIENT_SCAN_ERROR:Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;->$VALUES:[Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 102
    const-class v0, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;->$VALUES:[Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    invoke-virtual {v0}, [Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    return-object v0
.end method
