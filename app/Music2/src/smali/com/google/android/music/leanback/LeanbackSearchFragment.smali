.class public Lcom/google/android/music/leanback/LeanbackSearchFragment;
.super Landroid/support/v17/leanback/app/SearchFragment;
.source "LeanbackSearchFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Landroid/support/v17/leanback/app/SearchFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 10
    invoke-super {p0, p1}, Landroid/support/v17/leanback/app/SearchFragment;->onCreate(Landroid/os/Bundle;)V

    .line 11
    const v0, 0x7f0b00e3

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackSearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackSearchFragment;->setTitle(Ljava/lang/String;)V

    .line 12
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackSearchFragment;->setBadgeDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 15
    return-void
.end method
