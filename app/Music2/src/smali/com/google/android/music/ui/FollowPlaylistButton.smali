.class public Lcom/google/android/music/ui/FollowPlaylistButton;
.super Lcom/google/android/music/ui/BaseActionButton;
.source "FollowPlaylistButton.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    const v0, 0x7f0b0230

    const v1, 0x7f020161

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/music/ui/BaseActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 22
    return-void
.end method

.method public static followPlaylist(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "medialist"    # Lcom/google/android/music/medialist/MediaList;

    .prologue
    .line 30
    if-eqz p1, :cond_1

    .line 31
    instance-of v0, p1, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-eqz v0, :cond_0

    .line 32
    check-cast p1, Lcom/google/android/music/medialist/SharedWithMeSongList;

    .end local p1    # "medialist":Lcom/google/android/music/medialist/MediaList;
    invoke-virtual {p1, p0}, Lcom/google/android/music/medialist/SharedWithMeSongList;->followPlaylist(Landroid/content/Context;)V

    .line 33
    const v0, 0x7f0b00d1

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 42
    :goto_0
    return-void

    .line 37
    .restart local p1    # "medialist":Lcom/google/android/music/medialist/MediaList;
    :cond_0
    const-string v0, "ActionButton"

    const-string v1, "The MediaList was not SharedWithMeSongList"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 40
    :cond_1
    const-string v0, "ActionButton"

    const-string v1, "MediaList is null"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected handleAction(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "medialist"    # Lcom/google/android/music/medialist/MediaList;

    .prologue
    .line 26
    invoke-static {p1, p2}, Lcom/google/android/music/ui/FollowPlaylistButton;->followPlaylist(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;)V

    .line 27
    return-void
.end method
