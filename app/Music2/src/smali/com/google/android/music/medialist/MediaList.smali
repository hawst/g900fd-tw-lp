.class public abstract Lcom/google/android/music/medialist/MediaList;
.super Ljava/lang/Object;
.source "MediaList.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/medialist/MediaList$AsyncQueryTask;,
        Lcom/google/android/music/medialist/MediaList$QueryParams;,
        Lcom/google/android/music/medialist/MediaList$MediaCursor;,
        Lcom/google/android/music/medialist/MediaList$OnQueryCompletionHandler;,
        Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/medialist/MediaList;",
            ">;"
        }
    .end annotation
.end field

.field public static final FLAG_ALBUM_INFO_EXPANDABLE_IN_MEDIA_PLAYBACK:I = 0x100

.field public static final FLAG_REWIND_SONG_ALLOWED:I = 0x20

.field public static final FLAG_SHOW_CONTAINER_CONTEXT_MENU:I = 0x1

.field public static final FLAG_SHOW_CONTEXT_MENU:I = 0x2

.field public static final FLAG_SHOW_SHUFFLE_OPTION:I = 0x4

.field public static final IMAGE_LARGE:I = 0x2

.field public static final IMAGE_THUMB:I = 0x1

.field private static final LOGTAG:Ljava/lang/String; = "MediaList"

.field private static final NULL:Ljava/lang/String; = "<null>"

.field private static final NUMERIC_ARRAY_DELIMITER:C = ':'


# instance fields
.field private final mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

.field private mFlags:I

.field protected final mShouldFilter:Z

.field protected final mShouldIncludeExternal:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 729
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/music/medialist/MediaList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/google/android/music/download/ContentIdentifier$Domain;ZZ)V
    .locals 8
    .param p1, "domain"    # Lcom/google/android/music/download/ContentIdentifier$Domain;
    .param p2, "shouldFilter"    # Z
    .param p3, "shouldIncludeExternal"    # Z

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const v5, 0x7fffffff

    iput v5, p0, Lcom/google/android/music/medialist/MediaList;->mFlags:I

    .line 82
    iput-object p1, p0, Lcom/google/android/music/medialist/MediaList;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    .line 83
    iput-boolean p2, p0, Lcom/google/android/music/medialist/MediaList;->mShouldFilter:Z

    .line 84
    iput-boolean p3, p0, Lcom/google/android/music/medialist/MediaList;->mShouldIncludeExternal:Z

    .line 85
    invoke-virtual {p0}, Lcom/google/android/music/medialist/MediaList;->isDefaultDomain()Z

    move-result v5

    if-nez v5, :cond_0

    instance-of v5, p0, Lcom/google/android/music/medialist/ExternalSongList;

    if-nez v5, :cond_0

    instance-of v5, p0, Lcom/google/android/music/medialist/NautilusMediaList;

    if-nez v5, :cond_0

    .line 87
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Media list must be ExternalSongList or NautilusMediaList if domain is not default"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 96
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 97
    .local v0, "c":Ljava/lang/Class;
    invoke-virtual {v0}, Ljava/lang/Class;->getConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 98
    .local v1, "ctors":[Ljava/lang/reflect/Constructor;, "[Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v1

    if-ge v3, v5, :cond_2

    .line 99
    aget-object v5, v1, v3

    invoke-virtual {v5}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v4

    .line 100
    .local v4, "params":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    array-length v5, v4

    if-eqz v5, :cond_1

    .line 103
    :try_start_0
    const-string v6, "getArgs"

    const/4 v5, 0x0

    check-cast v5, [Ljava/lang/Class;

    invoke-virtual {v0, v6, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    .line 98
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 104
    :catch_0
    move-exception v2

    .line 105
    .local v2, "e":Ljava/lang/SecurityException;
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " must implement getArgs"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 106
    .end local v2    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v2

    .line 107
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " must implement getArgs"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 112
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    .end local v4    # "params":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :cond_2
    return-void
.end method

.method private static decodeLongsArg(Ljava/lang/String;)[J
    .locals 6
    .param p0, "encoded"    # Ljava/lang/String;

    .prologue
    .line 774
    if-eqz p0, :cond_0

    const-string v4, "<null>"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 775
    :cond_0
    const/4 v2, 0x0

    .line 786
    :cond_1
    :goto_0
    return-object v2

    .line 776
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_3

    .line 777
    const/4 v4, 0x0

    new-array v2, v4, [J

    goto :goto_0

    .line 779
    :cond_3
    const/16 v4, 0x3a

    invoke-static {v4}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 780
    .local v3, "stringValues":[Ljava/lang/String;
    array-length v1, v3

    .line 781
    .local v1, "length":I
    array-length v4, v3

    new-array v2, v4, [J

    .line 782
    .local v2, "longs":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 783
    aget-object v4, v3, v0

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    aput-wide v4, v2, v0

    .line 782
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private static decodeStringsArg(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p0, "encoded"    # Ljava/lang/String;

    .prologue
    .line 795
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static encodeArg([J)Ljava/lang/String;
    .locals 1
    .param p0, "longs"    # [J

    .prologue
    .line 756
    if-nez p0, :cond_0

    .line 757
    const-string v0, "<null>"

    .line 761
    :goto_0
    return-object v0

    .line 758
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 759
    const-string v0, ""

    goto :goto_0

    .line 761
    :cond_1
    const/16 v0, 0x3a

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/common/primitives/Longs;->join(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected static encodeArg([Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "strings"    # [Ljava/lang/String;

    .prologue
    .line 770
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->encodeStringArray([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static thaw(Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList;
    .locals 26
    .param p0, "args"    # Ljava/lang/String;

    .prologue
    .line 123
    const/16 v23, 0x2c

    :try_start_0
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    .line 124
    .local v6, "comma":I
    if-lez v6, :cond_1

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 127
    .local v14, "name":Ljava/lang/String;
    :goto_0
    invoke-static {v14}, Lcom/google/android/music/medialist/MediaList;->thawClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    .line 128
    .local v5, "c":Ljava/lang/Class;, "Ljava/lang/Class<Lcom/google/android/music/medialist/MediaList;>;"
    invoke-virtual {v5}, Ljava/lang/Class;->getConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v8

    check-cast v8, [Ljava/lang/reflect/Constructor;

    .line 129
    .local v8, "ctors":[Ljava/lang/reflect/Constructor;, "[Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    array-length v0, v8

    move/from16 v18, v0

    .line 130
    .local v18, "numctors":I
    const/4 v7, 0x0

    .line 133
    .local v7, "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    if-gez v6, :cond_2

    .line 134
    const/16 v21, 0x0

    .line 140
    .local v21, "splitargs":[Ljava/lang/String;
    :goto_1
    const/16 v23, 0x1

    move/from16 v0, v18

    move/from16 v1, v23

    if-ne v0, v1, :cond_3

    .line 141
    const/16 v23, 0x0

    aget-object v7, v8, v23

    .line 166
    :cond_0
    invoke-virtual {v7}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v19

    .line 167
    .local v19, "paramtypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v17, v0

    .line 168
    .local v17, "numargs":I
    if-nez v17, :cond_8

    .line 170
    const/16 v23, 0x0

    check-cast v23, [Ljava/lang/Object;

    move-object/from16 v0, v23

    invoke-virtual {v7, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/google/android/music/medialist/MediaList;

    .line 219
    .end local v5    # "c":Ljava/lang/Class;, "Ljava/lang/Class<Lcom/google/android/music/medialist/MediaList;>;"
    .end local v6    # "comma":I
    .end local v7    # "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    .end local v8    # "ctors":[Ljava/lang/reflect/Constructor;, "[Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    .end local v14    # "name":Ljava/lang/String;
    .end local v17    # "numargs":I
    .end local v18    # "numctors":I
    .end local v19    # "paramtypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v21    # "splitargs":[Ljava/lang/String;
    :goto_2
    return-object v23

    .restart local v6    # "comma":I
    :cond_1
    move-object/from16 v14, p0

    .line 124
    goto :goto_0

    .line 136
    .restart local v5    # "c":Ljava/lang/Class;, "Ljava/lang/Class<Lcom/google/android/music/medialist/MediaList;>;"
    .restart local v7    # "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    .restart local v8    # "ctors":[Ljava/lang/reflect/Constructor;, "[Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    .restart local v14    # "name":Ljava/lang/String;
    .restart local v18    # "numctors":I
    :cond_2
    add-int/lit8 v23, v6, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 137
    .local v2, "actualargs":Ljava/lang/String;
    const-string v23, ","

    const/16 v24, -0x1

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v21

    .restart local v21    # "splitargs":[Ljava/lang/String;
    goto :goto_1

    .line 144
    .end local v2    # "actualargs":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/google/common/collect/Sets;->newTreeSet()Ljava/util/TreeSet;

    move-result-object v10

    .line 145
    .local v10, "foundArgLengths":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    if-nez v21, :cond_4

    const/16 v16, 0x0

    .line 146
    .local v16, "numProvidedArgs":I
    :goto_3
    move-object v4, v8

    .local v4, "arr$":[Ljava/lang/reflect/Constructor;
    array-length v13, v4

    .local v13, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_4
    if-ge v12, v13, :cond_7

    aget-object v20, v4, v12

    .line 147
    .local v20, "possibleConstructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    invoke-virtual/range {v20 .. v20}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v23

    move-object/from16 v0, v23

    array-length v15, v0

    .line 148
    .local v15, "numArgs":I
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-interface {v10, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 149
    new-instance v23, Ljava/lang/IllegalArgumentException;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Class: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " has multiple "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "constructors with the same number of parameters: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v23
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    .end local v4    # "arr$":[Ljava/lang/reflect/Constructor;
    .end local v5    # "c":Ljava/lang/Class;, "Ljava/lang/Class<Lcom/google/android/music/medialist/MediaList;>;"
    .end local v6    # "comma":I
    .end local v7    # "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    .end local v8    # "ctors":[Ljava/lang/reflect/Constructor;, "[Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    .end local v10    # "foundArgLengths":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    .end local v14    # "name":Ljava/lang/String;
    .end local v15    # "numArgs":I
    .end local v16    # "numProvidedArgs":I
    .end local v18    # "numctors":I
    .end local v20    # "possibleConstructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    .end local v21    # "splitargs":[Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 217
    .local v9, "e":Ljava/lang/Exception;
    const-string v23, "MediaList"

    const-string v24, "Error thawing medialist"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-static {v0, v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 219
    const/16 v23, 0x0

    goto :goto_2

    .line 145
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v5    # "c":Ljava/lang/Class;, "Ljava/lang/Class<Lcom/google/android/music/medialist/MediaList;>;"
    .restart local v6    # "comma":I
    .restart local v7    # "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    .restart local v8    # "ctors":[Ljava/lang/reflect/Constructor;, "[Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    .restart local v10    # "foundArgLengths":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v14    # "name":Ljava/lang/String;
    .restart local v18    # "numctors":I
    .restart local v21    # "splitargs":[Ljava/lang/String;
    :cond_4
    :try_start_1
    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v16, v0

    goto :goto_3

    .line 152
    .restart local v4    # "arr$":[Ljava/lang/reflect/Constructor;
    .restart local v12    # "i$":I
    .restart local v13    # "len$":I
    .restart local v15    # "numArgs":I
    .restart local v16    # "numProvidedArgs":I
    .restart local v20    # "possibleConstructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    :cond_5
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-interface {v10, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 153
    move/from16 v0, v16

    if-ne v0, v15, :cond_6

    .line 156
    move-object/from16 v7, v20

    .line 146
    :cond_6
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    .line 159
    .end local v15    # "numArgs":I
    .end local v20    # "possibleConstructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Lcom/google/android/music/medialist/MediaList;>;"
    :cond_7
    if-nez v7, :cond_0

    .line 160
    new-instance v23, Ljava/lang/IllegalArgumentException;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Could not find a constructor for class: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " with "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " arguments"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 172
    .end local v4    # "arr$":[Ljava/lang/reflect/Constructor;
    .end local v10    # "foundArgLengths":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    .end local v16    # "numProvidedArgs":I
    .restart local v17    # "numargs":I
    .restart local v19    # "paramtypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :cond_8
    if-gez v6, :cond_9

    .line 173
    new-instance v23, Ljava/lang/IllegalArgumentException;

    const-string v24, "no args found"

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 176
    :cond_9
    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v17

    move/from16 v1, v23

    if-eq v0, v1, :cond_a

    .line 177
    const-string v23, "MediaList"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Argument count mismatch: got "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", expected "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    const/16 v23, 0x0

    goto/16 :goto_2

    .line 181
    :cond_a
    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    .line 182
    .local v22, "thawedArgs":[Ljava/lang/Object;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_5
    move/from16 v0, v17

    if-ge v11, v0, :cond_1c

    .line 184
    aget-object v23, v21, v11

    invoke-static/range {v23 .. v23}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 185
    .local v3, "arg":Ljava/lang/String;
    const-string v23, "<null>"

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 186
    const/4 v3, 0x0

    .line 188
    :cond_b
    aget-object v23, v19, v11

    sget-object v24, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_d

    .line 189
    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v23

    aput-object v23, v22, v11

    .line 182
    :cond_c
    :goto_6
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    .line 190
    :cond_d
    aget-object v23, v19, v11

    const-class v24, Ljava/lang/String;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_e

    .line 191
    aput-object v3, v22, v11

    goto :goto_6

    .line 192
    :cond_e
    aget-object v23, v19, v11

    sget-object v24, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_f

    .line 193
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v22, v11

    goto :goto_6

    .line 194
    :cond_f
    aget-object v23, v19, v11

    const-class v24, [J

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_10

    .line 195
    invoke-static {v3}, Lcom/google/android/music/medialist/MediaList;->decodeLongsArg(Ljava/lang/String;)[J

    move-result-object v23

    aput-object v23, v22, v11

    goto :goto_6

    .line 196
    :cond_10
    aget-object v23, v19, v11

    const-class v24, [Ljava/lang/String;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_11

    .line 197
    invoke-static {v3}, Lcom/google/android/music/medialist/MediaList;->decodeStringsArg(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    aput-object v23, v22, v11

    goto :goto_6

    .line 198
    :cond_11
    aget-object v23, v19, v11

    sget-object v24, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_12

    .line 199
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v23

    aput-object v23, v22, v11

    goto :goto_6

    .line 200
    :cond_12
    aget-object v23, v19, v11

    const-class v24, Landroid/net/Uri;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_14

    .line 201
    if-nez v3, :cond_13

    const/16 v23, 0x0

    :goto_7
    aput-object v23, v22, v11

    goto :goto_6

    :cond_13
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v23

    goto :goto_7

    .line 202
    :cond_14
    aget-object v23, v19, v11

    const-class v24, Lcom/google/android/music/medialist/SongData;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_16

    .line 203
    if-nez v3, :cond_15

    const/16 v23, 0x0

    :goto_8
    aput-object v23, v22, v11

    goto :goto_6

    :cond_15
    invoke-static {v3}, Lcom/google/android/music/medialist/SongData;->parseFromJson(Ljava/lang/String;)Lcom/google/android/music/medialist/SongData;

    move-result-object v23

    goto :goto_8

    .line 204
    :cond_16
    aget-object v23, v19, v11

    const-class v24, Lcom/google/android/music/medialist/SongDataList;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_18

    .line 205
    if-nez v3, :cond_17

    const/16 v23, 0x0

    :goto_9
    aput-object v23, v22, v11

    goto/16 :goto_6

    :cond_17
    invoke-static {v3}, Lcom/google/android/music/medialist/SongDataList;->parseFromJson(Ljava/lang/String;)Lcom/google/android/music/medialist/SongDataList;

    move-result-object v23

    goto :goto_9

    .line 206
    :cond_18
    aget-object v23, v19, v11

    const-class v24, Lcom/google/android/music/sync/google/model/Track;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_1a

    .line 207
    if-nez v3, :cond_19

    const/16 v23, 0x0

    :goto_a
    aput-object v23, v22, v11

    goto/16 :goto_6

    :cond_19
    const-class v23, Lcom/google/android/music/sync/google/model/Track;

    move-object/from16 v0, v23

    invoke-static {v0, v3}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonString(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/google/android/music/sync/google/model/Track;

    goto :goto_a

    .line 209
    :cond_1a
    aget-object v23, v19, v11

    const-class v24, Lcom/google/android/music/store/ContainerDescriptor;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_c

    .line 210
    if-nez v3, :cond_1b

    const/16 v23, 0x0

    :goto_b
    aput-object v23, v22, v11

    goto/16 :goto_6

    :cond_1b
    invoke-static {v3}, Lcom/google/android/music/store/ContainerDescriptor;->parseFromExportString(Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v23

    goto :goto_b

    .line 215
    .end local v3    # "arg":Ljava/lang/String;
    :cond_1c
    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/google/android/music/medialist/MediaList;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method

.method static thawClass(Ljava/lang/String;)Ljava/lang/Class;
    .locals 5
    .param p0, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<",
            "Lcom/google/android/music/medialist/MediaList;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 230
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 237
    :goto_0
    return-object v3

    .line 231
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v2, "com.android.music."

    .line 233
    .local v2, "oldPackagePrefix":Ljava/lang/String;
    const-string v3, "com.android.music."

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 235
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "com.google.android.music."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "com.android.music."

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 237
    .local v1, "newName":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    goto :goto_0

    .line 239
    .end local v1    # "newName":Ljava/lang/String;
    :cond_0
    throw v0
.end method


# virtual methods
.method protected final clearFlag(I)V
    .locals 2
    .param p1, "flag"    # I

    .prologue
    .line 268
    iget v0, p0, Lcom/google/android/music/medialist/MediaList;->mFlags:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/medialist/MediaList;->mFlags:I

    .line 269
    return-void
.end method

.method protected createAsyncCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/music/AsyncCursor;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cols"    # [Ljava/lang/String;
    .param p3, "filter"    # Ljava/lang/String;
    .param p4, "isHighPriority"    # Z

    .prologue
    const/4 v4, 0x0

    .line 553
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/MediaList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v10

    .line 554
    .local v10, "uri":Landroid/net/Uri;
    if-nez v10, :cond_0

    .line 557
    :goto_0
    return-object v4

    :cond_0
    new-instance v0, Lcom/google/android/music/AsyncCursor;

    invoke-virtual {p0, v10, p3}, Lcom/google/android/music/medialist/MediaList;->filterUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-boolean v8, p0, Lcom/google/android/music/medialist/MediaList;->mShouldFilter:Z

    iget-boolean v9, p0, Lcom/google/android/music/medialist/MediaList;->mShouldIncludeExternal:Z

    move-object v1, p1

    move-object v3, p2

    move-object v5, v4

    move-object v6, v4

    move v7, p4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/music/AsyncCursor;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZZ)V

    move-object v4, v0

    goto :goto_0
.end method

.method protected createMediaCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/medialist/MediaList$MediaCursor;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 493
    new-instance v0, Lcom/google/android/music/medialist/MediaList$MediaCursor;

    invoke-direct {v0, p2}, Lcom/google/android/music/medialist/MediaList$MediaCursor;-><init>(Landroid/database/Cursor;)V

    return-object v0
.end method

.method protected createSyncCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cols"    # [Ljava/lang/String;
    .param p3, "filter"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 532
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/MediaList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v8

    .line 533
    .local v8, "uri":Landroid/net/Uri;
    if-nez v8, :cond_0

    .line 536
    :goto_0
    return-object v3

    :cond_0
    invoke-virtual {p0, v8, p3}, Lcom/google/android/music/medialist/MediaList;->filterUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-boolean v6, p0, Lcom/google/android/music/medialist/MediaList;->mShouldFilter:Z

    iget-boolean v7, p0, Lcom/google/android/music/medialist/MediaList;->mShouldIncludeExternal:Z

    move-object v0, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 732
    const/4 v0, 0x0

    return v0
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 5
    .param p1, "writer"    # Ljava/io/PrintWriter;

    .prologue
    .line 295
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  class="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 296
    invoke-virtual {p0}, Lcom/google/android/music/medialist/MediaList;->getArgs()[Ljava/lang/String;

    move-result-object v1

    .line 297
    .local v1, "args":[Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v3, v1

    if-nez v3, :cond_1

    .line 311
    :cond_0
    return-void

    .line 300
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    .line 301
    const-string v3, "    "

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 302
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 303
    const-string v3, ": "

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 304
    aget-object v0, v1, v2

    .line 305
    .local v0, "arg":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 306
    const-string v3, "    <null>"

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 300
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 308
    :cond_2
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 325
    if-eqz p1, :cond_0

    instance-of v3, p1, Lcom/google/android/music/medialist/MediaList;

    if-nez v3, :cond_1

    .line 326
    :cond_0
    const/4 v3, 0x0

    .line 334
    :goto_0
    return v3

    .line 328
    :cond_1
    if-ne p1, p0, :cond_2

    .line 329
    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    move-object v2, p1

    .line 331
    check-cast v2, Lcom/google/android/music/medialist/MediaList;

    .line 332
    .local v2, "other":Lcom/google/android/music/medialist/MediaList;
    invoke-virtual {v2}, Lcom/google/android/music/medialist/MediaList;->freeze()Ljava/lang/String;

    move-result-object v0

    .line 333
    .local v0, "ice1":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/medialist/MediaList;->freeze()Ljava/lang/String;

    move-result-object v1

    .line 334
    .local v1, "ice2":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0
.end method

.method protected filterUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "filterWords"    # Ljava/lang/String;

    .prologue
    .line 692
    return-object p1
.end method

.method public final freeze()Ljava/lang/String;
    .locals 5

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/google/android/music/medialist/MediaList;->getArgs()[Ljava/lang/String;

    move-result-object v1

    .line 277
    .local v1, "args":[Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v4, v1

    if-nez v4, :cond_1

    .line 278
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    .line 291
    :goto_0
    return-object v4

    .line 280
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 281
    .local v2, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    array-length v4, v1

    if-ge v3, v4, :cond_3

    .line 283
    const/16 v4, 0x2c

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 284
    aget-object v0, v1, v3

    .line 285
    .local v0, "arg":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 286
    const-string v0, "<null>"

    .line 288
    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 289
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 291
    .end local v0    # "arg":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public getAlbumCount(Landroid/content/Context;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 743
    const/4 v0, -0x1

    return v0
.end method

.method public getAlbumId(Landroid/content/Context;)J
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 402
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 338
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    return-object v0
.end method

.method public getArtistId(Landroid/content/Context;)J
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 409
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public abstract getContentUri(Landroid/content/Context;)Landroid/net/Uri;
.end method

.method public final getCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cols"    # [Ljava/lang/String;
    .param p3, "filter"    # Ljava/lang/String;

    .prologue
    .line 500
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/music/medialist/MediaList;->getCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/music/medialist/MediaList$MediaCursor;

    move-result-object v0

    return-object v0
.end method

.method public final getCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/music/medialist/MediaList$MediaCursor;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cols"    # [Ljava/lang/String;
    .param p3, "filter"    # Ljava/lang/String;
    .param p4, "isHighPriority"    # Z

    .prologue
    .line 509
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/music/medialist/MediaList;->createAsyncCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/music/AsyncCursor;

    move-result-object v0

    .line 510
    .local v0, "c":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 511
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/music/medialist/MediaList;->createSyncCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 514
    :cond_0
    if-eqz v0, :cond_1

    .line 515
    invoke-virtual {p0, p1, v0}, Lcom/google/android/music/medialist/MediaList;->createMediaCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/medialist/MediaList$MediaCursor;

    move-result-object v1

    .line 517
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCursorAsync(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/medialist/MediaList$OnQueryCompletionHandler;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "filter"    # Ljava/lang/String;
    .param p4, "callback"    # Lcom/google/android/music/medialist/MediaList$OnQueryCompletionHandler;

    .prologue
    .line 576
    new-instance v0, Lcom/google/android/music/medialist/MediaList$QueryParams;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/music/medialist/MediaList$QueryParams;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    .local v0, "params":Lcom/google/android/music/medialist/MediaList$QueryParams;
    new-instance v1, Lcom/google/android/music/medialist/MediaList$1;

    invoke-direct {v1, p0, p4}, Lcom/google/android/music/medialist/MediaList$1;-><init>(Lcom/google/android/music/medialist/MediaList;Lcom/google/android/music/medialist/MediaList$OnQueryCompletionHandler;)V

    .line 583
    .local v1, "task":Lcom/google/android/music/medialist/MediaList$AsyncQueryTask;
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/music/medialist/MediaList$QueryParams;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/MediaList$AsyncQueryTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 584
    return-void
.end method

.method public getDescription(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 417
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDescriptionAttribution(Landroid/content/Context;)Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 444
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDomain()Lcom/google/android/music/download/ContentIdentifier$Domain;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/music/medialist/MediaList;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    return-object v0
.end method

.method public getFlags()I
    .locals 1

    .prologue
    .line 249
    iget v0, p0, Lcom/google/android/music/medialist/MediaList;->mFlags:I

    return v0
.end method

.method public final getFullContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 471
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/MediaList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    .line 473
    .local v0, "baseUri":Landroid/net/Uri;
    if-nez v0, :cond_0

    .line 474
    const/4 v1, 0x0

    .line 477
    :goto_0
    return-object v1

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/music/medialist/MediaList;->mShouldFilter:Z

    iget-boolean v2, p0, Lcom/google/android/music/medialist/MediaList;->mShouldIncludeExternal:Z

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/music/store/MusicContent;->appendFilter(Landroid/content/Context;Landroid/net/Uri;ZZ)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public getImage(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "allowAlias"    # Z

    .prologue
    .line 395
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 378
    const/4 v0, 0x0

    return-object v0
.end method

.method public getScreenTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 370
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/MediaList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSecondaryName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 386
    const/4 v0, 0x0

    return-object v0
.end method

.method public getShouldFilter()Z
    .locals 1

    .prologue
    .line 346
    iget-boolean v0, p0, Lcom/google/android/music/medialist/MediaList;->mShouldFilter:Z

    return v0
.end method

.method public getSongCursor(Landroid/content/Context;Lcom/google/android/music/download/ContentIdentifier;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "songId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p3, "cols"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 587
    const/4 v6, 0x0

    .line 588
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez p2, :cond_0

    .line 608
    :goto_0
    return-object v3

    .line 591
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/music/download/ContentIdentifier;->isDefaultDomain()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 596
    invoke-virtual {p2}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$XAudio;->getRemoteAudio(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v2, p3

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 600
    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_3

    .line 602
    :cond_2
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 603
    invoke-virtual {p2}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$XAudio;->getAudioUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v2, p3

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :cond_3
    move-object v3, v6

    .line 608
    goto :goto_0
.end method

.method public getStoreUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 452
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getSyncMediaCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cols"    # [Ljava/lang/String;
    .param p3, "filter"    # Ljava/lang/String;

    .prologue
    .line 566
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/music/medialist/MediaList;->createSyncCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 567
    .local v0, "c":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 568
    const/4 v1, 0x0

    .line 570
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/music/medialist/MediaList;->createMediaCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/medialist/MediaList$MediaCursor;

    move-result-object v1

    goto :goto_0
.end method

.method public hasDifferentTrackArtists(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 752
    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/google/android/music/medialist/MediaList;->freeze()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isDefaultDomain()Z
    .locals 2

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/music/medialist/MediaList;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    sget-object v1, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEditable()Z
    .locals 1

    .prologue
    .line 622
    const/4 v0, 0x0

    return v0
.end method

.method public isFlagSet(I)Z
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/google/android/music/medialist/MediaList;->getFlags()I

    move-result v0

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSharedDomain()Z
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/music/medialist/MediaList;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    sget-object v1, Lcom/google/android/music/download/ContentIdentifier$Domain;->SHARED:Lcom/google/android/music/download/ContentIdentifier$Domain;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public mayIncludeExternalContent()Z
    .locals 1

    .prologue
    .line 486
    iget-boolean v0, p0, Lcom/google/android/music/medialist/MediaList;->mShouldIncludeExternal:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/music/medialist/MediaList;->getArgs()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 736
    return-void
.end method
