.class Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;
.super Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
.source "AsyncAlbumArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ImFeelingLuckyArtworkMode"
.end annotation


# instance fields
.field private mCanStartIFL:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 866
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;)V
    .locals 1
    .param p1, "other"    # Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;

    .prologue
    .line 868
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    return-void
.end method


# virtual methods
.method public copy()Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .locals 1

    .prologue
    .line 915
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;

    invoke-direct {v0, p0}, Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;)V

    return-object v0
.end method

.method public createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "missingUrlSink"    # Lcom/google/android/music/download/artwork/RemoteUrlSink;

    .prologue
    .line 879
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020045

    invoke-static {v7, v8}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 881
    .local v2, "bmpLarge":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020046

    invoke-static {v7, v8}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 883
    .local v3, "bmpSmall":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    sub-int v1, v7, v8

    .line 884
    .local v1, "bmpDelta":I
    const/4 v5, -0x1

    .line 885
    .local v5, "offset":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    if-lt p2, v7, :cond_2

    .line 886
    move-object v0, v2

    .line 897
    .local v0, "bmp":Landroid/graphics/Bitmap;
    :goto_0
    # getter for: Lcom/google/android/music/AsyncAlbumArtImageView;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$200()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 898
    const-string v8, "AsyncAlbumArtImageView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "delta: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", chosen bmp: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-ne v0, v2, :cond_5

    const-string v7, "Large"

    :goto_1
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 901
    :cond_0
    move-object v4, v0

    .line 902
    .local v4, "mutable":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v7

    if-nez v7, :cond_1

    .line 903
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v0, v7, v8}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 904
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 906
    :cond_1
    invoke-static {v4, p2, p3}, Lcom/google/android/music/utils/AlbumArtUtils;->scaleToSize(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 907
    .local v6, "scaled":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 908
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 909
    invoke-static {p1, v6, p2, p3}, Lcom/google/android/music/utils/AlbumArtUtils;->drawImFeelingLuckyRadioArtOverlay(Landroid/content/Context;Landroid/graphics/Bitmap;II)V

    .line 910
    return-object v6

    .line 887
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    .end local v4    # "mutable":Landroid/graphics/Bitmap;
    .end local v6    # "scaled":Landroid/graphics/Bitmap;
    :cond_2
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    if-gt p2, v7, :cond_3

    .line 888
    move-object v0, v3

    .restart local v0    # "bmp":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 890
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_3
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    sub-int v5, v7, p2

    .line 891
    div-int/lit8 v7, v1, 0x2

    if-ge v5, v7, :cond_4

    .line 892
    move-object v0, v2

    .restart local v0    # "bmp":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 894
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_4
    move-object v0, v3

    .restart local v0    # "bmp":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 898
    :cond_5
    const-string v7, "Small"

    goto :goto_1
.end method

.method public hasIFLAvailabilityChanged(Z)Z
    .locals 1
    .param p1, "newCanStartIFL"    # Z

    .prologue
    .line 925
    iget-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;->mCanStartIFL:Z

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIFLAvailability(Z)V
    .locals 0
    .param p1, "newCanStartIFL"    # Z

    .prologue
    .line 933
    iput-boolean p1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;->mCanStartIFL:Z

    .line 934
    return-void
.end method
