.class Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
.super Ljava/lang/Object;
.source "AlbumArtUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/AlbumArtUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArtCacheKey"
.end annotation


# instance fields
.field private final mId:J

.field private final mType:I


# direct methods
.method public constructor <init>(IJ)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "id"    # J

    .prologue
    .line 2810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2811
    iput p1, p0, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;->mType:I

    .line 2812
    iput-wide p2, p0, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;->mId:J

    .line 2813
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 2817
    instance-of v2, p1, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 2818
    check-cast v0, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;

    .line 2819
    .local v0, "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    iget-wide v2, v0, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;->mId:J

    iget-wide v4, p0, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;->mId:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget v2, v0, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;->mType:I

    iget v3, p0, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;->mType:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 2821
    .end local v0    # "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 2835
    iget-wide v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;->mId:J

    iget-wide v2, p0, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;->mId:J

    const/16 v4, 0x20

    shr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    iget v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;->mType:I

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2840
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ArtCacheKey: type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;->mType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;->mId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
