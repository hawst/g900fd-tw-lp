.class public Lcom/google/android/music/medialist/AlbumSongList;
.super Lcom/google/android/music/medialist/SongList;
.source "AlbumSongList.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AlbumSongList"


# instance fields
.field private final mAlbumId:J

.field private mAlbumMetajamId:Ljava/lang/String;

.field private mAlbumName:Ljava/lang/String;

.field private mAlbumYear:I

.field private mArtUrl:Ljava/lang/String;

.field private mArtistId:J

.field private mArtistName:Ljava/lang/String;

.field private mArtistSort:Ljava/lang/String;


# direct methods
.method public constructor <init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "albumId"    # J
    .param p3, "artistId"    # J
    .param p5, "albumName"    # Ljava/lang/String;
    .param p6, "artistName"    # Ljava/lang/String;
    .param p7, "artistSort"    # Ljava/lang/String;
    .param p8, "shouldFilter"    # Z

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0, v0, p8, v0}, Lcom/google/android/music/medialist/SongList;-><init>(IZZ)V

    .line 64
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 65
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid album id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    iput-wide p1, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    .line 68
    iput-wide p3, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistId:J

    .line 69
    iput-object p5, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumName:Ljava/lang/String;

    .line 70
    iput-object p6, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistName:Ljava/lang/String;

    .line 71
    iput-object p7, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistSort:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 11
    .param p1, "albumId"    # J
    .param p3, "albumName"    # Ljava/lang/String;
    .param p4, "artistName"    # Ljava/lang/String;
    .param p5, "artistSort"    # Ljava/lang/String;
    .param p6, "shouldFilter"    # Z

    .prologue
    .line 59
    const-wide/16 v4, -0x1

    move-object v1, p0

    move-wide v2, p1

    move-object v6, p3

    move-object v7, p4

    move-object/from16 v8, p5

    move/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 60
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Z)V
    .locals 11
    .param p1, "albumId"    # J
    .param p3, "albumName"    # Ljava/lang/String;
    .param p4, "artistName"    # Ljava/lang/String;
    .param p5, "shouldFilter"    # Z

    .prologue
    .line 53
    const-wide/16 v4, -0x1

    const/4 v8, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v6, p3

    move-object v7, p4

    move/from16 v9, p5

    invoke-direct/range {v1 .. v9}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 54
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 11
    .param p1, "albumId"    # J
    .param p3, "shouldFilter"    # Z

    .prologue
    const/4 v6, 0x0

    .line 49
    const-wide/16 v4, -0x1

    move-object v1, p0

    move-wide v2, p1

    move-object v7, v6

    move-object v8, v6

    move v9, p3

    invoke-direct/range {v1 .. v9}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 50
    return-void
.end method

.method private getNames(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 106
    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistSort:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 107
    :cond_0
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "album_name"

    aput-object v0, v2, v7

    const-string v0, "album_artist"

    aput-object v0, v2, v8

    const-string v0, "album_artist_id"

    aput-object v0, v2, v9

    const-string v0, "album_artist_sort"

    aput-object v0, v2, v10

    const/4 v0, 0x4

    const-string v1, "album_year"

    aput-object v1, v2, v0

    .line 114
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 116
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 119
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 120
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 121
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumName:Ljava/lang/String;

    .line 122
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistName:Ljava/lang/String;

    .line 123
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistId:J

    .line 124
    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistSort:Ljava/lang/String;

    .line 125
    const/4 v0, 0x4

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumYear:I

    .line 127
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 130
    const v0, 0x7f0b00c5

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumName:Ljava/lang/String;

    .line 132
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 133
    const v0, 0x7f0b00c4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistName:Ljava/lang/String;

    .line 136
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_4
    return-void
.end method


# virtual methods
.method public appendToPlaylist(Landroid/content/Context;J)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "playlistId"    # J

    .prologue
    .line 370
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v0, p2, p3, v2, v3}, Lcom/google/android/music/store/MusicContent$Playlists;->appendAlbumToPlayList(Landroid/content/ContentResolver;JJ)I

    move-result v0

    return v0
.end method

.method public containsLocalItems(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 237
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "hasLocal"

    aput-object v0, v2, v8

    .line 240
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 243
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 244
    :cond_0
    const-string v0, "AlbumSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown album id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v8

    :goto_0
    return v0

    .line 247
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    move v0, v7

    .line 250
    :goto_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    move v0, v8

    .line 247
    goto :goto_1

    .line 250
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public containsRemoteItems(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 218
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "hasRemote"

    aput-object v0, v2, v8

    .line 221
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 224
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 225
    :cond_0
    const-string v0, "AlbumSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown album id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v8

    :goto_0
    return v0

    .line 228
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-ne v0, v7, :cond_2

    move v0, v7

    .line 231
    :goto_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    move v0, v8

    .line 228
    goto :goto_1

    .line 231
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getAlbumId(Landroid/content/Context;)J
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 171
    :try_start_0
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 174
    :goto_0
    return-wide v0

    .line 172
    :catch_0
    move-exception v0

    .line 174
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getAlbumMetajamId(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 414
    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumMetajamId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 415
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "StoreAlbumId"

    aput-object v0, v2, v1

    .line 418
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 420
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 422
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 423
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumMetajamId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 428
    :goto_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 431
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumMetajamId:Ljava/lang/String;

    return-object v0

    .line 425
    .restart local v2    # "cols":[Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_1
    :try_start_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumMetajamId:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 428
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getAlbumYear(Landroid/content/Context;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 188
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/AlbumSongList;->getNames(Landroid/content/Context;)V

    .line 189
    iget v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumYear:I

    return v0
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 92
    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 93
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumName:Ljava/lang/String;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistName:Ljava/lang/String;

    aput-object v1, v0, v6

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/music/medialist/AlbumSongList;->mShouldFilter:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 96
    :goto_0
    return-object v0

    :cond_0
    new-array v0, v6, [Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iget-boolean v1, p0, Lcom/google/android/music/medialist/AlbumSongList;->mShouldFilter:Z

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_0
.end method

.method public getArtUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 438
    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 439
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "artworkUrl"

    aput-object v0, v2, v1

    .line 442
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 444
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 446
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 447
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtUrl:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 452
    :goto_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 455
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtUrl:Ljava/lang/String;

    return-object v0

    .line 449
    .restart local v2    # "cols":[Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_1
    :try_start_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtUrl:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 452
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getArtistId(Landroid/content/Context;)J
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 178
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/AlbumSongList;->getNames(Landroid/content/Context;)V

    .line 179
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistId:J

    return-wide v0
.end method

.method public getArtistSort(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 183
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/AlbumSongList;->getNames(Landroid/content/Context;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistSort:Ljava/lang/String;

    return-object v0
.end method

.method public getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 140
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/AlbumSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 141
    .local v0, "name":Ljava/lang/String;
    iget-wide v2, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v2, v3, v0}, Lcom/google/android/music/store/ContainerDescriptor;->newAlbumDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v1

    return-object v1
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Albums;->getAudioInAlbumUri(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getDownloadedSongCount(Landroid/content/Context;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 292
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "keeponDownloadedSongCount"

    aput-object v0, v2, v1

    .line 295
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 298
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 299
    :cond_0
    const-string v0, "AlbumSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown album id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    const/4 v0, -0x1

    .line 305
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return v0

    .line 302
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 305
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getImage(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "allowAlias"    # Z

    .prologue
    .line 160
    :try_start_0
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 161
    .local v2, "albumid":J
    const/4 v6, 0x1

    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/AlbumSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/AlbumSongList;->getSecondaryName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    move-object v1, p1

    move v4, p2

    move v5, p3

    move-object v9, p4

    move/from16 v10, p5

    invoke-static/range {v1 .. v10}, Lcom/google/android/music/utils/AlbumArtUtils;->getArtwork(Landroid/content/Context;JIIZLjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 165
    .end local v2    # "albumid":J
    :goto_0
    return-object v0

    .line 163
    :catch_0
    move-exception v0

    .line 165
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getKeepOnSongCount(Landroid/content/Context;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 311
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "keeponSongCount"

    aput-object v0, v2, v1

    .line 314
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 317
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 318
    :cond_0
    const-string v0, "AlbumSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown album id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    const/4 v0, -0x1

    .line 324
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return v0

    .line 321
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 324
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/AlbumSongList;->getNames(Landroid/content/Context;)V

    .line 147
    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumName:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondaryName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 152
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/AlbumSongList;->getNames(Landroid/content/Context;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistName:Ljava/lang/String;

    return-object v0
.end method

.method public getValidSortOrders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasArtistArt()Z
    .locals 1

    .prologue
    .line 402
    const/4 v0, 0x1

    return v0
.end method

.method public hasDifferentTrackArtists(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 199
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "HasDifferentTrackArtists"

    aput-object v0, v2, v8

    .line 202
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 205
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 206
    :cond_0
    const-string v0, "AlbumSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown album id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v8

    :goto_0
    return v0

    .line 209
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-ne v0, v7, :cond_2

    move v0, v7

    .line 212
    :goto_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    move v0, v8

    .line 209
    goto :goto_1

    .line 212
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public hasMetaData()Z
    .locals 1

    .prologue
    .line 381
    const/4 v0, 0x1

    return v0
.end method

.method public hasPersistentNautilus(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 274
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "hasPersistNautilus"

    aput-object v0, v2, v8

    .line 277
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 280
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 281
    :cond_0
    const-string v0, "AlbumSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown album id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v8

    :goto_0
    return v0

    .line 284
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    move v0, v7

    .line 286
    :goto_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    move v0, v8

    .line 284
    goto :goto_1

    .line 286
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public isAllLocal(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 256
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "isAllLocal"

    aput-object v0, v2, v8

    .line 259
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 262
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 263
    :cond_0
    const-string v0, "AlbumSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown album id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v8

    :goto_0
    return v0

    .line 266
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    move v0, v7

    .line 269
    :goto_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    move v0, v8

    .line 266
    goto :goto_1

    .line 269
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/AvailableSpaceTracker;Lcom/google/android/music/store/IStoreService;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "offlineMusicManager"    # Lcom/google/android/music/AvailableSpaceTracker;
    .param p3, "storeService"    # Lcom/google/android/music/store/IStoreService;

    .prologue
    .line 345
    iget-wide v2, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-virtual {p2, v2, v3}, Lcom/google/android/music/AvailableSpaceTracker;->isAlbumSelected(J)Ljava/lang/Boolean;

    move-result-object v0

    .line 346
    .local v0, "result":Ljava/lang/Boolean;
    if-nez v0, :cond_0

    .line 349
    invoke-virtual {p0, p1, p3}, Lcom/google/android/music/medialist/AlbumSongList;->isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/store/IStoreService;)Z

    move-result v1

    .line 351
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method public isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/store/IStoreService;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "storeService"    # Lcom/google/android/music/store/IStoreService;

    .prologue
    .line 336
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-interface {p2, v2, v3}, Lcom/google/android/music/store/IStoreService;->isAlbumSelectedAsKeepOn(J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 339
    :goto_0
    return v1

    .line 337
    :catch_0
    move-exception v0

    .line 338
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AlbumSongList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not get keep on status for album id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public populateExternalSearchExtras(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "i"    # Landroid/content/Intent;

    .prologue
    .line 391
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/AlbumSongList;->getNames(Landroid/content/Context;)V

    .line 392
    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 393
    const-string v0, "android.intent.extra.album"

    iget-object v1, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumName:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 396
    const-string v0, "android.intent.extra.artist"

    iget-object v1, p0, Lcom/google/android/music/medialist/AlbumSongList;->mArtistName:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 398
    :cond_1
    return-void
.end method

.method public refreshMetaData(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 376
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumName:Ljava/lang/String;

    .line 377
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/AlbumSongList;->getNames(Landroid/content/Context;)V

    .line 378
    return-void
.end method

.method public registerMetaDataObserver(Landroid/content/Context;Landroid/database/ContentObserver;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 385
    iget-wide v2, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 386
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, p2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 387
    return-void
.end method

.method public removeFromMyLibrary(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 459
    iget-wide v0, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    invoke-static {p1, v0, v1}, Lcom/google/android/music/store/MusicContent;->deletePersistentNautilusContentFromAlbum(Landroid/content/Context;J)V

    .line 460
    return-void
.end method

.method public shouldShowTrackNumbers()Z
    .locals 1

    .prologue
    .line 407
    const/4 v0, 0x1

    return v0
.end method

.method public storeDefaultSortOrder(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "not supported for AlbumSongList"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public supportsAppendToPlaylist()Z
    .locals 1

    .prologue
    .line 365
    const/4 v0, 0x1

    return v0
.end method

.method protected supportsOfflineCaching()Z
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x1

    return v0
.end method

.method public supportsVideoCluster()Z
    .locals 1

    .prologue
    .line 330
    const/4 v0, 0x1

    return v0
.end method

.method public toggleOfflineCaching(Landroid/content/Context;Lcom/google/android/music/activitymanagement/KeepOnManager;Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keepOnManager"    # Lcom/google/android/music/activitymanagement/KeepOnManager;
    .param p3, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p4, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;

    .prologue
    .line 360
    iget-wide v2, p0, Lcom/google/android/music/medialist/AlbumSongList;->mAlbumId:J

    move-object v1, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/activitymanagement/KeepOnManager;->toggleAlbumKeepOn(JLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Landroid/content/Context;)V

    .line 361
    return-void
.end method
