.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist$2;
.super Ljava/lang/Object;
.source "DocumentMenuHandler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;->onActionSelected()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;)V
    .locals 0

    .prologue
    .line 1242
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist$2;->this$1:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 1246
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist$2;->this$1:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist$2;->this$1:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v2

    # invokes: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->setShareStateTask(J)V
    invoke-static {v0, v2, v3}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$800(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;J)V

    .line 1247
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist$2;->this$1:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # invokes: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->sharePlaylistWithExternalApps()V
    invoke-static {v0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$700(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)V

    .line 1248
    return-void
.end method
