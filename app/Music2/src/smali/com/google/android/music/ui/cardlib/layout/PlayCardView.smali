.class public Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
.super Lcom/google/android/music/ui/cardlib/layout/accessible/AccessibleRelativeLayout;
.source "PlayCardView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/music/KeepOnViewSmall$KeepOnVisibilityCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;
    }
.end annotation


# instance fields
.field protected mAccessibilityOverlay:Landroid/view/View;

.field protected mDescription:Landroid/widget/TextView;

.field private mDocument:Lcom/google/android/music/ui/cardlib/PlayDocument;

.field protected mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

.field protected final mOldOverflowArea:Landroid/graphics/Rect;

.field private mOriginalRightMargin:I

.field protected mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

.field protected final mOverflowArea:Landroid/graphics/Rect;

.field protected final mOverflowTouchExtend:I

.field protected mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

.field protected mReason2:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

.field protected mSubtitle:Landroid/widget/TextView;

.field protected mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

.field protected mThumbnailAspectRatio:F

.field protected mTitle:Landroid/widget/TextView;

.field protected mTrackDuration:Landroid/widget/TextView;

.field protected mUnavailableCardOpacity:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/cardlib/layout/accessible/AccessibleRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOriginalRightMargin:I

    .line 87
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 88
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0f00f0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowTouchExtend:I

    .line 90
    const v1, 0x7f0d0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mUnavailableCardOpacity:F

    .line 92
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    .line 93
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOldOverflowArea:Landroid/graphics/Rect;

    .line 94
    return-void
.end method

.method private setupKeepOn(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/KeepOnView;)V
    .locals 3
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p2, "keepon"    # Lcom/google/android/music/KeepOnView;

    .prologue
    const/4 v2, 0x0

    .line 260
    if-nez p2, :cond_0

    .line 280
    :goto_0
    return-void

    .line 267
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->updateKeepOnViewSmallVisibility(Z)V

    .line 269
    const/4 v0, 0x0

    .line 271
    .local v0, "songList":Lcom/google/android/music/medialist/SongList;
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->doesSupportKeepOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 272
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    .line 275
    :cond_1
    if-eqz v0, :cond_2

    .line 276
    invoke-virtual {p2, v0}, Lcom/google/android/music/KeepOnView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0

    .line 278
    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->updateKeepOnViewSmallVisibility(Z)V

    goto :goto_0
.end method


# virtual methods
.method public bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V
    .locals 12
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p2, "contextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDocument:Lcom/google/android/music/ui/cardlib/PlayDocument;

    .line 134
    if-eqz p1, :cond_11

    .line 135
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v5

    .line 136
    .local v5, "title":Ljava/lang/String;
    if-eqz v5, :cond_b

    .line 137
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mTitle:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 138
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    :goto_0
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mSubtitle:Landroid/widget/TextView;

    if-eqz v7, :cond_0

    .line 144
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getSubTitle()Ljava/lang/String;

    move-result-object v4

    .line 145
    .local v4, "subtitle":Ljava/lang/String;
    if-eqz v4, :cond_c

    .line 146
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mSubtitle:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 147
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    .end local v4    # "subtitle":Ljava/lang/String;
    :cond_0
    :goto_1
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    if-eqz v7, :cond_1

    .line 154
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getDescription()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_d

    .line 155
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 156
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getDescription()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    :cond_1
    :goto_2
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mTrackDuration:Landroid/widget/TextView;

    if-eqz v7, :cond_2

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getDuration()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_2

    .line 163
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getDuration()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    long-to-int v1, v8

    .line 164
    .local v1, "duration":I
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mTrackDuration:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getContext()Landroid/content/Context;

    move-result-object v8

    int-to-long v10, v1

    invoke-static {v8, v10, v11}, Lcom/google/android/music/utils/MusicUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    .end local v1    # "duration":I
    :cond_2
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v7, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    if-eqz v7, :cond_3

    .line 171
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->setVisibility(I)V

    .line 172
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v7, p1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->bind(Lcom/google/android/music/ui/cardlib/model/Document;)V

    .line 175
    :cond_3
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    if-eqz v7, :cond_4

    .line 176
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getReason1()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_e

    .line 177
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->setVisibility(I)V

    .line 180
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getReason1()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 183
    .local v3, "reason1":Ljava/lang/CharSequence;
    const/4 v6, 0x0

    .line 184
    .local v6, "url":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    invoke-virtual {v7, v3, v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->bind(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 186
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    invoke-virtual {v7, p1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->setReasonIcon(Lcom/google/android/music/ui/cardlib/model/Document;)V

    .line 193
    .end local v3    # "reason1":Ljava/lang/CharSequence;
    .end local v6    # "url":Ljava/lang/String;
    :cond_4
    :goto_3
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mReason2:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    if-eqz v7, :cond_5

    .line 194
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mReason2:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->setVisibility(I)V

    .line 197
    :cond_5
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    invoke-direct {p0, p1, v7}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setupKeepOn(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/KeepOnView;)V

    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 201
    .local v0, "contentDescription":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 202
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getSubTitle()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 205
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getSubTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getReason1()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 208
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getReason1()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    :cond_8
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 213
    if-eqz p2, :cond_9

    .line 214
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    new-instance v8, Lcom/google/android/music/ui/cardlib/layout/PlayCardView$1;

    invoke-direct {v8, p0, p2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView$1;-><init>(Lcom/google/android/music/ui/cardlib/layout/PlayCardView;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    invoke-virtual {v7, v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    :cond_9
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setVisibility(I)V

    .line 225
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getIsAvailable()Z

    move-result v7

    if-eqz v7, :cond_f

    .line 226
    const/high16 v7, 0x3f800000    # 1.0f

    invoke-static {p0, v7}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 234
    :goto_4
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 235
    .local v2, "p":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->shouldShowContextMenu()Z

    move-result v7

    if-nez v7, :cond_10

    .line 236
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->setVisibility(I)V

    .line 237
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->setEnabled(Z)V

    .line 238
    const/4 v7, 0x0

    iput v7, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 247
    :goto_5
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v7

    if-eqz v7, :cond_a

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v7

    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v7, v8, :cond_a

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->shouldShowContextMenuForArtist()Z

    move-result v7

    if-nez v7, :cond_a

    .line 249
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->setVisibility(I)V

    .line 250
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->setEnabled(Z)V

    .line 251
    const/4 v7, 0x0

    iput v7, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 256
    .end local v0    # "contentDescription":Ljava/lang/StringBuilder;
    .end local v2    # "p":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v5    # "title":Ljava/lang/String;
    :cond_a
    :goto_6
    return-void

    .line 140
    .restart local v5    # "title":Ljava/lang/String;
    :cond_b
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mTitle:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 149
    .restart local v4    # "subtitle":Ljava/lang/String;
    :cond_c
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mSubtitle:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 158
    .end local v4    # "subtitle":Ljava/lang/String;
    :cond_d
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 188
    :cond_e
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->setVisibility(I)V

    goto/16 :goto_3

    .line 228
    .restart local v0    # "contentDescription":Ljava/lang/StringBuilder;
    :cond_f
    iget v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mUnavailableCardOpacity:F

    invoke-static {p0, v7}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    goto :goto_4

    .line 240
    .restart local v2    # "p":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_10
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->setEnabled(Z)V

    .line 241
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->setVisibility(I)V

    .line 242
    iget v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOriginalRightMargin:I

    iput v7, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto :goto_5

    .line 254
    .end local v0    # "contentDescription":Ljava/lang/StringBuilder;
    .end local v2    # "p":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v5    # "title":Ljava/lang/String;
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bindNoDocument()V

    goto :goto_6
.end method

.method public bindLoading()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 284
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 285
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 286
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 288
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 289
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mSubtitle:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    if-eqz v0, :cond_1

    .line 293
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->setVisibility(I)V

    .line 295
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 296
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 298
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    if-eqz v0, :cond_3

    .line 299
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->setVisibility(I)V

    .line 301
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mReason2:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    if-eqz v0, :cond_4

    .line 302
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mReason2:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->setVisibility(I)V

    .line 304
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->setVisibility(I)V

    .line 306
    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setVisibility(I)V

    .line 307
    return-void
.end method

.method public bindNoDocument()V
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setVisibility(I)V

    .line 312
    return-void
.end method

.method public clearThumbnail()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->clear()V

    .line 130
    :cond_0
    return-void
.end method

.method public getDocument()Lcom/google/android/music/ui/cardlib/PlayDocument;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDocument:Lcom/google/android/music/ui/cardlib/PlayDocument;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 451
    invoke-super {p0}, Lcom/google/android/music/ui/cardlib/layout/accessible/AccessibleRelativeLayout;->onAttachedToWindow()V

    .line 452
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    invoke-virtual {v0, p0}, Lcom/google/android/music/KeepOnViewSmall;->registerCallback(Lcom/google/android/music/KeepOnViewSmall$KeepOnVisibilityCallback;)V

    .line 455
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 408
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->performClick()Z

    .line 409
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 442
    invoke-super {p0}, Lcom/google/android/music/ui/cardlib/layout/accessible/AccessibleRelativeLayout;->onDetachedFromWindow()V

    .line 444
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    invoke-virtual {v0}, Lcom/google/android/music/KeepOnViewSmall;->unregisterCallback()V

    .line 447
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 98
    invoke-super {p0}, Lcom/google/android/music/ui/cardlib/layout/accessible/AccessibleRelativeLayout;->onFinishInflate()V

    .line 100
    const v0, 0x7f0e01f1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    .line 101
    const v0, 0x7f0e0108

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mTitle:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e017e

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mSubtitle:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0e0182

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    .line 104
    const v0, 0x7f0e01fd

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mReason2:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    .line 105
    const v0, 0x7f0e010b

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    .line 106
    const v0, 0x7f0e0109

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0e010a

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    .line 108
    const v0, 0x7f0e01fe

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mTrackDuration:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0e0183

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/KeepOnViewSmall;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    .line 112
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    invoke-virtual {v0, p0}, Lcom/google/android/music/KeepOnViewSmall;->registerCallback(Lcom/google/android/music/KeepOnViewSmall$KeepOnVisibilityCallback;)V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iput v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOriginalRightMargin:I

    .line 116
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 417
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 418
    .local v1, "xPos":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 420
    .local v2, "yPos":F
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    cmpg-float v5, v1, v5

    if-gtz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    cmpl-float v5, v1, v5

    if-ltz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    cmpl-float v5, v2, v5

    if-ltz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    cmpg-float v5, v2, v5

    if-gtz v5, :cond_0

    move v0, v3

    .line 424
    .local v0, "inside":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 427
    :goto_1
    return v3

    .end local v0    # "inside":Z
    :cond_0
    move v0, v4

    .line 420
    goto :goto_0

    .restart local v0    # "inside":Z
    :cond_1
    move v3, v4

    .line 427
    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 17
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    .prologue
    .line 362
    invoke-super/range {p0 .. p5}, Lcom/google/android/music/ui/cardlib/layout/accessible/AccessibleRelativeLayout;->onLayout(ZIIII)V

    .line 364
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getPaddingLeft()I

    move-result v8

    .line 365
    .local v8, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getPaddingTop()I

    move-result v9

    .line 366
    .local v9, "paddingTop":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 368
    .local v1, "accessibilityLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    iget v12, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v12, v8

    iget v13, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v13, v9

    iget v14, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v14, v8

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v15}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    add-int/2addr v14, v15

    iget v15, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v15, v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    add-int v15, v15, v16

    invoke-virtual {v11, v12, v13, v14, v15}, Landroid/view/View;->layout(IIII)V

    .line 378
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    invoke-virtual {v11, v12}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->getHitRect(Landroid/graphics/Rect;)V

    .line 379
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowTouchExtend:I

    sub-int/2addr v12, v13

    iput v12, v11, Landroid/graphics/Rect;->top:I

    .line 380
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowTouchExtend:I

    add-int/2addr v12, v13

    iput v12, v11, Landroid/graphics/Rect;->bottom:I

    .line 381
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowTouchExtend:I

    sub-int/2addr v12, v13

    iput v12, v11, Landroid/graphics/Rect;->left:I

    .line 382
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowTouchExtend:I

    add-int/2addr v12, v13

    iput v12, v11, Landroid/graphics/Rect;->right:I

    .line 383
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->top:I

    if-ne v11, v12, :cond_1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->bottom:I

    if-ne v11, v12, :cond_1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->left:I

    if-ne v11, v12, :cond_1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->right:I

    if-ne v11, v12, :cond_1

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 390
    :cond_1
    new-instance v11, Landroid/view/TouchDelegate;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    invoke-direct {v11, v12, v13}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 391
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOldOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    invoke-virtual {v11, v12}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 393
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    if-eqz v11, :cond_0

    .line 394
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getWidth()I

    move-result v10

    .line 395
    .local v10, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getHeight()I

    move-result v2

    .line 396
    .local v2, "height":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    invoke-virtual {v11}, Lcom/google/android/music/KeepOnViewSmall;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 397
    .local v7, "keeponLpRow3":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getPaddingBottom()I

    move-result v11

    sub-int v11, v2, v11

    iget v12, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v3, v11, v12

    .line 398
    .local v3, "keepOnRow3Bottom":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    invoke-virtual {v11}, Lcom/google/android/music/KeepOnViewSmall;->getMeasuredHeight()I

    move-result v11

    sub-int v6, v3, v11

    .line 399
    .local v6, "keepOnRow3Top":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getPaddingRight()I

    move-result v11

    sub-int v11, v10, v11

    iget v12, v7, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v5, v11, v12

    .line 400
    .local v5, "keepOnRow3Right":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    invoke-virtual {v11}, Lcom/google/android/music/KeepOnViewSmall;->getMeasuredWidth()I

    move-result v11

    sub-int v4, v5, v11

    .line 401
    .local v4, "keepOnRow3Left":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    invoke-virtual {v11, v4, v6, v5, v3}, Lcom/google/android/music/KeepOnViewSmall;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v8, 0x0

    .line 316
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    if-eqz v7, :cond_0

    .line 317
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    const/16 v9, 0x64

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 320
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/cardlib/layout/accessible/AccessibleRelativeLayout;->onMeasure(II)V

    .line 322
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 324
    .local v1, "accessibilityLp":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getMeasuredWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getPaddingLeft()I

    move-result v9

    sub-int/2addr v7, v9

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getPaddingRight()I

    move-result v9

    sub-int/2addr v7, v9

    iget v9, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v7, v9

    iget v9, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v2, v7, v9

    .line 326
    .local v2, "accessibilityWidth":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getPaddingTop()I

    move-result v9

    sub-int/2addr v7, v9

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getPaddingBottom()I

    move-result v9

    sub-int/2addr v7, v9

    iget v9, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v7, v9

    iget v9, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v0, v7, v9

    .line 328
    .local v0, "accessibilityHeight":I
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-static {v2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-static {v0, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v7, v9, v10}, Landroid/view/View;->measure(II)V

    .line 332
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    if-eqz v7, :cond_1

    .line 335
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    .line 336
    .local v4, "descriptionHeight":I
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v5

    .line 337
    .local v5, "layout":Landroid/text/Layout;
    const/4 v6, 0x0

    .local v6, "line":I
    :goto_0
    invoke-virtual {v5}, Landroid/text/Layout;->getLineCount()I

    move-result v7

    if-ge v6, v7, :cond_1

    .line 338
    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v3

    .line 339
    .local v3, "currLineBottom":I
    if-le v3, v4, :cond_4

    .line 344
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 348
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    const/4 v7, 0x2

    if-lt v6, v7, :cond_3

    move v7, v8

    :goto_1
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 354
    .end local v3    # "currLineBottom":I
    .end local v4    # "descriptionHeight":I
    .end local v5    # "layout":Landroid/text/Layout;
    .end local v6    # "line":I
    :cond_1
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    if-eqz v7, :cond_2

    .line 355
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    invoke-virtual {v7, v8, v8}, Lcom/google/android/music/KeepOnViewSmall;->measure(II)V

    .line 357
    :cond_2
    return-void

    .line 348
    .restart local v3    # "currLineBottom":I
    .restart local v4    # "descriptionHeight":I
    .restart local v5    # "layout":Landroid/text/Layout;
    .restart local v6    # "line":I
    :cond_3
    const/4 v7, 0x4

    goto :goto_1

    .line 337
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method public setThumbnailAspectRatio(F)V
    .locals 0
    .param p1, "thumbnailAspectRatio"    # F

    .prologue
    .line 119
    iput p1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mThumbnailAspectRatio:F

    .line 120
    return-void
.end method

.method public updateKeepOnViewSmallVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    const/4 v1, 0x4

    .line 433
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    invoke-virtual {v0}, Lcom/google/android/music/KeepOnViewSmall;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 434
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/KeepOnViewSmall;->setVisibility(I)V

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 435
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    invoke-virtual {v0}, Lcom/google/android/music/KeepOnViewSmall;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 436
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    invoke-virtual {v0, v1}, Lcom/google/android/music/KeepOnViewSmall;->setVisibility(I)V

    goto :goto_0
.end method
