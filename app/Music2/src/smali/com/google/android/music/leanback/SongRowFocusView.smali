.class public Lcom/google/android/music/leanback/SongRowFocusView;
.super Landroid/view/View;
.source "SongRowFocusView.java"


# instance fields
.field private final mPaint:Landroid/graphics/Paint;

.field private final mRoundRectF:Landroid/graphics/RectF;

.field private final mRoundRectRadius:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 20
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mRoundRectF:Landroid/graphics/RectF;

    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/SongRowFocusView;->createPaint(Landroid/content/Context;)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mPaint:Landroid/graphics/Paint;

    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/SongRowFocusView;->getRoundRectRadius(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mRoundRectRadius:I

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mRoundRectF:Landroid/graphics/RectF;

    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/SongRowFocusView;->createPaint(Landroid/content/Context;)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mPaint:Landroid/graphics/Paint;

    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/SongRowFocusView;->getRoundRectRadius(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mRoundRectRadius:I

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mRoundRectF:Landroid/graphics/RectF;

    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/SongRowFocusView;->createPaint(Landroid/content/Context;)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mPaint:Landroid/graphics/Paint;

    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/SongRowFocusView;->getRoundRectRadius(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mRoundRectRadius:I

    .line 39
    return-void
.end method

.method private createPaint(Landroid/content/Context;)Landroid/graphics/Paint;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 52
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00c3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 53
    return-object v0
.end method

.method private getRoundRectRadius(Landroid/content/Context;)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f017e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 44
    iget v2, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mRoundRectRadius:I

    mul-int/lit8 v0, v2, 0x2

    .line 45
    .local v0, "drawHeight":I
    invoke-virtual {p0}, Lcom/google/android/music/leanback/SongRowFocusView;->getHeight()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v1, v2, 0x2

    .line 46
    .local v1, "drawOffset":I
    iget-object v2, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mRoundRectF:Landroid/graphics/RectF;

    const/4 v3, 0x0

    neg-int v4, v1

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/google/android/music/leanback/SongRowFocusView;->getWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/google/android/music/leanback/SongRowFocusView;->getHeight()I

    move-result v6

    add-int/2addr v6, v1

    int-to-float v6, v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 47
    iget-object v2, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mRoundRectF:Landroid/graphics/RectF;

    iget v3, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mRoundRectRadius:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mRoundRectRadius:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/music/leanback/SongRowFocusView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 48
    return-void
.end method
