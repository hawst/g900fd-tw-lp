.class public interface abstract Lcom/google/android/music/ui/MusicFragment;
.super Ljava/lang/Object;
.source "MusicFragment.java"


# virtual methods
.method public abstract getAlbumId()J
.end method

.method public abstract getAlbumMetajamId()Ljava/lang/String;
.end method

.method public abstract getArtistId()J
.end method

.method public abstract getArtistMetajamId()Ljava/lang/String;
.end method

.method public abstract getFragment()Landroid/support/v4/app/Fragment;
.end method

.method public abstract getFragmentMediaList()Lcom/google/android/music/medialist/MediaList;
.end method

.method public abstract onTutorialCardClosed()V
.end method
