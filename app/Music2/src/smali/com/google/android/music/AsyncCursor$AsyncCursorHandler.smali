.class Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;
.super Lcom/google/android/music/utils/LoggableHandler;
.source "AsyncCursor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AsyncCursorHandler"
.end annotation


# instance fields
.field mLogTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 191
    const-string v0, "AsyncCursor"

    invoke-direct {p0, v0}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    .line 192
    const-string v0, "AsyncCursor"

    iput-object v0, p0, Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;->mLogTag:Ljava/lang/String;

    .line 193
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 198
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/AsyncCursor;

    .line 199
    .local v0, "c":Lcom/google/android/music/AsyncCursor;
    if-nez v0, :cond_0

    .line 200
    iget-object v1, p0, Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;->mLogTag:Ljava/lang/String;

    const-string v2, "Cursor disappeared, quitting"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    invoke-virtual {p0}, Lcom/google/android/music/AsyncCursor$AsyncCursorHandler;->quit()V

    .line 218
    :goto_0
    return-void

    .line 204
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 206
    :pswitch_0
    # invokes: Lcom/google/android/music/AsyncCursor;->doCount()V
    invoke-static {v0}, Lcom/google/android/music/AsyncCursor;->access$300(Lcom/google/android/music/AsyncCursor;)V

    goto :goto_0

    .line 209
    :pswitch_1
    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    # invokes: Lcom/google/android/music/AsyncCursor;->doFetch(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/music/AsyncCursor;->access$400(Lcom/google/android/music/AsyncCursor;II)V

    goto :goto_0

    .line 212
    :pswitch_2
    # invokes: Lcom/google/android/music/AsyncCursor;->doFreshen()V
    invoke-static {v0}, Lcom/google/android/music/AsyncCursor;->access$500(Lcom/google/android/music/AsyncCursor;)V

    goto :goto_0

    .line 215
    :pswitch_3
    # invokes: Lcom/google/android/music/AsyncCursor;->doClose()V
    invoke-static {v0}, Lcom/google/android/music/AsyncCursor;->access$600(Lcom/google/android/music/AsyncCursor;)V

    goto :goto_0

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
