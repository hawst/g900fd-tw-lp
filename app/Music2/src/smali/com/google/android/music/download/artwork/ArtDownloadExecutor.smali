.class Lcom/google/android/music/download/artwork/ArtDownloadExecutor;
.super Ljava/util/concurrent/ThreadPoolExecutor;
.source "ArtDownloadExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/artwork/ArtDownloadExecutor$DownloadListener;,
        Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    }
.end annotation


# static fields
.field private static final ARTWORK_CACHE_DIRECTORY:Ljava/lang/String;

.field private static final LOGV:Z

.field private static final sRejectionHandler:Ljava/util/concurrent/RejectedExecutionHandler;


# instance fields
.field private mArtCacheDir:Ljava/io/File;

.field private final mContext:Landroid/content/Context;

.field private mCurrentDownloads:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;",
            ">;"
        }
    .end annotation
.end field

.field private final mDownloadListener:Lcom/google/android/music/download/artwork/ArtDownloadExecutor$DownloadListener;

.field private mFutureDownloads:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;",
            ">;"
        }
    .end annotation
.end field

.field private final mPendingAbsolutePaths:Lcom/google/android/music/utils/FileSetProtector;

.field private mRunningThreads:I

.field private final mStorageType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->LOGV:Z

    .line 57
    invoke-static {}, Lcom/google/android/music/download/artwork/ArtDownloadUtils;->getArtCacheDirectory()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->ARTWORK_CACHE_DIRECTORY:Ljava/lang/String;

    .line 425
    new-instance v0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$2;

    invoke-direct {v0}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$2;-><init>()V

    sput-object v0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->sRejectionHandler:Ljava/util/concurrent/RejectedExecutionHandler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/download/artwork/ArtDownloadExecutor$DownloadListener;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "downloadListener"    # Lcom/google/android/music/download/artwork/ArtDownloadExecutor$DownloadListener;

    .prologue
    const/4 v10, 0x0

    const/4 v2, 0x5

    .line 85
    const-wide/16 v4, 0x3

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sget-object v8, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->sRejectionHandler:Ljava/util/concurrent/RejectedExecutionHandler;

    move-object v1, p0

    move v3, v2

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/RejectedExecutionHandler;)V

    .line 64
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mFutureDownloads:Ljava/util/List;

    .line 67
    new-instance v1, Ljava/util/HashSet;

    const/4 v3, 0x4

    invoke-direct {v1, v3}, Ljava/util/HashSet;-><init>(I)V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mCurrentDownloads:Ljava/util/Set;

    .line 73
    new-instance v1, Lcom/google/android/music/utils/FileSetProtector;

    invoke-direct {v1, v2}, Lcom/google/android/music/utils/FileSetProtector;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mPendingAbsolutePaths:Lcom/google/android/music/utils/FileSetProtector;

    .line 75
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mRunningThreads:I

    .line 87
    iput-object p1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mContext:Landroid/content/Context;

    .line 88
    iput-object p2, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mDownloadListener:Lcom/google/android/music/download/artwork/ArtDownloadExecutor$DownloadListener;

    .line 90
    const/4 v9, 0x0

    .line 91
    .local v9, "isExternalCacheDir":Z
    invoke-static {}, Lcom/google/android/music/utils/PostFroyoUtils$EnvironmentCompat;->isExternalStorageEmulated()Z

    move-result v1

    if-nez v1, :cond_0

    .line 92
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mContext:Landroid/content/Context;

    invoke-static {v1, v10}, Lcom/google/android/music/download/cache/CacheUtils;->getExternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mArtCacheDir:Ljava/io/File;

    .line 93
    const/4 v9, 0x1

    .line 95
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mArtCacheDir:Ljava/io/File;

    if-nez v1, :cond_1

    .line 96
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mContext:Landroid/content/Context;

    invoke-static {v1, v10}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mArtCacheDir:Ljava/io/File;

    .line 97
    const/4 v9, 0x0

    .line 100
    :cond_1
    if-eqz v9, :cond_3

    const/4 v1, 0x2

    :goto_0
    iput v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mStorageType:I

    .line 103
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mArtCacheDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    .line 104
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mArtCacheDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 106
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mArtCacheDir:Ljava/io/File;

    const-string v3, ".nomedia"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :cond_2
    :goto_1
    return-void

    .line 100
    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "ArtDownloadExecutor"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->ARTWORK_CACHE_DIRECTORY:Ljava/lang/String;

    return-object v0
.end method

.method private declared-synchronized afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;Z)V
    .locals 6
    .param p1, "r"    # Ljava/lang/Runnable;
    .param p2, "t"    # Ljava/lang/Throwable;
    .param p3, "isImmediateDownload"    # Z

    .prologue
    .line 193
    monitor-enter p0

    :try_start_0
    iget v3, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mRunningThreads:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mRunningThreads:I

    .line 194
    move-object v0, p1

    check-cast v0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;

    move-object v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 197
    .local v2, "downloadTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    if-eqz p2, :cond_3

    .line 198
    :try_start_1
    sget-boolean v3, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->LOGV:Z

    if-eqz v3, :cond_0

    const-string v3, "ArtDownloadExecutor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error running download thread: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, p2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 199
    :cond_0
    # getter for: Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationFullPath:Ljava/io/File;
    invoke-static {v2}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->access$000(Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;)Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 200
    # getter for: Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationFullPath:Ljava/io/File;
    invoke-static {v2}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->access$000(Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208
    :cond_1
    :goto_0
    :try_start_2
    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mCurrentDownloads:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 209
    invoke-virtual {v2}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->getLocalLocationAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 210
    .local v1, "absolutePath":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mPendingAbsolutePaths:Lcom/google/android/music/utils/FileSetProtector;

    invoke-virtual {v3, v1}, Lcom/google/android/music/utils/FileSetProtector;->unRegisterAbsolutePath(Ljava/lang/String;)V

    .line 213
    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mCurrentDownloads:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mFutureDownloads:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez p2, :cond_2

    invoke-virtual {v2}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->wasDownloadSuccessful()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 222
    sget-object v3, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v4, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$1;

    invoke-direct {v4, p0}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$1;-><init>(Lcom/google/android/music/download/artwork/ArtDownloadExecutor;)V

    invoke-static {v3, v4}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 229
    :cond_2
    monitor-exit p0

    return-void

    .line 202
    .end local v1    # "absolutePath":Ljava/lang/String;
    :cond_3
    :try_start_3
    # getter for: Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mArtworkExists:Z
    invoke-static {v2}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->access$100(Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->wasDownloadSuccessful()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 205
    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mDownloadListener:Lcom/google/android/music/download/artwork/ArtDownloadExecutor$DownloadListener;

    invoke-virtual {v2, v3}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->afterExecute(Lcom/google/android/music/download/artwork/ArtDownloadExecutor$DownloadListener;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 208
    :catchall_0
    move-exception v3

    :try_start_4
    iget-object v4, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mCurrentDownloads:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 209
    invoke-virtual {v2}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->getLocalLocationAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 210
    .restart local v1    # "absolutePath":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mPendingAbsolutePaths:Lcom/google/android/music/utils/FileSetProtector;

    invoke-virtual {v4, v1}, Lcom/google/android/music/utils/FileSetProtector;->unRegisterAbsolutePath(Ljava/lang/String;)V

    .line 211
    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 193
    .end local v1    # "absolutePath":Ljava/lang/String;
    .end local v2    # "downloadTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    :catchall_1
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private decodeAndStripIfNeeded(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "remoteUrl"    # Ljava/lang/String;

    .prologue
    .line 154
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "res":[Ljava/lang/String;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 156
    :cond_0
    const/4 v1, 0x0

    .line 161
    :goto_0
    return-object v1

    .line 158
    :cond_1
    array-length v1, v0

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    .line 159
    const-string v1, "ArtDownloadExecutor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "decodeAndStripIfNeeded: res.length="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_2
    const/4 v1, 0x0

    aget-object v1, v0, v1

    goto :goto_0
.end method

.method private shouldIgnoreDownload(Ljava/lang/String;)Z
    .locals 6
    .param p1, "remoteUrl"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 127
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 150
    :cond_0
    :goto_0
    return v2

    .line 131
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "music_validate_art_urls"

    invoke-static {v3, v4, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 134
    sget-object v3, Landroid/util/Patterns;->WEB_URL:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-nez v3, :cond_2

    .line 135
    const-string v3, "ArtDownloadExecutor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Somehow got an invalid remote url! url="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 140
    :cond_2
    invoke-static {p1}, Lcom/google/android/music/download/cache/CacheUtils;->isRemoteLocationSideLoaded(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 143
    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mCurrentDownloads:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;

    .line 144
    .local v1, "task":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    invoke-virtual {v1}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->getTaskId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    .line 147
    .end local v1    # "task":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mFutureDownloads:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;

    .line 148
    .restart local v1    # "task":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    invoke-virtual {v1}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->getTaskId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    goto :goto_0

    .line 150
    .end local v1    # "task":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    :cond_6
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addDownload(Ljava/lang/String;Z)V
    .locals 8
    .param p1, "remoteUrl"    # Ljava/lang/String;
    .param p2, "isPrefetch"    # Z

    .prologue
    .line 165
    const/4 v7, 0x0

    .line 169
    .local v7, "newTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    invoke-direct {p0, p1}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->decodeAndStripIfNeeded(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 171
    monitor-enter p0

    .line 172
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->shouldIgnoreDownload(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    monitor-exit p0

    move-object v0, v7

    .line 185
    .end local v7    # "newTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    .local v0, "newTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    :goto_0
    return-void

    .line 176
    .end local v0    # "newTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    .restart local v7    # "newTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    :cond_0
    new-instance v0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;

    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mArtCacheDir:Ljava/io/File;

    iget v3, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mStorageType:I

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;-><init>(Landroid/content/Context;Ljava/io/File;ILjava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    .end local v7    # "newTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    .restart local v0    # "newTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->initLocalLocation()Ljava/lang/String;

    move-result-object v6

    .line 180
    .local v6, "absolutePath":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mPendingAbsolutePaths:Lcom/google/android/music/utils/FileSetProtector;

    invoke-virtual {v1, v6}, Lcom/google/android/music/utils/FileSetProtector;->registerAbsolutePath(Ljava/lang/String;)V

    .line 182
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mFutureDownloads:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 184
    invoke-virtual {p0, v0}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 183
    .end local v0    # "newTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    .end local v6    # "absolutePath":Ljava/lang/String;
    .restart local v7    # "newTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    :catchall_0
    move-exception v1

    move-object v0, v7

    .end local v7    # "newTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    .restart local v0    # "newTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    :goto_1
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1

    :catchall_1
    move-exception v1

    goto :goto_1
.end method

.method protected declared-synchronized afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "r"    # Ljava/lang/Runnable;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 189
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    monitor-exit p0

    return-void

    .line 189
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized beforeExecute(Ljava/lang/Thread;Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "t"    # Ljava/lang/Thread;
    .param p2, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 234
    monitor-enter p0

    :try_start_0
    move-object v0, p2

    check-cast v0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;

    move-object v1, v0

    .line 236
    .local v1, "downloadTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mCurrentDownloads:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 237
    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mFutureDownloads:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 238
    iget v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mRunningThreads:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mRunningThreads:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    monitor-exit p0

    return-void

    .line 234
    .end local v1    # "downloadTask":Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public deleteFileIfPossible(Ljava/lang/String;)Z
    .locals 1
    .param p1, "absolutePath"    # Ljava/lang/String;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mPendingAbsolutePaths:Lcom/google/android/music/utils/FileSetProtector;

    invoke-virtual {v0, p1}, Lcom/google/android/music/utils/FileSetProtector;->deleteFileIfPossible(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected notifyQueueComplete()V
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->mDownloadListener:Lcom/google/android/music/download/artwork/ArtDownloadExecutor$DownloadListener;

    invoke-interface {v0}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$DownloadListener;->onDownloadQueueCompleted()V

    .line 251
    return-void
.end method
