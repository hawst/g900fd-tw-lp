.class public abstract Lcom/google/android/music/ui/dialogs/TwoButtonsDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "TwoButtonsDialog.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected createArgs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "okLabel"    # Ljava/lang/String;
    .param p3, "cancelLabel"    # Ljava/lang/String;

    .prologue
    .line 29
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 30
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    const-string v1, "okLabel"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    if-eqz p3, :cond_0

    const-string v1, "cancelLabel"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    :cond_0
    return-object v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    new-instance v6, Lcom/google/android/music/ui/dialogs/TwoButtonsDialog$1;

    invoke-direct {v6, p0}, Lcom/google/android/music/ui/dialogs/TwoButtonsDialog$1;-><init>(Lcom/google/android/music/ui/dialogs/TwoButtonsDialog;)V

    .line 47
    .local v6, "okListener":Landroid/content/DialogInterface$OnClickListener;
    invoke-virtual {p0}, Lcom/google/android/music/ui/dialogs/TwoButtonsDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 48
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {p0}, Lcom/google/android/music/ui/dialogs/TwoButtonsDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 49
    .local v1, "args":Landroid/os/Bundle;
    const-string v7, "message"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 50
    .local v4, "message":Ljava/lang/String;
    const-string v7, "okLabel"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 51
    .local v5, "okLabel":Ljava/lang/String;
    const-string v7, "cancelLabel"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 53
    .local v3, "cancelLabel":Ljava/lang/String;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 54
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 55
    invoke-virtual {v2, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 56
    if-eqz v3, :cond_0

    .line 57
    const/4 v7, 0x0

    invoke-virtual {v2, v3, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 59
    :cond_0
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    return-object v7
.end method

.method protected abstract onOkClicked()V
.end method
