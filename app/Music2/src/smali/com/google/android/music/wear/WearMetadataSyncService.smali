.class public final Lcom/google/android/music/wear/WearMetadataSyncService;
.super Landroid/app/IntentService;
.source "WearMetadataSyncService.java"


# instance fields
.field private mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private mController:Lcom/google/android/music/wear/WearMetadataSyncController;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/android/music/wear/WearMetadataSyncService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 26
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 4

    .prologue
    .line 30
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 31
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/wearable/Wearable;->API:Lcom/google/android/gms/common/api/Api;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/wear/WearMetadataSyncService;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 35
    new-instance v0, Lcom/google/android/music/wear/WearMetadataSyncController;

    new-instance v1, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;

    invoke-direct {v1, p0}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/google/android/music/wear/util/DefaultDataApiWrapper;

    iget-object v3, p0, Lcom/google/android/music/wear/WearMetadataSyncService;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-direct {v2, v3}, Lcom/google/android/music/wear/util/DefaultDataApiWrapper;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/wear/WearMetadataSyncController;-><init>(Lcom/google/android/music/wear/WearMusicDataProvider;Lcom/google/android/music/wear/util/DataApiWrapper;)V

    iput-object v0, p0, Lcom/google/android/music/wear/WearMetadataSyncService;->mController:Lcom/google/android/music/wear/WearMetadataSyncController;

    .line 38
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/wear/WearMetadataSyncService;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    .line 43
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 44
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 48
    iget-object v1, p0, Lcom/google/android/music/wear/WearMetadataSyncService;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    const-wide/16 v2, 0x14

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/gms/common/api/GoogleApiClient;->blockingConnect(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v0

    .line 49
    .local v0, "result":Lcom/google/android/gms/common/ConnectionResult;
    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v1

    if-nez v1, :cond_0

    .line 50
    invoke-static {p0, v0}, Lcom/google/android/music/wear/GmsUtils;->handleConnectionFailureWithNotification(Landroid/content/Context;Lcom/google/android/gms/common/ConnectionResult;)V

    .line 54
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/wear/WearMetadataSyncService;->mController:Lcom/google/android/music/wear/WearMetadataSyncController;

    invoke-virtual {v1}, Lcom/google/android/music/wear/WearMetadataSyncController;->updateDataItems()V

    goto :goto_0
.end method
