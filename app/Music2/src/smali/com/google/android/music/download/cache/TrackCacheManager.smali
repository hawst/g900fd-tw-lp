.class Lcom/google/android/music/download/cache/TrackCacheManager;
.super Lcom/google/android/music/download/cache/BaseCacheManager;
.source "TrackCacheManager.java"


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CACHE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/music/download/cache/TrackCacheManager;->LOGV:Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "musicPreferences"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/download/cache/BaseCacheManager;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 58
    return-void
.end method


# virtual methods
.method protected clearOrphanedFiles()V
    .locals 39

    .prologue
    .line 137
    invoke-static {}, Lcom/google/android/music/download/cache/TrackCacheManager;->getLogFile()Lcom/google/android/music/log/LogFile;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 138
    invoke-static {}, Lcom/google/android/music/download/cache/TrackCacheManager;->getLogFile()Lcom/google/android/music/log/LogFile;

    move-result-object v5

    const-string v6, "CacheManagerImpl"

    const-string v7, "clearOrphanedFiles"

    invoke-virtual {v5, v6, v7}, Lcom/google/android/music/log/LogFile;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v38

    .line 143
    .local v38, "store":Lcom/google/android/music/store/Store;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->getSelectedStorageVolumeId()Ljava/util/UUID;

    move-result-object v35

    .line 144
    .local v35, "selectedStorageVolumeId":Ljava/util/UUID;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v30

    .line 145
    .local v30, "manager":Lcom/google/android/music/download/cache/CacheLocationManager;
    if-eqz v35, :cond_1

    .line 146
    move-object/from16 v0, v30

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownLocationByID(Ljava/util/UUID;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v29

    .line 147
    .local v29, "location":Lcom/google/android/music/download/cache/CacheLocation;
    if-eqz v29, :cond_1

    .line 148
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/music/download/cache/CacheLocation;->getVolumeId()Ljava/util/UUID;

    move-result-object v5

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Lcom/google/android/music/store/Store;->restoreTrumpedMusicFiles(Ljava/util/UUID;)V

    .line 154
    .end local v29    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    :cond_1
    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/store/Store;->deleteOrphanedExternalMusic()I

    .line 156
    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 158
    .local v4, "readDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual/range {v30 .. v30}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownUsableLocations()Ljava/util/Collection;

    move-result-object v12

    .line 160
    .local v12, "cacheDirs":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/download/cache/CacheLocation;>;"
    const-string v5, "MUSIC"

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "Id"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "LocalCopyPath"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "LocalCopySize"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "LocalCopyStorageType"

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "LocalCopyStorageVolumeId"

    aput-object v8, v6, v7

    const-string v7, "LocalCopyType IN (100,200)"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v27

    .line 168
    .local v27, "localFiles":Landroid/database/Cursor;
    new-instance v26, Ljava/util/HashSet;

    invoke-direct/range {v26 .. v26}, Ljava/util/HashSet;-><init>()V

    .line 169
    .local v26, "knownFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v33, Ljava/util/HashSet;

    invoke-direct/range {v33 .. v33}, Ljava/util/HashSet;-><init>()V

    .line 170
    .local v33, "nonExisting":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    new-instance v28, Ljava/util/HashMap;

    invoke-direct/range {v28 .. v28}, Ljava/util/HashMap;-><init>()V

    .line 171
    .local v28, "localPathSizeToFix":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Long;>;"
    new-instance v20, Ljava/util/HashSet;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashSet;-><init>()V

    .line 172
    .local v20, "filesMissingOnSelectedVolume":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_2
    :goto_0
    if-eqz v27, :cond_d

    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 173
    const/4 v5, 0x1

    move-object/from16 v0, v27

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v34

    .line 174
    .local v34, "path":Ljava/lang/String;
    const/4 v5, 0x0

    move-object/from16 v0, v27

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 175
    .local v22, "id":J
    const/4 v5, 0x3

    move-object/from16 v0, v27

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v37

    .line 176
    .local v37, "storageType":I
    const/4 v5, 0x4

    move-object/from16 v0, v27

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/utils/MusicUtils;->getUUIDFromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v36

    .line 177
    .local v36, "storageId":Ljava/util/UUID;
    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 178
    const-string v5, "CacheManagerImpl"

    const-string v6, "Cleanup requested on row but LocalCopyPath is empty. This should not happen."

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 298
    .end local v12    # "cacheDirs":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/download/cache/CacheLocation;>;"
    .end local v20    # "filesMissingOnSelectedVolume":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v22    # "id":J
    .end local v26    # "knownFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v27    # "localFiles":Landroid/database/Cursor;
    .end local v28    # "localPathSizeToFix":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Long;>;"
    .end local v33    # "nonExisting":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v34    # "path":Ljava/lang/String;
    .end local v36    # "storageId":Ljava/util/UUID;
    .end local v37    # "storageType":I
    :catchall_0
    move-exception v5

    if-eqz v4, :cond_3

    .line 299
    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_3
    throw v5

    .line 182
    .restart local v12    # "cacheDirs":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/download/cache/CacheLocation;>;"
    .restart local v20    # "filesMissingOnSelectedVolume":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .restart local v22    # "id":J
    .restart local v26    # "knownFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v27    # "localFiles":Landroid/database/Cursor;
    .restart local v28    # "localPathSizeToFix":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Long;>;"
    .restart local v33    # "nonExisting":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .restart local v34    # "path":Ljava/lang/String;
    .restart local v36    # "storageId":Ljava/util/UUID;
    .restart local v37    # "storageType":I
    :cond_4
    const/16 v16, 0x0

    .line 183
    .local v16, "f":Ljava/io/File;
    if-eqz v36, :cond_9

    .line 184
    :try_start_1
    move-object/from16 v0, v30

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownLocationByID(Ljava/util/UUID;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v29

    .line 185
    .restart local v29    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    if-eqz v29, :cond_7

    .line 188
    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/cache/CacheLocationManager;->asMusicCacheLocation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v32

    .line 189
    .local v32, "musicLocation":Lcom/google/android/music/download/cache/CacheLocation;
    if-eqz v32, :cond_6

    .line 190
    new-instance v16, Ljava/io/File;

    .end local v16    # "f":Ljava/io/File;
    invoke-virtual/range {v32 .. v32}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v5

    move-object/from16 v0, v16

    move-object/from16 v1, v34

    invoke-direct {v0, v5, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 219
    .end local v29    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    .end local v32    # "musicLocation":Lcom/google/android/music/download/cache/CacheLocation;
    .restart local v16    # "f":Ljava/io/File;
    :cond_5
    :goto_1
    if-eqz v16, :cond_2

    .line 220
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_c

    .line 227
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 192
    .restart local v29    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    .restart local v32    # "musicLocation":Lcom/google/android/music/download/cache/CacheLocation;
    :cond_6
    new-instance v16, Ljava/io/File;

    .end local v16    # "f":Ljava/io/File;
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v5

    move-object/from16 v0, v16

    move-object/from16 v1, v34

    invoke-direct {v0, v5, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .restart local v16    # "f":Ljava/io/File;
    goto :goto_1

    .line 196
    .end local v32    # "musicLocation":Lcom/google/android/music/download/cache/CacheLocation;
    :cond_7
    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 197
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 199
    :cond_8
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 207
    .end local v29    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/download/cache/CacheUtils;->getExternalMusicCacheDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v15

    .line 208
    .local v15, "external":Ljava/io/File;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalMusicCacheDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v24

    .line 209
    .local v24, "internal":Ljava/io/File;
    const/4 v5, 0x2

    move/from16 v0, v37

    if-eq v0, v5, :cond_a

    const/4 v5, 0x3

    move/from16 v0, v37

    if-ne v0, v5, :cond_b

    if-eqz v15, :cond_b

    .line 213
    :cond_a
    new-instance v16, Ljava/io/File;

    .end local v16    # "f":Ljava/io/File;
    move-object/from16 v0, v16

    move-object/from16 v1, v34

    invoke-direct {v0, v15, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .restart local v16    # "f":Ljava/io/File;
    goto :goto_1

    .line 214
    :cond_b
    if-eqz v24, :cond_5

    .line 215
    new-instance v16, Ljava/io/File;

    .end local v16    # "f":Ljava/io/File;
    move-object/from16 v0, v16

    move-object/from16 v1, v24

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .restart local v16    # "f":Ljava/io/File;
    goto :goto_1

    .line 229
    .end local v15    # "external":Ljava/io/File;
    .end local v24    # "internal":Ljava/io/File;
    :cond_c
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 233
    const/4 v5, 0x2

    move-object/from16 v0, v27

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 234
    .local v18, "fileSize":J
    const-wide/16 v6, 0x0

    cmp-long v5, v18, v6

    if-nez v5, :cond_2

    .line 235
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 240
    .end local v16    # "f":Ljava/io/File;
    .end local v18    # "fileSize":J
    .end local v22    # "id":J
    .end local v34    # "path":Ljava/lang/String;
    .end local v36    # "storageId":Ljava/util/UUID;
    .end local v37    # "storageType":I
    :cond_d
    invoke-static/range {v27 .. v27}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 241
    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 242
    const/4 v4, 0x0

    .line 245
    invoke-static {}, Lcom/google/android/music/download/cache/TrackCacheManager;->getLogFile()Lcom/google/android/music/log/LogFile;

    move-result-object v5

    if-eqz v5, :cond_e

    .line 246
    invoke-static {}, Lcom/google/android/music/download/cache/TrackCacheManager;->getLogFile()Lcom/google/android/music/log/LogFile;

    move-result-object v5

    const-string v6, "CacheManagerImpl"

    const-string v7, "Before validation: knownFiles=%d nonExisting=%d filesMissingOnSelectedVolume=%d"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-interface/range {v26 .. v26}, Ljava/util/Set;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-interface/range {v33 .. v33}, Ljava/util/Set;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/music/log/LogFile;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    :cond_e
    invoke-interface/range {v33 .. v33}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_10

    .line 255
    const-string v5, "CacheManagerImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Detected "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v33 .. v33}, Ljava/util/Set;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " non-existing files "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "referenced in db"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    invoke-static {}, Lcom/google/android/music/download/cache/TrackCacheManager;->getLogFile()Lcom/google/android/music/log/LogFile;

    move-result-object v5

    if-eqz v5, :cond_f

    .line 258
    invoke-static {}, Lcom/google/android/music/download/cache/TrackCacheManager;->getLogFile()Lcom/google/android/music/log/LogFile;

    move-result-object v5

    const-string v6, "CacheManagerImpl"

    const-string v7, "nonExisting=%d Clear references: %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-interface/range {v33 .. v33}, Ljava/util/Set;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object v33, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/music/log/LogFile;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    :cond_f
    move-object/from16 v0, v38

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/google/android/music/store/Store;->clearReferencesInDatabase(Ljava/util/Set;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269
    :cond_10
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInternal(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v24

    .line 271
    .local v24, "internal":Lcom/google/android/music/download/cache/CacheLocation;
    if-eqz v24, :cond_13

    .line 272
    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_13

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/music/download/cache/CacheLocation;

    .line 273
    .local v13, "cl":Lcom/google/android/music/download/cache/CacheLocation;
    invoke-static {}, Lcom/google/android/music/download/cache/TrackCacheManager;->getLogFile()Lcom/google/android/music/log/LogFile;

    move-result-object v5

    if-eqz v5, :cond_11

    .line 274
    invoke-static {}, Lcom/google/android/music/download/cache/TrackCacheManager;->getLogFile()Lcom/google/android/music/log/LogFile;

    move-result-object v5

    const-string v6, "CacheManagerImpl"

    const-string v7, "validateLocalFiles: cacheLocation=%s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v13}, Lcom/google/android/music/download/cache/CacheLocation;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/music/log/LogFile;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :cond_11
    move-object/from16 v0, v24

    invoke-virtual {v13, v0}, Lcom/google/android/music/download/cache/CacheLocation;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_15

    const/16 v25, 0x1

    .line 278
    .local v25, "isExternal":Z
    :goto_3
    invoke-virtual {v13}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v17

    .line 279
    .local v17, "filesFolder":Ljava/io/File;
    move-object/from16 v0, v30

    invoke-virtual {v0, v13}, Lcom/google/android/music/download/cache/CacheLocationManager;->asMusicCacheLocation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v31

    .line 280
    .local v31, "maybeMusicLocation":Lcom/google/android/music/download/cache/CacheLocation;
    if-eqz v31, :cond_12

    .line 281
    invoke-virtual/range {v31 .. v31}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v17

    .line 283
    :cond_12
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move-object/from16 v2, v17

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/download/cache/TrackCacheManager;->validateLocalFiles(Ljava/util/Set;Ljava/io/File;Z)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 287
    .end local v13    # "cl":Lcom/google/android/music/download/cache/CacheLocation;
    .end local v17    # "filesFolder":Ljava/io/File;
    .end local v21    # "i$":Ljava/util/Iterator;
    .end local v24    # "internal":Lcom/google/android/music/download/cache/CacheLocation;
    .end local v25    # "isExternal":Z
    .end local v31    # "maybeMusicLocation":Lcom/google/android/music/download/cache/CacheLocation;
    :catch_0
    move-exception v14

    .line 288
    .local v14, "e":Ljava/io/IOException;
    :try_start_3
    const-string v5, "CacheManagerImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to validate files: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    invoke-static {}, Lcom/google/android/music/download/cache/TrackCacheManager;->getLogFile()Lcom/google/android/music/log/LogFile;

    move-result-object v5

    if-eqz v5, :cond_13

    .line 290
    invoke-static {}, Lcom/google/android/music/download/cache/TrackCacheManager;->getLogFile()Lcom/google/android/music/log/LogFile;

    move-result-object v5

    const-string v6, "CacheManagerImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to validate files: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v14}, Lcom/google/android/music/log/LogFile;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 295
    .end local v14    # "e":Ljava/io/IOException;
    :cond_13
    move-object/from16 v0, v38

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/google/android/music/store/Store;->fixLocalPathSize(Ljava/util/Map;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 298
    if-eqz v4, :cond_14

    .line 299
    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 307
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/download/cache/CacheUtils;->getExternalMusicCacheDirectory_Old(Landroid/content/Context;)Ljava/io/File;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/music/download/cache/TrackCacheManager;->cleanUpDirectoryIfExists(Ljava/io/File;)V

    .line 308
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/download/cache/CacheUtils;->getExternalArtworkCacheDirectory_Old(Landroid/content/Context;)Ljava/io/File;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/music/download/cache/TrackCacheManager;->cleanUpDirectoryIfExists(Ljava/io/File;)V

    .line 309
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalMusicCacheDirectory_Old(Landroid/content/Context;)Ljava/io/File;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/music/download/cache/TrackCacheManager;->cleanUpDirectoryIfExists(Ljava/io/File;)V

    .line 310
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalArtworkCacheDirectory_Old(Landroid/content/Context;)Ljava/io/File;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/music/download/cache/TrackCacheManager;->cleanUpDirectoryIfExists(Ljava/io/File;)V

    .line 311
    return-void

    .line 277
    .restart local v13    # "cl":Lcom/google/android/music/download/cache/CacheLocation;
    .restart local v21    # "i$":Ljava/util/Iterator;
    .restart local v24    # "internal":Lcom/google/android/music/download/cache/CacheLocation;
    :cond_15
    const/16 v25, 0x0

    goto/16 :goto_3
.end method

.method protected getMaxBytesSpaceToUse()J
    .locals 2

    .prologue
    .line 80
    const-wide v0, 0x80000000L

    return-wide v0
.end method

.method protected getMaxPercentageSpaceToUse()F
    .locals 1

    .prologue
    .line 85
    const v0, 0x3f4ccccd    # 0.8f

    return v0
.end method

.method protected getMinFreeSpaceMB()J
    .locals 4

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_short_term_cache_min_free_space"

    const-wide/16 v2, 0x1f4

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method protected getOwnerFromInt(I)Lcom/google/android/music/download/DownloadRequest$Owner;
    .locals 1
    .param p1, "val"    # I

    .prologue
    .line 62
    invoke-static {}, Lcom/google/android/music/download/TrackOwner;->values()[Lcom/google/android/music/download/TrackOwner;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method protected handleClearCache()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 90
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 91
    .local v7, "values":Landroid/content/ContentValues;
    const-string v9, "LocalCopyType"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 92
    const-string v9, "LocalCopyStorageType"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 94
    const-string v9, "LocalCopyStreamFidelity"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 96
    const-string v9, "LocalCopyPath"

    invoke-virtual {v7, v9}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 97
    const-string v9, "LocalCopySize"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 99
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    .local v8, "where":Ljava/lang/StringBuilder;
    const-string v9, "LocalCopyType"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0x3d

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0x64

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 103
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getFilteredFileDeleter()Lcom/google/android/music/download/cache/FilteredFileDeleter;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/android/music/download/cache/FilteredFileDeleter;->getFilteredIds()Ljava/util/Set;

    move-result-object v3

    .line 104
    .local v3, "filteredIds":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/music/download/ContentIdentifier;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 105
    .local v2, "filteredCloudCached":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/ContentIdentifier;

    .line 106
    .local v0, "d":Lcom/google/android/music/download/ContentIdentifier;
    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier;->isCacheable()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 107
    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v2, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    .end local v0    # "d":Lcom/google/android/music/download/ContentIdentifier;
    :cond_1
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v9

    if-eqz v9, :cond_2

    .line 111
    const-string v9, " AND "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Id"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " NOT "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    invoke-static {v8, v2}, Lcom/google/android/music/utils/DbUtils;->appendIN(Ljava/lang/StringBuilder;Ljava/util/Collection;)V

    .line 115
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v5

    .line 116
    .local v5, "store":Lcom/google/android/music/store/Store;
    const/4 v6, 0x0

    .line 117
    .local v6, "success":Z
    invoke-virtual {v5}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 119
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v9, "MUSIC"

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v1, v9, v7, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    const/4 v6, 0x1

    .line 122
    invoke-virtual {v5, v1, v6}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 124
    if-eqz v6, :cond_3

    .line 125
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->clearOrphanedFiles()V

    .line 127
    :cond_3
    return-void

    .line 122
    :catchall_0
    move-exception v9

    invoke-virtual {v5, v1, v6}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v9
.end method

.method protected handleLowStorage()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    .line 316
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isStorageLowHandlingEnabled()Z

    move-result v8

    if-nez v8, :cond_1

    .line 374
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 323
    sget-boolean v8, Lcom/google/android/music/download/cache/TrackCacheManager;->LOGV:Z

    if-eqz v8, :cond_0

    .line 324
    const-string v8, "CacheManagerImpl"

    const-string v9, "User is in downloaded only mode, ignoring storage low broadcast."

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 330
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/music/download/cache/CacheUtils;->getSelectedVolume(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v4

    .line 331
    .local v4, "location":Lcom/google/android/music/download/cache/CacheLocation;
    if-nez v4, :cond_3

    .line 332
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/music/download/cache/CacheLocationManager;->getDefaultLocation()Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v4

    .line 338
    :cond_3
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lcom/google/android/music/download/cache/CacheLocation;->getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v8

    sget-object v9, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->INTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-virtual {v8, v9}, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 339
    :cond_4
    sget-boolean v8, Lcom/google/android/music/download/cache/TrackCacheManager;->LOGV:Z

    if-eqz v8, :cond_0

    .line 340
    const-string v8, "CacheManagerImpl"

    const-string v9, "Cache type unknown or external, ignoring storage low broadcast."

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 346
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "music_storage_low_broadcast_timeout_millis"

    sget-wide v10, Lcom/google/android/music/MusicGservicesKeys;->DEFAULT_STORAGE_LOW_BROADCAST_TIMEOUT_MILLIS:J

    invoke-static {v8, v9, v10, v11}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v6

    .line 350
    .local v6, "timeout":J
    cmp-long v8, v6, v12

    if-gez v8, :cond_6

    .line 351
    const-string v8, "CacheManagerImpl"

    const-string v9, "Storage low broadcast timeout was less than 0, using default value."

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    sget-wide v6, Lcom/google/android/music/MusicGservicesKeys;->DEFAULT_STORAGE_LOW_BROADCAST_TIMEOUT_MILLIS:J

    .line 357
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "cachemanager.prefs"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 359
    .local v5, "prefs":Landroid/content/SharedPreferences;
    const-string v8, "storagelow.handled.songs.time"

    invoke-interface {v5, v8, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 360
    .local v2, "lastTimeHandled":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 364
    .local v0, "cur":J
    sub-long v8, v0, v2

    cmp-long v8, v8, v6

    if-lez v8, :cond_7

    .line 365
    const-string v8, "CacheManagerImpl"

    const-string v9, "Clearing cache due to low free storage"

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "storagelow.handled.songs.time"

    invoke-interface {v8, v9, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 369
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->handleClearCache()V

    goto/16 :goto_0

    .line 370
    :cond_7
    sget-boolean v8, Lcom/google/android/music/download/cache/TrackCacheManager;->LOGV:Z

    if-eqz v8, :cond_0

    .line 371
    const-string v8, "CacheManagerImpl"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Storage low broadcast was handled recently, do nothing. Last time handled: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected shouldAllowCaching()Z
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/TrackCacheManager;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isCachedStreamingMusicEnabled()Z

    move-result v0

    return v0
.end method
