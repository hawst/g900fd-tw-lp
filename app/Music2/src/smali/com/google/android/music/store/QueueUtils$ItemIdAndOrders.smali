.class public Lcom/google/android/music/store/QueueUtils$ItemIdAndOrders;
.super Ljava/lang/Object;
.source "QueueUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/QueueUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ItemIdAndOrders"
.end annotation


# instance fields
.field public final mId:J

.field public final mOrder:J

.field public final mUnshuffledOrder:J


# direct methods
.method public constructor <init>(JJJ)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "order"    # J
    .param p5, "unshuffledOrder"    # J

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-wide p1, p0, Lcom/google/android/music/store/QueueUtils$ItemIdAndOrders;->mId:J

    .line 35
    iput-wide p3, p0, Lcom/google/android/music/store/QueueUtils$ItemIdAndOrders;->mOrder:J

    .line 36
    iput-wide p5, p0, Lcom/google/android/music/store/QueueUtils$ItemIdAndOrders;->mUnshuffledOrder:J

    .line 37
    return-void
.end method
