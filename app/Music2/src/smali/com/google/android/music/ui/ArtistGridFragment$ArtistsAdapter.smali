.class final Lcom/google/android/music/ui/ArtistGridFragment$ArtistsAdapter;
.super Lcom/google/android/music/ui/MediaListCardAdapter;
.source "ArtistGridFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/ArtistGridFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ArtistsAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/ArtistGridFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/ArtistGridFragment;Lcom/google/android/music/ui/ArtistGridFragment;)V
    .locals 1
    .param p2, "fragment"    # Lcom/google/android/music/ui/ArtistGridFragment;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistGridFragment$ArtistsAdapter;->this$0:Lcom/google/android/music/ui/ArtistGridFragment;

    .line 118
    # getter for: Lcom/google/android/music/ui/ArtistGridFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {p1}, Lcom/google/android/music/ui/ArtistGridFragment;->access$200(Lcom/google/android/music/ui/ArtistGridFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/music/ui/MediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;I)V

    .line 119
    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/ui/ArtistGridFragment;Lcom/google/android/music/ui/ArtistGridFragment;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "fragment"    # Lcom/google/android/music/ui/ArtistGridFragment;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistGridFragment$ArtistsAdapter;->this$0:Lcom/google/android/music/ui/ArtistGridFragment;

    .line 122
    # getter for: Lcom/google/android/music/ui/ArtistGridFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {p1}, Lcom/google/android/music/ui/ArtistGridFragment;->access$200(Lcom/google/android/music/ui/ArtistGridFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v0

    invoke-direct {p0, p2, v0, p3}, Lcom/google/android/music/ui/MediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ILandroid/database/Cursor;)V

    .line 123
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/ArtistGridFragment;Lcom/google/android/music/ui/ArtistGridFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/ArtistGridFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/ArtistGridFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/ArtistGridFragment;
    .param p3, "x2"    # Landroid/database/Cursor;
    .param p4, "x3"    # Lcom/google/android/music/ui/ArtistGridFragment$1;

    .prologue
    .line 115
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/ui/ArtistGridFragment$ArtistsAdapter;-><init>(Lcom/google/android/music/ui/ArtistGridFragment;Lcom/google/android/music/ui/ArtistGridFragment;Landroid/database/Cursor;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/ArtistGridFragment;Lcom/google/android/music/ui/ArtistGridFragment;Lcom/google/android/music/ui/ArtistGridFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/ArtistGridFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/ArtistGridFragment;
    .param p3, "x2"    # Lcom/google/android/music/ui/ArtistGridFragment$1;

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/ArtistGridFragment$ArtistsAdapter;-><init>(Lcom/google/android/music/ui/ArtistGridFragment;Lcom/google/android/music/ui/ArtistGridFragment;)V

    return-void
.end method


# virtual methods
.method protected bindViewToLoadingItem(Landroid/view/View;Landroid/content/Context;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 168
    instance-of v1, p1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 169
    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 170
    .local v0, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bindLoading()V

    .line 172
    .end local v0    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_0
    return-void
.end method

.method protected bindViewToMediaListItem(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;J)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "itemId"    # J

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 139
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->getDocument(Landroid/view/View;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v1

    .line 141
    .local v1, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 142
    .local v4, "id":J
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, "artistName":Ljava/lang/String;
    const/4 v7, 0x2

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_3

    .line 145
    .local v2, "hasLocal":Z
    :goto_0
    invoke-virtual {v1, v4, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 146
    invoke-virtual {v1, v4, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistId(J)V

    .line 147
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 148
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 149
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v1, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 150
    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setHasLocal(Z)V

    .line 152
    invoke-interface {p3, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 153
    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistMetajamId(Ljava/lang/String;)V

    .line 156
    :cond_0
    invoke-interface {p3, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 157
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 160
    :cond_1
    instance-of v6, p1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v6, :cond_2

    move-object v3, p1

    .line 161
    check-cast v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 162
    .local v3, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    iget-object v6, p0, Lcom/google/android/music/ui/ArtistGridFragment$ArtistsAdapter;->this$0:Lcom/google/android/music/ui/ArtistGridFragment;

    iget-object v6, v6, Lcom/google/android/music/ui/ArtistGridFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-virtual {v3, v1, v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 164
    .end local v3    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_2
    return-void

    .end local v2    # "hasLocal":Z
    :cond_3
    move v2, v6

    .line 143
    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 127
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/MediaListCardAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 128
    .local v0, "view":Landroid/view/View;
    instance-of v1, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 129
    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistGridFragment$ArtistsAdapter;->this$0:Lcom/google/android/music/ui/ArtistGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistGridFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {v2}, Lcom/google/android/music/ui/ArtistGridFragment;->access$200(Lcom/google/android/music/ui/ArtistGridFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 132
    :cond_0
    return-object v0
.end method
