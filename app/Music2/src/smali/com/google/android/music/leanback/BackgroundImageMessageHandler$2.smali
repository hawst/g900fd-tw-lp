.class Lcom/google/android/music/leanback/BackgroundImageMessageHandler$2;
.super Ljava/lang/Object;
.source "BackgroundImageMessageHandler.java"

# interfaces
.implements Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->loadImage(Landroid/util/Pair;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

.field final synthetic val$timeItemPair:Landroid/util/Pair;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;Landroid/util/Pair;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$2;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    iput-object p2, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$2;->val$timeItemPair:Landroid/util/Pair;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemReceived(Lcom/google/android/music/leanback/Item;)V
    .locals 4
    .param p1, "item"    # Lcom/google/android/music/leanback/Item;

    .prologue
    .line 183
    iget-object v1, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$2;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    iget-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$2;->val$timeItemPair:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    # invokes: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->loadItemBackground(JLcom/google/android/music/leanback/Item;)V
    invoke-static {v1, v2, v3, p1}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$400(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;JLcom/google/android/music/leanback/Item;)V

    .line 184
    return-void
.end method
