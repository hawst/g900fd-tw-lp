.class Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;
.super Ljava/lang/Object;
.source "MaterialContainerHeaderBg.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/MaterialContainerHeaderBg;->setupArtistArtSlideShow(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$100(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideSwitchRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$000(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 316
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$100(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideRefreshRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$200(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 319
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mViewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$300(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showNext()V

    .line 320
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mShowingFirstSlide:Z
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$400(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    # setter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mShowingFirstSlide:Z
    invoke-static {v1, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$402(Lcom/google/android/music/ui/MaterialContainerHeaderBg;Z)Z

    .line 324
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$100(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideRefreshRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$200(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x640

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 328
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$100(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideSwitchRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$000(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 330
    return-void

    .line 320
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
