.class Lcom/google/android/music/leanback/SongRowPresenter$2;
.super Ljava/lang/Object;
.source "SongRowPresenter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/SongRowPresenter;->onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/SongRowPresenter;

.field final synthetic val$songRow:Lcom/google/android/music/leanback/SongRow;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/SongRowPresenter;Lcom/google/android/music/leanback/SongRow;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/music/leanback/SongRowPresenter$2;->this$0:Lcom/google/android/music/leanback/SongRowPresenter;

    iput-object p2, p0, Lcom/google/android/music/leanback/SongRowPresenter$2;->val$songRow:Lcom/google/android/music/leanback/SongRow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRowPresenter$2;->this$0:Lcom/google/android/music/leanback/SongRowPresenter;

    # getter for: Lcom/google/android/music/leanback/SongRowPresenter;->mListener:Lcom/google/android/music/leanback/SongRowPresenter$Listener;
    invoke-static {v0}, Lcom/google/android/music/leanback/SongRowPresenter;->access$000(Lcom/google/android/music/leanback/SongRowPresenter;)Lcom/google/android/music/leanback/SongRowPresenter$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/SongRowPresenter$2;->val$songRow:Lcom/google/android/music/leanback/SongRow;

    invoke-interface {v0, v1}, Lcom/google/android/music/leanback/SongRowPresenter$Listener;->onSongRowRadioClicked(Lcom/google/android/music/leanback/SongRow;)V

    .line 110
    return-void
.end method
