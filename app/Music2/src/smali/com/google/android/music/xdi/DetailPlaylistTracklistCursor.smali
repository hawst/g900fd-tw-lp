.class Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;
.super Landroid/database/MatrixCursor;
.source "DetailPlaylistTracklistCursor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "playlistId"    # J

    .prologue
    .line 36
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 37
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mContext:Landroid/content/Context;

    .line 38
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 40
    invoke-direct {p0, p3, p4}, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->addPlaylistTracks(J)V

    .line 41
    return-void
.end method

.method private addPlaylistTracks(J)V
    .locals 7
    .param p1, "playlistId"    # J

    .prologue
    .line 44
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/google/android/music/xdi/MusicProjections;->PLAYLIST_COLUMNS:[Ljava/lang/String;

    invoke-static {v1, p1, p2, v2}, Lcom/google/android/music/xdi/XdiUtils;->getPlaylistCursor(Landroid/content/Context;J[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 47
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 79
    :goto_0
    return-void

    .line 56
    :cond_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 57
    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 58
    .local v4, "type":I
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 59
    .local v5, "name":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/music/xdi/XdiUtils;->getSongListForPlaylistCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/medialist/SongList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 64
    .local v6, "songList":Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 67
    if-gez v4, :cond_2

    .line 68
    const-string v1, "MusicXdi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not get playlist type for playlist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 64
    .end local v4    # "type":I
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_1
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v1

    .line 71
    .restart local v4    # "type":I
    .restart local v5    # "name":Ljava/lang/String;
    .restart local v6    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_2
    if-nez v6, :cond_3

    .line 72
    const-string v1, "MusicXdi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not get songlist for playlist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 75
    :cond_3
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 76
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mContext:Landroid/content/Context;

    const v2, 0x7f0b00c6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    :cond_4
    move-object v1, p0

    move-wide v2, p1

    .line 78
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->addPlaylistTracks(JILjava/lang/String;Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0
.end method


# virtual methods
.method addPlaylistTracks(JILjava/lang/String;Lcom/google/android/music/medialist/SongList;)V
    .locals 27
    .param p1, "playlistId"    # J
    .param p3, "playlistType"    # I
    .param p4, "playlistName"    # Ljava/lang/String;
    .param p5, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 83
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mContext:Landroid/content/Context;

    invoke-static/range {p1 .. p2}, Lcom/google/android/music/store/MusicContent$Playlists$Members;->getPlaylistItemsUri(J)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/xdi/TrackAdapter;->TRACK_COLUMNS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static/range {v2 .. v9}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v14

    .line 89
    .local v14, "cursor":Landroid/database/Cursor;
    if-nez v14, :cond_0

    .line 154
    :goto_0
    return-void

    .line 93
    :cond_0
    new-instance v24, Lcom/google/android/music/xdi/TrackAdapter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mContext:Landroid/content/Context;

    move-object/from16 v0, v24

    move-object/from16 v1, p5

    invoke-direct {v0, v2, v1, v14}, Lcom/google/android/music/xdi/TrackAdapter;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;Landroid/database/Cursor;)V

    .line 94
    .local v24, "trackAdapter":Lcom/google/android/music/xdi/TrackAdapter;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v2}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v2

    new-array v0, v2, [Ljava/lang/Object;

    move-object/from16 v26, v0

    .line 95
    .local v26, "values":[Ljava/lang/Object;
    const/4 v15, 0x0

    .local v15, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    move-object/from16 v16, v15

    .line 98
    .end local v15    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .local v16, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :goto_1
    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 99
    const/4 v2, 0x0

    move-object/from16 v0, v26

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 100
    invoke-interface {v14}, Landroid/database/Cursor;->getPosition()I

    move-result v20

    .line 101
    .local v20, "position":I
    if-nez v16, :cond_6

    .line 102
    new-instance v15, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v15}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    .end local v16    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v15    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :goto_2
    :try_start_1
    move-object/from16 v0, v24

    invoke-virtual {v0, v15, v14}, Lcom/google/android/music/xdi/TrackAdapter;->populateDocumentFromCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)V

    .line 106
    invoke-virtual {v15}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v21

    .line 107
    .local v21, "title":Ljava/lang/String;
    invoke-virtual {v15}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v22

    .line 108
    .local v22, "songId":J
    invoke-virtual {v15}, Lcom/google/android/music/ui/cardlib/model/Document;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v25

    .line 109
    .local v25, "trackMetajamId":Ljava/lang/String;
    invoke-virtual {v15}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v12

    .line 110
    .local v12, "artistName":Ljava/lang/String;
    invoke-static {v12}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00c4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 113
    :cond_1
    invoke-virtual {v15}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumName()Ljava/lang/String;

    move-result-object v11

    .line 114
    .local v11, "albumName":Ljava/lang/String;
    invoke-static {v11}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 115
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00c5

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 117
    :cond_2
    invoke-virtual {v15}, Lcom/google/android/music/ui/cardlib/model/Document;->getDuration()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long v18, v2, v4

    .line 118
    .local v18, "duration":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mContext:Landroid/content/Context;

    move-wide/from16 v0, v18

    invoke-static {v2, v0, v1}, Lcom/google/android/music/utils/MusicUtils;->makeTimeStringSync(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v17

    .line 120
    .local v17, "durationString":Ljava/lang/String;
    invoke-virtual {v15}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v13

    .line 121
    .local v13, "artworkUrl":Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 122
    invoke-virtual {v15}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumId()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    .line 123
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    .line 130
    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "_id"

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 131
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_name"

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v2, v0, v3, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_subname"

    move-object/from16 v0, v26

    invoke-virtual {v2, v0, v3, v12}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 134
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_length"

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v2, v0, v3, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_description"

    const/4 v4, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 138
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_number"

    add-int/lit8 v4, v20, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 140
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "image_uri"

    move-object/from16 v0, v26

    invoke-virtual {v2, v0, v3, v13}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 141
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "item_display_type"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 143
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "music_duration"

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 144
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "music_trackArtist"

    move-object/from16 v0, v26

    invoke-virtual {v2, v0, v3, v12}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 145
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "music_album"

    move-object/from16 v0, v26

    invoke-virtual {v2, v0, v3, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 146
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "music_trackImageUri"

    move-object/from16 v0, v26

    invoke-virtual {v2, v0, v3, v13}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 149
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->addRow([Ljava/lang/Object;)V

    move-object/from16 v16, v15

    .line 150
    .end local v15    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v16    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    goto/16 :goto_1

    .line 125
    .end local v16    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v15    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_4
    invoke-virtual {v15}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    .line 126
    .local v10, "albumId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;->mContext:Landroid/content/Context;

    invoke-static {v2, v10}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumArtUriOrDefault(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v13

    goto/16 :goto_3

    .line 152
    .end local v10    # "albumId":Ljava/lang/String;
    .end local v11    # "albumName":Ljava/lang/String;
    .end local v12    # "artistName":Ljava/lang/String;
    .end local v13    # "artworkUrl":Ljava/lang/String;
    .end local v15    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v17    # "durationString":Ljava/lang/String;
    .end local v18    # "duration":J
    .end local v20    # "position":I
    .end local v21    # "title":Ljava/lang/String;
    .end local v22    # "songId":J
    .end local v25    # "trackMetajamId":Ljava/lang/String;
    .restart local v16    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_5
    invoke-static {v14}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    move-object/from16 v15, v16

    .end local v16    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v15    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :goto_4
    invoke-static {v14}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    .restart local v20    # "position":I
    :catchall_1
    move-exception v2

    goto :goto_4

    .end local v15    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v16    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_6
    move-object/from16 v15, v16

    .end local v16    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v15    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    goto/16 :goto_2
.end method
