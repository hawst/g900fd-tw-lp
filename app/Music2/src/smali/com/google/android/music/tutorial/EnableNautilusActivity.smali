.class public Lcom/google/android/music/tutorial/EnableNautilusActivity;
.super Lcom/google/android/music/tutorial/TutorialActivity;
.source "EnableNautilusActivity.java"


# instance fields
.field private mOfferId:Ljava/lang/String;

.field private mSuccess:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/tutorial/EnableNautilusActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/EnableNautilusActivity;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/music/tutorial/EnableNautilusActivity;->mOfferId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/tutorial/EnableNautilusActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/EnableNautilusActivity;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/google/android/music/tutorial/EnableNautilusActivity;->mSuccess:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/music/tutorial/EnableNautilusActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/EnableNautilusActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/google/android/music/tutorial/EnableNautilusActivity;->mSuccess:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/music/tutorial/EnableNautilusActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/EnableNautilusActivity;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->buildRetryDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/tutorial/EnableNautilusActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/EnableNautilusActivity;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->enableNautilus()V

    return-void
.end method

.method private buildRetryDialog()V
    .locals 4

    .prologue
    .line 86
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 87
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0b035c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b035e

    new-instance v3, Lcom/google/android/music/tutorial/EnableNautilusActivity$3;

    invoke-direct {v3, p0}, Lcom/google/android/music/tutorial/EnableNautilusActivity$3;-><init>(Lcom/google/android/music/tutorial/EnableNautilusActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b035d

    new-instance v3, Lcom/google/android/music/tutorial/EnableNautilusActivity$2;

    invoke-direct {v3, p0}, Lcom/google/android/music/tutorial/EnableNautilusActivity$2;-><init>(Lcom/google/android/music/tutorial/EnableNautilusActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 105
    return-void
.end method

.method public static buildStartIntent(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "cxt"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "offerId"    # Ljava/lang/String;

    .prologue
    .line 47
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/tutorial/EnableNautilusActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 48
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 49
    const-string v1, "offerId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    return-object v0
.end method

.method private enableNautilus()V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/music/tutorial/EnableNautilusActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/tutorial/EnableNautilusActivity$1;-><init>(Lcom/google/android/music/tutorial/EnableNautilusActivity;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 83
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/music/tutorial/EnableNautilusActivity;->mSuccess:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->setResult(I)V

    .line 43
    invoke-super {p0}, Lcom/google/android/music/tutorial/TutorialActivity;->finish()V

    .line 44
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 22
    invoke-super {p0}, Lcom/google/android/music/tutorial/TutorialActivity;->isActivityDestroyed()Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super {p0, p1}, Lcom/google/android/music/tutorial/TutorialActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    const v0, 0x7f040032

    invoke-virtual {p0, v0}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->setContentView(I)V

    .line 34
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "offerId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/tutorial/EnableNautilusActivity;->mOfferId:Ljava/lang/String;

    .line 36
    invoke-direct {p0}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->enableNautilus()V

    .line 38
    :cond_0
    return-void
.end method
