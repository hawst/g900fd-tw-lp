.class Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$2;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackPlaylistDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->createSongList(Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mTrackAdapter:Lcom/google/android/music/xdi/TrackAdapter;

.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

.field final synthetic val$item:Lcom/google/android/music/leanback/Item;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;Lcom/google/android/music/leanback/Item;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$2;->val$item:Lcom/google/android/music/leanback/Item;

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 150
    new-instance v2, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v2}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 151
    .local v2, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$2;->mTrackAdapter:Lcom/google/android/music/xdi/TrackAdapter;

    invoke-virtual {v0, v2, p1}, Lcom/google/android/music/xdi/TrackAdapter;->populateDocumentFromCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)V

    .line 152
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 153
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v6, 0x1

    .line 154
    .local v6, "isLast":Z
    :goto_0
    new-instance v0, Lcom/google/android/music/leanback/SongRow;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    invoke-static {v4, v2}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/content/Intent;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$2;->val$item:Lcom/google/android/music/leanback/Item;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/leanback/SongRow;-><init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;ILandroid/content/Intent;Lcom/google/android/music/leanback/Item;Z)V

    return-object v0

    .line 153
    .end local v6    # "isLast":Z
    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method protected bindColumns(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 144
    new-instance v0, Lcom/google/android/music/xdi/TrackAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v2}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->access$000(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;)Lcom/google/android/music/medialist/SongList;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/music/xdi/TrackAdapter;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$2;->mTrackAdapter:Lcom/google/android/music/xdi/TrackAdapter;

    .line 146
    return-void
.end method
