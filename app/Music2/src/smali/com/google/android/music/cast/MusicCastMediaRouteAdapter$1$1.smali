.class Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1$1;
.super Ljava/lang/Object;
.source "MusicCastMediaRouteAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;->onRouteVolumeChanged(Ljava/lang/String;D)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;

.field final synthetic val$volume:D


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;D)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1$1;->this$1:Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;

    iput-wide p2, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1$1;->val$volume:D

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1$1;->this$1:Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;

    iget-object v0, v0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;->this$0:Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;

    # getter for: Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mMediaRouteStateChangeListener:Lcom/google/cast/MediaRouteStateChangeListener;
    invoke-static {v0}, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->access$100(Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;)Lcom/google/cast/MediaRouteStateChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1$1;->this$1:Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;

    iget-object v0, v0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;->this$0:Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;

    # getter for: Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mMediaRouteStateChangeListener:Lcom/google/cast/MediaRouteStateChangeListener;
    invoke-static {v0}, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->access$100(Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;)Lcom/google/cast/MediaRouteStateChangeListener;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1$1;->val$volume:D

    invoke-interface {v0, v2, v3}, Lcom/google/cast/MediaRouteStateChangeListener;->onVolumeChanged(D)V

    .line 49
    :cond_0
    return-void
.end method
