.class Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;
.super Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;
.source "LeanbackPlayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->setSongListObjectAdapter(Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)V
    .locals 0

    .prologue
    .line 1025
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2400(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)V

    .line 1029
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->notifyChanged()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2500(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)V

    .line 1030
    return-void
.end method

.method public onItemRangeChanged(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 1034
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2600(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)I

    move-result v1

    add-int/2addr v1, p1

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->notifyItemRangeChanged(II)V
    invoke-static {v0, v1, p2}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2700(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;II)V

    .line 1036
    return-void
.end method

.method public onItemRangeInserted(II)V
    .locals 6
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    const/4 v3, 0x1

    .line 1040
    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSize:I
    invoke-static {v4}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2800(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)I

    move-result v4

    if-ne v4, v3, :cond_2

    move v2, v3

    .line 1041
    .local v2, "wasEmpty":Z
    :goto_0
    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->recalculateSize()V
    invoke-static {v4}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2400(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)V

    .line 1042
    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I
    invoke-static {v4}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2600(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)I

    move-result v4

    add-int v1, v4, p1

    .line 1043
    .local v1, "newPositionStart":I
    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I
    invoke-static {v4}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2600(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)I

    move-result v4

    add-int v0, v4, p1

    .line 1045
    .local v0, "newItemCount":I
    if-eqz v2, :cond_0

    .line 1046
    add-int/lit8 v1, v1, -0x1

    .line 1047
    add-int/lit8 v0, v0, 0x2

    .line 1049
    :cond_0
    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSize:I
    invoke-static {v4}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2800(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)I

    move-result v4

    if-ge v4, v3, :cond_1

    .line 1050
    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    iget-object v5, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSize:I
    invoke-static {v5}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2800(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->notifyItemRangeChanged(II)V
    invoke-static {v4, v5, v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2900(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;II)V

    .line 1052
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->notifyItemRangeInserted(II)V
    invoke-static {v3, v1, v0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$3000(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;II)V

    .line 1053
    return-void

    .line 1040
    .end local v0    # "newItemCount":I
    .end local v1    # "newPositionStart":I
    .end local v2    # "wasEmpty":Z
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onItemRangeRemoved(II)V
    .locals 4
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    const/4 v0, 0x1

    .line 1057
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->recalculateSize()V
    invoke-static {v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2400(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)V

    .line 1058
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSize:I
    invoke-static {v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2800(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)I

    move-result v3

    if-ne v3, v0, :cond_1

    .line 1059
    .local v0, "isEmpty":Z
    :goto_0
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I
    invoke-static {v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2600(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)I

    move-result v3

    add-int v2, v3, p1

    .line 1060
    .local v2, "newPositionStart":I
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I
    invoke-static {v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$2600(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)I

    move-result v3

    add-int v1, v3, p1

    .line 1062
    .local v1, "newItemCount":I
    if-eqz v0, :cond_0

    .line 1063
    add-int/lit8 v2, v2, -0x1

    .line 1064
    add-int/lit8 v1, v1, 0x2

    .line 1066
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->notifyItemRangeRemoved(II)V
    invoke-static {v3, v2, v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$3100(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;II)V

    .line 1067
    return-void

    .line 1058
    .end local v0    # "isEmpty":Z
    .end local v1    # "newItemCount":I
    .end local v2    # "newPositionStart":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
