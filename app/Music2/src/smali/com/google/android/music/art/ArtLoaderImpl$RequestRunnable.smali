.class abstract Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;
.super Ljava/lang/Object;
.source "ArtLoaderImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtLoaderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "RequestRunnable"
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/music/art/ArtLoader$Listener;

.field private final mListenerHandler:Landroid/os/Handler;

.field private final mMode:Lcom/google/android/music/art/ArtLoader$DownloadMode;

.field protected final mRemoteArtUrl:Ljava/lang/String;

.field private final mRequestNumber:I

.field final synthetic this$0:Lcom/google/android/music/art/ArtLoaderImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/music/art/ArtLoaderImpl;Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;Landroid/os/Handler;)V
    .locals 1
    .param p2, "remoteArtUrl"    # Ljava/lang/String;
    .param p3, "mode"    # Lcom/google/android/music/art/ArtLoader$DownloadMode;
    .param p4, "listener"    # Lcom/google/android/music/art/ArtLoader$Listener;
    .param p5, "listenerHandler"    # Landroid/os/Handler;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->this$0:Lcom/google/android/music/art/ArtLoaderImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    iput-object p2, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mRemoteArtUrl:Ljava/lang/String;

    .line 197
    iput-object p3, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mMode:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    .line 198
    iput-object p4, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mListener:Lcom/google/android/music/art/ArtLoader$Listener;

    .line 199
    iput-object p5, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mListenerHandler:Landroid/os/Handler;

    .line 200
    # getter for: Lcom/google/android/music/art/ArtLoaderImpl;->sRequestCount:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {p1}, Lcom/google/android/music/art/ArtLoaderImpl;->access$200(Lcom/google/android/music/art/ArtLoaderImpl;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mRequestNumber:I

    .line 201
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;)Lcom/google/android/music/art/ArtLoader$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mListener:Lcom/google/android/music/art/ArtLoader$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;

    .prologue
    .line 186
    iget v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mRequestNumber:I

    return v0
.end method


# virtual methods
.method protected getFileDescriptor()Landroid/os/ParcelFileDescriptor;
    .locals 7

    .prologue
    .line 207
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->this$0:Lcom/google/android/music/art/ArtLoaderImpl;

    # getter for: Lcom/google/android/music/art/ArtLoaderImpl;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/art/ArtLoaderImpl;->access$300(Lcom/google/android/music/art/ArtLoaderImpl;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mRemoteArtUrl:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mMode:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/music/store/MusicContent$UrlArt;->openFileDescriptor(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;II)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 213
    .local v1, "pfd":Landroid/os/ParcelFileDescriptor;
    :goto_0
    return-object v1

    .line 209
    .end local v1    # "pfd":Landroid/os/ParcelFileDescriptor;
    :catch_0
    move-exception v0

    .line 210
    .local v0, "e":Ljava/io/FileNotFoundException;
    const/4 v1, 0x0

    .restart local v1    # "pfd":Landroid/os/ParcelFileDescriptor;
    goto :goto_0
.end method

.method protected notifyListener(Landroid/os/ParcelFileDescriptor;)V
    .locals 3
    .param p1, "pfd"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 217
    iget-object v1, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mListener:Lcom/google/android/music/art/ArtLoader$Listener;

    if-eqz v1, :cond_0

    .line 218
    new-instance v0, Lcom/google/android/music/art/ArtLoaderImpl$ResultRunnable;

    iget-object v1, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mListener:Lcom/google/android/music/art/ArtLoader$Listener;

    iget-object v2, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mRemoteArtUrl:Ljava/lang/String;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/music/art/ArtLoaderImpl$ResultRunnable;-><init>(Landroid/os/ParcelFileDescriptor;Lcom/google/android/music/art/ArtLoader$Listener;Ljava/lang/String;)V

    .line 220
    .local v0, "runnable":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mListenerHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 221
    iget-object v1, p0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->mListenerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 226
    .end local v0    # "runnable":Ljava/lang/Runnable;
    :cond_0
    :goto_0
    return-void

    .line 223
    .restart local v0    # "runnable":Ljava/lang/Runnable;
    :cond_1
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method
