.class Lcom/google/android/music/leanback/LeanbackNowPlayingView$1;
.super Landroid/content/BroadcastReceiver;
.source "LeanbackNowPlayingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackNowPlayingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackNowPlayingView;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackNowPlayingView;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView$1;->this$0:Lcom/google/android/music/leanback/LeanbackNowPlayingView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 85
    const-string v3, "playing"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 87
    .local v0, "isPlaying":Z
    const-string v3, "streaming"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 89
    .local v2, "isStreaming":Z
    const-string v3, "preparing"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 92
    .local v1, "isPreparing":Z
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView$1;->this$0:Lcom/google/android/music/leanback/LeanbackNowPlayingView;

    # invokes: Lcom/google/android/music/leanback/LeanbackNowPlayingView;->updatePlayState(ZZZ)V
    invoke-static {v3, v0, v2, v1}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->access$000(Lcom/google/android/music/leanback/LeanbackNowPlayingView;ZZZ)V

    .line 93
    return-void
.end method
