.class public Lcom/google/android/music/sync/google/model/MagicPlaylistRequest$Builder;
.super Ljava/lang/Object;
.source "MagicPlaylistRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/sync/google/model/MagicPlaylistRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mPlaylistRequest:Lcom/google/android/music/sync/google/model/MagicPlaylistRequest;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lcom/google/android/music/sync/google/model/MagicPlaylistRequest;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/model/MagicPlaylistRequest;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/sync/google/model/MagicPlaylistRequest$Builder;->mPlaylistRequest:Lcom/google/android/music/sync/google/model/MagicPlaylistRequest;

    return-void
.end method
