.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToMyLibrary;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$AsyncMenuEntry;
.source "DocumentMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddToMyLibrary"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V
    .locals 2
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 1116
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToMyLibrary;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 1117
    const v0, 0x7f0e0027

    const v1, 0x7f0b0242

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$AsyncMenuEntry;-><init>(Landroid/content/res/Resources;II)V

    .line 1118
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 3

    .prologue
    .line 1122
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToMyLibrary;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToMyLibrary;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v1, v2, :cond_1

    .line 1124
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToMyLibrary;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToMyLibrary;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    .line 1125
    .local v0, "songList":Lcom/google/android/music/medialist/SongList;
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToMyLibrary;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/google/android/music/ui/AddToLibraryButton;->addToLibrary(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;Z)V

    .line 1129
    .end local v0    # "songList":Lcom/google/android/music/medialist/SongList;
    :goto_0
    return-void

    .line 1127
    :cond_1
    const-string v1, "DocumentMenuHandler"

    const-string v2, "Unexpected doc type"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
