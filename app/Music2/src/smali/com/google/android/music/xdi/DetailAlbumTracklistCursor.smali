.class Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;
.super Landroid/database/MatrixCursor;
.source "DetailAlbumTracklistCursor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "albumId"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 38
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mContext:Landroid/content/Context;

    .line 39
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 41
    invoke-direct {p0, p3}, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->addAlbumTracks(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method private addAlbumTracks(Ljava/lang/String;)V
    .locals 28
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 45
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    sget-object v24, Lcom/google/android/music/xdi/TrackAdapter;->TRACK_COLUMNS:[Ljava/lang/String;

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/google/android/music/xdi/XdiUtils;->getAudioInAlbumCursor(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 48
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v23

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    .line 53
    .local v22, "values":[Ljava/lang/Object;
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v14

    .line 54
    .local v14, "isNautilusAlbum":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumArtUriOrDefault(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 56
    .local v4, "albumArtUrl":Ljava/lang/String;
    invoke-direct/range {p0 .. p1}, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->getSongListForAlbum(Ljava/lang/String;)Lcom/google/android/music/medialist/SongList;

    move-result-object v18

    .line 57
    .local v18, "songList":Lcom/google/android/music/medialist/SongList;
    if-eqz v18, :cond_0

    .line 60
    new-instance v20, Lcom/google/android/music/xdi/TrackAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v8}, Lcom/google/android/music/xdi/TrackAdapter;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;Landroid/database/Cursor;)V

    .line 61
    .local v20, "trackAdapter":Lcom/google/android/music/xdi/TrackAdapter;
    const/4 v9, 0x0

    .local v9, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    move-object v10, v9

    .line 64
    .end local v9    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .local v10, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :goto_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v23

    if-eqz v23, :cond_4

    .line 65
    const/16 v23, 0x0

    invoke-static/range {v22 .. v23}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 66
    invoke-interface {v8}, Landroid/database/Cursor;->getPosition()I

    move-result v15

    .line 67
    .local v15, "position":I
    if-nez v10, :cond_5

    .line 68
    new-instance v9, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v9}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    .end local v10    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v9    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :goto_2
    :try_start_1
    move-object/from16 v0, v20

    invoke-virtual {v0, v9, v8}, Lcom/google/android/music/xdi/TrackAdapter;->populateDocumentFromCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)V

    .line 71
    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v19

    .line 72
    .local v19, "title":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v16

    .line 73
    .local v16, "songId":J
    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v21

    .line 74
    .local v21, "trackMetajamId":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v7

    .line 75
    .local v7, "artistName":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumName()Ljava/lang/String;

    move-result-object v5

    .line 76
    .local v5, "albumName":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getDuration()J

    move-result-wide v24

    const-wide/16 v26, 0x3e8

    div-long v12, v24, v26

    .line 77
    .local v12, "duration":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-static {v0, v12, v13}, Lcom/google/android/music/utils/MusicUtils;->makeTimeStringSync(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v11

    .line 78
    .local v11, "durationString":Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 79
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    const v24, 0x7f0b00c4

    invoke-virtual/range {v23 .. v24}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 82
    :cond_2
    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v6

    .line 83
    .local v6, "artUrl":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_3

    .line 84
    move-object v6, v4

    .line 87
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    move-object/from16 v23, v0

    const-string v24, "_id"

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 88
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    move-object/from16 v23, v0

    const-string v24, "display_name"

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    move-object/from16 v23, v0

    const-string v24, "display_subname"

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 91
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    move-object/from16 v23, v0

    const-string v24, "display_length"

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 93
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    move-object/from16 v23, v0

    const-string v24, "display_description"

    const/16 v25, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 94
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    move-object/from16 v23, v0

    const-string v24, "display_number"

    add-int/lit8 v25, v15, 0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 96
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    move-object/from16 v23, v0

    const-string v24, "image_uri"

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 97
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    move-object/from16 v23, v0

    const-string v24, "item_display_type"

    const/16 v25, 0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 99
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    move-object/from16 v23, v0

    const-string v24, "music_duration"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    move-object/from16 v23, v0

    const-string v24, "music_trackArtist"

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 101
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    move-object/from16 v23, v0

    const-string v24, "music_album"

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 102
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    move-object/from16 v23, v0

    const-string v24, "music_trackImageUri"

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 105
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;->addRow([Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v10, v9

    .line 106
    .end local v9    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v10    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    goto/16 :goto_1

    .line 108
    .end local v5    # "albumName":Ljava/lang/String;
    .end local v6    # "artUrl":Ljava/lang/String;
    .end local v7    # "artistName":Ljava/lang/String;
    .end local v11    # "durationString":Ljava/lang/String;
    .end local v12    # "duration":J
    .end local v15    # "position":I
    .end local v16    # "songId":J
    .end local v19    # "title":Ljava/lang/String;
    .end local v21    # "trackMetajamId":Ljava/lang/String;
    :cond_4
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v23

    move-object v9, v10

    .end local v10    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v9    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :goto_3
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v23

    .restart local v15    # "position":I
    :catchall_1
    move-exception v23

    goto :goto_3

    .end local v9    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v10    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_5
    move-object v9, v10

    .end local v10    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v9    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    goto/16 :goto_2
.end method

.method private getSongListForAlbum(Ljava/lang/String;)Lcom/google/android/music/medialist/SongList;
    .locals 7
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 113
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v2

    .line 114
    .local v2, "isNautilusAlbum":Z
    if-eqz v2, :cond_0

    .line 115
    new-instance v3, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    invoke-direct {v3, p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;-><init>(Ljava/lang/String;)V

    .line 124
    :goto_0
    return-object v3

    .line 119
    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 124
    .local v0, "albumIdLong":Ljava/lang/Long;
    new-instance v3, Lcom/google/android/music/medialist/AlbumSongList;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    goto :goto_0

    .line 120
    .end local v0    # "albumIdLong":Ljava/lang/Long;
    :catch_0
    move-exception v1

    .line 121
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string v3, "MusicXdi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error converting album ID to long: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const/4 v3, 0x0

    goto :goto_0
.end method
