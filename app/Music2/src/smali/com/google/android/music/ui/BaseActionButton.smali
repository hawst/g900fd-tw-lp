.class public abstract Lcom/google/android/music/ui/BaseActionButton;
.super Landroid/widget/TextView;
.source "BaseActionButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;,
        Lcom/google/android/music/ui/BaseActionButton$ClickHandlerTask;
    }
.end annotation


# instance fields
.field private mListener:Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;

.field private mMediaList:Lcom/google/android/music/medialist/MediaList;

.field private mTextLabel:Ljava/lang/String;

.field private mTextShown:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "textId"    # I
    .param p4, "drawableId"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 47
    .local v0, "resources":Landroid/content/res/Resources;
    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/BaseActionButton;->mTextLabel:Ljava/lang/String;

    .line 49
    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/BaseActionButton;->setLongClickable(Z)V

    .line 50
    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/BaseActionButton;->setClickable(Z)V

    .line 51
    invoke-virtual {p0, p0}, Lcom/google/android/music/ui/BaseActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    invoke-virtual {p0, p0}, Lcom/google/android/music/ui/BaseActionButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 55
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseActionButton;->setTypeface(Landroid/graphics/Typeface;)V

    .line 56
    const v1, 0x7f0c007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseActionButton;->setTextColor(I)V

    .line 57
    const/16 v1, 0x11

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseActionButton;->setGravity(I)V

    .line 58
    invoke-virtual {p0, p4, v2, v2, v2}, Lcom/google/android/music/ui/BaseActionButton;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 59
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActionButton;->mTextLabel:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseActionButton;->setText(Ljava/lang/CharSequence;)V

    .line 60
    const v1, 0x7f0f00c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseActionButton;->setCompoundDrawablePadding(I)V

    .line 61
    const v1, 0x7f0201f5

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseActionButton;->setBackgroundResource(I)V

    .line 62
    iput-boolean v3, p0, Lcom/google/android/music/ui/BaseActionButton;->mTextShown:Z

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/BaseActionButton;)Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActionButton;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActionButton;->mListener:Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;

    return-object v0
.end method


# virtual methods
.method protected abstract handleAction(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;)V
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActionButton;->mListener:Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/BaseActionButton;->mListener:Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;

    invoke-interface {v0}, Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;->onActionStart()V

    .line 105
    :cond_0
    new-instance v0, Lcom/google/android/music/ui/BaseActionButton$ClickHandlerTask;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActionButton;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/music/ui/BaseActionButton$ClickHandlerTask;-><init>(Lcom/google/android/music/ui/BaseActionButton;Lcom/google/android/music/ui/BaseActionButton;Lcom/google/android/music/medialist/MediaList;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 106
    return-void
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActionButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActionButton;->mTextLabel:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 114
    const/4 v0, 0x1

    return v0
.end method

.method public final onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 122
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 124
    .local v0, "newWidthSpec":I
    invoke-super {p0, v0, p2}, Landroid/widget/TextView;->onMeasure(II)V

    .line 125
    return-void
.end method

.method public setActionButtonListener(Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActionButton;->mListener:Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;

    .line 99
    return-void
.end method

.method public setMediaList(Lcom/google/android/music/medialist/MediaList;)V
    .locals 0
    .param p1, "mediaList"    # Lcom/google/android/music/medialist/MediaList;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActionButton;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    .line 92
    return-void
.end method
