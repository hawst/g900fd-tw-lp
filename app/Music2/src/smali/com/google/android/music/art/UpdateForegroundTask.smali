.class public Lcom/google/android/music/art/UpdateForegroundTask;
.super Ljava/lang/Object;
.source "UpdateForegroundTask.java"

# interfaces
.implements Lcom/google/android/music/art/ArtRequest$Listener;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/UpdateForegroundTask$Callback;
    }
.end annotation


# instance fields
.field protected final mArtRequest:Lcom/google/android/music/art/ArtRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/music/art/ArtRequest",
            "<*>;"
        }
    .end annotation
.end field

.field protected final mContext:Landroid/content/Context;

.field protected final mCropToCircle:Z

.field protected final mDefaultArtId:I

.field protected final mHeight:I

.field protected volatile mIsCanceled:Z

.field protected final mOwner:Lcom/google/android/music/art/UpdateForegroundTask$Callback;

.field protected final mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/art/UpdateForegroundTask$Callback;Lcom/google/android/music/art/ArtRequest;ZIII)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "owner"    # Lcom/google/android/music/art/UpdateForegroundTask$Callback;
    .param p4, "circleCrop"    # Z
    .param p5, "defaultArtId"    # I
    .param p6, "width"    # I
    .param p7, "height"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/music/art/UpdateForegroundTask$Callback;",
            "Lcom/google/android/music/art/ArtRequest",
            "<*>;ZIII)V"
        }
    .end annotation

    .prologue
    .local p3, "artRequest":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<*>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-boolean v2, p0, Lcom/google/android/music/art/UpdateForegroundTask;->mIsCanceled:Z

    .line 28
    if-lez p6, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "width must be > 0"

    invoke-static {v0, v3}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 29
    if-lez p7, :cond_1

    :goto_1
    const-string v0, "height must be > 0"

    invoke-static {v1, v0}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 30
    invoke-static {p2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    invoke-static {p3}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    iput-object p1, p0, Lcom/google/android/music/art/UpdateForegroundTask;->mContext:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Lcom/google/android/music/art/UpdateForegroundTask;->mOwner:Lcom/google/android/music/art/UpdateForegroundTask$Callback;

    .line 34
    iput-object p3, p0, Lcom/google/android/music/art/UpdateForegroundTask;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    .line 35
    iput-boolean p4, p0, Lcom/google/android/music/art/UpdateForegroundTask;->mCropToCircle:Z

    .line 38
    iget-object v0, p0, Lcom/google/android/music/art/UpdateForegroundTask;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    invoke-virtual {v0, p0}, Lcom/google/android/music/art/ArtRequest;->addListener(Lcom/google/android/music/art/ArtRequest$Listener;)V

    .line 39
    iput p5, p0, Lcom/google/android/music/art/UpdateForegroundTask;->mDefaultArtId:I

    .line 40
    iput p6, p0, Lcom/google/android/music/art/UpdateForegroundTask;->mWidth:I

    .line 41
    iput p7, p0, Lcom/google/android/music/art/UpdateForegroundTask;->mHeight:I

    .line 42
    return-void

    :cond_0
    move v0, v2

    .line 28
    goto :goto_0

    :cond_1
    move v1, v2

    .line 29
    goto :goto_1
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/art/UpdateForegroundTask;->mIsCanceled:Z

    .line 71
    invoke-static {}, Lcom/google/android/music/art/ArtUtils;->getHandler()Lcom/google/android/music/art/ArtUtils$ArtHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/music/art/ArtUtils$ArtHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 74
    invoke-static {}, Lcom/google/android/music/art/ArtUtils;->getHandler()Lcom/google/android/music/art/ArtUtils$ArtHandler;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/art/UpdateForegroundTask$2;

    invoke-direct {v1, p0}, Lcom/google/android/music/art/UpdateForegroundTask$2;-><init>(Lcom/google/android/music/art/UpdateForegroundTask;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/art/ArtUtils$ArtHandler;->postMainThread(Ljava/lang/Runnable;)Z

    .line 80
    return-void
.end method

.method public onLoadError(Lcom/google/android/music/art/ArtRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest;

    .prologue
    .line 99
    return-void
.end method

.method public onLoadFinished(Lcom/google/android/music/art/ArtRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest;

    .prologue
    .line 95
    return-void
.end method

.method protected renderForeground()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return-object v0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/music/art/UpdateForegroundTask;->renderForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 47
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-static {}, Lcom/google/android/music/art/ArtUtils;->getHandler()Lcom/google/android/music/art/ArtUtils$ArtHandler;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/art/UpdateForegroundTask$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/art/UpdateForegroundTask$1;-><init>(Lcom/google/android/music/art/UpdateForegroundTask;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/art/ArtUtils$ArtHandler;->postMainThread(Ljava/lang/Runnable;)Z

    .line 55
    return-void
.end method

.method public final schedule()V
    .locals 1

    .prologue
    .line 86
    invoke-static {}, Lcom/google/android/music/art/ArtUtils;->getHandler()Lcom/google/android/music/art/ArtUtils$ArtHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/music/art/ArtUtils$ArtHandler;->post(Ljava/lang/Runnable;)Z

    .line 87
    return-void
.end method

.method public showDefaultArt(Lcom/google/android/music/art/ArtRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest;

    .prologue
    .line 97
    return-void
.end method
