.class final Lcom/google/android/music/cloudclient/QuizArtistJson$1;
.super Ljava/lang/Object;
.source "QuizArtistJson.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/QuizArtistJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/music/cloudclient/QuizArtistJson;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/cloudclient/QuizArtistJson;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/music/cloudclient/QuizArtistJson;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/QuizArtistJson;-><init>()V

    .line 39
    .local v0, "artist":Lcom/google/android/music/cloudclient/QuizArtistJson;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/music/cloudclient/QuizArtistJson;->mName:Ljava/lang/String;

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/music/cloudclient/QuizArtistJson;->mArtistId:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/music/cloudclient/QuizArtistJson;->mArtistArtUrl:Ljava/lang/String;

    .line 42
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/google/android/music/cloudclient/QuizArtistJson$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/cloudclient/QuizArtistJson;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/music/cloudclient/QuizArtistJson;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 47
    new-array v0, p1, [Lcom/google/android/music/cloudclient/QuizArtistJson;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/google/android/music/cloudclient/QuizArtistJson$1;->newArray(I)[Lcom/google/android/music/cloudclient/QuizArtistJson;

    move-result-object v0

    return-object v0
.end method
