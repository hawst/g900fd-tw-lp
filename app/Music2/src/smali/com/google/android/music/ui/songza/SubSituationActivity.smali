.class public Lcom/google/android/music/ui/songza/SubSituationActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "SubSituationActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/SubSituationActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v5

    if-nez v5, :cond_1

    .line 24
    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/SubSituationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 25
    .local v3, "intent":Landroid/content/Intent;
    const/4 v2, 0x0

    .line 26
    .local v2, "id":Ljava/lang/String;
    const/4 v4, 0x0

    .line 27
    .local v4, "title":Ljava/lang/String;
    const/4 v0, 0x0

    .line 28
    .local v0, "artUrl":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 29
    const-string v5, "subSituationId"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 30
    const-string v5, "subSituationTitle"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 31
    const-string v5, "subSituationArtUrl"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 33
    :cond_0
    invoke-static {v2, v4, v0}, Lcom/google/android/music/ui/songza/SubSituationFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/ui/songza/SubSituationFragment;

    move-result-object v1

    .line 34
    .local v1, "fragment":Landroid/support/v4/app/Fragment;
    const/4 v5, 0x0

    invoke-virtual {p0, v1, v5}, Lcom/google/android/music/ui/songza/SubSituationActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    .line 36
    .end local v0    # "artUrl":Ljava/lang/String;
    .end local v1    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "title":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method protected usesActionBarOverlay()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method
