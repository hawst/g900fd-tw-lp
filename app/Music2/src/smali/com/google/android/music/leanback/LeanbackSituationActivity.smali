.class public Lcom/google/android/music/leanback/LeanbackSituationActivity;
.super Lcom/google/android/music/leanback/LeanbackBrowseActivity;
.source "LeanbackSituationActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mSituationId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/LeanbackSituationActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected buildRow(Landroid/database/Cursor;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 55
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 56
    .local v1, "situationId":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 58
    .local v2, "title":Ljava/lang/String;
    new-instance v0, Lcom/google/android/music/leanback/LeanbackSituationActivity$2;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSituationActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lcom/google/android/music/leanback/LeanbackSituationActivity$2;-><init>(Lcom/google/android/music/leanback/LeanbackSituationActivity;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 65
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    add-int/lit16 v3, v3, 0x130

    invoke-static {v1}, Lcom/google/android/music/store/MusicContent$SituationRadios;->getRadioStationsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/ui/songza/SubSituationFragment;->RADIO_PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v3, v0, v4, v5}, Lcom/google/android/music/leanback/LeanbackSituationActivity;->loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V

    .line 69
    new-instance v3, Landroid/support/v17/leanback/widget/ListRow;

    new-instance v4, Landroid/support/v17/leanback/widget/HeaderItem;

    const/4 v5, 0x0

    invoke-direct {v4, v2, v5}, Landroid/support/v17/leanback/widget/HeaderItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v4, v0}, Landroid/support/v17/leanback/widget/ListRow;-><init>(Landroid/support/v17/leanback/widget/HeaderItem;Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    return-object v3
.end method

.method protected extractDataForSituationRadio(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 13
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 73
    const/4 v11, 0x0

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 74
    .local v4, "id":J
    const/4 v11, 0x1

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 75
    .local v6, "name":Ljava/lang/String;
    const/4 v11, 0x2

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 76
    .local v1, "description":Ljava/lang/String;
    const/4 v11, 0x3

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 78
    .local v0, "artUri":Ljava/lang/String;
    const/4 v11, 0x4

    invoke-interface {p1, v11}, Landroid/database/Cursor;->isNull(I)Z

    move-result v11

    if-eqz v11, :cond_0

    const/4 v8, 0x0

    .line 80
    .local v8, "radioSeedId":Ljava/lang/String;
    :goto_0
    const/4 v11, 0x5

    invoke-interface {p1, v11}, Landroid/database/Cursor;->isNull(I)Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v9, -0x1

    .line 82
    .local v9, "radioSeedType":I
    :goto_1
    const/4 v11, 0x6

    invoke-interface {p1, v11}, Landroid/database/Cursor;->isNull(I)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v7, 0x0

    .line 85
    .local v7, "radioRemoteId":Ljava/lang/String;
    :goto_2
    const/4 v11, 0x4

    if-ne v9, v11, :cond_3

    const/4 v10, 0x1

    .line 87
    .local v10, "wide":Z
    :goto_3
    new-instance v2, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v2}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 88
    .local v2, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    sget-object v11, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v2, v11}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 89
    invoke-virtual {v2, v4, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 90
    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 91
    invoke-virtual {v2, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 92
    invoke-virtual {v2, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioSeedId(Ljava/lang/String;)V

    .line 93
    invoke-virtual {v2, v9}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioSeedType(I)V

    .line 94
    invoke-virtual {v2, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioRemoteId(Ljava/lang/String;)V

    .line 95
    invoke-virtual {v2, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 97
    invoke-static {p0, v2}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/content/Intent;

    move-result-object v3

    .line 99
    .local v3, "intent":Landroid/content/Intent;
    new-instance v11, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v11}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v11, v6}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    invoke-virtual {v11, v1}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    invoke-virtual {v11, v0}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    invoke-virtual {v11, v10}, Lcom/google/android/music/leanback/Item$Builder;->wide(Z)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    invoke-virtual {v11, v3}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    const v12, 0x7f0200cf

    invoke-virtual {v11, v12}, Lcom/google/android/music/leanback/Item$Builder;->overlayResourceId(I)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v11

    return-object v11

    .line 78
    .end local v2    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v7    # "radioRemoteId":Ljava/lang/String;
    .end local v8    # "radioSeedId":Ljava/lang/String;
    .end local v9    # "radioSeedType":I
    .end local v10    # "wide":Z
    :cond_0
    const/4 v11, 0x4

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 80
    .restart local v8    # "radioSeedId":Ljava/lang/String;
    :cond_1
    const/4 v11, 0x5

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    goto :goto_1

    .line 82
    .restart local v9    # "radioSeedType":I
    :cond_2
    const/4 v11, 0x6

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_2

    .line 85
    .restart local v7    # "radioRemoteId":Ljava/lang/String;
    :cond_3
    const/4 v10, 0x0

    goto :goto_3
.end method

.method protected getBrowseTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSituationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getListRowsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 4

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/music/leanback/LeanbackSituationActivity$1;

    new-instance v1, Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-direct {v1}, Landroid/support/v17/leanback/widget/ListRowPresenter;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/leanback/LeanbackSituationActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackSituationActivity;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 48
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    const/16 v1, 0x12f

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSituationActivity;->mSituationId:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/music/store/MusicContent$Situations;->getSubSituationsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/songza/SituationCardHelper;->SITUATION_PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/music/leanback/LeanbackSituationActivity;->loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V

    .line 51
    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSituationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "situation_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSituationActivity;->mSituationId:Ljava/lang/String;

    .line 31
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    return-void
.end method
