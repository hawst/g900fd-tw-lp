.class Lcom/google/android/music/download/stream/StreamingClient$1;
.super Ljava/lang/Object;
.source "StreamingClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/download/stream/StreamingClient;->cancelNextStream()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/stream/StreamingClient;

.field final synthetic val$streamingContent:Lcom/google/android/music/download/stream/StreamingContent;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/stream/StreamingClient;Lcom/google/android/music/download/stream/StreamingContent;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/google/android/music/download/stream/StreamingClient$1;->this$0:Lcom/google/android/music/download/stream/StreamingClient;

    iput-object p2, p0, Lcom/google/android/music/download/stream/StreamingClient$1;->val$streamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient$1;->this$0:Lcom/google/android/music/download/stream/StreamingClient;

    # getter for: Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/music/download/stream/StreamingClient;->access$000(Lcom/google/android/music/download/stream/StreamingClient;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 234
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient$1;->this$0:Lcom/google/android/music/download/stream/StreamingClient;

    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingClient$1;->val$streamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    # invokes: Lcom/google/android/music/download/stream/StreamingClient;->handleCancelNextStreamIfMatching(Lcom/google/android/music/download/stream/StreamingContent;)V
    invoke-static {v0, v2}, Lcom/google/android/music/download/stream/StreamingClient;->access$100(Lcom/google/android/music/download/stream/StreamingClient;Lcom/google/android/music/download/stream/StreamingContent;)V

    .line 235
    monitor-exit v1

    .line 236
    return-void

    .line 235
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
