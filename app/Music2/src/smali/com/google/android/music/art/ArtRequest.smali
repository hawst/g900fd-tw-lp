.class public Lcom/google/android/music/art/ArtRequest;
.super Ljava/lang/Object;
.source "ArtRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/ArtRequest$Token;,
        Lcom/google/android/music/art/ArtRequest$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field public mArtType:Lcom/google/android/music/art/ArtType;

.field public final mDescriptor:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private mErrorLoading:Z

.field public final mImages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mIsDone:Z

.field private mIsPrefetchOnlyRequest:Z

.field private mIsRecycled:Z

.field private final mListeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/music/art/ArtRequest$Listener",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final mNeededItems:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ALBUM_ART:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/art/ArtRequest;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lcom/google/android/music/art/ArtType;)V
    .locals 2
    .param p2, "artType"    # Lcom/google/android/music/art/ArtType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/android/music/art/ArtType;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    .local p1, "descriptor":Ljava/lang/Object;, "TT;"
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/art/ArtRequest;->mImages:Ljava/util/List;

    .line 51
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/art/ArtRequest;->mListeners:Ljava/util/HashSet;

    .line 53
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/art/ArtRequest;->mNeededItems:Ljava/util/HashSet;

    .line 55
    iput-boolean v1, p0, Lcom/google/android/music/art/ArtRequest;->mErrorLoading:Z

    .line 56
    iput-boolean v1, p0, Lcom/google/android/music/art/ArtRequest;->mIsDone:Z

    .line 57
    iput-boolean v1, p0, Lcom/google/android/music/art/ArtRequest;->mIsRecycled:Z

    .line 61
    iput-boolean v1, p0, Lcom/google/android/music/art/ArtRequest;->mIsPrefetchOnlyRequest:Z

    .line 71
    iput-object p1, p0, Lcom/google/android/music/art/ArtRequest;->mDescriptor:Ljava/lang/Object;

    .line 72
    iput-object p2, p0, Lcom/google/android/music/art/ArtRequest;->mArtType:Lcom/google/android/music/art/ArtType;

    .line 73
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 42
    sget-boolean v0, Lcom/google/android/music/art/ArtRequest;->LOGV:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/art/ArtRequest;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtRequest;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest;->mListeners:Ljava/util/HashSet;

    return-object v0
.end method

.method private onLoadError(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 188
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    new-instance v0, Lcom/google/android/music/art/ArtRequest$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/art/ArtRequest$4;-><init>(Lcom/google/android/music/art/ArtRequest;)V

    invoke-static {v0, p1}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 198
    return-void
.end method

.method private onLoadFinished(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 157
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    new-instance v0, Lcom/google/android/music/art/ArtRequest$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/art/ArtRequest$2;-><init>(Lcom/google/android/music/art/ArtRequest;)V

    invoke-static {v0, p1}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 170
    return-void
.end method

.method private showDefaultArt(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 174
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    new-instance v0, Lcom/google/android/music/art/ArtRequest$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/art/ArtRequest$3;-><init>(Lcom/google/android/music/art/ArtRequest;)V

    invoke-static {v0, p1}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 184
    return-void
.end method


# virtual methods
.method public addListener(Lcom/google/android/music/art/ArtRequest$Listener;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest$Listener",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    .local p1, "listener":Lcom/google/android/music/art/ArtRequest$Listener;, "Lcom/google/android/music/art/ArtRequest$Listener<TT;>;"
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertMainThread()V

    .line 93
    iget-boolean v0, p0, Lcom/google/android/music/art/ArtRequest;->mIsPrefetchOnlyRequest:Z

    if-eqz v0, :cond_0

    .line 94
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unable to attach to prefetch-only request"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 98
    sget-boolean v0, Lcom/google/android/music/art/ArtRequest;->LOGV:Z

    if-eqz v0, :cond_1

    .line 99
    const-string v0, "ArtRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addListener, listeners="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mIsDone="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/art/ArtRequest;->mIsDone:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/music/art/ArtRequest;->mIsDone:Z

    if-eqz v0, :cond_3

    .line 102
    iget-boolean v0, p0, Lcom/google/android/music/art/ArtRequest;->mErrorLoading:Z

    if-eqz v0, :cond_2

    .line 103
    invoke-interface {p1, p0}, Lcom/google/android/music/art/ArtRequest$Listener;->onLoadError(Lcom/google/android/music/art/ArtRequest;)V

    .line 110
    :goto_0
    return-void

    .line 105
    :cond_2
    invoke-interface {p1, p0}, Lcom/google/android/music/art/ArtRequest$Listener;->onLoadFinished(Lcom/google/android/music/art/ArtRequest;)V

    goto :goto_0

    .line 108
    :cond_3
    invoke-interface {p1, p0}, Lcom/google/android/music/art/ArtRequest$Listener;->showDefaultArt(Lcom/google/android/music/art/ArtRequest;)V

    goto :goto_0
.end method

.method addNeededItem(Ljava/lang/Object;)V
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 140
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/music/art/ArtRequest;->mIsPrefetchOnlyRequest:Z

    if-eqz v0, :cond_0

    .line 142
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest;->mNeededItems:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 319
    if-ne p0, p1, :cond_1

    .line 330
    :cond_0
    :goto_0
    return v1

    .line 320
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 322
    check-cast v0, Lcom/google/android/music/art/ArtRequest;

    .line 324
    .local v0, "that":Lcom/google/android/music/art/ArtRequest;
    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest;->mArtType:Lcom/google/android/music/art/ArtType;

    iget-object v4, v0, Lcom/google/android/music/art/ArtRequest;->mArtType:Lcom/google/android/music/art/ArtType;

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 325
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest;->mDescriptor:Ljava/lang/Object;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest;->mDescriptor:Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/music/art/ArtRequest;->mDescriptor:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    .line 327
    goto :goto_0

    .line 325
    :cond_5
    iget-object v3, v0, Lcom/google/android/music/art/ArtRequest;->mDescriptor:Ljava/lang/Object;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method getArtKey()Lcom/google/android/music/art/ArtKey;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/music/art/ArtKey",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 267
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    new-instance v0, Lcom/google/android/music/art/ArtKey;

    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest;->mArtType:Lcom/google/android/music/art/ArtType;

    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest;->mDescriptor:Ljava/lang/Object;

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/art/ArtKey;-><init>(Lcom/google/android/music/art/ArtType;Ljava/lang/Object;)V

    return-object v0
.end method

.method public getErrorLoading()Z
    .locals 1

    .prologue
    .line 300
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/music/art/ArtRequest;->mErrorLoading:Z

    return v0
.end method

.method public getToken()Lcom/google/android/music/art/ArtRequest$Token;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/music/art/ArtRequest$Token",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 288
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    new-instance v0, Lcom/google/android/music/art/ArtRequest$Token;

    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest;->mDescriptor:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest;->mArtType:Lcom/google/android/music/art/ArtType;

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/art/ArtRequest$Token;-><init>(Ljava/lang/Object;Lcom/google/android/music/art/ArtType;)V

    return-object v0
.end method

.method public hasNeededItem(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 152
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest;->mNeededItems:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    const/4 v1, 0x0

    .line 335
    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest;->mDescriptor:Ljava/lang/Object;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest;->mDescriptor:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 336
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest;->mArtType:Lcom/google/android/music/art/ArtType;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest;->mArtType:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v1}, Lcom/google/android/music/art/ArtType;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 337
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 335
    goto :goto_0
.end method

.method public isDoneLoading()Z
    .locals 1

    .prologue
    .line 296
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/music/art/ArtRequest;->mIsDone:Z

    return v0
.end method

.method isPrefetchOnlyRequest()Z
    .locals 1

    .prologue
    .line 341
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/music/art/ArtRequest;->mIsPrefetchOnlyRequest:Z

    return v0
.end method

.method public isRecycled()Z
    .locals 1

    .prologue
    .line 292
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/music/art/ArtRequest;->mIsRecycled:Z

    return v0
.end method

.method public isSuccessfullyLoaded()Z
    .locals 1

    .prologue
    .line 309
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/music/art/ArtRequest;->mIsDone:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/art/ArtRequest;->mErrorLoading:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method notifyError(Landroid/content/Context;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    .line 232
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest;->mNeededItems:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest;->mNeededItems:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/art/ArtRequest;->mErrorLoading:Z

    .line 235
    invoke-virtual {p0, p1}, Lcom/google/android/music/art/ArtRequest;->notifyIfDone(Landroid/content/Context;)Z

    move-result v0

    .line 237
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method notifyIfDone(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    const/4 v0, 0x1

    .line 207
    sget-boolean v1, Lcom/google/android/music/art/ArtRequest;->LOGV:Z

    if-eqz v1, :cond_0

    .line 208
    const-string v1, "ArtRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyIfDone: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest;->mNeededItems:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 211
    iput-boolean v0, p0, Lcom/google/android/music/art/ArtRequest;->mIsDone:Z

    .line 212
    iget-boolean v1, p0, Lcom/google/android/music/art/ArtRequest;->mErrorLoading:Z

    if-eqz v1, :cond_1

    .line 213
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtRequest;->onLoadError(Landroid/content/Context;)V

    .line 217
    :goto_0
    invoke-static {p1}, Lcom/google/android/music/art/ArtResolver;->getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/music/art/ArtResolver;->removeRequest(Lcom/google/android/music/art/ArtRequest;)V

    .line 222
    :goto_1
    return v0

    .line 215
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtRequest;->onLoadFinished(Landroid/content/Context;)V

    goto :goto_0

    .line 220
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtRequest;->showDefaultArt(Landroid/content/Context;)V

    .line 222
    const/4 v0, 0x0

    goto :goto_1
.end method

.method notifyImageLoaded(Landroid/content/Context;Ljava/lang/Object;Landroid/graphics/Bitmap;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "missingItem"    # Ljava/lang/Object;
    .param p3, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 248
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    sget-boolean v0, Lcom/google/android/music/art/ArtRequest;->LOGV:Z

    if-eqz v0, :cond_0

    .line 249
    const-string v0, "ArtRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyImageLoaded: bitmap="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " missingItem="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "this="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    :cond_0
    sget-boolean v0, Lcom/google/android/music/art/ArtRequest;->LOGV:Z

    if-eqz v0, :cond_1

    if-nez p3, :cond_1

    .line 253
    const-string v0, "ArtRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyImageLoaded: WTF, null bitmap: missingItem="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bmp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest;->mNeededItems:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 257
    if-eqz p3, :cond_2

    .line 258
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest;->mImages:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/art/ArtRequest;->mNeededItems:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 261
    invoke-virtual {p0, p1}, Lcom/google/android/music/art/ArtRequest;->notifyIfDone(Landroid/content/Context;)Z

    move-result v0

    .line 263
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeListener(Lcom/google/android/music/art/ArtRequest$Listener;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest$Listener",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    .local p1, "listener":Lcom/google/android/music/art/ArtRequest$Listener;, "Lcom/google/android/music/art/ArtRequest$Listener<TT;>;"
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertMainThread()V

    .line 114
    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 115
    sget-boolean v1, Lcom/google/android/music/art/ArtRequest;->LOGV:Z

    if-eqz v1, :cond_0

    .line 116
    const-string v1, "ArtRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeListener, remaining listeners="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/music/art/ArtRequest;->mIsRecycled:Z

    .line 119
    iget-boolean v1, p0, Lcom/google/android/music/art/ArtRequest;->mIsRecycled:Z

    if-eqz v1, :cond_1

    .line 120
    invoke-static {}, Lcom/google/android/music/art/ArtResolver;->getInstance()Lcom/google/android/music/art/ArtResolver;

    move-result-object v0

    .line 121
    .local v0, "resolver":Lcom/google/android/music/art/ArtResolver;
    invoke-virtual {v0, p0}, Lcom/google/android/music/art/ArtResolver;->removeRequest(Lcom/google/android/music/art/ArtRequest;)V

    .line 125
    new-instance v1, Lcom/google/android/music/art/ArtRequest$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/music/art/ArtRequest$1;-><init>(Lcom/google/android/music/art/ArtRequest;Lcom/google/android/music/art/ArtResolver;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/art/ArtResolver;->executeOnBackgroundWorker(Ljava/lang/Runnable;)V

    .line 135
    .end local v0    # "resolver":Lcom/google/android/music/art/ArtResolver;
    :cond_1
    return-void
.end method

.method setPrefetchOnlyRequest(Z)V
    .locals 0
    .param p1, "isPrefetchOnlyRequest"    # Z

    .prologue
    .line 345
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    iput-boolean p1, p0, Lcom/google/android/music/art/ArtRequest;->mIsPrefetchOnlyRequest:Z

    .line 346
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ArtRequest{mArtType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest;->mArtType:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDescriptor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/art/ArtRequest;->mDescriptor:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsDone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/art/ArtRequest;->mIsDone:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mErrorLoading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/art/ArtRequest;->mErrorLoading:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsRecycled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/art/ArtRequest;->mIsRecycled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public tokenEqualsRequest(Lcom/google/android/music/art/ArtRequest$Token;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest$Token",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<TT;>;"
    .local p1, "token":Lcom/google/android/music/art/ArtRequest$Token;, "Lcom/google/android/music/art/ArtRequest$Token<*>;"
    const/4 v0, 0x0

    .line 313
    if-nez p1, :cond_1

    .line 314
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p1, Lcom/google/android/music/art/ArtRequest$Token;->mDescriptor:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest;->mDescriptor:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/music/art/ArtRequest$Token;->mArtType:Lcom/google/android/music/art/ArtType;

    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest;->mArtType:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v1, v2}, Lcom/google/android/music/art/ArtType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
