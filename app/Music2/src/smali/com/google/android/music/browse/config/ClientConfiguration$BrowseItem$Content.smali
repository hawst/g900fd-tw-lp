.class public Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;
.super Ljava/lang/Object;
.source "ClientConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Content"
.end annotation


# instance fields
.field private final mContent:Ljava/lang/String;

.field private final mMax:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "content"    # Ljava/lang/String;
    .param p2, "max"    # I

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    iput-object p1, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;->mContent:Ljava/lang/String;

    .line 158
    iput p2, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;->mMax:I

    .line 159
    return-void
.end method


# virtual methods
.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;->mContent:Ljava/lang/String;

    return-object v0
.end method

.method public getMax()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;->mMax:I

    return v0
.end method
