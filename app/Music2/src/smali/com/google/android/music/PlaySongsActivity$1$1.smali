.class Lcom/google/android/music/PlaySongsActivity$1$1;
.super Ljava/lang/Object;
.source "PlaySongsActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/PlaySongsActivity$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/PlaySongsActivity$1;


# direct methods
.method constructor <init>(Lcom/google/android/music/PlaySongsActivity$1;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/music/PlaySongsActivity$1$1;->this$1:Lcom/google/android/music/PlaySongsActivity$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 107
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity$1$1;->this$1:Lcom/google/android/music/PlaySongsActivity$1;

    iget-object v1, v1, Lcom/google/android/music/PlaySongsActivity$1;->this$0:Lcom/google/android/music/PlaySongsActivity;

    # getter for: Lcom/google/android/music/PlaySongsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v1}, Lcom/google/android/music/PlaySongsActivity;->access$000(Lcom/google/android/music/PlaySongsActivity;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity$1$1;->this$1:Lcom/google/android/music/PlaySongsActivity$1;

    iget-object v1, v1, Lcom/google/android/music/PlaySongsActivity$1;->this$0:Lcom/google/android/music/PlaySongsActivity;

    # getter for: Lcom/google/android/music/PlaySongsActivity;->mAsyncWorker:Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;
    invoke-static {v1}, Lcom/google/android/music/PlaySongsActivity;->access$100(Lcom/google/android/music/PlaySongsActivity;)Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->sendEmptyMessage(I)Z

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity$1$1;->this$1:Lcom/google/android/music/PlaySongsActivity$1;

    iget-object v1, v1, Lcom/google/android/music/PlaySongsActivity$1;->this$0:Lcom/google/android/music/PlaySongsActivity;

    # invokes: Lcom/google/android/music/PlaySongsActivity;->doesMarketAccountMatchMusicAccount()Z
    invoke-static {v1}, Lcom/google/android/music/PlaySongsActivity;->access$200(Lcom/google/android/music/PlaySongsActivity;)Z

    move-result v0

    .line 111
    .local v0, "accountMatch":Z
    if-eqz v0, :cond_2

    .line 112
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity$1$1;->this$1:Lcom/google/android/music/PlaySongsActivity$1;

    iget-object v1, v1, Lcom/google/android/music/PlaySongsActivity$1;->this$0:Lcom/google/android/music/PlaySongsActivity;

    sget-object v2, Lcom/google/android/music/PlaySongsActivity$CheckState;->WAITING_FOR_SYNC:Lcom/google/android/music/PlaySongsActivity$CheckState;

    # invokes: Lcom/google/android/music/PlaySongsActivity;->startState(Lcom/google/android/music/PlaySongsActivity$CheckState;)V
    invoke-static {v1, v2}, Lcom/google/android/music/PlaySongsActivity;->access$300(Lcom/google/android/music/PlaySongsActivity;Lcom/google/android/music/PlaySongsActivity$CheckState;)V

    .line 113
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity$1$1;->this$1:Lcom/google/android/music/PlaySongsActivity$1;

    iget-object v1, v1, Lcom/google/android/music/PlaySongsActivity$1;->this$0:Lcom/google/android/music/PlaySongsActivity;

    # getter for: Lcom/google/android/music/PlaySongsActivity;->mAsyncWorker:Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;
    invoke-static {v1}, Lcom/google/android/music/PlaySongsActivity;->access$100(Lcom/google/android/music/PlaySongsActivity;)Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 114
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity$1$1;->this$1:Lcom/google/android/music/PlaySongsActivity$1;

    iget-object v1, v1, Lcom/google/android/music/PlaySongsActivity$1;->this$0:Lcom/google/android/music/PlaySongsActivity;

    invoke-virtual {v1}, Lcom/google/android/music/PlaySongsActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 115
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity$1$1;->this$1:Lcom/google/android/music/PlaySongsActivity$1;

    iget-object v1, v1, Lcom/google/android/music/PlaySongsActivity$1;->this$0:Lcom/google/android/music/PlaySongsActivity;

    sget-object v2, Lcom/google/android/music/PlaySongsActivity$CheckState;->ACCOUNT_MATCH_FAILURE:Lcom/google/android/music/PlaySongsActivity$CheckState;

    # invokes: Lcom/google/android/music/PlaySongsActivity;->startState(Lcom/google/android/music/PlaySongsActivity$CheckState;)V
    invoke-static {v1, v2}, Lcom/google/android/music/PlaySongsActivity;->access$300(Lcom/google/android/music/PlaySongsActivity;Lcom/google/android/music/PlaySongsActivity$CheckState;)V

    goto :goto_0
.end method
