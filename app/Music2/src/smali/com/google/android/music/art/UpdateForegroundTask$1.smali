.class Lcom/google/android/music/art/UpdateForegroundTask$1;
.super Ljava/lang/Object;
.source "UpdateForegroundTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/art/UpdateForegroundTask;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/art/UpdateForegroundTask;

.field final synthetic val$drawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method constructor <init>(Lcom/google/android/music/art/UpdateForegroundTask;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/art/UpdateForegroundTask$1;->this$0:Lcom/google/android/music/art/UpdateForegroundTask;

    iput-object p2, p0, Lcom/google/android/music/art/UpdateForegroundTask$1;->val$drawable:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/art/UpdateForegroundTask$1;->this$0:Lcom/google/android/music/art/UpdateForegroundTask;

    iget-boolean v0, v0, Lcom/google/android/music/art/UpdateForegroundTask;->mIsCanceled:Z

    if-eqz v0, :cond_0

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/art/UpdateForegroundTask$1;->this$0:Lcom/google/android/music/art/UpdateForegroundTask;

    iget-object v0, v0, Lcom/google/android/music/art/UpdateForegroundTask;->mOwner:Lcom/google/android/music/art/UpdateForegroundTask$Callback;

    iget-object v1, p0, Lcom/google/android/music/art/UpdateForegroundTask$1;->this$0:Lcom/google/android/music/art/UpdateForegroundTask;

    iget-object v2, p0, Lcom/google/android/music/art/UpdateForegroundTask$1;->val$drawable:Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, v1, v2}, Lcom/google/android/music/art/UpdateForegroundTask$Callback;->onFinishForegroundUpdate(Lcom/google/android/music/art/UpdateForegroundTask;Landroid/graphics/drawable/Drawable;)V

    .line 52
    iget-object v0, p0, Lcom/google/android/music/art/UpdateForegroundTask$1;->this$0:Lcom/google/android/music/art/UpdateForegroundTask;

    iget-object v0, v0, Lcom/google/android/music/art/UpdateForegroundTask;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    iget-object v1, p0, Lcom/google/android/music/art/UpdateForegroundTask$1;->this$0:Lcom/google/android/music/art/UpdateForegroundTask;

    invoke-virtual {v0, v1}, Lcom/google/android/music/art/ArtRequest;->removeListener(Lcom/google/android/music/art/ArtRequest$Listener;)V

    goto :goto_0
.end method
