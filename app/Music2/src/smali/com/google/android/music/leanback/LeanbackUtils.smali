.class public Lcom/google/android/music/leanback/LeanbackUtils;
.super Ljava/lang/Object;
.source "LeanbackUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/LeanbackUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method static extractItemForAlbums(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 335
    new-instance v8, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v8}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    invoke-static {v8, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->getAlbumDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v5

    .line 337
    .local v5, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v1

    .line 339
    .local v1, "artworkUrl":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v0

    .line 340
    .local v0, "albumId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 341
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 342
    const/4 v1, 0x0

    .line 354
    :cond_0
    :goto_0
    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v7

    .line 355
    .local v7, "name":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v4

    .line 356
    .local v4, "description":Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/google/android/music/leanback/LeanbackUtils;->getAlbumDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 358
    .local v6, "intent":Landroid/content/Intent;
    new-instance v8, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v8}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v8, v7}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v8

    invoke-virtual {v8, v1}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v8

    invoke-virtual {v8, v0}, Lcom/google/android/music/leanback/Item$Builder;->id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v8

    return-object v8

    .line 345
    .end local v4    # "description":Ljava/lang/String;
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v7    # "name":Ljava/lang/String;
    :cond_1
    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumId()J

    move-result-wide v2

    .line 346
    .local v2, "albumIdLong":J
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 347
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 348
    const/4 v8, 0x1

    invoke-static {p0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v9

    invoke-static {p0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v10

    invoke-static {v2, v3, v8, v9, v10}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method static extractItemForArtists(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 370
    new-instance v7, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v7}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    invoke-static {v7, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->getArtistDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v4

    .line 372
    .local v4, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v1

    .line 373
    .local v1, "artworkUrl":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 374
    const/4 v1, 0x0

    .line 377
    :cond_0
    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistMetajamId()Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, "artistId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 379
    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistId()J

    move-result-wide v2

    .line 380
    .local v2, "artistIdLong":J
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 382
    .end local v2    # "artistIdLong":J
    :cond_1
    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v6

    .line 383
    .local v6, "name":Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/google/android/music/leanback/LeanbackUtils;->getArtistDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 385
    .local v5, "intent":Landroid/content/Intent;
    new-instance v7, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v7}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v7, v6}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/android/music/leanback/Item$Builder;->id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/google/android/music/leanback/Item$Builder;->wide(Z)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v7

    return-object v7
.end method

.method public static extractItemForGroupType(Landroid/content/Context;Landroid/database/Cursor;I)Lcom/google/android/music/leanback/Item;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "groupType"    # I

    .prologue
    .line 273
    const/4 v0, 0x0

    .line 274
    .local v0, "item":Lcom/google/android/music/leanback/Item;
    packed-switch p2, :pswitch_data_0

    .line 288
    sget-object v1, Lcom/google/android/music/leanback/LeanbackUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected group type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    :goto_0
    return-object v0

    .line 276
    :pswitch_0
    invoke-static {p0, p1}, Lcom/google/android/music/leanback/LeanbackUtils;->extractItemForSongs(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    .line 277
    goto :goto_0

    .line 279
    :pswitch_1
    invoke-static {p0, p1}, Lcom/google/android/music/leanback/LeanbackUtils;->extractItemForAlbums(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    .line 280
    goto :goto_0

    .line 282
    :pswitch_2
    invoke-static {p0, p1}, Lcom/google/android/music/leanback/LeanbackUtils;->extractItemForArtists(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    .line 283
    goto :goto_0

    .line 285
    :pswitch_3
    invoke-static {p0, p1}, Lcom/google/android/music/leanback/LeanbackUtils;->extractItemForSharedPlaylists(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    .line 286
    goto :goto_0

    .line 274
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static extractItemForSharedPlaylists(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 398
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v1}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    invoke-static {v1, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->getPlaylistDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 400
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v2

    .line 401
    .local v2, "playlistId":J
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistName()Ljava/lang/String;

    move-result-object v5

    .line 402
    .local v5, "name":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getDescription()Ljava/lang/String;

    move-result-object v6

    .line 403
    .local v6, "description":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v8

    .line 405
    .local v8, "encodedArtUrl":Ljava/lang/String;
    new-instance v1, Lcom/google/android/music/medialist/SharedWithMeSongList;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistShareToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistOwnerName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getProfilePhotoUrl()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v1 .. v9}, Lcom/google/android/music/medialist/SharedWithMeSongList;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v1}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v10

    .line 409
    .local v10, "intent":Landroid/content/Intent;
    new-instance v1, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v1}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v1, v5}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    const v4, 0x7f0200ce

    invoke-virtual {v1, v4}, Lcom/google/android/music/leanback/Item$Builder;->overlayResourceId(I)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v1

    return-object v1
.end method

.method static extractItemForSongs(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 303
    new-instance v9, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v9}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    invoke-static {v9, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->getTrackDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v3

    .line 305
    .local v3, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v1

    .line 306
    .local v1, "artworkUrl":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 307
    const/4 v1, 0x0

    .line 310
    :cond_0
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v7

    .line 311
    .local v7, "name":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumName()Ljava/lang/String;

    move-result-object v0

    .line 313
    .local v0, "albumName":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v6

    .line 314
    .local v6, "metajamTrackId":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    const/4 v5, 0x1

    .line 315
    .local v5, "isNautilus":Z
    :goto_0
    if-eqz v5, :cond_2

    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newUnknownContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v2

    .line 318
    .local v2, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :goto_1
    if-eqz v5, :cond_3

    new-instance v8, Lcom/google/android/music/medialist/NautilusSingleSongList;

    invoke-direct {v8, v2, v6, v7}, Lcom/google/android/music/medialist/NautilusSingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    .local v8, "songList":Lcom/google/android/music/medialist/SongList;
    :goto_2
    invoke-static {p0, v8}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v4

    .line 323
    .local v4, "intent":Landroid/content/Intent;
    new-instance v9, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v9}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v9, v7}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9, v1}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9, v4}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v9

    return-object v9

    .line 314
    .end local v2    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v5    # "isNautilus":Z
    .end local v8    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 315
    .restart local v5    # "isNautilus":Z
    :cond_2
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v10

    invoke-static {v10, v11, v7}, Lcom/google/android/music/store/ContainerDescriptor;->newSingleSongDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v2

    goto :goto_1

    .line 318
    .restart local v2    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :cond_3
    new-instance v8, Lcom/google/android/music/medialist/SingleSongList;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v10

    invoke-direct {v8, v2, v10, v11, v7}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    goto :goto_2
.end method

.method public static getAlbumDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 187
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "album_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static getArtistDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistId"    # Ljava/lang/String;

    .prologue
    .line 200
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "artist_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static getDeviceId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "androidId"    # Ljava/lang/String;

    .prologue
    .line 93
    const-string v0, "androidtv:"

    .line 94
    .local v0, "id":Ljava/lang/String;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 95
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 97
    :cond_0
    return-object v0
.end method

.method public static getPlaylistDetailsIntent(Landroid/content/Context;J)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistId"    # J

    .prologue
    .line 205
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "playlist_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static isDeviceIdPrefixEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 67
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "music_enable_device_id_prefix_for_leanback_environment"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isLeanbackEnvironment(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    .line 53
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-le v0, v1, :cond_0

    const-string v0, "uimode"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    invoke-virtual {v0}, Landroid/app/UiModeManager;->getCurrentModeType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static launchAccountSwitcherIntent(Landroid/app/Activity;)V
    .locals 2
    .param p0, "context"    # Landroid/app/Activity;

    .prologue
    .line 170
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->newAccountSwitcherIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 172
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 174
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 175
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 176
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 177
    return-void
.end method

.method public static launchLeanbackInterfaceIfNeeded(Landroid/app/Activity;)Z
    .locals 2
    .param p0, "hostActivity"    # Landroid/app/Activity;

    .prologue
    .line 110
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->isLeanbackEnvironment(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    const/4 v1, 0x0

    .line 118
    :goto_0
    return v1

    .line 114
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 115
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 116
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 117
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 118
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static newAccountSwitcherIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 166
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackAccountSwitcherActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static newExploreGenresIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "genreId"    # Ljava/lang/String;

    .prologue
    .line 257
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "genre_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static newExploreNewReleasesIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "genreId"    # Ljava/lang/String;

    .prologue
    .line 251
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackExploreNewReleasesActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "genre_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static newExploreRecommendedIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 245
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackExploreRecommendedActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static newExploreTopChartsIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "genreId"    # Ljava/lang/String;

    .prologue
    .line 233
    const/4 v0, -0x1

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/music/leanback/LeanbackUtils;->newExploreTopChartsIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static newExploreTopChartsIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "genreId"    # Ljava/lang/String;
    .param p3, "selectedRow"    # I

    .prologue
    .line 238
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackExploreTopChartsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "genre_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "selected_row"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static newLicensesIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 263
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/LicenseActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static newMyAlbumsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 214
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackMyAlbumsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static newMyArtistsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 210
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static newMyGenresIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 218
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackMyGenresActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static newMyMixesIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 222
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackMyMixesActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static newNowPlayingPlayIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 158
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_now_playing"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static newPlayIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 125
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackPlayActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 138
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;I)Landroid/content/Intent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "playMode"    # I

    .prologue
    .line 142
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;II)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;II)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "playMode"    # I
    .param p3, "offset"    # I

    .prologue
    .line 147
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "song_list"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "play_mode"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "offset"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static newPlayIntent(Landroid/content/Context;Lcom/google/android/music/mix/MixDescriptor;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mixDescriptor"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    .line 133
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "mix_descriptor"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static newPlayIntent(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "document"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 129
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "document"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static newSearchIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 154
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackSearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static newShuffleAllPlayIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 162
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "shuffle_all"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static newSituationIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "situationId"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 267
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackSituationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "title"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "situation_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static newSuggestedMixesIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 227
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackSuggestedMixesActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static showSituations(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 78
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 79
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "music_enable_situations_for_leanback_environment"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method
