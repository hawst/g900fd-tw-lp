.class public Lcom/google/android/music/store/SharedContentProviderHelper;
.super Ljava/lang/Object;
.source "SharedContentProviderHelper.java"


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/SharedContentProviderHelper;->LOGV:Z

    return-void
.end method

.method public static createFollowedSharedPlaylist(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/music/store/Store;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "store"    # Lcom/google/android/music/store/Store;
    .param p3, "listName"    # Ljava/lang/String;
    .param p4, "listDescription"    # Ljava/lang/String;
    .param p5, "listOwnerName"    # Ljava/lang/String;
    .param p6, "shareToken"    # Ljava/lang/String;
    .param p7, "artUrl"    # Ljava/lang/String;
    .param p8, "ownerProfilePhotoUrl"    # Ljava/lang/String;
    .param p9, "editorArtwork"    # Ljava/lang/String;

    .prologue
    .line 107
    move-object/from16 v0, p6

    invoke-static {p0, v0}, Lcom/google/android/music/store/SharedContentProviderHelper;->getSharedSyncablePlaylistEntries(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    .line 109
    .local v11, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    if-nez v11, :cond_0

    .line 110
    const-wide/16 v12, 0x0

    .line 115
    :goto_0
    return-wide v12

    :cond_0
    move-object/from16 v2, p2

    move-object v3, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    .line 112
    invoke-virtual/range {v2 .. v11}, Lcom/google/android/music/store/Store;->createFollowedSharedPlaylist(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)J

    move-result-wide v12

    .line 115
    .local v12, "newId":J
    goto :goto_0
.end method

.method private static getSharedEntries(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shareToken"    # Ljava/lang/String;
    .param p2, "maxNum"    # I
    .param p3, "continuationToken"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 149
    const/4 v8, 0x0

    .line 151
    .local v8, "updateMin":I
    const/4 v7, 0x0

    .line 153
    .local v7, "response":Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;
    :try_start_0
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 154
    .local v0, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    int-to-long v4, v8

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/music/cloudclient/MusicCloud;->getSharedEntries(Ljava/lang/String;ILjava/lang/String;J)Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    move-object v1, v7

    .line 162
    .end local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :goto_0
    return-object v1

    .line 155
    :catch_0
    move-exception v6

    .line 156
    .local v6, "e":Ljava/lang/InterruptedException;
    const-string v1, "SharedContentProviderHelper"

    invoke-virtual {v6}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v9

    .line 157
    goto :goto_0

    .line 158
    .end local v6    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v6

    .line 159
    .local v6, "e":Ljava/io/IOException;
    const-string v1, "SharedContentProviderHelper"

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v9

    .line 160
    goto :goto_0
.end method

.method private static getSharedSyncablePlaylistEntries(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shareToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 173
    const/4 v0, 0x0

    .line 174
    .local v0, "continuationToken":Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentCache;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/NautilusContentCache;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/google/android/music/store/NautilusContentCache;->getSharedPlaylistEntriesResponse(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 176
    .local v1, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    if-eqz v1, :cond_0

    move-object v2, v1

    .line 219
    .end local v1    # "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    .local v2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    :goto_0
    return-object v2

    .line 179
    .end local v2    # "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    .restart local v1    # "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "music_downstream_page_size"

    const/16 v8, 0xfa

    invoke-static {v6, v7, v8}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 181
    .local v3, "maxNum":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "music_max_shared_playlist_size"

    const/16 v8, 0x3e8

    invoke-static {v6, v7, v8}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 185
    .local v4, "maxSize":I
    :cond_1
    invoke-static {p0, p1, v3, v0}, Lcom/google/android/music/store/SharedContentProviderHelper;->getSharedEntries(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;

    move-result-object v5

    .line 187
    .local v5, "response":Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;
    if-eqz v5, :cond_2

    iget-object v6, v5, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;->mEntries:Ljava/util/List;

    if-eqz v6, :cond_2

    iget-object v6, v5, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;->mEntries:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, v5, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;->mEntries:Ljava/util/List;

    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;

    iget-object v6, v6, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;->mPlaylistEntry:Ljava/util/List;

    if-eqz v6, :cond_2

    iget-object v6, v5, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;->mEntries:Ljava/util/List;

    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;

    iget-object v6, v6, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;->mPlaylistEntry:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 197
    :cond_2
    const-string v6, "SharedContentProviderHelper"

    const-string v7, "Failed to get playlist entries response"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :goto_1
    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 216
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentCache;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/NautilusContentCache;

    move-result-object v6

    invoke-virtual {v6, p1, v1}, Lcom/google/android/music/store/NautilusContentCache;->putSharedPlaylistEntriesResponse(Ljava/lang/String;Ljava/util/List;)V

    :cond_3
    move-object v2, v1

    .line 219
    .end local v1    # "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    .restart local v2    # "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    goto :goto_0

    .line 200
    .end local v2    # "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    .restart local v1    # "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    :cond_4
    if-nez v1, :cond_5

    .line 201
    iget-object v6, v5, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;->mEntries:Ljava/util/List;

    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;

    iget-object v1, v6, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;->mPlaylistEntry:Ljava/util/List;

    .line 212
    :goto_2
    iget-object v6, v5, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;->mEntries:Ljava/util/List;

    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;

    iget-object v0, v6, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;->mNextPageToken:Ljava/lang/String;

    .line 213
    if-nez v0, :cond_1

    goto :goto_1

    .line 203
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    iget-object v6, v5, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;->mEntries:Ljava/util/List;

    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;

    iget-object v6, v6, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;->mPlaylistEntry:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/2addr v6, v7

    if-le v6, v4, :cond_6

    .line 204
    const-string v6, "SharedContentProviderHelper"

    const-string v7, "Shared playlists is larger than the max allowed: %s shareToken=%s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    const/4 v9, 0x1

    aput-object p1, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 209
    :cond_6
    iget-object v6, v5, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;->mEntries:Ljava/util/List;

    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;

    iget-object v6, v6, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;->mPlaylistEntry:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public static insert(Landroid/content/Context;Lcom/google/android/music/store/Store;ILandroid/net/Uri;)Landroid/net/Uri;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "store"    # Lcom/google/android/music/store/Store;
    .param p2, "type"    # I
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v7, 0x0

    .line 62
    const-string v8, "addToLibrary"

    invoke-virtual {p3, v8}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 64
    .local v0, "addToLibrary":Z
    const/4 v4, 0x0

    .line 65
    .local v4, "newUri":Landroid/net/Uri;
    const/16 v8, 0x76c

    if-ne p2, v8, :cond_2

    .line 66
    invoke-virtual {p3}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 68
    .local v5, "shareToken":Ljava/lang/String;
    invoke-static {p0, v5}, Lcom/google/android/music/store/SharedContentProviderHelper;->getSharedSyncablePlaylistEntries(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 70
    .local v1, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    if-nez v1, :cond_0

    .line 85
    .end local v1    # "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    .end local v5    # "shareToken":Ljava/lang/String;
    :goto_0
    return-object v7

    .line 73
    .restart local v1    # "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    .restart local v5    # "shareToken":Ljava/lang/String;
    :cond_0
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 74
    .local v6, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    .line 75
    .local v2, "entry":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    iget-object v8, v2, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 77
    .end local v2    # "entry":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    :cond_1
    invoke-static {p0, p1, v6, v0}, Lcom/google/android/music/store/NautilusContentProviderHelper;->insertTracks(Landroid/content/Context;Lcom/google/android/music/store/Store;Ljava/util/List;Z)Landroid/net/Uri;

    move-result-object v4

    .line 81
    .end local v1    # "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "shareToken":Ljava/lang/String;
    .end local v6    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :cond_2
    if-eqz v4, :cond_3

    if-eqz v0, :cond_3

    .line 82
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v9, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 83
    invoke-static {p0}, Lcom/google/android/music/store/RecentItemsManager;->updateRecentItemsAsync(Landroid/content/Context;)V

    :cond_3
    move-object v7, v4

    .line 85
    goto :goto_0
.end method

.method public static query(Landroid/content/Context;Landroid/net/Uri;I[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "matchUri"    # I
    .param p3, "projection"    # [Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 37
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, p3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 39
    .local v0, "cursor":Landroid/database/MatrixCursor;
    const/16 v5, 0x76c

    if-ne p2, v5, :cond_0

    .line 40
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 42
    .local v4, "shareToken":Ljava/lang/String;
    invoke-static {p0, v4}, Lcom/google/android/music/store/SharedContentProviderHelper;->getSharedSyncablePlaylistEntries(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 44
    .local v1, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    if-nez v1, :cond_1

    .line 45
    const/4 v0, 0x0

    .line 58
    .end local v0    # "cursor":Landroid/database/MatrixCursor;
    .end local v1    # "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    .end local v4    # "shareToken":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 47
    .restart local v0    # "cursor":Landroid/database/MatrixCursor;
    .restart local v1    # "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    .restart local v4    # "shareToken":Ljava/lang/String;
    :cond_1
    invoke-static {p3}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 49
    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    .line 54
    .local v2, "entry":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    invoke-static {v2, p3}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static updateFollowedSharedPlaylist(Landroid/content/Context;Landroid/accounts/Account;JLjava/lang/String;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "playlistId"    # J
    .param p4, "shareToken"    # Ljava/lang/String;

    .prologue
    .line 120
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v6

    .line 121
    .local v6, "store":Lcom/google/android/music/store/Store;
    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Lcom/google/android/music/store/Store;->getSharedEntryLastUpdateTimeMs(J)J

    move-result-wide v4

    .line 122
    .local v4, "lastUpateTime":J
    sget-boolean v7, Lcom/google/android/music/store/SharedContentProviderHelper;->LOGV:Z

    if-eqz v7, :cond_0

    .line 123
    const-string v7, "SharedContentProviderHelper"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "lastUpateTime="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 126
    .local v3, "resolver":Landroid/content/ContentResolver;
    const-string v7, "music_shared_playlist_update_delay_sec"

    const-wide/16 v10, 0x384

    invoke-static {v3, v7, v10, v11}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v8

    .line 129
    .local v8, "updateDelaySec":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v4

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    cmp-long v7, v10, v8

    if-gez v7, :cond_2

    .line 130
    sget-boolean v7, Lcom/google/android/music/store/SharedContentProviderHelper;->LOGV:Z

    if-eqz v7, :cond_1

    .line 131
    const-string v7, "SharedContentProviderHelper"

    const-string v10, "Not updating shared playlist as the request came too soon"

    invoke-static {v7, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_1
    :goto_0
    return-void

    .line 137
    :cond_2
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentCache;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/NautilusContentCache;

    move-result-object v7

    move-object/from16 v0, p4

    invoke-virtual {v7, v0}, Lcom/google/android/music/store/NautilusContentCache;->removeSharedPlaylistEntries(Ljava/lang/String;)V

    .line 139
    move-object/from16 v0, p4

    invoke-static {p0, v0}, Lcom/google/android/music/store/SharedContentProviderHelper;->getSharedSyncablePlaylistEntries(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 140
    .local v2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    if-eqz v2, :cond_1

    .line 143
    move-object/from16 v0, p4

    invoke-virtual {v6, p1, v0, v2}, Lcom/google/android/music/store/Store;->updateFollowedSharedPlaylistItems(Landroid/accounts/Account;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method
