.class public Lcom/google/android/music/ui/MaterialMainstageFragment;
.super Lcom/google/android/music/ui/BaseFragment;
.source "MaterialMainstageFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/MaterialMainstageFragment$1;,
        Lcom/google/android/music/ui/MaterialMainstageFragment$MainstageItemDecoration;,
        Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;,
        Lcom/google/android/music/ui/MaterialMainstageFragment$PlayClusterHeaderViewHolder;,
        Lcom/google/android/music/ui/MaterialMainstageFragment$ExploreCardViewHolder;,
        Lcom/google/android/music/ui/MaterialMainstageFragment$RecentCardViewHolder;,
        Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/music/ui/BaseFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field public static final LOGV:Z


# instance fields
.field protected final mCardContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

.field private mClearedFromOnStop:Z

.field private mEmptyView:Landroid/view/View;

.field private mRecyclerAdapter:Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

.field private mRecyclerView:Landroid/support/v7/widget/RecyclerView;

.field private mTileMetadata1x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field private mTileMetadata2x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field private mTopSituationsAdapter:Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/MaterialMainstageFragment;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseFragment;-><init>()V

    .line 63
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;-><init>(Lcom/google/android/music/ui/MusicFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mCardContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    .line 65
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_MATERIAL_MAINSTAGE_1x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mTileMetadata1x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 67
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_MATERIAL_MAINSTAGE_2x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mTileMetadata2x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 739
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/MaterialMainstageFragment;)Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialMainstageFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mTopSituationsAdapter:Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/MaterialMainstageFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialMainstageFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mTileMetadata1x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/MaterialMainstageFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialMainstageFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mTileMetadata2x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/MaterialMainstageFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialMainstageFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/MaterialMainstageFragment;->setEmptyViewVisibility(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/ui/MaterialMainstageFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialMainstageFragment;

    .prologue
    .line 50
    invoke-static {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->initSituationsLoaders(Lcom/google/android/music/ui/MaterialMainstageFragment;)V

    return-void
.end method

.method private static initSituationsLoaders(Lcom/google/android/music/ui/MaterialMainstageFragment;)V
    .locals 3
    .param p0, "fragment"    # Lcom/google/android/music/ui/MaterialMainstageFragment;

    .prologue
    const/4 v2, 0x0

    .line 211
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    .line 212
    .local v0, "lm":Landroid/support/v4/app/LoaderManager;
    const/16 v1, 0x66

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 213
    const/16 v1, 0x67

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 214
    return-void
.end method

.method private setEmptyViewVisibility(Z)V
    .locals 4
    .param p1, "visible"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 182
    iget-object v3, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mEmptyView:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 183
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 184
    return-void

    :cond_0
    move v0, v2

    .line 182
    goto :goto_0

    :cond_1
    move v2, v1

    .line 183
    goto :goto_1
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x1

    .line 90
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 92
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/utils/MusicUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->isXLargeScreen()Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    move v3, v4

    .line 94
    .local v3, "twoColumn":Z
    :goto_0
    if-eqz v3, :cond_3

    const/4 v0, 0x2

    .line 96
    .local v0, "columns":I
    :goto_1
    new-instance v2, Landroid/support/v7/widget/StaggeredGridLayoutManager;

    invoke-direct {v2, v0, v4}, Landroid/support/v7/widget/StaggeredGridLayoutManager;-><init>(II)V

    .line 98
    .local v2, "manager":Landroid/support/v7/widget/StaggeredGridLayoutManager;
    iget-object v5, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v5, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 99
    iget-object v5, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v6, Lcom/google/android/music/ui/MaterialMainstageFragment$MainstageItemDecoration;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v6, v7, v8}, Lcom/google/android/music/ui/MaterialMainstageFragment$MainstageItemDecoration;-><init>(Landroid/content/res/Resources;Lcom/google/android/music/ui/MaterialMainstageFragment$1;)V

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 100
    new-instance v5, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

    invoke-direct {v5, p0, v8}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;-><init>(Lcom/google/android/music/ui/MaterialMainstageFragment;Lcom/google/android/music/ui/MaterialMainstageFragment$1;)V

    iput-object v5, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerAdapter:Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

    .line 101
    iget-object v5, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerAdapter:Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

    invoke-virtual {v5, v4}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setHasStableIds(Z)V

    .line 102
    new-instance v4, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;

    invoke-direct {v4, p0}, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;-><init>(Lcom/google/android/music/ui/MaterialMainstageFragment;)V

    iput-object v4, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mTopSituationsAdapter:Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;

    .line 103
    iget-object v4, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerAdapter:Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 104
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    .line 105
    .local v1, "lm":Landroid/support/v4/app/LoaderManager;
    const/16 v4, 0x64

    invoke-virtual {v1, v4, v8, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 106
    const/16 v4, 0x65

    invoke-virtual {v1, v4, v8, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 107
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isConciergeListenNowEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 108
    invoke-static {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->initSituationsLoaders(Lcom/google/android/music/ui/MaterialMainstageFragment;)V

    .line 110
    :cond_1
    return-void

    .line 92
    .end local v0    # "columns":I
    .end local v1    # "lm":Landroid/support/v4/app/LoaderManager;
    .end local v2    # "manager":Landroid/support/v7/widget/StaggeredGridLayoutManager;
    .end local v3    # "twoColumn":Z
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .restart local v3    # "twoColumn":Z
    :cond_3
    move v0, v4

    .line 94
    goto :goto_1
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 4
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    sget-boolean v0, Lcom/google/android/music/ui/MaterialMainstageFragment;->LOGV:Z

    if-eqz v0, :cond_0

    .line 121
    const-string v0, "MusicMaterialMainstage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreateLoader: id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :cond_0
    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    .line 124
    new-instance v0, Lcom/google/android/music/ui/MediaListCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/medialist/MainstageList;

    invoke-direct {v2}, Lcom/google/android/music/medialist/MainstageList;-><init>()V

    sget-object v3, Lcom/google/android/music/ui/MainstageDocumentHelper;->MAINSTAGE_PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/ui/MediaListCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 132
    :goto_0
    return-object v0

    .line 126
    :cond_1
    const/16 v0, 0x65

    if-ne p1, v0, :cond_2

    .line 127
    new-instance v0, Lcom/google/android/music/ui/MediaListCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/medialist/RecentItemsList;

    invoke-direct {v2}, Lcom/google/android/music/medialist/RecentItemsList;-><init>()V

    sget-object v3, Lcom/google/android/music/ui/RecentCardDocumentHelper;->RECENT_PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/ui/MediaListCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    goto :goto_0

    .line 129
    :cond_2
    const/16 v0, 0x66

    if-ne p1, v0, :cond_3

    .line 130
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/ui/songza/SituationCardHelper;->initHeaderLoader(Landroid/content/Context;)Landroid/support/v4/content/CursorLoader;

    move-result-object v0

    goto :goto_0

    .line 131
    :cond_3
    const/16 v0, 0x67

    if-ne p1, v0, :cond_4

    .line 132
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/ui/songza/SituationCardHelper;->initTopSituationsLoader(Landroid/content/Context;)Landroid/support/v4/content/CursorLoader;

    move-result-object v0

    goto :goto_0

    .line 134
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid loader id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 78
    const v2, 0x7f040068

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 79
    .local v0, "root":Landroid/view/View;
    const v2, 0x7f0e0184

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    iput-object v2, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 80
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getItemAnimator()Landroid/support/v7/widget/RecyclerView$ItemAnimator;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/support/v7/widget/RecyclerView$ItemAnimator;->setSupportsChangeAnimations(Z)V

    .line 81
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v4}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 82
    const v2, 0x7f0e0185

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mEmptyView:Landroid/view/View;

    .line 83
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mEmptyView:Landroid/view/View;

    const v3, 0x7f0e0114

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 84
    .local v1, "text":Landroid/widget/TextView;
    const v2, 0x7f0b02a4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 85
    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 205
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onDestroyView()V

    .line 206
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 208
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 140
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    sget-boolean v0, Lcom/google/android/music/ui/MaterialMainstageFragment;->LOGV:Z

    if-eqz v0, :cond_0

    .line 141
    const-string v0, "MusicMaterialMainstage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoaderFinished: id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_1

    .line 144
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerAdapter:Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->swapMainstageCursor(Landroid/database/Cursor;)V

    .line 154
    :goto_0
    return-void

    .line 145
    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    const/16 v1, 0x65

    if-ne v0, v1, :cond_2

    .line 146
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerAdapter:Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->swapRecentCardCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .line 147
    :cond_2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    const/16 v1, 0x66

    if-ne v0, v1, :cond_3

    .line 148
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerAdapter:Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->swapSituationHeaderCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .line 149
    :cond_3
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    const/16 v1, 0x67

    if-ne v0, v1, :cond_4

    .line 150
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerAdapter:Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->swapTopSituationsCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .line 152
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid loader id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 50
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/MaterialMainstageFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const/4 v3, 0x0

    .line 158
    sget-boolean v0, Lcom/google/android/music/ui/MaterialMainstageFragment;->LOGV:Z

    if-eqz v0, :cond_0

    .line 159
    const-string v0, "MusicMaterialMainstage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoaderReset: loader="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    :cond_0
    if-nez p1, :cond_1

    .line 162
    const-string v0, "MusicMaterialMainstage"

    const-string v1, "We got null for a loader reset, something is clearly wrong here"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    :goto_0
    return-void

    .line 165
    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 167
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerAdapter:Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

    invoke-virtual {v0, v3}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->swapMainstageCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .line 170
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerAdapter:Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

    invoke-virtual {v0, v3}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->swapRecentCardCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .line 173
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerAdapter:Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

    invoke-virtual {v0, v3}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->swapSituationHeaderCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .line 176
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerAdapter:Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

    invoke-virtual {v0, v3}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->swapTopSituationsCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onResume()V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logNewMainstageSession()V

    .line 116
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 188
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onStart()V

    .line 189
    iget-boolean v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mClearedFromOnStop:Z

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerAdapter:Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->notifyDataSetChanged()V

    .line 192
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mClearedFromOnStop:Z

    .line 194
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mClearedFromOnStop:Z

    .line 199
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Lcom/google/android/music/ui/cardlib/utils/Utils;->clearPlayCardThumbnails(Landroid/view/ViewGroup;)V

    .line 200
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onStop()V

    .line 201
    return-void
.end method
