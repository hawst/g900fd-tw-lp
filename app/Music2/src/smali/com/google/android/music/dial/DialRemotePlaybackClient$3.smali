.class Lcom/google/android/music/dial/DialRemotePlaybackClient$3;
.super Ljava/lang/Object;
.source "DialRemotePlaybackClient.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/dial/DialRemotePlaybackClient;->handleExistingSession()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/dial/DialRemotePlaybackClient;


# direct methods
.method constructor <init>(Lcom/google/android/music/dial/DialRemotePlaybackClient;)V
    .locals 0

    .prologue
    .line 392
    iput-object p1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient$3;->this$0:Lcom/google/android/music/dial/DialRemotePlaybackClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 395
    # getter for: Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->access$200()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    const-string v0, "DialRemotePlaybackClient"

    const-string v1, "Getting cloud queue"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient$3;->this$0:Lcom/google/android/music/dial/DialRemotePlaybackClient;

    # getter for: Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->access$300(Lcom/google/android/music/dial/DialRemotePlaybackClient;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/cloudclient/CloudQueueManager;->getCloudQueue(Landroid/content/Context;)V

    .line 399
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient$3;->this$0:Lcom/google/android/music/dial/DialRemotePlaybackClient;

    # getter for: Lcom/google/android/music/dial/DialRemotePlaybackClient;->mItemId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->access$500(Lcom/google/android/music/dial/DialRemotePlaybackClient;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 404
    # getter for: Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->access$200()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    const-string v0, "DialRemotePlaybackClient"

    const-string v1, "Invalid mItemId.  Cannot call onCloudQueueTrackChanged."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient$3;->this$0:Lcom/google/android/music/dial/DialRemotePlaybackClient;

    # invokes: Lcom/google/android/music/dial/DialRemotePlaybackClient;->handleNewSession()V
    invoke-static {v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->access$600(Lcom/google/android/music/dial/DialRemotePlaybackClient;)V

    .line 416
    :goto_0
    return-void

    .line 415
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient$3;->this$0:Lcom/google/android/music/dial/DialRemotePlaybackClient;

    # getter for: Lcom/google/android/music/dial/DialRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->access$400(Lcom/google/android/music/dial/DialRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient$3;->this$0:Lcom/google/android/music/dial/DialRemotePlaybackClient;

    # getter for: Lcom/google/android/music/dial/DialRemotePlaybackClient;->mItemId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->access$500(Lcom/google/android/music/dial/DialRemotePlaybackClient;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->onCloudQueueAdditionalSenderConnected(Ljava/lang/String;)V

    goto :goto_0
.end method
