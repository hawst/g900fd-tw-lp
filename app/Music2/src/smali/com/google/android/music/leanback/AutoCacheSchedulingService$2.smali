.class Lcom/google/android/music/leanback/AutoCacheSchedulingService$2;
.super Ljava/lang/Object;
.source "AutoCacheSchedulingService.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/AutoCacheSchedulingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/AutoCacheSchedulingService;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/AutoCacheSchedulingService;)V
    .locals 0

    .prologue
    .line 856
    iput-object p1, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$2;->this$0:Lcom/google/android/music/leanback/AutoCacheSchedulingService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 859
    iget-object v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$2;->this$0:Lcom/google/android/music/leanback/AutoCacheSchedulingService;

    # invokes: Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->access$800(Lcom/google/android/music/leanback/AutoCacheSchedulingService;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isAutoCachingEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 860
    # getter for: Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 861
    const-string v0, "MusicLeanback"

    const-string v1, "Autocaching is no longer enabled in the preferences"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$2;->this$0:Lcom/google/android/music/leanback/AutoCacheSchedulingService;

    # invokes: Lcom/google/android/music/leanback/AutoCacheSchedulingService;->stopAutoCaching()V
    invoke-static {v0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->access$600(Lcom/google/android/music/leanback/AutoCacheSchedulingService;)V

    .line 865
    :cond_1
    return-void
.end method
