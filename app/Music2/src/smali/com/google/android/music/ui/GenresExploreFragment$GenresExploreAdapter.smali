.class final Lcom/google/android/music/ui/GenresExploreFragment$GenresExploreAdapter;
.super Lcom/google/android/music/ui/MediaListCursorAdapter;
.source "GenresExploreFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/GenresExploreFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "GenresExploreAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/GenresExploreFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/GenresExploreFragment;Lcom/google/android/music/ui/GenresExploreFragment;)V
    .locals 1
    .param p2, "fragment"    # Lcom/google/android/music/ui/GenresExploreFragment;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/music/ui/GenresExploreFragment$GenresExploreAdapter;->this$0:Lcom/google/android/music/ui/GenresExploreFragment;

    .line 107
    const v0, 0x1090003

    invoke-direct {p0, p2, v0}, Lcom/google/android/music/ui/MediaListCursorAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;I)V

    .line 108
    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/ui/GenresExploreFragment;Lcom/google/android/music/ui/GenresExploreFragment;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "fragment"    # Lcom/google/android/music/ui/GenresExploreFragment;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/music/ui/GenresExploreFragment$GenresExploreAdapter;->this$0:Lcom/google/android/music/ui/GenresExploreFragment;

    .line 111
    const v0, 0x1090003

    invoke-direct {p0, p2, v0, p3}, Lcom/google/android/music/ui/MediaListCursorAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ILandroid/database/Cursor;)V

    .line 112
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/GenresExploreFragment;Lcom/google/android/music/ui/GenresExploreFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/GenresExploreFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/GenresExploreFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/GenresExploreFragment;
    .param p3, "x2"    # Landroid/database/Cursor;
    .param p4, "x3"    # Lcom/google/android/music/ui/GenresExploreFragment$1;

    .prologue
    .line 104
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/ui/GenresExploreFragment$GenresExploreAdapter;-><init>(Lcom/google/android/music/ui/GenresExploreFragment;Lcom/google/android/music/ui/GenresExploreFragment;Landroid/database/Cursor;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/GenresExploreFragment;Lcom/google/android/music/ui/GenresExploreFragment;Lcom/google/android/music/ui/GenresExploreFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/GenresExploreFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/GenresExploreFragment;
    .param p3, "x2"    # Lcom/google/android/music/ui/GenresExploreFragment$1;

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/GenresExploreFragment$GenresExploreAdapter;-><init>(Lcom/google/android/music/ui/GenresExploreFragment;Lcom/google/android/music/ui/GenresExploreFragment;)V

    return-void
.end method


# virtual methods
.method protected bindViewToLoadingItem(Landroid/view/View;Landroid/content/Context;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 144
    const v1, 0x1020014

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 145
    .local v0, "tv":Landroid/widget/TextView;
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    return-void
.end method

.method protected bindViewToMediaListItem(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;J)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "itemId"    # J

    .prologue
    .line 128
    const/4 v5, 0x1

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 129
    .local v2, "genreNautilusId":Ljava/lang/String;
    const/4 v5, 0x2

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 130
    .local v1, "genreName":Ljava/lang/String;
    const/4 v5, 0x3

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 132
    .local v4, "subgenreCount":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 133
    .local v0, "extra":Landroid/os/Bundle;
    const-string v5, "nautilusId"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v5, "name"

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string v5, "subgenreCount"

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 136
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 138
    const v5, 0x1020014

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 139
    .local v3, "nameView":Landroid/widget/TextView;
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 116
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/MediaListCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 117
    .local v4, "view":Landroid/view/View;
    iget-object v5, p0, Lcom/google/android/music/ui/GenresExploreFragment$GenresExploreAdapter;->this$0:Lcom/google/android/music/ui/GenresExploreFragment;

    invoke-virtual {v5}, Lcom/google/android/music/ui/GenresExploreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f0138

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 118
    .local v1, "left":I
    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 119
    .local v2, "right":I
    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    .line 120
    .local v3, "top":I
    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    .line 121
    .local v0, "bottom":I
    invoke-virtual {v4, v1, v3, v2, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 122
    return-object v4
.end method
