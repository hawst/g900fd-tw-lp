.class public Lcom/google/android/music/sync/google/MusicAuthInfo;
.super Ljava/lang/Object;
.source "MusicAuthInfo.java"

# interfaces
.implements Lcom/google/android/music/sync/common/AuthInfo;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/music/sync/google/MusicAuthInfo;->mContext:Landroid/content/Context;

    .line 32
    return-void
.end method

.method public static getAuthTokenType(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_auth_token"

    const-string v2, "sj"

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    if-nez p1, :cond_0

    .line 41
    const-string v2, "Given null account to MusicAuthInfo.getAuthToken()"

    .line 42
    .local v2, "errMessage":Ljava/lang/String;
    const-string v5, "MusicAuthInfo"

    const-string v6, "Given null account to MusicAuthInfo.getAuthToken()"

    new-instance v7, Ljava/lang/Throwable;

    invoke-direct {v7}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5, v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 43
    new-instance v5, Landroid/accounts/AuthenticatorException;

    const-string v6, "Given null account to MusicAuthInfo.getAuthToken()"

    invoke-direct {v5, v6}, Landroid/accounts/AuthenticatorException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 46
    .end local v2    # "errMessage":Ljava/lang/String;
    :cond_0
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicAuthInfo;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/music/sync/google/MusicAuthInfo;->getAuthTokenType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 48
    .local v4, "tokenType":Ljava/lang/String;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicAuthInfo;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    .line 49
    .local v3, "manager":Landroid/accounts/AccountManager;
    const/4 v5, 0x1

    invoke-virtual {v3, p1, v4, v5}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "authToken":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 52
    new-instance v5, Landroid/accounts/AuthenticatorException;

    const-string v6, "Received null auth token."

    invoke-direct {v5, v6}, Landroid/accounts/AuthenticatorException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v3    # "manager":Landroid/accounts/AccountManager;
    :catch_0
    move-exception v1

    .line 56
    .local v1, "e":Landroid/accounts/OperationCanceledException;
    new-instance v5, Landroid/accounts/AuthenticatorException;

    invoke-direct {v5, v1}, Landroid/accounts/AuthenticatorException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 54
    .end local v1    # "e":Landroid/accounts/OperationCanceledException;
    .restart local v0    # "authToken":Ljava/lang/String;
    .restart local v3    # "manager":Landroid/accounts/AccountManager;
    :cond_1
    return-object v0
.end method

.method public getAuthTokenForeground(Landroid/app/Activity;Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;
    .locals 8
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p4, "handler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/accounts/Account;",
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;",
            "Landroid/os/Handler;",
            ")",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;
        }
    .end annotation

    .prologue
    .line 70
    .local p3, "callback":Landroid/accounts/AccountManagerCallback;, "Landroid/accounts/AccountManagerCallback<Landroid/os/Bundle;>;"
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicAuthInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "music_auth_token"

    const-string v5, "sj"

    invoke-static {v1, v4, v5}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 73
    .local v2, "tokenType":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicAuthInfo;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 74
    .local v0, "manager":Landroid/accounts/AccountManager;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .local v3, "options":Landroid/os/Bundle;
    move-object v1, p2

    move-object v4, p1

    move-object v5, p3

    move-object v6, p4

    .line 75
    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v7

    .line 77
    .local v7, "authToken":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    if-nez v7, :cond_0

    .line 78
    new-instance v1, Landroid/accounts/AuthenticatorException;

    const-string v4, "Received null auth token."

    invoke-direct {v1, v4}, Landroid/accounts/AuthenticatorException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 80
    :cond_0
    return-object v7
.end method

.method public invalidateAuthToken(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 5
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "authToken"    # Ljava/lang/String;

    .prologue
    .line 61
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicAuthInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "music_auth_token"

    const-string v4, "sj"

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 63
    .local v1, "tokenType":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicAuthInfo;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 64
    .local v0, "manager":Landroid/accounts/AccountManager;
    iget-object v2, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v2, p2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-void
.end method
