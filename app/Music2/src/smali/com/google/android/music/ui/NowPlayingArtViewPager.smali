.class public Lcom/google/android/music/ui/NowPlayingArtViewPager;
.super Lcom/google/android/music/widgets/LinkableViewPager;
.source "NowPlayingArtViewPager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/NowPlayingArtViewPager$1;,
        Lcom/google/android/music/ui/NowPlayingArtViewPager$NowPlayingArtTransformer;
    }
.end annotation


# static fields
.field private static MIN_SCALE:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/high16 v0, 0x3f400000    # 0.75f

    sput v0, Lcom/google/android/music/ui/NowPlayingArtViewPager;->MIN_SCALE:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/widgets/LinkableViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    const/4 v0, 0x1

    new-instance v1, Lcom/google/android/music/ui/NowPlayingArtViewPager$NowPlayingArtTransformer;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/music/ui/NowPlayingArtViewPager$NowPlayingArtTransformer;-><init>(Lcom/google/android/music/ui/NowPlayingArtViewPager;Lcom/google/android/music/ui/NowPlayingArtViewPager$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/NowPlayingArtViewPager;->setPageTransformer(ZLandroid/support/v4/view/ViewPager$PageTransformer;)V

    .line 26
    return-void
.end method

.method static synthetic access$100()F
    .locals 1

    .prologue
    .line 16
    sget v0, Lcom/google/android/music/ui/NowPlayingArtViewPager;->MIN_SCALE:F

    return v0
.end method


# virtual methods
.method protected getChildDrawingOrder(II)I
    .locals 2
    .param p1, "childCount"    # I
    .param p2, "i"    # I

    .prologue
    .line 37
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/google/android/music/widgets/LinkableViewPager;->getChildDrawingOrder(II)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 42
    .end local p2    # "i":I
    :goto_0
    return p2

    .line 38
    .restart local p2    # "i":I
    :catch_0
    move-exception v0

    .line 41
    const-string v0, "NowPlayingArtViewPager"

    const-string v1, "Error in getChildDrawingOrder. Falling back to default impl"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
