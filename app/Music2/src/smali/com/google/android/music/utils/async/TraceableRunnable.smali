.class public Lcom/google/android/music/utils/async/TraceableRunnable;
.super Ljava/lang/Object;
.source "TraceableRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mCalledFrom:Ljava/lang/Throwable;

.field private final mOrigRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ASYNC:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/utils/async/TraceableRunnable;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "origRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/music/utils/async/TraceableRunnable;->mOrigRunnable:Ljava/lang/Runnable;

    .line 23
    sget-boolean v0, Lcom/google/android/music/utils/async/TraceableRunnable;->LOGV:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/google/android/music/utils/async/TraceableRunnable;->mCalledFrom:Ljava/lang/Throwable;

    .line 24
    return-void

    .line 23
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 28
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/utils/async/TraceableRunnable;->mOrigRunnable:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    return-void

    .line 29
    :catch_0
    move-exception v0

    .line 30
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/google/android/music/utils/async/TraceableRunnable;->mCalledFrom:Ljava/lang/Throwable;

    if-eqz v1, :cond_0

    .line 31
    const/4 v1, 0x2

    new-array v1, v1, [[Ljava/lang/StackTraceElement;

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/music/utils/async/TraceableRunnable;->mCalledFrom:Ljava/lang/Throwable;

    invoke-virtual {v3}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/music/utils/ArrayUtils;->combine([[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/StackTraceElement;

    invoke-virtual {v0, v1}, Ljava/lang/Exception;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 33
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
