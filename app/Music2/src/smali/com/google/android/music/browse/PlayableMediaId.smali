.class public Lcom/google/android/music/browse/PlayableMediaId;
.super Ljava/lang/Object;
.source "PlayableMediaId.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/browse/PlayableMediaId$Type;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final TYPE_VALUES:[Lcom/google/android/music/browse/PlayableMediaId$Type;


# instance fields
.field private final mExtData:Ljava/lang/String;

.field private final mExtId:Ljava/lang/String;

.field private final mId:J

.field private final mName:Ljava/lang/String;

.field private final mType:Lcom/google/android/music/browse/PlayableMediaId$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/music/browse/PlayableMediaId;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/browse/PlayableMediaId;->TAG:Ljava/lang/String;

    .line 30
    invoke-static {}, Lcom/google/android/music/browse/PlayableMediaId$Type;->values()[Lcom/google/android/music/browse/PlayableMediaId$Type;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/browse/PlayableMediaId;->TYPE_VALUES:[Lcom/google/android/music/browse/PlayableMediaId$Type;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/browse/PlayableMediaId$Type;J)V
    .locals 2
    .param p1, "type"    # Lcom/google/android/music/browse/PlayableMediaId$Type;
    .param p2, "id"    # J

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    if-nez p1, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/browse/PlayableMediaId;->mType:Lcom/google/android/music/browse/PlayableMediaId$Type;

    .line 71
    iput-wide p2, p0, Lcom/google/android/music/browse/PlayableMediaId;->mId:J

    .line 72
    iput-object v0, p0, Lcom/google/android/music/browse/PlayableMediaId;->mName:Ljava/lang/String;

    .line 73
    iput-object v0, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtId:Ljava/lang/String;

    .line 74
    iput-object v0, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtData:Ljava/lang/String;

    .line 75
    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/browse/PlayableMediaId$Type;JLjava/lang/String;)V
    .locals 2
    .param p1, "type"    # Lcom/google/android/music/browse/PlayableMediaId$Type;
    .param p2, "id"    # J
    .param p4, "name"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    if-nez p1, :cond_0

    .line 79
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/browse/PlayableMediaId;->mType:Lcom/google/android/music/browse/PlayableMediaId$Type;

    .line 82
    iput-wide p2, p0, Lcom/google/android/music/browse/PlayableMediaId;->mId:J

    .line 83
    iput-object p4, p0, Lcom/google/android/music/browse/PlayableMediaId;->mName:Ljava/lang/String;

    .line 84
    iput-object v0, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtId:Ljava/lang/String;

    .line 85
    iput-object v0, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtData:Ljava/lang/String;

    .line 86
    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/browse/PlayableMediaId$Type;JLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Lcom/google/android/music/browse/PlayableMediaId$Type;
    .param p2, "id"    # J
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "extId"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    if-nez p1, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/browse/PlayableMediaId;->mType:Lcom/google/android/music/browse/PlayableMediaId$Type;

    .line 93
    iput-wide p2, p0, Lcom/google/android/music/browse/PlayableMediaId;->mId:J

    .line 94
    iput-object p4, p0, Lcom/google/android/music/browse/PlayableMediaId;->mName:Ljava/lang/String;

    .line 95
    iput-object p5, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtId:Ljava/lang/String;

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtData:Ljava/lang/String;

    .line 97
    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/browse/PlayableMediaId$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Lcom/google/android/music/browse/PlayableMediaId$Type;
    .param p2, "id"    # J
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "extId"    # Ljava/lang/String;
    .param p6, "extData"    # Ljava/lang/String;

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    if-nez p1, :cond_0

    .line 101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/browse/PlayableMediaId;->mType:Lcom/google/android/music/browse/PlayableMediaId$Type;

    .line 104
    iput-wide p2, p0, Lcom/google/android/music/browse/PlayableMediaId;->mId:J

    .line 105
    iput-object p4, p0, Lcom/google/android/music/browse/PlayableMediaId;->mName:Ljava/lang/String;

    .line 106
    iput-object p5, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtId:Ljava/lang/String;

    .line 107
    iput-object p6, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtData:Ljava/lang/String;

    .line 108
    return-void
.end method

.method static synthetic access$000()[Lcom/google/android/music/browse/PlayableMediaId$Type;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/music/browse/PlayableMediaId;->TYPE_VALUES:[Lcom/google/android/music/browse/PlayableMediaId$Type;

    return-object v0
.end method

.method private static decode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "encodedValue"    # Ljava/lang/String;

    .prologue
    .line 272
    const-string v0, "<null>"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 273
    invoke-static {p0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 275
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static encode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 264
    if-nez p0, :cond_0

    .line 265
    const-string v0, "<null>"

    .line 267
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static newAlbumMediaId(JLjava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;
    .locals 2
    .param p0, "albumId"    # J
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 140
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId;

    sget-object v1, Lcom/google/android/music/browse/PlayableMediaId$Type;->ALBUM:Lcom/google/android/music/browse/PlayableMediaId$Type;

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/android/music/browse/PlayableMediaId;-><init>(Lcom/google/android/music/browse/PlayableMediaId$Type;JLjava/lang/String;)V

    return-object v0
.end method

.method public static newAutoPlaylistMediaId(Lcom/google/android/music/browse/PlayableMediaId$Type;J)Lcom/google/android/music/browse/PlayableMediaId;
    .locals 1
    .param p0, "type"    # Lcom/google/android/music/browse/PlayableMediaId$Type;
    .param p1, "playListId"    # J

    .prologue
    .line 127
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/music/browse/PlayableMediaId;-><init>(Lcom/google/android/music/browse/PlayableMediaId$Type;J)V

    return-object v0
.end method

.method public static newLuckyRadioMediaId()Lcom/google/android/music/browse/PlayableMediaId;
    .locals 4

    .prologue
    .line 152
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId;

    sget-object v1, Lcom/google/android/music/browse/PlayableMediaId$Type;->LUCKY_RADIO:Lcom/google/android/music/browse/PlayableMediaId$Type;

    const-wide/16 v2, 0x2a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/browse/PlayableMediaId;-><init>(Lcom/google/android/music/browse/PlayableMediaId$Type;J)V

    return-object v0
.end method

.method public static newNautuilusAlbumMediaId(JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;
    .locals 6
    .param p0, "albumId"    # J
    .param p2, "nautilusAlbumId"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 145
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "nautilusAlbumId is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_0
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId;

    sget-object v1, Lcom/google/android/music/browse/PlayableMediaId$Type;->NAUTILUS_ALBUM:Lcom/google/android/music/browse/PlayableMediaId$Type;

    move-wide v2, p0

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/browse/PlayableMediaId;-><init>(Lcom/google/android/music/browse/PlayableMediaId$Type;JLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newPlaybackQueueItemMediaId(JLjava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;
    .locals 4
    .param p0, "trackId"    # J
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 123
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId;

    sget-object v1, Lcom/google/android/music/browse/PlayableMediaId$Type;->PLAYBACK_QUEUE_ITEM:Lcom/google/android/music/browse/PlayableMediaId$Type;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, p1, v2}, Lcom/google/android/music/browse/PlayableMediaId;-><init>(Lcom/google/android/music/browse/PlayableMediaId$Type;JLjava/lang/String;)V

    return-object v0
.end method

.method public static newPlaylistMediaId(JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;
    .locals 6
    .param p0, "id"    # J
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;

    .prologue
    .line 119
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId;

    sget-object v1, Lcom/google/android/music/browse/PlayableMediaId$Type;->PLAYLIST:Lcom/google/android/music/browse/PlayableMediaId$Type;

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/browse/PlayableMediaId;-><init>(Lcom/google/android/music/browse/PlayableMediaId$Type;JLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newRadioMediaId(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;
    .locals 8
    .param p0, "localRadioId"    # J
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "seedId"    # Ljava/lang/String;
    .param p4, "seedType"    # Ljava/lang/String;

    .prologue
    .line 157
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId;

    sget-object v1, Lcom/google/android/music/browse/PlayableMediaId$Type;->RADIO:Lcom/google/android/music/browse/PlayableMediaId$Type;

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/browse/PlayableMediaId;-><init>(Lcom/google/android/music/browse/PlayableMediaId$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newSharedWithMePlaylistMediaId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;
    .locals 7
    .param p0, "sharedToken"    # Ljava/lang/String;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "artUrl"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sharedToken is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_0
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId;

    sget-object v1, Lcom/google/android/music/browse/PlayableMediaId$Type;->PUBLIC_PLAYLIST:Lcom/google/android/music/browse/PlayableMediaId$Type;

    const-wide/16 v2, 0x2a

    move-object v4, p1

    move-object v5, p0

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/browse/PlayableMediaId;-><init>(Lcom/google/android/music/browse/PlayableMediaId$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static parseFromExportString(Ljava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;
    .locals 11
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 196
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v10, :cond_1

    .line 197
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v8, "String too short"

    invoke-direct {v0, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v9, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v8, ","

    invoke-virtual {v0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 201
    .local v7, "items":[Ljava/lang/String;
    if-eqz v7, :cond_2

    array-length v0, v7

    const/4 v8, 0x5

    if-eq v0, v8, :cond_3

    .line 202
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v8, "Invalid number of items"

    invoke-direct {v0, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205
    :cond_3
    const/4 v0, 0x0

    aget-object v0, v7, v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/music/browse/PlayableMediaId$Type;->fromId(I)Lcom/google/android/music/browse/PlayableMediaId$Type;

    move-result-object v1

    .line 206
    .local v1, "type":Lcom/google/android/music/browse/PlayableMediaId$Type;
    aget-object v0, v7, v9

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 207
    .local v2, "id":J
    aget-object v0, v7, v10

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/browse/PlayableMediaId;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 208
    .local v4, "name":Ljava/lang/String;
    const/4 v0, 0x3

    aget-object v0, v7, v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/browse/PlayableMediaId;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 209
    .local v5, "extId":Ljava/lang/String;
    const/4 v0, 0x4

    aget-object v0, v7, v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/browse/PlayableMediaId;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 210
    .local v6, "extData":Ljava/lang/String;
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/browse/PlayableMediaId;-><init>(Lcom/google/android/music/browse/PlayableMediaId$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 232
    instance-of v3, p1, Lcom/google/android/music/browse/PlayableMediaId;

    if-nez v3, :cond_1

    move v1, v2

    .line 239
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 235
    check-cast v0, Lcom/google/android/music/browse/PlayableMediaId;

    .line 236
    .local v0, "other":Lcom/google/android/music/browse/PlayableMediaId;
    if-eq p0, p1, :cond_0

    .line 239
    iget-object v3, p0, Lcom/google/android/music/browse/PlayableMediaId;->mType:Lcom/google/android/music/browse/PlayableMediaId$Type;

    invoke-virtual {v0}, Lcom/google/android/music/browse/PlayableMediaId;->getType()Lcom/google/android/music/browse/PlayableMediaId$Type;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/music/browse/PlayableMediaId$Type;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v4, p0, Lcom/google/android/music/browse/PlayableMediaId;->mId:J

    invoke-virtual {v0}, Lcom/google/android/music/browse/PlayableMediaId;->getId()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/browse/PlayableMediaId;->mName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/music/browse/PlayableMediaId;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/music/browse/PlayableMediaId;->getExtId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtData:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/music/browse/PlayableMediaId;->getExtData()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public getExtData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtData:Ljava/lang/String;

    return-object v0
.end method

.method public getExtId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtId:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 180
    iget-wide v0, p0, Lcom/google/android/music/browse/PlayableMediaId;->mId:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/music/browse/PlayableMediaId;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/google/android/music/browse/PlayableMediaId$Type;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/music/browse/PlayableMediaId;->mType:Lcom/google/android/music/browse/PlayableMediaId$Type;

    return-object v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 248
    const/4 v0, 0x0

    .line 249
    .local v0, "h":I
    iget-object v1, p0, Lcom/google/android/music/browse/PlayableMediaId;->mType:Lcom/google/android/music/browse/PlayableMediaId$Type;

    invoke-virtual {v1}, Lcom/google/android/music/browse/PlayableMediaId$Type;->ordinal()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    add-int/2addr v0, v1

    .line 250
    int-to-long v2, v0

    mul-int/lit8 v1, v0, 0x1f

    int-to-long v4, v1

    iget-wide v6, p0, Lcom/google/android/music/browse/PlayableMediaId;->mId:J

    add-long/2addr v4, v6

    add-long/2addr v2, v4

    long-to-int v0, v2

    .line 251
    iget-object v1, p0, Lcom/google/android/music/browse/PlayableMediaId;->mName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 252
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/browse/PlayableMediaId;->mName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 254
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 255
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 257
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtData:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 258
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtData:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 260
    :cond_2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 216
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    iget-object v1, p0, Lcom/google/android/music/browse/PlayableMediaId;->mType:Lcom/google/android/music/browse/PlayableMediaId$Type;

    invoke-virtual {v1}, Lcom/google/android/music/browse/PlayableMediaId$Type;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 218
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    iget-wide v2, p0, Lcom/google/android/music/browse/PlayableMediaId;->mId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 220
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    iget-object v1, p0, Lcom/google/android/music/browse/PlayableMediaId;->mName:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/music/browse/PlayableMediaId;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    iget-object v1, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/music/browse/PlayableMediaId;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    iget-object v1, p0, Lcom/google/android/music/browse/PlayableMediaId;->mExtData:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/music/browse/PlayableMediaId;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
