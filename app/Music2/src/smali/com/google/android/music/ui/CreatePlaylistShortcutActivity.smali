.class public Lcom/google/android/music/ui/CreatePlaylistShortcutActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "CreatePlaylistShortcutActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    invoke-virtual {p0}, Lcom/google/android/music/ui/CreatePlaylistShortcutActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 20
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    new-instance v1, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;

    invoke-direct {v1}, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;-><init>()V

    .line 21
    .local v1, "frag":Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;
    const-string v2, "playlist_shortcut_dialog"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/ui/CreatePlaylistShortcutDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 23
    return-void
.end method
