.class Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;
.super Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
.source "AsyncAlbumArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GenreMode"
.end annotation


# instance fields
.field public id:J

.field public parentName:Ljava/lang/String;

.field public uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 786
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;)V
    .locals 2
    .param p1, "other"    # Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;

    .prologue
    .line 788
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    .line 789
    iget-object v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->parentName:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->parentName:Ljava/lang/String;

    .line 790
    iget-wide v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->id:J

    iput-wide v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->id:J

    .line 791
    iget-object v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->uri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->uri:Landroid/net/Uri;

    .line 792
    return-void
.end method


# virtual methods
.method public copy()Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .locals 1

    .prologue
    .line 822
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;

    invoke-direct {v0, p0}, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;)V

    return-object v0
.end method

.method public createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "missingUrlSink"    # Lcom/google/android/music/download/artwork/RemoteUrlSink;

    .prologue
    .line 797
    const/4 v10, 0x0

    .line 798
    .local v10, "childAlbums":Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->uri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->uri:Landroid/net/Uri;

    invoke-static {p1, v0}, Lcom/google/android/music/utils/AlbumArtUtils;->createAlbumIdIteratorFactoryForContentUri(Landroid/content/Context;Landroid/net/Uri;)Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;

    move-result-object v10

    .line 801
    :cond_0
    const/16 v2, 0x8

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->id:J

    iget-object v8, p0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->parentName:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v12, 0x0

    move-object v1, p1

    move v6, p2

    move/from16 v7, p3

    move-object/from16 v11, p4

    invoke-static/range {v1 .. v12}, Lcom/google/android/music/utils/AlbumArtUtils;->getFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 813
    instance-of v1, p1, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;

    if-nez v1, :cond_0

    .line 814
    const/4 v1, 0x0

    .line 817
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 816
    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;

    .line 817
    .local v0, "o":Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;
    iget-object v1, v0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->parentName:Ljava/lang/String;

    iget-wide v2, v0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->id:J

    iget-object v4, v0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->uri:Landroid/net/Uri;

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->equals(Ljava/lang/String;JLandroid/net/Uri;)Z

    move-result v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/String;JLandroid/net/Uri;)Z
    .locals 2
    .param p1, "parentName"    # Ljava/lang/String;
    .param p2, "id"    # J
    .param p4, "uri"    # Landroid/net/Uri;

    .prologue
    .line 826
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->parentName:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->id:J

    cmp-long v0, v0, p2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->uri:Landroid/net/Uri;

    invoke-static {v0, p4}, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->isEqual(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 808
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GenreMode<parentName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->parentName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
