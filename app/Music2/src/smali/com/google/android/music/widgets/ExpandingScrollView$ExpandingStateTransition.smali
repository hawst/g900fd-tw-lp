.class public Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;
.super Ljava/lang/Object;
.source "ExpandingScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/widgets/ExpandingScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExpandingStateTransition"
.end annotation


# instance fields
.field private final scrollableStates:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 927
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;-><init>(Lcom/google/common/collect/ImmutableList;)V

    .line 928
    return-void
.end method

.method protected constructor <init>(Lcom/google/common/collect/ImmutableList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 922
    .local p1, "scrollableStates":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 923
    iput-object p1, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;->scrollableStates:Lcom/google/common/collect/ImmutableList;

    .line 924
    return-void
.end method


# virtual methods
.method public convertToAllowedState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .locals 0
    .param p1, "expandingState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 968
    return-object p1
.end method

.method public getNext(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .locals 1
    .param p1, "oldState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 946
    iget-object v0, p1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->next:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;->convertToAllowedState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v0

    return-object v0
.end method

.method public getPrevious(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .locals 1
    .param p1, "oldState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 956
    iget-object v0, p1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->previous:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    return-object v0
.end method

.method public getScrollableStates()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 936
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;->scrollableStates:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method
