.class final Lcom/google/android/music/medialist/NautilusArtistAlbumList$1;
.super Ljava/lang/Object;
.source "NautilusArtistAlbumList.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/medialist/NautilusArtistAlbumList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/music/medialist/NautilusArtistAlbumList;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/medialist/NautilusArtistAlbumList;
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, "nautilusId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 63
    .local v0, "artistName":Ljava/lang/String;
    new-instance v2, Lcom/google/android/music/medialist/NautilusArtistAlbumList;

    invoke-direct {v2, v1, v0}, Lcom/google/android/music/medialist/NautilusArtistAlbumList;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/NautilusArtistAlbumList$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/medialist/NautilusArtistAlbumList;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/music/medialist/NautilusArtistAlbumList;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 67
    new-array v0, p1, [Lcom/google/android/music/medialist/NautilusArtistAlbumList;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/NautilusArtistAlbumList$1;->newArray(I)[Lcom/google/android/music/medialist/NautilusArtistAlbumList;

    move-result-object v0

    return-object v0
.end method
