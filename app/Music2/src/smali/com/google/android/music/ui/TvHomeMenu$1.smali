.class final Lcom/google/android/music/ui/TvHomeMenu$1;
.super Ljava/lang/Object;
.source "TvHomeMenu.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/TvHomeMenu;->createClickListener(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;)Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Lcom/google/android/music/ui/BaseActivity;

.field final synthetic val$adapter:Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/google/android/music/ui/TvHomeMenu$1;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iput-object p2, p0, Lcom/google/android/music/ui/TvHomeMenu$1;->val$adapter:Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 101
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-static {}, Lcom/google/android/music/ui/HomeMenuScreens;->getMenuScreens()[Lcom/google/android/music/ui/HomeActivity$Screen;

    move-result-object v0

    .line 102
    .local v0, "itemScreens":[Lcom/google/android/music/ui/HomeActivity$Screen;
    aget-object v1, v0, p3

    .line 105
    .local v1, "targetScreen":Lcom/google/android/music/ui/HomeActivity$Screen;
    iget-object v2, p0, Lcom/google/android/music/ui/TvHomeMenu$1;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/HomeActivity$Screen;->isExternalLink(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/TvHomeMenu$1;->val$adapter:Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;

    invoke-virtual {v2, p3}, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->onItemSelected(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 106
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/TvHomeMenu$1;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-static {v2, v1}, Lcom/google/android/music/ui/AppNavigation;->showHomeScreen(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/HomeActivity$Screen;)V

    .line 110
    :goto_0
    return-void

    .line 108
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/TvHomeMenu$1;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v2}, Lcom/google/android/music/ui/BaseActivity;->closeSideDrawer()V

    goto :goto_0
.end method
