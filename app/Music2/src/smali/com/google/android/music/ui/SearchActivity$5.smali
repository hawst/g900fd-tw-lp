.class Lcom/google/android/music/ui/SearchActivity$5;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/SearchActivity;->handleSearchResult(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final cols:[Ljava/lang/String;

.field mPlaylistArtUrl:Ljava/lang/String;

.field mPlaylistName:Ljava/lang/String;

.field mPlaylistType:I

.field final synthetic this$0:Lcom/google/android/music/ui/SearchActivity;

.field final synthetic val$playlistId:J

.field final synthetic val$shouldAutoPlay:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/SearchActivity;JZ)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 487
    iput-object p1, p0, Lcom/google/android/music/ui/SearchActivity$5;->this$0:Lcom/google/android/music/ui/SearchActivity;

    iput-wide p2, p0, Lcom/google/android/music/ui/SearchActivity$5;->val$playlistId:J

    iput-boolean p4, p0, Lcom/google/android/music/ui/SearchActivity$5;->val$shouldAutoPlay:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 489
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "playlist_name"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "playlist_type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "playlist_art_url"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/music/ui/SearchActivity$5;->cols:[Ljava/lang/String;

    .line 495
    iput-object v4, p0, Lcom/google/android/music/ui/SearchActivity$5;->mPlaylistName:Ljava/lang/String;

    .line 496
    iput v3, p0, Lcom/google/android/music/ui/SearchActivity$5;->mPlaylistType:I

    .line 497
    iput-object v4, p0, Lcom/google/android/music/ui/SearchActivity$5;->mPlaylistArtUrl:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 500
    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity$5;->this$0:Lcom/google/android/music/ui/SearchActivity;

    iget-wide v4, p0, Lcom/google/android/music/ui/SearchActivity$5;->val$playlistId:J

    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/SearchActivity$5;->cols:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 504
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 505
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/SearchActivity$5;->mPlaylistName:Ljava/lang/String;

    .line 506
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/SearchActivity$5;->mPlaylistType:I

    .line 507
    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/SearchActivity$5;->mPlaylistArtUrl:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 510
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 512
    return-void

    .line 510
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public taskCompleted()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v6, 0x0

    .line 515
    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity$5;->this$0:Lcom/google/android/music/ui/SearchActivity;

    new-instance v1, Lcom/google/android/music/medialist/PlaylistSongList;

    iget-wide v2, p0, Lcom/google/android/music/ui/SearchActivity$5;->val$playlistId:J

    iget-object v4, p0, Lcom/google/android/music/ui/SearchActivity$5;->mPlaylistName:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/music/ui/SearchActivity$5;->mPlaylistType:I

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    move-object v10, v6

    invoke-direct/range {v1 .. v11}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v0, v1}, Lcom/google/android/music/ui/TrackContainerActivity;->showPlaylist(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V

    .line 518
    iget-boolean v0, p0, Lcom/google/android/music/ui/SearchActivity$5;->val$shouldAutoPlay:Z

    if-eqz v0, :cond_0

    .line 519
    new-instance v1, Lcom/google/android/music/medialist/PlaylistSongList;

    iget-wide v2, p0, Lcom/google/android/music/ui/SearchActivity$5;->val$playlistId:J

    iget-object v4, p0, Lcom/google/android/music/ui/SearchActivity$5;->mPlaylistName:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/music/ui/SearchActivity$5;->mPlaylistType:I

    iget-object v10, p0, Lcom/google/android/music/ui/SearchActivity$5;->mPlaylistArtUrl:Ljava/lang/String;

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    invoke-direct/range {v1 .. v11}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 522
    .local v1, "songlist":Lcom/google/android/music/medialist/SongList;
    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity$5;->this$0:Lcom/google/android/music/ui/SearchActivity;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V

    .line 524
    .end local v1    # "songlist":Lcom/google/android/music/medialist/SongList;
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity$5;->this$0:Lcom/google/android/music/ui/SearchActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/SearchActivity;->finish()V

    .line 525
    return-void
.end method
