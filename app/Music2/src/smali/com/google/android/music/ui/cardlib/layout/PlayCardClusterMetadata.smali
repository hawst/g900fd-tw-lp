.class public Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
.super Ljava/lang/Object;
.source "PlayCardClusterMetadata.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;,
        Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    }
.end annotation


# static fields
.field public static final CARD_CURATED_RADIO:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_LARGE:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MATERIAL_MAINSTAGE_1x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MATERIAL_MAINSTAGE_2x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MEDIUM:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MEDIUM_16x9:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_ROW:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_SMALL_2x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_SMALL_2x1_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_SMALL_WRAPPED_RADIO_MUTANT:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_VIDEO_THUMBNAIL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;


# instance fields
.field private mHeight:I

.field private mTiles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/high16 v9, 0x3f000000    # 0.5f

    const/4 v8, 0x2

    const v7, 0x7f04009b

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    .line 97
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-direct {v0, v7, v5, v5, v9}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_2x1_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 100
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-direct {v0, v7, v5, v5, v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 103
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f04009c

    invoke-direct {v0, v1, v5, v5, v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED_RADIO_MUTANT:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 106
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-direct {v0, v7, v8, v5, v9}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_2x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 109
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const/4 v1, 0x3

    invoke-direct {v0, v7, v8, v1, v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 112
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040096

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v8, v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_MEDIUM:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 115
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040094

    const/4 v2, 0x4

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_LARGE:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 118
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040096

    const/4 v2, 0x4

    const/4 v3, 0x3

    const/high16 v4, 0x3fc00000    # 1.5f

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 121
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f04009a

    invoke-direct {v0, v1, v5, v5, v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_ROW:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 124
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040067

    invoke-direct {v0, v1, v5, v5, v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_MATERIAL_MAINSTAGE_1x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 127
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040067

    invoke-direct {v0, v1, v8, v5, v9}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_MATERIAL_MAINSTAGE_2x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 130
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f04002a

    invoke-direct {v0, v1, v5, v5, v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_CURATED_RADIO:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 133
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const/high16 v1, 0x3f100000    # 0.5625f

    invoke-direct {v0, v7, v5, v5, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_VIDEO_THUMBNAIL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    .line 161
    iput p1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->mWidth:I

    .line 162
    iput p2, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->mHeight:I

    .line 163
    return-void
.end method


# virtual methods
.method public addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
    .locals 2
    .param p1, "cardMetadata"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .param p2, "xStart"    # I
    .param p3, "yStart"    # I

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    new-instance v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;-><init>(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    return-object p0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->mHeight:I

    return v0
.end method

.method public getTileCount()I
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getTileMetadata(I)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    .locals 1
    .param p1, "tileIndex"    # I

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->mWidth:I

    return v0
.end method
