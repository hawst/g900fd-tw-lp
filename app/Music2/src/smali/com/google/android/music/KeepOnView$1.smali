.class Lcom/google/android/music/KeepOnView$1;
.super Ljava/lang/Object;
.source "KeepOnView.java"

# interfaces
.implements Lcom/google/android/music/ui/UIStateManager$NetworkChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/KeepOnView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/KeepOnView;


# direct methods
.method constructor <init>(Lcom/google/android/music/KeepOnView;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/android/music/KeepOnView$1;->this$0:Lcom/google/android/music/KeepOnView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNetworkChanged(ZZ)V
    .locals 4
    .param p1, "mobileConnected"    # Z
    .param p2, "wifiOrEthernetConnected"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 134
    if-eqz p1, :cond_2

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->isOfflineDLOnlyOnWifi()Z

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    .line 136
    .local v0, "pinOnMobile":Z
    :goto_0
    iget-object v3, p0, Lcom/google/android/music/KeepOnView$1;->this$0:Lcom/google/android/music/KeepOnView;

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    # invokes: Lcom/google/android/music/KeepOnView;->setArtPaintColor(Z)V
    invoke-static {v3, v2}, Lcom/google/android/music/KeepOnView;->access$000(Lcom/google/android/music/KeepOnView;Z)V

    .line 137
    return-void

    .end local v0    # "pinOnMobile":Z
    :cond_2
    move v0, v2

    .line 134
    goto :goto_0
.end method
