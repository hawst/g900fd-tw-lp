.class public Lcom/google/android/music/mix/AsyncMixCreatorWorker;
.super Lcom/google/android/music/utils/LoggableHandler;
.source "AsyncMixCreatorWorker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;
    }
.end annotation


# instance fields
.field private final mArtLocation:Ljava/lang/String;

.field private final mCallback:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

.field private final mContext:Landroid/content/Context;

.field private final mIsRecommendation:Z

.field private final mLocalId:J

.field private mMix:Lcom/google/android/music/mix/MixDescriptor;

.field mMixGenerationReceiver:Landroid/content/BroadcastReceiver;

.field private final mMixTypeOrdinal:I

.field private mName:Ljava/lang/String;

.field private mOpenMixRunnable:Ljava/lang/Runnable;

.field private final mRadioLocalId:J

.field private final mRemoteSeedId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "callback"    # Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

    .prologue
    const-wide/16 v4, -0x1

    .line 94
    const-string v1, "AsyncMixCreatorWorker"

    invoke-direct {p0, v1}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    .line 82
    new-instance v1, Lcom/google/android/music/mix/AsyncMixCreatorWorker$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker$1;-><init>(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)V

    iput-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMixGenerationReceiver:Landroid/content/BroadcastReceiver;

    .line 95
    iput-object p1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;

    .line 96
    const-string v1, "localId"

    invoke-virtual {p2, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mLocalId:J

    .line 97
    const-string v1, "remoteId"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mRemoteSeedId:Ljava/lang/String;

    .line 98
    const-string v1, "radioLocalId"

    invoke-virtual {p2, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mRadioLocalId:J

    .line 99
    const-string v1, "name"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mName:Ljava/lang/String;

    .line 100
    const-string v1, "artLocation"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mArtLocation:Ljava/lang/String;

    .line 101
    const-string v1, "mixType"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMixTypeOrdinal:I

    .line 102
    const-string v1, "isRecommendation"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mIsRecommendation:Z

    .line 103
    iput-object p3, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mCallback:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

    .line 105
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 106
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.google.android.music.mix.generationfinished"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 107
    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMixGenerationReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 108
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    .prologue
    .line 30
    iget-wide v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mLocalId:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->createMix()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mCallback:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mRemoteSeedId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/music/mix/AsyncMixCreatorWorker;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/mix/AsyncMixCreatorWorker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Lcom/google/android/music/mix/MixDescriptor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mOpenMixRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method private createMix()V
    .locals 7

    .prologue
    const-wide/16 v2, -0x1

    .line 260
    iget-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mName:Ljava/lang/String;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMixTypeOrdinal:I

    sget-object v1, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v1}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 261
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid mix name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mRemoteSeedId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 265
    new-instance v0, Lcom/google/android/music/mix/MixDescriptor;

    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mRemoteSeedId:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/music/mix/MixDescriptor$Type;->values()[Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v2

    iget v3, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMixTypeOrdinal:I

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mArtLocation:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mIsRecommendation:Z

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/mix/MixDescriptor;-><init>(Ljava/lang/String;Lcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    .line 281
    :goto_0
    new-instance v0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker$3;-><init>(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)V

    iput-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mOpenMixRunnable:Ljava/lang/Runnable;

    .line 305
    new-instance v0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker$4;-><init>(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)V

    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 311
    return-void

    .line 268
    :cond_1
    iget-wide v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mLocalId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 269
    new-instance v1, Lcom/google/android/music/mix/MixDescriptor;

    iget-wide v2, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mLocalId:J

    invoke-static {}, Lcom/google/android/music/mix/MixDescriptor$Type;->values()[Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v0

    iget v4, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMixTypeOrdinal:I

    aget-object v4, v0, v4

    iget-object v5, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mName:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mArtLocation:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/mix/MixDescriptor;-><init>(JLcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    goto :goto_0

    .line 271
    :cond_2
    iget-wide v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mRadioLocalId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 272
    new-instance v1, Lcom/google/android/music/mix/MixDescriptor;

    iget-wide v2, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mRadioLocalId:J

    invoke-static {}, Lcom/google/android/music/mix/MixDescriptor$Type;->values()[Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v0

    iget v4, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMixTypeOrdinal:I

    aget-object v4, v0, v4

    iget-object v5, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mName:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mArtLocation:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/mix/MixDescriptor;-><init>(JLcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    goto :goto_0

    .line 274
    :cond_3
    iget v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMixTypeOrdinal:I

    sget-object v1, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v1}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 275
    iget-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/mix/MixDescriptor;->getLuckyRadio(Landroid/content/Context;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    goto :goto_0

    .line 277
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Missing mix seed information"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private startMixCreation()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 175
    iget-wide v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mLocalId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mRemoteSeedId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mRadioLocalId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMixTypeOrdinal:I

    sget-object v1, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v1}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 177
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid seed id to play the mix."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_0
    iget v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMixTypeOrdinal:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 181
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid mix type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_1
    iget v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMixTypeOrdinal:I

    sget-object v1, Lcom/google/android/music/mix/MixDescriptor$Type;->PLAYLIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v1}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 188
    new-instance v0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker$2;-><init>(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 257
    :goto_0
    return-void

    .line 255
    :cond_2
    invoke-direct {p0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->createMix()V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 116
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 168
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown message type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->what:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 118
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->startMixCreation()V

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 122
    :pswitch_1
    sget-object v3, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 123
    .local v3, "service":Lcom/google/android/music/playback/IMusicPlaybackService;
    if-nez v3, :cond_1

    .line 124
    const-string v4, "MusicMixCreator"

    const-string v5, "Failed to get service instance"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 128
    :cond_1
    const/4 v2, 0x0

    .line 130
    .local v2, "mixState":Lcom/google/android/music/mix/MixGenerationState;
    :try_start_0
    invoke-interface {v3}, Lcom/google/android/music/playback/IMusicPlaybackService;->getMixState()Lcom/google/android/music/mix/MixGenerationState;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 136
    if-nez v2, :cond_2

    .line 137
    const-string v4, "MusicMixCreator"

    const-string v5, "Failed to retrieve mix state"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 131
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "MusicMixCreator"

    const-string v5, "Failed to call the music playback service"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 141
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    iget-object v4, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {v2, v4}, Lcom/google/android/music/mix/MixGenerationState;->isMyMix(Lcom/google/android/music/mix/MixDescriptor;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 142
    const-string v4, "MusicMixCreator"

    const-string v5, "Mix state %s for different mix: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 146
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/music/mix/MixGenerationState;->getState()Lcom/google/android/music/mix/MixGenerationState$State;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/mix/MixGenerationState$State;->FINISHED:Lcom/google/android/music/mix/MixGenerationState$State;

    if-ne v4, v5, :cond_4

    .line 147
    const-string v4, "MusicMixCreator"

    const-string v5, "Mix generation succeeded"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 149
    :cond_4
    invoke-virtual {v2}, Lcom/google/android/music/mix/MixGenerationState;->getState()Lcom/google/android/music/mix/MixGenerationState$State;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/mix/MixGenerationState$State;->FAILED:Lcom/google/android/music/mix/MixGenerationState$State;

    if-ne v4, v5, :cond_0

    .line 150
    const-string v4, "MusicMixCreator"

    const-string v5, "Mix generation failed"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    const/4 v4, 0x4

    invoke-virtual {p0, v4}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 157
    .end local v2    # "mixState":Lcom/google/android/music/mix/MixGenerationState;
    .end local v3    # "service":Lcom/google/android/music/playback/IMusicPlaybackService;
    :pswitch_2
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 158
    iget-object v4, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;

    const v5, 0x7f0b01d6

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 162
    .local v1, "errorMessage":Ljava/lang/String;
    :goto_1
    iget-object v4, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mCallback:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

    invoke-interface {v4, v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;->onFailure(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 160
    .end local v1    # "errorMessage":Ljava/lang/String;
    :cond_5
    iget-object v4, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;

    const v5, 0x7f0b01d7

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "errorMessage":Ljava/lang/String;
    goto :goto_1

    .line 165
    .end local v1    # "errorMessage":Ljava/lang/String;
    :pswitch_3
    iget-object v4, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mCallback:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

    invoke-interface {v4}, Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;->onSuccess()V

    goto/16 :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public quit()V
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMixGenerationReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 316
    iget-object v0, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mOpenMixRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 319
    new-instance v0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$5;

    invoke-direct {v0, p0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker$5;-><init>(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)V

    iget-object v1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 327
    :cond_0
    invoke-super {p0}, Lcom/google/android/music/utils/LoggableHandler;->quit()V

    .line 328
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->sendEmptyMessage(I)Z

    .line 112
    return-void
.end method
