.class Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ManageDownloadsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/ManageDownloadsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/ManageDownloadsFragment;)V
    .locals 0

    .prologue
    .line 585
    iput-object p1, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/ManageDownloadsFragment;Lcom/google/android/music/ui/ManageDownloadsFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/ManageDownloadsFragment$1;

    .prologue
    .line 585
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;-><init>(Lcom/google/android/music/ui/ManageDownloadsFragment;)V

    return-void
.end method

.method private newView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 647
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    invoke-virtual {v3, v5}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 648
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f04006b

    invoke-virtual {v0, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 650
    .local v1, "vg":Landroid/view/ViewGroup;
    new-instance v2, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;

    invoke-direct {v2, v5}, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;-><init>(Lcom/google/android/music/ui/ManageDownloadsFragment$1;)V

    .line 651
    .local v2, "vh":Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;
    const v3, 0x7f0e007c

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v3, v2, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->art:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 652
    const v3, 0x7f0e00d3

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->title:Landroid/widget/TextView;

    .line 653
    const v3, 0x7f0e00d4

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->subtitle:Landroid/widget/TextView;

    .line 654
    const v3, 0x7f0e0178

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->size:Landroid/widget/TextView;

    .line 655
    iget-object v3, v2, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->size:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 656
    const v3, 0x7f0e010e

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/KeepOnView;

    iput-object v3, v2, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->keepOn:Lcom/google/android/music/KeepOnView;

    .line 657
    iget-object v3, v2, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->keepOn:Lcom/google/android/music/KeepOnView;

    invoke-virtual {v3, v4}, Lcom/google/android/music/KeepOnView;->setVisibility(I)V

    .line 658
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 659
    return-object v1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mListBackingData:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$1200(Lcom/google/android/music/ui/ManageDownloadsFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 594
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mListBackingData:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$1200(Lcom/google/android/music/ui/ManageDownloadsFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 585
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->getItem(I)Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 599
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->getItem(I)Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;

    move-result-object v0

    .line 600
    .local v0, "entry":Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;
    iget-wide v2, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->id:J

    return-wide v2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 605
    if-nez p2, :cond_0

    .line 606
    invoke-direct {p0, p3}, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->newView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 609
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;

    .line 610
    .local v1, "vh":Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->getItem(I)Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;

    move-result-object v0

    .line 611
    .local v0, "entry":Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;
    iget-boolean v2, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->isRemoving:Z

    if-eqz v2, :cond_1

    .line 612
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mUnavailableCardOpacity:F
    invoke-static {v2}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$1600(Lcom/google/android/music/ui/ManageDownloadsFragment;)F

    move-result v2

    invoke-static {p2, v2}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 617
    :goto_0
    iget-wide v2, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->id:J

    iput-wide v2, v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->id:J

    .line 618
    iget-object v2, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->containerType:Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;

    iput-object v2, v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->containerType:Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;

    .line 619
    iget-object v2, v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 620
    iget-object v2, v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->subtitle:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 621
    iget-object v2, v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->size:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->size:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 623
    iget-object v2, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->containerType:Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;

    sget-object v3, Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;->ALBUM:Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;

    if-ne v2, v3, :cond_2

    .line 624
    iget-object v2, v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->art:Lcom/google/android/music/AsyncAlbumArtImageView;

    iget-wide v4, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->id:J

    iget-object v3, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->title:Ljava/lang/String;

    iget-object v6, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumId(JLjava/lang/String;Ljava/lang/String;)V

    .line 636
    :goto_1
    iget-object v2, v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->keepOn:Lcom/google/android/music/KeepOnView;

    iget v3, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->downloadedSongCount:I

    int-to-float v3, v3

    iget v4, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->keeponSongCount:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/music/KeepOnView;->setDownloadFraction(F)V

    .line 637
    iget-object v2, v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->keepOn:Lcom/google/android/music/KeepOnView;

    invoke-virtual {v2}, Lcom/google/android/music/KeepOnView;->invalidate()V

    .line 638
    iget-object v2, v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->keepOn:Lcom/google/android/music/KeepOnView;

    iget-object v3, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->songList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v2, v3}, Lcom/google/android/music/KeepOnView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    .line 640
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mOnContainerRowClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v2}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$1700(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 641
    iget-object v2, v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->keepOn:Lcom/google/android/music/KeepOnView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/music/KeepOnView;->setOnClick(Z)V

    .line 643
    return-object p2

    .line 614
    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {p2, v2}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    goto :goto_0

    .line 625
    :cond_2
    iget-object v2, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->containerType:Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;

    sget-object v3, Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;->RADIO_STATION:Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;

    if-ne v2, v3, :cond_4

    .line 626
    iget-object v2, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->artUrls:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 627
    iget-object v2, v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->art:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v2}, Lcom/google/android/music/AsyncAlbumArtImageView;->showDefaultArtwork()V

    goto :goto_1

    .line 630
    :cond_3
    iget-object v2, v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->art:Lcom/google/android/music/AsyncAlbumArtImageView;

    iget-object v3, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->artUrls:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalArtRadio(Ljava/lang/String;)V

    goto :goto_1

    .line 633
    :cond_4
    iget-object v2, v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;->art:Lcom/google/android/music/AsyncAlbumArtImageView;

    iget-wide v4, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->id:J

    iget-object v3, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->title:Ljava/lang/String;

    iget v6, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->listType:I

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/google/android/music/AsyncAlbumArtImageView;->setPlaylistAlbumArt(JLjava/lang/String;I)V

    goto :goto_1
.end method
