.class Lcom/google/android/music/AsyncAlbumArtImageView$2;
.super Ljava/lang/Object;
.source "AsyncAlbumArtImageView.java"

# interfaces
.implements Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/AsyncAlbumArtImageView;


# direct methods
.method constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView;)V
    .locals 0

    .prologue
    .line 1541
    iput-object p1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$2;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onArtChanged(Ljava/lang/String;)V
    .locals 3
    .param p1, "remoteLocation"    # Ljava/lang/String;

    .prologue
    .line 1544
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$2;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    monitor-enter v1

    .line 1545
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$2;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z
    invoke-static {v0, v2}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$402(Lcom/google/android/music/AsyncAlbumArtImageView;Z)Z

    .line 1546
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$2;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I
    invoke-static {v0, v2}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$502(Lcom/google/android/music/AsyncAlbumArtImageView;I)I

    .line 1547
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$2;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I
    invoke-static {v0, v2}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$602(Lcom/google/android/music/AsyncAlbumArtImageView;I)I

    .line 1548
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$2;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    new-instance v2, Lcom/google/android/music/AsyncAlbumArtImageView$2$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$2$1;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$2;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/google/android/music/AsyncAlbumArtImageView;->post(Ljava/lang/Runnable;)Z

    .line 1555
    monitor-exit v1

    .line 1556
    return-void

    .line 1555
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onError(Ljava/lang/String;)V
    .locals 3
    .param p1, "remoteLocation"    # Ljava/lang/String;

    .prologue
    .line 1560
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$2;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    monitor-enter v1

    .line 1561
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$2;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z
    invoke-static {v0, v2}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$402(Lcom/google/android/music/AsyncAlbumArtImageView;Z)Z

    .line 1562
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$2;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I
    invoke-static {v0, v2}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$502(Lcom/google/android/music/AsyncAlbumArtImageView;I)I

    .line 1563
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$2;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I
    invoke-static {v0, v2}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$602(Lcom/google/android/music/AsyncAlbumArtImageView;I)I

    .line 1564
    monitor-exit v1

    .line 1565
    return-void

    .line 1564
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
