.class public Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;
.super Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
.source "PlayCardViewMedium.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 81
    invoke-super/range {p0 .. p5}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->onLayout(ZIIII)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/music/utils/MusicUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 95
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->getPaddingLeft()I

    move-result v0

    .line 96
    .local v0, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->getPaddingTop()I

    move-result v1

    .line 97
    .local v1, "paddingTop":I
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v7}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v2

    .line 98
    .local v2, "thumbnailHeight":I
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v7}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v6

    .line 99
    .local v6, "thumbnailWidth":I
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v7}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 100
    .local v4, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    sub-int v7, v6, v2

    div-int/lit8 v3, v7, 0x2

    .line 101
    .local v3, "thumbnailLeft":I
    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int v5, v1, v7

    .line 102
    .local v5, "thumbnailTop":I
    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    add-int v8, v0, v6

    add-int v9, v5, v2

    invoke-virtual {v7, v3, v5, v8, v9}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->layout(IIII)V

    .line 105
    .end local v0    # "paddingLeft":I
    .end local v1    # "paddingTop":I
    .end local v2    # "thumbnailHeight":I
    .end local v3    # "thumbnailLeft":I
    .end local v4    # "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v5    # "thumbnailTop":I
    .end local v6    # "thumbnailWidth":I
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 17
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 24
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 26
    .local v2, "availableWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->getPaddingLeft()I

    move-result v3

    .line 27
    .local v3, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->getPaddingRight()I

    move-result v4

    .line 29
    .local v4, "paddingRight":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v13}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 31
    .local v8, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    div-int/lit8 v13, v2, 0x2

    sub-int/2addr v13, v3

    sub-int/2addr v13, v4

    iget v14, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v13, v14

    iget v14, v8, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v9, v13, v14

    .line 33
    .local v9, "thumbnailWidth":I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mThumbnailAspectRatio:F

    int-to-float v14, v9

    mul-float/2addr v13, v14

    float-to-int v7, v13

    .line 34
    .local v7, "thumbnailHeight":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v13}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    iput v9, v13, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 35
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v13}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    iput v7, v13, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 36
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v13, v9, v7}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->setThumbnailMetrics(II)V

    .line 38
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v9, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    const/high16 v15, 0x40000000    # 2.0f

    invoke-static {v7, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v13, v14, v15}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->measure(II)V

    .line 41
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 44
    .local v11, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    sub-int v13, v2, v3

    sub-int/2addr v13, v4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v14

    sub-int/2addr v13, v14

    iget v14, v8, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v13, v14

    iget v14, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v13, v14

    iget v14, v11, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v13, v14

    iget v14, v11, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v12, v13, v14

    .line 50
    .local v12, "widthForTitle":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mTitle:Landroid/widget/TextView;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/widget/TextView;->measure(II)V

    .line 51
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v10

    .line 53
    .local v10, "titleLongWidth":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    if-le v10, v12, :cond_1

    const/4 v13, 0x2

    :goto_0
    invoke-virtual {v14, v13}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->setReasonMaxLines(I)V

    .line 55
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    invoke-virtual {v13}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getVisibility()I

    move-result v13

    const/16 v14, 0x8

    if-eq v13, v14, :cond_0

    .line 56
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    invoke-virtual {v13}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 57
    .local v6, "reasonLp":Landroid/view/ViewGroup$MarginLayoutParams;
    sub-int v13, v2, v3

    sub-int/2addr v13, v4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v14

    sub-int/2addr v13, v14

    iget v14, v8, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v13, v14

    iget v14, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v13, v14

    iget v14, v6, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    neg-int v14, v14

    sub-int/2addr v13, v14

    iget v14, v6, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v5, v13, v14

    .line 64
    .local v5, "reason1Width":I
    iget v13, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    if-lez v13, :cond_2

    .line 65
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v5, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    iget v15, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    const/high16 v16, 0x40000000    # 2.0f

    invoke-static/range {v15 .. v16}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v13, v14, v15}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->measure(II)V

    .line 75
    .end local v5    # "reason1Width":I
    .end local v6    # "reasonLp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_0
    :goto_1
    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v7, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-super {v0, v1, v13}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->onMeasure(II)V

    .line 77
    return-void

    .line 53
    :cond_1
    const/4 v13, 0x3

    goto :goto_0

    .line 69
    .restart local v5    # "reason1Width":I
    .restart local v6    # "reasonLp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewMedium;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v5, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->measure(II)V

    goto :goto_1
.end method
