.class Lcom/google/android/music/net/NetworkMonitor$1;
.super Landroid/content/BroadcastReceiver;
.source "NetworkMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/net/NetworkMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/net/NetworkMonitor;


# direct methods
.method constructor <init>(Lcom/google/android/music/net/NetworkMonitor;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/google/android/music/net/NetworkMonitor$1;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 278
    iget-object v6, p0, Lcom/google/android/music/net/NetworkMonitor$1;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mStateRecievedLock:Ljava/lang/Object;
    invoke-static {v6}, Lcom/google/android/music/net/NetworkMonitor;->access$100(Lcom/google/android/music/net/NetworkMonitor;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 279
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 280
    .local v0, "action":Ljava/lang/String;
    const-string v6, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 281
    iget-object v6, p0, Lcom/google/android/music/net/NetworkMonitor$1;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    const-string v8, "connectivity"

    invoke-virtual {v6, v8}, Lcom/google/android/music/net/NetworkMonitor;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 284
    .local v1, "cm":Landroid/net/ConnectivityManager;
    invoke-static {v1, p2}, Landroid/support/v4/net/ConnectivityManagerCompat;->getNetworkInfoFromBroadcast(Landroid/net/ConnectivityManager;Landroid/content/Intent;)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 286
    .local v3, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_3

    .line 287
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    .line 288
    .local v2, "connectivityType":I
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v5

    .line 290
    .local v5, "typeName":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/download/DownloadUtils;->isSupportedNetworkType(I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 291
    const-string v6, "noConnectivity"

    const/4 v8, 0x0

    invoke-virtual {p2, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 295
    .local v4, "noDataAvailable":Z
    # getter for: Lcom/google/android/music/net/NetworkMonitor;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/net/NetworkMonitor;->access$200()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 296
    const-string v6, "NetworkMonitor"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Network changed: type("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") name("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") isConnected: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " noData: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " bitrate:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->sCurrentBitrate:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {}, Lcom/google/android/music/net/NetworkMonitor;->access$300()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    :cond_0
    iget-object v6, p0, Lcom/google/android/music/net/NetworkMonitor$1;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # invokes: Lcom/google/android/music/net/NetworkMonitor;->updateInternalNetworkState(Landroid/net/NetworkInfo;)V
    invoke-static {v6, v3}, Lcom/google/android/music/net/NetworkMonitor;->access$400(Lcom/google/android/music/net/NetworkMonitor;Landroid/net/NetworkInfo;)V

    .line 304
    iget-object v6, p0, Lcom/google/android/music/net/NetworkMonitor$1;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # invokes: Lcom/google/android/music/net/NetworkMonitor;->processConnectivityChange()V
    invoke-static {v6}, Lcom/google/android/music/net/NetworkMonitor;->access$500(Lcom/google/android/music/net/NetworkMonitor;)V

    .line 325
    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    .end local v2    # "connectivityType":I
    .end local v3    # "netInfo":Landroid/net/NetworkInfo;
    .end local v4    # "noDataAvailable":Z
    .end local v5    # "typeName":Ljava/lang/String;
    :cond_1
    :goto_0
    monitor-exit v7

    .line 326
    return-void

    .line 306
    .restart local v1    # "cm":Landroid/net/ConnectivityManager;
    .restart local v2    # "connectivityType":I
    .restart local v3    # "netInfo":Landroid/net/NetworkInfo;
    .restart local v5    # "typeName":Ljava/lang/String;
    :cond_2
    # getter for: Lcom/google/android/music/net/NetworkMonitor;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/net/NetworkMonitor;->access$200()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 307
    const-string v6, "NetworkMonitor"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Ignore following network type: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") - action: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 325
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    .end local v2    # "connectivityType":I
    .end local v3    # "netInfo":Landroid/net/NetworkInfo;
    .end local v5    # "typeName":Ljava/lang/String;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 313
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "cm":Landroid/net/ConnectivityManager;
    .restart local v3    # "netInfo":Landroid/net/NetworkInfo;
    :cond_3
    :try_start_1
    iget-object v6, p0, Lcom/google/android/music/net/NetworkMonitor$1;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    iget-object v8, p0, Lcom/google/android/music/net/NetworkMonitor$1;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    const/4 v9, 0x0

    # setter for: Lcom/google/android/music/net/NetworkMonitor;->mUnmeteredWifiOrEthernetConnected:Z
    invoke-static {v8, v9}, Lcom/google/android/music/net/NetworkMonitor;->access$702(Lcom/google/android/music/net/NetworkMonitor;Z)Z

    move-result v8

    # setter for: Lcom/google/android/music/net/NetworkMonitor;->mMobileOrMeteredConnected:Z
    invoke-static {v6, v8}, Lcom/google/android/music/net/NetworkMonitor;->access$602(Lcom/google/android/music/net/NetworkMonitor;Z)Z

    .line 314
    iget-object v6, p0, Lcom/google/android/music/net/NetworkMonitor$1;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    const/4 v8, -0x1

    # setter for: Lcom/google/android/music/net/NetworkMonitor;->mConnectivityType:I
    invoke-static {v6, v8}, Lcom/google/android/music/net/NetworkMonitor;->access$802(Lcom/google/android/music/net/NetworkMonitor;I)I

    .line 315
    iget-object v6, p0, Lcom/google/android/music/net/NetworkMonitor$1;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    iget-object v8, p0, Lcom/google/android/music/net/NetworkMonitor$1;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v8}, Lcom/google/android/music/net/NetworkMonitor;->access$1000(Lcom/google/android/music/net/NetworkMonitor;)Landroid/telephony/TelephonyManager;

    move-result-object v8

    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v8

    # setter for: Lcom/google/android/music/net/NetworkMonitor;->mConnectedMobileDataType:I
    invoke-static {v6, v8}, Lcom/google/android/music/net/NetworkMonitor;->access$902(Lcom/google/android/music/net/NetworkMonitor;I)I

    .line 316
    # getter for: Lcom/google/android/music/net/NetworkMonitor;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/net/NetworkMonitor;->access$200()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 317
    const-string v6, "NetworkMonitor"

    const-string v8, "Network changed: No Connectivity"

    invoke-static {v6, v8}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    :cond_4
    iget-object v6, p0, Lcom/google/android/music/net/NetworkMonitor$1;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # invokes: Lcom/google/android/music/net/NetworkMonitor;->processConnectivityChange()V
    invoke-static {v6}, Lcom/google/android/music/net/NetworkMonitor;->access$500(Lcom/google/android/music/net/NetworkMonitor;)V

    goto :goto_0

    .line 323
    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    .end local v3    # "netInfo":Landroid/net/NetworkInfo;
    :cond_5
    const-string v6, "NetworkMonitor"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unknown action: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method
