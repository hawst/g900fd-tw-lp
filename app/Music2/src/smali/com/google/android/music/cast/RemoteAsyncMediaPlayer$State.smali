.class public final enum Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;
.super Ljava/lang/Enum;
.source "RemoteAsyncMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum BUFFERING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum CANCELED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum DATA_SOURCE_SET:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum FINISHED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum INVALIDATED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum NONE:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum PAUSED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum PENDING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum PLAYING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum PLAY_REQUEST_SENT:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum RELEASED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum WAITING_FOR_CAST_TOKEN:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum WAITING_FOR_RECEIVER_LOADED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field public static final enum WAITING_FOR_SESSION_ID:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 96
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->NONE:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 97
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "DATA_SOURCE_SET"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->DATA_SOURCE_SET:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 98
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "WAITING_FOR_CAST_TOKEN"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_CAST_TOKEN:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 99
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "WAITING_FOR_SESSION_ID"

    invoke-direct {v0, v1, v6}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_SESSION_ID:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 100
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "WAITING_FOR_RECEIVER_LOADED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_RECEIVER_LOADED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 101
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "PLAY_REQUEST_SENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PLAY_REQUEST_SENT:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 102
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "PENDING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PENDING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 103
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "BUFFERING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->BUFFERING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 104
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "PLAYING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PLAYING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 105
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "PAUSED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PAUSED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 106
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "FINISHED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->FINISHED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 107
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "CANCELED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->CANCELED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 108
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "INVALIDATED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->INVALIDATED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 109
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "RELEASED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->RELEASED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 110
    new-instance v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    const-string v1, "ERROR"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 95
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->NONE:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->DATA_SOURCE_SET:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_CAST_TOKEN:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_SESSION_ID:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_RECEIVER_LOADED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PLAY_REQUEST_SENT:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PENDING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->BUFFERING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PLAYING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PAUSED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->FINISHED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->CANCELED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->INVALIDATED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->RELEASED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->$VALUES:[Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 95
    const-class v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->$VALUES:[Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v0}, [Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    return-object v0
.end method


# virtual methods
.method public canBePlayed()Z
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isWaitingForPlayback()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isError()Z
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isWaitingForPlayback()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PAUSED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaybackRequestAllowed()Z
    .locals 1

    .prologue
    .line 161
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->DATA_SOURCE_SET:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_CAST_TOKEN:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_SESSION_ID:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_RECEIVER_LOADED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 127
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PLAYING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReleased()Z
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->RELEASED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWaitingForPlayback()Z
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->DATA_SOURCE_SET:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_CAST_TOKEN:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_SESSION_ID:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_RECEIVER_LOADED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PLAY_REQUEST_SENT:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PENDING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->BUFFERING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
