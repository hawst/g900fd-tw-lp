.class Lcom/google/android/music/ui/AddToPlaylistFragment$1$1;
.super Ljava/lang/Object;
.source "AddToPlaylistFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/AddToPlaylistFragment$1;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mSongsAddedCount:I

.field final synthetic this$1:Lcom/google/android/music/ui/AddToPlaylistFragment$1;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$item:Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/AddToPlaylistFragment$1;Landroid/content/Context;Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;)V
    .locals 1

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1$1;->this$1:Lcom/google/android/music/ui/AddToPlaylistFragment$1;

    iput-object p2, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1$1;->val$item:Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1$1;->mSongsAddedCount:I

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 4

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1$1;->this$1:Lcom/google/android/music/ui/AddToPlaylistFragment$1;

    iget-object v0, v0, Lcom/google/android/music/ui/AddToPlaylistFragment$1;->this$0:Lcom/google/android/music/ui/AddToPlaylistFragment;

    # getter for: Lcom/google/android/music/ui/AddToPlaylistFragment;->mSongsToAdd:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/ui/AddToPlaylistFragment;->access$000(Lcom/google/android/music/ui/AddToPlaylistFragment;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1$1;->val$item:Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;

    iget-wide v2, v2, Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;->id:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/medialist/SongList;->appendToPlaylist(Landroid/content/Context;J)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1$1;->mSongsAddedCount:I

    .line 83
    return-void
.end method

.method public taskCompleted()V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1$1;->val$context:Landroid/content/Context;

    iget v1, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1$1;->mSongsAddedCount:I

    iget-object v2, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1$1;->val$item:Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;

    iget-object v2, v2, Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;->name:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/MusicUtils;->showSongsAddedToPlaylistToast(Landroid/content/Context;ILjava/lang/String;)V

    .line 90
    return-void
.end method
