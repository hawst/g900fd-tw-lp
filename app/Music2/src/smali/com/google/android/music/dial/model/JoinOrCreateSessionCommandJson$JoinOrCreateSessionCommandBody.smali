.class public Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;
.super Lcom/google/api/client/json/GenericJson;
.source "JoinOrCreateSessionCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "JoinOrCreateSessionCommandBody"
.end annotation


# instance fields
.field public mAppContext:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "appContext"
    .end annotation
.end field

.field public mAppId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "appId"
    .end annotation
.end field

.field public mCustomData:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "customData"
    .end annotation
.end field

.field public mGroupId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "groupId"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
