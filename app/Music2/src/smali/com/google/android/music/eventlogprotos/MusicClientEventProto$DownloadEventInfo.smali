.class public final Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "MusicClientEventProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/eventlogprotos/MusicClientEventProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DownloadEventInfo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;


# instance fields
.field public completeDownloadLatencyMillis:Ljava/lang/Long;

.field public downloadContentType:I

.field public downloadEventType:I

.field public downloadOwner:Ljava/lang/String;

.field public downloadPriority:Ljava/lang/Integer;

.field public downloadSeekMillis:Ljava/lang/Long;

.field public downloadSizeBytes:Ljava/lang/Long;

.field public finalHttpUrl:Ljava/lang/String;

.field public firstByteLatencyMillis:Ljava/lang/Long;

.field public httpRangeHeaderValue:Ljava/lang/String;

.field public httpResponseCode:Ljava/lang/Integer;

.field public initialHttpUrl:Ljava/lang/String;

.field public networkSubtype:I

.field public networkType:I

.field public trackId:Ljava/lang/String;

.field public trackRemoteId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    sput-object v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 10
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 65
    iput v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadEventType:I

    .line 68
    iput v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    .line 85
    iput v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkType:I

    .line 88
    iput v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkSubtype:I

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 155
    const/4 v0, 0x0

    .line 156
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadEventType:I

    if-eq v1, v4, :cond_0

    .line 157
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadEventType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    :cond_0
    iget v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    if-eq v1, v4, :cond_1

    .line 161
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 165
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackRemoteId:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 169
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackRemoteId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadOwner:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 173
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadOwner:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadPriority:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 177
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadPriority:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_5
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSeekMillis:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 181
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSeekMillis:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    :cond_6
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->firstByteLatencyMillis:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 185
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->firstByteLatencyMillis:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    :cond_7
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->completeDownloadLatencyMillis:Ljava/lang/Long;

    if-eqz v1, :cond_8

    .line 189
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->completeDownloadLatencyMillis:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    :cond_8
    iget v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkType:I

    if-eq v1, v4, :cond_9

    .line 193
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    :cond_9
    iget v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkSubtype:I

    if-eq v1, v4, :cond_a

    .line 197
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkSubtype:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    :cond_a
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->initialHttpUrl:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 201
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->initialHttpUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_b
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->finalHttpUrl:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 205
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->finalHttpUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 208
    :cond_c
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->httpRangeHeaderValue:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 209
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->httpRangeHeaderValue:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    :cond_d
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->httpResponseCode:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 213
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->httpResponseCode:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_e
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSizeBytes:Ljava/lang/Long;

    if-eqz v1, :cond_f

    .line 217
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSizeBytes:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    :cond_f
    iput v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->cachedSize:I

    .line 221
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 229
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 230
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 234
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 235
    :sswitch_0
    return-object p0

    .line 240
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 241
    .local v1, "temp":I
    if-eqz v1, :cond_1

    if-eq v1, v5, :cond_1

    if-eq v1, v6, :cond_1

    if-eq v1, v7, :cond_1

    if-eq v1, v8, :cond_1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_2

    .line 251
    :cond_1
    iput v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadEventType:I

    goto :goto_0

    .line 253
    :cond_2
    iput v4, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadEventType:I

    goto :goto_0

    .line 258
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 259
    .restart local v1    # "temp":I
    if-eqz v1, :cond_3

    if-eq v1, v5, :cond_3

    if-ne v1, v6, :cond_4

    .line 262
    :cond_3
    iput v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    goto :goto_0

    .line 264
    :cond_4
    iput v4, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    goto :goto_0

    .line 269
    .end local v1    # "temp":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackId:Ljava/lang/String;

    goto :goto_0

    .line 273
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackRemoteId:Ljava/lang/String;

    goto :goto_0

    .line 277
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadOwner:Ljava/lang/String;

    goto :goto_0

    .line 281
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadPriority:Ljava/lang/Integer;

    goto :goto_0

    .line 285
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSeekMillis:Ljava/lang/Long;

    goto :goto_0

    .line 289
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->firstByteLatencyMillis:Ljava/lang/Long;

    goto :goto_0

    .line 293
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->completeDownloadLatencyMillis:Ljava/lang/Long;

    goto/16 :goto_0

    .line 297
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 298
    .restart local v1    # "temp":I
    if-eqz v1, :cond_5

    if-eq v1, v5, :cond_5

    if-eq v1, v6, :cond_5

    if-eq v1, v7, :cond_5

    if-eq v1, v8, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_5

    const/4 v2, 0x6

    if-eq v1, v2, :cond_5

    const/4 v2, 0x7

    if-eq v1, v2, :cond_5

    const/16 v2, 0x8

    if-eq v1, v2, :cond_5

    const/16 v2, 0x9

    if-eq v1, v2, :cond_5

    const/16 v2, 0xa

    if-ne v1, v2, :cond_6

    .line 309
    :cond_5
    iput v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkType:I

    goto/16 :goto_0

    .line 311
    :cond_6
    iput v4, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkType:I

    goto/16 :goto_0

    .line 316
    .end local v1    # "temp":I
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 317
    .restart local v1    # "temp":I
    if-eqz v1, :cond_7

    if-eq v1, v5, :cond_7

    if-eq v1, v6, :cond_7

    if-eq v1, v7, :cond_7

    if-eq v1, v8, :cond_7

    const/4 v2, 0x5

    if-eq v1, v2, :cond_7

    const/4 v2, 0x6

    if-eq v1, v2, :cond_7

    const/4 v2, 0x7

    if-eq v1, v2, :cond_7

    const/16 v2, 0x8

    if-eq v1, v2, :cond_7

    const/16 v2, 0x9

    if-eq v1, v2, :cond_7

    const/16 v2, 0xa

    if-eq v1, v2, :cond_7

    const/16 v2, 0xb

    if-eq v1, v2, :cond_7

    const/16 v2, 0xc

    if-eq v1, v2, :cond_7

    const/16 v2, 0xd

    if-eq v1, v2, :cond_7

    const/16 v2, 0xe

    if-eq v1, v2, :cond_7

    const/16 v2, 0xf

    if-ne v1, v2, :cond_8

    .line 333
    :cond_7
    iput v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkSubtype:I

    goto/16 :goto_0

    .line 335
    :cond_8
    iput v4, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkSubtype:I

    goto/16 :goto_0

    .line 340
    .end local v1    # "temp":I
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->initialHttpUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 344
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->finalHttpUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 348
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->httpRangeHeaderValue:Ljava/lang/String;

    goto/16 :goto_0

    .line 352
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->httpResponseCode:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 356
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSizeBytes:Ljava/lang/Long;

    goto/16 :goto_0

    .line 230
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/high16 v4, -0x80000000

    .line 103
    iget v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadEventType:I

    if-eq v0, v4, :cond_0

    .line 104
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadEventType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 106
    :cond_0
    iget v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    if-eq v0, v4, :cond_1

    .line 107
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 110
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 112
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackRemoteId:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 113
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackRemoteId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 115
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadOwner:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 116
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadOwner:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 118
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadPriority:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 119
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadPriority:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 121
    :cond_5
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSeekMillis:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 122
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSeekMillis:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 124
    :cond_6
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->firstByteLatencyMillis:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 125
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->firstByteLatencyMillis:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 127
    :cond_7
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->completeDownloadLatencyMillis:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 128
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->completeDownloadLatencyMillis:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 130
    :cond_8
    iget v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkType:I

    if-eq v0, v4, :cond_9

    .line 131
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 133
    :cond_9
    iget v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkSubtype:I

    if-eq v0, v4, :cond_a

    .line 134
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkSubtype:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 136
    :cond_a
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->initialHttpUrl:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 137
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->initialHttpUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 139
    :cond_b
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->finalHttpUrl:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 140
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->finalHttpUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 142
    :cond_c
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->httpRangeHeaderValue:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 143
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->httpRangeHeaderValue:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 145
    :cond_d
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->httpResponseCode:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 146
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->httpResponseCode:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 148
    :cond_e
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSizeBytes:Ljava/lang/Long;

    if-eqz v0, :cond_f

    .line 149
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSizeBytes:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 151
    :cond_f
    return-void
.end method
