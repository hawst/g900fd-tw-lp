.class public final Lcom/google/api/client/http/UrlEncodedParser;
.super Ljava/lang/Object;
.source "UrlEncodedParser.java"


# direct methods
.method public static parse(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 20
    .param p0, "content"    # Ljava/lang/String;
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 105
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    .line 106
    .local v5, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {v5}, Lcom/google/api/client/util/ClassInfo;->of(Ljava/lang/Class;)Lcom/google/api/client/util/ClassInfo;

    move-result-object v4

    .line 107
    .local v4, "classInfo":Lcom/google/api/client/util/ClassInfo;
    const-class v18, Lcom/google/api/client/util/GenericData;

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v18

    if-eqz v18, :cond_2

    move-object/from16 v18, p1

    check-cast v18, Lcom/google/api/client/util/GenericData;

    move-object/from16 v9, v18

    .line 110
    .local v9, "genericData":Lcom/google/api/client/util/GenericData;
    :goto_0
    const-class v18, Ljava/util/Map;

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v18

    if-eqz v18, :cond_3

    move-object/from16 v18, p1

    check-cast v18, Ljava/util/Map;

    move-object/from16 v12, v18

    .line 112
    .local v12, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;"
    :goto_1
    const/4 v7, 0x0

    .line 113
    .local v7, "cur":I
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v10

    .line 114
    .local v10, "length":I
    const/16 v18, 0x3d

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v14

    .line 115
    .local v14, "nextEquals":I
    :goto_2
    if-ge v7, v10, :cond_9

    .line 116
    const/16 v18, 0x26

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1, v7}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 117
    .local v3, "amp":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-ne v3, v0, :cond_0

    .line 118
    move v3, v10

    .line 122
    :cond_0
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v14, v0, :cond_4

    if-ge v14, v3, :cond_4

    .line 123
    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 124
    .local v13, "name":Ljava/lang/String;
    add-int/lit8 v18, v14, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/google/api/client/escape/CharEscapers;->decodeUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 126
    .local v15, "stringValue":Ljava/lang/String;
    const/16 v18, 0x3d

    add-int/lit8 v19, v3, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v14

    .line 131
    :goto_3
    invoke-static {v13}, Lcom/google/api/client/escape/CharEscapers;->decodeUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 133
    invoke-virtual {v4, v13}, Lcom/google/api/client/util/ClassInfo;->getFieldInfo(Ljava/lang/String;)Lcom/google/api/client/util/FieldInfo;

    move-result-object v8

    .line 134
    .local v8, "fieldInfo":Lcom/google/api/client/util/FieldInfo;
    if-eqz v8, :cond_6

    .line 135
    iget-object v0, v8, Lcom/google/api/client/util/FieldInfo;->type:Ljava/lang/Class;

    move-object/from16 v17, v0

    .line 136
    .local v17, "type":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v18, Ljava/util/Collection;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 137
    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Lcom/google/api/client/util/FieldInfo;->getValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Collection;

    .line 139
    .local v6, "collection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Object;>;"
    if-nez v6, :cond_1

    .line 140
    invoke-static/range {v17 .. v17}, Lcom/google/api/client/util/ClassInfo;->newCollectionInstance(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v6

    .line 141
    move-object/from16 v0, p1

    invoke-virtual {v8, v0, v6}, Lcom/google/api/client/util/FieldInfo;->setValue(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 143
    :cond_1
    iget-object v0, v8, Lcom/google/api/client/util/FieldInfo;->field:Ljava/lang/reflect/Field;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/google/api/client/util/ClassInfo;->getCollectionParameter(Ljava/lang/reflect/Field;)Ljava/lang/Class;

    move-result-object v16

    .line 145
    .local v16, "subFieldClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/api/client/util/FieldInfo;->parsePrimitiveValue(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v6, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 163
    .end local v6    # "collection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Object;>;"
    .end local v16    # "subFieldClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v17    # "type":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_4
    add-int/lit8 v7, v3, 0x1

    .line 164
    goto :goto_2

    .line 107
    .end local v3    # "amp":I
    .end local v7    # "cur":I
    .end local v8    # "fieldInfo":Lcom/google/api/client/util/FieldInfo;
    .end local v9    # "genericData":Lcom/google/api/client/util/GenericData;
    .end local v10    # "length":I
    .end local v12    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;"
    .end local v13    # "name":Ljava/lang/String;
    .end local v14    # "nextEquals":I
    .end local v15    # "stringValue":Ljava/lang/String;
    :cond_2
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 110
    .restart local v9    # "genericData":Lcom/google/api/client/util/GenericData;
    :cond_3
    const/4 v12, 0x0

    goto/16 :goto_1

    .line 128
    .restart local v3    # "amp":I
    .restart local v7    # "cur":I
    .restart local v10    # "length":I
    .restart local v12    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;"
    .restart local v14    # "nextEquals":I
    :cond_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 129
    .restart local v13    # "name":Ljava/lang/String;
    const-string v15, ""

    .restart local v15    # "stringValue":Ljava/lang/String;
    goto :goto_3

    .line 148
    .restart local v8    # "fieldInfo":Lcom/google/api/client/util/FieldInfo;
    .restart local v17    # "type":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_5
    move-object/from16 v0, v17

    invoke-static {v0, v15}, Lcom/google/api/client/util/FieldInfo;->parsePrimitiveValue(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Lcom/google/api/client/util/FieldInfo;->setValue(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_4

    .line 152
    .end local v17    # "type":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_6
    invoke-interface {v12, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/ArrayList;

    .line 153
    .local v11, "listValue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v11, :cond_7

    .line 154
    new-instance v11, Ljava/util/ArrayList;

    .end local v11    # "listValue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 155
    .restart local v11    # "listValue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v9, :cond_8

    .line 156
    invoke-virtual {v9, v13, v11}, Lcom/google/api/client/util/GenericData;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 161
    :cond_7
    :goto_5
    invoke-virtual {v11, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 158
    :cond_8
    invoke-interface {v12, v13, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 165
    .end local v3    # "amp":I
    .end local v8    # "fieldInfo":Lcom/google/api/client/util/FieldInfo;
    .end local v11    # "listValue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v13    # "name":Ljava/lang/String;
    .end local v15    # "stringValue":Ljava/lang/String;
    :cond_9
    return-void
.end method
