.class public Landroid/support/v4/view/ViewPagerShim;
.super Landroid/support/v4/view/ViewPager;
.source "ViewPagerShim.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method


# virtual methods
.method protected pageLeft()Z
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->pageLeft()Z

    move-result v0

    return v0
.end method

.method protected pageRight()Z
    .locals 1

    .prologue
    .line 32
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->pageRight()Z

    move-result v0

    return v0
.end method
