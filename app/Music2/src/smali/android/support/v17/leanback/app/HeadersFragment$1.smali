.class Landroid/support/v17/leanback/app/HeadersFragment$1;
.super Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
.source "HeadersFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/HeadersFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/HeadersFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/HeadersFragment;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Landroid/support/v17/leanback/app/HeadersFragment$1;->this$0:Landroid/support/v17/leanback/app/HeadersFragment;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 3
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    const/4 v2, 0x1

    .line 89
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v1

    iget-object v0, v1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    .line 90
    .local v0, "headerView":Landroid/view/View;
    new-instance v1, Landroid/support/v17/leanback/app/HeadersFragment$1$1;

    invoke-direct {v1, p0}, Landroid/support/v17/leanback/app/HeadersFragment$1$1;-><init>(Landroid/support/v17/leanback/app/HeadersFragment$1;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 99
    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 100
    iget-object v1, p0, Landroid/support/v17/leanback/app/HeadersFragment$1;->this$0:Landroid/support/v17/leanback/app/HeadersFragment;

    # getter for: Landroid/support/v17/leanback/app/HeadersFragment;->mWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;
    invoke-static {v1}, Landroid/support/v17/leanback/app/HeadersFragment;->access$100(Landroid/support/v17/leanback/app/HeadersFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p1, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->itemView:Landroid/view/View;

    # getter for: Landroid/support/v17/leanback/app/HeadersFragment;->sLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;
    invoke-static {}, Landroid/support/v17/leanback/app/HeadersFragment;->access$200()Landroid/view/View$OnLayoutChangeListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_0
    # getter for: Landroid/support/v17/leanback/app/HeadersFragment;->sLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;
    invoke-static {}, Landroid/support/v17/leanback/app/HeadersFragment;->access$200()Landroid/view/View$OnLayoutChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    goto :goto_0
.end method
