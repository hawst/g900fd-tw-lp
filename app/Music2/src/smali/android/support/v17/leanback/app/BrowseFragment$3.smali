.class Landroid/support/v17/leanback/app/BrowseFragment$3;
.super Ljava/lang/Object;
.source "BrowseFragment.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnChildFocusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/BrowseFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/BrowseFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/BrowseFragment;)V
    .locals 0

    .prologue
    .line 558
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseFragment$3;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 3
    .param p1, "child"    # Landroid/view/View;
    .param p2, "focused"    # Landroid/view/View;

    .prologue
    .line 579
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$3;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/BrowseFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 589
    :cond_0
    :goto_0
    return-void

    .line 582
    :cond_1
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$3;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1200(Landroid/support/v17/leanback/app/BrowseFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$3;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/BrowseFragment;->isInHeadersTransition()Z

    move-result v1

    if-nez v1, :cond_0

    .line 583
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 584
    .local v0, "childId":I
    sget v1, Landroid/support/v17/leanback/R$id;->browse_container_dock:I

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$3;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseFragment;->access$000(Landroid/support/v17/leanback/app/BrowseFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 585
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$3;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    const/4 v2, 0x0

    # invokes: Landroid/support/v17/leanback/app/BrowseFragment;->startHeadersTransitionInternal(Z)V
    invoke-static {v1, v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$200(Landroid/support/v17/leanback/app/BrowseFragment;Z)V

    goto :goto_0

    .line 586
    :cond_2
    sget v1, Landroid/support/v17/leanback/R$id;->browse_headers_dock:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$3;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseFragment;->access$000(Landroid/support/v17/leanback/app/BrowseFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 587
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$3;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    const/4 v2, 0x1

    # invokes: Landroid/support/v17/leanback/app/BrowseFragment;->startHeadersTransitionInternal(Z)V
    invoke-static {v1, v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$200(Landroid/support/v17/leanback/app/BrowseFragment;Z)V

    goto :goto_0
.end method

.method public onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 2
    .param p1, "direction"    # I
    .param p2, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v0, 0x1

    .line 562
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$3;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/BrowseFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 574
    :cond_0
    :goto_0
    return v0

    .line 566
    :cond_1
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$3;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1200(Landroid/support/v17/leanback/app/BrowseFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$3;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseFragment;->access$000(Landroid/support/v17/leanback/app/BrowseFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 567
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$3;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseFragment;->access$300(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/HeadersFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/HeadersFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 571
    :cond_2
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$3;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1600(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/RowsFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/RowsFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 574
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment$3;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;
    invoke-static {v0}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1300(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/widget/TitleView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v17/leanback/widget/TitleView;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    goto :goto_0
.end method
