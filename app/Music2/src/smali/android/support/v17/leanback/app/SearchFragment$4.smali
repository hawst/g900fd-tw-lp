.class Landroid/support/v17/leanback/app/SearchFragment$4;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/SearchBar$SearchBarListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/app/SearchFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/SearchFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/SearchFragment;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Landroid/support/v17/leanback/app/SearchFragment$4;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKeyboardDismiss(Ljava/lang/String;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 257
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$4;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # invokes: Landroid/support/v17/leanback/app/SearchFragment;->queryComplete()V
    invoke-static {v0}, Landroid/support/v17/leanback/app/SearchFragment;->access$1300(Landroid/support/v17/leanback/app/SearchFragment;)V

    .line 258
    return-void
.end method

.method public onSearchQueryChange(Ljava/lang/String;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 238
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$4;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mProvider:Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;
    invoke-static {v0}, Landroid/support/v17/leanback/app/SearchFragment;->access$700(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$4;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # invokes: Landroid/support/v17/leanback/app/SearchFragment;->retrieveResults(Ljava/lang/String;)V
    invoke-static {v0, p1}, Landroid/support/v17/leanback/app/SearchFragment;->access$1100(Landroid/support/v17/leanback/app/SearchFragment;Ljava/lang/String;)V

    .line 243
    :goto_0
    return-void

    .line 241
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$4;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # setter for: Landroid/support/v17/leanback/app/SearchFragment;->mPendingQuery:Ljava/lang/String;
    invoke-static {v0, p1}, Landroid/support/v17/leanback/app/SearchFragment;->access$1202(Landroid/support/v17/leanback/app/SearchFragment;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public onSearchQuerySubmit(Ljava/lang/String;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 248
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$4;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # invokes: Landroid/support/v17/leanback/app/SearchFragment;->queryComplete()V
    invoke-static {v0}, Landroid/support/v17/leanback/app/SearchFragment;->access$1300(Landroid/support/v17/leanback/app/SearchFragment;)V

    .line 249
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$4;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mProvider:Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;
    invoke-static {v0}, Landroid/support/v17/leanback/app/SearchFragment;->access$700(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$4;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mProvider:Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;
    invoke-static {v0}, Landroid/support/v17/leanback/app/SearchFragment;->access$700(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;->onQueryTextSubmit(Ljava/lang/String;)Z

    .line 252
    :cond_0
    return-void
.end method
