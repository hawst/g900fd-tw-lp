.class Landroid/support/v17/leanback/app/BrowseFragment$8;
.super Landroid/support/v17/leanback/transition/TransitionListener;
.source "BrowseFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/app/BrowseFragment;->createHeadersTransition()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/BrowseFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/BrowseFragment;)V
    .locals 0

    .prologue
    .line 770
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-direct {p0}, Landroid/support/v17/leanback/transition/TransitionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransitionEnd(Ljava/lang/Object;)V
    .locals 4
    .param p1, "transition"    # Ljava/lang/Object;

    .prologue
    .line 776
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    const/4 v3, 0x0

    # setter for: Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransition:Ljava/lang/Object;
    invoke-static {v2, v3}, Landroid/support/v17/leanback/app/BrowseFragment;->access$802(Landroid/support/v17/leanback/app/BrowseFragment;Ljava/lang/Object;)Ljava/lang/Object;

    .line 777
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1600(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/RowsFragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/RowsFragment;->onTransitionEnd()V

    .line 778
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$300(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/HeadersFragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/HeadersFragment;->onTransitionEnd()V

    .line 779
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$000(Landroid/support/v17/leanback/app/BrowseFragment;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 780
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$300(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/HeadersFragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/HeadersFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    .line 781
    .local v0, "headerGridView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->hasFocus()Z

    move-result v2

    if-nez v2, :cond_0

    .line 782
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->requestFocus()Z

    .line 790
    .end local v0    # "headerGridView":Landroid/support/v17/leanback/widget/VerticalGridView;
    :cond_0
    :goto_0
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mBrowseTransitionListener:Landroid/support/v17/leanback/app/BrowseFragment$BrowseTransitionListener;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$500(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/BrowseFragment$BrowseTransitionListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 791
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mBrowseTransitionListener:Landroid/support/v17/leanback/app/BrowseFragment$BrowseTransitionListener;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$500(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/BrowseFragment$BrowseTransitionListener;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v17/leanback/app/BrowseFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z
    invoke-static {v3}, Landroid/support/v17/leanback/app/BrowseFragment;->access$000(Landroid/support/v17/leanback/app/BrowseFragment;)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/app/BrowseFragment$BrowseTransitionListener;->onHeadersTransitionStop(Z)V

    .line 793
    :cond_1
    return-void

    .line 785
    :cond_2
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1600(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/RowsFragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    .line 786
    .local v1, "rowsGridView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->hasFocus()Z

    move-result v2

    if-nez v2, :cond_0

    .line 787
    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->requestFocus()Z

    goto :goto_0
.end method

.method public onTransitionStart(Ljava/lang/Object;)V
    .locals 0
    .param p1, "transition"    # Ljava/lang/Object;

    .prologue
    .line 773
    return-void
.end method
