.class public Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;
.super Landroid/support/v17/leanback/widget/RowPresenter;
.source "DetailsOverviewRowPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;
    }
.end annotation


# instance fields
.field private mActionClickedListener:Landroid/support/v17/leanback/widget/OnActionClickedListener;

.field private final mActionPresenterSelector:Landroid/support/v17/leanback/widget/ActionPresenterSelector;

.field private mBackgroundColor:I

.field private mBackgroundColorSet:Z

.field private final mDetailsPresenter:Landroid/support/v17/leanback/widget/Presenter;

.field private mIsStyleLarge:Z

.field private mSharedElementHelper:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;


# direct methods
.method public constructor <init>(Landroid/support/v17/leanback/widget/Presenter;)V
    .locals 2
    .param p1, "detailsPresenter"    # Landroid/support/v17/leanback/widget/Presenter;

    .prologue
    const/4 v1, 0x0

    .line 276
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/RowPresenter;-><init>()V

    .line 264
    iput v1, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mBackgroundColor:I

    .line 266
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mIsStyleLarge:Z

    .line 277
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->setHeaderPresenter(Landroid/support/v17/leanback/widget/RowHeaderPresenter;)V

    .line 278
    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->setSelectEffectEnabled(Z)V

    .line 279
    iput-object p1, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mDetailsPresenter:Landroid/support/v17/leanback/widget/Presenter;

    .line 280
    new-instance v0, Landroid/support/v17/leanback/widget/ActionPresenterSelector;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/ActionPresenterSelector;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mActionPresenterSelector:Landroid/support/v17/leanback/widget/ActionPresenterSelector;

    .line 281
    return-void
.end method

.method static synthetic access$100(Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;)Landroid/support/v17/leanback/widget/OnActionClickedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;

    .prologue
    .line 60
    iget-object v0, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mActionClickedListener:Landroid/support/v17/leanback/widget/OnActionClickedListener;

    return-object v0
.end method

.method private getCardHeight(Landroid/content/Context;)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 391
    iget-boolean v1, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mIsStyleLarge:Z

    if-eqz v1, :cond_0

    sget v0, Landroid/support/v17/leanback/R$dimen;->lb_details_overview_height_large:I

    .line 393
    .local v0, "resId":I
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    return v1

    .line 391
    .end local v0    # "resId":I
    :cond_0
    sget v0, Landroid/support/v17/leanback/R$dimen;->lb_details_overview_height_small:I

    goto :goto_0
.end method

.method private getDefaultBackgroundColor(Landroid/content/Context;)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 367
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 368
    .local v0, "outValue":Landroid/util/TypedValue;
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Landroid/support/v17/leanback/R$attr;->defaultBrandColor:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 369
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    return v1
.end method

.method private static getNonNegativeHeight(Landroid/graphics/drawable/Drawable;)I
    .locals 2
    .param p0, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v1, 0x0

    .line 413
    if-nez p0, :cond_0

    move v0, v1

    .line 414
    .local v0, "height":I
    :goto_0
    if-lez v0, :cond_1

    .end local v0    # "height":I
    :goto_1
    return v0

    .line 413
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    goto :goto_0

    .restart local v0    # "height":I
    :cond_1
    move v0, v1

    .line 414
    goto :goto_1
.end method

.method private static getNonNegativeWidth(Landroid/graphics/drawable/Drawable;)I
    .locals 2
    .param p0, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v1, 0x0

    .line 408
    if-nez p0, :cond_0

    move v0, v1

    .line 409
    .local v0, "width":I
    :goto_0
    if-lez v0, :cond_1

    .end local v0    # "width":I
    :goto_1
    return v0

    .line 408
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    goto :goto_0

    .restart local v0    # "width":I
    :cond_1
    move v0, v1

    .line 409
    goto :goto_1
.end method

.method private initDetailsOverview(Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;)V
    .locals 4
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;

    .prologue
    .line 397
    iget-object v1, p1, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mOverviewFrame:Landroid/widget/FrameLayout;

    .line 398
    .local v1, "overview":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 399
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->getCardHeight(Landroid/content/Context;)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 400
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 402
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->getSelectEffectEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 403
    iget-object v2, p1, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mOverviewFrame:Landroid/widget/FrameLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 405
    :cond_0
    return-void
.end method


# virtual methods
.method protected createRowViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 381
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Landroid/support/v17/leanback/R$layout;->lb_details_overview:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 383
    .local v0, "v":Landroid/view/View;
    new-instance v1, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;

    iget-object v2, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mDetailsPresenter:Landroid/support/v17/leanback/widget/Presenter;

    invoke-direct {v1, p0, v0, v2}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;-><init>(Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;Landroid/view/View;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 385
    .local v1, "vh":Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;
    invoke-direct {p0, v1}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->initDetailsOverview(Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;)V

    .line 387
    return-object v1
.end method

.method public final isUsingDefaultSelectEffect()Z
    .locals 1

    .prologue
    .line 521
    const/4 v0, 0x0

    return v0
.end method

.method protected onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V
    .locals 17
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 419
    invoke-super/range {p0 .. p2}, Landroid/support/v17/leanback/widget/RowPresenter;->onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V

    move-object/from16 v9, p2

    .line 421
    check-cast v9, Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    .local v9, "row":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    move-object/from16 v13, p1

    .line 422
    check-cast v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;

    .line 424
    .local v13, "vh":Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v14}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 426
    .local v8, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v14}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->getCardHeight(Landroid/content/Context;)I

    move-result v3

    .line 427
    .local v3, "cardHeight":I
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v14}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Landroid/support/v17/leanback/R$dimen;->lb_details_overview_image_margin_vertical:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 429
    .local v12, "verticalMargin":I
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v14}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Landroid/support/v17/leanback/R$dimen;->lb_details_overview_image_margin_horizontal:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 431
    .local v6, "horizontalMargin":I
    invoke-virtual {v9}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->getImageDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v14

    invoke-static {v14}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->getNonNegativeWidth(Landroid/graphics/drawable/Drawable;)I

    move-result v5

    .line 432
    .local v5, "drawableWidth":I
    invoke-virtual {v9}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->getImageDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v14

    invoke-static {v14}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->getNonNegativeHeight(Landroid/graphics/drawable/Drawable;)I

    move-result v4

    .line 434
    .local v4, "drawableHeight":I
    invoke-virtual {v9}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->isImageScaleUpAllowed()Z

    move-result v10

    .line 435
    .local v10, "scaleImage":Z
    const/4 v11, 0x0

    .line 437
    .local v11, "useMargin":Z
    invoke-virtual {v9}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->getImageDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v14

    if-eqz v14, :cond_5

    .line 438
    const/4 v7, 0x0

    .line 441
    .local v7, "landscape":Z
    if-le v5, v4, :cond_0

    .line 442
    const/4 v7, 0x1

    .line 443
    move-object/from16 v0, p0

    iget-boolean v14, v0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mIsStyleLarge:Z

    if-eqz v14, :cond_0

    .line 444
    const/4 v11, 0x1

    .line 448
    :cond_0
    if-eqz v7, :cond_1

    if-gt v5, v3, :cond_2

    :cond_1
    if-nez v7, :cond_3

    if-le v4, v3, :cond_3

    .line 450
    :cond_2
    const/4 v10, 0x1

    .line 453
    :cond_3
    if-nez v10, :cond_4

    .line 454
    const/4 v11, 0x1

    .line 457
    :cond_4
    if-eqz v11, :cond_5

    if-nez v10, :cond_5

    .line 458
    if-eqz v7, :cond_7

    sub-int v14, v3, v6

    if-le v5, v14, :cond_7

    .line 459
    const/4 v10, 0x1

    .line 466
    .end local v7    # "landscape":Z
    :cond_5
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v14, v0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mBackgroundColorSet:Z

    if-eqz v14, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mBackgroundColor:I

    .line 469
    .local v2, "bgColor":I
    :goto_1
    if-eqz v11, :cond_9

    .line 470
    iput v6, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 471
    iput v12, v8, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iput v12, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 472
    invoke-static {}, Landroid/support/v17/leanback/widget/RoundedRectHelper;->getInstance()Landroid/support/v17/leanback/widget/RoundedRectHelper;

    move-result-object v14

    iget-object v15, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mOverviewFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v14, v15, v2}, Landroid/support/v17/leanback/widget/RoundedRectHelper;->setRoundedRectBackground(Landroid/view/View;I)V

    .line 473
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mRightPanel:Landroid/view/ViewGroup;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 474
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 482
    :goto_2
    if-eqz v10, :cond_a

    .line 483
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    sget-object v15, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 484
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 485
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v14, v3}, Landroid/widget/ImageView;->setMaxWidth(I)V

    .line 486
    const/4 v14, -0x1

    iput v14, v8, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 487
    const/4 v14, -0x2

    iput v14, v8, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 495
    :goto_3
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v14, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 496
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->getImageDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 498
    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mDetailsPresenter:Landroid/support/v17/leanback/widget/Presenter;

    iget-object v15, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mDetailsDescriptionViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-virtual {v9}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->getItem()Ljava/lang/Object;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Landroid/support/v17/leanback/widget/Presenter;->onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V

    .line 500
    new-instance v1, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mActionPresenterSelector:Landroid/support/v17/leanback/widget/ActionPresenterSelector;

    invoke-direct {v1, v14}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 501
    .local v1, "aoa":Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    const/4 v14, 0x0

    invoke-virtual {v9}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->getActions()Ljava/util/List;

    move-result-object v15

    invoke-virtual {v1, v14, v15}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->addAll(ILjava/util/Collection;)V

    .line 502
    invoke-virtual {v13, v1}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->bind(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 504
    invoke-virtual {v9}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->getImageDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v14

    if-eqz v14, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mSharedElementHelper:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;

    if-eqz v14, :cond_6

    .line 505
    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mSharedElementHelper:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;

    invoke-virtual {v14, v13}, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->onBindToDrawable(Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;)V

    .line 507
    :cond_6
    return-void

    .line 460
    .end local v1    # "aoa":Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    .end local v2    # "bgColor":I
    .restart local v7    # "landscape":Z
    :cond_7
    if-nez v7, :cond_5

    mul-int/lit8 v14, v12, 0x2

    sub-int v14, v3, v14

    if-le v4, v14, :cond_5

    .line 461
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 466
    .end local v7    # "landscape":Z
    :cond_8
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mOverviewView:Landroid/view/ViewGroup;

    invoke-virtual {v14}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->getDefaultBackgroundColor(Landroid/content/Context;)I

    move-result v2

    goto/16 :goto_1

    .line 476
    .restart local v2    # "bgColor":I
    :cond_9
    const/4 v14, 0x0

    iput v14, v8, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iput v14, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v14, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 477
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mRightPanel:Landroid/view/ViewGroup;

    invoke-virtual {v14, v2}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 478
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v14, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 479
    invoke-static {}, Landroid/support/v17/leanback/widget/RoundedRectHelper;->getInstance()Landroid/support/v17/leanback/widget/RoundedRectHelper;

    move-result-object v14

    iget-object v15, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mOverviewFrame:Landroid/widget/FrameLayout;

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/support/v17/leanback/widget/RoundedRectHelper;->setRoundedRectBackground(Landroid/view/View;I)V

    goto/16 :goto_2

    .line 489
    :cond_a
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    sget-object v15, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 490
    iget-object v14, v13, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mImageView:Landroid/widget/ImageView;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 491
    const/4 v14, -0x2

    iput v14, v8, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 493
    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v14

    iput v14, v8, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    goto/16 :goto_3
.end method

.method protected onRowViewSelected(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Z)V
    .locals 1
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p2, "selected"    # Z

    .prologue
    .line 373
    invoke-super {p0, p1, p2}, Landroid/support/v17/leanback/widget/RowPresenter;->onRowViewSelected(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Z)V

    .line 374
    if-eqz p2, :cond_0

    .line 375
    check-cast p1, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;

    .end local p1    # "vh":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->dispatchItemSelection(Landroid/view/View;)V

    .line 377
    :cond_0
    return-void
.end method

.method protected onSelectLevelChanged(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    .prologue
    .line 526
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->onSelectLevelChanged(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    .line 527
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->getSelectEffectEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 528
    check-cast v1, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;

    .line 529
    .local v1, "vh":Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;
    iget-object v2, v1, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mColorDimmer:Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;

    invoke-virtual {v2}, Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    .line 530
    .local v0, "dimmedColor":I
    iget-object v2, v1, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mOverviewFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 532
    .end local v0    # "dimmedColor":I
    .end local v1    # "vh":Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;
    :cond_0
    return-void
.end method

.method protected onUnbindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    .prologue
    .line 511
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->onUnbindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    move-object v0, p1

    .line 513
    check-cast v0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;

    .line 514
    .local v0, "vh":Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;
    iget-object v1, v0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mDetailsDescriptionViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    if-eqz v1, :cond_0

    .line 515
    iget-object v1, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mDetailsPresenter:Landroid/support/v17/leanback/widget/Presenter;

    iget-object v2, v0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mDetailsDescriptionViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/Presenter;->onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    .line 517
    :cond_0
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 301
    iput p1, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mBackgroundColor:I

    .line 302
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mBackgroundColorSet:Z

    .line 303
    return-void
.end method

.method public setOnActionClickedListener(Landroid/support/v17/leanback/widget/OnActionClickedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnActionClickedListener;

    .prologue
    .line 287
    iput-object p1, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mActionClickedListener:Landroid/support/v17/leanback/widget/OnActionClickedListener;

    .line 288
    return-void
.end method

.method public final setSharedElementEnterTransition(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "sharedElementName"    # Ljava/lang/String;

    .prologue
    .line 363
    const-wide/16 v0, 0x1388

    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->setSharedElementEnterTransition(Landroid/app/Activity;Ljava/lang/String;J)V

    .line 364
    return-void
.end method

.method public final setSharedElementEnterTransition(Landroid/app/Activity;Ljava/lang/String;J)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "sharedElementName"    # Ljava/lang/String;
    .param p3, "timeoutMs"    # J

    .prologue
    .line 342
    iget-object v0, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mSharedElementHelper:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;

    if-nez v0, :cond_0

    .line 343
    new-instance v0, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mSharedElementHelper:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;

    .line 345
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mSharedElementHelper:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->setSharedElementEnterTransition(Landroid/app/Activity;Ljava/lang/String;J)V

    .line 347
    return-void
.end method

.method public setStyleLarge(Z)V
    .locals 0
    .param p1, "large"    # Z

    .prologue
    .line 318
    iput-boolean p1, p0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->mIsStyleLarge:Z

    .line 319
    return-void
.end method
