.class Landroid/support/v17/leanback/app/PlaybackOverlayFragment$2;
.super Landroid/os/Handler;
.source "PlaybackOverlayFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/PlaybackOverlayFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$2;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 147
    iget v0, p1, Landroid/os/Message;->what:I

    # getter for: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->START_FADE_OUT:I
    invoke-static {}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$600()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$2;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # getter for: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingEnabled:Z
    invoke-static {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$700(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$2;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    const/4 v1, 0x0

    # invokes: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->fade(Z)V
    invoke-static {v0, v1}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$800(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;Z)V

    .line 150
    :cond_0
    return-void
.end method
