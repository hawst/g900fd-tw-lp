.class public Landroid/support/v17/leanback/widget/WindowAlignment$Axis;
.super Ljava/lang/Object;
.source "WindowAlignment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/WindowAlignment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Axis"
.end annotation


# instance fields
.field private mMaxEdge:I

.field private mMaxScroll:I

.field private mMinEdge:I

.field private mMinScroll:I

.field private mName:Ljava/lang/String;

.field private mPaddingHigh:I

.field private mPaddingLow:I

.field private mScrollCenter:F

.field private mSize:I

.field private mWindowAlignment:I

.field private mWindowAlignmentOffset:I

.field private mWindowAlignmentOffsetPercent:F


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x3

    iput v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mWindowAlignment:I

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mWindowAlignmentOffset:I

    .line 60
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mWindowAlignmentOffsetPercent:F

    .line 71
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->reset()V

    .line 72
    iput-object p1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mName:Ljava/lang/String;

    .line 73
    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/widget/WindowAlignment$Axis;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->reset()V

    return-void
.end method

.method private reset()V
    .locals 1

    .prologue
    .line 159
    const/high16 v0, -0x31000000

    iput v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mScrollCenter:F

    .line 160
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMinEdge:I

    .line 161
    const v0, 0x7fffffff

    iput v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMaxEdge:I

    .line 162
    return-void
.end method


# virtual methods
.method public final getClientSize()I
    .locals 2

    .prologue
    .line 194
    iget v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mSize:I

    iget v1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mPaddingLow:I

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mPaddingHigh:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final getMaxEdge()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMaxEdge:I

    return v0
.end method

.method public final getMaxScroll()I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMaxScroll:I

    return v0
.end method

.method public final getMinEdge()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMinEdge:I

    return v0
.end method

.method public final getMinScroll()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMinScroll:I

    return v0
.end method

.method public final getPaddingLow()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mPaddingLow:I

    return v0
.end method

.method public final getSize()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mSize:I

    return v0
.end method

.method public final getSystemScrollPos(IZZ)I
    .locals 7
    .param p1, "scrollCenter"    # I
    .param p2, "isFirst"    # Z
    .param p3, "isLast"    # Z

    .prologue
    .line 203
    iget v5, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mWindowAlignmentOffset:I

    if-ltz v5, :cond_1

    .line 204
    iget v5, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mWindowAlignmentOffset:I

    iget v6, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mPaddingLow:I

    sub-int v4, v5, v6

    .line 208
    .local v4, "middlePosition":I
    :goto_0
    iget v5, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mWindowAlignmentOffsetPercent:F

    const/high16 v6, -0x40800000    # -1.0f

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_0

    .line 209
    iget v5, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mSize:I

    int-to-float v5, v5

    iget v6, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mWindowAlignmentOffsetPercent:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr v5, v6

    float-to-int v5, v5

    add-int/2addr v4, v5

    .line 211
    :cond_0
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getClientSize()I

    move-result v1

    .line 212
    .local v1, "clientSize":I
    sub-int v0, v1, v4

    .line 213
    .local v0, "afterMiddlePosition":I
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->isMinUnknown()Z

    move-result v3

    .line 214
    .local v3, "isMinUnknown":Z
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->isMaxUnknown()Z

    move-result v2

    .line 215
    .local v2, "isMaxUnknown":Z
    if-nez v3, :cond_2

    if-nez v2, :cond_2

    iget v5, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mWindowAlignment:I

    and-int/lit8 v5, v5, 0x3

    const/4 v6, 0x3

    if-ne v5, v6, :cond_2

    .line 217
    iget v5, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMaxEdge:I

    iget v6, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMinEdge:I

    sub-int/2addr v5, v6

    if-gt v5, v1, :cond_2

    .line 220
    iget v5, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMinEdge:I

    iget v6, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mPaddingLow:I

    sub-int/2addr v5, v6

    .line 240
    :goto_1
    return v5

    .line 206
    .end local v0    # "afterMiddlePosition":I
    .end local v1    # "clientSize":I
    .end local v2    # "isMaxUnknown":Z
    .end local v3    # "isMinUnknown":Z
    .end local v4    # "middlePosition":I
    :cond_1
    iget v5, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mSize:I

    iget v6, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mWindowAlignmentOffset:I

    add-int/2addr v5, v6

    iget v6, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mPaddingLow:I

    sub-int v4, v5, v6

    .restart local v4    # "middlePosition":I
    goto :goto_0

    .line 223
    .restart local v0    # "afterMiddlePosition":I
    .restart local v1    # "clientSize":I
    .restart local v2    # "isMaxUnknown":Z
    .restart local v3    # "isMinUnknown":Z
    :cond_2
    if-nez v3, :cond_4

    .line 224
    iget v5, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mWindowAlignment:I

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_4

    if-nez p2, :cond_3

    iget v5, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMinEdge:I

    sub-int v5, p1, v5

    if-gt v5, v4, :cond_4

    .line 228
    :cond_3
    iget v5, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMinEdge:I

    iget v6, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mPaddingLow:I

    sub-int/2addr v5, v6

    goto :goto_1

    .line 231
    :cond_4
    if-nez v2, :cond_6

    .line 232
    iget v5, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mWindowAlignment:I

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_6

    if-nez p3, :cond_5

    iget v5, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMaxEdge:I

    sub-int/2addr v5, p1

    if-gt v5, v0, :cond_6

    .line 236
    :cond_5
    iget v5, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMaxEdge:I

    iget v6, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mPaddingLow:I

    sub-int/2addr v5, v6

    sub-int/2addr v5, v1

    goto :goto_1

    .line 240
    :cond_6
    sub-int v5, p1, v4

    iget v6, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mPaddingLow:I

    sub-int/2addr v5, v6

    goto :goto_1
.end method

.method public final invalidateScrollMax()V
    .locals 1

    .prologue
    const v0, 0x7fffffff

    .line 149
    iput v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMaxEdge:I

    .line 150
    iput v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMaxScroll:I

    .line 151
    return-void
.end method

.method public final invalidateScrollMin()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 126
    iput v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMinEdge:I

    .line 127
    iput v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMinScroll:I

    .line 128
    return-void
.end method

.method public final isMaxUnknown()Z
    .locals 2

    .prologue
    .line 169
    iget v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMaxEdge:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isMinUnknown()Z
    .locals 2

    .prologue
    .line 165
    iget v0, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMinEdge:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setMaxEdge(I)V
    .locals 0
    .param p1, "maxEdge"    # I

    .prologue
    .line 132
    iput p1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMaxEdge:I

    .line 133
    return-void
.end method

.method public final setMaxScroll(I)V
    .locals 0
    .param p1, "maxScroll"    # I

    .prologue
    .line 141
    iput p1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMaxScroll:I

    .line 142
    return-void
.end method

.method public final setMinEdge(I)V
    .locals 0
    .param p1, "minEdge"    # I

    .prologue
    .line 109
    iput p1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMinEdge:I

    .line 110
    return-void
.end method

.method public final setMinScroll(I)V
    .locals 0
    .param p1, "minScroll"    # I

    .prologue
    .line 118
    iput p1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMinScroll:I

    .line 119
    return-void
.end method

.method public final setPadding(II)V
    .locals 0
    .param p1, "paddingLow"    # I
    .param p2, "paddingHigh"    # I

    .prologue
    .line 181
    iput p1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mPaddingLow:I

    .line 182
    iput p2, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mPaddingHigh:I

    .line 183
    return-void
.end method

.method public final setSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 173
    iput p1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mSize:I

    .line 174
    return-void
.end method

.method public final setWindowAlignment(I)V
    .locals 0
    .param p1, "windowAlignment"    # I

    .prologue
    .line 80
    iput p1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mWindowAlignment:I

    .line 81
    return-void
.end method

.method public final setWindowAlignmentOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 88
    iput p1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mWindowAlignmentOffset:I

    .line 89
    return-void
.end method

.method public final setWindowAlignmentOffsetPercent(F)V
    .locals 1
    .param p1, "percent"    # F

    .prologue
    .line 92
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x42c80000    # 100.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1

    .line 94
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 96
    :cond_1
    iput p1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mWindowAlignmentOffsetPercent:F

    .line 97
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "center: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mScrollCenter:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " min:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMinEdge:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " max:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->mMaxEdge:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
