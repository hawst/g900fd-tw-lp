.class interface abstract Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;
.super Ljava/lang/Object;
.source "TransitionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/transition/TransitionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "TransitionHelperVersionImpl"
.end annotation


# virtual methods
.method public abstract addTarget(Ljava/lang/Object;Landroid/view/View;)V
.end method

.method public abstract addTransition(Ljava/lang/Object;Ljava/lang/Object;)V
.end method

.method public abstract createChangeBounds(Z)Ljava/lang/Object;
.end method

.method public abstract createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;
.end method

.method public abstract createFadeTransition(I)Ljava/lang/Object;
.end method

.method public abstract createScale()Ljava/lang/Object;
.end method

.method public abstract createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;
.end method

.method public abstract createSlide(Landroid/support/v17/leanback/transition/SlideCallback;)Ljava/lang/Object;
.end method

.method public abstract createTransitionSet(Z)Ljava/lang/Object;
.end method

.method public abstract excludeChildren(Ljava/lang/Object;IZ)V
.end method

.method public abstract getSharedElementEnterTransition(Landroid/view/Window;)Ljava/lang/Object;
.end method

.method public abstract runTransition(Ljava/lang/Object;Ljava/lang/Object;)V
.end method

.method public abstract setDuration(Ljava/lang/Object;J)V
.end method

.method public abstract setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V
.end method

.method public abstract setStartDelay(Ljava/lang/Object;J)V
.end method

.method public abstract setTransitionListener(Ljava/lang/Object;Landroid/support/v17/leanback/transition/TransitionListener;)V
.end method
