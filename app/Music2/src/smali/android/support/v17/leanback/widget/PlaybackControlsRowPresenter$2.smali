.class Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$2;
.super Ljava/lang/Object;
.source "PlaybackControlsRowPresenter.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$2;->this$0:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onControlClicked(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/ControlBarPresenter$BoundData;)V
    .locals 3
    .param p1, "itemViewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;
    .param p3, "data"    # Landroid/support/v17/leanback/widget/ControlBarPresenter$BoundData;

    .prologue
    .line 178
    check-cast p3, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$BoundData;

    .end local p3    # "data":Landroid/support/v17/leanback/widget/ControlBarPresenter$BoundData;
    iget-object v0, p3, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$BoundData;->mRowViewHolder:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;

    .line 179
    .local v0, "vh":Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$2;->this$0:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->getOnItemClickedListener()Landroid/support/v17/leanback/widget/OnItemClickedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 180
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$2;->this$0:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->getOnItemClickedListener()Landroid/support/v17/leanback/widget/OnItemClickedListener;

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;->getRow()Landroid/support/v17/leanback/widget/Row;

    move-result-object v2

    invoke-interface {v1, p2, v2}, Landroid/support/v17/leanback/widget/OnItemClickedListener;->onItemClicked(Ljava/lang/Object;Landroid/support/v17/leanback/widget/Row;)V

    .line 182
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$2;->this$0:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->getOnItemViewClickedListener()Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 183
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$2;->this$0:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->getOnItemViewClickedListener()Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;->getRow()Landroid/support/v17/leanback/widget/Row;

    move-result-object v2

    invoke-interface {v1, p1, p2, v0, v2}, Landroid/support/v17/leanback/widget/OnItemViewClickedListener;->onItemClicked(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V

    .line 186
    :cond_1
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$2;->this$0:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    # getter for: Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->mOnActionClickedListener:Landroid/support/v17/leanback/widget/OnActionClickedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->access$500(Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;)Landroid/support/v17/leanback/widget/OnActionClickedListener;

    move-result-object v1

    if-eqz v1, :cond_2

    instance-of v1, p2, Landroid/support/v17/leanback/widget/Action;

    if-eqz v1, :cond_2

    .line 187
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$2;->this$0:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    # getter for: Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->mOnActionClickedListener:Landroid/support/v17/leanback/widget/OnActionClickedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->access$500(Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;)Landroid/support/v17/leanback/widget/OnActionClickedListener;

    move-result-object v1

    check-cast p2, Landroid/support/v17/leanback/widget/Action;

    .end local p2    # "item":Ljava/lang/Object;
    invoke-interface {v1, p2}, Landroid/support/v17/leanback/widget/OnActionClickedListener;->onActionClicked(Landroid/support/v17/leanback/widget/Action;)V

    .line 189
    :cond_2
    return-void
.end method
