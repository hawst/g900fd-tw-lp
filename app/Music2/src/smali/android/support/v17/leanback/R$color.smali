.class public final Landroid/support/v17/leanback/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final lb_default_search_color:I = 0x7f0c00ef

.field public static final lb_playback_controls_background_dark:I = 0x7f0c00f6

.field public static final lb_playback_controls_background_light:I = 0x7f0c00f5

.field public static final lb_search_bar_hint:I = 0x7f0c00e4

.field public static final lb_search_bar_hint_speech_mode:I = 0x7f0c00e5

.field public static final lb_search_bar_text:I = 0x7f0c00e2

.field public static final lb_search_bar_text_speech_mode:I = 0x7f0c00e3

.field public static final lb_speech_orb_not_recording:I = 0x7f0c00e6

.field public static final lb_speech_orb_not_recording_icon:I = 0x7f0c00e9

.field public static final lb_speech_orb_not_recording_pulsed:I = 0x7f0c00e7

.field public static final lb_speech_orb_recording:I = 0x7f0c00e8

.field public static final lb_view_dim_mask_color:I = 0x7f0c00d9
