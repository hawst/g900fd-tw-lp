.class Landroid/support/v17/leanback/app/BrowseFragment$12;
.super Ljava/lang/Object;
.source "BrowseFragment.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/BrowseFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/BrowseFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/BrowseFragment;)V
    .locals 0

    .prologue
    .line 864
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseFragment$12;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Ljava/lang/Object;Landroid/support/v17/leanback/widget/Row;)V
    .locals 4
    .param p1, "item"    # Ljava/lang/Object;
    .param p2, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 867
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$12;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseFragment;->access$300(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/HeadersFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/HeadersFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getSelectedPosition()I

    move-result v0

    .line 868
    .local v0, "position":I
    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->DEBUG:Z
    invoke-static {}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1400()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "BrowseFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "header selected position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 869
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$12;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # invokes: Landroid/support/v17/leanback/app/BrowseFragment;->onRowSelected(I)V
    invoke-static {v1, v0}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1800(Landroid/support/v17/leanback/app/BrowseFragment;I)V

    .line 870
    return-void
.end method
