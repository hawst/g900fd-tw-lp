.class abstract Landroid/support/v17/leanback/widget/StaggeredGrid;
.super Ljava/lang/Object;
.source "StaggeredGrid.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/StaggeredGrid$Row;,
        Landroid/support/v17/leanback/widget/StaggeredGrid$Location;,
        Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;
    }
.end annotation


# instance fields
.field protected mFirstIndex:I

.field protected mLocations:Landroid/support/v4/util/CircularArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/CircularArray",
            "<",
            "Landroid/support/v17/leanback/widget/StaggeredGrid$Location;",
            ">;"
        }
    .end annotation
.end field

.field protected mNumRows:I

.field protected mProvider:Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;

.field protected mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

.field protected mStartIndex:I

.field protected mStartRow:I

.field private mTmpItemPositionsInRows:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mNumRows:I

    .line 98
    new-instance v0, Landroid/support/v4/util/CircularArray;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Landroid/support/v4/util/CircularArray;-><init>(I)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mLocations:Landroid/support/v4/util/CircularArray;

    .line 108
    iput v2, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mStartIndex:I

    .line 110
    iput v2, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mStartRow:I

    .line 112
    iput v2, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mFirstIndex:I

    return-void
.end method


# virtual methods
.method protected final appendItemToRow(II)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    .locals 3
    .param p1, "itemIndex"    # I
    .param p2, "rowIndex"    # I

    .prologue
    .line 237
    new-instance v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    invoke-direct {v0, p2}, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;-><init>(I)V

    .line 238
    .local v0, "loc":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    iget-object v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mLocations:Landroid/support/v4/util/CircularArray;

    invoke-virtual {v1}, Landroid/support/v4/util/CircularArray;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 239
    iput p1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mFirstIndex:I

    .line 241
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mLocations:Landroid/support/v4/util/CircularArray;

    invoke-virtual {v1, v0}, Landroid/support/v4/util/CircularArray;->addLast(Ljava/lang/Object;)V

    .line 242
    iget-object v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mProvider:Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;

    const/4 v2, 0x1

    invoke-interface {v1, p1, p2, v2}, Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;->createItem(IIZ)V

    .line 243
    return-object v0
.end method

.method public abstract appendItems(I)V
.end method

.method public final getFirstIndex()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mFirstIndex:I

    return v0
.end method

.method public final getItemPositionsInRows(II)[Ljava/util/List;
    .locals 3
    .param p1, "startPos"    # I
    .param p2, "endPos"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)[",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mNumRows:I

    if-ge v0, v1, :cond_0

    .line 286
    iget-object v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mTmpItemPositionsInRows:[Ljava/util/ArrayList;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 285
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 288
    :cond_0
    if-ltz p1, :cond_1

    .line 289
    move v0, p1

    :goto_1
    if-gt v0, p2, :cond_1

    .line 290
    iget-object v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mTmpItemPositionsInRows:[Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v2

    iget v2, v2, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    aget-object v1, v1, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 289
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 293
    :cond_1
    iget-object v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mTmpItemPositionsInRows:[Ljava/util/ArrayList;

    return-object v1
.end method

.method public final getLastIndex()I
    .locals 2

    .prologue
    .line 172
    iget v0, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mFirstIndex:I

    iget-object v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mLocations:Landroid/support/v4/util/CircularArray;

    invoke-virtual {v1}, Landroid/support/v4/util/CircularArray;->size()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 186
    iget-object v0, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mLocations:Landroid/support/v4/util/CircularArray;

    invoke-virtual {v0}, Landroid/support/v4/util/CircularArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 187
    const/4 v0, 0x0

    .line 189
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mLocations:Landroid/support/v4/util/CircularArray;

    iget v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mFirstIndex:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/support/v4/util/CircularArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    goto :goto_0
.end method

.method protected final getMaxHighRowIndex()I
    .locals 4

    .prologue
    .line 217
    const/4 v1, 0x0

    .line 218
    .local v1, "maxHighRowIndex":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mNumRows:I

    if-ge v0, v2, :cond_1

    .line 219
    iget-object v2, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    iget-object v3, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v3, v3, v1

    iget v3, v3, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    if-le v2, v3, :cond_0

    .line 220
    move v1, v0

    .line 218
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 223
    :cond_1
    return v1
.end method

.method protected final getMaxLowRowIndex()I
    .locals 4

    .prologue
    .line 252
    const/4 v1, 0x0

    .line 253
    .local v1, "maxLowRowIndex":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mNumRows:I

    if-ge v0, v2, :cond_1

    .line 254
    iget-object v2, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    iget-object v3, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v3, v3, v1

    iget v3, v3, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    if-le v2, v3, :cond_0

    .line 255
    move v1, v0

    .line 253
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 258
    :cond_1
    return v1
.end method

.method protected final getMinHighRowIndex()I
    .locals 4

    .prologue
    .line 227
    const/4 v1, 0x0

    .line 228
    .local v1, "minHighRowIndex":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mNumRows:I

    if-ge v0, v2, :cond_1

    .line 229
    iget-object v2, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    iget-object v3, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v3, v3, v1

    iget v3, v3, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    if-ge v2, v3, :cond_0

    .line 230
    move v1, v0

    .line 228
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233
    :cond_1
    return v1
.end method

.method protected final getMinLowRowIndex()I
    .locals 4

    .prologue
    .line 262
    const/4 v1, 0x0

    .line 263
    .local v1, "minLowRowIndex":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mNumRows:I

    if-ge v0, v2, :cond_1

    .line 264
    iget-object v2, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    iget-object v3, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v3, v3, v1

    iget v3, v3, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    if-ge v2, v3, :cond_0

    .line 265
    move v1, v0

    .line 263
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 268
    :cond_1
    return v1
.end method

.method public final getNumRows()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mNumRows:I

    return v0
.end method

.method public final getSize()I
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mLocations:Landroid/support/v4/util/CircularArray;

    invoke-virtual {v0}, Landroid/support/v4/util/CircularArray;->size()I

    move-result v0

    return v0
.end method

.method protected final prependItemToRow(II)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    .locals 3
    .param p1, "itemIndex"    # I
    .param p2, "rowIndex"    # I

    .prologue
    .line 272
    new-instance v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    invoke-direct {v0, p2}, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;-><init>(I)V

    .line 273
    .local v0, "loc":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    iput p1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mFirstIndex:I

    .line 274
    iget-object v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mLocations:Landroid/support/v4/util/CircularArray;

    invoke-virtual {v1, v0}, Landroid/support/v4/util/CircularArray;->addFirst(Ljava/lang/Object;)V

    .line 275
    iget-object v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mProvider:Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;

    const/4 v2, 0x0

    invoke-interface {v1, p1, p2, v2}, Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;->createItem(IIZ)V

    .line 276
    return-object v0
.end method

.method public abstract prependItems(I)V
.end method

.method public final removeFirst()V
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mFirstIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mFirstIndex:I

    .line 197
    iget-object v0, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mLocations:Landroid/support/v4/util/CircularArray;

    invoke-virtual {v0}, Landroid/support/v4/util/CircularArray;->popFirst()Ljava/lang/Object;

    .line 198
    return-void
.end method

.method public final removeLast()V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mLocations:Landroid/support/v4/util/CircularArray;

    invoke-virtual {v0}, Landroid/support/v4/util/CircularArray;->popLast()Ljava/lang/Object;

    .line 205
    return-void
.end method

.method public setProvider(Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;)V
    .locals 0
    .param p1, "provider"    # Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;

    .prologue
    .line 120
    iput-object p1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mProvider:Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;

    .line 121
    return-void
.end method

.method public final setRows([Landroid/support/v17/leanback/widget/StaggeredGrid$Row;)V
    .locals 4
    .param p1, "row"    # [Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    .prologue
    .line 131
    if-eqz p1, :cond_0

    array-length v1, p1

    if-nez v1, :cond_1

    .line 132
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 134
    :cond_1
    array-length v1, p1

    iput v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mNumRows:I

    .line 135
    iput-object p1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    .line 136
    iget v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mNumRows:I

    new-array v1, v1, [Ljava/util/ArrayList;

    iput-object v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mTmpItemPositionsInRows:[Ljava/util/ArrayList;

    .line 137
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mNumRows:I

    if-ge v0, v1, :cond_2

    .line 138
    iget-object v1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mTmpItemPositionsInRows:[Ljava/util/ArrayList;

    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    aput-object v2, v1, v0

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_2
    return-void
.end method

.method public final setStart(II)V
    .locals 0
    .param p1, "startIndex"    # I
    .param p2, "startRow"    # I

    .prologue
    .line 157
    iput p1, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mStartIndex:I

    .line 158
    iput p2, p0, Landroid/support/v17/leanback/widget/StaggeredGrid;->mStartRow:I

    .line 159
    return-void
.end method

.method public abstract stripDownTo(I)V
.end method
