.class Landroid/support/v17/leanback/app/TitleTransitionHelper;
.super Ljava/lang/Object;
.source "TitleTransitionHelper.java"


# static fields
.field static final sSlideCallback:Landroid/support/v17/leanback/transition/SlideCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Landroid/support/v17/leanback/app/TitleTransitionHelper$1;

    invoke-direct {v0}, Landroid/support/v17/leanback/app/TitleTransitionHelper$1;-><init>()V

    sput-object v0, Landroid/support/v17/leanback/app/TitleTransitionHelper;->sSlideCallback:Landroid/support/v17/leanback/transition/SlideCallback;

    return-void
.end method

.method private static createTransitionInterpolatorDown()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    return-object v0
.end method

.method private static createTransitionInterpolatorUp()Landroid/view/animation/Interpolator;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40800000    # 4.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    return-object v0
.end method

.method public static createTransitionTitleDown(Landroid/support/v17/leanback/transition/TransitionHelper;)Ljava/lang/Object;
    .locals 2
    .param p0, "helper"    # Landroid/support/v17/leanback/transition/TransitionHelper;

    .prologue
    .line 48
    sget-object v1, Landroid/support/v17/leanback/app/TitleTransitionHelper;->sSlideCallback:Landroid/support/v17/leanback/transition/SlideCallback;

    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/transition/TransitionHelper;->createSlide(Landroid/support/v17/leanback/transition/SlideCallback;)Ljava/lang/Object;

    move-result-object v0

    .line 49
    .local v0, "transition":Ljava/lang/Object;
    invoke-static {}, Landroid/support/v17/leanback/app/TitleTransitionHelper;->createTransitionInterpolatorDown()Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v17/leanback/transition/TransitionHelper;->setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 50
    return-object v0
.end method

.method public static createTransitionTitleUp(Landroid/support/v17/leanback/transition/TransitionHelper;)Ljava/lang/Object;
    .locals 2
    .param p0, "helper"    # Landroid/support/v17/leanback/transition/TransitionHelper;

    .prologue
    .line 42
    sget-object v1, Landroid/support/v17/leanback/app/TitleTransitionHelper;->sSlideCallback:Landroid/support/v17/leanback/transition/SlideCallback;

    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/transition/TransitionHelper;->createSlide(Landroid/support/v17/leanback/transition/SlideCallback;)Ljava/lang/Object;

    move-result-object v0

    .line 43
    .local v0, "transition":Ljava/lang/Object;
    invoke-static {}, Landroid/support/v17/leanback/app/TitleTransitionHelper;->createTransitionInterpolatorUp()Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v17/leanback/transition/TransitionHelper;->setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 44
    return-object v0
.end method
