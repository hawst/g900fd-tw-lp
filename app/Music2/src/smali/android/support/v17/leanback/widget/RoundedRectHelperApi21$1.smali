.class final Landroid/support/v17/leanback/widget/RoundedRectHelperApi21$1;
.super Landroid/view/ViewOutlineProvider;
.source "RoundedRectHelperApi21.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/RoundedRectHelperApi21;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/view/ViewOutlineProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public getOutline(Landroid/view/View;Landroid/graphics/Outline;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "outline"    # Landroid/graphics/Outline;

    .prologue
    const/4 v1, 0x0

    .line 30
    # getter for: Landroid/support/v17/leanback/widget/RoundedRectHelperApi21;->sCornerRadius:I
    invoke-static {}, Landroid/support/v17/leanback/widget/RoundedRectHelperApi21;->access$000()I

    move-result v0

    if-nez v0, :cond_0

    .line 31
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Landroid/support/v17/leanback/R$dimen;->lb_rounded_rect_corner_radius:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    # setter for: Landroid/support/v17/leanback/widget/RoundedRectHelperApi21;->sCornerRadius:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/RoundedRectHelperApi21;->access$002(I)I

    .line 34
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    # getter for: Landroid/support/v17/leanback/widget/RoundedRectHelperApi21;->sCornerRadius:I
    invoke-static {}, Landroid/support/v17/leanback/widget/RoundedRectHelperApi21;->access$000()I

    move-result v0

    int-to-float v5, v0

    move-object v0, p2

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Outline;->setRoundRect(IIIIF)V

    .line 35
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p2, v0}, Landroid/graphics/Outline;->setAlpha(F)V

    .line 36
    return-void
.end method
