.class public final Landroid/support/v17/leanback/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final bottom_spacer:I = 0x7f0e014c

.field public static final browse_badge:I = 0x7f0e015a

.field public static final browse_container_dock:I = 0x7f0e0124

.field public static final browse_frame:I = 0x7f0e0123

.field public static final browse_grid:I = 0x7f0e015d

.field public static final browse_grid_dock:I = 0x7f0e015e

.field public static final browse_headers:I = 0x7f0e0137

.field public static final browse_headers_dock:I = 0x7f0e0125

.field public static final browse_orb:I = 0x7f0e015c

.field public static final browse_title:I = 0x7f0e015b

.field public static final browse_title_group:I = 0x7f0e0126

.field public static final button:I = 0x7f0e0128

.field public static final container_list:I = 0x7f0e014f

.field public static final content_text:I = 0x7f0e013c

.field public static final control_bar:I = 0x7f0e0127

.field public static final controls_card:I = 0x7f0e0147

.field public static final controls_dock:I = 0x7f0e014a

.field public static final current_time:I = 0x7f0e0145

.field public static final description:I = 0x7f0e0141

.field public static final description_dock:I = 0x7f0e0148

.field public static final details_frame:I = 0x7f0e012e

.field public static final details_overview:I = 0x7f0e012f

.field public static final details_overview_actions:I = 0x7f0e0133

.field public static final details_overview_description:I = 0x7f0e0132

.field public static final details_overview_image:I = 0x7f0e0130

.field public static final details_overview_right_panel:I = 0x7f0e0131

.field public static final extra_badge:I = 0x7f0e013d

.field public static final fade_mask:I = 0x7f0e013e

.field public static final fade_out_edge:I = 0x7f0e0138

.field public static final fragment_dock:I = 0x7f0e012d

.field public static final icon:I = 0x7f0e007c

.field public static final image:I = 0x7f0e00b5

.field public static final info_field:I = 0x7f0e013a

.field public static final lb_action_button:I = 0x7f0e0121

.field public static final lb_control_more_actions:I = 0x7f0e003e

.field public static final lb_control_play_pause:I = 0x7f0e0039

.field public static final lb_control_repeat:I = 0x7f0e0041

.field public static final lb_control_shuffle:I = 0x7f0e0042

.field public static final lb_control_skip_next:I = 0x7f0e003c

.field public static final lb_control_skip_previous:I = 0x7f0e003d

.field public static final lb_control_thumbs_down:I = 0x7f0e0040

.field public static final lb_control_thumbs_up:I = 0x7f0e003f

.field public static final lb_details_description_body:I = 0x7f0e012b

.field public static final lb_details_description_subtitle:I = 0x7f0e012a

.field public static final lb_details_description_title:I = 0x7f0e0129

.field public static final lb_focus_animator:I = 0x7f0e0037

.field public static final lb_results_frame:I = 0x7f0e0155

.field public static final lb_row_container_header_dock:I = 0x7f0e014d

.field public static final lb_search_bar:I = 0x7f0e0156

.field public static final lb_search_bar_badge:I = 0x7f0e0152

.field public static final lb_search_bar_items:I = 0x7f0e0151

.field public static final lb_search_bar_speech_orb:I = 0x7f0e0150

.field public static final lb_search_frame:I = 0x7f0e0154

.field public static final lb_search_text_editor:I = 0x7f0e0153

.field public static final lb_shadow_focused:I = 0x7f0e0159

.field public static final lb_shadow_normal:I = 0x7f0e0158

.field public static final lb_slide_transition_value:I = 0x7f0e0038

.field public static final main_image:I = 0x7f0e0139

.field public static final more_actions_dock:I = 0x7f0e0144

.field public static final playback_progress:I = 0x7f0e0142

.field public static final row_content:I = 0x7f0e013f

.field public static final scale_frame:I = 0x7f0e014e

.field public static final search_orb:I = 0x7f0e0157

.field public static final secondary_controls_dock:I = 0x7f0e014b

.field public static final spacer:I = 0x7f0e0149

.field public static final title:I = 0x7f0e00b8

.field public static final title_text:I = 0x7f0e013b

.field public static final total_time:I = 0x7f0e0146
