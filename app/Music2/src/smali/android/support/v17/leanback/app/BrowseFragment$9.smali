.class Landroid/support/v17/leanback/app/BrowseFragment$9;
.super Ljava/lang/Object;
.source "BrowseFragment.java"

# interfaces
.implements Landroid/support/v17/leanback/app/HeadersFragment$OnHeaderClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/BrowseFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/BrowseFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/BrowseFragment;)V
    .locals 0

    .prologue
    .line 830
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseFragment$9;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHeaderClicked()V
    .locals 2

    .prologue
    .line 833
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment$9;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z
    invoke-static {v0}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1200(Landroid/support/v17/leanback/app/BrowseFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment$9;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z
    invoke-static {v0}, Landroid/support/v17/leanback/app/BrowseFragment;->access$000(Landroid/support/v17/leanback/app/BrowseFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment$9;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BrowseFragment;->isInHeadersTransition()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 838
    :cond_0
    :goto_0
    return-void

    .line 836
    :cond_1
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment$9;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    const/4 v1, 0x0

    # invokes: Landroid/support/v17/leanback/app/BrowseFragment;->startHeadersTransitionInternal(Z)V
    invoke-static {v0, v1}, Landroid/support/v17/leanback/app/BrowseFragment;->access$200(Landroid/support/v17/leanback/app/BrowseFragment;Z)V

    .line 837
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment$9;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;
    invoke-static {v0}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1600(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/RowsFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->requestFocus()Z

    goto :goto_0
.end method
