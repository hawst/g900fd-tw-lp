.class public final Landroid/support/v17/leanback/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final lb_action_padding_horizontal:I = 0x7f0f020a

.field public static final lb_action_with_icon_padding_left:I = 0x7f0f020b

.field public static final lb_action_with_icon_padding_right:I = 0x7f0f020c

.field public static final lb_browse_expanded_row_no_hovercard_bottom_padding:I = 0x7f0f01ed

.field public static final lb_browse_expanded_selected_row_top_padding:I = 0x7f0f01ec

.field public static final lb_browse_header_select_duration:I = 0x7f0f01e4

.field public static final lb_browse_header_select_scale:I = 0x7f0f01e5

.field public static final lb_browse_selected_row_top_padding:I = 0x7f0f01eb

.field public static final lb_control_icon_width:I = 0x7f0f0226

.field public static final lb_details_description_body_line_spacing:I = 0x7f0f0204

.field public static final lb_details_description_title_baseline:I = 0x7f0f0205

.field public static final lb_details_description_title_line_spacing:I = 0x7f0f0203

.field public static final lb_details_description_under_subtitle_baseline_margin:I = 0x7f0f0207

.field public static final lb_details_description_under_title_baseline_margin:I = 0x7f0f0206

.field public static final lb_details_overview_actions_fade_size:I = 0x7f0f01fe

.field public static final lb_details_overview_height_large:I = 0x7f0f01ee

.field public static final lb_details_overview_height_small:I = 0x7f0f01ef

.field public static final lb_details_overview_image_margin_horizontal:I = 0x7f0f01f7

.field public static final lb_details_overview_image_margin_vertical:I = 0x7f0f01f8

.field public static final lb_details_rows_align_top:I = 0x7f0f01ff

.field public static final lb_material_shadow_focused_z:I = 0x7f0f024f

.field public static final lb_material_shadow_normal_z:I = 0x7f0f024e

.field public static final lb_playback_controls_align_bottom:I = 0x7f0f0210

.field public static final lb_playback_controls_child_margin_bigger:I = 0x7f0f0220

.field public static final lb_playback_controls_child_margin_biggest:I = 0x7f0f0221

.field public static final lb_playback_controls_child_margin_default:I = 0x7f0f021f

.field public static final lb_playback_controls_padding_bottom:I = 0x7f0f0211

.field public static final lb_playback_controls_z:I = 0x7f0f0254

.field public static final lb_playback_major_fade_translate_y:I = 0x7f0f0212

.field public static final lb_playback_minor_fade_translate_y:I = 0x7f0f0213

.field public static final lb_rounded_rect_corner_radius:I = 0x7f0f0256

.field public static final lb_search_bar_height:I = 0x7f0f022d

.field public static final lb_search_browse_rows_align_top:I = 0x7f0f023d

.field public static final lb_search_orb_focused_z:I = 0x7f0f0252

.field public static final lb_search_orb_unfocused_z:I = 0x7f0f0251
