.class public Landroid/support/v17/leanback/widget/Action;
.super Ljava/lang/Object;
.source "Action.java"


# instance fields
.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mId:J

.field private mLabel1:Ljava/lang/CharSequence;

.field private mLabel2:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 38
    const-string v0, ""

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v17/leanback/widget/Action;-><init>(JLjava/lang/CharSequence;)V

    .line 39
    return-void
.end method

.method public constructor <init>(JLjava/lang/CharSequence;)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "label"    # Ljava/lang/CharSequence;

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v17/leanback/widget/Action;-><init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 49
    return-void
.end method

.method public constructor <init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 7
    .param p1, "id"    # J
    .param p3, "label1"    # Ljava/lang/CharSequence;
    .param p4, "label2"    # Ljava/lang/CharSequence;

    .prologue
    .line 59
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v6}, Landroid/support/v17/leanback/widget/Action;-><init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    .line 60
    return-void
.end method

.method public constructor <init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "label1"    # Ljava/lang/CharSequence;
    .param p4, "label2"    # Ljava/lang/CharSequence;
    .param p5, "icon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v17/leanback/widget/Action;->mId:J

    .line 71
    invoke-virtual {p0, p1, p2}, Landroid/support/v17/leanback/widget/Action;->setId(J)V

    .line 72
    invoke-virtual {p0, p3}, Landroid/support/v17/leanback/widget/Action;->setLabel1(Ljava/lang/CharSequence;)V

    .line 73
    invoke-virtual {p0, p4}, Landroid/support/v17/leanback/widget/Action;->setLabel2(Ljava/lang/CharSequence;)V

    .line 74
    invoke-virtual {p0, p5}, Landroid/support/v17/leanback/widget/Action;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 75
    return-void
.end method


# virtual methods
.method public final getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Landroid/support/v17/leanback/widget/Action;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getId()J
    .locals 2

    .prologue
    .line 88
    iget-wide v0, p0, Landroid/support/v17/leanback/widget/Action;->mId:J

    return-wide v0
.end method

.method public final getLabel1()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Landroid/support/v17/leanback/widget/Action;->mLabel1:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getLabel2()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Landroid/support/v17/leanback/widget/Action;->mLabel2:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "icon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 123
    iput-object p1, p0, Landroid/support/v17/leanback/widget/Action;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 124
    return-void
.end method

.method public final setId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 81
    iput-wide p1, p0, Landroid/support/v17/leanback/widget/Action;->mId:J

    .line 82
    return-void
.end method

.method public final setLabel1(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "label"    # Ljava/lang/CharSequence;

    .prologue
    .line 95
    iput-object p1, p0, Landroid/support/v17/leanback/widget/Action;->mLabel1:Ljava/lang/CharSequence;

    .line 96
    return-void
.end method

.method public final setLabel2(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "label"    # Ljava/lang/CharSequence;

    .prologue
    .line 109
    iput-object p1, p0, Landroid/support/v17/leanback/widget/Action;->mLabel2:Ljava/lang/CharSequence;

    .line 110
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Landroid/support/v17/leanback/widget/Action;->mLabel1:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 137
    iget-object v1, p0, Landroid/support/v17/leanback/widget/Action;->mLabel1:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 139
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/widget/Action;->mLabel2:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 140
    iget-object v1, p0, Landroid/support/v17/leanback/widget/Action;->mLabel1:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 141
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    :cond_1
    iget-object v1, p0, Landroid/support/v17/leanback/widget/Action;->mLabel2:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 145
    :cond_2
    iget-object v1, p0, Landroid/support/v17/leanback/widget/Action;->mIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-nez v1, :cond_3

    .line 146
    const-string v1, "(action icon)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
