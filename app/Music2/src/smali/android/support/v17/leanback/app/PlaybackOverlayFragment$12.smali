.class Landroid/support/v17/leanback/app/PlaybackOverlayFragment$12;
.super Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
.source "PlaybackOverlayFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/PlaybackOverlayFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V
    .locals 0

    .prologue
    .line 625
    iput-object p1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$12;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttachedToWindow(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 2
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 629
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$12;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # getter for: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingStatus:I
    invoke-static {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$500(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$12;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # getter for: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgAlpha:I
    invoke-static {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$100(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$12;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # getter for: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingStatus:I
    invoke-static {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$500(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 631
    :cond_1
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 633
    :cond_2
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPosition()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$12;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # getter for: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mResetControlsToPrimaryActionsPending:Z
    invoke-static {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$1300(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 634
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$12;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # invokes: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->resetControlsToPrimaryActions(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    invoke-static {v0, p1}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$400(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 636
    :cond_3
    return-void
.end method

.method public onBind(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 1
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 653
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPosition()I

    move-result v0

    if-nez v0, :cond_0

    .line 654
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$12;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # invokes: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->updateControlsBottomSpace(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    invoke-static {v0, p1}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$1400(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 656
    :cond_0
    return-void
.end method

.method public onDetachedFromWindow(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 4
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 641
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v1

    iget-object v1, v1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 642
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v1

    iget-object v1, v1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 643
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v1

    instance-of v1, v1, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;

    if-eqz v1, :cond_0

    .line 644
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;

    iget-object v0, v1, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;->mDescriptionViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .line 646
    .local v0, "descriptionVh":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    if-eqz v0, :cond_0

    .line 647
    iget-object v1, v0, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 650
    .end local v0    # "descriptionVh":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    :cond_0
    return-void
.end method
