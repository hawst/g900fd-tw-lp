.class public Landroid/support/v17/leanback/widget/HeaderItem;
.super Ljava/lang/Object;
.source "HeaderItem.java"


# instance fields
.field private final mId:J

.field private final mImageUri:Ljava/lang/String;

.field private final mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "imageUri"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-wide p1, p0, Landroid/support/v17/leanback/widget/HeaderItem;->mId:J

    .line 33
    iput-object p3, p0, Landroid/support/v17/leanback/widget/HeaderItem;->mName:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Landroid/support/v17/leanback/widget/HeaderItem;->mImageUri:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "imageUri"    # Ljava/lang/String;

    .prologue
    .line 41
    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1, p1, p2}, Landroid/support/v17/leanback/widget/HeaderItem;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    .line 42
    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Landroid/support/v17/leanback/widget/HeaderItem;->mName:Ljava/lang/String;

    return-object v0
.end method
