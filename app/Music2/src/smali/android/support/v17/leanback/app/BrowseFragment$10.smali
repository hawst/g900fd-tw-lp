.class Landroid/support/v17/leanback/app/BrowseFragment$10;
.super Ljava/lang/Object;
.source "BrowseFragment.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/BrowseFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/BrowseFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/BrowseFragment;)V
    .locals 0

    .prologue
    .line 841
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseFragment$10;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V
    .locals 4
    .param p1, "itemViewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;
    .param p3, "rowViewHolder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p4, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 845
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$10;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1600(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/RowsFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getSelectedPosition()I

    move-result v0

    .line 846
    .local v0, "position":I
    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->DEBUG:Z
    invoke-static {}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1400()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "BrowseFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "row selected position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$10;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # invokes: Landroid/support/v17/leanback/app/BrowseFragment;->onRowSelected(I)V
    invoke-static {v1, v0}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1800(Landroid/support/v17/leanback/app/BrowseFragment;I)V

    .line 848
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$10;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mExternalOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1900(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 849
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment$10;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mExternalOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1900(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v1

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;->onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V

    .line 852
    :cond_1
    return-void
.end method
