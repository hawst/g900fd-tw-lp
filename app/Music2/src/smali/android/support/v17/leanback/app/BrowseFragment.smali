.class public Landroid/support/v17/leanback/app/BrowseFragment;
.super Landroid/app/Fragment;
.source "BrowseFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/app/BrowseFragment$SetSelectionRunnable;,
        Landroid/support/v17/leanback/app/BrowseFragment$BrowseTransitionListener;,
        Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;
    }
.end annotation


# static fields
.field private static final ARG_BADGE_URI:Ljava/lang/String;

.field private static final ARG_HEADERS_STATE:Ljava/lang/String;

.field private static final ARG_TITLE:Ljava/lang/String;

.field private static DEBUG:Z

.field private static sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;


# instance fields
.field private mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;

.field private mBadgeDrawable:Landroid/graphics/drawable/Drawable;

.field private mBrandColor:I

.field private mBrandColorSet:Z

.field private mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

.field private mBrowseTransitionListener:Landroid/support/v17/leanback/app/BrowseFragment$BrowseTransitionListener;

.field private mCanShowHeaders:Z

.field private mContainerListAlignTop:I

.field private mContainerListMarginLeft:I

.field private mExternalOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

.field private mExternalOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

.field private mExternalOnSearchClickedListener:Landroid/view/View$OnClickListener;

.field private mHeaderClickedListener:Landroid/support/v17/leanback/app/HeadersFragment$OnHeaderClickedListener;

.field private mHeaderPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

.field private mHeaderSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

.field private mHeadersBackStackEnabled:Z

.field private mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

.field private mHeadersState:I

.field private mHeadersTransition:Ljava/lang/Object;

.field private mHeadersTransitionDuration:I

.field private mHeadersTransitionStartDelay:I

.field private final mOnChildFocusListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnChildFocusListener;

.field private final mOnFocusSearchListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnFocusSearchListener;

.field private mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

.field private mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

.field private mReparentHeaderId:I

.field private mRowScaleEnabled:Z

.field private mRowSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

.field private mRowViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

.field private mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

.field private mSceneWithHeaders:Ljava/lang/Object;

.field private mSceneWithTitle:Ljava/lang/Object;

.field private mSceneWithoutHeaders:Ljava/lang/Object;

.field private mSceneWithoutTitle:Ljava/lang/Object;

.field private mSearchAffordanceColorSet:Z

.field private mSearchAffordanceColors:Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

.field private mSelectedPosition:I

.field private final mSetSelectionRunnable:Landroid/support/v17/leanback/app/BrowseFragment$SetSelectionRunnable;

.field private mShowingHeaders:Z

.field private mShowingTitle:Z

.field private mTitle:Ljava/lang/String;

.field private mTitleDownTransition:Ljava/lang/Object;

.field private mTitleUpTransition:Ljava/lang/Object;

.field private mTitleView:Landroid/support/v17/leanback/widget/TitleView;

.field private mWithHeadersBackStackName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 159
    const/4 v0, 0x0

    sput-boolean v0, Landroid/support/v17/leanback/app/BrowseFragment;->DEBUG:Z

    .line 205
    invoke-static {}, Landroid/support/v17/leanback/transition/TransitionHelper;->getInstance()Landroid/support/v17/leanback/transition/TransitionHelper;

    move-result-object v0

    sput-object v0, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".title"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v17/leanback/app/BrowseFragment;->ARG_TITLE:Ljava/lang/String;

    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".badge"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v17/leanback/app/BrowseFragment;->ARG_BADGE_URI:Ljava/lang/String;

    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".headersState"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v17/leanback/app/BrowseFragment;->ARG_HEADERS_STATE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 71
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 179
    iput v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersState:I

    .line 180
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrandColor:I

    .line 185
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingTitle:Z

    .line 186
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersBackStackEnabled:Z

    .line 188
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    .line 189
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z

    .line 192
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowScaleEnabled:Z

    .line 200
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSelectedPosition:I

    .line 206
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mReparentHeaderId:I

    .line 522
    new-instance v0, Landroid/support/v17/leanback/app/BrowseFragment$2;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BrowseFragment$2;-><init>(Landroid/support/v17/leanback/app/BrowseFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mOnFocusSearchListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnFocusSearchListener;

    .line 557
    new-instance v0, Landroid/support/v17/leanback/app/BrowseFragment$3;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BrowseFragment$3;-><init>(Landroid/support/v17/leanback/app/BrowseFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mOnChildFocusListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnChildFocusListener;

    .line 829
    new-instance v0, Landroid/support/v17/leanback/app/BrowseFragment$9;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BrowseFragment$9;-><init>(Landroid/support/v17/leanback/app/BrowseFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeaderClickedListener:Landroid/support/v17/leanback/app/HeadersFragment$OnHeaderClickedListener;

    .line 841
    new-instance v0, Landroid/support/v17/leanback/app/BrowseFragment$10;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BrowseFragment$10;-><init>(Landroid/support/v17/leanback/app/BrowseFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .line 855
    new-instance v0, Landroid/support/v17/leanback/app/BrowseFragment$11;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BrowseFragment$11;-><init>(Landroid/support/v17/leanback/app/BrowseFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .line 864
    new-instance v0, Landroid/support/v17/leanback/app/BrowseFragment$12;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BrowseFragment$12;-><init>(Landroid/support/v17/leanback/app/BrowseFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeaderSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .line 898
    new-instance v0, Landroid/support/v17/leanback/app/BrowseFragment$SetSelectionRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v17/leanback/app/BrowseFragment$SetSelectionRunnable;-><init>(Landroid/support/v17/leanback/app/BrowseFragment;Landroid/support/v17/leanback/app/BrowseFragment$1;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSetSelectionRunnable:Landroid/support/v17/leanback/app/BrowseFragment$SetSelectionRunnable;

    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/app/BrowseFragment;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    return v0
.end method

.method static synthetic access$002(Landroid/support/v17/leanback/app/BrowseFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    return p1
.end method

.method static synthetic access$100(Landroid/support/v17/leanback/app/BrowseFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mWithHeadersBackStackName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Landroid/support/v17/leanback/app/BrowseFragment;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersBackStackEnabled:Z

    return v0
.end method

.method static synthetic access$1100(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;

    return-object v0
.end method

.method static synthetic access$1200(Landroid/support/v17/leanback/app/BrowseFragment;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z

    return v0
.end method

.method static synthetic access$1300(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/widget/TitleView;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    return-object v0
.end method

.method static synthetic access$1400()Z
    .locals 1

    .prologue
    .line 71
    sget-boolean v0, Landroid/support/v17/leanback/app/BrowseFragment;->DEBUG:Z

    return v0
.end method

.method static synthetic access$1500(Landroid/support/v17/leanback/app/BrowseFragment;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->isVerticalScrolling()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/RowsFragment;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    return-object v0
.end method

.method static synthetic access$1700(Landroid/support/v17/leanback/app/BrowseFragment;Z)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/BrowseFragment;->showHeaders(Z)V

    return-void
.end method

.method static synthetic access$1800(Landroid/support/v17/leanback/app/BrowseFragment;I)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;
    .param p1, "x1"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/BrowseFragment;->onRowSelected(I)V

    return-void
.end method

.method static synthetic access$1900(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mExternalOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    return-object v0
.end method

.method static synthetic access$200(Landroid/support/v17/leanback/app/BrowseFragment;Z)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/BrowseFragment;->startHeadersTransitionInternal(Z)V

    return-void
.end method

.method static synthetic access$2000(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/widget/OnItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mExternalOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$2100(Landroid/support/v17/leanback/app/BrowseFragment;I)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;
    .param p1, "x1"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/BrowseFragment;->setSelection(I)V

    return-void
.end method

.method static synthetic access$300(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/HeadersFragment;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    return-object v0
.end method

.method static synthetic access$400(Landroid/support/v17/leanback/app/BrowseFragment;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->createHeadersTransition()V

    return-void
.end method

.method static synthetic access$500(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/BrowseFragment$BrowseTransitionListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrowseTransitionListener:Landroid/support/v17/leanback/app/BrowseFragment$BrowseTransitionListener;

    return-object v0
.end method

.method static synthetic access$600(Landroid/support/v17/leanback/app/BrowseFragment;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSceneWithHeaders:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$700(Landroid/support/v17/leanback/app/BrowseFragment;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSceneWithoutHeaders:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$800(Landroid/support/v17/leanback/app/BrowseFragment;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransition:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$802(Landroid/support/v17/leanback/app/BrowseFragment;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseFragment;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 71
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransition:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$900()Landroid/support/v17/leanback/transition/TransitionHelper;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    return-object v0
.end method

.method private createHeadersTransition()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 735
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v5, v8}, Landroid/support/v17/leanback/transition/TransitionHelper;->createTransitionSet(Z)Ljava/lang/Object;

    move-result-object v5

    iput-object v5, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransition:Ljava/lang/Object;

    .line 736
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v6, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransition:Ljava/lang/Object;

    sget v7, Landroid/support/v17/leanback/R$id;->browse_title_group:I

    invoke-virtual {v5, v6, v7, v9}, Landroid/support/v17/leanback/transition/TransitionHelper;->excludeChildren(Ljava/lang/Object;IZ)V

    .line 737
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v5, v8}, Landroid/support/v17/leanback/transition/TransitionHelper;->createChangeBounds(Z)Ljava/lang/Object;

    move-result-object v0

    .line 738
    .local v0, "changeBounds":Ljava/lang/Object;
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v5, v9}, Landroid/support/v17/leanback/transition/TransitionHelper;->createFadeTransition(I)Ljava/lang/Object;

    move-result-object v2

    .line 739
    .local v2, "fadeIn":Ljava/lang/Object;
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Landroid/support/v17/leanback/transition/TransitionHelper;->createFadeTransition(I)Ljava/lang/Object;

    move-result-object v3

    .line 740
    .local v3, "fadeOut":Ljava/lang/Object;
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v5}, Landroid/support/v17/leanback/transition/TransitionHelper;->createScale()Ljava/lang/Object;

    move-result-object v4

    .line 741
    .local v4, "scale":Ljava/lang/Object;
    invoke-static {}, Landroid/support/v17/leanback/transition/TransitionHelper;->systemSupportsTransitions()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 742
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->getView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 743
    .local v1, "context":Landroid/content/Context;
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    sget-object v6, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v6, v1}, Landroid/support/v17/leanback/transition/TransitionHelper;->createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Landroid/support/v17/leanback/transition/TransitionHelper;->setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 745
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    sget-object v6, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v6, v1}, Landroid/support/v17/leanback/transition/TransitionHelper;->createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Landroid/support/v17/leanback/transition/TransitionHelper;->setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 747
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    sget-object v6, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v6, v1}, Landroid/support/v17/leanback/transition/TransitionHelper;->createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/support/v17/leanback/transition/TransitionHelper;->setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 749
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    sget-object v6, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v6, v1}, Landroid/support/v17/leanback/transition/TransitionHelper;->createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/support/v17/leanback/transition/TransitionHelper;->setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 753
    .end local v1    # "context":Landroid/content/Context;
    :cond_0
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget v6, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransitionDuration:I

    int-to-long v6, v6

    invoke-virtual {v5, v3, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setDuration(Ljava/lang/Object;J)V

    .line 754
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v6, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransition:Ljava/lang/Object;

    invoke-virtual {v5, v6, v3}, Landroid/support/v17/leanback/transition/TransitionHelper;->addTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 756
    iget-boolean v5, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    if-eqz v5, :cond_1

    .line 757
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget v6, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransitionStartDelay:I

    int-to-long v6, v6

    invoke-virtual {v5, v0, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setStartDelay(Ljava/lang/Object;J)V

    .line 758
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget v6, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransitionStartDelay:I

    int-to-long v6, v6

    invoke-virtual {v5, v4, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setStartDelay(Ljava/lang/Object;J)V

    .line 760
    :cond_1
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget v6, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransitionDuration:I

    int-to-long v6, v6

    invoke-virtual {v5, v0, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setDuration(Ljava/lang/Object;J)V

    .line 761
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v6, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransition:Ljava/lang/Object;

    invoke-virtual {v5, v6, v0}, Landroid/support/v17/leanback/transition/TransitionHelper;->addTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 762
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v6, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v6}, Landroid/support/v17/leanback/app/RowsFragment;->getScaleFrameLayout()Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/support/v17/leanback/transition/TransitionHelper;->addTarget(Ljava/lang/Object;Landroid/view/View;)V

    .line 763
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget v6, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransitionDuration:I

    int-to-long v6, v6

    invoke-virtual {v5, v4, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setDuration(Ljava/lang/Object;J)V

    .line 764
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v6, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransition:Ljava/lang/Object;

    invoke-virtual {v5, v6, v4}, Landroid/support/v17/leanback/transition/TransitionHelper;->addTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 766
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget v6, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransitionDuration:I

    int-to-long v6, v6

    invoke-virtual {v5, v2, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setDuration(Ljava/lang/Object;J)V

    .line 767
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget v6, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransitionStartDelay:I

    int-to-long v6, v6

    invoke-virtual {v5, v2, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setStartDelay(Ljava/lang/Object;J)V

    .line 768
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v6, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransition:Ljava/lang/Object;

    invoke-virtual {v5, v6, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->addTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 770
    sget-object v5, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v6, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransition:Ljava/lang/Object;

    new-instance v7, Landroid/support/v17/leanback/app/BrowseFragment$8;

    invoke-direct {v7, p0}, Landroid/support/v17/leanback/app/BrowseFragment$8;-><init>(Landroid/support/v17/leanback/app/BrowseFragment;)V

    invoke-virtual {v5, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setTransitionListener(Ljava/lang/Object;Landroid/support/v17/leanback/transition/TransitionListener;)V

    .line 795
    return-void
.end method

.method private isVerticalScrolling()Z
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/HeadersFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->getScrollState()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->getScrollState()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onRowSelected(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 874
    iget v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSelectedPosition:I

    if-eq p1, v0, :cond_1

    .line 875
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSetSelectionRunnable:Landroid/support/v17/leanback/app/BrowseFragment$SetSelectionRunnable;

    iput p1, v0, Landroid/support/v17/leanback/app/BrowseFragment$SetSelectionRunnable;->mPosition:I

    .line 876
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/BrowseFrameLayout;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSetSelectionRunnable:Landroid/support/v17/leanback/app/BrowseFragment$SetSelectionRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 878
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_2

    .line 879
    :cond_0
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingTitle:Z

    if-nez v0, :cond_1

    .line 880
    sget-object v0, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSceneWithTitle:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleDownTransition:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->runTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 881
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingTitle:Z

    .line 888
    :cond_1
    :goto_0
    return-void

    .line 883
    :cond_2
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingTitle:Z

    if-eqz v0, :cond_1

    .line 884
    sget-object v0, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSceneWithoutTitle:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleUpTransition:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->runTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 885
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingTitle:Z

    goto :goto_0
.end method

.method private readArguments(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "args"    # Landroid/os/Bundle;

    .prologue
    .line 953
    if-nez p1, :cond_1

    .line 962
    :cond_0
    :goto_0
    return-void

    .line 956
    :cond_1
    sget-object v0, Landroid/support/v17/leanback/app/BrowseFragment;->ARG_TITLE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 957
    sget-object v0, Landroid/support/v17/leanback/app/BrowseFragment;->ARG_TITLE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/app/BrowseFragment;->setTitle(Ljava/lang/String;)V

    .line 959
    :cond_2
    sget-object v0, Landroid/support/v17/leanback/app/BrowseFragment;->ARG_HEADERS_STATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 960
    sget-object v0, Landroid/support/v17/leanback/app/BrowseFragment;->ARG_HEADERS_STATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/app/BrowseFragment;->setHeadersState(I)V

    goto :goto_0
.end method

.method private setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 901
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 902
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/app/RowsFragment;->setSelectedPosition(I)V

    .line 903
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/app/HeadersFragment;->setSelectedPosition(I)V

    .line 905
    :cond_0
    iput p1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSelectedPosition:I

    .line 906
    return-void
.end method

.method private showHeaders(Z)V
    .locals 6
    .param p1, "show"    # Z

    .prologue
    const/4 v3, 0x0

    .line 811
    sget-boolean v2, Landroid/support/v17/leanback/app/BrowseFragment;->DEBUG:Z

    if-eqz v2, :cond_0

    const-string v2, "BrowseFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "showHeaders "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 812
    :cond_0
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    invoke-virtual {v2, p1}, Landroid/support/v17/leanback/app/HeadersFragment;->setHeadersEnabled(Z)V

    .line 816
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/RowsFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 817
    .local v0, "containerList":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 818
    .local v1, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    if-eqz p1, :cond_2

    iget v2, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mContainerListMarginLeft:I

    :goto_0
    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 819
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 821
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/HeadersFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 822
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .end local v1    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 823
    .restart local v1    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    if-eqz p1, :cond_3

    move v2, v3

    :goto_1
    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 824
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 826
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    if-nez p1, :cond_1

    const/4 v3, 0x1

    :cond_1
    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/app/RowsFragment;->setExpand(Z)V

    .line 827
    return-void

    :cond_2
    move v2, v3

    .line 818
    goto :goto_0

    .line 823
    :cond_3
    iget v2, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mContainerListMarginLeft:I

    neg-int v2, v2

    goto :goto_1
.end method

.method private startHeadersTransitionInternal(Z)V
    .locals 3
    .param p1, "withHeaders"    # Z

    .prologue
    .line 483
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 512
    :goto_0
    return-void

    .line 486
    :cond_0
    iput-boolean p1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    .line 487
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    new-instance v2, Landroid/support/v17/leanback/app/BrowseFragment$1;

    invoke-direct {v2, p0, p1}, Landroid/support/v17/leanback/app/BrowseFragment$1;-><init>(Landroid/support/v17/leanback/app/BrowseFragment;Z)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v17/leanback/app/RowsFragment;->onExpandTransitionStart(ZLjava/lang/Runnable;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    return-object v0
.end method

.method public getHeadersState()I
    .locals 1

    .prologue
    .line 1046
    iget v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersState:I

    return v0
.end method

.method public isInHeadersTransition()Z
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransition:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowingHeaders()Z
    .locals 1

    .prologue
    .line 456
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 604
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 605
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Landroid/support/v17/leanback/R$styleable;->LeanbackTheme:[I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 606
    .local v0, "ta":Landroid/content/res/TypedArray;
    sget v1, Landroid/support/v17/leanback/R$styleable;->LeanbackTheme_browseRowsMarginStart:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mContainerListMarginLeft:I

    .line 608
    sget v1, Landroid/support/v17/leanback/R$styleable;->LeanbackTheme_browseRowsMarginTop:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mContainerListAlignTop:I

    .line 610
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 612
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Landroid/support/v17/leanback/R$integer;->lb_browse_headers_transition_delay:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransitionStartDelay:I

    .line 614
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Landroid/support/v17/leanback/R$integer;->lb_browse_headers_transition_duration:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransitionDuration:I

    .line 617
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v1}, Landroid/support/v17/leanback/app/BrowseFragment;->readArguments(Landroid/os/Bundle;)V

    .line 619
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z

    if-eqz v1, :cond_0

    .line 620
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersBackStackEnabled:Z

    if-eqz v1, :cond_1

    .line 621
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lbHeadersBackStack_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mWithHeadersBackStackName:Ljava/lang/String;

    .line 622
    new-instance v1, Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;

    invoke-direct {v1, p0}, Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;-><init>(Landroid/support/v17/leanback/app/BrowseFragment;)V

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;

    .line 623
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 624
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;

    invoke-virtual {v1, p1}, Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;->load(Landroid/os/Bundle;)V

    .line 632
    :cond_0
    :goto_0
    return-void

    .line 626
    :cond_1
    if-eqz p1, :cond_0

    .line 627
    const-string v1, "headerShow"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 645
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sget v4, Landroid/support/v17/leanback/R$id;->browse_container_dock:I

    invoke-virtual {v1, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_5

    .line 646
    new-instance v1, Landroid/support/v17/leanback/app/RowsFragment;

    invoke-direct {v1}, Landroid/support/v17/leanback/app/RowsFragment;-><init>()V

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    .line 647
    new-instance v1, Landroid/support/v17/leanback/app/HeadersFragment;

    invoke-direct {v1}, Landroid/support/v17/leanback/app/HeadersFragment;-><init>()V

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    .line 648
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    sget v4, Landroid/support/v17/leanback/R$id;->browse_headers_dock:I

    iget-object v5, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    invoke-virtual {v1, v4, v5}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    sget v4, Landroid/support/v17/leanback/R$id;->browse_container_dock:I

    iget-object v5, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v1, v4, v5}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 658
    :goto_0
    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z

    if-nez v1, :cond_6

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/support/v17/leanback/app/HeadersFragment;->setHeadersGone(Z)V

    .line 660
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/RowsFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 661
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeaderPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    if-eqz v1, :cond_0

    .line 662
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeaderPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/HeadersFragment;->setPresenterSelector(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 664
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/HeadersFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 666
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    iget-boolean v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowScaleEnabled:Z

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/RowsFragment;->enableRowScaling(Z)V

    .line 667
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/RowsFragment;->setOnItemSelectedListener(Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V

    .line 668
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/RowsFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 669
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeaderSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/HeadersFragment;->setOnItemSelectedListener(Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V

    .line 670
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeaderClickedListener:Landroid/support/v17/leanback/app/HeadersFragment$OnHeaderClickedListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/HeadersFragment;->setOnHeaderClickedListener(Landroid/support/v17/leanback/app/HeadersFragment$OnHeaderClickedListener;)V

    .line 671
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/RowsFragment;->setOnItemClickedListener(Landroid/support/v17/leanback/widget/OnItemClickedListener;)V

    .line 672
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/RowsFragment;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 674
    sget v1, Landroid/support/v17/leanback/R$layout;->lb_browse_fragment:I

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 676
    .local v0, "root":Landroid/view/View;
    sget v1, Landroid/support/v17/leanback/R$id;->browse_frame:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    .line 677
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mOnFocusSearchListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnFocusSearchListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/BrowseFrameLayout;->setOnFocusSearchListener(Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnFocusSearchListener;)V

    .line 678
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mOnChildFocusListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnChildFocusListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/BrowseFrameLayout;->setOnChildFocusListener(Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnChildFocusListener;)V

    .line 680
    sget v1, Landroid/support/v17/leanback/R$id;->browse_title_group:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/TitleView;

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    .line 681
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/TitleView;->setTitle(Ljava/lang/String;)V

    .line 682
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBadgeDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/TitleView;->setBadgeDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 683
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSearchAffordanceColorSet:Z

    if-eqz v1, :cond_1

    .line 684
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSearchAffordanceColors:Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/TitleView;->setSearchAffordanceColors(Landroid/support/v17/leanback/widget/SearchOrbView$Colors;)V

    .line 686
    :cond_1
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mExternalOnSearchClickedListener:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_2

    .line 687
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mExternalOnSearchClickedListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/TitleView;->setOnSearchClickedListener(Landroid/view/View$OnClickListener;)V

    .line 690
    :cond_2
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrandColorSet:Z

    if-eqz v1, :cond_3

    .line 691
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    iget v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrandColor:I

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/HeadersFragment;->setBackgroundColor(I)V

    .line 694
    :cond_3
    sget-object v1, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    new-instance v5, Landroid/support/v17/leanback/app/BrowseFragment$4;

    invoke-direct {v5, p0}, Landroid/support/v17/leanback/app/BrowseFragment$4;-><init>(Landroid/support/v17/leanback/app/BrowseFragment;)V

    invoke-virtual {v1, v4, v5}, Landroid/support/v17/leanback/transition/TransitionHelper;->createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSceneWithTitle:Ljava/lang/Object;

    .line 700
    sget-object v1, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    new-instance v5, Landroid/support/v17/leanback/app/BrowseFragment$5;

    invoke-direct {v5, p0}, Landroid/support/v17/leanback/app/BrowseFragment$5;-><init>(Landroid/support/v17/leanback/app/BrowseFragment;)V

    invoke-virtual {v1, v4, v5}, Landroid/support/v17/leanback/transition/TransitionHelper;->createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSceneWithoutTitle:Ljava/lang/Object;

    .line 706
    sget-object v1, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    new-instance v5, Landroid/support/v17/leanback/app/BrowseFragment$6;

    invoke-direct {v5, p0}, Landroid/support/v17/leanback/app/BrowseFragment$6;-><init>(Landroid/support/v17/leanback/app/BrowseFragment;)V

    invoke-virtual {v1, v4, v5}, Landroid/support/v17/leanback/transition/TransitionHelper;->createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSceneWithHeaders:Ljava/lang/Object;

    .line 712
    sget-object v1, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    new-instance v5, Landroid/support/v17/leanback/app/BrowseFragment$7;

    invoke-direct {v5, p0}, Landroid/support/v17/leanback/app/BrowseFragment$7;-><init>(Landroid/support/v17/leanback/app/BrowseFragment;)V

    invoke-virtual {v1, v4, v5}, Landroid/support/v17/leanback/transition/TransitionHelper;->createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSceneWithoutHeaders:Ljava/lang/Object;

    .line 718
    sget-object v1, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-static {v1}, Landroid/support/v17/leanback/app/TitleTransitionHelper;->createTransitionTitleUp(Landroid/support/v17/leanback/transition/TransitionHelper;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleUpTransition:Ljava/lang/Object;

    .line 719
    sget-object v1, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-static {v1}, Landroid/support/v17/leanback/app/TitleTransitionHelper;->createTransitionTitleDown(Landroid/support/v17/leanback/transition/TransitionHelper;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleDownTransition:Ljava/lang/Object;

    .line 721
    sget-object v1, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleUpTransition:Ljava/lang/Object;

    sget v5, Landroid/support/v17/leanback/R$id;->browse_headers:I

    invoke-virtual {v1, v4, v5, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->excludeChildren(Ljava/lang/Object;IZ)V

    .line 722
    sget-object v1, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleDownTransition:Ljava/lang/Object;

    sget v5, Landroid/support/v17/leanback/R$id;->browse_headers:I

    invoke-virtual {v1, v4, v5, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->excludeChildren(Ljava/lang/Object;IZ)V

    .line 723
    sget-object v1, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleUpTransition:Ljava/lang/Object;

    sget v5, Landroid/support/v17/leanback/R$id;->container_list:I

    invoke-virtual {v1, v4, v5, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->excludeChildren(Ljava/lang/Object;IZ)V

    .line 724
    sget-object v1, Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleDownTransition:Ljava/lang/Object;

    sget v5, Landroid/support/v17/leanback/R$id;->container_list:I

    invoke-virtual {v1, v4, v5, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->excludeChildren(Ljava/lang/Object;IZ)V

    .line 726
    if-eqz p3, :cond_4

    .line 727
    const-string v1, "titleShow"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingTitle:Z

    .line 729
    :cond_4
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-boolean v2, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingTitle:Z

    if-eqz v2, :cond_7

    :goto_2
    invoke-virtual {v1, v3}, Landroid/support/v17/leanback/widget/TitleView;->setVisibility(I)V

    .line 731
    return-object v0

    .line 652
    .end local v0    # "root":Landroid/view/View;
    :cond_5
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sget v4, Landroid/support/v17/leanback/R$id;->browse_headers_dock:I

    invoke-virtual {v1, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/app/HeadersFragment;

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    .line 654
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sget v4, Landroid/support/v17/leanback/R$id;->browse_container_dock:I

    invoke-virtual {v1, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/app/RowsFragment;

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    goto/16 :goto_0

    :cond_6
    move v1, v3

    .line 658
    goto/16 :goto_1

    .line 729
    .restart local v0    # "root":Landroid/view/View;
    :cond_7
    const/4 v3, 0x4

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 636
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;

    if-eqz v0, :cond_0

    .line 637
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->removeOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 639
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 640
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 594
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;

    if-eqz v0, :cond_0

    .line 595
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;->save(Landroid/os/Bundle;)V

    .line 599
    :goto_0
    const-string v0, "titleShow"

    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingTitle:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 600
    return-void

    .line 597
    :cond_0
    const-string v0, "headerShow"

    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 910
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 911
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    iget v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mContainerListAlignTop:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/HeadersFragment;->setWindowAlignmentFromTop(I)V

    .line 912
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/HeadersFragment;->setItemAlignment()V

    .line 913
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    iget v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mContainerListAlignTop:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/RowsFragment;->setWindowAlignmentFromTop(I)V

    .line 914
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsFragment;->setItemAlignment()V

    .line 916
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsFragment;->getScaleFrameLayout()Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ScaleFrameLayout;->setPivotX(F)V

    .line 917
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsFragment;->getScaleFrameLayout()Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    move-result-object v0

    iget v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mContainerListAlignTop:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ScaleFrameLayout;->setPivotY(F)V

    .line 919
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/HeadersFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 920
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/HeadersFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 925
    :cond_0
    :goto_0
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z

    if-eqz v0, :cond_1

    .line 926
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/app/BrowseFragment;->showHeaders(Z)V

    .line 928
    :cond_1
    return-void

    .line 921
    :cond_2
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    if-nez v0, :cond_0

    :cond_3
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 923
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0
.end method

.method public setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V
    .locals 1
    .param p1, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 279
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 280
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/app/RowsFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 282
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/app/HeadersFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 284
    :cond_0
    return-void
.end method

.method public setBadgeDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 970
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBadgeDrawable:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    .line 971
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBadgeDrawable:Landroid/graphics/drawable/Drawable;

    .line 972
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    if-eqz v0, :cond_0

    .line 973
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/TitleView;->setBadgeDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 976
    :cond_0
    return-void
.end method

.method public setBrandColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 252
    iput p1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrandColor:I

    .line 253
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrandColorSet:Z

    .line 255
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    iget v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mBrandColor:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/HeadersFragment;->setBackgroundColor(I)V

    .line 258
    :cond_0
    return-void
.end method

.method public setHeadersState(I)V
    .locals 5
    .param p1, "headersState"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1012
    if-lt p1, v0, :cond_0

    const/4 v2, 0x3

    if-le p1, v2, :cond_1

    .line 1013
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid headers state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1015
    :cond_1
    sget-boolean v2, Landroid/support/v17/leanback/app/BrowseFragment;->DEBUG:Z

    if-eqz v2, :cond_2

    const-string v2, "BrowseFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setHeadersState "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1017
    :cond_2
    iget v2, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersState:I

    if-eq p1, v2, :cond_3

    .line 1018
    iput p1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersState:I

    .line 1019
    packed-switch p1, :pswitch_data_0

    .line 1033
    const-string v2, "BrowseFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown headers state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1036
    :goto_0
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    if-eqz v2, :cond_3

    .line 1037
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;

    iget-boolean v3, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z

    if-nez v3, :cond_4

    :goto_1
    invoke-virtual {v2, v0}, Landroid/support/v17/leanback/app/HeadersFragment;->setHeadersGone(Z)V

    .line 1040
    :cond_3
    return-void

    .line 1021
    :pswitch_0
    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z

    .line 1022
    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    goto :goto_0

    .line 1025
    :pswitch_1
    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z

    .line 1026
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    goto :goto_0

    .line 1029
    :pswitch_2
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z

    .line 1030
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1037
    goto :goto_1

    .line 1019
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .prologue
    .line 352
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .line 353
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/app/RowsFragment;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 356
    :cond_0
    return-void
.end method

.method public setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .prologue
    .line 308
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mExternalOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .line 309
    return-void
.end method

.method public setOnSearchClickedListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 378
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mExternalOnSearchClickedListener:Landroid/view/View$OnClickListener;

    .line 379
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/TitleView;->setOnSearchClickedListener(Landroid/view/View$OnClickListener;)V

    .line 382
    :cond_0
    return-void
.end method

.method public setSearchAffordanceColors(Landroid/support/v17/leanback/widget/SearchOrbView$Colors;)V
    .locals 2
    .param p1, "colors"    # Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

    .prologue
    .line 388
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSearchAffordanceColors:Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

    .line 389
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSearchAffordanceColorSet:Z

    .line 390
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mSearchAffordanceColors:Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/TitleView;->setSearchAffordanceColors(Landroid/support/v17/leanback/widget/SearchOrbView$Colors;)V

    .line 393
    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 991
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitle:Ljava/lang/String;

    .line 992
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    if-eqz v0, :cond_0

    .line 993
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/TitleView;->setTitle(Ljava/lang/String;)V

    .line 995
    :cond_0
    return-void
.end method

.method public startHeadersTransition(Z)V
    .locals 2
    .param p1, "withHeaders"    # Z

    .prologue
    .line 436
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mCanShowHeaders:Z

    if-nez v0, :cond_0

    .line 437
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot start headers transition"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 439
    :cond_0
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseFragment;->isInHeadersTransition()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseFragment;->mShowingHeaders:Z

    if-ne v0, p1, :cond_2

    .line 443
    :cond_1
    :goto_0
    return-void

    .line 442
    :cond_2
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/BrowseFragment;->startHeadersTransitionInternal(Z)V

    goto :goto_0
.end method
