.class public abstract Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;
.super Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;
.source "PlaybackControlsRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/PlaybackControlsRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ThumbsAction"
.end annotation


# static fields
.field public static OUTLINE:I

.field public static SOLID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 242
    const/4 v0, 0x0

    sput v0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;->SOLID:I

    .line 247
    const/4 v0, 0x1

    sput v0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;->OUTLINE:I

    return-void
.end method

.method public constructor <init>(ILandroid/content/Context;II)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "solidIconIndex"    # I
    .param p4, "outlineIconIndex"    # I

    .prologue
    .line 254
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;-><init>(I)V

    .line 255
    const/4 v1, 0x2

    new-array v0, v1, [Landroid/graphics/drawable/Drawable;

    .line 256
    .local v0, "drawables":[Landroid/graphics/drawable/Drawable;
    sget v1, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;->SOLID:I

    # invokes: Landroid/support/v17/leanback/widget/PlaybackControlsRow;->getStyledDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    invoke-static {p2, p3}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->access$000(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 257
    sget v1, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;->OUTLINE:I

    # invokes: Landroid/support/v17/leanback/widget/PlaybackControlsRow;->getStyledDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    invoke-static {p2, p4}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->access$000(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 258
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;->setDrawables([Landroid/graphics/drawable/Drawable;)V

    .line 259
    return-void
.end method
