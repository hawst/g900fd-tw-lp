.class public Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector;
.super Landroid/support/v17/leanback/widget/PresenterSelector;
.source "ControlButtonPresenterSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector$ControlButtonPresenter;,
        Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector$ActionViewHolder;
    }
.end annotation


# instance fields
.field private final mPrimaryPresenter:Landroid/support/v17/leanback/widget/Presenter;

.field private final mSecondaryPresenter:Landroid/support/v17/leanback/widget/Presenter;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/PresenterSelector;-><init>()V

    .line 32
    new-instance v0, Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector$ControlButtonPresenter;

    sget v1, Landroid/support/v17/leanback/R$layout;->lb_control_button_primary:I

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector$ControlButtonPresenter;-><init>(I)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector;->mPrimaryPresenter:Landroid/support/v17/leanback/widget/Presenter;

    .line 34
    new-instance v0, Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector$ControlButtonPresenter;

    sget v1, Landroid/support/v17/leanback/R$layout;->lb_control_button_secondary:I

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector$ControlButtonPresenter;-><init>(I)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector;->mSecondaryPresenter:Landroid/support/v17/leanback/widget/Presenter;

    .line 69
    return-void
.end method


# virtual methods
.method public getPresenter(Ljava/lang/Object;)Landroid/support/v17/leanback/widget/Presenter;
    .locals 1
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 55
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector;->mPrimaryPresenter:Landroid/support/v17/leanback/widget/Presenter;

    return-object v0
.end method

.method public getPrimaryPresenter()Landroid/support/v17/leanback/widget/Presenter;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector;->mPrimaryPresenter:Landroid/support/v17/leanback/widget/Presenter;

    return-object v0
.end method

.method public getSecondaryPresenter()Landroid/support/v17/leanback/widget/Presenter;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector;->mSecondaryPresenter:Landroid/support/v17/leanback/widget/Presenter;

    return-object v0
.end method
