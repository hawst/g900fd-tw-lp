.class Landroid/support/v17/leanback/widget/VerticalGridPresenter$1;
.super Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;
.source "VerticalGridPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/VerticalGridPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/widget/VerticalGridPresenter;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/VerticalGridPresenter;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$1;->this$0:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;-><init>()V

    return-void
.end method


# virtual methods
.method public createWrapper(Landroid/view/View;)Landroid/view/View;
    .locals 4
    .param p1, "root"    # Landroid/view/View;

    .prologue
    const/4 v2, -0x2

    .line 160
    new-instance v0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;-><init>(Landroid/content/Context;)V

    .line 161
    .local v0, "wrapper":Landroid/support/v17/leanback/widget/ShadowOverlayContainer;
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    iget-object v1, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$1;->this$0:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->needsDefaultShadow()Z

    move-result v1

    const/4 v2, 0x1

    iget-object v3, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$1;->this$0:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    invoke-virtual {v3}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->areChildRoundedCornersEnabled()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->initialize(ZZZ)V

    .line 164
    return-object v0
.end method

.method public wrap(Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p1, "wrapper"    # Landroid/view/View;
    .param p2, "wrapped"    # Landroid/view/View;

    .prologue
    .line 168
    check-cast p1, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;

    .end local p1    # "wrapper":Landroid/view/View;
    invoke-virtual {p1, p2}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->wrap(Landroid/view/View;)V

    .line 169
    return-void
.end method
