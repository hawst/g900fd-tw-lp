.class Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight$HeaderFocusAnimator;
.super Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;
.source "FocusHighlightHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "HeaderFocusAnimator"
.end annotation


# instance fields
.field mViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

.field final synthetic this$0:Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;Landroid/view/View;FI)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "scale"    # F
    .param p4, "duration"    # I

    .prologue
    .line 212
    iput-object p1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight$HeaderFocusAnimator;->this$0:Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;

    .line 213
    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0, p4}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;-><init>(Landroid/view/View;FZI)V

    .line 214
    # getter for: Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;->mGridView:Landroid/support/v17/leanback/widget/BaseGridView;
    invoke-static {p1}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;->access$000(Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;)Landroid/support/v17/leanback/widget/BaseGridView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v17/leanback/widget/BaseGridView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    iput-object v0, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight$HeaderFocusAnimator;->mViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 215
    return-void
.end method


# virtual methods
.method setFocusLevel(F)V
    .locals 2
    .param p1, "level"    # F

    .prologue
    .line 219
    iget-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight$HeaderFocusAnimator;->mViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v0

    .line 220
    .local v0, "presenter":Landroid/support/v17/leanback/widget/Presenter;
    instance-of v1, v0, Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    if-eqz v1, :cond_0

    .line 221
    check-cast v0, Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    .end local v0    # "presenter":Landroid/support/v17/leanback/widget/Presenter;
    iget-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight$HeaderFocusAnimator;->mViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    invoke-virtual {v0, v1, p1}, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->setSelectLevel(Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;F)V

    .line 224
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->setFocusLevel(F)V

    .line 225
    return-void
.end method
