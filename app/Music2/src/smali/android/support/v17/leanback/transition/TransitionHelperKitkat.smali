.class final Landroid/support/v17/leanback/transition/TransitionHelperKitkat;
.super Ljava/lang/Object;
.source "TransitionHelperKitkat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;
    }
.end annotation


# direct methods
.method static addTarget(Ljava/lang/Object;Landroid/view/View;)V
    .locals 0
    .param p0, "transition"    # Ljava/lang/Object;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 224
    check-cast p0, Landroid/transition/Transition;

    .end local p0    # "transition":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 225
    return-void
.end method

.method static addTransition(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p0, "transitionSet"    # Ljava/lang/Object;
    .param p1, "transition"    # Ljava/lang/Object;

    .prologue
    .line 54
    check-cast p0, Landroid/transition/TransitionSet;

    .end local p0    # "transitionSet":Ljava/lang/Object;
    check-cast p1, Landroid/transition/Transition;

    .end local p1    # "transition":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 55
    return-void
.end method

.method static createChangeBounds(Z)Ljava/lang/Object;
    .locals 1
    .param p0, "reparent"    # Z

    .prologue
    .line 134
    new-instance v0, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;

    invoke-direct {v0}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;-><init>()V

    .line 135
    .local v0, "changeBounds":Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;
    invoke-virtual {v0, p0}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;->setReparent(Z)V

    .line 136
    return-object v0
.end method

.method static createFadeTransition(I)Ljava/lang/Object;
    .locals 1
    .param p0, "fadingMode"    # I

    .prologue
    .line 73
    new-instance v0, Landroid/transition/Fade;

    invoke-direct {v0, p0}, Landroid/transition/Fade;-><init>(I)V

    .line 74
    .local v0, "fade":Landroid/transition/Fade;
    return-object v0
.end method

.method static createScale()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Landroid/support/v17/leanback/transition/Scale;

    invoke-direct {v0}, Landroid/support/v17/leanback/transition/Scale;-><init>()V

    .line 69
    .local v0, "scale":Landroid/support/v17/leanback/transition/Scale;
    return-object v0
.end method

.method static createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;
    .locals 1
    .param p0, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p1, "enterAction"    # Ljava/lang/Runnable;

    .prologue
    .line 41
    new-instance v0, Landroid/transition/Scene;

    invoke-direct {v0, p0}, Landroid/transition/Scene;-><init>(Landroid/view/ViewGroup;)V

    .line 42
    .local v0, "scene":Landroid/transition/Scene;
    invoke-virtual {v0, p1}, Landroid/transition/Scene;->setEnterAction(Ljava/lang/Runnable;)V

    .line 43
    return-object v0
.end method

.method static createSlide(Landroid/support/v17/leanback/transition/SlideCallback;)Ljava/lang/Object;
    .locals 1
    .param p0, "callback"    # Landroid/support/v17/leanback/transition/SlideCallback;

    .prologue
    .line 62
    new-instance v0, Landroid/support/v17/leanback/transition/Slide;

    invoke-direct {v0}, Landroid/support/v17/leanback/transition/Slide;-><init>()V

    .line 63
    .local v0, "slide":Landroid/support/v17/leanback/transition/Slide;
    invoke-virtual {v0, p0}, Landroid/support/v17/leanback/transition/Slide;->setCallback(Landroid/support/v17/leanback/transition/SlideCallback;)V

    .line 64
    return-object v0
.end method

.method static createTransitionSet(Z)Ljava/lang/Object;
    .locals 2
    .param p0, "sequential"    # Z

    .prologue
    .line 47
    new-instance v0, Landroid/transition/TransitionSet;

    invoke-direct {v0}, Landroid/transition/TransitionSet;-><init>()V

    .line 48
    .local v0, "set":Landroid/transition/TransitionSet;
    if-eqz p0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->setOrdering(I)Landroid/transition/TransitionSet;

    .line 50
    return-object v0

    .line 48
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static excludeChildren(Ljava/lang/Object;IZ)V
    .locals 0
    .param p0, "transition"    # Ljava/lang/Object;
    .param p1, "targetId"    # I
    .param p2, "exclude"    # Z

    .prologue
    .line 172
    check-cast p0, Landroid/transition/Transition;

    .end local p0    # "transition":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Landroid/transition/Transition;->excludeChildren(IZ)Landroid/transition/Transition;

    .line 173
    return-void
.end method

.method static runTransition(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p0, "scene"    # Ljava/lang/Object;
    .param p1, "transition"    # Ljava/lang/Object;

    .prologue
    .line 216
    check-cast p0, Landroid/transition/Scene;

    .end local p0    # "scene":Ljava/lang/Object;
    check-cast p1, Landroid/transition/Transition;

    .end local p1    # "transition":Ljava/lang/Object;
    invoke-static {p0, p1}, Landroid/transition/TransitionManager;->go(Landroid/transition/Scene;Landroid/transition/Transition;)V

    .line 217
    return-void
.end method

.method static setDuration(Ljava/lang/Object;J)V
    .locals 1
    .param p0, "transition"    # Ljava/lang/Object;
    .param p1, "duration"    # J

    .prologue
    .line 160
    check-cast p0, Landroid/transition/Transition;

    .end local p0    # "transition":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    .line 161
    return-void
.end method

.method static setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p0, "transition"    # Ljava/lang/Object;
    .param p1, "timeInterpolator"    # Ljava/lang/Object;

    .prologue
    .line 220
    check-cast p0, Landroid/transition/Transition;

    .end local p0    # "transition":Ljava/lang/Object;
    check-cast p1, Landroid/animation/TimeInterpolator;

    .end local p1    # "timeInterpolator":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Landroid/transition/Transition;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 221
    return-void
.end method

.method static setStartDelay(Ljava/lang/Object;J)V
    .locals 1
    .param p0, "transition"    # Ljava/lang/Object;
    .param p1, "startDelay"    # J

    .prologue
    .line 156
    check-cast p0, Landroid/transition/Transition;

    .end local p0    # "transition":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Landroid/transition/Transition;->setStartDelay(J)Landroid/transition/Transition;

    .line 157
    return-void
.end method

.method static setTransitionListener(Ljava/lang/Object;Landroid/support/v17/leanback/transition/TransitionListener;)V
    .locals 2
    .param p0, "transition"    # Ljava/lang/Object;
    .param p1, "listener"    # Landroid/support/v17/leanback/transition/TransitionListener;

    .prologue
    .line 188
    move-object v0, p0

    check-cast v0, Landroid/transition/Transition;

    .line 189
    .local v0, "t":Landroid/transition/Transition;
    new-instance v1, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$1;

    invoke-direct {v1, p1}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$1;-><init>(Landroid/support/v17/leanback/transition/TransitionListener;)V

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 213
    return-void
.end method
