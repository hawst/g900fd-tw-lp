.class final Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;
.super Ljava/lang/Object;
.source "RowsFragment.java"

# interfaces
.implements Landroid/animation/TimeAnimator$TimeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/RowsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "RowViewHolderExtra"
.end annotation


# instance fields
.field final mRowPresenter:Landroid/support/v17/leanback/widget/RowPresenter;

.field final mRowViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

.field final mSelectAnimator:Landroid/animation/TimeAnimator;

.field mSelectAnimatorDurationInUse:I

.field mSelectAnimatorInterpolatorInUse:Landroid/view/animation/Interpolator;

.field mSelectLevelAnimDelta:F

.field mSelectLevelAnimStart:F

.field final synthetic this$0:Landroid/support/v17/leanback/app/RowsFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/RowsFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 1
    .param p2, "ibvh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 63
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Landroid/animation/TimeAnimator;

    invoke-direct {v0}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectAnimator:Landroid/animation/TimeAnimator;

    .line 64
    invoke-virtual {p2}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/RowPresenter;

    iput-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mRowPresenter:Landroid/support/v17/leanback/widget/RowPresenter;

    .line 65
    invoke-virtual {p2}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mRowViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .line 66
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    .line 67
    return-void
.end method


# virtual methods
.method animateSelect(ZZ)V
    .locals 3
    .param p1, "select"    # Z
    .param p2, "immediate"    # Z

    .prologue
    .line 92
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->endSelectAnimation()V

    .line 93
    if-eqz p1, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 94
    .local v0, "end":F
    :goto_0
    if-eqz p2, :cond_2

    .line 95
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mRowPresenter:Landroid/support/v17/leanback/widget/RowPresenter;

    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mRowViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-virtual {v1, v2, v0}, Landroid/support/v17/leanback/widget/RowPresenter;->setSelectLevel(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;F)V

    .line 103
    :cond_0
    :goto_1
    return-void

    .line 93
    .end local v0    # "end":F
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 96
    .restart local v0    # "end":F
    :cond_2
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mRowPresenter:Landroid/support/v17/leanback/widget/RowPresenter;

    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mRowViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/RowPresenter;->getSelectLevel(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)F

    move-result v1

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    iget v1, v1, Landroid/support/v17/leanback/app/RowsFragment;->mSelectAnimatorDuration:I

    iput v1, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectAnimatorDurationInUse:I

    .line 98
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    iget-object v1, v1, Landroid/support/v17/leanback/app/RowsFragment;->mSelectAnimatorInterpolator:Landroid/view/animation/Interpolator;

    iput-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectAnimatorInterpolatorInUse:Landroid/view/animation/Interpolator;

    .line 99
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mRowPresenter:Landroid/support/v17/leanback/widget/RowPresenter;

    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mRowViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/RowPresenter;->getSelectLevel(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)F

    move-result v1

    iput v1, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectLevelAnimStart:F

    .line 100
    iget v1, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectLevelAnimStart:F

    sub-float v1, v0, v1

    iput v1, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectLevelAnimDelta:F

    .line 101
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v1}, Landroid/animation/TimeAnimator;->start()V

    goto :goto_1
.end method

.method endAnimations()V
    .locals 0

    .prologue
    .line 106
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->endSelectAnimation()V

    .line 107
    return-void
.end method

.method endSelectAnimation()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->end()V

    .line 111
    return-void
.end method

.method public onTimeUpdate(Landroid/animation/TimeAnimator;JJ)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/TimeAnimator;
    .param p2, "totalTime"    # J
    .param p4, "deltaTime"    # J

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0, p2, p3, p4, p5}, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->updateSelect(JJ)V

    .line 74
    :cond_0
    return-void
.end method

.method updateSelect(JJ)V
    .locals 7
    .param p1, "totalTime"    # J
    .param p3, "deltaTime"    # J

    .prologue
    .line 78
    iget v2, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectAnimatorDurationInUse:I

    int-to-long v2, v2

    cmp-long v2, p1, v2

    if-ltz v2, :cond_1

    .line 79
    const/high16 v0, 0x3f800000    # 1.0f

    .line 80
    .local v0, "fraction":F
    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v2}, Landroid/animation/TimeAnimator;->end()V

    .line 84
    :goto_0
    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectAnimatorInterpolatorInUse:Landroid/view/animation/Interpolator;

    if-eqz v2, :cond_0

    .line 85
    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectAnimatorInterpolatorInUse:Landroid/view/animation/Interpolator;

    invoke-interface {v2, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    .line 87
    :cond_0
    iget v2, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectLevelAnimStart:F

    iget v3, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectLevelAnimDelta:F

    mul-float/2addr v3, v0

    add-float v1, v2, v3

    .line 88
    .local v1, "level":F
    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mRowPresenter:Landroid/support/v17/leanback/widget/RowPresenter;

    iget-object v3, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mRowViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-virtual {v2, v3, v1}, Landroid/support/v17/leanback/widget/RowPresenter;->setSelectLevel(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;F)V

    .line 89
    return-void

    .line 82
    .end local v0    # "fraction":F
    .end local v1    # "level":F
    :cond_1
    long-to-double v2, p1

    iget v4, p0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->mSelectAnimatorDurationInUse:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    double-to-float v0, v2

    .restart local v0    # "fraction":F
    goto :goto_0
.end method
