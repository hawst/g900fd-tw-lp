.class Landroid/support/v17/leanback/app/SearchFragment$3;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/SearchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/SearchFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/SearchFragment;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 146
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mProvider:Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;
    invoke-static {v2}, Landroid/support/v17/leanback/app/SearchFragment;->access$700(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;

    move-result-object v2

    invoke-interface {v2}, Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;->getResultsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    .line 147
    .local v0, "adapter":Landroid/support/v17/leanback/widget/ObjectAdapter;
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v2}, Landroid/support/v17/leanback/app/SearchFragment;->access$300(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    if-eq v0, v2, :cond_4

    .line 148
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v2}, Landroid/support/v17/leanback/app/SearchFragment;->access$300(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    if-nez v2, :cond_5

    const/4 v1, 0x1

    .line 149
    .local v1, "firstTime":Z
    :goto_0
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # invokes: Landroid/support/v17/leanback/app/SearchFragment;->releaseAdapter()V
    invoke-static {v2}, Landroid/support/v17/leanback/app/SearchFragment;->access$800(Landroid/support/v17/leanback/app/SearchFragment;)V

    .line 150
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # setter for: Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v2, v0}, Landroid/support/v17/leanback/app/SearchFragment;->access$302(Landroid/support/v17/leanback/app/SearchFragment;Landroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 151
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v2}, Landroid/support/v17/leanback/app/SearchFragment;->access$300(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 152
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v2}, Landroid/support/v17/leanback/app/SearchFragment;->access$300(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mAdapterObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;
    invoke-static {v3}, Landroid/support/v17/leanback/app/SearchFragment;->access$900(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 154
    :cond_0
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;
    invoke-static {v2}, Landroid/support/v17/leanback/app/SearchFragment;->access$200(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/app/RowsFragment;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 157
    if-eqz v1, :cond_1

    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v2}, Landroid/support/v17/leanback/app/SearchFragment;->access$300(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v2}, Landroid/support/v17/leanback/app/SearchFragment;->access$300(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v2

    if-eqz v2, :cond_2

    .line 158
    :cond_1
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;
    invoke-static {v2}, Landroid/support/v17/leanback/app/SearchFragment;->access$200(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/app/RowsFragment;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v3}, Landroid/support/v17/leanback/app/SearchFragment;->access$300(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/app/RowsFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 160
    :cond_2
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # invokes: Landroid/support/v17/leanback/app/SearchFragment;->executePendingQuery()V
    invoke-static {v2}, Landroid/support/v17/leanback/app/SearchFragment;->access$1000(Landroid/support/v17/leanback/app/SearchFragment;)V

    .line 162
    :cond_3
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment$3;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # invokes: Landroid/support/v17/leanback/app/SearchFragment;->updateSearchBarNextFocusId()V
    invoke-static {v2}, Landroid/support/v17/leanback/app/SearchFragment;->access$600(Landroid/support/v17/leanback/app/SearchFragment;)V

    .line 164
    .end local v1    # "firstTime":Z
    :cond_4
    return-void

    .line 148
    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method
