.class Landroid/support/v17/leanback/transition/Scale;
.super Landroid/transition/Transition;
.source "Scale.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 29
    return-void
.end method

.method private captureValues(Landroid/transition/TransitionValues;)V
    .locals 4
    .param p1, "values"    # Landroid/transition/TransitionValues;

    .prologue
    .line 32
    iget-object v0, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 33
    .local v0, "view":Landroid/view/View;
    iget-object v1, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "android:leanback:scale"

    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    return-void
.end method


# virtual methods
.method public captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/transition/Scale;->captureValues(Landroid/transition/TransitionValues;)V

    .line 44
    return-void
.end method

.method public captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/transition/Scale;->captureValues(Landroid/transition/TransitionValues;)V

    .line 39
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 6
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 49
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 50
    :cond_0
    const/4 v0, 0x0

    .line 69
    :goto_0
    return-object v0

    .line 53
    :cond_1
    iget-object v4, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v5, "android:leanback:scale"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 54
    .local v2, "startScale":F
    iget-object v4, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v5, "android:leanback:scale"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 56
    .local v1, "endScale":F
    iget-object v3, p2, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 57
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3, v2}, Landroid/view/View;->setScaleX(F)V

    .line 58
    invoke-virtual {v3, v2}, Landroid/view/View;->setScaleY(F)V

    .line 60
    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v2, v4, v5

    const/4 v5, 0x1

    aput v1, v4, v5

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 61
    .local v0, "animator":Landroid/animation/ValueAnimator;
    new-instance v4, Landroid/support/v17/leanback/transition/Scale$1;

    invoke-direct {v4, p0, v3}, Landroid/support/v17/leanback/transition/Scale$1;-><init>(Landroid/support/v17/leanback/transition/Scale;Landroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    goto :goto_0
.end method
