.class Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;
.super Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
.source "ControlBarPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/ControlBarPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewHolder"
.end annotation


# instance fields
.field mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field mControlBar:Landroid/support/v17/leanback/widget/ControlBar;

.field mData:Landroid/support/v17/leanback/widget/ControlBarPresenter$BoundData;

.field mDataObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

.field mPresenter:Landroid/support/v17/leanback/widget/Presenter;

.field mViewHolders:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v17/leanback/widget/Presenter$ViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Landroid/support/v17/leanback/widget/ControlBarPresenter;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/ControlBarPresenter;Landroid/view/View;)V
    .locals 2
    .param p2, "rootView"    # Landroid/view/View;

    .prologue
    .line 78
    iput-object p1, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->this$0:Landroid/support/v17/leanback/widget/ControlBarPresenter;

    .line 79
    invoke-direct {p0, p2}, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 71
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mViewHolders:Landroid/util/SparseArray;

    .line 80
    sget v0, Landroid/support/v17/leanback/R$id;->control_bar:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/ControlBar;

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mControlBar:Landroid/support/v17/leanback/widget/ControlBar;

    .line 81
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mControlBar:Landroid/support/v17/leanback/widget/ControlBar;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Couldn\'t find control_bar"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mControlBar:Landroid/support/v17/leanback/widget/ControlBar;

    new-instance v1, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder$1;

    invoke-direct {v1, p0, p1}, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder$1;-><init>(Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;Landroid/support/v17/leanback/widget/ControlBarPresenter;)V

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ControlBar;->setOnChildFocusedListener(Landroid/support/v17/leanback/widget/ControlBar$OnChildFocusedListener;)V

    .line 100
    new-instance v0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder$2;

    invoke-direct {v0, p0, p1}, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder$2;-><init>(Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;Landroid/support/v17/leanback/widget/ControlBarPresenter;)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mDataObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    .line 116
    return-void
.end method

.method private bindControlToAction(ILandroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Presenter;)V
    .locals 5
    .param p1, "position"    # I
    .param p2, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;
    .param p3, "presenter"    # Landroid/support/v17/leanback/widget/Presenter;

    .prologue
    .line 149
    iget-object v3, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mViewHolders:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .line 150
    .local v2, "vh":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    invoke-virtual {p2, p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 151
    .local v0, "item":Ljava/lang/Object;
    if-nez v2, :cond_0

    .line 152
    iget-object v3, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mControlBar:Landroid/support/v17/leanback/widget/ControlBar;

    invoke-virtual {p3, v3}, Landroid/support/v17/leanback/widget/Presenter;->onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v2

    .line 153
    iget-object v3, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mViewHolders:Landroid/util/SparseArray;

    invoke-virtual {v3, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 155
    move-object v1, v2

    .line 156
    .local v1, "itemViewHolder":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    new-instance v3, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder$3;

    invoke-direct {v3, p0, p1, v1}, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder$3;-><init>(Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;ILandroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    invoke-virtual {p3, v2, v3}, Landroid/support/v17/leanback/widget/Presenter;->setOnClickListener(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Landroid/view/View$OnClickListener;)V

    .line 167
    .end local v1    # "itemViewHolder":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    :cond_0
    iget-object v3, v2, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-nez v3, :cond_1

    .line 168
    iget-object v3, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mControlBar:Landroid/support/v17/leanback/widget/ControlBar;

    iget-object v4, v2, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/support/v17/leanback/widget/ControlBar;->addView(Landroid/view/View;)V

    .line 170
    :cond_1
    invoke-virtual {p3, v2, v0}, Landroid/support/v17/leanback/widget/Presenter;->onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V

    .line 171
    return-void
.end method


# virtual methods
.method bindControlToAction(ILandroid/support/v17/leanback/widget/Presenter;)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "presenter"    # Landroid/support/v17/leanback/widget/Presenter;

    .prologue
    .line 144
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->getDisplayedAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->bindControlToAction(ILandroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 145
    return-void
.end method

.method getChildMarginFromCenter(Landroid/content/Context;I)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "numControls"    # I

    .prologue
    .line 120
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->this$0:Landroid/support/v17/leanback/widget/ControlBarPresenter;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ControlBarPresenter;->getChildMarginDefault(Landroid/content/Context;)I

    move-result v0

    iget-object v1, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->this$0:Landroid/support/v17/leanback/widget/ControlBarPresenter;

    invoke-virtual {v1, p1}, Landroid/support/v17/leanback/widget/ControlBarPresenter;->getControlIconWidth(Landroid/content/Context;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method getDisplayedAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    return-object v0
.end method

.method showControls(Landroid/support/v17/leanback/widget/Presenter;)V
    .locals 7
    .param p1, "presenter"    # Landroid/support/v17/leanback/widget/Presenter;

    .prologue
    .line 124
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->getDisplayedAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    .line 125
    .local v0, "adapter":Landroid/support/v17/leanback/widget/ObjectAdapter;
    if-nez v0, :cond_1

    const/4 v1, 0x0

    .line 127
    .local v1, "adapterSize":I
    :goto_0
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mControlBar:Landroid/support/v17/leanback/widget/ControlBar;

    invoke-virtual {v5}, Landroid/support/v17/leanback/widget/ControlBar;->getFocusedChild()Landroid/view/View;

    move-result-object v2

    .line 128
    .local v2, "focusedView":Landroid/view/View;
    if-eqz v2, :cond_0

    if-lez v1, :cond_0

    iget-object v5, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mControlBar:Landroid/support/v17/leanback/widget/ControlBar;

    invoke-virtual {v5, v2}, Landroid/support/v17/leanback/widget/ControlBar;->indexOfChild(Landroid/view/View;)I

    move-result v5

    if-lt v5, v1, :cond_0

    .line 130
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mControlBar:Landroid/support/v17/leanback/widget/ControlBar;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Landroid/support/v17/leanback/widget/ControlBar;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->requestFocus()Z

    .line 132
    :cond_0
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mControlBar:Landroid/support/v17/leanback/widget/ControlBar;

    invoke-virtual {v5}, Landroid/support/v17/leanback/widget/ControlBar;->getChildCount()I

    move-result v5

    add-int/lit8 v3, v5, -0x1

    .local v3, "i":I
    :goto_1
    if-lt v3, v1, :cond_2

    .line 133
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mControlBar:Landroid/support/v17/leanback/widget/ControlBar;

    invoke-virtual {v5, v3}, Landroid/support/v17/leanback/widget/ControlBar;->removeViewAt(I)V

    .line 132
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 125
    .end local v1    # "adapterSize":I
    .end local v2    # "focusedView":Landroid/view/View;
    .end local v3    # "i":I
    :cond_1
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v1

    goto :goto_0

    .line 135
    .restart local v1    # "adapterSize":I
    .restart local v2    # "focusedView":Landroid/view/View;
    .restart local v3    # "i":I
    :cond_2
    const/4 v4, 0x0

    .local v4, "position":I
    :goto_2
    if-ge v4, v1, :cond_3

    const/4 v5, 0x7

    if-ge v4, v5, :cond_3

    .line 137
    invoke-direct {p0, v4, v0, p1}, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->bindControlToAction(ILandroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 136
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 139
    :cond_3
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mControlBar:Landroid/support/v17/leanback/widget/ControlBar;

    iget-object v6, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mControlBar:Landroid/support/v17/leanback/widget/ControlBar;

    invoke-virtual {v6}, Landroid/support/v17/leanback/widget/ControlBar;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p0, v6, v1}, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->getChildMarginFromCenter(Landroid/content/Context;I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/support/v17/leanback/widget/ControlBar;->setChildMarginFromCenter(I)V

    .line 141
    return-void
.end method
