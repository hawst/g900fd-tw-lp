.class public Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;
.super Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;
.source "PlaybackControlsRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/PlaybackControlsRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RepeatAction"
.end annotation


# static fields
.field public static ALL:I

.field public static NONE:I

.field public static ONE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 299
    const/4 v0, 0x0

    sput v0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->NONE:I

    .line 304
    const/4 v0, 0x1

    sput v0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->ALL:I

    .line 309
    const/4 v0, 0x2

    sput v0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->ONE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "repeatAllColor"    # I
    .param p3, "repeatOneColor"    # I

    .prologue
    .line 336
    sget v4, Landroid/support/v17/leanback/R$id;->lb_control_repeat:I

    invoke-direct {p0, v4}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;-><init>(I)V

    .line 337
    const/4 v4, 0x3

    new-array v0, v4, [Landroid/graphics/drawable/Drawable;

    .line 338
    .local v0, "drawables":[Landroid/graphics/drawable/Drawable;
    sget v4, Landroid/support/v17/leanback/R$styleable;->lbPlaybackControlsActionIcons_repeat:I

    # invokes: Landroid/support/v17/leanback/widget/PlaybackControlsRow;->getStyledDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    invoke-static {p1, v4}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->access$000(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .line 340
    .local v2, "repeatDrawable":Landroid/graphics/drawable/BitmapDrawable;
    sget v4, Landroid/support/v17/leanback/R$styleable;->lbPlaybackControlsActionIcons_repeat_one:I

    # invokes: Landroid/support/v17/leanback/widget/PlaybackControlsRow;->getStyledDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    invoke-static {p1, v4}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->access$000(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    .line 342
    .local v3, "repeatOneDrawable":Landroid/graphics/drawable/BitmapDrawable;
    sget v4, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->NONE:I

    aput-object v2, v0, v4

    .line 343
    sget v4, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->ALL:I

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    # invokes: Landroid/support/v17/leanback/widget/PlaybackControlsRow;->createBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    invoke-static {v7, p2}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->access$200(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v5, v0, v4

    .line 345
    sget v4, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->ONE:I

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    # invokes: Landroid/support/v17/leanback/widget/PlaybackControlsRow;->createBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    invoke-static {v7, p3}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->access$200(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v5, v0, v4

    .line 347
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->setDrawables([Landroid/graphics/drawable/Drawable;)V

    .line 349
    array-length v4, v0

    new-array v1, v4, [Ljava/lang/String;

    .line 351
    .local v1, "labels":[Ljava/lang/String;
    sget v4, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->NONE:I

    sget v5, Landroid/support/v17/leanback/R$string;->lb_playback_controls_repeat_all:I

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    .line 352
    sget v4, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->ALL:I

    sget v5, Landroid/support/v17/leanback/R$string;->lb_playback_controls_repeat_one:I

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    .line 353
    sget v4, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->ONE:I

    sget v5, Landroid/support/v17/leanback/R$string;->lb_playback_controls_repeat_none:I

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    .line 354
    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->setLabels([Ljava/lang/String;)V

    .line 355
    return-void
.end method
