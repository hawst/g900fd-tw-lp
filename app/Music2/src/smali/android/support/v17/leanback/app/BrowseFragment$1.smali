.class Landroid/support/v17/leanback/app/BrowseFragment$1;
.super Ljava/lang/Object;
.source "BrowseFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/app/BrowseFragment;->startHeadersTransitionInternal(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/BrowseFragment;

.field final synthetic val$withHeaders:Z


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/BrowseFragment;Z)V
    .locals 0

    .prologue
    .line 487
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    iput-boolean p2, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->val$withHeaders:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 490
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersFragment:Landroid/support/v17/leanback/app/HeadersFragment;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$300(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/HeadersFragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/HeadersFragment;->onTransitionStart()V

    .line 491
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # invokes: Landroid/support/v17/leanback/app/BrowseFragment;->createHeadersTransition()V
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$400(Landroid/support/v17/leanback/app/BrowseFragment;)V

    .line 492
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mBrowseTransitionListener:Landroid/support/v17/leanback/app/BrowseFragment$BrowseTransitionListener;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$500(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/BrowseFragment$BrowseTransitionListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 493
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mBrowseTransitionListener:Landroid/support/v17/leanback/app/BrowseFragment$BrowseTransitionListener;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$500(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/BrowseFragment$BrowseTransitionListener;

    move-result-object v2

    iget-boolean v3, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->val$withHeaders:Z

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/app/BrowseFragment$BrowseTransitionListener;->onHeadersTransitionStart(Z)V

    .line 495
    :cond_0
    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;
    invoke-static {}, Landroid/support/v17/leanback/app/BrowseFragment;->access$900()Landroid/support/v17/leanback/transition/TransitionHelper;

    move-result-object v3

    iget-boolean v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->val$withHeaders:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mSceneWithHeaders:Ljava/lang/Object;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$600(Landroid/support/v17/leanback/app/BrowseFragment;)Ljava/lang/Object;

    move-result-object v2

    :goto_0
    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersTransition:Ljava/lang/Object;
    invoke-static {v4}, Landroid/support/v17/leanback/app/BrowseFragment;->access$800(Landroid/support/v17/leanback/app/BrowseFragment;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/support/v17/leanback/transition/TransitionHelper;->runTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 497
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mHeadersBackStackEnabled:Z
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1000(Landroid/support/v17/leanback/app/BrowseFragment;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 498
    iget-boolean v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->val$withHeaders:Z

    if-nez v2, :cond_3

    .line 499
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mWithHeadersBackStackName:Ljava/lang/String;
    invoke-static {v3}, Landroid/support/v17/leanback/app/BrowseFragment;->access$100(Landroid/support/v17/leanback/app/BrowseFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 510
    :cond_1
    :goto_1
    return-void

    .line 495
    :cond_2
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mSceneWithoutHeaders:Ljava/lang/Object;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$700(Landroid/support/v17/leanback/app/BrowseFragment;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 502
    :cond_3
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->access$1100(Landroid/support/v17/leanback/app/BrowseFragment;)Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;

    move-result-object v2

    iget v1, v2, Landroid/support/v17/leanback/app/BrowseFragment$BackStackListener;->mIndexOfHeadersBackStack:I

    .line 503
    .local v1, "index":I
    if-ltz v1, :cond_1

    .line 504
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/FragmentManager;->getBackStackEntryAt(I)Landroid/app/FragmentManager$BackStackEntry;

    move-result-object v0

    .line 505
    .local v0, "entry":Landroid/app/FragmentManager$BackStackEntry;
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/BrowseFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-interface {v0}, Landroid/app/FragmentManager$BackStackEntry;->getId()I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/app/FragmentManager;->popBackStackImmediate(II)Z

    goto :goto_1
.end method
