.class public Landroid/support/v17/leanback/app/SearchFragment;
.super Landroid/app/Fragment;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;
    }
.end annotation


# static fields
.field private static final ARG_PREFIX:Ljava/lang/String;

.field private static final ARG_QUERY:Ljava/lang/String;

.field private static final ARG_TITLE:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final QUERY_COMPLETE:I

.field private final RESULTS_CHANGED:I

.field private final mAdapterObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

.field private mBadgeDrawable:Landroid/graphics/drawable/Drawable;

.field private final mHandler:Landroid/os/Handler;

.field private mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

.field private mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

.field private mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

.field private mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

.field private mPendingQuery:Ljava/lang/String;

.field private mProvider:Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;

.field private mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private final mResultsChangedCallback:Ljava/lang/Runnable;

.field private mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

.field private mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

.field private final mSetSearchResultProvider:Ljava/lang/Runnable;

.field private mSpeechRecognitionCallback:Landroid/support/v17/leanback/widget/SpeechRecognitionCallback;

.field private mSpeechRecognizer:Landroid/speech/SpeechRecognizer;

.field private mStatus:I

.field private mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 64
    const-class v0, Landroid/support/v17/leanback/app/SearchFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v17/leanback/app/SearchFragment;->TAG:Ljava/lang/String;

    .line 67
    const-class v0, Landroid/support/v17/leanback/app/SearchFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v17/leanback/app/SearchFragment;->ARG_PREFIX:Ljava/lang/String;

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/support/v17/leanback/app/SearchFragment;->ARG_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".query"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v17/leanback/app/SearchFragment;->ARG_QUERY:Ljava/lang/String;

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/support/v17/leanback/app/SearchFragment;->ARG_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".title"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v17/leanback/app/SearchFragment;->ARG_TITLE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 112
    new-instance v0, Landroid/support/v17/leanback/app/SearchFragment$1;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/SearchFragment$1;-><init>(Landroid/support/v17/leanback/app/SearchFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mAdapterObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    .line 122
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mHandler:Landroid/os/Handler;

    .line 124
    new-instance v0, Landroid/support/v17/leanback/app/SearchFragment$2;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/SearchFragment$2;-><init>(Landroid/support/v17/leanback/app/SearchFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mResultsChangedCallback:Ljava/lang/Runnable;

    .line 142
    new-instance v0, Landroid/support/v17/leanback/app/SearchFragment$3;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/SearchFragment$3;-><init>(Landroid/support/v17/leanback/app/SearchFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSetSearchResultProvider:Ljava/lang/Runnable;

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mPendingQuery:Ljava/lang/String;

    .line 184
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->RESULTS_CHANGED:I

    .line 185
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->QUERY_COMPLETE:I

    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/app/SearchFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mResultsChangedCallback:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Landroid/support/v17/leanback/app/SearchFragment;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/support/v17/leanback/app/SearchFragment;->executePendingQuery()V

    return-void
.end method

.method static synthetic access$1100(Landroid/support/v17/leanback/app/SearchFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/SearchFragment;->retrieveResults(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1202(Landroid/support/v17/leanback/app/SearchFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mPendingQuery:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1300(Landroid/support/v17/leanback/app/SearchFragment;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/support/v17/leanback/app/SearchFragment;->queryComplete()V

    return-void
.end method

.method static synthetic access$1400(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/SearchBar;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    return-object v0
.end method

.method static synthetic access$1500(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/OnItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$1600(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    return-object v0
.end method

.method static synthetic access$1700(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/OnItemClickedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

    return-object v0
.end method

.method static synthetic access$1800(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/OnItemViewClickedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    return-object v0
.end method

.method static synthetic access$200(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/app/RowsFragment;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    return-object v0
.end method

.method static synthetic access$300(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    return-object v0
.end method

.method static synthetic access$302(Landroid/support/v17/leanback/app/SearchFragment;Landroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 63
    iput-object p1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    return-object p1
.end method

.method static synthetic access$400(Landroid/support/v17/leanback/app/SearchFragment;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    iget v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mStatus:I

    return v0
.end method

.method static synthetic access$476(Landroid/support/v17/leanback/app/SearchFragment;I)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;
    .param p1, "x1"    # I

    .prologue
    .line 63
    iget v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mStatus:I

    or-int/2addr v0, p1

    iput v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mStatus:I

    return v0
.end method

.method static synthetic access$500(Landroid/support/v17/leanback/app/SearchFragment;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/support/v17/leanback/app/SearchFragment;->focusOnResults()V

    return-void
.end method

.method static synthetic access$600(Landroid/support/v17/leanback/app/SearchFragment;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/support/v17/leanback/app/SearchFragment;->updateSearchBarNextFocusId()V

    return-void
.end method

.method static synthetic access$700(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mProvider:Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;

    return-object v0
.end method

.method static synthetic access$800(Landroid/support/v17/leanback/app/SearchFragment;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/support/v17/leanback/app/SearchFragment;->releaseAdapter()V

    return-void
.end method

.method static synthetic access$900(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/SearchFragment;

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mAdapterObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    return-object v0
.end method

.method private executePendingQuery()V
    .locals 2

    .prologue
    .line 605
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mPendingQuery:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-eqz v1, :cond_0

    .line 606
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mPendingQuery:Ljava/lang/String;

    .line 607
    .local v0, "query":Ljava/lang/String;
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mPendingQuery:Ljava/lang/String;

    .line 608
    invoke-direct {p0, v0}, Landroid/support/v17/leanback/app/SearchFragment;->retrieveResults(Ljava/lang/String;)V

    .line 610
    .end local v0    # "query":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private focusOnResults()V
    .locals 2

    .prologue
    .line 581
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 590
    :cond_0
    :goto_0
    return-void

    .line 586
    :cond_1
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/RowsFragment;->setSelectedPosition(I)V

    .line 587
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->requestFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588
    iget v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mStatus:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mStatus:I

    goto :goto_0
.end method

.method private onSetSearchResultProvider()V
    .locals 2

    .prologue
    .line 593
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSetSearchResultProvider:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 594
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSetSearchResultProvider:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 595
    return-void
.end method

.method private queryComplete()V
    .locals 1

    .prologue
    .line 566
    iget v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mStatus:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mStatus:I

    .line 567
    invoke-direct {p0}, Landroid/support/v17/leanback/app/SearchFragment;->focusOnResults()V

    .line 568
    return-void
.end method

.method private readArguments(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "args"    # Landroid/os/Bundle;

    .prologue
    .line 613
    if-nez p1, :cond_1

    .line 623
    :cond_0
    :goto_0
    return-void

    .line 616
    :cond_1
    sget-object v0, Landroid/support/v17/leanback/app/SearchFragment;->ARG_QUERY:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 617
    sget-object v0, Landroid/support/v17/leanback/app/SearchFragment;->ARG_QUERY:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/app/SearchFragment;->setSearchQuery(Ljava/lang/String;)V

    .line 620
    :cond_2
    sget-object v0, Landroid/support/v17/leanback/app/SearchFragment;->ARG_TITLE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621
    sget-object v0, Landroid/support/v17/leanback/app/SearchFragment;->ARG_TITLE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/app/SearchFragment;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private releaseAdapter()V
    .locals 2

    .prologue
    .line 598
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-eqz v0, :cond_0

    .line 599
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mAdapterObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->unregisterObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 600
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 602
    :cond_0
    return-void
.end method

.method private releaseRecognizer()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 362
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSpeechRecognizer:Landroid/speech/SpeechRecognizer;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/SearchBar;->setSpeechRecognizer(Landroid/speech/SpeechRecognizer;)V

    .line 364
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSpeechRecognizer:Landroid/speech/SpeechRecognizer;

    invoke-virtual {v0}, Landroid/speech/SpeechRecognizer;->destroy()V

    .line 365
    iput-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSpeechRecognizer:Landroid/speech/SpeechRecognizer;

    .line 367
    :cond_0
    return-void
.end method

.method private retrieveResults(Ljava/lang/String;)V
    .locals 1
    .param p1, "searchQuery"    # Ljava/lang/String;

    .prologue
    .line 561
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mProvider:Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;

    invoke-interface {v0, p1}, Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;->onQueryTextChange(Ljava/lang/String;)Z

    .line 562
    iget v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mStatus:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mStatus:I

    .line 563
    return-void
.end method

.method private setSearchQuery(Ljava/lang/String;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 626
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/SearchBar;->setSearchQuery(Ljava/lang/String;)V

    .line 627
    return-void
.end method

.method private updateSearchBarNextFocusId()V
    .locals 2

    .prologue
    .line 571
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-nez v1, :cond_1

    .line 578
    :cond_0
    :goto_0
    return-void

    .line 574
    :cond_1
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    if-nez v1, :cond_3

    :cond_2
    const/4 v0, 0x0

    .line 577
    .local v0, "viewId":I
    :goto_1
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-virtual {v1, v0}, Landroid/support/v17/leanback/widget/SearchBar;->setNextFocusDownId(I)V

    goto :goto_0

    .line 574
    .end local v0    # "viewId":I
    :cond_3
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getId()I

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public getRecognizerIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 549
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 550
    .local v0, "recognizerIntent":Landroid/content/Intent;
    const-string v1, "android.speech.extra.LANGUAGE_MODEL"

    const-string v2, "free_form"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 552
    const-string v1, "android.speech.extra.PARTIAL_RESULTS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 553
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/SearchBar;->getHint()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 554
    const-string v1, "android.speech.extra.PROMPT"

    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/SearchBar;->getHint()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 556
    :cond_0
    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 223
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 224
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 229
    sget v2, Landroid/support/v17/leanback/R$layout;->lb_search_fragment:I

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 231
    .local v0, "root":Landroid/view/View;
    sget v2, Landroid/support/v17/leanback/R$id;->lb_search_frame:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 232
    .local v1, "searchFrame":Landroid/widget/FrameLayout;
    sget v2, Landroid/support/v17/leanback/R$id;->lb_search_bar:I

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v17/leanback/widget/SearchBar;

    iput-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    .line 233
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    new-instance v3, Landroid/support/v17/leanback/app/SearchFragment$4;

    invoke-direct {v3, p0}, Landroid/support/v17/leanback/app/SearchFragment$4;-><init>(Landroid/support/v17/leanback/app/SearchFragment;)V

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/SearchBar;->setSearchBarListener(Landroid/support/v17/leanback/widget/SearchBar$SearchBarListener;)V

    .line 260
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    iget-object v3, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSpeechRecognitionCallback:Landroid/support/v17/leanback/widget/SpeechRecognitionCallback;

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/SearchBar;->setSpeechRecognitionCallback(Landroid/support/v17/leanback/widget/SpeechRecognitionCallback;)V

    .line 262
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/SearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v2}, Landroid/support/v17/leanback/app/SearchFragment;->readArguments(Landroid/os/Bundle;)V

    .line 263
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mBadgeDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 264
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mBadgeDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v2}, Landroid/support/v17/leanback/app/SearchFragment;->setBadgeDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 266
    :cond_0
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mTitle:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 267
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/support/v17/leanback/app/SearchFragment;->setTitle(Ljava/lang/String;)V

    .line 271
    :cond_1
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/SearchFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    sget v3, Landroid/support/v17/leanback/R$id;->lb_results_frame:I

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_4

    .line 272
    new-instance v2, Landroid/support/v17/leanback/app/RowsFragment;

    invoke-direct {v2}, Landroid/support/v17/leanback/app/RowsFragment;-><init>()V

    iput-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    .line 273
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/SearchFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    sget v3, Landroid/support/v17/leanback/R$id;->lb_results_frame:I

    iget-object v4, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v2, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 279
    :goto_0
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    new-instance v3, Landroid/support/v17/leanback/app/SearchFragment$5;

    invoke-direct {v3, p0}, Landroid/support/v17/leanback/app/SearchFragment$5;-><init>(Landroid/support/v17/leanback/app/SearchFragment;)V

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/app/RowsFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 295
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    new-instance v3, Landroid/support/v17/leanback/app/SearchFragment$6;

    invoke-direct {v3, p0}, Landroid/support/v17/leanback/app/SearchFragment$6;-><init>(Landroid/support/v17/leanback/app/SearchFragment;)V

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/app/RowsFragment;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 310
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/app/RowsFragment;->setExpand(Z)V

    .line 311
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mProvider:Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;

    if-eqz v2, :cond_2

    .line 312
    invoke-direct {p0}, Landroid/support/v17/leanback/app/SearchFragment;->onSetSearchResultProvider()V

    .line 314
    :cond_2
    if-nez p3, :cond_3

    .line 316
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mHandler:Landroid/os/Handler;

    new-instance v3, Landroid/support/v17/leanback/app/SearchFragment$7;

    invoke-direct {v3, p0}, Landroid/support/v17/leanback/app/SearchFragment$7;-><init>(Landroid/support/v17/leanback/app/SearchFragment;)V

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 323
    :cond_3
    return-object v0

    .line 276
    :cond_4
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/SearchFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    sget v3, Landroid/support/v17/leanback/R$id;->lb_results_frame:I

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Landroid/support/v17/leanback/app/RowsFragment;

    iput-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 357
    invoke-direct {p0}, Landroid/support/v17/leanback/app/SearchFragment;->releaseAdapter()V

    .line 358
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 359
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 351
    invoke-direct {p0}, Landroid/support/v17/leanback/app/SearchFragment;->releaseRecognizer()V

    .line 352
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 353
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 342
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 343
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSpeechRecognitionCallback:Landroid/support/v17/leanback/widget/SpeechRecognitionCallback;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSpeechRecognizer:Landroid/speech/SpeechRecognizer;

    if-nez v0, :cond_0

    .line 344
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/speech/SpeechRecognizer;->createSpeechRecognizer(Landroid/content/Context;)Landroid/speech/SpeechRecognizer;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSpeechRecognizer:Landroid/speech/SpeechRecognizer;

    .line 345
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSpeechRecognizer:Landroid/speech/SpeechRecognizer;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/SearchBar;->setSpeechRecognizer(Landroid/speech/SpeechRecognizer;)V

    .line 347
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, -0x40800000    # -1.0f

    .line 328
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 330
    iget-object v2, p0, Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    .line 331
    .local v0, "list":Landroid/support/v17/leanback/widget/VerticalGridView;
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Landroid/support/v17/leanback/R$dimen;->lb_search_browse_rows_align_top:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 333
    .local v1, "mContainerListAlignTop":I
    invoke-virtual {v0, v5}, Landroid/support/v17/leanback/widget/VerticalGridView;->setItemAlignmentOffset(I)V

    .line 334
    invoke-virtual {v0, v4}, Landroid/support/v17/leanback/widget/VerticalGridView;->setItemAlignmentOffsetPercent(F)V

    .line 335
    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignmentOffset(I)V

    .line 336
    invoke-virtual {v0, v4}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignmentOffsetPercent(F)V

    .line 337
    invoke-virtual {v0, v5}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignment(I)V

    .line 338
    return-void
.end method

.method public setBadgeDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 460
    iput-object p1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mBadgeDrawable:Landroid/graphics/drawable/Drawable;

    .line 461
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/SearchBar;->setBadgeDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 464
    :cond_0
    return-void
.end method

.method public setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .prologue
    .line 430
    iput-object p1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .line 431
    return-void
.end method

.method public setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .prologue
    .line 420
    iput-object p1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .line 421
    return-void
.end method

.method public setSearchQuery(Landroid/content/Intent;Z)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "submit"    # Z

    .prologue
    .line 527
    const-string v1, "android.speech.extra.RESULTS"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 528
    .local v0, "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 529
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1, p2}, Landroid/support/v17/leanback/app/SearchFragment;->setSearchQuery(Ljava/lang/String;Z)V

    .line 531
    :cond_0
    return-void
.end method

.method public setSearchQuery(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "submit"    # Z

    .prologue
    .line 512
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/SearchBar;->setSearchQuery(Ljava/lang/String;)V

    .line 513
    if-eqz p2, :cond_0

    .line 514
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mProvider:Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;

    invoke-interface {v0, p1}, Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;->onQueryTextSubmit(Ljava/lang/String;)Z

    .line 516
    :cond_0
    return-void
.end method

.method public setSearchResultProvider(Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;)V
    .locals 1
    .param p1, "searchResultProvider"    # Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;

    .prologue
    .line 385
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mProvider:Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;

    if-eq v0, p1, :cond_0

    .line 386
    iput-object p1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mProvider:Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;

    .line 387
    invoke-direct {p0}, Landroid/support/v17/leanback/app/SearchFragment;->onSetSearchResultProvider()V

    .line 389
    :cond_0
    return-void
.end method

.method public setSpeechRecognitionCallback(Landroid/support/v17/leanback/widget/SpeechRecognitionCallback;)V
    .locals 2
    .param p1, "callback"    # Landroid/support/v17/leanback/widget/SpeechRecognitionCallback;

    .prologue
    .line 492
    iput-object p1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSpeechRecognitionCallback:Landroid/support/v17/leanback/widget/SpeechRecognitionCallback;

    .line 493
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSpeechRecognitionCallback:Landroid/support/v17/leanback/widget/SpeechRecognitionCallback;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/SearchBar;->setSpeechRecognitionCallback(Landroid/support/v17/leanback/widget/SpeechRecognitionCallback;)V

    .line 496
    :cond_0
    if-eqz p1, :cond_1

    .line 497
    invoke-direct {p0}, Landroid/support/v17/leanback/app/SearchFragment;->releaseRecognizer()V

    .line 499
    :cond_1
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 439
    iput-object p1, p0, Landroid/support/v17/leanback/app/SearchFragment;->mTitle:Ljava/lang/String;

    .line 440
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/SearchBar;->setTitle(Ljava/lang/String;)V

    .line 443
    :cond_0
    return-void
.end method

.method public startRecognition()V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/SearchBar;->startRecognition()V

    .line 378
    return-void
.end method
