.class final Landroid/support/v17/leanback/widget/ShadowHelper;
.super Ljava/lang/Object;
.source "ShadowHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/ShadowHelper$1;,
        Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperApi21Impl;,
        Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperJbmr2Impl;,
        Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperStubImpl;,
        Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperVersionImpl;
    }
.end annotation


# static fields
.field static final sInstance:Landroid/support/v17/leanback/widget/ShadowHelper;


# instance fields
.field mImpl:Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperVersionImpl;

.field mSupportsShadow:Z

.field mUsesZShadow:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Landroid/support/v17/leanback/widget/ShadowHelper;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/ShadowHelper;-><init>()V

    sput-object v0, Landroid/support/v17/leanback/widget/ShadowHelper;->sInstance:Landroid/support/v17/leanback/widget/ShadowHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 134
    iput-boolean v2, p0, Landroid/support/v17/leanback/widget/ShadowHelper;->mSupportsShadow:Z

    .line 135
    iput-boolean v2, p0, Landroid/support/v17/leanback/widget/ShadowHelper;->mUsesZShadow:Z

    .line 136
    new-instance v0, Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperApi21Impl;

    invoke-direct {v0, v3}, Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperApi21Impl;-><init>(Landroid/support/v17/leanback/widget/ShadowHelper$1;)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ShadowHelper;->mImpl:Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperVersionImpl;

    .line 144
    :goto_0
    return-void

    .line 137
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    .line 138
    iput-boolean v2, p0, Landroid/support/v17/leanback/widget/ShadowHelper;->mSupportsShadow:Z

    .line 139
    new-instance v0, Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperJbmr2Impl;

    invoke-direct {v0, v3}, Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperJbmr2Impl;-><init>(Landroid/support/v17/leanback/widget/ShadowHelper$1;)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ShadowHelper;->mImpl:Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperVersionImpl;

    goto :goto_0

    .line 141
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v17/leanback/widget/ShadowHelper;->mSupportsShadow:Z

    .line 142
    new-instance v0, Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperStubImpl;

    invoke-direct {v0, v3}, Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperStubImpl;-><init>(Landroid/support/v17/leanback/widget/ShadowHelper$1;)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ShadowHelper;->mImpl:Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperVersionImpl;

    goto :goto_0
.end method

.method public static getInstance()Landroid/support/v17/leanback/widget/ShadowHelper;
    .locals 1

    .prologue
    .line 147
    sget-object v0, Landroid/support/v17/leanback/widget/ShadowHelper;->sInstance:Landroid/support/v17/leanback/widget/ShadowHelper;

    return-object v0
.end method


# virtual methods
.method public addShadow(Landroid/view/ViewGroup;Z)Ljava/lang/Object;
    .locals 1
    .param p1, "shadowContainer"    # Landroid/view/ViewGroup;
    .param p2, "roundedCorners"    # Z

    .prologue
    .line 163
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ShadowHelper;->mImpl:Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperVersionImpl;

    invoke-interface {v0, p1, p2}, Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperVersionImpl;->addShadow(Landroid/view/ViewGroup;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public prepareParent(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 159
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ShadowHelper;->mImpl:Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperVersionImpl;

    invoke-interface {v0, p1}, Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperVersionImpl;->prepareParent(Landroid/view/ViewGroup;)V

    .line 160
    return-void
.end method

.method public setShadowFocusLevel(Ljava/lang/Object;F)V
    .locals 1
    .param p1, "impl"    # Ljava/lang/Object;
    .param p2, "level"    # F

    .prologue
    .line 167
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ShadowHelper;->mImpl:Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperVersionImpl;

    invoke-interface {v0, p1, p2}, Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperVersionImpl;->setShadowFocusLevel(Ljava/lang/Object;F)V

    .line 168
    return-void
.end method

.method public setZ(Landroid/view/View;F)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "z"    # F

    .prologue
    .line 174
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ShadowHelper;->mImpl:Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperVersionImpl;

    invoke-interface {v0, p1, p2}, Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperVersionImpl;->setZ(Landroid/view/View;F)V

    .line 175
    return-void
.end method

.method public supportsShadow()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/ShadowHelper;->mSupportsShadow:Z

    return v0
.end method

.method public usesZShadow()Z
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/ShadowHelper;->mUsesZShadow:Z

    return v0
.end method
