.class Landroid/support/v17/leanback/widget/VerticalGridPresenter$3$1;
.super Ljava/lang/Object;
.source "VerticalGridPresenter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/widget/VerticalGridPresenter$3;->onBind(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/support/v17/leanback/widget/VerticalGridPresenter$3;

.field final synthetic val$itemViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/VerticalGridPresenter$3;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3$1;->this$1:Landroid/support/v17/leanback/widget/VerticalGridPresenter$3;

    iput-object p2, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3$1;->val$itemViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 213
    iget-object v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3$1;->this$1:Landroid/support/v17/leanback/widget/VerticalGridPresenter$3;

    iget-object v0, v0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3;->this$0:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->getOnItemClickedListener()Landroid/support/v17/leanback/widget/OnItemClickedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3$1;->this$1:Landroid/support/v17/leanback/widget/VerticalGridPresenter$3;

    iget-object v0, v0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3;->this$0:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->getOnItemClickedListener()Landroid/support/v17/leanback/widget/OnItemClickedListener;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3$1;->val$itemViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    iget-object v1, v1, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mItem:Ljava/lang/Object;

    invoke-interface {v0, v1, v3}, Landroid/support/v17/leanback/widget/OnItemClickedListener;->onItemClicked(Ljava/lang/Object;Landroid/support/v17/leanback/widget/Row;)V

    .line 218
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3$1;->this$1:Landroid/support/v17/leanback/widget/VerticalGridPresenter$3;

    iget-object v0, v0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3;->this$0:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->getOnItemViewClickedListener()Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 220
    iget-object v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3$1;->this$1:Landroid/support/v17/leanback/widget/VerticalGridPresenter$3;

    iget-object v0, v0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3;->this$0:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->getOnItemViewClickedListener()Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3$1;->val$itemViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    iget-object v1, v1, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    iget-object v2, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3$1;->val$itemViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    iget-object v2, v2, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mItem:Ljava/lang/Object;

    invoke-interface {v0, v1, v2, v3, v3}, Landroid/support/v17/leanback/widget/OnItemViewClickedListener;->onItemClicked(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V

    .line 223
    :cond_1
    return-void
.end method
