.class public final Landroid/support/v17/leanback/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final lb_playback_controls_more_actions:I = 0x7f0b03fe

.field public static final lb_playback_controls_pause:I = 0x7f0b03f9

.field public static final lb_playback_controls_play:I = 0x7f0b03f8

.field public static final lb_playback_controls_repeat_all:I = 0x7f0b0404

.field public static final lb_playback_controls_repeat_none:I = 0x7f0b0403

.field public static final lb_playback_controls_repeat_one:I = 0x7f0b0405

.field public static final lb_playback_controls_shuffle_disable:I = 0x7f0b0407

.field public static final lb_playback_controls_shuffle_enable:I = 0x7f0b0406

.field public static final lb_playback_controls_skip_next:I = 0x7f0b03fc

.field public static final lb_playback_controls_skip_previous:I = 0x7f0b03fd

.field public static final lb_playback_controls_thumb_down:I = 0x7f0b0401

.field public static final lb_playback_controls_thumb_down_outline:I = 0x7f0b0402

.field public static final lb_playback_controls_thumb_up:I = 0x7f0b03ff

.field public static final lb_playback_controls_thumb_up_outline:I = 0x7f0b0400

.field public static final lb_search_bar_hint:I = 0x7f0b03f4

.field public static final lb_search_bar_hint_speech:I = 0x7f0b03f5

.field public static final lb_search_bar_hint_with_title:I = 0x7f0b03f6

.field public static final lb_search_bar_hint_with_title_speech:I = 0x7f0b03f7
