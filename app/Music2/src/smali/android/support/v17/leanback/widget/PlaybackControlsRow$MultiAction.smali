.class public abstract Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;
.super Landroid/support/v17/leanback/widget/Action;
.source "PlaybackControlsRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/PlaybackControlsRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MultiAction"
.end annotation


# instance fields
.field private mDrawables:[Landroid/graphics/drawable/Drawable;

.field private mIndex:I

.field private mLabels:[Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 58
    int-to-long v0, p1

    invoke-direct {p0, v0, v1}, Landroid/support/v17/leanback/widget/Action;-><init>(J)V

    .line 59
    return-void
.end method


# virtual methods
.method public getNumberOfDrawables()I
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;->mDrawables:[Landroid/graphics/drawable/Drawable;

    array-length v0, v0

    return v0
.end method

.method public setDrawables([Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawables"    # [Landroid/graphics/drawable/Drawable;

    .prologue
    .line 66
    iput-object p1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;->mDrawables:[Landroid/graphics/drawable/Drawable;

    .line 67
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;->setIndex(I)V

    .line 68
    return-void
.end method

.method public setIndex(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 107
    iput p1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;->mIndex:I

    .line 108
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;->mDrawables:[Landroid/graphics/drawable/Drawable;

    iget v1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;->mIndex:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 109
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;->mLabels:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;->mLabels:[Ljava/lang/String;

    iget v1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;->mIndex:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;->setLabel1(Ljava/lang/CharSequence;)V

    .line 112
    :cond_0
    return-void
.end method

.method public setLabels([Ljava/lang/String;)V
    .locals 1
    .param p1, "labels"    # [Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;->mLabels:[Ljava/lang/String;

    .line 72
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;->setIndex(I)V

    .line 73
    return-void
.end method
