.class final Landroid/support/v17/leanback/widget/RoundedRectHelper$Api21Impl;
.super Ljava/lang/Object;
.source "RoundedRectHelper.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/RoundedRectHelper$Impl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/RoundedRectHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Api21Impl"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v17/leanback/widget/RoundedRectHelper$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v17/leanback/widget/RoundedRectHelper$1;

    .prologue
    .line 83
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/RoundedRectHelper$Api21Impl;-><init>()V

    return-void
.end method


# virtual methods
.method public clearBackground(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 91
    invoke-static {p1}, Landroid/support/v17/leanback/widget/RoundedRectHelperApi21;->clearBackground(Landroid/view/View;)V

    .line 92
    return-void
.end method

.method public setRoundedRectBackground(Landroid/view/View;I)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "color"    # I

    .prologue
    .line 86
    invoke-static {p1, p2}, Landroid/support/v17/leanback/widget/RoundedRectHelperApi21;->setRoundedRectBackground(Landroid/view/View;I)V

    .line 87
    return-void
.end method
