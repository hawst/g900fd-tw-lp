.class public Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;
.super Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;
.source "PlaybackControlsRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/PlaybackControlsRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ThumbsDownAction"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 282
    sget v1, Landroid/support/v17/leanback/R$id;->lb_control_thumbs_down:I

    sget v2, Landroid/support/v17/leanback/R$styleable;->lbPlaybackControlsActionIcons_thumb_down:I

    sget v3, Landroid/support/v17/leanback/R$styleable;->lbPlaybackControlsActionIcons_thumb_down_outline:I

    invoke-direct {p0, v1, p1, v2, v3}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;-><init>(ILandroid/content/Context;II)V

    .line 285
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;->getNumberOfDrawables()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    .line 286
    .local v0, "labels":[Ljava/lang/String;
    sget v1, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;->SOLID:I

    sget v2, Landroid/support/v17/leanback/R$string;->lb_playback_controls_thumb_down:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 287
    sget v1, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;->OUTLINE:I

    sget v2, Landroid/support/v17/leanback/R$string;->lb_playback_controls_thumb_down_outline:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 288
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;->setLabels([Ljava/lang/String;)V

    .line 289
    return-void
.end method
