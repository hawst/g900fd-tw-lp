.class Landroid/support/v17/leanback/widget/ListRowPresenter$3;
.super Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
.source "ListRowPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/widget/ListRowPresenter;->initializeRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/widget/ListRowPresenter;

.field final synthetic val$rowViewHolder:Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/ListRowPresenter;Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Landroid/support/v17/leanback/widget/ListRowPresenter$3;->this$0:Landroid/support/v17/leanback/widget/ListRowPresenter;

    iput-object p2, p0, Landroid/support/v17/leanback/widget/ListRowPresenter$3;->val$rowViewHolder:Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAddPresenter(Landroid/support/v17/leanback/widget/Presenter;I)V
    .locals 2
    .param p1, "presenter"    # Landroid/support/v17/leanback/widget/Presenter;
    .param p2, "type"    # I

    .prologue
    .line 245
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ListRowPresenter$3;->val$rowViewHolder:Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/HorizontalGridView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/HorizontalGridView;->getRecycledViewPool()Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    move-result-object v0

    const/16 v1, 0x18

    invoke-virtual {v0, p2, v1}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 246
    return-void
.end method

.method public onAttachedToWindow(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 3
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 236
    iget-object v1, p1, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->itemView:Landroid/view/View;

    instance-of v1, v1, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;

    if-eqz v1, :cond_0

    .line 237
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ListRowPresenter$3;->val$rowViewHolder:Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    iget-object v1, v1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mColorDimmer:Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;

    invoke-virtual {v1}, Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    .line 238
    .local v0, "dimmedColor":I
    iget-object v1, p1, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->itemView:Landroid/view/View;

    check-cast v1, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;

    invoke-virtual {v1, v0}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->setOverlayColor(I)V

    .line 240
    .end local v0    # "dimmedColor":I
    :cond_0
    iget-object v1, p1, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->itemView:Landroid/view/View;

    iget-object v2, p0, Landroid/support/v17/leanback/widget/ListRowPresenter$3;->val$rowViewHolder:Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    iget-boolean v2, v2, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mExpanded:Z

    invoke-virtual {v1, v2}, Landroid/view/View;->setActivated(Z)V

    .line 241
    return-void
.end method

.method public onBind(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 2
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 207
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ListRowPresenter$3;->this$0:Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getOnItemClickedListener()Landroid/support/v17/leanback/widget/OnItemClickedListener;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/widget/ListRowPresenter$3;->this$0:Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getOnItemViewClickedListener()Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 208
    :cond_0
    iget-object v0, p1, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    iget-object v0, v0, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    new-instance v1, Landroid/support/v17/leanback/widget/ListRowPresenter$3$1;

    invoke-direct {v1, p0, p1}, Landroid/support/v17/leanback/widget/ListRowPresenter$3$1;-><init>(Landroid/support/v17/leanback/widget/ListRowPresenter$3;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    :cond_1
    return-void
.end method

.method public onUnbind(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 2
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 229
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ListRowPresenter$3;->this$0:Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getOnItemClickedListener()Landroid/support/v17/leanback/widget/OnItemClickedListener;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/widget/ListRowPresenter$3;->this$0:Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getOnItemViewClickedListener()Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 230
    :cond_0
    iget-object v0, p1, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    iget-object v0, v0, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    :cond_1
    return-void
.end method
