.class Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;
.super Ljava/lang/Object;
.source "PlaybackOverlayFragment.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/PlaybackOverlayFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 120
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v2, 0x0

    .line 124
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # getter for: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgAlpha:I
    invoke-static {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$100(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)I

    move-result v0

    if-lez v0, :cond_1

    .line 125
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    const/4 v1, 0x1

    # invokes: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->enableVerticalGridAnimations(Z)V
    invoke-static {v0, v1}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$000(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;Z)V

    .line 126
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # invokes: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->startFadeTimer()V
    invoke-static {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$200(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V

    .line 127
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # getter for: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadeCompleteListener:Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$300(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # getter for: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadeCompleteListener:Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$300(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;->onFadeInComplete()V

    .line 140
    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # setter for: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingStatus:I
    invoke-static {v0, v2}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$502(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;I)I

    .line 141
    return-void

    .line 131
    :cond_1
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 133
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->setSelectedPosition(I)V

    .line 134
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    const/4 v1, 0x0

    # invokes: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->resetControlsToPrimaryActions(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    invoke-static {v0, v1}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$400(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 136
    :cond_2
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # getter for: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadeCompleteListener:Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$300(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # getter for: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadeCompleteListener:Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$300(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;->onFadeOutComplete()V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 117
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 113
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    const/4 v1, 0x0

    # invokes: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->enableVerticalGridAnimations(Z)V
    invoke-static {v0, v1}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$000(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;Z)V

    .line 114
    return-void
.end method
