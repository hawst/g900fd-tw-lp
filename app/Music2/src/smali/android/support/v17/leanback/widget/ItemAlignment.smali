.class Landroid/support/v17/leanback/widget/ItemAlignment;
.super Ljava/lang/Object;
.source "ItemAlignment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/ItemAlignment$Axis;
    }
.end annotation


# instance fields
.field public final horizontal:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

.field private mMainAxis:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

.field private mOrientation:I

.field private mSecondAxis:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

.field public final vertical:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput v2, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->mOrientation:I

    .line 146
    new-instance v0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;-><init>(I)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->vertical:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    .line 148
    new-instance v0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    invoke-direct {v0, v2}, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;-><init>(I)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->horizontal:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    .line 150
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->horizontal:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->mMainAxis:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    .line 152
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->vertical:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->mSecondAxis:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    return-void
.end method


# virtual methods
.method public final mainAxis()Landroid/support/v17/leanback/widget/ItemAlignment$Axis;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->mMainAxis:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    return-object v0
.end method

.method public final setOrientation(I)V
    .locals 1
    .param p1, "orientation"    # I

    .prologue
    .line 163
    iput p1, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->mOrientation:I

    .line 164
    iget v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->mOrientation:I

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->horizontal:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->mMainAxis:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    .line 166
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->vertical:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->mSecondAxis:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    .line 171
    :goto_0
    return-void

    .line 168
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->vertical:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->mMainAxis:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    .line 169
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->horizontal:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment;->mSecondAxis:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    goto :goto_0
.end method
