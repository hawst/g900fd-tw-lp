.class public Landroid/support/v17/leanback/widget/Row;
.super Ljava/lang/Object;
.source "Row.java"


# instance fields
.field private mFlags:I

.field private mHeaderItem:Landroid/support/v17/leanback/widget/HeaderItem;

.field private mId:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v17/leanback/widget/Row;->mFlags:I

    .line 31
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v17/leanback/widget/Row;->mId:J

    .line 59
    return-void
.end method

.method public constructor <init>(JLandroid/support/v17/leanback/widget/HeaderItem;)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "headerItem"    # Landroid/support/v17/leanback/widget/HeaderItem;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v17/leanback/widget/Row;->mFlags:I

    .line 31
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v17/leanback/widget/Row;->mId:J

    .line 41
    invoke-virtual {p0, p1, p2}, Landroid/support/v17/leanback/widget/Row;->setId(J)V

    .line 42
    invoke-virtual {p0, p3}, Landroid/support/v17/leanback/widget/Row;->setHeaderItem(Landroid/support/v17/leanback/widget/HeaderItem;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/support/v17/leanback/widget/HeaderItem;)V
    .locals 2
    .param p1, "headerItem"    # Landroid/support/v17/leanback/widget/HeaderItem;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v17/leanback/widget/Row;->mFlags:I

    .line 31
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v17/leanback/widget/Row;->mId:J

    .line 52
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/Row;->setHeaderItem(Landroid/support/v17/leanback/widget/HeaderItem;)V

    .line 53
    return-void
.end method


# virtual methods
.method public final getHeaderItem()Landroid/support/v17/leanback/widget/HeaderItem;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Landroid/support/v17/leanback/widget/Row;->mHeaderItem:Landroid/support/v17/leanback/widget/HeaderItem;

    return-object v0
.end method

.method final setFlags(II)V
    .locals 2
    .param p1, "flags"    # I
    .param p2, "mask"    # I

    .prologue
    .line 114
    iget v0, p0, Landroid/support/v17/leanback/widget/Row;->mFlags:I

    xor-int/lit8 v1, p2, -0x1

    and-int/2addr v0, v1

    and-int v1, p1, p2

    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/v17/leanback/widget/Row;->mFlags:I

    .line 115
    return-void
.end method

.method public final setHeaderItem(Landroid/support/v17/leanback/widget/HeaderItem;)V
    .locals 0
    .param p1, "headerItem"    # Landroid/support/v17/leanback/widget/HeaderItem;

    .prologue
    .line 77
    iput-object p1, p0, Landroid/support/v17/leanback/widget/Row;->mHeaderItem:Landroid/support/v17/leanback/widget/HeaderItem;

    .line 78
    return-void
.end method

.method public final setId(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 86
    iput-wide p1, p0, Landroid/support/v17/leanback/widget/Row;->mId:J

    .line 87
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/v17/leanback/widget/Row;->setFlags(II)V

    .line 88
    return-void
.end method
