.class Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;
.super Ljava/lang/Object;
.source "FocusHighlightHelper.java"

# interfaces
.implements Landroid/animation/TimeAnimator$TimeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/FocusHighlightHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FocusAnimator"
.end annotation


# instance fields
.field private final mAnimator:Landroid/animation/TimeAnimator;

.field private final mDimmer:Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;

.field private final mDuration:I

.field private mFocusLevel:F

.field private mFocusLevelDelta:F

.field private mFocusLevelStart:F

.field private final mInterpolator:Landroid/view/animation/Interpolator;

.field private final mScaleDiff:F

.field private final mView:Landroid/view/View;

.field private final mWrapper:Landroid/support/v17/leanback/widget/ShadowOverlayContainer;


# direct methods
.method constructor <init>(Landroid/view/View;FZI)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "scale"    # F
    .param p3, "useDimmer"    # Z
    .param p4, "duration"    # I

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mFocusLevel:F

    .line 41
    new-instance v0, Landroid/animation/TimeAnimator;

    invoke-direct {v0}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mAnimator:Landroid/animation/TimeAnimator;

    .line 42
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 58
    iput-object p1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mView:Landroid/view/View;

    .line 59
    iput p4, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mDuration:I

    .line 60
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v0, p2, v0

    iput v0, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mScaleDiff:F

    .line 61
    instance-of v0, p1, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 62
    check-cast v0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;

    iput-object v0, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mWrapper:Landroid/support/v17/leanback/widget/ShadowOverlayContainer;

    .line 66
    :goto_0
    iget-object v0, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    .line 67
    iget-object v0, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mWrapper:Landroid/support/v17/leanback/widget/ShadowOverlayContainer;

    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    .line 68
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;->createDefault(Landroid/content/Context;)Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mDimmer:Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;

    .line 72
    :goto_1
    return-void

    .line 64
    :cond_0
    iput-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mWrapper:Landroid/support/v17/leanback/widget/ShadowOverlayContainer;

    goto :goto_0

    .line 70
    :cond_1
    iput-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mDimmer:Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;

    goto :goto_1
.end method


# virtual methods
.method animateFocus(ZZ)V
    .locals 2
    .param p1, "select"    # Z
    .param p2, "immediate"    # Z

    .prologue
    .line 46
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->endAnimation()V

    .line 47
    if-eqz p1, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 48
    .local v0, "end":F
    :goto_0
    if-eqz p2, :cond_2

    .line 49
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->setFocusLevel(F)V

    .line 55
    :cond_0
    :goto_1
    return-void

    .line 47
    .end local v0    # "end":F
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 50
    .restart local v0    # "end":F
    :cond_2
    iget v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mFocusLevel:F

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_0

    .line 51
    iget v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mFocusLevel:F

    iput v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mFocusLevelStart:F

    .line 52
    iget v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mFocusLevelStart:F

    sub-float v1, v0, v1

    iput v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mFocusLevelDelta:F

    .line 53
    iget-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v1}, Landroid/animation/TimeAnimator;->start()V

    goto :goto_1
.end method

.method endAnimation()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->end()V

    .line 94
    return-void
.end method

.method public onTimeUpdate(Landroid/animation/TimeAnimator;JJ)V
    .locals 6
    .param p1, "animation"    # Landroid/animation/TimeAnimator;
    .param p2, "totalTime"    # J
    .param p4, "deltaTime"    # J

    .prologue
    .line 99
    iget v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mDuration:I

    int-to-long v2, v1

    cmp-long v1, p2, v2

    if-ltz v1, :cond_1

    .line 100
    const/high16 v0, 0x3f800000    # 1.0f

    .line 101
    .local v0, "fraction":F
    iget-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v1}, Landroid/animation/TimeAnimator;->end()V

    .line 105
    :goto_0
    iget-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mInterpolator:Landroid/view/animation/Interpolator;

    if-eqz v1, :cond_0

    .line 106
    iget-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    .line 108
    :cond_0
    iget v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mFocusLevelStart:F

    iget v2, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mFocusLevelDelta:F

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->setFocusLevel(F)V

    .line 109
    return-void

    .line 103
    .end local v0    # "fraction":F
    :cond_1
    long-to-double v2, p2

    iget v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mDuration:I

    int-to-double v4, v1

    div-double/2addr v2, v4

    double-to-float v0, v2

    .restart local v0    # "fraction":F
    goto :goto_0
.end method

.method setFocusLevel(F)V
    .locals 3
    .param p1, "level"    # F

    .prologue
    .line 75
    iput p1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mFocusLevel:F

    .line 76
    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mScaleDiff:F

    mul-float/2addr v2, p1

    add-float v0, v1, v2

    .line 77
    .local v0, "scale":F
    iget-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 78
    iget-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 79
    iget-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mWrapper:Landroid/support/v17/leanback/widget/ShadowOverlayContainer;

    if-eqz v1, :cond_0

    .line 80
    iget-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mWrapper:Landroid/support/v17/leanback/widget/ShadowOverlayContainer;

    invoke-virtual {v1, p1}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->setShadowFocusLevel(F)V

    .line 81
    iget-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mDimmer:Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mDimmer:Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;

    invoke-virtual {v1, p1}, Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;->setActiveLevel(F)V

    .line 83
    iget-object v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mWrapper:Landroid/support/v17/leanback/widget/ShadowOverlayContainer;

    iget-object v2, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->mDimmer:Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;

    invoke-virtual {v2}, Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Paint;->getColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->setOverlayColor(I)V

    .line 86
    :cond_0
    return-void
.end method
