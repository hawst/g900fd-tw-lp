.class Landroid/support/v17/leanback/transition/Slide;
.super Landroid/transition/Visibility;
.source "Slide.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/transition/Slide$SlideAnimatorListener;,
        Landroid/support/v17/leanback/transition/Slide$CalculateSlideVertical;,
        Landroid/support/v17/leanback/transition/Slide$CalculateSlideHorizontal;,
        Landroid/support/v17/leanback/transition/Slide$CalculateSlide;
    }
.end annotation


# static fields
.field private static final sAccelerate:Landroid/animation/TimeInterpolator;

.field private static final sCalculateBottom:Landroid/support/v17/leanback/transition/Slide$CalculateSlide;

.field private static final sCalculateLeft:Landroid/support/v17/leanback/transition/Slide$CalculateSlide;

.field private static final sCalculateRight:Landroid/support/v17/leanback/transition/Slide$CalculateSlide;

.field private static final sCalculateTop:Landroid/support/v17/leanback/transition/Slide$CalculateSlide;

.field private static final sDecelerate:Landroid/animation/TimeInterpolator;


# instance fields
.field mCallback:Landroid/support/v17/leanback/transition/SlideCallback;

.field private mTempDistance:[F

.field private mTempEdge:[I

.field private mTempLoc:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Landroid/support/v17/leanback/transition/Slide;->sDecelerate:Landroid/animation/TimeInterpolator;

    .line 69
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    sput-object v0, Landroid/support/v17/leanback/transition/Slide;->sAccelerate:Landroid/animation/TimeInterpolator;

    .line 111
    new-instance v0, Landroid/support/v17/leanback/transition/Slide$1;

    invoke-direct {v0}, Landroid/support/v17/leanback/transition/Slide$1;-><init>()V

    sput-object v0, Landroid/support/v17/leanback/transition/Slide;->sCalculateLeft:Landroid/support/v17/leanback/transition/Slide$CalculateSlide;

    .line 118
    new-instance v0, Landroid/support/v17/leanback/transition/Slide$2;

    invoke-direct {v0}, Landroid/support/v17/leanback/transition/Slide$2;-><init>()V

    sput-object v0, Landroid/support/v17/leanback/transition/Slide;->sCalculateTop:Landroid/support/v17/leanback/transition/Slide$CalculateSlide;

    .line 125
    new-instance v0, Landroid/support/v17/leanback/transition/Slide$3;

    invoke-direct {v0}, Landroid/support/v17/leanback/transition/Slide$3;-><init>()V

    sput-object v0, Landroid/support/v17/leanback/transition/Slide;->sCalculateRight:Landroid/support/v17/leanback/transition/Slide$CalculateSlide;

    .line 132
    new-instance v0, Landroid/support/v17/leanback/transition/Slide$4;

    invoke-direct {v0}, Landroid/support/v17/leanback/transition/Slide$4;-><init>()V

    sput-object v0, Landroid/support/v17/leanback/transition/Slide;->sCalculateBottom:Landroid/support/v17/leanback/transition/Slide$CalculateSlide;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 139
    invoke-direct {p0}, Landroid/transition/Visibility;-><init>()V

    .line 71
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/v17/leanback/transition/Slide;->mTempLoc:[I

    .line 73
    new-array v0, v1, [I

    iput-object v0, p0, Landroid/support/v17/leanback/transition/Slide;->mTempEdge:[I

    .line 74
    new-array v0, v1, [F

    iput-object v0, p0, Landroid/support/v17/leanback/transition/Slide;->mTempDistance:[F

    .line 140
    return-void
.end method

.method private createAnimation(Landroid/view/View;Landroid/util/Property;FFFLandroid/animation/TimeInterpolator;I)Landroid/animation/Animator;
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p3, "start"    # F
    .param p4, "end"    # F
    .param p5, "terminalValue"    # F
    .param p6, "interpolator"    # Landroid/animation/TimeInterpolator;
    .param p7, "finalVisibility"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;FFF",
            "Landroid/animation/TimeInterpolator;",
            "I)",
            "Landroid/animation/Animator;"
        }
    .end annotation

    .prologue
    .local p2, "property":Landroid/util/Property;, "Landroid/util/Property<Landroid/view/View;Ljava/lang/Float;>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 164
    sget v1, Landroid/support/v17/leanback/R$id;->lb_slide_transition_value:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    move-object v7, v1

    check-cast v7, [F

    .line 165
    .local v7, "startPosition":[F
    if-eqz v7, :cond_0

    .line 166
    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    if-ne v1, p2, :cond_1

    aget p3, v7, v4

    .line 167
    :goto_0
    sget v1, Landroid/support/v17/leanback/R$id;->lb_slide_transition_value:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 169
    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [F

    aput p3, v1, v3

    aput p4, v1, v4

    invoke-static {p1, p2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 171
    .local v6, "anim":Landroid/animation/ObjectAnimator;
    new-instance v0, Landroid/support/v17/leanback/transition/Slide$SlideAnimatorListener;

    move-object v1, p1

    move-object v2, p2

    move v3, p5

    move v4, p4

    move v5, p7

    invoke-direct/range {v0 .. v5}, Landroid/support/v17/leanback/transition/Slide$SlideAnimatorListener;-><init>(Landroid/view/View;Landroid/util/Property;FFI)V

    .line 173
    .local v0, "listener":Landroid/support/v17/leanback/transition/Slide$SlideAnimatorListener;
    invoke-virtual {v6, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 174
    invoke-virtual {v6, v0}, Landroid/animation/ObjectAnimator;->addPauseListener(Landroid/animation/Animator$AnimatorPauseListener;)V

    .line 175
    invoke-virtual {v6, p6}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 176
    return-object v6

    .line 166
    .end local v0    # "listener":Landroid/support/v17/leanback/transition/Slide$SlideAnimatorListener;
    .end local v6    # "anim":Landroid/animation/ObjectAnimator;
    :cond_1
    aget p3, v7, v3

    goto :goto_0
.end method

.method private getSlideEdge(I)Landroid/support/v17/leanback/transition/Slide$CalculateSlide;
    .locals 2
    .param p1, "slideEdge"    # I

    .prologue
    .line 147
    packed-switch p1, :pswitch_data_0

    .line 157
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid slide direction"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :pswitch_0
    sget-object v0, Landroid/support/v17/leanback/transition/Slide;->sCalculateLeft:Landroid/support/v17/leanback/transition/Slide$CalculateSlide;

    .line 155
    :goto_0
    return-object v0

    .line 151
    :pswitch_1
    sget-object v0, Landroid/support/v17/leanback/transition/Slide;->sCalculateTop:Landroid/support/v17/leanback/transition/Slide$CalculateSlide;

    goto :goto_0

    .line 153
    :pswitch_2
    sget-object v0, Landroid/support/v17/leanback/transition/Slide;->sCalculateRight:Landroid/support/v17/leanback/transition/Slide$CalculateSlide;

    goto :goto_0

    .line 155
    :pswitch_3
    sget-object v0, Landroid/support/v17/leanback/transition/Slide;->sCalculateBottom:Landroid/support/v17/leanback/transition/Slide$CalculateSlide;

    goto :goto_0

    .line 147
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onAppear(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;ILandroid/transition/TransitionValues;I)Landroid/animation/Animator;
    .locals 10
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "startVisibility"    # I
    .param p4, "endValues"    # Landroid/transition/TransitionValues;
    .param p5, "endVisibility"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 183
    if-eqz p4, :cond_1

    iget-object v1, p4, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 184
    .local v1, "view":Landroid/view/View;
    :goto_0
    if-nez v1, :cond_2

    .line 193
    :cond_0
    :goto_1
    return-object v0

    .end local v1    # "view":Landroid/view/View;
    :cond_1
    move-object v1, v0

    .line 183
    goto :goto_0

    .line 187
    .restart local v1    # "view":Landroid/view/View;
    :cond_2
    iget-object v2, p0, Landroid/support/v17/leanback/transition/Slide;->mCallback:Landroid/support/v17/leanback/transition/SlideCallback;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/v17/leanback/transition/Slide;->mCallback:Landroid/support/v17/leanback/transition/SlideCallback;

    const/4 v5, 0x1

    iget-object v6, p0, Landroid/support/v17/leanback/transition/Slide;->mTempEdge:[I

    iget-object v9, p0, Landroid/support/v17/leanback/transition/Slide;->mTempDistance:[F

    invoke-interface {v2, v1, v5, v6, v9}, Landroid/support/v17/leanback/transition/SlideCallback;->getSlide(Landroid/view/View;Z[I[F)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 190
    iget-object v0, p0, Landroid/support/v17/leanback/transition/Slide;->mTempEdge:[I

    aget v0, v0, v7

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/transition/Slide;->getSlideEdge(I)Landroid/support/v17/leanback/transition/Slide$CalculateSlide;

    move-result-object v8

    .line 191
    .local v8, "slideCalculator":Landroid/support/v17/leanback/transition/Slide$CalculateSlide;
    invoke-interface {v8, v1}, Landroid/support/v17/leanback/transition/Slide$CalculateSlide;->getHere(Landroid/view/View;)F

    move-result v4

    .line 192
    .local v4, "end":F
    iget-object v0, p0, Landroid/support/v17/leanback/transition/Slide;->mTempDistance:[F

    aget v0, v0, v7

    invoke-interface {v8, v0, v1}, Landroid/support/v17/leanback/transition/Slide$CalculateSlide;->getGone(FLandroid/view/View;)F

    move-result v3

    .line 193
    .local v3, "start":F
    invoke-interface {v8}, Landroid/support/v17/leanback/transition/Slide$CalculateSlide;->getProperty()Landroid/util/Property;

    move-result-object v2

    sget-object v6, Landroid/support/v17/leanback/transition/Slide;->sDecelerate:Landroid/animation/TimeInterpolator;

    move-object v0, p0

    move v5, v4

    invoke-direct/range {v0 .. v7}, Landroid/support/v17/leanback/transition/Slide;->createAnimation(Landroid/view/View;Landroid/util/Property;FFFLandroid/animation/TimeInterpolator;I)Landroid/animation/Animator;

    move-result-object v0

    goto :goto_1
.end method

.method public onDisappear(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;ILandroid/transition/TransitionValues;I)Landroid/animation/Animator;
    .locals 9
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "startVisibility"    # I
    .param p4, "endValues"    # Landroid/transition/TransitionValues;
    .param p5, "endVisibility"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 201
    if-eqz p2, :cond_1

    iget-object v1, p2, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 202
    .local v1, "view":Landroid/view/View;
    :goto_0
    if-nez v1, :cond_2

    .line 212
    :cond_0
    :goto_1
    return-object v0

    .end local v1    # "view":Landroid/view/View;
    :cond_1
    move-object v1, v0

    .line 201
    goto :goto_0

    .line 205
    .restart local v1    # "view":Landroid/view/View;
    :cond_2
    iget-object v2, p0, Landroid/support/v17/leanback/transition/Slide;->mCallback:Landroid/support/v17/leanback/transition/SlideCallback;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/v17/leanback/transition/Slide;->mCallback:Landroid/support/v17/leanback/transition/SlideCallback;

    iget-object v5, p0, Landroid/support/v17/leanback/transition/Slide;->mTempEdge:[I

    iget-object v6, p0, Landroid/support/v17/leanback/transition/Slide;->mTempDistance:[F

    invoke-interface {v2, v1, v7, v5, v6}, Landroid/support/v17/leanback/transition/SlideCallback;->getSlide(Landroid/view/View;Z[I[F)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 208
    iget-object v0, p0, Landroid/support/v17/leanback/transition/Slide;->mTempEdge:[I

    aget v0, v0, v7

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/transition/Slide;->getSlideEdge(I)Landroid/support/v17/leanback/transition/Slide$CalculateSlide;

    move-result-object v8

    .line 209
    .local v8, "slideCalculator":Landroid/support/v17/leanback/transition/Slide$CalculateSlide;
    invoke-interface {v8, v1}, Landroid/support/v17/leanback/transition/Slide$CalculateSlide;->getHere(Landroid/view/View;)F

    move-result v3

    .line 210
    .local v3, "start":F
    iget-object v0, p0, Landroid/support/v17/leanback/transition/Slide;->mTempDistance:[F

    aget v0, v0, v7

    invoke-interface {v8, v0, v1}, Landroid/support/v17/leanback/transition/Slide$CalculateSlide;->getGone(FLandroid/view/View;)F

    move-result v4

    .line 212
    .local v4, "end":F
    invoke-interface {v8}, Landroid/support/v17/leanback/transition/Slide$CalculateSlide;->getProperty()Landroid/util/Property;

    move-result-object v2

    sget-object v6, Landroid/support/v17/leanback/transition/Slide;->sAccelerate:Landroid/animation/TimeInterpolator;

    const/4 v7, 0x4

    move-object v0, p0

    move v5, v3

    invoke-direct/range {v0 .. v7}, Landroid/support/v17/leanback/transition/Slide;->createAnimation(Landroid/view/View;Landroid/util/Property;FFFLandroid/animation/TimeInterpolator;I)Landroid/animation/Animator;

    move-result-object v0

    goto :goto_1
.end method

.method public setCallback(Landroid/support/v17/leanback/transition/SlideCallback;)V
    .locals 0
    .param p1, "callback"    # Landroid/support/v17/leanback/transition/SlideCallback;

    .prologue
    .line 143
    iput-object p1, p0, Landroid/support/v17/leanback/transition/Slide;->mCallback:Landroid/support/v17/leanback/transition/SlideCallback;

    .line 144
    return-void
.end method
