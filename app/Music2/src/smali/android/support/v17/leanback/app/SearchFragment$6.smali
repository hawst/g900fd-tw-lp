.class Landroid/support/v17/leanback/app/SearchFragment$6;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/OnItemViewClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/app/SearchFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/SearchFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/SearchFragment;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Landroid/support/v17/leanback/app/SearchFragment$6;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClicked(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V
    .locals 2
    .param p1, "itemViewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;
    .param p3, "rowViewHolder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p4, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 299
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment$6;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;
    invoke-static {v1}, Landroid/support/v17/leanback/app/SearchFragment;->access$200(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/app/RowsFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getSelectedPosition()I

    move-result v0

    .line 301
    .local v0, "position":I
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment$6;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/SearchFragment;->access$1700(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/OnItemClickedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 302
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment$6;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/SearchFragment;->access$1700(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/OnItemClickedListener;

    move-result-object v1

    invoke-interface {v1, p2, p4}, Landroid/support/v17/leanback/widget/OnItemClickedListener;->onItemClicked(Ljava/lang/Object;Landroid/support/v17/leanback/widget/Row;)V

    .line 304
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment$6;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/SearchFragment;->access$1800(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 305
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment$6;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/SearchFragment;->access$1800(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    move-result-object v1

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/support/v17/leanback/widget/OnItemViewClickedListener;->onItemClicked(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V

    .line 308
    :cond_1
    return-void
.end method
