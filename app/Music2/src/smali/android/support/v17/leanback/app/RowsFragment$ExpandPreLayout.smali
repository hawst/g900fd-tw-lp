.class Landroid/support/v17/leanback/app/RowsFragment$ExpandPreLayout;
.super Ljava/lang/Object;
.source "RowsFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/RowsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ExpandPreLayout"
.end annotation


# instance fields
.field final mCallback:Ljava/lang/Runnable;

.field mState:I

.field final mVerticalView:Landroid/view/View;

.field final synthetic this$0:Landroid/support/v17/leanback/app/RowsFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/RowsFragment;Ljava/lang/Runnable;)V
    .locals 1
    .param p2, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 485
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsFragment$ExpandPreLayout;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486
    invoke-virtual {p1}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$ExpandPreLayout;->mVerticalView:Landroid/view/View;

    .line 487
    iput-object p2, p0, Landroid/support/v17/leanback/app/RowsFragment$ExpandPreLayout;->mCallback:Ljava/lang/Runnable;

    .line 488
    return-void
.end method


# virtual methods
.method execute()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 491
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$ExpandPreLayout;->mVerticalView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 492
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$ExpandPreLayout;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/RowsFragment;->setExpand(Z)V

    .line 493
    iput v1, p0, Landroid/support/v17/leanback/app/RowsFragment$ExpandPreLayout;->mState:I

    .line 494
    return-void
.end method

.method public onPreDraw()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 498
    iget v0, p0, Landroid/support/v17/leanback/app/RowsFragment$ExpandPreLayout;->mState:I

    if-nez v0, :cond_1

    .line 499
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$ExpandPreLayout;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/RowsFragment;->setExpand(Z)V

    .line 500
    iput v1, p0, Landroid/support/v17/leanback/app/RowsFragment$ExpandPreLayout;->mState:I

    .line 506
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 501
    :cond_1
    iget v0, p0, Landroid/support/v17/leanback/app/RowsFragment$ExpandPreLayout;->mState:I

    if-ne v0, v1, :cond_0

    .line 502
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$ExpandPreLayout;->mCallback:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 503
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$ExpandPreLayout;->mVerticalView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 504
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/v17/leanback/app/RowsFragment$ExpandPreLayout;->mState:I

    goto :goto_0
.end method
