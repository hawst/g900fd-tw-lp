.class final Landroid/support/v17/leanback/widget/ItemAlignment$Axis;
.super Ljava/lang/Object;
.source "ItemAlignment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/ItemAlignment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Axis"
.end annotation


# instance fields
.field private mOffset:I

.field private mOffsetPercent:F

.field private mOffsetWithPadding:Z

.field private mOrientation:I

.field private mRect:Landroid/graphics/Rect;

.field private mViewId:I


# direct methods
.method constructor <init>(I)V
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput v1, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffset:I

    .line 37
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffsetPercent:F

    .line 38
    iput v1, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mViewId:I

    .line 39
    iput-boolean v1, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffsetWithPadding:Z

    .line 40
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mRect:Landroid/graphics/Rect;

    .line 43
    iput p1, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOrientation:I

    .line 44
    return-void
.end method


# virtual methods
.method public getAlignmentPosition(Landroid/view/View;)I
    .locals 7
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    const/high16 v6, 0x42c80000    # 100.0f

    const/high16 v5, -0x40800000    # -1.0f

    .line 86
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    .line 87
    .local v1, "p":Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;
    move-object v2, p1

    .line 88
    .local v2, "view":Landroid/view/View;
    iget v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mViewId:I

    if-eqz v3, :cond_0

    .line 89
    iget v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mViewId:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 90
    if-nez v2, :cond_0

    .line 91
    move-object v2, p1

    .line 95
    :cond_0
    iget v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOrientation:I

    if-nez v3, :cond_7

    .line 96
    iget v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffset:I

    if-ltz v3, :cond_4

    .line 97
    iget v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffset:I

    .line 98
    .local v0, "alignPos":I
    iget-boolean v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffsetWithPadding:Z

    if-eqz v3, :cond_1

    .line 99
    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    add-int/2addr v0, v3

    .line 108
    :cond_1
    :goto_0
    iget v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffsetPercent:F

    cmpl-float v3, v3, v5

    if-eqz v3, :cond_2

    .line 109
    int-to-float v4, v0

    if-ne v2, p1, :cond_6

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getOpticalWidth(Landroid/view/View;)I

    move-result v3

    :goto_1
    int-to-float v3, v3

    iget v5, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffsetPercent:F

    mul-float/2addr v3, v5

    div-float/2addr v3, v6

    add-float/2addr v3, v4

    float-to-int v0, v3

    .line 112
    :cond_2
    if-eq p1, v2, :cond_3

    .line 113
    iget-object v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mRect:Landroid/graphics/Rect;

    iput v0, v3, Landroid/graphics/Rect;->left:I

    .line 114
    check-cast p1, Landroid/view/ViewGroup;

    .end local p1    # "itemView":Landroid/view/View;
    iget-object v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v2, v3}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 115
    iget-object v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getOpticalLeftInset()I

    move-result v4

    sub-int v0, v3, v4

    .line 140
    :cond_3
    :goto_2
    return v0

    .line 102
    .end local v0    # "alignPos":I
    .restart local p1    # "itemView":Landroid/view/View;
    :cond_4
    if-ne v2, p1, :cond_5

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getOpticalWidth(Landroid/view/View;)I

    move-result v0

    .line 104
    .restart local v0    # "alignPos":I
    :goto_3
    iget-boolean v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffsetWithPadding:Z

    if-eqz v3, :cond_1

    .line 105
    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    sub-int/2addr v0, v3

    goto :goto_0

    .line 102
    .end local v0    # "alignPos":I
    :cond_5
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    iget v4, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffset:I

    add-int v0, v3, v4

    goto :goto_3

    .line 109
    .restart local v0    # "alignPos":I
    :cond_6
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    goto :goto_1

    .line 118
    .end local v0    # "alignPos":I
    :cond_7
    iget v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffset:I

    if-ltz v3, :cond_a

    .line 119
    iget v0, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffset:I

    .line 120
    .restart local v0    # "alignPos":I
    iget-boolean v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffsetWithPadding:Z

    if-eqz v3, :cond_8

    .line 121
    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    add-int/2addr v0, v3

    .line 130
    :cond_8
    :goto_4
    iget v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffsetPercent:F

    cmpl-float v3, v3, v5

    if-eqz v3, :cond_9

    .line 131
    int-to-float v4, v0

    if-ne v2, p1, :cond_c

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getOpticalHeight(Landroid/view/View;)I

    move-result v3

    :goto_5
    int-to-float v3, v3

    iget v5, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffsetPercent:F

    mul-float/2addr v3, v5

    div-float/2addr v3, v6

    add-float/2addr v3, v4

    float-to-int v0, v3

    .line 134
    :cond_9
    if-eq p1, v2, :cond_3

    .line 135
    iget-object v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mRect:Landroid/graphics/Rect;

    iput v0, v3, Landroid/graphics/Rect;->top:I

    .line 136
    check-cast p1, Landroid/view/ViewGroup;

    .end local p1    # "itemView":Landroid/view/View;
    iget-object v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v2, v3}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 137
    iget-object v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getOpticalTopInset()I

    move-result v4

    sub-int v0, v3, v4

    goto :goto_2

    .line 124
    .end local v0    # "alignPos":I
    .restart local p1    # "itemView":Landroid/view/View;
    :cond_a
    if-ne v2, p1, :cond_b

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getOpticalHeight(Landroid/view/View;)I

    move-result v0

    .line 126
    .restart local v0    # "alignPos":I
    :goto_6
    iget-boolean v3, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffsetWithPadding:Z

    if-eqz v3, :cond_8

    .line 127
    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_4

    .line 124
    .end local v0    # "alignPos":I
    :cond_b
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v3

    iget v4, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffset:I

    add-int v0, v3, v4

    goto :goto_6

    .line 131
    .restart local v0    # "alignPos":I
    :cond_c
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v3

    goto :goto_5
.end method

.method public setItemAlignmentOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 47
    iput p1, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffset:I

    .line 48
    return-void
.end method

.method public setItemAlignmentOffsetPercent(F)V
    .locals 1
    .param p1, "percent"    # F

    .prologue
    .line 63
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x42c80000    # 100.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1

    .line 65
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 67
    :cond_1
    iput p1, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffsetPercent:F

    .line 68
    return-void
.end method

.method public setItemAlignmentOffsetWithPadding(Z)V
    .locals 0
    .param p1, "withPadding"    # Z

    .prologue
    .line 55
    iput-boolean p1, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mOffsetWithPadding:Z

    .line 56
    return-void
.end method

.method public setItemAlignmentViewId(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 75
    iput p1, p0, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->mViewId:I

    .line 76
    return-void
.end method
