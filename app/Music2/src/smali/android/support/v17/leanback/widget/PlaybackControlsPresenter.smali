.class Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;
.super Landroid/support/v17/leanback/widget/ControlBarPresenter;
.source "PlaybackControlsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;,
        Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$BoundData;
    }
.end annotation


# static fields
.field private static sChildMarginBigger:I

.field private static sChildMarginBiggest:I


# instance fields
.field private mMoreActionsEnabled:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "layoutResourceId"    # I

    .prologue
    .line 209
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/ControlBarPresenter;-><init>(I)V

    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;->mMoreActionsEnabled:Z

    .line 210
    return-void
.end method

.method static synthetic access$000(ILjava/lang/StringBuilder;)V
    .locals 0
    .param p0, "x0"    # I
    .param p1, "x1"    # Ljava/lang/StringBuilder;

    .prologue
    .line 39
    invoke-static {p0, p1}, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;->formatTime(ILjava/lang/StringBuilder;)V

    return-void
.end method

.method private static formatTime(ILjava/lang/StringBuilder;)V
    .locals 6
    .param p0, "seconds"    # I
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v5, 0x3a

    const/16 v4, 0x30

    const/16 v3, 0xa

    .line 180
    div-int/lit8 v1, p0, 0x3c

    .line 181
    .local v1, "minutes":I
    div-int/lit8 v0, v1, 0x3c

    .line 182
    .local v0, "hours":I
    mul-int/lit8 v2, v1, 0x3c

    sub-int/2addr p0, v2

    .line 183
    mul-int/lit8 v2, v0, 0x3c

    sub-int/2addr v1, v2

    .line 185
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 186
    if-lez v0, :cond_0

    .line 187
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 188
    if-ge v1, v3, :cond_0

    .line 189
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 192
    :cond_0
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 193
    if-ge p0, v3, :cond_1

    .line 194
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 196
    :cond_1
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 197
    return-void
.end method


# virtual methods
.method public enableSecondaryActions(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 218
    iput-boolean p1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;->mMoreActionsEnabled:Z

    .line 219
    return-void
.end method

.method public enableTimeMargins(Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;Z)V
    .locals 3
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;
    .param p2, "enable"    # Z

    .prologue
    const/4 v2, 0x0

    .line 267
    iget-object v1, p1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 268
    .local v0, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    if-eqz p2, :cond_1

    iget v1, p1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mCurrentTimeMarginStart:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 269
    iget-object v1, p1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 271
    iget-object v1, p1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mTotalTime:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 272
    .restart local v0    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    if-eqz p2, :cond_0

    iget v2, p1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mTotalTimeMarginEnd:I

    :cond_0
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 273
    iget-object v1, p1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mTotalTime:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 274
    return-void

    :cond_1
    move v1, v2

    .line 268
    goto :goto_0
.end method

.method getChildMarginBigger(Landroid/content/Context;)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 308
    sget v0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;->sChildMarginBigger:I

    if-nez v0, :cond_0

    .line 309
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$dimen;->lb_playback_controls_child_margin_bigger:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;->sChildMarginBigger:I

    .line 312
    :cond_0
    sget v0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;->sChildMarginBigger:I

    return v0
.end method

.method getChildMarginBiggest(Landroid/content/Context;)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 316
    sget v0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;->sChildMarginBiggest:I

    if-nez v0, :cond_0

    .line 317
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$dimen;->lb_playback_controls_child_margin_biggest:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;->sChildMarginBiggest:I

    .line 320
    :cond_0
    sget v0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;->sChildMarginBiggest:I

    return v0
.end method

.method public onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V
    .locals 4
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 285
    move-object v1, p1

    check-cast v1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;

    .local v1, "vh":Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;
    move-object v0, p2

    .line 286
    check-cast v0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$BoundData;

    .line 289
    .local v0, "data":Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$BoundData;
    iget-object v2, v1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mMoreActionsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget-object v3, v0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$BoundData;->secondaryActionsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-eq v2, v3, :cond_0

    .line 290
    iget-object v2, v0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$BoundData;->secondaryActionsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iput-object v2, v1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mMoreActionsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 291
    iget-object v2, v1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mMoreActionsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget-object v3, v1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mMoreActionsObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 292
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mMoreActionsShowing:Z

    .line 295
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v17/leanback/widget/ControlBarPresenter;->onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V

    .line 296
    iget-boolean v2, p0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;->mMoreActionsEnabled:Z

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->showMoreActions(Z)V

    .line 297
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 278
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;->getLayoutResourceId()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 280
    .local v0, "v":Landroid/view/View;
    new-instance v1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;

    invoke-direct {v1, p0, v0}, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;-><init>(Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;Landroid/view/View;)V

    return-object v1
.end method

.method public onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 301
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/ControlBarPresenter;->onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    move-object v0, p1

    .line 302
    check-cast v0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;

    .line 303
    .local v0, "vh":Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;
    iget-object v1, v0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mMoreActionsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget-object v2, v0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mMoreActionsObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/ObjectAdapter;->unregisterObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 304
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mMoreActionsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 305
    return-void
.end method

.method public setCurrentTime(Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;I)V
    .locals 0
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;
    .param p2, "ms"    # I

    .prologue
    .line 244
    invoke-virtual {p1, p2}, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->setCurrentTime(I)V

    .line 245
    return-void
.end method

.method public setProgressColor(Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;I)V
    .locals 4
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;
    .param p2, "color"    # I

    .prologue
    .line 229
    new-instance v0, Landroid/graphics/drawable/ClipDrawable;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, p2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 231
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v1, p1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/LayerDrawable;

    const v2, 0x102000d

    invoke-virtual {v1, v2, v0}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 233
    return-void
.end method

.method public setSecondaryProgress(Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;I)V
    .locals 0
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;
    .param p2, "progressMs"    # I

    .prologue
    .line 252
    invoke-virtual {p1, p2}, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->setSecondaryProgress(I)V

    .line 253
    return-void
.end method

.method public setTotalTime(Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;I)V
    .locals 0
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;
    .param p2, "ms"    # I

    .prologue
    .line 236
    invoke-virtual {p1, p2}, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->setTotalTime(I)V

    .line 237
    return-void
.end method

.method public showPrimaryActions(Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;)V
    .locals 1
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;

    .prologue
    .line 260
    iget-boolean v0, p1, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->mMoreActionsShowing:Z

    if-eqz v0, :cond_0

    .line 261
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;->toggleMoreActions()V

    .line 263
    :cond_0
    return-void
.end method
