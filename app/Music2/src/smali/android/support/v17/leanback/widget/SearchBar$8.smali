.class Landroid/support/v17/leanback/widget/SearchBar$8;
.super Ljava/lang/Object;
.source "SearchBar.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/widget/SearchBar;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/widget/SearchBar;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/SearchBar;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Landroid/support/v17/leanback/widget/SearchBar$8;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 292
    if-eqz p2, :cond_1

    .line 293
    iget-object v0, p0, Landroid/support/v17/leanback/widget/SearchBar$8;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    # invokes: Landroid/support/v17/leanback/widget/SearchBar;->hideNativeKeyboard()V
    invoke-static {v0}, Landroid/support/v17/leanback/widget/SearchBar;->access$800(Landroid/support/v17/leanback/widget/SearchBar;)V

    .line 294
    iget-object v0, p0, Landroid/support/v17/leanback/widget/SearchBar$8;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->mAutoStartRecognition:Z
    invoke-static {v0}, Landroid/support/v17/leanback/widget/SearchBar;->access$1000(Landroid/support/v17/leanback/widget/SearchBar;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Landroid/support/v17/leanback/widget/SearchBar$8;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/SearchBar;->startRecognition()V

    .line 296
    iget-object v0, p0, Landroid/support/v17/leanback/widget/SearchBar$8;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    const/4 v1, 0x0

    # setter for: Landroid/support/v17/leanback/widget/SearchBar;->mAutoStartRecognition:Z
    invoke-static {v0, v1}, Landroid/support/v17/leanback/widget/SearchBar;->access$1002(Landroid/support/v17/leanback/widget/SearchBar;Z)Z

    .line 301
    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v17/leanback/widget/SearchBar$8;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    # invokes: Landroid/support/v17/leanback/widget/SearchBar;->updateUi()V
    invoke-static {v0}, Landroid/support/v17/leanback/widget/SearchBar;->access$100(Landroid/support/v17/leanback/widget/SearchBar;)V

    .line 302
    return-void

    .line 299
    :cond_1
    iget-object v0, p0, Landroid/support/v17/leanback/widget/SearchBar$8;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/SearchBar;->stopRecognition()V

    goto :goto_0
.end method
