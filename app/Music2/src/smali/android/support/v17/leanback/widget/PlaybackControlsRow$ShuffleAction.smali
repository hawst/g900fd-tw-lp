.class public Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;
.super Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;
.source "PlaybackControlsRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/PlaybackControlsRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShuffleAction"
.end annotation


# static fields
.field public static OFF:I

.field public static ON:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x0

    sput v0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->OFF:I

    .line 363
    const/4 v0, 0x1

    sput v0, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->ON:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "highlightColor"    # I

    .prologue
    .line 380
    sget v3, Landroid/support/v17/leanback/R$id;->lb_control_shuffle:I

    invoke-direct {p0, v3}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$MultiAction;-><init>(I)V

    .line 381
    sget v3, Landroid/support/v17/leanback/R$styleable;->lbPlaybackControlsActionIcons_shuffle:I

    # invokes: Landroid/support/v17/leanback/widget/PlaybackControlsRow;->getStyledDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    invoke-static {p1, v3}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->access$000(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .line 383
    .local v2, "uncoloredDrawable":Landroid/graphics/drawable/BitmapDrawable;
    const/4 v3, 0x2

    new-array v0, v3, [Landroid/graphics/drawable/Drawable;

    .line 384
    .local v0, "drawables":[Landroid/graphics/drawable/Drawable;
    sget v3, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->OFF:I

    aput-object v2, v0, v3

    .line 385
    sget v3, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->ON:I

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    # invokes: Landroid/support/v17/leanback/widget/PlaybackControlsRow;->createBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    invoke-static {v6, p2}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->access$200(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v4, v0, v3

    .line 387
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->setDrawables([Landroid/graphics/drawable/Drawable;)V

    .line 389
    array-length v3, v0

    new-array v1, v3, [Ljava/lang/String;

    .line 390
    .local v1, "labels":[Ljava/lang/String;
    sget v3, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->OFF:I

    sget v4, Landroid/support/v17/leanback/R$string;->lb_playback_controls_shuffle_enable:I

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 391
    sget v3, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->ON:I

    sget v4, Landroid/support/v17/leanback/R$string;->lb_playback_controls_shuffle_disable:I

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 392
    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->setLabels([Ljava/lang/String;)V

    .line 393
    return-void
.end method
