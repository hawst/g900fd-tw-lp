.class public final Landroid/support/v17/leanback/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final lb_action_1_line:I = 0x7f040038

.field public static final lb_action_2_lines:I = 0x7f040039

.field public static final lb_browse_fragment:I = 0x7f04003b

.field public static final lb_card_color_overlay:I = 0x7f04003d

.field public static final lb_control_bar:I = 0x7f04003e

.field public static final lb_control_button_primary:I = 0x7f04003f

.field public static final lb_control_button_secondary:I = 0x7f040040

.field public static final lb_details_description:I = 0x7f040041

.field public static final lb_details_fragment:I = 0x7f040042

.field public static final lb_details_overview:I = 0x7f040043

.field public static final lb_header:I = 0x7f040045

.field public static final lb_headers_fragment:I = 0x7f040046

.field public static final lb_image_card_view:I = 0x7f040047

.field public static final lb_list_row:I = 0x7f040048

.field public static final lb_list_row_hovercard:I = 0x7f040049

.field public static final lb_playback_controls:I = 0x7f04004a

.field public static final lb_playback_controls_row:I = 0x7f04004b

.field public static final lb_row_container:I = 0x7f04004c

.field public static final lb_row_header:I = 0x7f04004d

.field public static final lb_rows_fragment:I = 0x7f04004e

.field public static final lb_search_bar:I = 0x7f04004f

.field public static final lb_search_fragment:I = 0x7f040050

.field public static final lb_search_orb:I = 0x7f040051

.field public static final lb_shadow:I = 0x7f040052

.field public static final lb_speech_orb:I = 0x7f040053

.field public static final lb_title_view:I = 0x7f040054

.field public static final lb_vertical_grid:I = 0x7f040055

.field public static final lb_vertical_grid_fragment:I = 0x7f040056
