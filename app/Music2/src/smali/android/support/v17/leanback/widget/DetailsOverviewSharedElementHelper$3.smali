.class Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;
.super Ljava/lang/Object;
.source "DetailsOverviewSharedElementHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->onBindToDrawable(Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;->this$0:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 167
    iget-object v2, p0, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;->this$0:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;

    # getter for: Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->mViewHolder:Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;
    invoke-static {v2}, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->access$200(Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;)Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;

    move-result-object v2

    iget-object v2, v2, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mOverviewFrame:Landroid/widget/FrameLayout;

    iget-object v3, p0, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;->this$0:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;

    # getter for: Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->mSharedElementName:Ljava/lang/String;
    invoke-static {v3}, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->access$500(Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/support/v4/view/ViewCompat;->setTransitionName(Landroid/view/View;Ljava/lang/String;)V

    .line 168
    invoke-static {}, Landroid/support/v17/leanback/transition/TransitionHelper;->getInstance()Landroid/support/v17/leanback/transition/TransitionHelper;

    move-result-object v1

    .line 169
    .local v1, "transitionHelper":Landroid/support/v17/leanback/transition/TransitionHelper;
    iget-object v2, p0, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;->this$0:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;

    # getter for: Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->mActivityToRunTransition:Landroid/app/Activity;
    invoke-static {v2}, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->access$600(Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->getSharedElementEnterTransition(Landroid/view/Window;)Ljava/lang/Object;

    move-result-object v0

    .line 171
    .local v0, "transition":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 172
    new-instance v2, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3$1;

    invoke-direct {v2, p0, v1}, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3$1;-><init>(Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;Landroid/support/v17/leanback/transition/TransitionHelper;)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->setTransitionListener(Ljava/lang/Object;Landroid/support/v17/leanback/transition/TransitionListener;)V

    .line 187
    :cond_0
    iget-object v2, p0, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;->this$0:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;

    # invokes: Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->startPostponedEnterTransition()V
    invoke-static {v2}, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->access$100(Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;)V

    .line 188
    return-void
.end method
