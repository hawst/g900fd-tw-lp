.class Landroid/support/v17/leanback/app/SearchFragment$5;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/app/SearchFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/SearchFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/SearchFragment;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Landroid/support/v17/leanback/app/SearchFragment$5;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V
    .locals 3
    .param p1, "itemViewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;
    .param p3, "rowViewHolder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p4, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 283
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment$5;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;
    invoke-static {v1}, Landroid/support/v17/leanback/app/SearchFragment;->access$200(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/app/RowsFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getSelectedPosition()I

    move-result v0

    .line 285
    .local v0, "position":I
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment$5;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mSearchBar:Landroid/support/v17/leanback/widget/SearchBar;
    invoke-static {v1}, Landroid/support/v17/leanback/app/SearchFragment;->access$1400(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/SearchBar;

    move-result-object v2

    if-gtz v0, :cond_2

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/support/v17/leanback/widget/SearchBar;->setVisibility(I)V

    .line 286
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment$5;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/SearchFragment;->access$1500(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 287
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment$5;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/SearchFragment;->access$1500(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    move-result-object v1

    invoke-interface {v1, p2, p4}, Landroid/support/v17/leanback/widget/OnItemSelectedListener;->onItemSelected(Ljava/lang/Object;Landroid/support/v17/leanback/widget/Row;)V

    .line 289
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment$5;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/SearchFragment;->access$1600(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 290
    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment$5;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/SearchFragment;->access$1600(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v1

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;->onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V

    .line 293
    :cond_1
    return-void

    .line 285
    :cond_2
    const/16 v1, 0x8

    goto :goto_0
.end method
