.class Landroid/support/v17/leanback/app/SearchFragment$2;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/SearchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/SearchFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/SearchFragment;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Landroid/support/v17/leanback/app/SearchFragment$2;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$2;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;
    invoke-static {v0}, Landroid/support/v17/leanback/app/SearchFragment;->access$200(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/app/RowsFragment;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$2;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;
    invoke-static {v0}, Landroid/support/v17/leanback/app/SearchFragment;->access$200(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/app/RowsFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsFragment;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment$2;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v1}, Landroid/support/v17/leanback/app/SearchFragment;->access$300(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 130
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$2;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;
    invoke-static {v0}, Landroid/support/v17/leanback/app/SearchFragment;->access$200(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/app/RowsFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsFragment;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$2;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v0}, Landroid/support/v17/leanback/app/SearchFragment;->access$300(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$2;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mRowsFragment:Landroid/support/v17/leanback/app/RowsFragment;
    invoke-static {v0}, Landroid/support/v17/leanback/app/SearchFragment;->access$200(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/app/RowsFragment;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/app/SearchFragment$2;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mResultAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v1}, Landroid/support/v17/leanback/app/SearchFragment;->access$300(Landroid/support/v17/leanback/app/SearchFragment;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/RowsFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 134
    :cond_1
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$2;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    const/4 v1, 0x1

    # |= operator for: Landroid/support/v17/leanback/app/SearchFragment;->mStatus:I
    invoke-static {v0, v1}, Landroid/support/v17/leanback/app/SearchFragment;->access$476(Landroid/support/v17/leanback/app/SearchFragment;I)I

    .line 135
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$2;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # getter for: Landroid/support/v17/leanback/app/SearchFragment;->mStatus:I
    invoke-static {v0}, Landroid/support/v17/leanback/app/SearchFragment;->access$400(Landroid/support/v17/leanback/app/SearchFragment;)I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 136
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$2;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # invokes: Landroid/support/v17/leanback/app/SearchFragment;->focusOnResults()V
    invoke-static {v0}, Landroid/support/v17/leanback/app/SearchFragment;->access$500(Landroid/support/v17/leanback/app/SearchFragment;)V

    .line 138
    :cond_2
    iget-object v0, p0, Landroid/support/v17/leanback/app/SearchFragment$2;->this$0:Landroid/support/v17/leanback/app/SearchFragment;

    # invokes: Landroid/support/v17/leanback/app/SearchFragment;->updateSearchBarNextFocusId()V
    invoke-static {v0}, Landroid/support/v17/leanback/app/SearchFragment;->access$600(Landroid/support/v17/leanback/app/SearchFragment;)V

    .line 139
    return-void
.end method
