.class public Landroid/support/v17/leanback/widget/ShadowOverlayContainer;
.super Landroid/view/ViewGroup;
.source "ShadowOverlayContainer.java"


# static fields
.field private static final sTempRect:Landroid/graphics/Rect;


# instance fields
.field private mColorDimOverlay:Landroid/view/View;

.field private mInitialized:Z

.field private mShadowImpl:Ljava/lang/Object;

.field private mWrappedView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->sTempRect:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    return-void
.end method

.method public static prepareParentForShadow(Landroid/view/ViewGroup;)V
    .locals 1
    .param p0, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 80
    invoke-static {}, Landroid/support/v17/leanback/widget/ShadowHelper;->getInstance()Landroid/support/v17/leanback/widget/ShadowHelper;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v17/leanback/widget/ShadowHelper;->prepareParent(Landroid/view/ViewGroup;)V

    .line 81
    return-void
.end method

.method public static supportsShadow()Z
    .locals 1

    .prologue
    .line 71
    invoke-static {}, Landroid/support/v17/leanback/widget/ShadowHelper;->getInstance()Landroid/support/v17/leanback/widget/ShadowHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ShadowHelper;->supportsShadow()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public initialize(ZZZ)V
    .locals 3
    .param p1, "hasShadow"    # Z
    .param p2, "hasColorDimOverlay"    # Z
    .param p3, "roundedCorners"    # Z

    .prologue
    const/4 v2, 0x0

    .line 96
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mInitialized:Z

    if-eqz v0, :cond_0

    .line 97
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 99
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mInitialized:Z

    .line 100
    if-eqz p1, :cond_3

    .line 101
    invoke-static {}, Landroid/support/v17/leanback/widget/ShadowHelper;->getInstance()Landroid/support/v17/leanback/widget/ShadowHelper;

    move-result-object v0

    invoke-virtual {v0, p0, p3}, Landroid/support/v17/leanback/widget/ShadowHelper;->addShadow(Landroid/view/ViewGroup;Z)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mShadowImpl:Ljava/lang/Object;

    .line 106
    :cond_1
    :goto_0
    if-eqz p2, :cond_2

    .line 107
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$layout;->lb_card_color_overlay:I

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mColorDimOverlay:Landroid/view/View;

    .line 109
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mColorDimOverlay:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->addView(Landroid/view/View;)V

    .line 111
    :cond_2
    return-void

    .line 102
    :cond_3
    if-eqz p3, :cond_1

    .line 103
    invoke-static {}, Landroid/support/v17/leanback/widget/RoundedRectHelper;->getInstance()Landroid/support/v17/leanback/widget/RoundedRectHelper;

    move-result-object v0

    invoke-virtual {v0, p0, v2}, Landroid/support/v17/leanback/widget/RoundedRectHelper;->setRoundedRectBackground(Landroid/view/View;I)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v7, 0x0

    .line 203
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->getChildCount()I

    move-result v1

    .line 204
    .local v1, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_1

    .line 205
    invoke-virtual {p0, v3}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 206
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_0

    .line 207
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 208
    .local v4, "width":I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 209
    .local v2, "height":I
    invoke-virtual {v0, v7, v7, v4, v2}, Landroid/view/View;->layout(IIII)V

    .line 204
    .end local v2    # "height":I
    .end local v4    # "width":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 212
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mWrappedView:Landroid/view/View;

    if-eqz v5, :cond_2

    .line 213
    sget-object v5, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->sTempRect:Landroid/graphics/Rect;

    iget-object v6, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mWrappedView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getPivotX()F

    move-result v6

    float-to-int v6, v6

    iput v6, v5, Landroid/graphics/Rect;->left:I

    .line 214
    sget-object v5, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->sTempRect:Landroid/graphics/Rect;

    iget-object v6, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mWrappedView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getPivotY()F

    move-result v6

    float-to-int v6, v6

    iput v6, v5, Landroid/graphics/Rect;->top:I

    .line 215
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mWrappedView:Landroid/view/View;

    sget-object v6, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->sTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v5, v6}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 216
    sget-object v5, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->sTempRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    invoke-virtual {p0, v5}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->setPivotX(F)V

    .line 217
    sget-object v5, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->sTempRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    invoke-virtual {p0, v5}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->setPivotY(F)V

    .line 219
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 153
    iget-object v7, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mWrappedView:Landroid/view/View;

    if-nez v7, :cond_0

    .line 154
    new-instance v7, Ljava/lang/IllegalStateException;

    invoke-direct {v7}, Ljava/lang/IllegalStateException;-><init>()V

    throw v7

    .line 159
    :cond_0
    iget-object v7, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mWrappedView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 160
    .local v4, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v7, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v7, v8, :cond_1

    .line 161
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-static {v7, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 166
    .local v2, "childWidthMeasureSpec":I
    :goto_0
    iget v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v7, v8, :cond_2

    .line 167
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-static {v7, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 172
    .local v1, "childHeightMeasureSpec":I
    :goto_1
    iget-object v7, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mWrappedView:Landroid/view/View;

    invoke-virtual {v7, v2, v1}, Landroid/view/View;->measure(II)V

    .line 174
    iget-object v7, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mWrappedView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 175
    .local v6, "measuredWidth":I
    iget-object v7, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mWrappedView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 177
    .local v5, "measuredHeight":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->getChildCount()I

    move-result v7

    if-ge v3, v7, :cond_6

    .line 178
    invoke-virtual {p0, v3}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 179
    .local v0, "child":Landroid/view/View;
    iget-object v7, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mWrappedView:Landroid/view/View;

    if-ne v0, v7, :cond_3

    .line 177
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 164
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childHeightMeasureSpec":I
    .end local v2    # "childWidthMeasureSpec":I
    .end local v3    # "i":I
    .end local v5    # "measuredHeight":I
    .end local v6    # "measuredWidth":I
    :cond_1
    iget v7, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p1, v9, v7}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->getChildMeasureSpec(III)I

    move-result v2

    .restart local v2    # "childWidthMeasureSpec":I
    goto :goto_0

    .line 170
    :cond_2
    iget v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v9, v7}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->getChildMeasureSpec(III)I

    move-result v1

    .restart local v1    # "childHeightMeasureSpec":I
    goto :goto_1

    .line 182
    .restart local v0    # "child":Landroid/view/View;
    .restart local v3    # "i":I
    .restart local v5    # "measuredHeight":I
    .restart local v6    # "measuredWidth":I
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 183
    iget v7, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v7, v8, :cond_4

    .line 184
    invoke-static {v6, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 190
    :goto_4
    iget v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v7, v8, :cond_5

    .line 191
    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 196
    :goto_5
    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    goto :goto_3

    .line 187
    :cond_4
    iget v7, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p1, v9, v7}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->getChildMeasureSpec(III)I

    move-result v2

    goto :goto_4

    .line 194
    :cond_5
    iget v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v9, v7}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->getChildMeasureSpec(III)I

    move-result v1

    goto :goto_5

    .line 198
    .end local v0    # "child":Landroid/view/View;
    :cond_6
    invoke-virtual {p0, v6, v5}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->setMeasuredDimension(II)V

    .line 199
    return-void
.end method

.method public setOverlayColor(I)V
    .locals 1
    .param p1, "overlayColor"    # I

    .prologue
    .line 131
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mColorDimOverlay:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mColorDimOverlay:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 134
    :cond_0
    return-void
.end method

.method public setShadowFocusLevel(F)V
    .locals 2
    .param p1, "level"    # F

    .prologue
    .line 117
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mShadowImpl:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 118
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2

    .line 119
    const/4 p1, 0x0

    .line 123
    :cond_0
    :goto_0
    invoke-static {}, Landroid/support/v17/leanback/widget/ShadowHelper;->getInstance()Landroid/support/v17/leanback/widget/ShadowHelper;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mShadowImpl:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Landroid/support/v17/leanback/widget/ShadowHelper;->setShadowFocusLevel(Ljava/lang/Object;F)V

    .line 125
    :cond_1
    return-void

    .line 120
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 121
    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public wrap(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 140
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mInitialized:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mWrappedView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 141
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 143
    :cond_1
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mColorDimOverlay:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 144
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mColorDimOverlay:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->addView(Landroid/view/View;I)V

    .line 148
    :goto_0
    iput-object p1, p0, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->mWrappedView:Landroid/view/View;

    .line 149
    return-void

    .line 146
    :cond_2
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
