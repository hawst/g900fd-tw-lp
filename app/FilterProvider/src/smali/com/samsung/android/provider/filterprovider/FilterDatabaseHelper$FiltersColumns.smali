.class interface abstract Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper$FiltersColumns;
.super Ljava/lang/Object;
.source "FilterDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "FiltersColumns"
.end annotation


# static fields
.field public static final APK_ID:Ljava/lang/String; = "package_id"

.field public static final CONCRETE_FILENAME:Ljava/lang/String; = "filters.filename"

.field public static final CONCRETE_ICON:Ljava/lang/String; = "filters.icon"

.field public static final CONCRETE_ID:Ljava/lang/String; = "filters._ID"

.field public static final CONCRETE_NAME:Ljava/lang/String; = "filters.name"

.field public static final DELETED:Ljava/lang/String; = "deleted"

.field public static final FAVORITE:Ljava/lang/String; = "favorite"

.field public static final FILENAME:Ljava/lang/String; = "filename"

.field public static final FILTER_ORDER:Ljava/lang/String; = "filter_order"

.field public static final FILTER_TYPE:Ljava/lang/String; = "filter_type"

.field public static final HANDLER:Ljava/lang/String; = "handler"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final LIVE_TYPE:Ljava/lang/String; = "livepost"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PACKAGE_ICON:Ljava/lang/String; = "package_icon"

.field public static final PACKAGE_ID:Ljava/lang/String; = "package_id"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field public static final POSX:Ljava/lang/String; = "posx"

.field public static final POSY:Ljava/lang/String; = "posy"

.field public static final VENDOR:Ljava/lang/String; = "vendor"

.field public static final VERSION:Ljava/lang/String; = "version"

.field public static final WIDTH:Ljava/lang/String; = "width"

.field public static final _ID:Ljava/lang/String; = "_ID"
