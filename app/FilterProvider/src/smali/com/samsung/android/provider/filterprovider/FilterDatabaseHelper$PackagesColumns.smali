.class interface abstract Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper$PackagesColumns;
.super Ljava/lang/Object;
.source "FilterDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "PackagesColumns"
.end annotation


# static fields
.field public static final CONCRETE_ICON:Ljava/lang/String; = "packages.icon"

.field public static final CONCRETE_ID:Ljava/lang/String; = "packages._ID"

.field public static final CONCRETE_NAME:Ljava/lang/String; = "packages.name"

.field public static final FILTER_COUNT:Ljava/lang/String; = "filter_count"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final NOT_USED_COUNT:Ljava/lang/String; = "not_used_count"

.field public static final _ID:Ljava/lang/String; = "_ID"
