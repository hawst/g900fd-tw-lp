.class public interface abstract Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper$Views;
.super Ljava/lang/Object;
.source "FilterDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Views"
.end annotation


# static fields
.field public static final COUNTS:Ljava/lang/String; = "view_counts"

.field public static final FILTERS:Ljava/lang/String; = "view_filters"

.field public static final PACKAGES:Ljava/lang/String; = "view_packages"

.field public static final TITLES:Ljava/lang/String; = "view_titles"
