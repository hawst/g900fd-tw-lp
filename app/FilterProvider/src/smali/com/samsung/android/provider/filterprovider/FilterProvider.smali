.class public Lcom/samsung/android/provider/filterprovider/FilterProvider;
.super Landroid/content/ContentProvider;
.source "FilterProvider.java"


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.provider.filterprovider"

.field public static final AUTHORITY_URI:Landroid/net/Uri;

.field private static final DBNAME:Ljava/lang/String; = "filter.db"

.field private static final DBVERSION:I = 0xf

.field private static final FILTERS:I = 0x3e8

.field public static final FILTERS_CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/filters"

.field public static final FILTERS_CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/filters"

.field private static final FILTERS_FAVORITES:I = 0x3ea

.field private static final FILTERS_ID:I = 0x3e9

.field private static final FILTERS_INCDEL:I = 0x3ee

.field private static final FILTERS_LIVE:I = 0x3eb

.field private static final FILTERS_NAME:I = 0x3ed

.field private static final FILTERS_POST:I = 0x3ec

.field public static final FILTERS_URI:Landroid/net/Uri;

.field private static final FILTER_ORDER:Ljava/lang/String; = "filter_order"

.field private static final ICONS:I = 0xbb8

.field public static final ICONS_CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/icons"

.field public static final ICONS_CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/icons"

.field private static final ICONS_ID:I = 0xbb9

.field private static final PACKAGES:I = 0x7d0

.field public static final PACKAGES_CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/packages"

.field public static final PACKAGES_CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/packages"

.field private static final PACKAGES_FILTERS:I = 0x7d3

.field private static final PACKAGES_ID:I = 0x7d1

.field private static final PACKAGES_ID_FILTERS:I = 0x7d4

.field private static final PACKAGES_NAME:I = 0x7d2

.field public static final PRELOAD_FILTER_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.filter"

.field private static final TEXTURES:I = 0x1388

.field public static final TEXTURES_CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/textures"

.field public static final TEXTURES_CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/textures"

.field private static final TEXTURES_ID:I = 0x1389

.field private static final TITLES:I = 0xfa0

.field public static final TITLES_CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/titles"

.field public static final TITLES_CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/titles"

.field private static final TITLES_ID:I = 0xfa1

.field private static final sUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

.field private final mValues:Landroid/content/ContentValues;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 51
    const-string v1, "content://com.samsung.android.provider.filterprovider"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/provider/filterprovider/FilterProvider;->AUTHORITY_URI:Landroid/net/Uri;

    .line 52
    sget-object v1, Lcom/samsung/android/provider/filterprovider/FilterProvider;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v2, "filters"

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/provider/filterprovider/FilterProvider;->FILTERS_URI:Landroid/net/Uri;

    .line 54
    new-instance v1, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v1, Lcom/samsung/android/provider/filterprovider/FilterProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 95
    sget-object v0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 97
    .local v0, "matcher":Landroid/content/UriMatcher;
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "filters"

    const/16 v3, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 98
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "filters/favorites"

    const/16 v3, 0x3ea

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 99
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "filters/live"

    const/16 v3, 0x3eb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 100
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "filters/post"

    const/16 v3, 0x3ec

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 101
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "filters/include_deleted"

    const/16 v3, 0x3ee

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 102
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "filters/#"

    const/16 v3, 0x3e9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 103
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "filters/*"

    const/16 v3, 0x3ed

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 105
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "packages"

    const/16 v3, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 106
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "packages/*"

    const/16 v3, 0x7d2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 107
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "packages/*/filters"

    const/16 v3, 0x7d3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 108
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "packages/#"

    const/16 v3, 0x7d1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 109
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "packages/#/filters"

    const/16 v3, 0x7d4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 111
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "icons"

    const/16 v3, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 112
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "icons/#"

    const/16 v3, 0xbb9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 114
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "titles"

    const/16 v3, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 115
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "titles/#"

    const/16 v3, 0xfa1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 117
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "textures"

    const/16 v3, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 118
    const-string v1, "com.samsung.android.provider.filterprovider"

    const-string v2, "textures/#"

    const/16 v3, 0x1389

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 119
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 331
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mValues:Landroid/content/ContentValues;

    return-void
.end method

.method private DeleteFilterById(Ljava/lang/String;)I
    .locals 4
    .param p1, "filterId"    # Ljava/lang/String;

    .prologue
    .line 686
    iget-object v2, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v2}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 688
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 690
    .local v1, "selectionArgs":[Ljava/lang/String;
    const-string v2, "filters"

    const-string v3, "_ID = ?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 692
    const-string v2, "icons"

    const-string v3, "_ID = ?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 694
    const-string v2, "titles"

    const-string v3, "_ID = ?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 696
    const-string v2, "textures"

    const-string v3, "_ID = ?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method private DeleteFilterByName(Ljava/lang/String;)I
    .locals 5
    .param p1, "filterName"    # Ljava/lang/String;

    .prologue
    .line 669
    iget-object v3, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v3}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 671
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, p1}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->queryFilterId(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 673
    .local v1, "filterId":Ljava/lang/Long;
    const/4 v3, 0x1

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 675
    .local v2, "selectionArgs":[Ljava/lang/String;
    const-string v3, "filters"

    const-string v4, "_ID = ?"

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 677
    const-string v3, "icons"

    const-string v4, "_ID = ?"

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 679
    const-string v3, "titles"

    const-string v4, "_ID = ?"

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 681
    const-string v3, "textures"

    const-string v4, "_ID = ?"

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    return v3
.end method

.method private deletePackageAndItsFilters(Ljava/lang/Long;)I
    .locals 4
    .param p1, "packageId"    # Ljava/lang/Long;

    .prologue
    .line 701
    iget-object v2, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v2}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 703
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 705
    .local v1, "selectionArgs":[Ljava/lang/String;
    const-string v2, "filters"

    const-string v3, "package_id = ?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 707
    const-string v2, "icons"

    const-string v3, "package_id = ?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 709
    const-string v2, "titles"

    const-string v3, "package_id = ?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 711
    const-string v2, "textures"

    const-string v3, "package_id = ?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 713
    const-string v2, "packages"

    const-string v3, "_ID = ?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method private getPackageId(Ljava/lang/String;)J
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 282
    const-wide/16 v8, -0x1

    .line 284
    .local v8, "apkId":J
    iget-object v1, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v1}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 285
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "packages"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_ID"

    aput-object v3, v2, v6

    const-string v3, "name = ?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 292
    .local v10, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 293
    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 297
    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 300
    return-wide v8

    .line 297
    :catchall_0
    move-exception v1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private insertFilters(Landroid/content/ContentValues;)J
    .locals 9
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 333
    iget-object v0, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 334
    iget-object v0, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v0, p1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 336
    iget-object v0, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mValues:Landroid/content/ContentValues;

    const-string v1, "favorite"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 338
    iget-object v0, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mValues:Landroid/content/ContentValues;

    invoke-direct {p0, v0}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->replacePackageNameWithID(Landroid/content/ContentValues;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    const-wide/16 v0, -0x1

    .line 355
    :goto_0
    return-wide v0

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v0}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "filters"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "(SELECT MAX(filter_order) FROM filters)"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 347
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    iget-object v0, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mValues:Landroid/content/ContentValues;

    const-string v1, "filter_order"

    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v0}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "filters"

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 354
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 355
    :cond_2
    const/4 v8, 0x0

    goto :goto_0

    .line 354
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 355
    :cond_3
    const/4 v8, 0x0

    throw v0
.end method

.method private insertIcons(Landroid/content/ContentValues;)J
    .locals 10
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const-wide/16 v2, -0x1

    .line 304
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 306
    .local v1, "filterId":Ljava/lang/Long;
    const-string v5, "filter_id"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-wide v2

    .line 309
    :cond_1
    const-string v5, "filter_id"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 311
    const-string v5, "filter_id"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 314
    invoke-direct {p0, p1}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->replacePackageNameWithID(Landroid/content/ContentValues;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 318
    iget-object v5, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v5}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 319
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v5, "icons"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 321
    .local v2, "iconId":J
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 322
    .local v4, "iconValues":Landroid/content/ContentValues;
    const-string v5, "icon"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 324
    const-string v5, "filters"

    const-string v6, "_ID = ?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v0, v5, v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private insertPackages(Landroid/content/ContentValues;)J
    .locals 18
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 360
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v3}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 361
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 363
    .local v14, "iconValues":Landroid/content/ContentValues;
    const-wide/16 v12, -0x1

    .line 366
    .local v12, "iconId":J
    const-string v3, "icon"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 367
    const-string v3, "icon"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 369
    .local v11, "filename":Ljava/lang/String;
    const-string v3, "icons"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_ID"

    aput-object v6, v4, v5

    const-string v5, "filename=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v11, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 374
    .local v10, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 375
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v12

    .line 378
    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 381
    const-wide/16 v4, -0x1

    cmp-long v3, v12, v4

    if-nez v3, :cond_1

    .line 382
    const-string v3, "filename"

    invoke-virtual {v14, v3, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    const-string v3, "icons"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v14}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v12

    .line 387
    :cond_1
    const-string v3, "icon"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 390
    .end local v10    # "c":Landroid/database/Cursor;
    .end local v11    # "filename":Ljava/lang/String;
    :cond_2
    const-wide/16 v16, -0x1

    .line 392
    .local v16, "packageId":J
    const-string v3, "name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 393
    .local v15, "packageName":Ljava/lang/String;
    const-string v3, "packages"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_ID"

    aput-object v6, v4, v5

    const-string v5, "name = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v15, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 398
    .restart local v10    # "c":Landroid/database/Cursor;
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 399
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v16

    .line 402
    :cond_3
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 405
    const-wide/16 v4, -0x1

    cmp-long v3, v16, v4

    if-nez v3, :cond_4

    .line 406
    const-string v3, "packages"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v16

    .line 413
    :goto_0
    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    .line 414
    const-string v3, "package_id"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v14, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 415
    const-string v3, "icons"

    const-string v4, "_ID = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v14, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 418
    return-wide v16

    .line 378
    .end local v15    # "packageName":Ljava/lang/String;
    .end local v16    # "packageId":J
    .restart local v11    # "filename":Ljava/lang/String;
    :catchall_0
    move-exception v3

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v3

    .line 402
    .end local v11    # "filename":Ljava/lang/String;
    .restart local v15    # "packageName":Ljava/lang/String;
    .restart local v16    # "packageId":J
    :catchall_1
    move-exception v3

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v3

    .line 408
    :cond_4
    const-string v3, "packages"

    const-string v4, "_ID = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p1

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private insertSelectionArg([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 5
    .param p1, "selectionArgs"    # [Ljava/lang/String;
    .param p2, "arg"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 423
    if-nez p1, :cond_0

    .line 424
    new-array v1, v4, [Ljava/lang/String;

    aput-object p2, v1, v3

    .line 431
    :goto_0
    return-object v1

    .line 426
    :cond_0
    array-length v2, p1

    add-int/lit8 v0, v2, 0x1

    .line 427
    .local v0, "newLength":I
    new-array v1, v0, [Ljava/lang/String;

    .line 428
    .local v1, "newSelectionArgs":[Ljava/lang/String;
    aput-object p2, v1, v3

    .line 429
    array-length v2, p1

    invoke-static {p1, v3, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method private insertTextures(Landroid/content/ContentValues;)J
    .locals 4
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 264
    invoke-direct {p0, p1}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->replacePackageNameWithID(Landroid/content/ContentValues;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 265
    const-wide/16 v2, -0x1

    .line 269
    :goto_0
    return-wide v2

    .line 268
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v1}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 269
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "textures"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    goto :goto_0
.end method

.method private insertTitles(Landroid/content/ContentValues;)J
    .locals 4
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 273
    invoke-direct {p0, p1}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->replacePackageNameWithID(Landroid/content/ContentValues;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 274
    const-wide/16 v2, -0x1

    .line 278
    :goto_0
    return-wide v2

    .line 277
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v1}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 278
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "titles"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    goto :goto_0
.end method

.method private queryFilterId(Ljava/lang/String;)Ljava/lang/Long;
    .locals 9
    .param p1, "filterName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 737
    iget-object v1, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v1}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 738
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "packages"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_ID"

    aput-object v3, v2, v6

    const-string v3, "name = ?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 745
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 746
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 749
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 752
    :goto_0
    return-object v1

    .line 749
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 752
    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 749
    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private queryPackageId(Ljava/lang/String;)Ljava/lang/Long;
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 718
    iget-object v1, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v1}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 719
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "packages"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_ID"

    aput-object v3, v2, v6

    const-string v3, "name = ?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 726
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 727
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 730
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 733
    :goto_0
    return-object v1

    .line 730
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 733
    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 730
    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private replacePackageNameWithID(Landroid/content/ContentValues;)Z
    .locals 6
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, 0x0

    .line 244
    const-wide/16 v0, -0x1

    .line 246
    .local v0, "apkId":J
    const-string v4, "package_name"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 247
    const-string v4, "package_name"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 249
    .local v2, "packageName":Ljava/lang/String;
    const-string v4, "package_name"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 251
    invoke-direct {p0, v2}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->getPackageId(Ljava/lang/String;)J

    move-result-wide v0

    .line 252
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gez v4, :cond_1

    .line 259
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 256
    .restart local v2    # "packageName":Ljava/lang/String;
    :cond_1
    const-string v3, "package_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 257
    const/4 v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 623
    sget-object v6, Lcom/samsung/android/provider/filterprovider/FilterProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    .line 624
    .local v3, "match":I
    const/4 v0, 0x0

    .line 626
    .local v0, "count":I
    sparse-switch v3, :sswitch_data_0

    .line 655
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown URI "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 628
    :sswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    .line 629
    .local v5, "packageName":Ljava/lang/String;
    const-string v6, "com.sec.android.filter"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 630
    iget-object v6, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v6}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->close()V

    .line 631
    const-string v6, "FilterProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "reset DB = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mContext:Landroid/content/Context;

    const-string v9, "/data/data/com.samsung.android.provider.filterprovider/databases/filter.db"

    invoke-virtual {v8, v9}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mContext:Landroid/content/Context;

    const-string v9, "/data/data/com.samsung.android.provider.filterprovider/databases/filter.db-journal"

    invoke-virtual {v8, v9}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    :cond_0
    invoke-direct {p0, v5}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->queryPackageId(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    .line 635
    .local v4, "packageId":Ljava/lang/Long;
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-ltz v6, :cond_1

    .line 636
    invoke-direct {p0, v4}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->deletePackageAndItsFilters(Ljava/lang/Long;)I

    move-result v0

    .line 662
    .end local v4    # "packageId":Ljava/lang/Long;
    .end local v5    # "packageName":Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 665
    return v0

    .line 642
    :sswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 643
    .local v2, "filterName":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->DeleteFilterByName(Ljava/lang/String;)I

    move-result v0

    .line 644
    goto :goto_0

    .line 648
    .end local v2    # "filterName":Ljava/lang/String;
    :sswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 649
    .local v1, "filterId":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->DeleteFilterById(Ljava/lang/String;)I

    move-result v0

    .line 650
    goto :goto_0

    .line 626
    nop

    :sswitch_data_0
    .sparse-switch
        0x3e9 -> :sswitch_2
        0x3ed -> :sswitch_1
        0x7d2 -> :sswitch_0
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 123
    sget-object v1, Lcom/samsung/android/provider/filterprovider/FilterProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 124
    .local v0, "match":I
    sparse-switch v0, :sswitch_data_0

    .line 162
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 130
    :sswitch_0
    const-string v1, "vnd.android.cursor.dir/filters"

    goto :goto_0

    .line 133
    :sswitch_1
    const-string v1, "vnd.android.cursor.item/filters"

    goto :goto_0

    .line 139
    :sswitch_2
    const-string v1, "vnd.android.cursor.dir/packages"

    goto :goto_0

    .line 142
    :sswitch_3
    const-string v1, "vnd.android.cursor.item/packages"

    goto :goto_0

    .line 145
    :sswitch_4
    const-string v1, "vnd.android.cursor.dir/icons"

    goto :goto_0

    .line 148
    :sswitch_5
    const-string v1, "vnd.android.cursor.item/icons"

    goto :goto_0

    .line 151
    :sswitch_6
    const-string v1, "vnd.android.cursor.dir/titles"

    goto :goto_0

    .line 154
    :sswitch_7
    const-string v1, "vnd.android.cursor.item/titles"

    goto :goto_0

    .line 157
    :sswitch_8
    const-string v1, "vnd.android.cursor.dir/textures"

    goto :goto_0

    .line 160
    :sswitch_9
    const-string v1, "vnd.android.cursor.item/textures"

    goto :goto_0

    .line 124
    nop

    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_0
        0x3e9 -> :sswitch_1
        0x3ea -> :sswitch_0
        0x3eb -> :sswitch_0
        0x3ec -> :sswitch_0
        0x3ee -> :sswitch_0
        0x7d0 -> :sswitch_2
        0x7d1 -> :sswitch_3
        0x7d2 -> :sswitch_2
        0x7d3 -> :sswitch_2
        0x7d4 -> :sswitch_2
        0xbb8 -> :sswitch_4
        0xbb9 -> :sswitch_5
        0xfa0 -> :sswitch_6
        0xfa1 -> :sswitch_7
        0x1388 -> :sswitch_8
        0x1389 -> :sswitch_9
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v5, 0x0

    .line 180
    sget-object v6, Lcom/samsung/android/provider/filterprovider/FilterProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    .line 181
    .local v4, "match":I
    const/4 v0, 0x0

    .line 183
    .local v0, "effectiveUri":Landroid/net/Uri;
    const-wide/16 v2, 0x0

    .line 184
    .local v2, "id":J
    sparse-switch v4, :sswitch_data_0

    move-object v1, v5

    .line 236
    :cond_0
    :goto_0
    return-object v1

    .line 186
    :sswitch_0
    invoke-direct {p0, p2}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->insertFilters(Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 228
    :cond_1
    :goto_1
    cmp-long v6, v2, v8

    if-lez v6, :cond_2

    .line 229
    invoke-static {p1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 230
    .local v1, "insertedUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v6, v1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 232
    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v6, v0, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 191
    .end local v1    # "insertedUri":Landroid/net/Uri;
    :sswitch_1
    invoke-direct {p0, p2}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->insertPackages(Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 192
    goto :goto_1

    .line 196
    :sswitch_2
    invoke-direct {p0, p2}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->insertIcons(Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 197
    cmp-long v6, v2, v8

    if-lez v6, :cond_1

    .line 199
    const-string v6, "filter_id"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 200
    sget-object v6, Lcom/samsung/android/provider/filterprovider/FilterProvider;->FILTERS_URI:Landroid/net/Uri;

    const-string v7, "filter_id"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 208
    :sswitch_3
    invoke-direct {p0, p2}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->insertTitles(Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 209
    cmp-long v6, v2, v8

    if-lez v6, :cond_1

    .line 211
    const-string v6, "filter_id"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 212
    sget-object v6, Lcom/samsung/android/provider/filterprovider/FilterProvider;->FILTERS_URI:Landroid/net/Uri;

    const-string v7, "filter_id"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 220
    :sswitch_4
    invoke-direct {p0, p2}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->insertTextures(Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 221
    goto :goto_1

    .line 240
    :cond_2
    new-instance v5, Landroid/database/SQLException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to insert row into "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 184
    nop

    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_0
        0x7d0 -> :sswitch_1
        0xbb8 -> :sswitch_2
        0xfa0 -> :sswitch_3
        0x1388 -> :sswitch_4
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 5

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mContext:Landroid/content/Context;

    .line 169
    new-instance v0, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    iget-object v1, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mContext:Landroid/content/Context;

    const-string v2, "filter.db"

    const/4 v3, 0x0

    const/16 v4, 0xf

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object v0, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    .line 175
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 15
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 438
    iget-object v3, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v3}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 440
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "view_filters"

    .line 442
    .local v2, "tableName":Ljava/lang/String;
    sget-object v3, Lcom/samsung/android/provider/filterprovider/FilterProvider;->sUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v11

    .line 443
    .local v11, "match":I
    sparse-switch v11, :sswitch_data_0

    .line 552
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 445
    :sswitch_0
    const-string v2, "view_packages"

    .line 555
    :goto_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 559
    .local v9, "c":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v9, v3, v0}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 560
    return-object v9

    .line 451
    .end local v9    # "c":Landroid/database/Cursor;
    :sswitch_1
    const/16 v3, 0x3e8

    if-ne v11, v3, :cond_0

    .line 452
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 453
    const-string p3, "deleted = 0"

    .line 460
    :cond_0
    :goto_1
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 461
    const-string p5, "filter_order"

    goto :goto_0

    .line 455
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleted = 0 AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 463
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "filter_order, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 465
    goto :goto_0

    .line 469
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v13

    .line 470
    .local v13, "paths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-interface {v13, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 472
    .local v12, "packageName":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 473
    const-string p3, "package_name = ?"

    .line 478
    :goto_2
    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->insertSelectionArg([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 480
    goto :goto_0

    .line 475
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "package_name = ? AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_2

    .line 484
    .end local v12    # "packageName":Ljava/lang/String;
    .end local v13    # "paths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_3
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 485
    const-string p3, "livepost = 1"

    goto/16 :goto_0

    .line 487
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "livepost"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = 1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 490
    goto/16 :goto_0

    .line 494
    :sswitch_4
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 495
    const-string p3, "livepost = 2"

    goto/16 :goto_0

    .line 497
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "livepost"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = 2"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 500
    goto/16 :goto_0

    .line 504
    :sswitch_5
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 505
    const-string p3, "favorite = 1"

    goto/16 :goto_0

    .line 507
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "favorite"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = 1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 510
    goto/16 :goto_0

    .line 514
    :sswitch_6
    const-string v2, "icons"

    .line 515
    goto/16 :goto_0

    .line 519
    :sswitch_7
    const-string v2, "icons"

    .line 520
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v10

    .line 522
    .local v10, "iconId":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 523
    const-string p3, "_ID = ?"

    .line 528
    :goto_3
    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->insertSelectionArg([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 529
    goto/16 :goto_0

    .line 525
    :cond_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_ID = ? AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_3

    .line 533
    .end local v10    # "iconId":Ljava/lang/String;
    :sswitch_8
    const-string v2, "textures"

    .line 534
    goto/16 :goto_0

    .line 538
    :sswitch_9
    const-string v2, "textures"

    .line 539
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v14

    .line 541
    .local v14, "textureId":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 542
    const-string p3, "_ID = ?"

    .line 547
    :goto_4
    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->insertSelectionArg([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 548
    goto/16 :goto_0

    .line 544
    :cond_8
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_ID = ? AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_4

    .line 443
    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_1
        0x3ea -> :sswitch_5
        0x3eb -> :sswitch_3
        0x3ec -> :sswitch_4
        0x3ee -> :sswitch_1
        0x7d0 -> :sswitch_0
        0x7d3 -> :sswitch_2
        0xbb8 -> :sswitch_6
        0xbb9 -> :sswitch_7
        0x1388 -> :sswitch_8
        0x1389 -> :sswitch_9
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 566
    sget-object v3, Lcom/samsung/android/provider/filterprovider/FilterProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 567
    .local v2, "match":I
    const/4 v0, 0x0

    .line 569
    .local v0, "count":I
    sparse-switch v2, :sswitch_data_0

    .line 618
    :goto_0
    return v0

    .line 571
    :sswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 572
    .local v1, "id":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 573
    const-string p3, "_ID = ?"

    .line 579
    :goto_1
    invoke-direct {p0, p4, v1}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->insertSelectionArg([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 581
    iget-object v3, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v3}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "filters"

    invoke-virtual {v3, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 583
    invoke-virtual {p0}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, p1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 575
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_ID = ? AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 589
    .end local v1    # "id":Ljava/lang/String;
    :sswitch_1
    iget-object v3, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v3}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "filters"

    invoke-virtual {v3, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 591
    invoke-virtual {p0}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, p1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 596
    :sswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 597
    .restart local v1    # "id":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 598
    const-string p3, "_ID = ?"

    .line 604
    :goto_2
    invoke-direct {p0, p4, v1}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->insertSelectionArg([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 606
    iget-object v3, p0, Lcom/samsung/android/provider/filterprovider/FilterProvider;->mDbHelper:Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;

    invoke-virtual {v3}, Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "titles"

    invoke-virtual {v3, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 609
    invoke-virtual {p0}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, p1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 610
    invoke-virtual {p0}, Lcom/samsung/android/provider/filterprovider/FilterProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/provider/filterprovider/FilterProvider;->FILTERS_URI:Landroid/net/Uri;

    invoke-static {v4, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_0

    .line 600
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_ID = ? AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_2

    .line 569
    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_1
        0x3e9 -> :sswitch_0
        0xfa1 -> :sswitch_2
    .end sparse-switch
.end method
