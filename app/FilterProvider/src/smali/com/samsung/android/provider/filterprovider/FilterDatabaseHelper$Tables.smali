.class public interface abstract Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper$Tables;
.super Ljava/lang/Object;
.source "FilterDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Tables"
.end annotation


# static fields
.field public static final FILTERS:Ljava/lang/String; = "filters"

.field public static final ICONS:Ljava/lang/String; = "icons"

.field public static final PACKAGES:Ljava/lang/String; = "packages"

.field public static final TEXTURES:Ljava/lang/String; = "textures"

.field public static final TITLES:Ljava/lang/String; = "titles"
