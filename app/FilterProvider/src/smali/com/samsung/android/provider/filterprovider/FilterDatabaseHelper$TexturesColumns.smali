.class interface abstract Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper$TexturesColumns;
.super Ljava/lang/Object;
.source "FilterDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "TexturesColumns"
.end annotation


# static fields
.field public static final APK_ID:Ljava/lang/String; = "package_id"

.field public static final CONCRETE_ID:Ljava/lang/String; = "textures._ID"

.field public static final FILENAME:Ljava/lang/String; = "filename"

.field public static final FILTER_ID:Ljava/lang/String; = "filter_id"

.field public static final _ID:Ljava/lang/String; = "_ID"
