.class interface abstract Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper$TitlesColumns;
.super Ljava/lang/Object;
.source "FilterDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/provider/filterprovider/FilterDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "TitlesColumns"
.end annotation


# static fields
.field public static final APK_ID:Ljava/lang/String; = "package_id"

.field public static final CONCRETE_ID:Ljava/lang/String; = "titles._ID"

.field public static final COUNTRY:Ljava/lang/String; = "country"

.field public static final FILTER_ID:Ljava/lang/String; = "filter_id"

.field public static final RSRC_NAME:Ljava/lang/String; = "resource_name"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final _ID:Ljava/lang/String; = "_ID"
