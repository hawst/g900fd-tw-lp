if ((typeof fido !== "undefined") && fido && fido.ready && (!window.hasOwnProperty('injectedfidojs'))) {
    window.injectedfidojs = true;
	
	fido.injected = new InjectedFido();
	function InjectedFido() {
	
		//client Version Info
		this.clientVersionInfo = {"JSAPIVer": "1.0"};
		
		//helpers
		this.isVersionInRange = function (range) {
			var versionArr = this.clientVersionInfo.JSAPIVer.split(".");
			var startArr = range.start.split(".");
			var endArr = range.end.split(".");
			var bFoundInRange = false;
			for (var i = 0; i < Math.min(versionArr.length, startArr.length); i++) {
				if (parseInt(versionArr[i]) > parseInt(startArr[i])) {
					bFoundInRange = true;
					break;
				}
				else if (parseInt(versionArr[i]) < parseInt(startArr[i]))
					return false;
			};

			if (!bFoundInRange && versionArr.length < startArr.length)
				return false;
			bFoundInRange = false;
			for (var i = 0; i < Math.min(versionArr.length, endArr.length); i++) {
				if (parseInt(versionArr[i]) < parseInt(endArr[i])) {
					bFoundInRange = true;
					break;
				}
				else if (parseInt(versionArr[i]) > parseInt(endArr[i]))
					return false;
			}
			if (versionArr.length <= endArr.length)
				bFoundInRange = true;
			if (!bFoundInRange && versionArr.length > endArr.length)
				return false;

			return bFoundInRange;
		}
		
		//parse response
		this.parseResponse = function (response, type) {
			try {
				var obj = JSON.parse(response);
				if (obj.V == "0.1") {
					if ((typeof (obj.Op) !== 'undefined') && (obj.Op == type)) {
						return obj;
					}
				}
			} catch (err) {
			}
			return null;
		};
		
		this.parseMfacResponse = function (response) {
			var out = {"start":"","end":""};
			var rs = this.parseResponse(response, "MFAC");
			if (rs) {
				if (typeof(rs.Out) !== 'undefined' && rs.Out != null) {
					if (typeof(rs.Out.VersionLowRange) === 'string') {
						out.start = rs.Out.VersionLowRange;
					}
					if (typeof(rs.Out.VersionHighRange) === 'string') {
						out.end = rs.Out.VersionHighRange;
					}
				}
			}
			return out;
		};
		
		this.parseCommitResponse = function (response) {
			var out = "failure";
						
			var rs = this.parseResponse(response, "NotifyResult");
			if (rs) {
				if (typeof(rs.Result) === 'string') {
					out = rs.Result.toLowerCase();
				}
			}
				
			return out;
		};
		
		this.parseOstpResponse = function (response) {
			var out = {
						"error":"failure",
						"noMatch":false,
						"resp":"",
						"registrationID":"",
						"isReged":false };
						
			var rs = this.parseResponse(response, "OSTP");
			if (rs) {
				
				if (typeof(rs.Result) === 'string') {
					if (rs.Result == "NO_MATCH") {
						out.error = "success";
						out.noMatch = true;
					} else {
						out.error = rs.Result.toLowerCase();
					}
				}
				
				if (typeof(rs.Out) !== 'undefined' && rs.Out != null) {
					if (typeof(rs.Out.response) === 'string') {
						out.resp = rs.Out.response;
					}
					if (typeof(rs.Out.registrationID) === 'string') {
						out.registrationID = rs.Out.registrationID;
					}
				}
				
				if ( typeof(rs.Out.registeredUsers) !== 'undefined' && rs.Out.registeredUsers != null) {
					if (typeof(rs.Out.registeredUsers.length) !== 'undefined' && 
							   rs.Out.registeredUsers.length != null && 
							   rs.Out.registeredUsers.length > 0) {
						out.isReged = true;
					}
				}
			}
			return out;
		};
		
		//build requests
		this.buildRequest = function (type) {
		
			var rq = {"V":"0.1", "Op":type};
			
			if (typeof(fido.getServerInfo) === 'function') {
				var si = fido.getServerInfo();
				if(typeof(si) !== 'undefined' && si) {
					try {
						rq.ServerInfo = JSON.stringify(si);
					} catch(err) {
						rq.ServerInfo = err;
					}
				}
			}
			return rq;
		};
		
		this.buildMfacRequest = function () {
		
			return this.buildRequest("MFAC");
		};
		
		this.buildCommitRequest = function (regId) {
			var rq = null;
			if (typeof(regId) === 'string') {
				var rq = this.buildRequest("NotifyResult");
				rq.In = {"regId":regId};
			}
			return rq;
		};
		
		this.buildOstpRequest = function (ostpRequest, requestParam, processParams) {
		
			var rq = this.buildRequest("OSTP");
			
			rq.In = {};
			if ( (typeof (ostpRequest) !== 'undefined') && (ostpRequest != null)) {
				rq.In.request = ostpRequest;
			}
			if ( (typeof (requestParams) !== 'undefined') && (requestParams != null)) {
				rq.In.requestParams = requestParams;
			}
			if ( (typeof (processParams) !== 'undefined') && (processParams != null)) {
				if ( (typeof (processParams.checkPolicyOnly) !== 'undefined') && (processParams.checkPolicyOnly != null)) {
					rq.In.checkPolicyOnly = processParams.checkPolicyOnly;
				}
				if ( (typeof (processParams.deferredCommit) !== 'undefined') && (processParams.deferredCommit != null)) {
					rq.In.deferredCommit = processParams.deferredCommit;
				}
			}
			
			return rq;
		};
		
		//calbacks
		this.onVerRange = function (response) {
			fido.injected.clientVersionInfo.OSTPRange = fido.injected.parseMfacResponse(response);
			
			//check any registration available
			fido.sendRequest( null, fido.injected.buildOstpRequest(""), fido.injected.onIsReged);
		};

		this.onIsReged = function (response) {
			fido.ready("success", { "reged": fido.injected.parseOstpResponse(response).isReged })
		};
		
		this.onProcess = function (response) {
			rs = fido.injected.parseOstpResponse(response);
			//console.log('Response: '+response);
			fido.respond(rs.error, { "nomatch": rs.noMatch, "cmsg": rs.resp, "registrationID": rs.registrationID });
		}

		this.onCommit = function (response) {
			if (typeof(fido.injected.onFinish) === 'function') {
				fido.injected.onFinish(fido.injected.parseCommitResponse(response));
			}
		}

		//init
		this.initialize = function() {
			//get version range supported by MFAC
			fido.sendRequest( null, fido.injected.buildMfacRequest(), fido.injected.onVerRange);
		};
	}
	
	/** The function checks that its version is in range supported by server 
	 *  and returns version range supported by MFAC, e.g. {"JSAPIVer":"1.0", "start":"0.11, "end":"0.14"}
	 *
	 */
    fido.getClientVersionInfo = function (serverVersionInfoObj) {
		var ver = null;
        if (fido.injected.isVersionInRange (serverVersionInfoObj.JSAPIRange)) {
            ver = fido.injected.clientVersionInfo;
		}
        return ver;
    }

    fido.io = {}

    fido.io.send = function(ostpRequest, requestParam, processParams) {

        try {
            // process OSTP request
			fido.sendRequest( 
				null, 
				fido.injected.buildOstpRequest(ostpRequest, requestParam, processParams), 
				fido.injected.onProcess ); 
                 
        } catch(e) {
            // handle error situation
            fido.respond("failure");
        }
    }

    fido.io.commit = function (registrationID, onFinish) {
        fido.injected.onFinish = onFinish;
		fido.sendRequest(
			null,
			fido.injected.buildCommitRequest(registrationID),
			fido.injected.onCommit);
    }
	
	fido.injected.initialize();
}
