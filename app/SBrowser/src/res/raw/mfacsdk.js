if ((typeof fido !== "undefined") && fido && fido.ready && (!window.hasOwnProperty('mfacsdkjs'))) {
    window.mfacsdkjs = true;
	
	/*
	 *	infoReq     is an object that provides additional parameters for the request. 
	 *              The content of this object needs to be defined, 
	 *              but for example version information can be passed here.
	 *	jsonRequest is an object that is passed to MFAC according to agent.schema.json
	 *	onReply     is a function that is called when the operation is done.
	 */	
	fido.sendRequest = function (infoReq, jsonRequest, onReply) {
		FIDOSDK.process(
			fido.MFACSDK.serializeObject(infoReq), 
			fido.MFACSDK.serializeObject(jsonRequest), 
			fido.MFACSDK.serializeFunction(onReply));
	}
	
	fido.MFACSDK = new MFACSDKImpl();
	function MFACSDKImpl () {
	
		this.serializeObject = function (obj) {
			return ( (typeof (obj) === "undefined") || (obj == null) )? null: JSON.stringify(obj);
		}
		
		this.serializeFunction = function (func) {
			this.OnReply = ( (typeof (func) === "function")? (func) : (function (){}) );
			return "fido.MFACSDK.OnReply";
		}
    };
}
