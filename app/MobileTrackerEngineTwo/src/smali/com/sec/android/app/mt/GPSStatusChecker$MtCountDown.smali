.class public Lcom/sec/android/app/mt/GPSStatusChecker$MtCountDown;
.super Landroid/os/CountDownTimer;
.source "GPSStatusChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mt/GPSStatusChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MtCountDown"
.end annotation


# instance fields
.field signalFlag:Z

.field final synthetic this$0:Lcom/sec/android/app/mt/GPSStatusChecker;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mt/GPSStatusChecker;JJ)V
    .locals 2
    .param p2, "millisInFuture"    # J
    .param p4, "countDownInterval"    # J

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/mt/GPSStatusChecker$MtCountDown;->this$0:Lcom/sec/android/app/mt/GPSStatusChecker;

    .line 30
    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mt/GPSStatusChecker$MtCountDown;->signalFlag:Z

    .line 31
    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/mt/GPSStatusChecker$MtCountDown;->this$0:Lcom/sec/android/app/mt/GPSStatusChecker;

    iget-object v0, v0, Lcom/sec/android/app/mt/GPSStatusChecker;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.settings.mt.MobileTracker"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 71
    return-void
.end method

.method public onTick(J)V
    .locals 12
    .param p1, "millisUntilFinished"    # J

    .prologue
    const-wide/16 v10, 0x0

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/mt/GPSStatusChecker$MtCountDown;->this$0:Lcom/sec/android/app/mt/GPSStatusChecker;

    # getter for: Lcom/sec/android/app/mt/GPSStatusChecker;->lm:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/sec/android/app/mt/GPSStatusChecker;->access$000(Lcom/sec/android/app/mt/GPSStatusChecker;)Landroid/location/LocationManager;

    move-result-object v0

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    move-result-object v7

    .line 40
    .local v7, "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-wide/16 v2, 0x0

    .line 41
    .local v2, "minTime":J
    const/4 v4, 0x0

    .line 42
    .local v4, "minDistance":F
    if-eqz v7, :cond_2

    .line 43
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 49
    .local v1, "locProvider":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/mt/GPSStatusChecker$MtCountDown;->this$0:Lcom/sec/android/app/mt/GPSStatusChecker;

    # getter for: Lcom/sec/android/app/mt/GPSStatusChecker;->lm:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/sec/android/app/mt/GPSStatusChecker;->access$000(Lcom/sec/android/app/mt/GPSStatusChecker;)Landroid/location/LocationManager;

    move-result-object v0

    iget-object v5, p0, Lcom/sec/android/app/mt/GPSStatusChecker$MtCountDown;->this$0:Lcom/sec/android/app/mt/GPSStatusChecker;

    iget-object v5, v5, Lcom/sec/android/app/mt/GPSStatusChecker;->gpsLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 53
    # getter for: Lcom/sec/android/app/mt/GPSStatusChecker;->lat:D
    invoke-static {}, Lcom/sec/android/app/mt/GPSStatusChecker;->access$100()D

    move-result-wide v8

    cmpl-double v0, v8, v10

    if-nez v0, :cond_1

    # getter for: Lcom/sec/android/app/mt/GPSStatusChecker;->lng:D
    invoke-static {}, Lcom/sec/android/app/mt/GPSStatusChecker;->access$200()D

    move-result-wide v8

    cmpl-double v0, v8, v10

    if-eqz v0, :cond_0

    .line 55
    :cond_1
    # getter for: Lcom/sec/android/app/mt/GPSStatusChecker;->lat:D
    invoke-static {}, Lcom/sec/android/app/mt/GPSStatusChecker;->access$100()D

    move-result-wide v8

    sput-wide v8, Lcom/sec/android/app/mt/Mobiletrackerreceiver;->latitude:D

    .line 56
    # getter for: Lcom/sec/android/app/mt/GPSStatusChecker;->lng:D
    invoke-static {}, Lcom/sec/android/app/mt/GPSStatusChecker;->access$200()D

    move-result-wide v8

    sput-wide v8, Lcom/sec/android/app/mt/Mobiletrackerreceiver;->longitude:D

    goto :goto_0

    .line 62
    .end local v1    # "locProvider":Ljava/lang/String;
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method
