.class public Lcom/sec/android/app/mt/GPSStatusChecker;
.super Landroid/content/BroadcastReceiver;
.source "GPSStatusChecker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mt/GPSStatusChecker$MtCountDown;
    }
.end annotation


# static fields
.field public static LANGITUDE:Ljava/lang/String;

.field public static LATITUDE:Ljava/lang/String;

.field private static lat:D

.field private static lng:D


# instance fields
.field gpsLocationListener:Landroid/location/LocationListener;

.field private lm:Landroid/location/LocationManager;

.field mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-string v0, "latitude"

    sput-object v0, Lcom/sec/android/app/mt/GPSStatusChecker;->LATITUDE:Ljava/lang/String;

    .line 24
    const-string v0, "langitude"

    sput-object v0, Lcom/sec/android/app/mt/GPSStatusChecker;->LANGITUDE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 101
    new-instance v0, Lcom/sec/android/app/mt/GPSStatusChecker$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mt/GPSStatusChecker$1;-><init>(Lcom/sec/android/app/mt/GPSStatusChecker;)V

    iput-object v0, p0, Lcom/sec/android/app/mt/GPSStatusChecker;->gpsLocationListener:Landroid/location/LocationListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mt/GPSStatusChecker;)Landroid/location/LocationManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mt/GPSStatusChecker;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/mt/GPSStatusChecker;->lm:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic access$100()D
    .locals 2

    .prologue
    .line 19
    sget-wide v0, Lcom/sec/android/app/mt/GPSStatusChecker;->lat:D

    return-wide v0
.end method

.method static synthetic access$102(D)D
    .locals 0
    .param p0, "x0"    # D

    .prologue
    .line 19
    sput-wide p0, Lcom/sec/android/app/mt/GPSStatusChecker;->lat:D

    return-wide p0
.end method

.method static synthetic access$200()D
    .locals 2

    .prologue
    .line 19
    sget-wide v0, Lcom/sec/android/app/mt/GPSStatusChecker;->lng:D

    return-wide v0
.end method

.method static synthetic access$202(D)D
    .locals 0
    .param p0, "x0"    # D

    .prologue
    .line 19
    sput-wide p0, Lcom/sec/android/app/mt/GPSStatusChecker;->lng:D

    return-wide p0
.end method

.method private setProviders(Ljava/lang/String;)V
    .locals 0
    .param p1, "providers"    # Ljava/lang/String;

    .prologue
    .line 98
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/mt/GPSStatusChecker;->mContext:Landroid/content/Context;

    .line 82
    const-string v1, "gps"

    invoke-direct {p0, v1}, Lcom/sec/android/app/mt/GPSStatusChecker;->setProviders(Ljava/lang/String;)V

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/mt/GPSStatusChecker;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/mt/GPSStatusChecker;->mContext:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    iput-object v1, p0, Lcom/sec/android/app/mt/GPSStatusChecker;->lm:Landroid/location/LocationManager;

    .line 85
    new-instance v0, Lcom/sec/android/app/mt/GPSStatusChecker$MtCountDown;

    const-wide/32 v2, 0x1d4c0

    const-wide/16 v4, 0x2710

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/mt/GPSStatusChecker$MtCountDown;-><init>(Lcom/sec/android/app/mt/GPSStatusChecker;JJ)V

    .line 86
    .local v0, "mtCountDown":Lcom/sec/android/app/mt/GPSStatusChecker$MtCountDown;
    invoke-virtual {v0}, Lcom/sec/android/app/mt/GPSStatusChecker$MtCountDown;->start()Landroid/os/CountDownTimer;

    .line 87
    return-void
.end method
