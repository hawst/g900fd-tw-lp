.class public Lcom/sec/android/app/mt/StatusChecker;
.super Landroid/content/BroadcastReceiver;
.source "StatusChecker.java"


# static fields
.field private static final BOOL_MT_BOOT_COMPLETED:Ljava/lang/String; = "MT_BOOT_COMPLETED"

.field private static final BOOL_MT_GET_SMSC:Ljava/lang/String; = "MT_GET_SMSC"

.field private static final BOOL_MT_GET_SMSC2:Ljava/lang/String; = "MT_GET_SMSC2"

.field private static final BOOL_MT_IN_SERVICE_STATE:Ljava/lang/String; = "MT_IN_SERVICE_STATE"

.field private static final BOOL_MT_IN_SERVICE_STATE2:Ljava/lang/String; = "MT_IN_SERVICE_STATE2"

.field private static final BOOL_MT_SMS_SEND:Ljava/lang/String; = "MT_SMS_SEND"

.field private static final DEFAULT_IMSI:Ljava/lang/String; = "0000"

.field private static final PREFS_NAME:Ljava/lang/String; = "MT_shared_fref"

.field private static final SIM_CHANGED:I = 0x1

.field private static final SIM_NOT_CHANGED:I = 0x2

.field private static final SIM_NOT_DETECTED:I = 0x3

.field private static final SIM_NOT_REGISTERED:I = 0x4

.field private static final STRING_MT_SMSC:Ljava/lang/String; = "MT_SMSC"

.field private static final STRING_MT_SMSC2:Ljava/lang/String; = "MT_SMSC2"

.field private static final TAG:Ljava/lang/String; = "StatusChecker"

.field private static final TELEPHONY_SERVICE2:Ljava/lang/String; = "phone2"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMtPrefs:Landroid/content/SharedPreferences;

.field private mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private CheckSIMChangeStatus(I)I
    .locals 13
    .param p1, "simSlot"    # I

    .prologue
    const/4 v8, 0x4

    const/4 v9, 0x3

    const/4 v10, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 226
    new-instance v3, Lcom/android/internal/util/NVStore;

    iget-object v11, p0, Lcom/sec/android/app/mt/StatusChecker;->mContext:Landroid/content/Context;

    invoke-direct {v3, v11}, Lcom/android/internal/util/NVStore;-><init>(Landroid/content/Context;)V

    .line 228
    .local v3, "mts":Lcom/android/internal/util/NVStore;
    if-nez p1, :cond_1

    move v0, v6

    .line 229
    .local v0, "anotherSim":I
    :goto_0
    const-string v11, "LGT"

    const-string v12, "ro.csc.sales_code"

    invoke-static {v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 230
    invoke-virtual {v3}, Lcom/android/internal/util/NVStore;->GetStoredICCID()Ljava/lang/String;

    move-result-object v1

    .line 231
    .local v1, "iccIdValue":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v10, "0000"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 232
    const-string v6, "StatusChecker"

    const-string v7, "SIM is not registered"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v8

    .line 320
    .end local v1    # "iccIdValue":Ljava/lang/String;
    :cond_0
    :goto_1
    return v6

    .end local v0    # "anotherSim":I
    :cond_1
    move v0, v7

    .line 228
    goto :goto_0

    .line 235
    .restart local v0    # "anotherSim":I
    .restart local v1    # "iccIdValue":Ljava/lang/String;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/app/mt/StatusChecker;->mContext:Landroid/content/Context;

    const-string v8, "phone"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 236
    .local v5, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v4

    .line 237
    .local v4, "sId_current":Ljava/lang/String;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    :cond_3
    move v6, v9

    .line 240
    goto :goto_1

    .line 244
    .end local v1    # "iccIdValue":Ljava/lang/String;
    .end local v4    # "sId_current":Ljava/lang/String;
    .end local v5    # "tm":Landroid/telephony/TelephonyManager;
    :cond_4
    new-array v2, v10, [Ljava/lang/String;

    .line 245
    .local v2, "imsi":[Ljava/lang/String;
    new-array v4, v10, [Ljava/lang/String;

    .line 246
    .local v4, "sId_current":[Ljava/lang/String;
    invoke-virtual {v3}, Lcom/android/internal/util/NVStore;->GetStoredIMSI()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v2, v7

    .line 247
    invoke-static {v7}, Lcom/samsung/android/telephony/MultiSimManager;->getSubscriberId(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v4, v7

    .line 249
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v11

    const-string v12, "SEC_FLOATING_FEATURE_COMMON_USE_GED_MULTISIM"

    invoke-virtual {v11, v12}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v11

    if-eq v11, v10, :cond_8

    .line 252
    :cond_5
    aget-object v10, v2, v7

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    const-string v11, "0000"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 253
    const-string v6, "StatusChecker"

    const-string v7, "SIM is not registered"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v8

    .line 254
    goto :goto_1

    .line 257
    :cond_6
    aget-object v8, v4, v7

    if-eqz v8, :cond_7

    aget-object v8, v4, v7

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    const-string v10, ""

    invoke-virtual {v8, v10}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_7

    aget-object v8, v4, v7

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    aget-object v7, v2, v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    :cond_7
    move v6, v9

    .line 262
    goto/16 :goto_1

    .line 267
    :cond_8
    invoke-virtual {v3}, Lcom/android/internal/util/NVStore;->GetStoredIMSI2()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v2, v6

    .line 268
    aget-object v11, v2, v7

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    const-string v12, "0000"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    aget-object v11, v2, v6

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    const-string v12, "0000"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 269
    const-string v6, "StatusChecker"

    const-string v7, "SIM1 && SIM2 are not registered"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v8

    .line 270
    goto/16 :goto_1

    .line 273
    :cond_9
    invoke-static {v6}, Lcom/samsung/android/telephony/MultiSimManager;->getSubscriberId(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v6

    .line 275
    aget-object v8, v4, v7

    if-eqz v8, :cond_a

    aget-object v8, v4, v7

    if-eqz v8, :cond_b

    aget-object v8, v4, v7

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    const-string v11, ""

    invoke-virtual {v8, v11}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_b

    .line 276
    :cond_a
    const-string v8, "0000"

    aput-object v8, v4, v7

    .line 278
    :cond_b
    aget-object v8, v4, v6

    if-eqz v8, :cond_c

    aget-object v8, v4, v6

    if-eqz v8, :cond_d

    aget-object v8, v4, v6

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    const-string v11, ""

    invoke-virtual {v8, v11}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_d

    .line 279
    :cond_c
    const-string v8, "0000"

    aput-object v8, v4, v6

    .line 281
    :cond_d
    aget-object v8, v4, v7

    const-string v11, "0000"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    aget-object v8, v4, v6

    const-string v11, "0000"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 282
    const-string v6, "StatusChecker"

    const-string v7, "sId_current[MultiSimManager.SIMSLOT1] and sId_current[MultiSimManager.SIMSLOT2] are null"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v9

    .line 283
    goto/16 :goto_1

    .line 285
    :cond_e
    aget-object v8, v2, v7

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    const-string v11, "0000"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_f

    aget-object v8, v2, v6

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    const-string v11, "0000"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_13

    .line 286
    :cond_f
    const-string v8, "StatusChecker"

    const-string v11, "Previous state : One SIM"

    invoke-static {v8, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    aget-object v8, v2, v7

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    aget-object v11, v4, v7

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    aget-object v8, v2, v6

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    aget-object v11, v4, v6

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_11

    :cond_10
    aget-object v8, v2, v7

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    aget-object v11, v4, v6

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_12

    aget-object v8, v2, v6

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    aget-object v7, v4, v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 290
    :cond_11
    const-string v6, "StatusChecker"

    const-string v7, "Current state : SIM card is not changed"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v10

    .line 291
    goto/16 :goto_1

    .line 293
    :cond_12
    aget-object v7, v2, p1

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    aget-object v8, v4, p1

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1a

    aget-object v7, v2, v0

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    aget-object v8, v4, v0

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1a

    .line 294
    const-string v6, "StatusChecker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Current state : SMSC["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] is not correspond with changed simSlot["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v9

    .line 295
    goto/16 :goto_1

    .line 299
    :cond_13
    const-string v8, "StatusChecker"

    const-string v11, "Previous state : Two SIM"

    invoke-static {v8, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    aget-object v8, v2, v7

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    aget-object v11, v4, v7

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_14

    aget-object v8, v2, v7

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    aget-object v11, v4, v6

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_16

    :cond_14
    aget-object v8, v2, v6

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    aget-object v11, v4, v7

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_15

    aget-object v8, v2, v6

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    aget-object v11, v4, v6

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 303
    :cond_15
    const-string v6, "StatusChecker"

    const-string v7, "Current state : SIM cards are not changed"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v10

    .line 304
    goto/16 :goto_1

    .line 306
    :cond_16
    aget-object v8, v2, v7

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    aget-object v11, v4, v7

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_17

    aget-object v8, v2, v7

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    aget-object v11, v4, v6

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_17

    aget-object v8, v2, v6

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    aget-object v11, v4, v7

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_17

    aget-object v8, v2, v6

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    aget-object v11, v4, v6

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_19

    :cond_17
    aget-object v7, v4, v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, "0000"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_18

    aget-object v7, v4, v6

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, "0000"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_19

    .line 310
    :cond_18
    const-string v6, "StatusChecker"

    const-string v7, "Current state : Only one SIM is remained"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v10

    .line 311
    goto/16 :goto_1

    .line 313
    :cond_19
    aget-object v7, v2, p1

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    aget-object v8, v4, p1

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1a

    aget-object v7, v2, v0

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    aget-object v8, v4, v0

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1a

    .line 314
    const-string v6, "StatusChecker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Current state : SMSC["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] is not correspond with changed simSlot["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v9

    .line 315
    goto/16 :goto_1

    .line 319
    :cond_1a
    const-string v7, "StatusChecker"

    const-string v8, "Current state : New SIM added"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private checkSIMChangeAlertOption()Z
    .locals 4

    .prologue
    .line 121
    const/4 v0, 0x0

    .line 126
    .local v0, "status":Z
    iget-object v1, p0, Lcom/sec/android/app/mt/StatusChecker;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "change_alert"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 127
    const/4 v0, 0x1

    .line 129
    :cond_0
    return v0
.end method

.method private trySendSMS()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 133
    invoke-direct {p0}, Lcom/sec/android/app/mt/StatusChecker;->checkSIMChangeAlertOption()Z

    move-result v4

    if-nez v4, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    const/4 v1, 0x0

    .line 138
    .local v1, "isCDMA":Z
    invoke-static {v9}, Lcom/samsung/android/telephony/MultiSimManager;->getCurrentPhoneType(I)I

    move-result v2

    .line 139
    .local v2, "phoneType":I
    if-ne v2, v8, :cond_2

    .line 140
    const/4 v1, 0x1

    .line 142
    :cond_2
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v4

    const-string v5, "SEC_FLOATING_FEATURE_COMMON_USE_GED_MULTISIM"

    invoke-virtual {v4, v5}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "CTC"

    const-string v5, "ro.csc.sales_code"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 144
    invoke-static {v9}, Lcom/samsung/android/telephony/MultiSimManager;->getSubscriberId(I)Ljava/lang/String;

    move-result-object v0

    .line 145
    .local v0, "imsiValue":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "111111111111111"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 146
    :cond_3
    const/4 v1, 0x0

    .line 151
    .end local v0    # "imsiValue":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mContext:Landroid/content/Context;

    const-string v5, "MT_shared_fref"

    invoke-virtual {v4, v5, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    .line 152
    const-string v4, "StatusChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "trySendSMS, in service : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v7, "MT_IN_SERVICE_STATE"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , SMS send : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v7, "MT_SMS_SEND"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , boot completed : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v7, "MT_BOOT_COMPLETED"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , SMSC : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v7, "MT_GET_SMSC"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , in service2 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v7, "MT_IN_SERVICE_STATE2"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , SMSC2 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v7, "MT_GET_SMSC2"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    const-string v4, "StatusChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isCDMA = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v5, "MT_IN_SERVICE_STATE"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v5, "MT_BOOT_COMPLETED"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v5, "MT_SMS_SEND"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_9

    if-nez v1, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v5, "MT_GET_SMSC"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 161
    :cond_5
    invoke-direct {p0, v9}, Lcom/sec/android/app/mt/StatusChecker;->CheckSIMChangeStatus(I)I

    move-result v4

    if-ne v4, v10, :cond_6

    .line 162
    const-string v4, "StatusChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "try send SMS for mobile tracker1 SMSC:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v7, "MT_SMSC"

    const-string v8, ""

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.settings.mt.MobileTracker"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 164
    .local v3, "sendMT":Landroid/content/Intent;
    const-string v4, "simSlot"

    invoke-virtual {v3, v4, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 165
    const-string v4, "smsc"

    iget-object v5, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v6, "MT_SMSC"

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 166
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 167
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 168
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_IN_SERVICE_STATE"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 169
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_SMS_SEND"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 170
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_BOOT_COMPLETED"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 171
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_GET_SMSC"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 172
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_IN_SERVICE_STATE2"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 173
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_GET_SMSC2"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 174
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 176
    .end local v3    # "sendMT":Landroid/content/Intent;
    :cond_6
    invoke-direct {p0, v9}, Lcom/sec/android/app/mt/StatusChecker;->CheckSIMChangeStatus(I)I

    move-result v4

    if-eq v4, v8, :cond_7

    invoke-direct {p0, v9}, Lcom/sec/android/app/mt/StatusChecker;->CheckSIMChangeStatus(I)I

    move-result v4

    if-ne v4, v12, :cond_8

    .line 177
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 178
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_IN_SERVICE_STATE"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 179
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_SMS_SEND"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 180
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_BOOT_COMPLETED"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 181
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_GET_SMSC"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 182
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_IN_SERVICE_STATE2"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 183
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_GET_SMSC2"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 184
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 186
    :cond_8
    invoke-direct {p0, v9}, Lcom/sec/android/app/mt/StatusChecker;->CheckSIMChangeStatus(I)I

    move-result v4

    if-ne v4, v11, :cond_0

    goto/16 :goto_0

    .line 190
    :cond_9
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v4

    const-string v5, "SEC_FLOATING_FEATURE_COMMON_USE_GED_MULTISIM"

    invoke-virtual {v4, v5}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v5, "MT_IN_SERVICE_STATE2"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v5, "MT_BOOT_COMPLETED"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v5, "MT_SMS_SEND"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    if-nez v1, :cond_a

    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v5, "MT_GET_SMSC2"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 193
    :cond_a
    invoke-direct {p0, v10}, Lcom/sec/android/app/mt/StatusChecker;->CheckSIMChangeStatus(I)I

    move-result v4

    if-ne v4, v10, :cond_b

    .line 194
    const-string v4, "StatusChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "try send SMS for mobile tracker2 SMSC:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v7, "MT_SMSC2"

    const-string v8, ""

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.settings.mt.MobileTracker"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 196
    .restart local v3    # "sendMT":Landroid/content/Intent;
    const-string v4, "simSlot"

    invoke-virtual {v3, v4, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 197
    const-string v4, "smsc"

    iget-object v5, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    const-string v6, "MT_SMSC2"

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 199
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 200
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_IN_SERVICE_STATE"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 201
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_IN_SERVICE_STATE2"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 202
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_SMS_SEND"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 203
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_BOOT_COMPLETED"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 204
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_GET_SMSC"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 205
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_GET_SMSC2"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 206
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 208
    .end local v3    # "sendMT":Landroid/content/Intent;
    :cond_b
    invoke-direct {p0, v10}, Lcom/sec/android/app/mt/StatusChecker;->CheckSIMChangeStatus(I)I

    move-result v4

    if-eq v4, v8, :cond_c

    invoke-direct {p0, v10}, Lcom/sec/android/app/mt/StatusChecker;->CheckSIMChangeStatus(I)I

    move-result v4

    if-ne v4, v12, :cond_d

    .line 209
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 210
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_IN_SERVICE_STATE"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 211
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_IN_SERVICE_STATE2"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 212
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_SMS_SEND"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 213
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_BOOT_COMPLETED"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 214
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_GET_SMSC"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 215
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "MT_GET_SMSC2"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 216
    iget-object v4, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 218
    :cond_d
    invoke-direct {p0, v10}, Lcom/sec/android/app/mt/StatusChecker;->CheckSIMChangeStatus(I)I

    move-result v4

    if-ne v4, v11, :cond_0

    goto/16 :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 54
    const-string v7, "StatusChecker"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onReceive : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/mt/StatusChecker;->mContext:Landroid/content/Context;

    .line 59
    iget-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mContext:Landroid/content/Context;

    const-string v8, "MT_shared_fref"

    invoke-virtual {v7, v8, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    .line 60
    iget-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 62
    const-string v7, "DONE"

    const-string v8, "net.mt.init"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 63
    const-string v7, "StatusChecker"

    const-string v8, "onReceive : preference initializing"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iget-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v8, "MT_SMS_SEND"

    invoke-interface {v7, v8, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 65
    iget-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v8, "MT_BOOT_COMPLETED"

    invoke-interface {v7, v8, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 66
    iget-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v8, "MT_GET_SMSC"

    invoke-interface {v7, v8, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 67
    iget-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v8, "MT_GET_SMSC2"

    invoke-interface {v7, v8, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 68
    iget-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v8, "MT_IN_SERVICE_STATE"

    invoke-interface {v7, v8, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 69
    iget-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v8, "MT_IN_SERVICE_STATE2"

    invoke-interface {v7, v8, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 70
    const-string v7, "net.mt.init"

    const-string v8, "DONE"

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 75
    iget-object v6, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v7, "MT_SMS_SEND"

    invoke-interface {v6, v7, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 76
    iget-object v5, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/mt/StatusChecker;->trySendSMS()V

    goto :goto_0

    .line 79
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 80
    iget-object v6, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v7, "MT_BOOT_COMPLETED"

    invoke-interface {v6, v7, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 81
    iget-object v5, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/mt/StatusChecker;->trySendSMS()V

    goto/16 :goto_0

    .line 85
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.provider.Telephony.GET_SMSC"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 86
    const-string v7, "phone"

    invoke-virtual {p2, v7, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 87
    .local v1, "simSlot":I
    const-string v7, "smsc"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "mSmsCenter":[Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 89
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v7

    const-string v8, "SEC_FLOATING_FEATURE_COMMON_USE_GED_MULTISIM"

    invoke-virtual {v7, v8}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    if-ne v1, v5, :cond_5

    .line 90
    iget-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v8, "MT_GET_SMSC2"

    invoke-interface {v7, v8, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 91
    iget-object v5, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v7, "MT_SMSC2"

    aget-object v6, v0, v6

    invoke-interface {v5, v7, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 97
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 98
    invoke-direct {p0}, Lcom/sec/android/app/mt/StatusChecker;->trySendSMS()V

    goto/16 :goto_0

    .line 94
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v8, "MT_GET_SMSC"

    invoke-interface {v7, v8, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 95
    iget-object v5, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v7, "MT_SMSC"

    aget-object v6, v0, v6

    invoke-interface {v5, v7, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 102
    .end local v0    # "mSmsCenter":[Ljava/lang/String;
    .end local v1    # "simSlot":I
    :cond_6
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.intent.action.SERVICE_STATE"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 103
    const-string v7, "subscription"

    const-wide/16 v8, 0x0

    invoke-virtual {p2, v7, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 104
    .local v2, "mSub":J
    invoke-static {v2, v3}, Lcom/samsung/android/telephony/MultiSimManager;->getSlotId(J)I

    move-result v1

    .line 105
    .restart local v1    # "simSlot":I
    invoke-static {v2, v3}, Lcom/samsung/android/telephony/MultiSimManager;->getServiceStateUsingSubId(J)I

    move-result v4

    .line 106
    .local v4, "ss":I
    const-string v7, "StatusChecker"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Service state changed : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mSub"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-static {v2, v3}, Lcom/samsung/android/telephony/MultiSimManager;->getServiceStateUsingSubId(J)I

    move-result v7

    if-eqz v7, :cond_7

    .line 111
    :cond_7
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v7

    const-string v8, "SEC_FLOATING_FEATURE_COMMON_USE_GED_MULTISIM"

    invoke-virtual {v7, v8}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    if-ne v1, v5, :cond_9

    .line 112
    iget-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v8, "MT_IN_SERVICE_STATE2"

    if-nez v4, :cond_8

    :goto_2
    invoke-interface {v7, v8, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 115
    :goto_3
    iget-object v5, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 116
    invoke-direct {p0}, Lcom/sec/android/app/mt/StatusChecker;->trySendSMS()V

    goto/16 :goto_0

    :cond_8
    move v5, v6

    .line 112
    goto :goto_2

    .line 114
    :cond_9
    iget-object v7, p0, Lcom/sec/android/app/mt/StatusChecker;->mMtPrefsEditor:Landroid/content/SharedPreferences$Editor;

    const-string v8, "MT_IN_SERVICE_STATE"

    if-nez v4, :cond_a

    :goto_4
    invoke-interface {v7, v8, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_3

    :cond_a
    move v5, v6

    goto :goto_4
.end method
