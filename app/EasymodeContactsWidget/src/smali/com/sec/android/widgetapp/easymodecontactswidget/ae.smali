.class Lcom/sec/android/widgetapp/easymodecontactswidget/ae;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lcom/sec/android/widgetapp/easymodecontactswidget/ab;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ae;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    const/4 v0, 0x2

    new-array v1, v0, [I

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ae;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ab;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->d(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    sget-object v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ah;->a:[I

    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ae;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ab;

    invoke-static {v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->e(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)Lcom/sec/android/widgetapp/easymodecontactswidget/ai;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/ai;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ae;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ab;

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->setBubbleAndIndicatorLeftPosition(I)V

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ae;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ab;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ae;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ab;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->d(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ae;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ab;

    invoke-static {v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->f(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)I

    move-result v2

    if-le v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ae;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ab;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->d(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ae;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ab;

    invoke-static {v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->f(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    :goto_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ae;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ab;

    const/4 v3, 0x1

    aget v3, v1, v3

    add-int/2addr v0, v3

    iget-object v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ae;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ab;

    invoke-static {v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {v2, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->setTopPostion(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ae;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ab;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->f(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)I

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
