.class Lcom/sec/android/widgetapp/easymodecontactswidget/j;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Landroid/widget/FrameLayout;

.field final synthetic c:Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;ZLandroid/widget/FrameLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/j;->c:Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;

    iput-boolean p2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/j;->a:Z

    iput-object p3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/j;->b:Landroid/widget/FrameLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "SeniorContactSelector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAnimationEnd cancel:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/j;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/j;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/j;->c:Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->finish()V

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/j;->c:Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;

    invoke-virtual {v0, v3, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->overridePendingTransition(II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/j;->b:Landroid/widget/FrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/j;->c:Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->finish()V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method
