.class public Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;
.super Landroid/app/Activity;


# static fields
.field public static final c:[Ljava/lang/String;


# instance fields
.field public a:Ljava/lang/String;

.field b:Landroid/net/Uri;

.field final d:[Ljava/lang/String;

.field private e:Z

.field private f:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

.field private g:I

.field private h:I

.field private i:I

.field private final j:Ljava/lang/String;

.field private k:Landroid/os/Handler;

.field private l:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "is_super_primary"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->e:Z

    const-string v0, "com.android.contacts.activities.ContactEditorActivity"

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->a:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->i:I

    const-string v0, "addMode"

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->j:Ljava/lang/String;

    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->b:Landroid/net/Uri;

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "data3"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "display_name_alt"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "is_super_primary"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "photo_file_id"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->d:[Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->g:I

    return v0
.end method

.method private a()V
    .locals 3

    const-string v0, "SeniorAddDialogActivity"

    const-string v1, "return to EasyLauncher on Help mode."

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.sec.android.app.easylauncher"

    const-string v2, "com.sec.android.app.easylauncher.Launcher"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const/high16 v0, 0x20000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private a(I)V
    .locals 5

    const-string v0, "SeniorAddDialogActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeDataAt:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;-><init>(Landroid/content/Context;)V

    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->g:I

    invoke-static {v0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/m;->a(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->c(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    const-wide/16 v0, -0x1

    if-eqz v3, :cond_1

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_0

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    const-string v0, "contact_id"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :cond_0
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "widget_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->g:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "\' AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "position"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v4, 0x27

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->a(Ljava/lang/String;)I

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->b()V

    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v0, v1, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->a(Landroid/content/Context;JZ)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->a(I)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    const-string v0, "SeniorAddDialogActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "send Intent to EasyLauncher with [result_success:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] for Help mode."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.action.easymodecontactswidget.HELP_RESULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "appWidgetId"

    iget v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->g:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "result_success"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected a(Landroid/net/Uri;IIJ)V
    .locals 10

    const/4 v7, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->f:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    new-instance v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;

    invoke-direct {v6}, Lcom/sec/android/widgetapp/easymodecontactswidget/i;-><init>()V

    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    iput-wide v0, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->a:J

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v4, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->a:J

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "data"

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->d:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_3
    const-string v0, "is_super_primary"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v7, :cond_7

    const-string v0, "data2"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string v2, "data3"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v0, v2}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_1
    if-nez v0, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    const-string v0, "data2"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string v2, "data3"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v0, v2}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_2
    iput-object v0, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->f:Ljava/lang/String;

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "display_name"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->c:Ljava/lang/String;

    const-string v0, "display_name_alt"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->d:Ljava/lang/String;

    const-string v0, "photo_file_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v8, 0x0

    cmp-long v0, v4, v8

    if-lez v0, :cond_5

    sget-object v0, Landroid/provider/ContactsContract$DisplayPhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "r"

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_3
    if-eqz v3, :cond_8

    invoke-static {p0, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->b(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_4
    if-eqz v0, :cond_6

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)[B

    move-result-object v3

    iput-object v3, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->e:[B

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    iget-wide v4, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->a:J

    invoke-static {v3, v4, v5, v7}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->a(Landroid/content/Context;JZ)V

    iget-wide v4, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->a:J

    invoke-static {v0, v4, v5}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->a(Landroid/graphics/Bitmap;J)V

    :cond_6
    const-string v0, "raw_contact_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->b:J

    const-string v0, "contact_id"

    iget-wide v4, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "raw_contact_id"

    iget-wide v4, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "display_name_primary"

    iget-object v3, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->c:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "display_name"

    iget-object v3, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->d:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "type"

    iget-object v3, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->f:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "data_id"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "position"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "widget_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "photo"

    iget-object v3, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->e:[B

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->f:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    invoke-virtual {v0, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->a(Landroid/content/ContentValues;)J

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->f:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->f:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->b()V

    goto/16 :goto_0

    :cond_7
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    move-object v0, v3

    goto/16 :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v4, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->a:J

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->b(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_4

    :cond_9
    iget-wide v4, v6, Lcom/sec/android/widgetapp/easymodecontactswidget/i;->a:J

    invoke-static {p0, v4, v5}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->a(Landroid/content/Context;J)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_4

    :cond_a
    move-object v0, v3

    goto/16 :goto_2
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-string v0, "SeniorAddDialogActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resultCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v7, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->e:Z

    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->g:I

    invoke-static {p0, v0, v7}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->c(Landroid/content/Context;IZ)Z

    move-result v6

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    if-eqz v6, :cond_0

    invoke-direct {p0, v7}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->a(Z)V

    invoke-direct {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->a()V

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_2

    iget v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->g:I

    iget v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->h:I

    const-string v0, "phone_data_id"

    const-wide/16 v4, -0x1

    invoke-virtual {p3, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->a(Landroid/net/Uri;IIJ)V

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->g:I

    const v2, 0x7f080003

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    if-eqz v6, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {p0, v1, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-direct {p0, v8}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->a(Z)V

    invoke-direct {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->a()V

    iget v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->g:I

    invoke-static {p0, v1, v7}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->d(Landroid/content/Context;IZ)V

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->k:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->k:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/widgetapp/easymodecontactswidget/h;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/h;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;Landroid/appwidget/AppWidgetManager;)V

    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->finish()V

    goto :goto_0

    :cond_3
    const-string v0, "accessibility"

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0a0001

    invoke-static {p0, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->g:I

    if-eqz p1, :cond_0

    const-string v0, "isIntentFired"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->e:Z

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "position"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->h:I

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "addMode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->i:I

    iget-boolean v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->e:Z

    if-nez v0, :cond_1

    iput-boolean v5, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->e:Z

    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->i:I

    packed-switch v0, :pswitch_data_0

    iput v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->i:I

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->finish()V

    :cond_1
    :goto_0
    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/g;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/g;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->l:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->l:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :pswitch_0
    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->g:I

    invoke-static {p0, v0, v4}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->c(Landroid/content/Context;IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK_FOR_HELP"

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    :goto_1
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "actionCode"

    const/16 v2, 0x46

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "flag_help"

    iget v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->g:I

    invoke-static {p0, v2, v4}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->c(Landroid/content/Context;IZ)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    iput v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->i:I

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_1

    :pswitch_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.INSERT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :cond_5
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Contact_EnableCallerIdSearch4Korea"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "com.android.contacts"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :cond_6
    const-string v1, "vnd.android.cursor.dir/contact"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "finishActivityOnSaveCompleted"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "from_easy_widget"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iput v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->i:I

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "SeniorAddDialogActivity"

    const-string v1, "Destroy Activity"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->l:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->l:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->l:Landroid/content/BroadcastReceiver;

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "isIntentFired"

    iget-boolean v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method
