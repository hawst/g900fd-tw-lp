.class public Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;
.super Landroid/app/Activity;


# instance fields
.field a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;

.field private b:I

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 3

    const v0, 0x7f080001

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    const v1, 0x7f040002

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    new-instance v2, Lcom/sec/android/widgetapp/easymodecontactswidget/j;

    invoke-direct {v2, p0, p1, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/j;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;ZLandroid/widget/FrameLayout;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public finish()V
    .locals 2

    const-string v0, "SeniorContactSelector"

    const-string v1, "selector finish"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "SeniorContactSelector"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f040001

    const v1, 0x7f040002

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->overridePendingTransition(II)V

    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->setContentView(I)V

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "appWidgetId"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->b:I

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "position"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->c:I

    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->b:I

    iget v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->c:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a(Landroid/app/FragmentManager;II)V

    return-void
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "SeniorContactSelector"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "SeniorContactSelector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume mNeedFinish:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->d:Z

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->finish()V

    :cond_0
    return-void
.end method
