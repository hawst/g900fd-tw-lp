.class public Lcom/sec/android/widgetapp/easymodecontactswidget/w;
.super Landroid/app/DialogFragment;


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/widget/ArrayAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/widgetapp/easymodecontactswidget/w;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a:I

    return v0
.end method

.method private a(I)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorAddDialogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "appWidgetId"

    iget v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "position"

    iget v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "addMode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->startActivityForResult(Landroid/content/Intent;I)V

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f040001

    const v2, 0x7f040002

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void
.end method

.method private a(Landroid/content/DialogInterface;Z)V
    .locals 2

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f080001

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;

    invoke-virtual {v0, p2}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;->a(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/widgetapp/easymodecontactswidget/w;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/widgetapp/easymodecontactswidget/w;Landroid/content/DialogInterface;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a(Landroid/content/DialogInterface;Z)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/widgetapp/easymodecontactswidget/w;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    const-string v0, "SeniorShowOrCreateDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "send Intent to EasyLauncher with [result_success:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] for Help mode."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.action.easymodecontactswidget.HELP_RESULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "appWidgetId"

    iget v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "result_success"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/widgetapp/easymodecontactswidget/w;)Landroid/widget/ArrayAdapter;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->c:Landroid/widget/ArrayAdapter;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/app/FragmentManager;II)V
    .locals 3

    const-string v0, "SeniorShowOrCreateDialogFragment"

    const-string v1, "show"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "position"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :try_start_0
    new-instance v1, Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    invoke-direct {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;-><init>()V

    if-eqz v1, :cond_1

    invoke-virtual {v1, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->setArguments(Landroid/os/Bundle;)V

    const-string v0, "SeniorShowOrCreateDialogFragment"

    invoke-virtual {p1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    const-string v0, "SeniorShowOrCreateDialogFragment"

    const-string v2, "SeniorShowOrCreateDialogFragment is already added"

    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "SeniorShowOrCreateDialogFragment"

    invoke-virtual {v1, p1, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a(Landroid/content/DialogInterface;Z)V

    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "appWidgetId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a:I

    const-string v2, "position"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->b:I

    new-instance v1, Lcom/sec/android/widgetapp/easymodecontactswidget/x;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f030006

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/x;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/w;Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->c:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->c:Landroid/widget/ArrayAdapter;

    const v2, 0x7f0a0007

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->c:Landroid/widget/ArrayAdapter;

    const v2, 0x7f0a0015

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v1, Lcom/sec/android/widgetapp/easymodecontactswidget/y;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/y;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/w;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a0002

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->c:Landroid/widget/ArrayAdapter;

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/widgetapp/easymodecontactswidget/z;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/z;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/w;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a:I

    invoke-static {v2, v3, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->c(Landroid/content/Context;IZ)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    return-object v1
.end method

.method public onResume()V
    .locals 10

    const v9, 0x7f070024

    const v5, 0x7f070023

    const/4 v8, 0x1

    const/4 v7, 0x0

    const v6, 0x7f070027

    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a:I

    invoke-static {v0, v1, v7}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->c(Landroid/content/Context;IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->setText(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v5

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->setBubbleAndIndicatorLeftPosition(I)V

    invoke-virtual {v1, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->setTopPostion(I)V

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, v5}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->setTouchableArea(Landroid/graphics/Rect;)V

    invoke-virtual {v1, v8, v8, v7}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->a(ZZZ)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "appWidgetId"

    iget v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "position"

    iget v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
