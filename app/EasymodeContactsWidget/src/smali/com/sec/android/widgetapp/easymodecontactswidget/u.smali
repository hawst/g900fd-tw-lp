.class Lcom/sec/android/widgetapp/easymodecontactswidget/u;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Lcom/sec/android/widgetapp/easymodecontactswidget/ak;)Ljava/lang/Boolean;
    .locals 15

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iget-object v6, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;->a:Landroid/content/Context;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iget-object v0, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;->b:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    const-string v7, "display_name_alt COLLATE LOCALIZED ASC"

    const/4 v8, 0x0

    :try_start_0
    new-instance v9, Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    invoke-direct {v9, v6}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->a()Landroid/database/Cursor;

    move-result-object v10

    if-eqz v10, :cond_11

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v0

    const-string v1, "raw_contact_id"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->b:J

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v0

    const-string v1, "contact_id"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->a:J

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v0

    const-string v1, "widget_id"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->h:J

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v0

    const-string v1, "position"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->g:J

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-wide v2, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->b:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = \'"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-wide v4, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->b:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "\' AND "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "deleted"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = \'0\'"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_6

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v2, "widget_id"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " = \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v2

    iget-wide v2, v2, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->h:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, "\' AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "position"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " = \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v2

    iget-wide v2, v2, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->g:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->a(Ljava/lang/String;)I

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v10, :cond_3

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_3
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->b()V

    :cond_4
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iget-object v0, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iget-object v0, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_5
    :goto_1
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_2
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_2

    :cond_6
    if-eqz v1, :cond_7

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_7
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v2

    iget-wide v2, v2, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->a:J

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "data"

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    iget-object v2, v2, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v5, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    const/4 v0, 0x0

    if-eqz v2, :cond_d

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_8
    const-string v1, "is_super_primary"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_c

    const-string v0, "data2"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string v1, "data3"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v0, v1}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_3
    if-nez v0, :cond_9

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    const-string v0, "data2"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string v1, "data3"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v0, v1}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_9
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v0

    const-string v1, "display_name"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v0

    const-string v1, "display_name_alt"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->d:Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "photo_file_id"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v12, 0x0

    cmp-long v1, v4, v12

    if-lez v1, :cond_a

    sget-object v1, Landroid/provider/ContactsContract$DisplayPhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    :try_start_1
    iget-object v4, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v4}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "r"

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    :try_start_2
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_a
    :goto_4
    if-eqz v0, :cond_e

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->b(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_5
    if-eqz v0, :cond_b

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)[B

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v4}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v4

    iput-object v1, v4, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->e:[B

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-wide v4, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->a:J

    const/4 v1, 0x1

    invoke-static {v6, v4, v5, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->a(Landroid/content/Context;JZ)V

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-wide v4, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->a:J

    invoke-static {v0, v4, v5}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->a(Landroid/graphics/Bitmap;J)V

    :cond_b
    const-string v0, "contact_id"

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-wide v4, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "raw_contact_id"

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-wide v4, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "display_name_primary"

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->c:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "display_name"

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->d:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "type"

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->f:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "position"

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-wide v4, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->g:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->e:[B

    if-eqz v0, :cond_10

    const-string v0, "photo"

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->e:[B

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raw_contact_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-wide v4, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->b:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "position"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-wide v4, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->g:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "widget_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-wide v4, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->h:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v3, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->a(Landroid/content/ContentValues;Ljava/lang/String;)I

    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_c
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_3

    :cond_d
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catch_1
    move-exception v1

    move-object v14, v1

    move-object v1, v0

    move-object v0, v14

    :goto_7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    goto/16 :goto_4

    :cond_e
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v4}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v4

    iget-wide v4, v4, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->a:J

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->b(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_5

    :cond_f
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    move-result-object v1

    iget-wide v4, v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;->a:J

    invoke-static {v0, v4, v5}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->a(Landroid/content/Context;J)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_5

    :cond_10
    const-string v0, "photo"

    invoke-virtual {v3, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_11
    if-eqz v10, :cond_12

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_12
    if-eqz v9, :cond_5

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->b()V

    goto/16 :goto_1

    :catch_2
    move-exception v1

    move-object v14, v1

    move-object v1, v0

    move-object v0, v14

    goto :goto_7
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d(Z)Z

    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Lcom/sec/android/widgetapp/easymodecontactswidget/ak;

    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a([Lcom/sec/android/widgetapp/easymodecontactswidget/ak;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    new-instance v1, Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    invoke-direct {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/r;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/n;Lcom/sec/android/widgetapp/easymodecontactswidget/r;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d(Z)Z

    return-void
.end method
