.class Lcom/sec/android/widgetapp/easymodecontactswidget/x;
.super Landroid/widget/ArrayAdapter;


# instance fields
.field final synthetic a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/easymodecontactswidget/w;Landroid/content/Context;I)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/x;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    invoke-direct {p0, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-eqz p2, :cond_0

    instance-of v0, p2, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    check-cast p2, Landroid/widget/TextView;

    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/x;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    return-object p2

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/x;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f030006

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object p2, v0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/x;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/x;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    invoke-static {v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/w;)I

    move-result v2

    invoke-static {v1, v2, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->c(Landroid/content/Context;IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method
