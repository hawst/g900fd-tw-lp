.class Lcom/sec/android/widgetapp/easymodecontactswidget/s;
.super Landroid/database/ContentObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/easymodecontactswidget/n;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/s;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4

    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    const-string v0, "SeniorFavoriteWidgetProviderBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onChange() selfChange = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;-><init>()V

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/s;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Landroid/content/Context;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/s;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-static {v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->b(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;->b:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    new-instance v1, Lcom/sec/android/widgetapp/easymodecontactswidget/t;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/t;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/s;)V

    iput-object v1, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;->c:Ljava/lang/Runnable;

    new-instance v1, Lcom/sec/android/widgetapp/easymodecontactswidget/u;

    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/s;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/n;

    invoke-direct {v1, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/u;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/android/widgetapp/easymodecontactswidget/ak;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
