.class public Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;
.super Landroid/app/Activity;


# instance fields
.field private a:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    const v0, 0x7f080001

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    const v1, 0x7f040002

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    new-instance v2, Lcom/sec/android/widgetapp/easymodecontactswidget/f;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/f;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->a()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v3, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040001

    const v1, 0x7f040002

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->overridePendingTransition(II)V

    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->finish()V

    :cond_0
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "position"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-eq v1, v3, :cond_1

    if-ne v2, v3, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->finish()V

    :cond_2
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0a000b

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0a000a

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x104000a

    new-instance v5, Lcom/sec/android/widgetapp/easymodecontactswidget/e;

    invoke-direct {v5, p0, v1, v2, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/e;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;IILandroid/content/Intent;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lcom/sec/android/widgetapp/easymodecontactswidget/d;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/d;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/widgetapp/easymodecontactswidget/c;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/c;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->a:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method
