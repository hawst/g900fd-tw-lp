.class public Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorWidgetRemoteViewLargeService;
.super Landroid/widget/RemoteViewsService;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/widget/RemoteViewsService;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetViewFactory(Landroid/content/Intent;)Landroid/widget/RemoteViewsService$RemoteViewsFactory;
    .locals 3

    const-string v0, "appWidgetId"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-gez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorWidgetRemoteViewLargeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;-><init>(Landroid/content/Context;I)V

    goto :goto_0
.end method
