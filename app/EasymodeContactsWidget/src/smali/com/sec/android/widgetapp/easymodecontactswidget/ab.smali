.class public Lcom/sec/android/widgetapp/easymodecontactswidget/ab;
.super Landroid/widget/RelativeLayout;


# instance fields
.field private a:Lcom/sec/android/widgetapp/easymodecontactswidget/ai;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/LinearLayout;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/LinearLayout;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/view/View;

.field private l:Ljava/util/List;

.field private m:I

.field private n:Landroid/view/animation/Animation;

.field private o:I

.field private p:Lcom/sec/android/widgetapp/easymodecontactswidget/aj;

.field private q:Z

.field private r:Landroid/content/Context;

.field private s:Landroid/view/animation/Animation$AnimationListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    sget-object v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ai;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ai;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;-><init>(Landroid/content/Context;Lcom/sec/android/widgetapp/easymodecontactswidget/ai;Landroid/view/View;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/widgetapp/easymodecontactswidget/ai;Landroid/view/View;)V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->m:I

    iput v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->o:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->q:Z

    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/af;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/af;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->s:Landroid/view/animation/Animation$AnimationListener;

    iput-object p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->r:Landroid/content/Context;

    iput-object p4, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ai;

    iput-object p5, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->c:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->r:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    new-instance v1, Lcom/sec/android/widgetapp/easymodecontactswidget/ac;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ac;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)V

    sget-object v2, Lcom/sec/android/widgetapp/easymodecontactswidget/ah;->a:[I

    iget-object v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ai;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/ai;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b:Landroid/view/View;

    if-nez v0, :cond_1

    const-string v0, "TutorialPopupViews"

    const-string v1, "mMainView is null"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :pswitch_0
    const v2, 0x7f030007

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b:Landroid/view/View;

    const v1, 0x7f080008

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->d:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b:Landroid/view/View;

    const v1, 0x7f080009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->e:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b:Landroid/view/View;

    const v1, 0x7f08000f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->f:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b:Landroid/view/View;

    const v1, 0x7f08000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->g:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b:Landroid/view/View;

    const v1, 0x7f080010

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->k:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b:Landroid/view/View;

    const v1, 0x7f08000b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b:Landroid/view/View;

    const v1, 0x7f08000e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b:Landroid/view/View;

    const v1, 0x7f08000c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->i:Landroid/widget/LinearLayout;

    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->o:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->r:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_2
    iput v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->o:I

    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ad;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ad;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)V

    sget-object v1, Lcom/sec/android/widgetapp/easymodecontactswidget/ah;->a:[I

    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ai;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/ai;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    :cond_2
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->a()V

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->c:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/sec/android/widgetapp/easymodecontactswidget/ae;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ae;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :goto_4
    invoke-direct {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->c()V

    goto/16 :goto_1

    :cond_3
    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->o:I

    goto :goto_2

    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->k:Landroid/view/View;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->k:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_3

    :cond_4
    invoke-direct {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->getDefaultDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->setBubbleAndIndicatorLeftPosition(I)V

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/widgetapp/easymodecontactswidget/ai;Landroid/view/View;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/widgetapp/easymodecontactswidget/ai;Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->d:Landroid/view/View;

    return-object v0
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->r:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->r:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/view/MotionEvent;->setLocation(FF)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->a(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)Lcom/sec/android/widgetapp/easymodecontactswidget/aj;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->p:Lcom/sec/android/widgetapp/easymodecontactswidget/aj;

    return-object v0
.end method

.method private c()V
    .locals 4

    const-wide/16 v2, 0x2bc

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->l:Ljava/util/List;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->r:Landroid/content/Context;

    const v1, 0x7f040004

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->s:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->l:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->r:Landroid/content/Context;

    const v1, 0x7f040005

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->s:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->l:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->r:Landroid/content/Context;

    const v1, 0x7f040006

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->s:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->l:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->r:Landroid/content/Context;

    const v1, 0x7f040007

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->s:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->l:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->r:Landroid/content/Context;

    const v1, 0x7f040008

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->s:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->l:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->r:Landroid/content/Context;

    const/high16 v1, 0x7f040000

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->n:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->n:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->l:Ljava/util/List;

    iget v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->m:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    return-void
.end method

.method static synthetic c(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->q:Z

    return v0
.end method

.method static synthetic d(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)Lcom/sec/android/widgetapp/easymodecontactswidget/ai;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/ai;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->o:I

    return v0
.end method

.method static synthetic g(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)I
    .locals 2

    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->m:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->m:I

    return v0
.end method

.method private getDefaultDisplayMetrics()Landroid/util/DisplayMetrics;
    .locals 3

    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->r:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    return-object v1
.end method

.method static synthetic h(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->m:I

    return v0
.end method

.method static synthetic i(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->l:Ljava/util/List;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->e:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->h:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->setText(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->setTopPostion(I)V

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->setBubbleAndIndicatorLeftPosition(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->setIndicatorVisibility(Z)V

    iput v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->m:I

    return-void
.end method

.method public a(ZZZ)V
    .locals 4

    const/4 v0, -0x1

    const/4 v3, 0x0

    iput-boolean p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->q:Z

    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    if-eqz p2, :cond_0

    const/16 v0, 0x18

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    :goto_0
    const/4 v0, -0x3

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->format:I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    const/16 v0, 0x33

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b()V

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->r:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    :try_start_0
    invoke-interface {v0, p0, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/view/WindowManager$InvalidDisplayException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz p3, :cond_1

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->setIndicatorVisibility(Z)V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->h:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->n:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_2
    return-void

    :cond_0
    const/16 v0, 0x8

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Landroid/view/WindowManager$BadTokenException;->printStackTrace()V

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Landroid/view/WindowManager$InvalidDisplayException;->printStackTrace()V

    goto :goto_2

    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->e:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->l:Ljava/util/List;

    iget v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->m:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->r:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    :try_start_0
    invoke-interface {v0, p0}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public setArrowVisibility(Z)V
    .locals 2

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->f:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setBubbleAndIndicatorLeftPosition(I)V
    .locals 5

    invoke-direct {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->getDefaultDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v2, v0, 0x3

    mul-int/lit8 v3, v2, 0x2

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->g:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int v4, v1, v0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v4, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->f:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getWidth()I

    move-result v4

    sub-int v4, v1, v4

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-ge v1, v2, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->i:Landroid/widget/LinearLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p1

    move v1, v0

    goto :goto_0

    :cond_2
    if-le v1, v3, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->i:Landroid/widget/LinearLayout;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->i:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    goto :goto_1
.end method

.method public setIndicatorVisibility(Z)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->g:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->g:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setOnIndicatorTouchListener(Lcom/sec/android/widgetapp/easymodecontactswidget/aj;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->p:Lcom/sec/android/widgetapp/easymodecontactswidget/aj;

    return-void
.end method

.method public setPostionFromBottom(I)V
    .locals 1

    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ag;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/ag;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/ab;I)V

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTopPostion(I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->d:Landroid/view/View;

    invoke-virtual {v0, v1, p1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method public setTouchableArea(Landroid/graphics/Rect;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p1, Landroid/graphics/Rect;->right:I

    iget v2, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/ab;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    return-void
.end method
