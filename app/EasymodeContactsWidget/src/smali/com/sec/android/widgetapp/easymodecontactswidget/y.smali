.class Lcom/sec/android/widgetapp/easymodecontactswidget/y;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/easymodecontactswidget/w;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/y;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/y;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->b(Lcom/sec/android/widgetapp/easymodecontactswidget/w;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/y;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "SeniorShowOrCreateDialogFragment"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/y;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    invoke-static {v0, p1, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/w;Landroid/content/DialogInterface;Z)V

    sparse-switch v1, :sswitch_data_0

    const-string v0, "SeniorShowOrCreateDialogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected resource: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/y;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->b(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/y;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/w;I)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/y;->a:Lcom/sec/android/widgetapp/easymodecontactswidget/w;

    invoke-static {v0, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/w;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/w;I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0a0007 -> :sswitch_0
        0x7f0a0015 -> :sswitch_1
    .end sparse-switch
.end method
