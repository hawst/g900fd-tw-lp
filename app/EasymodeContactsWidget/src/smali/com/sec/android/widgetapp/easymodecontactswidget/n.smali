.class public Lcom/sec/android/widgetapp/easymodecontactswidget/n;
.super Landroid/appwidget/AppWidgetProvider;


# static fields
.field private static b:Landroid/database/ContentObserver;

.field private static e:Z

.field private static f:Z

.field private static g:Z

.field private static h:Z

.field private static final i:[Ljava/lang/String;


# instance fields
.field final a:[Ljava/lang/String;

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

.field private j:Lcom/sec/android/widgetapp/easymodecontactswidget/r;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->e:Z

    sput-boolean v2, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->f:Z

    sput-boolean v2, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->g:Z

    sput-boolean v2, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->h:Z

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->i:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "data3"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "display_name_alt"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "is_super_primary"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "photo_file_id"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->a:[Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/widgetapp/easymodecontactswidget/n;Lcom/sec/android/widgetapp/easymodecontactswidget/r;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->j:Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    return-object p1
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;I)V
    .locals 5

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f030001

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorFavoriteWidgetProviderLarge;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "appWidgetId"

    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v3, "ACTION_MODE"

    const-string v4, "EDIT"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {p0, v3, v2, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    const v2, 0x7f080003

    invoke-virtual {v0, p2, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    invoke-virtual {v0, p2, v1}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget(ILandroid/widget/RemoteViews;)V

    return-void
.end method

.method static synthetic a()Z
    .locals 1

    sget-boolean v0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->f:Z

    return v0
.end method

.method static synthetic a(Z)Z
    .locals 0

    sput-boolean p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->f:Z

    return p0
.end method

.method static synthetic b(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/l;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    return-object v0
.end method

.method static synthetic b()Z
    .locals 1

    sget-boolean v0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->g:Z

    return v0
.end method

.method static synthetic b(Z)Z
    .locals 0

    sput-boolean p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->g:Z

    return p0
.end method

.method static synthetic c(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)Lcom/sec/android/widgetapp/easymodecontactswidget/r;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->j:Lcom/sec/android/widgetapp/easymodecontactswidget/r;

    return-object v0
.end method

.method static synthetic c(Z)Z
    .locals 0

    sput-boolean p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->e:Z

    return p0
.end method

.method static synthetic c()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->i:[Ljava/lang/String;

    return-object v0
.end method

.method private declared-synchronized d()V
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/s;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/s;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/n;Landroid/os/Handler;)V

    sput-object v0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->b:Landroid/database/ContentObserver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Z)Z
    .locals 0

    sput-boolean p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->h:Z

    return p0
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .locals 6

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    invoke-direct {v0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget v2, p2, v0

    iget-object v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "widget_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->a(Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->b()V

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->b:Landroid/database/ContentObserver;

    if-eqz v1, :cond_0

    const-string v1, "SeniorFavoriteWidgetProviderBase"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unregisterContentObserver() = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->b:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->b:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c:Landroid/content/Context;

    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/s;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/s;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/n;Landroid/os/Handler;)V

    sput-object v0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->b:Landroid/database/ContentObserver;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    sget-object v3, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->b:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const-string v0, "SeniorFavoriteWidgetProviderBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onEnabled registerContentObserver() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->b:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11

    iput-object p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c:Landroid/content/Context;

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "appWidgetId"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v0, "position"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string v0, "ACTION_MODE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "editState"

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v2, "contactwidgetid"

    const/4 v6, -0x1

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const/4 v6, -0x1

    if-eq v2, v6, :cond_9

    :goto_1
    const-string v3, "contact_id"

    const-wide/16 v6, -0x1

    invoke-virtual {p2, v3, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v3, ""

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v8, "SeniorFavoriteWidgetProviderBase"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onReceive action:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " appWidgetId:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ExtraMode:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " editState:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " actionMode:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, -0x1

    if-eq v2, v8, :cond_1

    new-instance v8, Landroid/content/Intent;

    const-class v9, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;

    invoke-direct {v8, p1, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v9, "position"

    invoke-virtual {v8, v9, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "appWidgetId"

    invoke-virtual {v8, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "ACTION_MODE"

    invoke-virtual {v8, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "contact_id"

    invoke-virtual {v8, v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v4, "editState"

    invoke-virtual {v8, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p1, v8}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_1
    sget-object v4, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->b:Landroid/database/ContentObserver;

    if-nez v4, :cond_2

    invoke-direct {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d()V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x1

    sget-object v7, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->b:Landroid/database/ContentObserver;

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const-string v4, "SeniorFavoriteWidgetProviderBase"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "registerContentObserver() = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->b:Landroid/database/ContentObserver;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;

    invoke-direct {v4}, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;-><init>()V

    iget-object v5, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c:Landroid/content/Context;

    iput-object v5, v4, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;->a:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    iput-object v5, v4, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;->b:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    new-instance v5, Lcom/sec/android/widgetapp/easymodecontactswidget/o;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/o;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)V

    iput-object v5, v4, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;->c:Ljava/lang/Runnable;

    new-instance v5, Lcom/sec/android/widgetapp/easymodecontactswidget/u;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/u;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)V

    const/4 v6, 0x1

    new-array v6, v6, [Lcom/sec/android/widgetapp/easymodecontactswidget/ak;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-virtual {v5, v6}, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    sget-boolean v4, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->h:Z

    if-nez v4, :cond_4

    const-string v4, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    sput-boolean v4, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->f:Z

    :cond_3
    const-string v4, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    sput-boolean v4, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->g:Z

    :cond_4
    if-eqz p2, :cond_5

    if-eqz v1, :cond_5

    iget-object v4, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c:Landroid/content/Context;

    if-eqz v4, :cond_5

    const-string v4, "EDIT_MODE"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorFavoriteWidgetProviderLarge;

    invoke-direct {v1, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "appWidgetId"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v4, 0x1

    invoke-virtual {p2, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v4, "ACTION_MODE"

    const-string v5, "EDIT"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v4, 0x10000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v4, "editState"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v0, 0x0

    invoke-static {p1, v2, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->d(Landroid/content/Context;IZ)V

    :cond_5
    :goto_2
    instance-of v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorFavoriteWidgetProviderLarge;

    if-eqz v0, :cond_7

    sget-boolean v0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->f:Z

    if-nez v0, :cond_6

    sget-boolean v0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->g:Z

    if-eqz v0, :cond_7

    :cond_6
    const-string v0, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-boolean v0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->e:Z

    if-nez v0, :cond_7

    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->e:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/widgetapp/easymodecontactswidget/p;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/p;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)V

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_7
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_8
    const-string v0, "HELP_MODE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    invoke-static {p1, v2, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->d(Landroid/content/Context;IZ)V

    goto :goto_2

    :cond_9
    move v2, v3

    goto/16 :goto_1
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 11

    const v10, 0x7f080003

    const/high16 v9, 0x8000000

    const/4 v8, 0x1

    const/4 v1, 0x0

    const-string v0, "SeniorFavoriteWidgetProviderBase"

    const-string v2, "onUpdate()"

    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c:Landroid/content/Context;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    :cond_0
    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f030001

    invoke-direct {v2, v0, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    array-length v3, p3

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget v4, p3, v0

    const-string v5, "SeniorFavoriteWidgetProviderBase"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "appWidgetId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorWidgetRemoteViewLargeService;

    invoke-direct {v5, p1, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "appWidgetId"

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v5, v8}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {v2, v10, v5}, Landroid/widget/RemoteViews;->setRemoteAdapter(ILandroid/content/Intent;)V

    new-instance v6, Landroid/content/Intent;

    const-class v7, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorFavoriteWidgetProviderLarge;

    invoke-direct {v6, p1, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v7, "appWidgetId"

    invoke-virtual {v6, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v5, v8}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v5, "ACTION_MODE"

    const-string v7, "EDIT"

    invoke-virtual {v6, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p1, v1, v6, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorFavoriteWidgetProviderLarge;

    invoke-direct {v5, p1, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "appWidgetId"

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v5, v8}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-static {p1, v1, v5, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v2, v10, v5}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    invoke-static {p1, v4, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a(Landroid/content/Context;IZ)Z

    invoke-virtual {p2, v4, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->b:Landroid/database/ContentObserver;

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d()V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->b:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v8, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const-string v0, "SeniorFavoriteWidgetProviderBase"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerContentObserver() = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->b:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;-><init>()V

    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->c:Landroid/content/Context;

    iput-object v2, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    iput-object v2, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;->b:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    new-instance v2, Lcom/sec/android/widgetapp/easymodecontactswidget/q;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/q;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)V

    iput-object v2, v0, Lcom/sec/android/widgetapp/easymodecontactswidget/ak;->c:Ljava/lang/Runnable;

    new-instance v2, Lcom/sec/android/widgetapp/easymodecontactswidget/u;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/u;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/n;)V

    new-array v3, v8, [Lcom/sec/android/widgetapp/easymodecontactswidget/ak;

    aput-object v0, v3, v1

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/u;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    return-void
.end method
