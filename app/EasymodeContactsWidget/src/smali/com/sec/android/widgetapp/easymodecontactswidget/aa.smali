.class Lcom/sec/android/widgetapp/easymodecontactswidget/aa;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# static fields
.field private static e:I


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:I

.field private d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->e:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    iput p2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->b:I

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->e:I

    return-void
.end method


# virtual methods
.method a(I)I
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "widget_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->c(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a([B)Landroid/graphics/Bitmap;
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p1, v0, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getCount()I
    .locals 1

    sget v0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->e:I

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.action.favoritecontacts.EDITMODEINTENT_LARGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "appWidgetId"

    iget v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "ACTION_MODE"

    const-string v2, "EDITCHECK"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 13

    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->b:I

    invoke-static {v0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/m;->a(II)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    invoke-virtual {v1, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->c(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_0

    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a:Z

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const-wide/16 v4, -0x1

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    iget v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->b:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a(Landroid/content/Context;IZ)Z

    move-result v8

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    iget v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->b:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->c(Landroid/content/Context;IZ)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz p1, :cond_2

    :cond_1
    if-nez v2, :cond_6

    if-eqz v0, :cond_2

    if-nez v8, :cond_6

    :cond_2
    const/4 v1, 0x1

    :goto_2
    const-string v3, "StackRemoteViewsLargeFactory"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "enable:"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " isEditModeEnabled:"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " isHelpMode:"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " mAppWidgetId:"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->b:I

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " position:"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " isEmpty:"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_9

    if-eqz v0, :cond_7

    const v2, 0x7f030003

    :goto_3
    new-instance v6, Landroid/widget/RemoteViews;

    iget-object v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const/4 v2, 0x0

    if-nez v0, :cond_11

    const v3, 0x7f080006

    const/4 v4, 0x0

    invoke-virtual {v6, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const-string v3, "contact_id"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-string v3, "display_name_primary"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v3, "display_name"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v10, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "android.contacts.DISPLAY_ORDER"

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    if-nez v3, :cond_a

    if-nez v9, :cond_a

    :try_start_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v9, 0x7f0a0010

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v9, 0x7f080006

    invoke-virtual {v6, v9, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v9, 0x7f080006

    invoke-virtual {v6, v9, v3}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    if-eqz v8, :cond_c

    const v2, 0x7f080007

    const/4 v9, 0x0

    invoke-virtual {v6, v2, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :goto_5
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->a(J)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_d

    new-instance v9, Ljava/io/File;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    const v9, 0x7f080005

    invoke-virtual {v6, v9, v2}, Landroid/widget/RemoteViews;->setImageViewUri(ILandroid/net/Uri;)V

    :cond_3
    :goto_6
    move-object v2, v3

    :goto_7
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    if-eqz v1, :cond_4

    if-eqz v8, :cond_14

    const v1, 0x7f080004

    const/4 v3, 0x0

    invoke-virtual {v6, v1, v3}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    if-eqz v0, :cond_13

    const-string v0, "StackRemoteViewsLargeFactory"

    const-string v1, "DO NOTHING!"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :goto_8
    move-object v0, v6

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_7
    if-eqz v8, :cond_8

    const v2, 0x7f030004

    goto/16 :goto_3

    :cond_8
    const v2, 0x7f030002

    goto/16 :goto_3

    :cond_9
    const v2, 0x7f030005

    goto/16 :goto_3

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    move-object v3, v2

    goto :goto_4

    :cond_a
    const/4 v2, 0x2

    if-ne v10, v2, :cond_b

    const v2, 0x7f080006

    invoke-virtual {v6, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v2, 0x7f080006

    invoke-virtual {v6, v2, v3}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_4

    :cond_b
    const v2, 0x7f080006

    invoke-virtual {v6, v2, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v2, 0x7f080006

    invoke-virtual {v6, v2, v9}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_4

    :cond_c
    const v2, 0x7f080007

    const/4 v9, 0x4

    invoke-virtual {v6, v2, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_5

    :cond_d
    const-string v2, "photo"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v7, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_f

    const/4 v2, 0x1

    :goto_9
    if-eqz v2, :cond_10

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a([B)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_e

    iget-object v9, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    invoke-static {v9, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->b(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    :cond_e
    const v9, 0x7f080005

    const-string v10, "setImageBitmap"

    invoke-virtual {v6, v9, v10, v2}, Landroid/widget/RemoteViews;->setBitmap(ILjava/lang/String;Landroid/graphics/Bitmap;)V

    goto/16 :goto_6

    :cond_f
    const/4 v2, 0x0

    goto :goto_9

    :cond_10
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    invoke-static {v2, v4, v5}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->a(Landroid/content/Context;J)Landroid/graphics/Bitmap;

    move-result-object v2

    const v9, 0x7f080005

    const-string v10, "setImageBitmap"

    invoke-virtual {v6, v9, v10, v2}, Landroid/widget/RemoteViews;->setBitmap(ILjava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_6

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto/16 :goto_6

    :cond_11
    const v3, 0x7f080007

    const/4 v9, 0x4

    invoke-virtual {v6, v3, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v3, 0x7f080006

    const/4 v9, 0x4

    invoke-virtual {v6, v3, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    if-eqz v1, :cond_12

    const v3, 0x7f020001

    iput v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->c:I

    :goto_a
    const v3, 0x7f080005

    iget v9, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->c:I

    invoke-virtual {v6, v3, v9}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_7

    :cond_12
    const v3, 0x7f020002

    iput v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->c:I

    goto :goto_a

    :cond_13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f080004

    invoke-virtual {v6, v1, v0}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "position"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "ACTION_MODE"

    const-string v3, "DELETE"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "contact_id"

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const v1, 0x7f080004

    invoke-virtual {v6, v1, v0}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    goto/16 :goto_8

    :cond_14
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "position"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "appWidgetId"

    iget v7, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->b:I

    invoke-virtual {v2, v3, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-eqz v0, :cond_15

    const v0, 0x7f080004

    iget-object v3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v6, v0, v3}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    const-string v0, "ACTION_MODE"

    const-string v3, "ITEM_ADD"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const v0, 0x7f080004

    invoke-virtual {v6, v0, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    goto/16 :goto_8

    :cond_15
    const v0, 0x7f080004

    const/4 v3, 0x0

    invoke-virtual {v6, v0, v3}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    const-string v0, "contact_id"

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "ACTION_MODE"

    const-string v3, "ITEM_VIEW"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const v0, 0x7f080004

    invoke-virtual {v6, v0, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    goto/16 :goto_8
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onCreate()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    const-string v0, "StackRemoteViewsLargeFactory"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->b:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a(Landroid/content/Context;IZ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->b:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->b(Landroid/content/Context;IZ)V

    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->b:I

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->f:I

    return-void
.end method

.method public onDataSetChanged()V
    .locals 1

    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->b:I

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->f:I

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/aa;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->b()V

    :cond_0
    return-void
.end method
