.class Lcom/sec/android/widgetapp/easymodecontactswidget/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:Landroid/content/Intent;

.field final synthetic d:Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;IILandroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/e;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;

    iput p2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/e;->a:I

    iput p3, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/e;->b:I

    iput-object p4, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/e;->c:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    new-instance v2, Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/e;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;-><init>(Landroid/content/Context;)V

    iget v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/e;->a:I

    iget v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/e;->b:I

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/m;->a(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->c(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    const-wide/16 v0, -0x1

    if-eqz v3, :cond_1

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_0

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    const-string v0, "contact_id"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :cond_0
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "widget_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/e;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "\' AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "position"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/e;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v4, 0x27

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->a(Ljava/lang/String;)I

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->b()V

    :cond_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/e;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v0, v1, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/b;->a(Landroid/content/Context;JZ)V

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/e;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/e;->c:Landroid/content/Intent;

    iget v2, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/e;->a:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->a(Landroid/content/Context;Landroid/content/Intent;I)V

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/e;->d:Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;

    invoke-static {v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;->a(Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;)V

    return-void
.end method
