.class public Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;
.super Landroid/app/IntentService;


# instance fields
.field private a:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "SeniorContactSelectorService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/widgetapp/easymodecontactswidget/k;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/k;-><init>(Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->a:Landroid/os/Handler;

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 6

    const/4 v5, 0x0

    const-string v0, "appWidgetId"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "editState"

    invoke-virtual {p1, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    sget-boolean v2, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a:Z

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v5}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a(Landroid/content/Context;IZ)Z

    move-result v2

    if-eq v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->b(Landroid/content/Context;IZ)V

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/widgetapp/easymodecontactswidget/l;

    invoke-direct {v2, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;-><init>(Landroid/content/Context;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "widget_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->b(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/l;->b()V

    invoke-static {v1, p1, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/n;->a(Landroid/content/Context;Landroid/content/Intent;I)V

    :cond_2
    sget-boolean v0, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a:Z

    if-eqz v0, :cond_0

    sput-boolean v5, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a:Z

    goto :goto_0
.end method

.method private b(Landroid/content/Intent;)V
    .locals 4

    const-string v0, "appWidgetId"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f030001

    invoke-direct {v2, v1, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    if-eqz v2, :cond_0

    :try_start_0
    invoke-virtual {v1, v0, v2}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget(ILandroid/widget/RemoteViews;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method private c(Landroid/content/Intent;)V
    .locals 8

    const-wide/16 v6, -0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    sget-boolean v0, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->a:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "appWidgetId"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "position"

    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "contact_id"

    invoke-virtual {p1, v2, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    if-eq v0, v4, :cond_0

    if-eq v1, v4, :cond_0

    sput-boolean v5, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a:Z

    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/widgetapp/easymodecontactswidget/RemoveDialogActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v3, 0x10008000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v3, "appWidgetId"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "position"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private d(Landroid/content/Intent;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v4, -0x1

    sget-boolean v0, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sput-boolean v1, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->b:Z

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->a:Landroid/os/Handler;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->a:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_2
    sget-boolean v0, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_3
    const-string v0, "appWidgetId"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "position"

    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorContactSelector;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v3, 0x10008000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v3, "appWidgetId"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "position"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private e(Landroid/content/Intent;)V
    .locals 8

    const-wide/16 v6, -0x1

    const/4 v4, 0x0

    sget-boolean v0, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->a:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "appWidgetId"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "contact_id"

    invoke-virtual {p1, v1, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v1, v2, v6

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v4}, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a(Landroid/content/Context;IZ)Z

    move-result v0

    if-nez v0, :cond_0

    sput-boolean v4, Lcom/sec/android/widgetapp/easymodecontactswidget/v;->a:Z

    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/android/widgetapp/easymodecontactswidget/QuickContactLauncherActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/high16 v0, 0x14200000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    const-string v0, "ACTION_MODE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SeniorEventProcessService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onHandleIntent action:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "NONE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "SeniorEventProcessService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onHandleIntent action:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", Do Nothing!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/easymodecontactswidget/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "ITEM_ADD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->d(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string v1, "ITEM_VIEW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->e(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    const-string v1, "EDIT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->a(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    const-string v1, "DELETE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->c(Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    const-string v1, "EDITCHECK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/easymodecontactswidget/SeniorEventProcessService;->b(Landroid/content/Intent;)V

    goto :goto_0
.end method
