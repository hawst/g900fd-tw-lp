.class Lcom/sec/kidsplat/installer/KidsModeInstallReceiver$1;
.super Ljava/lang/Object;
.source "KidsModeInstallReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/kidsplat/installer/KidsModeInstallReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/kidsplat/installer/KidsModeInstallReceiver;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/sec/kidsplat/installer/KidsModeInstallReceiver;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/kidsplat/installer/KidsModeInstallReceiver$1;->this$0:Lcom/sec/kidsplat/installer/KidsModeInstallReceiver;

    iput-object p2, p0, Lcom/sec/kidsplat/installer/KidsModeInstallReceiver$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/kidsplat/installer/KidsModeInstallReceiver$1;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/kidsplat/installer/KidsModeInstallReceiver$1;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/kidsplat/installer/KidsModeInstallReceiver$1;->this$0:Lcom/sec/kidsplat/installer/KidsModeInstallReceiver;

    iget-object v2, p0, Lcom/sec/kidsplat/installer/KidsModeInstallReceiver$1;->val$intent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Lcom/sec/kidsplat/installer/KidsModeInstallReceiver;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/StubAppConfig;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/sec/kidsplat/installer/KidsModeInstallReceiver$1;->this$0:Lcom/sec/kidsplat/installer/KidsModeInstallReceiver;

    iget-object v1, p0, Lcom/sec/kidsplat/installer/KidsModeInstallReceiver$1;->val$context:Landroid/content/Context;

    # invokes: Lcom/sec/kidsplat/installer/KidsModeInstallReceiver;->deleteAllKidsModePackage(Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/KidsModeInstallReceiver;->access$000(Lcom/sec/kidsplat/installer/KidsModeInstallReceiver;Landroid/content/Context;)V

    .line 41
    iget-object v0, p0, Lcom/sec/kidsplat/installer/KidsModeInstallReceiver$1;->this$0:Lcom/sec/kidsplat/installer/KidsModeInstallReceiver;

    iget-object v1, p0, Lcom/sec/kidsplat/installer/KidsModeInstallReceiver$1;->val$context:Landroid/content/Context;

    # invokes: Lcom/sec/kidsplat/installer/KidsModeInstallReceiver;->deleteUnusedAPKs(Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/KidsModeInstallReceiver;->access$100(Lcom/sec/kidsplat/installer/KidsModeInstallReceiver;Landroid/content/Context;)V

    .line 45
    :cond_0
    return-void
.end method
