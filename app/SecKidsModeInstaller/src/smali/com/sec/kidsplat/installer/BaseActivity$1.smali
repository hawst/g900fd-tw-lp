.class Lcom/sec/kidsplat/installer/BaseActivity$1;
.super Landroid/os/Handler;
.source "BaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/kidsplat/installer/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/kidsplat/installer/BaseActivity;


# direct methods
.method constructor <init>(Lcom/sec/kidsplat/installer/BaseActivity;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 139
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "MESSAGE_CMD"

    const-string v2, "DOWNLOAD_STATUS"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 141
    .local v11, "cmd":Ljava/lang/String;
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 414
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 143
    :pswitch_1
    const-string v0, "DOWNLOAD_STATUS"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 144
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "DOWNLOAD_STATUS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 148
    :pswitch_2
    const/4 v0, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mNeedUpdate:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$002(Z)Z

    .line 149
    const-string v0, "KidsPlatformStubBaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "STATUS_DOWNLOADED, mNeedUpdate :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mNeedUpdate:Z
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$000()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 152
    :pswitch_3
    const/4 v0, 0x1

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isInstalling:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$102(Z)Z

    .line 153
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$200(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->isInstalling:Z
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$100()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 154
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mBtnCancel:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 155
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTvDownloading:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$400(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 159
    :pswitch_4
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$500(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$500(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 161
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$502(Lcom/sec/kidsplat/installer/BaseActivity;Ljava/util/Timer;)Ljava/util/Timer;

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->cancel()V

    .line 165
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->cancel(Z)Z

    .line 166
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$602(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    .line 168
    :cond_2
    const/4 v0, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$702(Z)Z

    .line 169
    const/4 v0, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isInstalling:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$102(Z)Z

    goto/16 :goto_0

    .line 175
    :pswitch_5
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$500(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 176
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$500(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 177
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$502(Lcom/sec/kidsplat/installer/BaseActivity;Ljava/util/Timer;)Ljava/util/Timer;

    .line 179
    :cond_3
    const/4 v0, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$702(Z)Z

    .line 180
    const/4 v0, 0x1

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mNeedUpdate:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$002(Z)Z

    .line 181
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v0

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->isNetworkConnected(Landroid/content/Context;)Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$900(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 182
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->getNetworkErrorState()I
    invoke-static {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1000(Lcom/sec/kidsplat/installer/BaseActivity;)I

    move-result v1

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->sendBroadcastForNetworkErrorPopup(I)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1100(Lcom/sec/kidsplat/installer/BaseActivity;I)V

    .line 183
    :cond_4
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 184
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->cancel()V

    .line 185
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->cancel(Z)Z

    .line 186
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$602(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    .line 188
    :cond_5
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->setInstallLayout(Z)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1200(Lcom/sec/kidsplat/installer/BaseActivity;Z)V

    goto/16 :goto_0

    .line 191
    :pswitch_6
    const-string v0, "KidsPlatformStubBaseActivity"

    const-string v1, "handleMessage : STATUS_INSTALL_FAIL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 193
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->cancel()V

    .line 194
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->cancel(Z)Z

    .line 195
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$602(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    .line 197
    :cond_6
    const/4 v0, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isInstalling:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$102(Z)Z

    .line 198
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$200(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->isInstalling:Z
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$100()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 199
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x1

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mStartFromFirst:Z
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1302(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    .line 200
    const/4 v0, 0x1

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mNeedUpdate:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$002(Z)Z

    .line 201
    const/4 v0, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$702(Z)Z

    .line 202
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mBtnCancel:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 203
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTvDownloading:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$400(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->setInstallLayout(Z)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1200(Lcom/sec/kidsplat/installer/BaseActivity;Z)V

    goto/16 :goto_0

    .line 208
    :pswitch_7
    const-string v0, "KidsPlatformStubBaseActivity"

    const-string v1, "STATUS_REQUEST_DOWNLOAD"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 210
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->cancel()V

    .line 211
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->interrupt()V

    .line 212
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1402(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateCheckThread;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    .line 214
    :cond_7
    const/4 v0, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mNeedUpdate:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$002(Z)Z

    .line 215
    const-string v0, "KidsPlatformStubBaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "STATUS_REQUEST_DOWNLOAD, mNeedUpdate :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mNeedUpdate:Z
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$000()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    .line 217
    .local v12, "mMessageData":Landroid/os/Bundle;
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->MESSAGE_ID:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1600(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDataID:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1502(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 218
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->MESSAGE_RESULT:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1800(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDataResult:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1702(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 219
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->MESSAGE_DATA_URI:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2000(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDataUri:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1902(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 220
    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$700()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    iget-object v13, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    new-instance v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2100(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/os/Handler;

    move-result-object v2

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDataID:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1500()Ljava/util/ArrayList;

    move-result-object v3

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDataResult:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1700()Ljava/util/ArrayList;

    move-result-object v4

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDataUri:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1900()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    iget-object v7, v7, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->apkSize:D
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2200()D

    move-result-wide v8

    invoke-direct/range {v0 .. v9}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Boolean;Lcom/sec/kidsplat/installer/StubAppConfig$Config;D)V

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v13, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$602(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    .line 223
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Download"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 224
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mStartFromFirst:Z
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1302(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    .line 226
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$500(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/util/Timer;

    move-result-object v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$502(Lcom/sec/kidsplat/installer/BaseActivity;Ljava/util/Timer;)Ljava/util/Timer;

    .line 228
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$500(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/util/Timer;

    move-result-object v0

    new-instance v1, Lcom/sec/kidsplat/installer/BaseActivity$ProgressTimeTask;

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-direct {v1, v2}, Lcom/sec/kidsplat/installer/BaseActivity$ProgressTimeTask;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto/16 :goto_0

    .line 233
    .end local v12    # "mMessageData":Landroid/os/Bundle;
    :pswitch_8
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 234
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->cancel()V

    .line 235
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->interrupt()V

    .line 236
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1402(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateCheckThread;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    .line 238
    :cond_8
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 240
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 241
    :cond_9
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mShowProgressDialog:Z
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2402(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    .line 242
    const/4 v0, 0x1

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mNeedUpdate:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$002(Z)Z

    .line 243
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->setInstallLayout(Z)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1200(Lcom/sec/kidsplat/installer/BaseActivity;Z)V

    .line 244
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->getNetworkErrorState()I
    invoke-static {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1000(Lcom/sec/kidsplat/installer/BaseActivity;)I

    move-result v1

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->sendBroadcastForNetworkErrorPopup(I)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1100(Lcom/sec/kidsplat/installer/BaseActivity;I)V

    goto/16 :goto_0

    .line 247
    :pswitch_9
    const-string v0, "KidsPlatformStubBaseActivity"

    const-string v1, "STATUS_DOWNLOADED_ALL_COMPLETED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 249
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->cancel()V

    .line 250
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->interrupt()V

    .line 251
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1402(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateCheckThread;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    .line 253
    :cond_a
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 255
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 258
    :cond_b
    const/4 v0, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$702(Z)Z

    .line 259
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mShowProgressDialog:Z
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2402(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    .line 260
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->setInstallLayout(Z)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1200(Lcom/sec/kidsplat/installer/BaseActivity;Z)V

    .line 261
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mBtnUpdate:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2500(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f0c0008

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 262
    new-instance v10, Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->KIDSMODE_INSTALL_COMPLETED:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2600(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 263
    .local v10, "br":Landroid/content/Intent;
    const/16 v0, 0x20

    invoke-virtual {v10, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 264
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 265
    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->isHomeKeyPressed:Z
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2700()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    const-string v0, "KidsPlatformStubBaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isHomeKeyPressed : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->isHomeKeyPressed:Z
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2700()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->DisableActivity()V
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2800(Lcom/sec/kidsplat/installer/BaseActivity;)V

    goto/16 :goto_0

    .line 272
    .end local v10    # "br":Landroid/content/Intent;
    :pswitch_a
    const-string v0, "KidsPlatformStubBaseActivity"

    const-string v1, "handleMessage : STATUS_SIGNATURE_FAIL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 274
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->interrupt()V

    .line 275
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->cancel()V

    .line 276
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1402(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateCheckThread;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    .line 279
    :cond_c
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 280
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->cancel()V

    .line 281
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->cancel(Z)Z

    .line 282
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$602(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    .line 285
    :cond_d
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$500(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 286
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$500(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 287
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$500(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 288
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$502(Lcom/sec/kidsplat/installer/BaseActivity;Ljava/util/Timer;)Ljava/util/Timer;

    .line 290
    :cond_e
    const/4 v0, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$702(Z)Z

    .line 291
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->setInstallLayout(Z)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1200(Lcom/sec/kidsplat/installer/BaseActivity;Z)V

    .line 292
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->setInstallLayout(Z)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1200(Lcom/sec/kidsplat/installer/BaseActivity;Z)V

    .line 293
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const v1, 0x7f0c001a

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->showErrorDialog(I)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2900(Lcom/sec/kidsplat/installer/BaseActivity;I)V

    goto/16 :goto_0

    .line 296
    :cond_f
    const-string v0, "DOWNLOA_RATIO"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 297
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "DOWNLOA_RATIO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3002(I)I

    .line 298
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->cancel()V

    .line 300
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->interrupt()V

    .line 301
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1402(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateCheckThread;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    goto/16 :goto_0

    .line 303
    :cond_10
    const-string v0, "DOWNLODING_PROGRESS_UPDATE"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 304
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$200(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3000()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 305
    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3000()I

    move-result v0

    if-eqz v0, :cond_11

    .line 306
    const-string v0, "DOWNLOAD_RATIO"

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3000()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 307
    :cond_11
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewDownlodingSizeCurrent:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3200(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%.1f"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const v3, 0x7f0c000d

    invoke-virtual {v2, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mAppSize:D
    invoke-static {v4}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3100(Lcom/sec/kidsplat/installer/BaseActivity;)D

    move-result-wide v4

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3000()I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 309
    :cond_12
    const-string v0, "RETURNCODE_ERROR"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 310
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x1

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mStartFromFirst:Z
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1302(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    .line 311
    const/4 v0, 0x1

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mNeedUpdate:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$002(Z)Z

    .line 312
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mBtnCancel:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 313
    const/4 v0, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$702(Z)Z

    .line 314
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 315
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->interrupt()V

    .line 316
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->cancel()V

    .line 317
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1402(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateCheckThread;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    .line 320
    :cond_13
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 321
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->cancel()V

    .line 322
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->cancel(Z)Z

    .line 323
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$602(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    .line 326
    :cond_14
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$500(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 327
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$500(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 328
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$500(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 329
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$502(Lcom/sec/kidsplat/installer/BaseActivity;Ljava/util/Timer;)Ljava/util/Timer;

    .line 332
    :cond_15
    const/4 v0, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isInstalling:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$102(Z)Z

    .line 333
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$200(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->isInstalling:Z
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$100()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 334
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->setInstallLayout(Z)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1200(Lcom/sec/kidsplat/installer/BaseActivity;Z)V

    .line 335
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const v1, 0x7f0c001a

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->showErrorDialog(I)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2900(Lcom/sec/kidsplat/installer/BaseActivity;I)V

    .line 336
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTvDownloading:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$400(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 337
    :cond_16
    const-string v0, "APK_SIZE"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 339
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    iget-object v0, v0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-boolean v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsUpdateMode:Z

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    iget-object v0, v0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 340
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->showUpdateDialog()V
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3300(Lcom/sec/kidsplat/installer/BaseActivity;)V

    .line 342
    :cond_17
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 343
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 346
    :cond_18
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mShowProgressDialog:Z
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2402(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    .line 347
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "APK_SIZE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->apkSize:D
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2202(D)D

    .line 348
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->apkSize:D
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2200()D

    move-result-wide v2

    const-wide/high16 v4, 0x4130000000000000L    # 1048576.0

    div-double/2addr v2, v4

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mAppSize:D
    invoke-static {v0, v2, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3102(Lcom/sec/kidsplat/installer/BaseActivity;D)D

    .line 355
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    iget-object v0, v0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 356
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mBtnUpdate:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2500(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 357
    :cond_19
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewTranslatorInformation:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3400(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const v3, 0x7f0c000c

    invoke-virtual {v2, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%.1f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mAppSize:D
    invoke-static {v5}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3100(Lcom/sec/kidsplat/installer/BaseActivity;)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const v3, 0x7f0c000d

    invoke-virtual {v2, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 359
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewDownlodingSizeCurrent:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3200(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%.1f"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const v3, 0x7f0c000d

    invoke-virtual {v2, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mAppSize:D
    invoke-static {v4}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3100(Lcom/sec/kidsplat/installer/BaseActivity;)D

    move-result-wide v4

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I
    invoke-static {}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3000()I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewForwardSlash:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3500(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3600(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%.1f"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const v3, 0x7f0c000d

    invoke-virtual {v2, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mAppSize:D
    invoke-static {v4}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3100(Lcom/sec/kidsplat/installer/BaseActivity;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 364
    :cond_1a
    const-string v0, "VERSION_CODE"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "VERSION_CODE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->versionCode:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3702(Lcom/sec/kidsplat/installer/BaseActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 366
    const-string v0, "VERSION_CODE"

    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 367
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->versionCode:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3700(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "VERSION_CODE"

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eq v0, v1, :cond_1b

    .line 368
    const-string v0, "VERSION_CODE"

    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->versionCode:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3700(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 369
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x1

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mStartFromFirst:Z
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1302(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    goto/16 :goto_0

    .line 371
    :cond_1b
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mStartFromFirst:Z
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1302(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    goto/16 :goto_0

    .line 373
    :cond_1c
    const-string v0, "VERSION_CODE"

    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->versionCode:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3700(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 374
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x1

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mStartFromFirst:Z
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1302(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    goto/16 :goto_0

    .line 379
    :pswitch_b
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 380
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->cancel()V

    .line 381
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->interrupt()V

    .line 382
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1402(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateCheckThread;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    .line 384
    :cond_1d
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 386
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 387
    :cond_1e
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mShowProgressDialog:Z
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2402(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    .line 390
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v0

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->isNetworkConnected(Landroid/content/Context;)Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$900(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 391
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->getNetworkErrorState()I
    invoke-static {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1000(Lcom/sec/kidsplat/installer/BaseActivity;)I

    move-result v1

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->sendBroadcastForNetworkErrorPopup(I)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1100(Lcom/sec/kidsplat/installer/BaseActivity;I)V

    .line 395
    :goto_1
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->setInstallLayout(Z)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1200(Lcom/sec/kidsplat/installer/BaseActivity;Z)V

    goto/16 :goto_0

    .line 393
    :cond_1f
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const v1, 0x7f0c001a

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->showErrorDialog(I)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2900(Lcom/sec/kidsplat/installer/BaseActivity;I)V

    goto :goto_1

    .line 398
    :pswitch_c
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 399
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->cancel()V

    .line 400
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->interrupt()V

    .line 401
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1402(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateCheckThread;)Lcom/sec/kidsplat/installer/UpdateCheckThread;

    .line 403
    :cond_20
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_21

    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 405
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 406
    :cond_21
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mShowProgressDialog:Z
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2402(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    .line 407
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$1;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const v1, 0x7f0c001a

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->showErrorDialog(I)V
    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2900(Lcom/sec/kidsplat/installer/BaseActivity;I)V

    .line 408
    const/4 v0, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$702(Z)Z

    goto/16 :goto_0

    .line 141
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 144
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
