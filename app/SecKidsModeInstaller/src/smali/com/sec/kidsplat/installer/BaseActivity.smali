.class public Lcom/sec/kidsplat/installer/BaseActivity;
.super Landroid/app/Activity;
.source "BaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/kidsplat/installer/BaseActivity$ProgressTimeTask;
    }
.end annotation


# static fields
.field private static final APP_INFORMATION_IMAGE:Ljava/lang/String; = "info_image_"

.field private static final NETWORKERR_DATAROAMING_OFF:I = 0x2

.field private static final NETWORKERR_FLIGHTMODE_ON:I = 0x0

.field private static final NETWORKERR_MOBILEDATA_OFF:I = 0x1

.field private static final NETWORKERR_NO_SIGNAL:I = 0x4

.field private static final NETWORKERR_REACHED_DATALIMIT:I = 0x3

.field public static final TAG:Ljava/lang/String; = "KidsPlatformStubBaseActivity"

.field private static apkSize:D = 0.0

.field private static isDownloading:Z = false

.field private static isHomeKeyPressed:Z = false

.field private static isInstalling:Z = false

.field private static mCurrentStatus:I = 0x0

.field private static mDataID:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mDataResult:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mDataUri:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mNeedUpdate:Z = false

.field private static final mSamsungappsUrl:Ljava/lang/String; = "account.samsung.com/membership/pp "

.field private static ratio:I


# instance fields
.field private KIDSMODE_INSTALL_COMPLETED:Ljava/lang/String;

.field private MESSAGE_DATA_URI:Ljava/lang/String;

.field private MESSAGE_ID:Ljava/lang/String;

.field private MESSAGE_RESULT:Ljava/lang/String;

.field private isChinaUsingPopupDone:Z

.field private isDataUsingPopupDone:Z

.field private isRoamingUsingPopupDone:Z

.field private isUserInfoPopupDone:Z

.field private mAppName:Ljava/lang/String;

.field private mAppSize:D

.field private mBtnCancel:Landroid/widget/Button;

.field private mBtnUpdate:Landroid/widget/Button;

.field private mCheckBoxDoNotShowAgain:Landroid/widget/CheckBox;

.field mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

.field private mContext:Landroid/content/Context;

.field private mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

.field private mHandler:Landroid/os/Handler;

.field private mLayoutInstall:Landroid/widget/RelativeLayout;

.field private mLayoutProgressing:Landroid/widget/LinearLayout;

.field private mPackageName:Ljava/lang/String;

.field private mProgressBarInstall:Landroid/widget/ProgressBar;

.field private mShowErrDialog:Landroid/app/AlertDialog;

.field private mShowProgressDialog:Z

.field private mStartFromFirst:Z

.field private mStubImageCnt:I

.field private mStubImageContainer:Landroid/widget/LinearLayout;

.field private mTextViewDownlodingSize:Landroid/widget/TextView;

.field private mTextViewDownlodingSizeCurrent:Landroid/widget/TextView;

.field private mTextViewForwardSlash:Landroid/widget/TextView;

.field private mTextViewTranslatorInformation:Landroid/widget/TextView;

.field private mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

.field private mTimer:Ljava/util/Timer;

.field private mTopInformation:Landroid/widget/TextView;

.field private mTvDownloading:Landroid/widget/TextView;

.field private mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

.field private mUpdateDialog:Landroid/app/AlertDialog;

.field private mUserInfoPopup:Landroid/app/Dialog;

.field private userChoice:Z

.field private versionCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 104
    sput v1, Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I

    .line 106
    sput-boolean v1, Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z

    .line 107
    sput-boolean v1, Lcom/sec/kidsplat/installer/BaseActivity;->isInstalling:Z

    .line 108
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/kidsplat/installer/BaseActivity;->mNeedUpdate:Z

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataID:Ljava/util/ArrayList;

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataResult:Ljava/util/ArrayList;

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataUri:Ljava/util/ArrayList;

    .line 135
    const/4 v0, -0x1

    sput v0, Lcom/sec/kidsplat/installer/BaseActivity;->mCurrentStatus:I

    .line 136
    sput-boolean v1, Lcom/sec/kidsplat/installer/BaseActivity;->isHomeKeyPressed:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 62
    const-string v0, "com.sec.kidsplat.installer.KIDSMODE_INSTALL_COMPLETED"

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->KIDSMODE_INSTALL_COMPLETED:Ljava/lang/String;

    .line 70
    const-string v0, "id of the message"

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->MESSAGE_ID:Ljava/lang/String;

    .line 71
    const-string v0, "result of the message"

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->MESSAGE_RESULT:Ljava/lang/String;

    .line 72
    const-string v0, "uri of the message"

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->MESSAGE_DATA_URI:Ljava/lang/String;

    .line 74
    iput-boolean v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mStartFromFirst:Z

    .line 76
    iput-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

    .line 77
    iput-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    .line 89
    iput-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mStubImageContainer:Landroid/widget/LinearLayout;

    .line 93
    iput-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mAppName:Ljava/lang/String;

    .line 94
    iput-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mPackageName:Ljava/lang/String;

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mShowProgressDialog:Z

    .line 110
    iput-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->versionCode:Ljava/lang/String;

    .line 113
    iput-boolean v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->userChoice:Z

    .line 126
    iput-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mShowErrDialog:Landroid/app/AlertDialog;

    .line 128
    iput-boolean v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isUserInfoPopupDone:Z

    .line 132
    iput-boolean v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isDataUsingPopupDone:Z

    .line 133
    iput-boolean v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isRoamingUsingPopupDone:Z

    .line 134
    iput-boolean v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isChinaUsingPopupDone:Z

    .line 137
    new-instance v0, Lcom/sec/kidsplat/installer/BaseActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/kidsplat/installer/BaseActivity$1;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mHandler:Landroid/os/Handler;

    .line 925
    return-void
.end method

.method private DisableActivity()V
    .locals 4

    .prologue
    .line 1314
    const-string v2, "KidsPlatformStubBaseActivity"

    const-string v3, "DisableActivity()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1316
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-boolean v2, v2, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsUpdateMode:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->isKidsHomeInstalled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1319
    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1320
    .local v1, "pm":Landroid/content/pm/PackageManager;
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.sec.kidsplat.installer"

    const-string v3, "com.sec.kidsplat.installer.StartActivity"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1321
    .local v0, "cn":Landroid/content/ComponentName;
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 1323
    .end local v0    # "cn":Landroid/content/ComponentName;
    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    return-void
.end method

.method private RunUpdateCheckProgress()V
    .locals 3

    .prologue
    .line 1024
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-boolean v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsUpdateMode:Z

    if-eqz v0, :cond_0

    .line 1026
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    const-string v1, ""

    const v2, 0x7f0c0012

    invoke-virtual {p0, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    .line 1035
    :goto_0
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1036
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 1037
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/kidsplat/installer/BaseActivity$17;

    invoke-direct {v1, p0}, Lcom/sec/kidsplat/installer/BaseActivity$17;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1043
    return-void

    .line 1030
    :cond_0
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    const-string v1, ""

    const v2, 0x7f0c0011

    invoke-virtual {p0, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    goto :goto_0
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 60
    sget-boolean v0, Lcom/sec/kidsplat/installer/BaseActivity;->mNeedUpdate:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 60
    sput-boolean p0, Lcom/sec/kidsplat/installer/BaseActivity;->mNeedUpdate:Z

    return p0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 60
    sget-boolean v0, Lcom/sec/kidsplat/installer/BaseActivity;->isInstalling:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/kidsplat/installer/BaseActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->getNetworkErrorState()I

    move-result v0

    return v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 60
    sput-boolean p0, Lcom/sec/kidsplat/installer/BaseActivity;->isInstalling:Z

    return p0
.end method

.method static synthetic access$1100(Lcom/sec/kidsplat/installer/BaseActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;
    .param p1, "x1"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/sec/kidsplat/installer/BaseActivity;->sendBroadcastForNetworkErrorPopup(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/kidsplat/installer/BaseActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/sec/kidsplat/installer/BaseActivity;->setInstallLayout(Z)V

    return-void
.end method

.method static synthetic access$1302(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mStartFromFirst:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateCheckThread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateCheckThread;)Lcom/sec/kidsplat/installer/UpdateCheckThread;
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;
    .param p1, "x1"    # Lcom/sec/kidsplat/installer/UpdateCheckThread;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

    return-object p1
.end method

.method static synthetic access$1500()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataID:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1502(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Ljava/util/ArrayList;

    .prologue
    .line 60
    sput-object p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataID:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->MESSAGE_ID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataResult:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1702(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Ljava/util/ArrayList;

    .prologue
    .line 60
    sput-object p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataResult:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$1800(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->MESSAGE_RESULT:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataUri:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1902(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Ljava/util/ArrayList;

    .prologue
    .line 60
    sput-object p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataUri:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$200(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->MESSAGE_DATA_URI:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2200()D
    .locals 2

    .prologue
    .line 60
    sget-wide v0, Lcom/sec/kidsplat/installer/BaseActivity;->apkSize:D

    return-wide v0
.end method

.method static synthetic access$2202(D)D
    .locals 0
    .param p0, "x0"    # D

    .prologue
    .line 60
    sput-wide p0, Lcom/sec/kidsplat/installer/BaseActivity;->apkSize:D

    return-wide p0
.end method

.method static synthetic access$2300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mShowProgressDialog:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mBtnUpdate:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->KIDSMODE_INSTALL_COMPLETED:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2700()Z
    .locals 1

    .prologue
    .line 60
    sget-boolean v0, Lcom/sec/kidsplat/installer/BaseActivity;->isHomeKeyPressed:Z

    return v0
.end method

.method static synthetic access$2800(Lcom/sec/kidsplat/installer/BaseActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->DisableActivity()V

    return-void
.end method

.method static synthetic access$2900(Lcom/sec/kidsplat/installer/BaseActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;
    .param p1, "x1"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/sec/kidsplat/installer/BaseActivity;->showErrorDialog(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mBtnCancel:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3000()I
    .locals 1

    .prologue
    .line 60
    sget v0, Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I

    return v0
.end method

.method static synthetic access$3002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 60
    sput p0, Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I

    return p0
.end method

.method static synthetic access$3100(Lcom/sec/kidsplat/installer/BaseActivity;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mAppSize:D

    return-wide v0
.end method

.method static synthetic access$3102(Lcom/sec/kidsplat/installer/BaseActivity;D)D
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;
    .param p1, "x1"    # D

    .prologue
    .line 60
    iput-wide p1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mAppSize:D

    return-wide p1
.end method

.method static synthetic access$3200(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewDownlodingSizeCurrent:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/kidsplat/installer/BaseActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->showUpdateDialog()V

    return-void
.end method

.method static synthetic access$3400(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewTranslatorInformation:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewForwardSlash:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->versionCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3702(Lcom/sec/kidsplat/installer/BaseActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->versionCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3802(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->userChoice:Z

    return p1
.end method

.method static synthetic access$3900(Lcom/sec/kidsplat/installer/BaseActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->isChinaModel()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTvDownloading:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/kidsplat/installer/BaseActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isChinaUsingPopupDone:Z

    return v0
.end method

.method static synthetic access$4002(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isChinaUsingPopupDone:Z

    return p1
.end method

.method static synthetic access$4100(Lcom/sec/kidsplat/installer/BaseActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->showDataUsingDialogForChina()V

    return-void
.end method

.method static synthetic access$4200(Lcom/sec/kidsplat/installer/BaseActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->startUpdateThread()V

    return-void
.end method

.method static synthetic access$4300(Lcom/sec/kidsplat/installer/BaseActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isDataUsingPopupDone:Z

    return v0
.end method

.method static synthetic access$4302(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isDataUsingPopupDone:Z

    return p1
.end method

.method static synthetic access$4400(Lcom/sec/kidsplat/installer/BaseActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isRoamingUsingPopupDone:Z

    return v0
.end method

.method static synthetic access$4402(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isRoamingUsingPopupDone:Z

    return p1
.end method

.method static synthetic access$4500(Lcom/sec/kidsplat/installer/BaseActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->showDataUsingDialog()V

    return-void
.end method

.method static synthetic access$4600(Lcom/sec/kidsplat/installer/BaseActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->cancelDownload()V

    return-void
.end method

.method static synthetic access$4700(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mCheckBoxDoNotShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/kidsplat/installer/BaseActivity;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;
    .param p1, "x1"    # Ljava/util/Timer;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/kidsplat/installer/BaseActivity;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/kidsplat/installer/BaseActivity;Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;
    .param p1, "x1"    # Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    return-object p1
.end method

.method static synthetic access$700()Z
    .locals 1

    .prologue
    .line 60
    sget-boolean v0, Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z

    return v0
.end method

.method static synthetic access$702(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 60
    sput-boolean p0, Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z

    return p0
.end method

.method static synthetic access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/BaseActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Landroid/content/Context;)Z
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 60
    invoke-static {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private cancelDownload()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1003
    const-string v0, "KidsPlatformStubBaseActivity"

    const-string v1, "cancelDownload()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1004
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z

    .line 1005
    sget v0, Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I

    if-eqz v0, :cond_0

    .line 1006
    const-string v0, "DOWNLOAD_RATIO"

    sget v1, Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 1008
    :cond_0
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    if-eqz v0, :cond_1

    .line 1009
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->cancel()V

    .line 1010
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->cancel(Z)Z

    .line 1011
    iput-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    .line 1016
    :cond_1
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_2

    .line 1017
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1018
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 1019
    iput-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;

    .line 1021
    :cond_2
    return-void
.end method

.method private getNetworkErrorState()I
    .locals 4

    .prologue
    .line 1061
    const/4 v1, -0x1

    .line 1062
    .local v1, "networkStatus":I
    const-string v2, "gsm.operator.isroaming"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1063
    .local v0, "isRoaming":Ljava/lang/String;
    const-string v2, "ro.carrier"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "wifi-only"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1064
    const/4 v2, 0x4

    .line 1077
    :goto_0
    return v2

    .line 1066
    :cond_0
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->isFligtMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1067
    const/4 v1, 0x0

    :goto_1
    move v2, v1

    .line 1077
    goto :goto_0

    .line 1068
    :cond_1
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->isMobileDataOff()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1069
    const/4 v1, 0x1

    goto :goto_1

    .line 1070
    :cond_2
    const-string v2, "true"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->isRoamingOff()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1071
    const/4 v1, 0x2

    goto :goto_1

    .line 1072
    :cond_3
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->isReachToDataLimit()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1073
    const/4 v1, 0x3

    goto :goto_1

    .line 1075
    :cond_4
    const/4 v1, 0x4

    goto :goto_1
.end method

.method public static getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1214
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1215
    .local v0, "preferences":Landroid/content/SharedPreferences;
    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getResId(Ljava/lang/String;Ljava/lang/Class;)I
    .locals 4
    .param p1, "resName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 572
    .local p2, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v1, 0x0

    .line 573
    .local v1, "field":Ljava/lang/reflect/Field;
    const/4 v2, 0x0

    .line 575
    .local v2, "resId":I
    :try_start_0
    invoke-virtual {p2, p1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 577
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    .line 584
    :goto_0
    return v2

    .line 578
    :catch_0
    move-exception v0

    .line 579
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 581
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 582
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private init()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 503
    new-instance v1, Lcom/sec/kidsplat/installer/StubAppConfig;

    invoke-direct {v1}, Lcom/sec/kidsplat/installer/StubAppConfig;-><init>()V

    .line 504
    .local v1, "config":Lcom/sec/kidsplat/installer/StubAppConfig;
    iget-object v6, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Lcom/sec/kidsplat/installer/StubAppConfig;->getStubAppConfig(Landroid/content/Context;)Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    .line 505
    iget-object v6, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    if-eqz v6, :cond_2

    .line 506
    iget-object v6, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v6}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v6

    if-nez v6, :cond_0

    .line 507
    const-string v6, "KidsPlatformStubBaseActivity"

    const-string v7, "there is no package to install so call finish"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->finish()V

    .line 510
    :cond_0
    iget-object v6, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v6}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppName()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mAppName:Ljava/lang/String;

    .line 511
    iget-object v6, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v6}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppInfoImageCnt()I

    move-result v6

    iput v6, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mStubImageCnt:I

    .line 512
    iget-object v6, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v6, v9}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getPackageNameList(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mPackageName:Ljava/lang/String;

    .line 514
    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 515
    .local v4, "intent":Landroid/content/Intent;
    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 516
    .local v3, "extras":Landroid/os/Bundle;
    const/4 v0, 0x0

    .line 517
    .local v0, "IsUpdateMode":Z
    const/4 v5, 0x0

    .line 518
    .local v5, "isNeedUpdateKidsMode":Z
    if-eqz v3, :cond_1

    .line 519
    const-string v6, "isUpdateMode"

    invoke-virtual {v3, v6, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 520
    const-string v6, "KidsPlatformStubBaseActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "update mode is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    iget-object v6, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iput-boolean v0, v6, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsUpdateMode:Z

    .line 522
    const-string v6, "isNeedUpdateKidsMode"

    invoke-virtual {v3, v6, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 523
    const-string v6, "KidsPlatformStubBaseActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "isNeedUpdateKidsMode : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    iget-object v6, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iput-boolean v5, v6, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsNeedUpdateKidsMode:Z

    .line 526
    :cond_1
    iget-object v6, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-boolean v6, v6, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsNeedUpdateKidsMode:Z

    if-eqz v6, :cond_2

    .line 528
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 529
    .local v2, "dialog":Landroid/app/AlertDialog$Builder;
    const v6, 0x7f0c0028

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 530
    const v6, 0x7f0c0029

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 531
    const v6, 0x7f0c002a

    new-instance v7, Lcom/sec/kidsplat/installer/BaseActivity$5;

    invoke-direct {v7, p0}, Lcom/sec/kidsplat/installer/BaseActivity$5;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v2, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 539
    const v6, 0x7f0c002b

    new-instance v7, Lcom/sec/kidsplat/installer/BaseActivity$6;

    invoke-direct {v7, p0}, Lcom/sec/kidsplat/installer/BaseActivity$6;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v2, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 561
    invoke-virtual {v2, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 562
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 565
    .end local v0    # "IsUpdateMode":Z
    .end local v2    # "dialog":Landroid/app/AlertDialog$Builder;
    .end local v3    # "extras":Landroid/os/Bundle;
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v5    # "isNeedUpdateKidsMode":Z
    :cond_2
    return-void
.end method

.method private isChinaModel()Z
    .locals 2

    .prologue
    .line 1293
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1294
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "CHZ"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHU"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CTC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isDeviceTablet(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1326
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v1, 0xf

    .line 1328
    .local v0, "layout":I
    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isFligtMode()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1165
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private isKidsHomeInstalled(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 1300
    const/4 v1, 0x0

    .line 1303
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.android.app.kidshome"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1308
    :goto_0
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    .line 1310
    :cond_0
    return v2

    .line 1304
    :catch_0
    move-exception v0

    .line 1305
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "KidsPlatformStubBaseActivity"

    const-string v4, "isKidsHomeInstalled : false"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isMobileDataOff()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1170
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1172
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1

    .line 1175
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static isNetworkConnected(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1124
    const/4 v0, 0x0

    .line 1125
    .local v0, "bIsConnected":Z
    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 1127
    .local v1, "manager":Landroid/net/ConnectivityManager;
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1128
    .local v2, "mobile":Landroid/net/NetworkInfo;
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 1130
    .local v3, "wifi":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1131
    :cond_1
    const/4 v0, 0x1

    .line 1135
    :goto_0
    return v0

    .line 1133
    :cond_2
    const-string v4, "KidsPlatformStubBaseActivity"

    const-string v5, "isNetworkConnected : network error"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isReachToDataLimit()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1186
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1187
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1

    .line 1189
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->isMobilePolicyDataEnable()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isRoamingOff()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1179
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1180
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_1

    .line 1182
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {p0}, Lcom/sec/kidsplat/installer/StubAppConfig;->getDataRoamingEnabled(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static putPref(Ljava/lang/String;Ljava/lang/Double;Landroid/content/Context;)V
    .locals 3
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/Double;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1207
    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1208
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1209
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p1}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1210
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1211
    return-void
.end method

.method public static putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1200
    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1201
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1202
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1203
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1204
    return-void
.end method

.method private sendBroadcastForNetworkErrorPopup(I)V
    .locals 6
    .param p1, "status"    # I

    .prologue
    const/4 v5, 0x0

    .line 1082
    const-string v2, "KidsPlatformStubBaseActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendBroadcastForNetworkErrorPopup : status ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1084
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/kidsplat/installer/BaseActivity;->mNeedUpdate:Z

    .line 1085
    sput-boolean v5, Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z

    .line 1087
    sget v2, Lcom/sec/kidsplat/installer/BaseActivity;->mCurrentStatus:I

    if-ne p1, v2, :cond_0

    .line 1101
    :goto_0
    return-void

    .line 1090
    :cond_0
    sput p1, Lcom/sec/kidsplat/installer/BaseActivity;->mCurrentStatus:I

    .line 1092
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1093
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.popupuireceiver"

    const-string v3, "com.sec.android.app.popupuireceiver.popupNetworkError"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1095
    const-string v2, "network_err_type"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1097
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, v1, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1098
    :catch_0
    move-exception v0

    .line 1099
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "KidsPlatformStubBaseActivity"

    const-string v3, "ActivityNotFoundException, No Activity found for - com.sec.android.app.popupuireceiver"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setInstallLayout(Z)V
    .locals 4
    .param p1, "bIsInstalling"    # Z

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 1195
    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mLayoutProgressing:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1196
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mLayoutInstall:Landroid/widget/RelativeLayout;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1197
    return-void

    :cond_0
    move v0, v2

    .line 1195
    goto :goto_0

    :cond_1
    move v2, v1

    .line 1196
    goto :goto_1
.end method

.method private setLayout()V
    .locals 11

    .prologue
    const v2, 0x7f0c000c

    const v10, 0x7f0c000d

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 588
    const v0, 0x7f040001

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->setContentView(I)V

    .line 589
    iput-object p0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    .line 590
    const v0, 0x7f0e0006

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mBtnUpdate:Landroid/widget/Button;

    .line 591
    const v0, 0x7f0e000d

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mBtnCancel:Landroid/widget/Button;

    .line 592
    const v0, 0x7f0e0008

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTvDownloading:Landroid/widget/TextView;

    .line 593
    const v0, 0x7f0e0005

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewTranslatorInformation:Landroid/widget/TextView;

    .line 594
    const v0, 0x7f0e0009

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;

    .line 595
    const v0, 0x7f0e000b

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewDownlodingSizeCurrent:Landroid/widget/TextView;

    .line 596
    const v0, 0x7f0e000c

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;

    .line 597
    const v0, 0x7f0e0007

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mLayoutProgressing:Landroid/widget/LinearLayout;

    .line 598
    const v0, 0x7f0e0004

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mLayoutInstall:Landroid/widget/RelativeLayout;

    .line 599
    const v0, 0x7f0e000a

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewForwardSlash:Landroid/widget/TextView;

    .line 603
    const v0, 0x7f0e000e

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mStubImageContainer:Landroid/widget/LinearLayout;

    .line 604
    const v0, 0x7f0e0003

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTopInformation:Landroid/widget/TextView;

    .line 607
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    const-string v1, "com.sec.kidsplat.phone"

    invoke-virtual {v0, v1}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->hasPackageInInstallList(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTopInformation:Landroid/widget/TextView;

    const v1, 0x7f0c0002

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 626
    :cond_0
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->setStubIamgeLayout()V

    .line 632
    const-string v0, "APK_SIZE"

    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 633
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mAppSize:D

    .line 634
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewTranslatorInformation:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%.1f"

    new-array v3, v9, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mAppSize:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v10}, Lcom/sec/kidsplat/installer/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 636
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewDownlodingSizeCurrent:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%.1f"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v10}, Lcom/sec/kidsplat/installer/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mAppSize:D

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    sget v3, Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I

    int-to-double v6, v3

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 637
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewForwardSlash:Landroid/widget/TextView;

    const-string v1, " / "

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 638
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%.1f"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v10}, Lcom/sec/kidsplat/installer/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mAppSize:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 639
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;

    sget v1, Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 646
    :goto_0
    sget-boolean v0, Lcom/sec/kidsplat/installer/BaseActivity;->isInstalling:Z

    if-eqz v0, :cond_1

    .line 647
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;

    sget-boolean v1, Lcom/sec/kidsplat/installer/BaseActivity;->isInstalling:Z

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 648
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mBtnCancel:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setEnabled(Z)V

    .line 649
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTvDownloading:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 652
    :cond_1
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-boolean v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsUpdateMode:Z

    if-eqz v0, :cond_2

    .line 653
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mBtnUpdate:Landroid/widget/Button;

    const v1, 0x7f0c0007

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 655
    :cond_2
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mBtnUpdate:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 656
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mBtnUpdate:Landroid/widget/Button;

    new-instance v1, Lcom/sec/kidsplat/installer/BaseActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/kidsplat/installer/BaseActivity$7;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 713
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mBtnCancel:Landroid/widget/Button;

    new-instance v1, Lcom/sec/kidsplat/installer/BaseActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/kidsplat/installer/BaseActivity$8;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 725
    return-void

    .line 641
    :cond_3
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTextViewTranslatorInformation:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 643
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;

    sget v1, Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 644
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    goto/16 :goto_0
.end method

.method private setStubIamgeLayout()V
    .locals 8

    .prologue
    .line 1227
    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1228
    .local v3, "res":Landroid/content/res/Resources;
    const v5, 0x7f0a0001

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1229
    .local v4, "rightMargine":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v5, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mStubImageCnt:I

    if-ge v0, v5, :cond_2

    .line 1230
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1232
    .local v1, "iv":Landroid/widget/ImageView;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, 0x0

    const/4 v6, -0x2

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v2, v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 1233
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v5, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mStubImageCnt:I

    add-int/lit8 v5, v5, -0x1

    if-ge v0, v5, :cond_0

    .line 1234
    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1236
    :cond_0
    sget-object v5, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1237
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 1238
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1244
    if-nez v0, :cond_1

    .line 1245
    const v5, 0x7f020003

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1248
    :goto_1
    iget-object v5, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mStubImageContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1229
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1247
    :cond_1
    const v5, 0x7f020004

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 1250
    .end local v1    # "iv":Landroid/widget/ImageView;
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    return-void
.end method

.method private showCollectUserInfoDialog()V
    .locals 7

    .prologue
    const v6, 0x102000b

    const/4 v5, 0x0

    .line 762
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUserInfoPopup:Landroid/app/Dialog;

    if-nez v2, :cond_1

    .line 763
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 764
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0c000b

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 765
    const-string v2, "ro.csc.country_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "KOREA"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 766
    new-instance v1, Landroid/text/SpannableString;

    const v2, 0x7f0c001c

    invoke-virtual {p0, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "account.samsung.com/membership/pp "

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 767
    .local v1, "s":Landroid/text/SpannableString;
    const/16 v2, 0xf

    invoke-static {v1, v2}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    .line 768
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 774
    .end local v1    # "s":Landroid/text/SpannableString;
    :goto_0
    const/high16 v2, 0x1040000

    new-instance v3, Lcom/sec/kidsplat/installer/BaseActivity$9;

    invoke-direct {v3, p0}, Lcom/sec/kidsplat/installer/BaseActivity$9;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 783
    const v2, 0x104000a

    new-instance v3, Lcom/sec/kidsplat/installer/BaseActivity$10;

    invoke-direct {v3, p0}, Lcom/sec/kidsplat/installer/BaseActivity$10;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 792
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 794
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUserInfoPopup:Landroid/app/Dialog;

    .line 795
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUserInfoPopup:Landroid/app/Dialog;

    invoke-virtual {v2, v6}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 800
    .end local v0    # "dialog":Landroid/app/AlertDialog$Builder;
    :goto_1
    return-void

    .line 771
    .restart local v0    # "dialog":Landroid/app/AlertDialog$Builder;
    :cond_0
    const v2, 0x7f0c001d

    invoke-virtual {p0, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 797
    .end local v0    # "dialog":Landroid/app/AlertDialog$Builder;
    :cond_1
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUserInfoPopup:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    .line 798
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUserInfoPopup:Landroid/app/Dialog;

    invoke-virtual {v2, v6}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto :goto_1
.end method

.method private showDataUsingDialog()V
    .locals 6

    .prologue
    .line 803
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 805
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    iget-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 807
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/high16 v4, 0x7f040000

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 809
    .local v3, "view":Landroid/view/View;
    const/high16 v4, 0x7f0e0000

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 811
    .local v2, "tvChargesWarning":Landroid/widget/TextView;
    const v4, 0x7f0e0001

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mCheckBoxDoNotShowAgain:Landroid/widget/CheckBox;

    .line 814
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 816
    iget-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/kidsplat/installer/BaseActivity;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 818
    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->isRoamingConnected()Z

    move-result v4

    if-nez v4, :cond_1

    .line 819
    const-string v4, "KidsPlatformStubBaseActivity"

    const-string v5, "usingMobileData"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 820
    const v4, 0x7f0c0015

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 821
    const v4, 0x7f0c0017

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 822
    const v4, 0x104000a

    new-instance v5, Lcom/sec/kidsplat/installer/BaseActivity$11;

    invoke-direct {v5, p0}, Lcom/sec/kidsplat/installer/BaseActivity$11;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 853
    :goto_0
    const/high16 v4, 0x1040000

    new-instance v5, Lcom/sec/kidsplat/installer/BaseActivity$13;

    invoke-direct {v5, p0}, Lcom/sec/kidsplat/installer/BaseActivity$13;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 862
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 863
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 865
    :cond_0
    return-void

    .line 835
    :cond_1
    const-string v4, "KidsPlatformStubBaseActivity"

    const-string v5, "usingRoamingData"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 836
    const v4, 0x7f0c0016

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 837
    const v4, 0x7f0c0018

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 838
    const v4, 0x7f0c0014

    new-instance v5, Lcom/sec/kidsplat/installer/BaseActivity$12;

    invoke-direct {v5, p0}, Lcom/sec/kidsplat/installer/BaseActivity$12;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private showDataUsingDialogForChina()V
    .locals 6

    .prologue
    .line 868
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 870
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    iget-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 872
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/high16 v4, 0x7f040000

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 874
    .local v3, "view":Landroid/view/View;
    const/high16 v4, 0x7f0e0000

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 876
    .local v2, "tvChargesWarning":Landroid/widget/TextView;
    const v4, 0x7f0e0001

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mCheckBoxDoNotShowAgain:Landroid/widget/CheckBox;

    .line 879
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 881
    iget-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mCheckBoxDoNotShowAgain:Landroid/widget/CheckBox;

    new-instance v5, Lcom/sec/kidsplat/installer/BaseActivity$14;

    invoke-direct {v5, p0}, Lcom/sec/kidsplat/installer/BaseActivity$14;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 889
    iget-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/kidsplat/installer/BaseActivity;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->isWifiNetworkConnected()Z

    move-result v4

    if-nez v4, :cond_1

    .line 890
    const v4, 0x7f0c001e

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 891
    const v4, 0x7f0c001f

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 898
    :cond_0
    :goto_0
    const/high16 v4, 0x1040000

    new-instance v5, Lcom/sec/kidsplat/installer/BaseActivity$15;

    invoke-direct {v5, p0}, Lcom/sec/kidsplat/installer/BaseActivity$15;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 908
    const v4, 0x104000a

    new-instance v5, Lcom/sec/kidsplat/installer/BaseActivity$16;

    invoke-direct {v5, p0}, Lcom/sec/kidsplat/installer/BaseActivity$16;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 920
    iget-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mCheckBoxDoNotShowAgain:Landroid/widget/CheckBox;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 921
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 922
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 923
    return-void

    .line 893
    :cond_1
    iget-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/kidsplat/installer/BaseActivity;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->isWifiNetworkConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 894
    const v4, 0x7f0c0020

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 895
    const v4, 0x7f0c0021

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private showErrorDialog(I)V
    .locals 4
    .param p1, "stringId"    # I

    .prologue
    .line 418
    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 447
    :goto_0
    return-void

    .line 421
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 422
    .local v0, "build":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0c001b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/sec/kidsplat/installer/BaseActivity$3;

    invoke-direct {v3, p0}, Lcom/sec/kidsplat/installer/BaseActivity$3;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/kidsplat/installer/BaseActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/kidsplat/installer/BaseActivity$2;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 438
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mShowErrDialog:Landroid/app/AlertDialog;

    .line 439
    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mShowErrDialog:Landroid/app/AlertDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 440
    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mShowErrDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 441
    new-instance v1, Lcom/sec/kidsplat/installer/BaseActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/kidsplat/installer/BaseActivity$4;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {p0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private showUpdateDialog()V
    .locals 5

    .prologue
    .line 1254
    const-string v3, "KidsPlatformStubBaseActivity"

    const-string v4, "showUpdateDialog()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1255
    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-object v3, v3, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 1256
    .local v1, "needUpdate":Z
    if-eqz v1, :cond_1

    const v2, 0x7f0c0004

    .line 1257
    .local v2, "resMsg":I
    :goto_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1258
    .local v0, "mAlertDialog":Landroid/app/AlertDialog$Builder;
    const/high16 v3, 0x7f0c0000

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1259
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1261
    new-instance v3, Lcom/sec/kidsplat/installer/BaseActivity$18;

    invoke-direct {v3, p0}, Lcom/sec/kidsplat/installer/BaseActivity$18;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 1269
    const v3, 0x104000a

    new-instance v4, Lcom/sec/kidsplat/installer/BaseActivity$19;

    invoke-direct {v4, p0, v1}, Lcom/sec/kidsplat/installer/BaseActivity$19;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;Z)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1280
    if-eqz v1, :cond_0

    .line 1281
    const v3, 0x7f0c0006

    new-instance v4, Lcom/sec/kidsplat/installer/BaseActivity$20;

    invoke-direct {v4, p0}, Lcom/sec/kidsplat/installer/BaseActivity$20;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1289
    :cond_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateDialog:Landroid/app/AlertDialog;

    .line 1290
    return-void

    .line 1256
    .end local v0    # "mAlertDialog":Landroid/app/AlertDialog$Builder;
    .end local v2    # "resMsg":I
    :cond_1
    const v2, 0x7f0c0005

    goto :goto_0
.end method

.method private startUpdateThread()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1046
    invoke-static {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1047
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

    if-nez v0, :cond_0

    .line 1048
    sput-boolean v1, Lcom/sec/kidsplat/installer/BaseActivity;->isInstalling:Z

    .line 1050
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->RunUpdateCheckProgress()V

    .line 1051
    new-instance v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;

    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/kidsplat/installer/UpdateCheckThread;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/sec/kidsplat/installer/StubAppConfig$Config;)V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

    .line 1052
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->start()V

    .line 1058
    :cond_0
    :goto_0
    return-void

    .line 1055
    :cond_1
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->getNetworkErrorState()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->sendBroadcastForNetworkErrorPopup(I)V

    .line 1056
    invoke-direct {p0, v1}, Lcom/sec/kidsplat/installer/BaseActivity;->setInstallLayout(Z)V

    goto :goto_0
.end method


# virtual methods
.method protected isRoamingConnected()Z
    .locals 5

    .prologue
    .line 1153
    const/4 v0, 0x0

    .line 1154
    .local v0, "bIsConnected":Z
    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 1156
    .local v1, "manager":Landroid/net/ConnectivityManager;
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1158
    .local v2, "mobile":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1159
    const/4 v0, 0x1

    .line 1161
    :cond_0
    return v0
.end method

.method protected isWifiNetworkConnected()Z
    .locals 5

    .prologue
    .line 1139
    const/4 v2, 0x0

    .line 1141
    .local v2, "ret":Z
    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1144
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1145
    .local v1, "info":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1146
    const/4 v2, 0x1

    .line 1149
    :cond_0
    return v2
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, -0x1

    .line 1107
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1108
    const-string v0, "KidsPlatformStubBaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult : requestCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resultCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1110
    if-nez p1, :cond_0

    .line 1111
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.popupuireceiver.popupNetworkError"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "retVal"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    if-ne p2, v3, :cond_1

    .line 1113
    const-string v0, "KidsPlatformStubBaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult : ClassName : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1114
    sput v3, Lcom/sec/kidsplat/installer/BaseActivity;->mCurrentStatus:I

    .line 1115
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->startUpdateThread()V

    .line 1121
    :cond_0
    :goto_0
    return-void

    .line 1119
    :cond_1
    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->finish()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1220
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1224
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 453
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 455
    invoke-static {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->isDeviceTablet(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 456
    invoke-virtual {p0, v4}, Lcom/sec/kidsplat/installer/BaseActivity;->setRequestedOrientation(I)V

    .line 460
    :goto_0
    iput-object p0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    .line 461
    sget-object v2, Lcom/sec/kidsplat/installer/BaseActivity;->mDataID:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 462
    sget-object v2, Lcom/sec/kidsplat/installer/BaseActivity;->mDataResult:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 463
    sget-object v2, Lcom/sec/kidsplat/installer/BaseActivity;->mDataUri:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 464
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->init()V

    .line 465
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->setLayout()V

    .line 467
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-boolean v2, v2, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsNeedUpdateKidsMode:Z

    if-eq v2, v5, :cond_0

    .line 468
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->isChinaModel()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 469
    const-string v2, "IS_CHINA_USING_DIALOG_SHOW"

    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 470
    .local v1, "prefchina":Ljava/lang/String;
    const-string v2, "true"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 471
    iput-boolean v5, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isChinaUsingPopupDone:Z

    .line 475
    :goto_1
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isChinaUsingPopupDone:Z

    if-nez v2, :cond_4

    .line 476
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->showDataUsingDialogForChina()V

    .line 485
    .end local v1    # "prefchina":Ljava/lang/String;
    :cond_0
    :goto_2
    const-string v2, "IS_USER_INFO_DIALOG_SHOW"

    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 487
    .local v0, "pref":Ljava/lang/String;
    if-eqz v0, :cond_7

    .line 488
    const-string v2, "true"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 489
    iput-boolean v5, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isUserInfoPopupDone:Z

    .line 495
    :goto_3
    iget-boolean v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isUserInfoPopupDone:Z

    if-nez v2, :cond_1

    .line 496
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->showCollectUserInfoDialog()V

    .line 497
    :cond_1
    return-void

    .line 458
    .end local v0    # "pref":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0, v5}, Lcom/sec/kidsplat/installer/BaseActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 473
    .restart local v1    # "prefchina":Ljava/lang/String;
    :cond_3
    iput-boolean v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isChinaUsingPopupDone:Z

    goto :goto_1

    .line 478
    :cond_4
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->startUpdateThread()V

    goto :goto_2

    .line 480
    .end local v1    # "prefchina":Ljava/lang/String;
    :cond_5
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->startUpdateThread()V

    goto :goto_2

    .line 491
    .restart local v0    # "pref":Ljava/lang/String;
    :cond_6
    iput-boolean v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isUserInfoPopupDone:Z

    goto :goto_3

    .line 493
    :cond_7
    iput-boolean v4, p0, Lcom/sec/kidsplat/installer/BaseActivity;->isUserInfoPopupDone:Z

    goto :goto_3
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 967
    const-string v0, "KidsPlatformStubBaseActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 969
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 970
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 971
    :cond_0
    sput-boolean v2, Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z

    .line 972
    sput-boolean v2, Lcom/sec/kidsplat/installer/BaseActivity;->isInstalling:Z

    .line 973
    sput v2, Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I

    .line 975
    const/4 v0, -0x1

    sput v0, Lcom/sec/kidsplat/installer/BaseActivity;->mCurrentStatus:I

    .line 976
    sget-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataID:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 977
    sget-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataResult:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 978
    sget-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataUri:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 979
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

    if-eqz v0, :cond_1

    .line 980
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->interrupt()V

    .line 981
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->cancel()V

    .line 982
    iput-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

    .line 985
    :cond_1
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    if-eqz v0, :cond_2

    .line 986
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->cancel()V

    .line 987
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->cancel(Z)Z

    .line 988
    iput-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    .line 991
    :cond_2
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_3

    .line 992
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 993
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 994
    iput-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;

    .line 997
    :cond_3
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->DisableActivity()V

    .line 999
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1000
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 960
    const-string v0, "KidsPlatformStubBaseActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 961
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 962
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 940
    const-string v0, "KidsPlatformStubBaseActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 941
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 942
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/kidsplat/installer/BaseActivity;->isHomeKeyPressed:Z

    .line 943
    sget-boolean v0, Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z

    invoke-direct {p0, v0}, Lcom/sec/kidsplat/installer/BaseActivity;->setInstallLayout(Z)V

    .line 944
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;

    sget v1, Lcom/sec/kidsplat/installer/BaseActivity;->ratio:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 945
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 952
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 953
    const-string v0, "KidsPlatformStubBaseActivity"

    const-string v1, "onUserLeaveHint : isHomeKeyPressed true"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/kidsplat/installer/BaseActivity;->isHomeKeyPressed:Z

    .line 955
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->DisableActivity()V

    .line 957
    return-void
.end method

.method startDownload()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 728
    sget-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataID:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataResult:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataUri:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 759
    :cond_0
    :goto_0
    return-void

    .line 731
    :cond_1
    sput-boolean v11, Lcom/sec/kidsplat/installer/BaseActivity;->isDownloading:Z

    .line 732
    invoke-direct {p0, v11}, Lcom/sec/kidsplat/installer/BaseActivity;->setInstallLayout(Z)V

    .line 738
    sget-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataID:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataResult:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/kidsplat/installer/BaseActivity;->mDataUri:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/sec/kidsplat/installer/BaseActivity;->mNeedUpdate:Z

    if-nez v0, :cond_3

    .line 740
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

    if-eqz v0, :cond_2

    .line 741
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->cancel()V

    .line 742
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->interrupt()V

    .line 743
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mThread:Lcom/sec/kidsplat/installer/UpdateCheckThread;

    .line 746
    :cond_2
    new-instance v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mHandler:Landroid/os/Handler;

    sget-object v3, Lcom/sec/kidsplat/installer/BaseActivity;->mDataID:Ljava/util/ArrayList;

    sget-object v4, Lcom/sec/kidsplat/installer/BaseActivity;->mDataResult:Ljava/util/ArrayList;

    sget-object v5, Lcom/sec/kidsplat/installer/BaseActivity;->mDataUri:Ljava/util/ArrayList;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    sget-wide v8, Lcom/sec/kidsplat/installer/BaseActivity;->apkSize:D

    invoke-direct/range {v0 .. v9}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Boolean;Lcom/sec/kidsplat/installer/StubAppConfig$Config;D)V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    .line 748
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mDownloadTask:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    new-array v1, v11, [Ljava/lang/String;

    const-string v2, "Download"

    aput-object v2, v1, v10

    invoke-virtual {v0, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 749
    iput-boolean v10, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mStartFromFirst:Z

    .line 751
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 752
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;

    .line 753
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/kidsplat/installer/BaseActivity$ProgressTimeTask;

    invoke-direct {v1, p0}, Lcom/sec/kidsplat/installer/BaseActivity$ProgressTimeTask;-><init>(Lcom/sec/kidsplat/installer/BaseActivity;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto/16 :goto_0

    .line 756
    :cond_3
    sget-boolean v0, Lcom/sec/kidsplat/installer/BaseActivity;->mNeedUpdate:Z

    if-eqz v0, :cond_0

    .line 757
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/BaseActivity;->startUpdateThread()V

    goto/16 :goto_0
.end method
