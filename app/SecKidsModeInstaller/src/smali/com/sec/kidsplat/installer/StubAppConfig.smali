.class Lcom/sec/kidsplat/installer/StubAppConfig;
.super Ljava/lang/Object;
.source "StubAppConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/kidsplat/installer/StubAppConfig$Config;
    }
.end annotation


# static fields
.field public static final ROBOTO_BLACK:I = 0x5

.field public static final ROBOTO_BOLD:I = 0x4

.field public static final ROBOTO_LIGHT:I = 0x1

.field public static final ROBOTO_LIGHT_DEFAULT:I = 0x6

.field public static final ROBOTO_MEDIUM:I = 0x3

.field public static final ROBOTO_REGULAR:I = 0x2

.field public static final ROBOTO_THIN:I = 0x0

.field private static final STR_UNKNOWN_TYPEFACE:Ljava/lang/String; = "Unknown `typeface` attribute value "

.field private static final TAG:Ljava/lang/String; = "KidsPlatformStub"

.field private static final TAG_ATTR_NAME:Ljava/lang/String; = "name"

.field private static final TAG_ELEMENT_APPINFOIMAGE_CNT:Ljava/lang/String; = "AppInfoImageCnt"

.field private static final TAG_ELEMENT_CHECK_APPSIZE_URL:Ljava/lang/String; = "CheckAppSizeUrl"

.field private static final TAG_ELEMENT_CHECK_CTC_URL:Ljava/lang/String; = "CheckCtcUrl"

.field private static final TAG_ELEMENT_DOWNLOAD_APP_URL:Ljava/lang/String; = "DownloadAppUrl"

.field private static final TAG_ELEMENT_STUBAPK:Ljava/lang/String; = "StubApk"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    return-void
.end method

.method public static getDataRoamingEnabled(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 373
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "data_roaming"

    invoke-static {v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-lez v2, :cond_0

    const/4 v1, 0x1

    .line 377
    :cond_0
    :goto_0
    return v1

    .line 375
    :catch_0
    move-exception v0

    .line 376
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v2, "KidsPlatformStub"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDataRoamingEnabled(context) : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static hasPackage(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "c"    # Landroid/content/Context;
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 238
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 239
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x1

    .line 241
    .local v1, "hasPkg":Z
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 246
    :goto_0
    return v1

    .line 242
    :catch_0
    move-exception v0

    .line 243
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    .line 244
    const-string v3, "KidsPlatformStub"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Package not found : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isVoiceCapable(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 335
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 337
    .local v0, "telephony":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static readSalesCode()Ljava/lang/String;
    .locals 4

    .prologue
    .line 254
    const-string v1, ""

    .line 256
    .local v1, "sales_code":Ljava/lang/String;
    :try_start_0
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 257
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 258
    const-string v2, "ril.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 263
    :cond_0
    :goto_0
    return-object v1

    .line 260
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "KidsPlatformStub"

    const-string v3, "readSalesCode failed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setFont(Landroid/widget/TextView;I)V
    .locals 4
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "typefaceValue"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 342
    if-nez p0, :cond_0

    .line 369
    :goto_0
    return-void

    .line 346
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 366
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown `typeface` attribute value "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 348
    :pswitch_1
    const-string v1, "sans-serif-thin"

    invoke-static {v1, v2}, Lcom/sec/kidsplat/installer/FontCache;->get(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 368
    .local v0, "typeface":Landroid/graphics/Typeface;
    :goto_1
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 351
    .end local v0    # "typeface":Landroid/graphics/Typeface;
    :pswitch_2
    const-string v1, "sec-roboto-light"

    invoke-static {v1, v2}, Lcom/sec/kidsplat/installer/FontCache;->get(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 352
    .restart local v0    # "typeface":Landroid/graphics/Typeface;
    goto :goto_1

    .line 354
    .end local v0    # "typeface":Landroid/graphics/Typeface;
    :pswitch_3
    const-string v1, "sans-serif"

    invoke-static {v1, v2}, Lcom/sec/kidsplat/installer/FontCache;->get(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 355
    .restart local v0    # "typeface":Landroid/graphics/Typeface;
    goto :goto_1

    .line 357
    .end local v0    # "typeface":Landroid/graphics/Typeface;
    :pswitch_4
    const-string v1, "sec-roboto-light"

    invoke-static {v1, v3}, Lcom/sec/kidsplat/installer/FontCache;->get(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 358
    .restart local v0    # "typeface":Landroid/graphics/Typeface;
    goto :goto_1

    .line 360
    .end local v0    # "typeface":Landroid/graphics/Typeface;
    :pswitch_5
    const-string v1, "sans-serif"

    invoke-static {v1, v3}, Lcom/sec/kidsplat/installer/FontCache;->get(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 361
    .restart local v0    # "typeface":Landroid/graphics/Typeface;
    goto :goto_1

    .line 363
    .end local v0    # "typeface":Landroid/graphics/Typeface;
    :pswitch_6
    const-string v1, "roboto-light"

    invoke-static {v1, v2}, Lcom/sec/kidsplat/installer/FontCache;->get(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 364
    .restart local v0    # "typeface":Landroid/graphics/Typeface;
    goto :goto_1

    .line 346
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public getStubAppConfig(Landroid/content/Context;)Lcom/sec/kidsplat/installer/StubAppConfig$Config;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 267
    new-instance v2, Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-direct {v2, p0}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;-><init>(Lcom/sec/kidsplat/installer/StubAppConfig;)V

    .line 268
    .local v2, "config":Lcom/sec/kidsplat/installer/StubAppConfig$Config;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const/high16 v10, 0x7f060000

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v8

    .line 269
    .local v8, "parser":Landroid/content/res/XmlResourceParser;
    invoke-virtual {v2, p1}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->setPackageNameList(Landroid/content/Context;)V

    .line 273
    const/4 v4, 0x0

    .line 275
    .local v4, "currentTag":Ljava/lang/String;
    :cond_0
    :try_start_0
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->next()I

    .line 276
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v6

    .line 277
    .local v6, "eventType":I
    packed-switch v6, :pswitch_data_0

    .line 323
    :cond_1
    :goto_0
    :pswitch_0
    const/4 v9, 0x1

    if-ne v6, v9, :cond_0

    .line 329
    .end local v6    # "eventType":I
    :goto_1
    return-object v2

    .line 279
    .restart local v6    # "eventType":I
    :pswitch_1
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 280
    const-string v9, "StubApk"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 281
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getAttributeCount()I

    move-result v3

    .line 282
    .local v3, "count":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    if-ge v7, v3, :cond_1

    .line 283
    invoke-interface {v8, v7}, Landroid/content/res/XmlResourceParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 284
    .local v0, "attrKey":Ljava/lang/String;
    invoke-interface {v8, v7}, Landroid/content/res/XmlResourceParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v1

    .line 286
    .local v1, "attrValue":Ljava/lang/String;
    const-string v9, "name"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 287
    invoke-virtual {v2, v1}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->setAppName(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 324
    .end local v0    # "attrKey":Ljava/lang/String;
    .end local v1    # "attrValue":Ljava/lang/String;
    .end local v3    # "count":I
    .end local v6    # "eventType":I
    .end local v7    # "i":I
    :catch_0
    move-exception v5

    .line 325
    .local v5, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v5}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_1

    .line 282
    .end local v5    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v0    # "attrKey":Ljava/lang/String;
    .restart local v1    # "attrValue":Ljava/lang/String;
    .restart local v3    # "count":I
    .restart local v6    # "eventType":I
    .restart local v7    # "i":I
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 296
    .end local v0    # "attrKey":Ljava/lang/String;
    .end local v1    # "attrValue":Ljava/lang/String;
    .end local v3    # "count":I
    .end local v7    # "i":I
    :pswitch_2
    :try_start_1
    const-string v9, "CheckAppSizeUrl"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 297
    if-eqz v2, :cond_1

    .line 298
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->setCheckSizeAppUrl(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 326
    .end local v6    # "eventType":I
    :catch_1
    move-exception v5

    .line 327
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 300
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v6    # "eventType":I
    :cond_3
    :try_start_2
    const-string v9, "DownloadAppUrl"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 301
    if-eqz v2, :cond_1

    .line 302
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->setDownloadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 304
    :cond_4
    const-string v9, "CheckCtcUrl"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 305
    if-eqz v2, :cond_1

    .line 306
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->setCheckCtcUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 308
    :cond_5
    const-string v9, "AppInfoImageCnt"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 309
    if-eqz v2, :cond_1

    .line 310
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getText()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v2, v9}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->setAppInfoImageCnt(I)V
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 277
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
