.class Lcom/sec/kidsplat/installer/BaseActivity$19;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/kidsplat/installer/BaseActivity;->showUpdateDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/kidsplat/installer/BaseActivity;

.field final synthetic val$needUpdate:Z


# direct methods
.method constructor <init>(Lcom/sec/kidsplat/installer/BaseActivity;Z)V
    .locals 0

    .prologue
    .line 1269
    iput-object p1, p0, Lcom/sec/kidsplat/installer/BaseActivity$19;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    iput-boolean p2, p0, Lcom/sec/kidsplat/installer/BaseActivity$19;->val$needUpdate:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 1272
    iget-boolean v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$19;->val$needUpdate:Z

    if-nez v0, :cond_0

    .line 1273
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$19;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->finish()V

    .line 1278
    :goto_0
    return-void

    .line 1276
    :cond_0
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$19;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mBtnUpdate:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2500(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->performClick()Z

    goto :goto_0
.end method
