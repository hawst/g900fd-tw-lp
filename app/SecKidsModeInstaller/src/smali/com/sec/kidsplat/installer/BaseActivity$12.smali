.class Lcom/sec/kidsplat/installer/BaseActivity$12;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/kidsplat/installer/BaseActivity;->showDataUsingDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/kidsplat/installer/BaseActivity;


# direct methods
.method constructor <init>(Lcom/sec/kidsplat/installer/BaseActivity;)V
    .locals 0

    .prologue
    .line 839
    iput-object p1, p0, Lcom/sec/kidsplat/installer/BaseActivity$12;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 842
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$12;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mCheckBoxDoNotShowAgain:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4700(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 843
    const-string v0, "IS_ROAMING_USING_DIALOG_SHOW"

    const-string v1, "true"

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$12;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 845
    const-string v0, "KidsPlatformStubBaseActivity"

    const-string v1, "putPref: Roaming"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 848
    iget-object v0, p0, Lcom/sec/kidsplat/installer/BaseActivity$12;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-virtual {v0}, Lcom/sec/kidsplat/installer/BaseActivity;->startDownload()V

    .line 849
    return-void
.end method
