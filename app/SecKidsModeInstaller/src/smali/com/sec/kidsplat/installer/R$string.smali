.class public final Lcom/sec/kidsplat/installer/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/kidsplat/installer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final additional_charges_msg:I = 0x7f0c0017

.field public static final alert:I = 0x7f0c001b

.field public static final base_app_name:I = 0x7f0c0000

.field public static final base_footer_information:I = 0x7f0c0003

.field public static final base_top_information:I = 0x7f0c0001

.field public static final base_top_information_nocall:I = 0x7f0c0002

.field public static final calculating:I = 0x7f0c0010

.field public static final cancel:I = 0x7f0c0009

.field public static final cancel_button_kids_mode:I = 0x7f0c002a

.field public static final checking_for_updates:I = 0x7f0c0012

.field public static final china_data_connection_body:I = 0x7f0c001f

.field public static final china_data_connection_title:I = 0x7f0c001e

.field public static final china_wifi_connection_body:I = 0x7f0c0021

.field public static final china_wifi_connection_title:I = 0x7f0c0020

.field public static final connect:I = 0x7f0c0014

.field public static final connect_via_mobile_networks:I = 0x7f0c0015

.field public static final connect_via_roaming_network:I = 0x7f0c0016

.field public static final connecting_to_server:I = 0x7f0c0011

.field public static final do_not_show_again:I = 0x7f0c0019

.field public static final downloading:I = 0x7f0c000e

.field public static final install:I = 0x7f0c000a

.field public static final installation:I = 0x7f0c000b

.field public static final installing:I = 0x7f0c000f

.field public static final later:I = 0x7f0c0006

.field public static final launch:I = 0x7f0c0008

.field public static final mb:I = 0x7f0c000d

.field public static final message_mobile_data_is_disabled:I = 0x7f0c0013

.field public static final message_roaming_extra_charges:I = 0x7f0c0018

.field public static final message_update_kids_mode:I = 0x7f0c0029

.field public static final paging1_text:I = 0x7f0c0022

.field public static final paging2_text:I = 0x7f0c0023

.field public static final paging3_text:I = 0x7f0c0024

.field public static final paging4_text:I = 0x7f0c0025

.field public static final setup:I = 0x7f0c0026

.field public static final setup_button_text:I = 0x7f0c0027

.field public static final size:I = 0x7f0c000c

.field public static final unavailable_update:I = 0x7f0c001a

.field public static final update:I = 0x7f0c0007

.field public static final update_available_text:I = 0x7f0c0004

.field public static final update_button_kids_mode:I = 0x7f0c002b

.field public static final update_kids_mode:I = 0x7f0c0028

.field public static final update_unavailable_text:I = 0x7f0c0005

.field public static final userinformation_pop_up_msg:I = 0x7f0c001c

.field public static final userinformation_pop_up_msg_kor:I = 0x7f0c001d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
