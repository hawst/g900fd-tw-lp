.class public Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;
.super Landroid/app/Service;
.source "KidsModeUpdateService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer_intent;,
        Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer;
    }
.end annotation


# static fields
.field private static final ACTION_KIDS_MODE_CHANGE:Ljava/lang/String; = "com.android.launcher.action.KIDS_HOME_MODE_CHANGE"

.field private static final KIDS_MODE_CHANGE_PERMISSION:Ljava/lang/String; = "com.sec.kidsplat.parentalcontrol.broadcast.KIDS_HOME_MODE_CHANGE_PERMISSION"

.field private static final TAG:Ljava/lang/String; = "KidsModeUpdateService "


# instance fields
.field private check:Z

.field private final delay_time:I

.field private final intent_delay_time:I

.field private intent_timer:Ljava/util/Timer;

.field private isKidsMode:Z

.field private mSettingContentObserver:Landroid/database/ContentObserver;

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->check:Z

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->isKidsMode:Z

    .line 28
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->delay_time:I

    .line 30
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->intent_delay_time:I

    .line 121
    return-void
.end method

.method static synthetic access$100(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->isKidsMode:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;
    .param p1, "x1"    # Z

    .prologue
    .line 21
    iput-boolean p1, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->isKidsMode:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->timer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->intent_timer:Ljava/util/Timer;

    return-object v0
.end method


# virtual methods
.method public exitKidsMode(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    const-string v1, "KidsModeUpdateService "

    const-string v2, "exitkidsmode() "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.launcher.action.KIDS_HOME_MODE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 93
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "kidsmode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 95
    const-string v1, "kidsmode_from"

    const-string v2, "KidsHome"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 98
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 84
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 37
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 38
    const-string v0, "KidsModeUpdateService "

    const-string v1, "kidsmode_service_oncreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->timer:Ljava/util/Timer;

    .line 41
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->intent_timer:Ljava/util/Timer;

    .line 42
    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "kids_home_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v6, :cond_0

    .line 43
    const-string v0, "KidsModeUpdateService "

    const-string v1, "kid_home_mode is false but start kidshome "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    iget-object v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer;-><init>(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1f4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 46
    :cond_0
    new-instance v0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;-><init>(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->mSettingContentObserver:Landroid/database/ContentObserver;

    .line 64
    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "kids_home_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->mSettingContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v6, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 69
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 103
    const-string v0, "KidsModeUpdateService "

    const-string v1, "kidsmode_service_ondestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->timer:Ljava/util/Timer;

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->stopTimer(Ljava/util/Timer;)V

    .line 105
    iget-object v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->intent_timer:Ljava/util/Timer;

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->stopTimer(Ljava/util/Timer;)V

    .line 106
    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->mSettingContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 107
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 108
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 76
    const-string v0, "KidsModeUpdateService "

    const-string v1, "kidsmode_service_onStartCommand"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method public stopTimer(Ljava/util/Timer;)V
    .locals 2
    .param p1, "t"    # Ljava/util/Timer;

    .prologue
    .line 143
    const-string v0, "KidsModeUpdateService "

    const-string v1, "kidsmode_service_stopTimer()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    if-eqz p1, :cond_0

    .line 145
    const-string v0, "KidsModeUpdateService "

    const-string v1, "kidsmode_service_timer cancel"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-virtual {p1}, Ljava/util/Timer;->cancel()V

    .line 147
    invoke-virtual {p1}, Ljava/util/Timer;->purge()I

    .line 148
    const/4 p1, 0x0

    .line 150
    :cond_0
    return-void
.end method
