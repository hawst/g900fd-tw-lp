.class Lcom/sec/kidsplat/installer/BaseActivity$7;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/kidsplat/installer/BaseActivity;->setLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/kidsplat/installer/BaseActivity;


# direct methods
.method constructor <init>(Lcom/sec/kidsplat/installer/BaseActivity;)V
    .locals 0

    .prologue
    .line 656
    iput-object p1, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 660
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mBtnUpdate:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$2500(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-virtual {v3}, Lcom/sec/kidsplat/installer/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0008

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 661
    const-string v2, "KidsPlatformStubBaseActivity"

    const-string v3, "click launch button"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 663
    .local v1, "startIntent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.kidshome"

    const-string v3, "com.sec.android.app.kidshome.KidsHomeStartActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 664
    const v2, 0x10808000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 666
    :try_start_0
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 710
    .end local v1    # "startIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 667
    .restart local v1    # "startIntent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 668
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "KidsPlatformStubBaseActivity"

    const-string v3, "ActivityNotFoundException, No Activity found for - com.sec.android.app.kidshome.KidsHomeStartActivity"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 673
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "startIntent":Landroid/content/Intent;
    :cond_0
    const-string v2, "VERSION_CODE"

    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 674
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->versionCode:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3700(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "VERSION_CODE"

    iget-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/kidsplat/installer/BaseActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-eq v2, v3, :cond_2

    .line 675
    const-string v2, "VERSION_CODE"

    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->versionCode:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3700(Lcom/sec/kidsplat/installer/BaseActivity;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/kidsplat/installer/BaseActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 676
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mStartFromFirst:Z
    invoke-static {v2, v6}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1302(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    .line 681
    :cond_1
    :goto_1
    const-string v2, "IS_DATA_USING_DIALOG_SHOW"

    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 683
    const-string v2, "IS_DATA_USING_DIALOG_SHOW"

    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 684
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isDataUsingPopupDone:Z
    invoke-static {v2, v6}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4302(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    .line 691
    :goto_2
    const-string v2, "IS_ROAMING_USING_DIALOG_SHOW"

    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 692
    const-string v2, "IS_ROAMING_USING_DIALOG_SHOW"

    iget-object v3, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 693
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isRoamingUsingPopupDone:Z
    invoke-static {v2, v6}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4402(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    .line 700
    :goto_3
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v2

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->isNetworkConnected(Landroid/content/Context;)Z
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$900(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-virtual {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->isWifiNetworkConnected()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-virtual {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->isRoamingConnected()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->isRoamingUsingPopupDone:Z
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4400(Lcom/sec/kidsplat/installer/BaseActivity;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->isChinaModel()Z
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3900(Lcom/sec/kidsplat/installer/BaseActivity;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 702
    const-string v2, "KidsPlatformStubBaseActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isDataUsingPopupDone : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->isDataUsingPopupDone:Z
    invoke-static {v4}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4300(Lcom/sec/kidsplat/installer/BaseActivity;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    const-string v2, "KidsPlatformStubBaseActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isRoamingUsingPopupDone : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->isRoamingUsingPopupDone:Z
    invoke-static {v4}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4400(Lcom/sec/kidsplat/installer/BaseActivity;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->showDataUsingDialog()V
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4500(Lcom/sec/kidsplat/installer/BaseActivity;)V

    goto/16 :goto_0

    .line 678
    :cond_2
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->mStartFromFirst:Z
    invoke-static {v2, v5}, Lcom/sec/kidsplat/installer/BaseActivity;->access$1302(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    goto/16 :goto_1

    .line 686
    :cond_3
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isDataUsingPopupDone:Z
    invoke-static {v2, v5}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4302(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    goto/16 :goto_2

    .line 688
    :cond_4
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isDataUsingPopupDone:Z
    invoke-static {v2, v5}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4302(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    goto/16 :goto_2

    .line 695
    :cond_5
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isRoamingUsingPopupDone:Z
    invoke-static {v2, v5}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4402(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    goto/16 :goto_3

    .line 697
    :cond_6
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isRoamingUsingPopupDone:Z
    invoke-static {v2, v5}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4402(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    goto/16 :goto_3

    .line 705
    :cond_7
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v2

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->isNetworkConnected(Landroid/content/Context;)Z
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$900(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-virtual {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->isWifiNetworkConnected()Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-virtual {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->isRoamingConnected()Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->isDataUsingPopupDone:Z
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4300(Lcom/sec/kidsplat/installer/BaseActivity;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->isChinaModel()Z
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3900(Lcom/sec/kidsplat/installer/BaseActivity;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 707
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->showDataUsingDialog()V
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4500(Lcom/sec/kidsplat/installer/BaseActivity;)V

    goto/16 :goto_0

    .line 709
    :cond_8
    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$7;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-virtual {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->startDownload()V

    goto/16 :goto_0
.end method
