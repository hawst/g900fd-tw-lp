.class Lcom/sec/kidsplat/installer/ApplicationManager$PackageInstallObserver;
.super Landroid/content/pm/IPackageInstallObserver$Stub;
.source "ApplicationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/kidsplat/installer/ApplicationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageInstallObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/kidsplat/installer/ApplicationManager;


# direct methods
.method constructor <init>(Lcom/sec/kidsplat/installer/ApplicationManager;)V
    .locals 0

    .prologue
    .line 400
    iput-object p1, p0, Lcom/sec/kidsplat/installer/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/kidsplat/installer/ApplicationManager;

    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 404
    iget-object v1, p0, Lcom/sec/kidsplat/installer/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/kidsplat/installer/ApplicationManager;

    # getter for: Lcom/sec/kidsplat/installer/ApplicationManager;->onInstalledPackaged:Lcom/sec/kidsplat/installer/OnInstalledPackaged;
    invoke-static {v1}, Lcom/sec/kidsplat/installer/ApplicationManager;->access$000(Lcom/sec/kidsplat/installer/ApplicationManager;)Lcom/sec/kidsplat/installer/OnInstalledPackaged;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 408
    const/4 v1, 0x1

    if-eq p2, v1, :cond_0

    .line 418
    :cond_0
    iget-object v1, p0, Lcom/sec/kidsplat/installer/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/kidsplat/installer/ApplicationManager;

    iput-object p1, v1, Lcom/sec/kidsplat/installer/ApplicationManager;->pkgname:Ljava/lang/String;

    .line 420
    iget-object v1, p0, Lcom/sec/kidsplat/installer/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/kidsplat/installer/ApplicationManager;

    iput p2, v1, Lcom/sec/kidsplat/installer/ApplicationManager;->returncode:I

    .line 422
    iget-object v1, p0, Lcom/sec/kidsplat/installer/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/kidsplat/installer/ApplicationManager;

    # getter for: Lcom/sec/kidsplat/installer/ApplicationManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/kidsplat/installer/ApplicationManager;->access$100(Lcom/sec/kidsplat/installer/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 424
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/kidsplat/installer/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/kidsplat/installer/ApplicationManager;

    # getter for: Lcom/sec/kidsplat/installer/ApplicationManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/kidsplat/installer/ApplicationManager;->access$100(Lcom/sec/kidsplat/installer/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 428
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    return-void
.end method
