.class Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer_intent;
.super Ljava/util/TimerTask;
.source "KidsModeUpdateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyTimer_intent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;


# direct methods
.method private constructor <init>(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer_intent;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;
    .param p2, "x1"    # Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer_intent;-><init>(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 125
    const-string v1, "KidsModeUpdateService "

    const-string v2, "kidsmode_service_intent_timer run"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 128
    .local v0, "startIntent":Landroid/content/Intent;
    const-string v1, "com.sec.kidsplat.installer"

    const-string v2, "com.sec.kidsplat.installer.BaseActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 130
    const-string v1, "KidsModeUpdateService "

    const-string v2, "isKidsModefalse"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const/high16 v1, 0x10800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 135
    const-string v1, "isNeedUpdateKidsMode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 136
    const-string v1, "KidsModeUpdateService "

    const-string v2, "kidsmode_service_observer :  start installer"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v1, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer_intent;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 138
    iget-object v1, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer_intent;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->stopSelf()V

    .line 139
    return-void
.end method
