.class public Lcom/sec/kidsplat/installer/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field static final APK_SIZE:Ljava/lang/String; = "APK_SIZE"

.field static final DOWNLOAD_RATIO:Ljava/lang/String; = "DOWNLOAD_RATIO"

.field static final INSTALL_RESULT_FAILED:I = 0x0

.field static final INSTALL_RESULT_NOT_CHECKED:I = -0x1

.field static final INSTALL_RESULT_SUCCESSED:I = 0x1

.field static final IS_CHINA_USING_DIALOG_SHOW:Ljava/lang/String; = "IS_CHINA_USING_DIALOG_SHOW"

.field static final IS_DATA_USING_DIALOG_SHOW:Ljava/lang/String; = "IS_DATA_USING_DIALOG_SHOW"

.field static final IS_ROAMING_USING_DIALOG_SHOW:Ljava/lang/String; = "IS_ROAMING_USING_DIALOG_SHOW"

.field static final IS_USER_INFO_DIALOG_SHOW:Ljava/lang/String; = "IS_USER_INFO_DIALOG_SHOW"

.field static final MESSAGE_CMD:Ljava/lang/String; = "MESSAGE_CMD"

.field static final PROGRESSUPDATE:Ljava/lang/String; = "DOWNLODING_PROGRESS_UPDATE"

.field static final RATIO:Ljava/lang/String; = "DOWNLOA_RATIO"

.field static final RETURNCODE_ERROR:Ljava/lang/String; = "RETURNCODE_ERROR"

.field static final STATUS:Ljava/lang/String; = "DOWNLOAD_STATUS"

.field static final STATUS_DOWNLOADED:I = 0x1

.field static final STATUS_DOWNLOADED_ALL_COMPLETED:I = 0x8

.field static final STATUS_DOWNLOADING:I = 0x0

.field static final STATUS_DOWNLOAD_FAIL:I = 0x4

.field static final STATUS_INSTALLED:I = 0x3

.field static final STATUS_INSTALLING:I = 0x2

.field static final STATUS_INSTALL_FAIL:I = 0x5

.field static final STATUS_NETWORK_ERROR:I = 0x7

.field static final STATUS_REQUEST_DOWNLOAD:I = 0x6

.field static final STATUS_SIGNATURE_FAIL:I = 0x9

.field static final VERSION_CODE:Ljava/lang/String; = "VERSION_CODE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
