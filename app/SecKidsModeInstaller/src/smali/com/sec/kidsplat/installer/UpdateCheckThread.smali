.class public Lcom/sec/kidsplat/installer/UpdateCheckThread;
.super Ljava/lang/Thread;
.source "UpdateCheckThread.java"


# static fields
.field private static final CSC_PATH:Ljava/lang/String; = "/system/csc/sales_code.dat"

.field private static final PD_TEST_PATH:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "UpdateCheckThread"


# instance fields
.field private MESSAGE_DATA_URI:Ljava/lang/String;

.field private MESSAGE_ID:Ljava/lang/String;

.field private MESSAGE_RESULT:Ljava/lang/String;

.field private SIGNATURE:Ljava/lang/String;

.field private VERSION_CODE:Ljava/lang/String;

.field private mAppApkName:Ljava/lang/String;

.field private mCTCServerUrl:Ljava/lang/String;

.field private mCheckServerUrl:Ljava/lang/String;

.field private mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

.field private mContext:Landroid/content/Context;

.field private mDownloadServerUrl:Ljava/lang/String;

.field private mDownloadUri:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDownloadUriCTC:Ljava/lang/String;

.field private mFlagCancel:Z

.field private mHandler:Landroid/os/Handler;

.field private mID:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mResult:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdateUriCTC:Ljava/lang/String;

.field private szMCC:Ljava/lang/String;

.field private szMNC:Ljava/lang/String;

.field private szModel:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/go_to_andromeda.test"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->PD_TEST_PATH:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/sec/kidsplat/installer/StubAppConfig$Config;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "config"    # Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mFlagCancel:Z

    .line 53
    const-string v0, "id of the message"

    iput-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->MESSAGE_ID:Ljava/lang/String;

    .line 54
    const-string v0, "result of the message"

    iput-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->MESSAGE_RESULT:Ljava/lang/String;

    .line 55
    const-string v0, "uri of the message"

    iput-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->MESSAGE_DATA_URI:Ljava/lang/String;

    .line 56
    const-string v0, "version code of the app"

    iput-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->VERSION_CODE:Ljava/lang/String;

    .line 57
    const-string v0, "signature of the app"

    iput-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->SIGNATURE:Ljava/lang/String;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mDownloadUri:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mResult:Ljava/util/ArrayList;

    .line 62
    iput-object v1, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mUpdateUriCTC:Ljava/lang/String;

    .line 63
    iput-object v1, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mDownloadUriCTC:Ljava/lang/String;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMCC:Ljava/lang/String;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMNC:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szModel:Ljava/lang/String;

    .line 77
    iput-object p1, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mContext:Landroid/content/Context;

    .line 78
    iput-object p3, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    .line 79
    iput-object p2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    .line 80
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->init()V

    .line 81
    return-void
.end method

.method private checkMultipleDownload(Ljava/net/URL;)Z
    .locals 24
    .param p1, "url"    # Ljava/net/URL;

    .prologue
    .line 479
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v6

    .line 480
    .local v6, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v6}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v12

    .line 482
    .local v12, "parser":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v8, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v8}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 485
    .local v8, "httpclient":Lorg/apache/http/client/HttpClient;
    const-string v20, "46003"

    const-string v21, "gsm.operator.numeric"

    const-string v22, ""

    invoke-static/range {v21 .. v22}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 487
    const/16 v18, 0x78

    .line 488
    .local v18, "timeout":I
    invoke-interface {v8}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v7

    .line 489
    .local v7, "httpParams":Lorg/apache/http/params/HttpParams;
    const v20, 0x1d4c0

    move/from16 v0, v20

    invoke-static {v7, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 490
    const v20, 0x1d4c0

    move/from16 v0, v20

    invoke-static {v7, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 494
    .end local v7    # "httpParams":Lorg/apache/http/params/HttpParams;
    .end local v18    # "timeout":I
    :cond_0
    new-instance v9, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual/range {p1 .. p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v9, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6

    .line 496
    .local v9, "httpget":Lorg/apache/http/client/methods/HttpGet;
    const/4 v14, 0x0

    .line 498
    .local v14, "response":Lorg/apache/http/HttpResponse;
    :try_start_1
    invoke-interface {v8, v9}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v14

    .line 500
    const-string v20, "Praeda"

    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    move-result-object v5

    .line 506
    .local v5, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v5, :cond_1

    .line 510
    :try_start_2
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v11

    .line 512
    .local v11, "instream":Ljava/io/InputStream;
    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-interface {v12, v11, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_5

    .line 523
    .end local v5    # "entity":Lorg/apache/http/HttpEntity;
    .end local v11    # "instream":Ljava/io/InputStream;
    :cond_1
    :goto_0
    :try_start_3
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v13

    .line 525
    .local v13, "parserEvent":I
    const-string v10, ""

    .local v10, "id":Ljava/lang/String;
    const-string v15, ""

    .line 526
    .local v15, "result":Ljava/lang/String;
    const-string v2, ""

    .line 527
    .local v2, "DownloadURI":Ljava/lang/String;
    const-string v3, ""

    .line 528
    .local v3, "contentSize":Ljava/lang/String;
    const-string v16, ""

    .line 529
    .local v16, "signature":Ljava/lang/String;
    :goto_1
    const/16 v20, 0x1

    move/from16 v0, v20

    if-eq v13, v0, :cond_b

    .line 530
    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v13, v0, :cond_7

    .line 531
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v17

    .line 532
    .local v17, "tag":Ljava/lang/String;
    const-string v20, "appId"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 533
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v19

    .line 535
    .local v19, "type":I
    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    .line 536
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v10

    .line 537
    const-string v20, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "appId : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    .end local v19    # "type":I
    :cond_2
    const-string v20, "resultCode"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 541
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v19

    .line 542
    .restart local v19    # "type":I
    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 543
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v15

    .line 544
    const-string v20, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "resultCode : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    .end local v19    # "type":I
    :cond_3
    const-string v20, "downloadURI"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 548
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v19

    .line 549
    .restart local v19    # "type":I
    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 550
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v2

    .line 551
    const-string v20, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "downloadURI : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    .end local v19    # "type":I
    :cond_4
    const-string v20, "contentSize"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 555
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v19

    .line 556
    .restart local v19    # "type":I
    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_5

    .line 557
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v3

    .line 558
    const-string v20, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "contentSize : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    .end local v19    # "type":I
    :cond_5
    const-string v20, "signature"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 562
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v19

    .line 563
    .restart local v19    # "type":I
    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_6

    .line 564
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v16

    .line 565
    const-string v20, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "appId : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " get a signature"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    :cond_6
    const-string v20, "com.sec.android.app.kidsapps"

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 568
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->setStoreStignature(Ljava/lang/String;)V

    .line 575
    .end local v17    # "tag":Ljava/lang/String;
    .end local v19    # "type":I
    :cond_7
    :goto_2
    const/16 v20, 0x3

    move/from16 v0, v20

    if-ne v13, v0, :cond_9

    .line 576
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v17

    .line 577
    .restart local v17    # "tag":Ljava/lang/String;
    const-string v20, "contentSize"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 578
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mResult:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 580
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mDownloadUri:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 581
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getPackageIndex(Ljava/lang/String;)I

    move-result v21

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v22

    invoke-interface/range {v20 .. v22}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 582
    const-string v20, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "mID : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    const-string v20, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "mResult : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    const-string v20, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "contentSize"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    :cond_8
    const-string v20, "appInfo"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 588
    const-string v10, ""

    .line 589
    const-string v15, ""

    .line 590
    const-string v2, ""

    .line 591
    const-string v3, ""

    .line 594
    .end local v17    # "tag":Ljava/lang/String;
    :cond_9
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    move-result v13

    goto/16 :goto_1

    .line 513
    .end local v2    # "DownloadURI":Ljava/lang/String;
    .end local v3    # "contentSize":Ljava/lang/String;
    .end local v10    # "id":Ljava/lang/String;
    .end local v13    # "parserEvent":I
    .end local v15    # "result":Ljava/lang/String;
    .end local v16    # "signature":Ljava/lang/String;
    .restart local v5    # "entity":Lorg/apache/http/HttpEntity;
    :catch_0
    move-exception v4

    .line 514
    .local v4, "e":Ljava/lang/IllegalStateException;
    :try_start_4
    invoke-virtual {v4}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    goto/16 :goto_0

    .line 519
    .end local v4    # "e":Ljava/lang/IllegalStateException;
    .end local v5    # "entity":Lorg/apache/http/HttpEntity;
    :catch_1
    move-exception v4

    .line 520
    .local v4, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    goto/16 :goto_0

    .line 596
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v9    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .end local v12    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v14    # "response":Lorg/apache/http/HttpResponse;
    :catch_2
    move-exception v4

    .line 597
    .local v4, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v20, "UpdateCheckThread"

    const-string v21, "xml parsing error"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 599
    const/16 v20, 0x0

    .line 613
    .end local v4    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_3
    return v20

    .line 515
    .restart local v5    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v8    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v9    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .restart local v12    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v14    # "response":Lorg/apache/http/HttpResponse;
    :catch_3
    move-exception v4

    .line 516
    .local v4, "e":Ljava/io/IOException;
    :try_start_6
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    .line 600
    .end local v4    # "e":Ljava/io/IOException;
    .end local v5    # "entity":Lorg/apache/http/HttpEntity;
    .end local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v9    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .end local v12    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v14    # "response":Lorg/apache/http/HttpResponse;
    :catch_4
    move-exception v4

    .line 601
    .local v4, "e":Ljava/net/SocketException;
    invoke-virtual {v4}, Ljava/net/SocketException;->printStackTrace()V

    .line 602
    const-string v20, "UpdateCheckThread"

    const-string v21, "network is unavailable"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    const/16 v20, 0x0

    goto :goto_3

    .line 571
    .end local v4    # "e":Ljava/net/SocketException;
    .restart local v2    # "DownloadURI":Ljava/lang/String;
    .restart local v3    # "contentSize":Ljava/lang/String;
    .restart local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v8    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v9    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .restart local v10    # "id":Ljava/lang/String;
    .restart local v12    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v13    # "parserEvent":I
    .restart local v14    # "response":Lorg/apache/http/HttpResponse;
    .restart local v15    # "result":Ljava/lang/String;
    .restart local v16    # "signature":Ljava/lang/String;
    .restart local v17    # "tag":Ljava/lang/String;
    .restart local v19    # "type":I
    :cond_a
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->setSignature(Ljava/lang/String;)V
    :try_end_7
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/net/SocketException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_2

    .line 604
    .end local v2    # "DownloadURI":Ljava/lang/String;
    .end local v3    # "contentSize":Ljava/lang/String;
    .end local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v9    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .end local v10    # "id":Ljava/lang/String;
    .end local v12    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v13    # "parserEvent":I
    .end local v14    # "response":Lorg/apache/http/HttpResponse;
    .end local v15    # "result":Ljava/lang/String;
    .end local v16    # "signature":Ljava/lang/String;
    .end local v17    # "tag":Ljava/lang/String;
    .end local v19    # "type":I
    :catch_5
    move-exception v4

    .line 605
    .local v4, "e":Ljava/net/UnknownHostException;
    invoke-virtual {v4}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 606
    const-string v20, "UpdateCheckThread"

    const-string v21, "server is not response"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    .end local v4    # "e":Ljava/net/UnknownHostException;
    :cond_b
    const/16 v20, 0x1

    goto :goto_3

    .line 607
    :catch_6
    move-exception v4

    .line 608
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 609
    const-string v20, "UpdateCheckThread"

    const-string v21, "network error"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 610
    const/16 v20, 0x0

    goto :goto_3
.end method

.method private doMultipleCheckUpdate(Ljava/lang/String;)Z
    .locals 9
    .param p1, "serverURL"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    .line 147
    const/4 v2, 0x0

    .line 149
    .local v2, "result":Z
    :try_start_0
    const-string v4, "UpdateCheckThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "StubUpdateCheck url : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 152
    .local v3, "url":Ljava/net/URL;
    invoke-direct {p0, v3}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->multipleCheckUpdate(Ljava/net/URL;)Z
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 156
    const-string v4, "UpdateCheckThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "end StubUpdateCheck : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    if-nez v2, :cond_0

    .line 158
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 159
    .local v1, "msg":Landroid/os/Message;
    iput v8, v1, Landroid/os/Message;->what:I

    .line 160
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 163
    .end local v1    # "msg":Landroid/os/Message;
    .end local v3    # "url":Ljava/net/URL;
    :cond_0
    :goto_0
    return v2

    .line 153
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e1":Ljava/net/MalformedURLException;
    :try_start_1
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156
    const-string v4, "UpdateCheckThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "end StubUpdateCheck : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    if-nez v2, :cond_0

    .line 158
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 159
    .restart local v1    # "msg":Landroid/os/Message;
    iput v8, v1, Landroid/os/Message;->what:I

    .line 160
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 156
    .end local v0    # "e1":Ljava/net/MalformedURLException;
    .end local v1    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v4

    const-string v5, "UpdateCheckThread"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "end StubUpdateCheck : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    if-nez v2, :cond_1

    .line 158
    iget-object v5, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 159
    .restart local v1    # "msg":Landroid/os/Message;
    iput v8, v1, Landroid/os/Message;->what:I

    .line 160
    iget-object v5, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 161
    .end local v1    # "msg":Landroid/os/Message;
    :cond_1
    throw v4
.end method

.method private getCSC()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 712
    const-string v0, ""

    .line 713
    .local v0, "cscVersion":Ljava/lang/String;
    const/4 v1, 0x0

    .line 716
    .local v1, "value":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->isCSCExistFile()Z

    move-result v2

    if-eq v2, v3, :cond_0

    .line 731
    :goto_0
    return-object v0

    .line 719
    :cond_0
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->getCSCVersion()Ljava/lang/String;

    move-result-object v1

    .line 720
    if-nez v1, :cond_1

    .line 721
    const-string v2, "UpdateCheckThread"

    const-string v3, "getCSC::getCSCVersion::value is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 724
    :cond_1
    const-string v2, "FAIL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v3, :cond_2

    .line 725
    const-string v2, "UpdateCheckThread"

    const-string v3, "getCSC::getCSCVersion::Fail to read CSC Version"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 728
    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getCSCVersion()Ljava/lang/String;
    .locals 8

    .prologue
    .line 735
    const/4 v5, 0x0

    .line 736
    .local v5, "s":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    const-string v7, "/system/csc/sales_code.dat"

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 737
    .local v4, "mFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 738
    const/16 v7, 0x14

    new-array v0, v7, [B

    .line 739
    .local v0, "buffer":[B
    const/4 v2, 0x0

    .line 742
    .local v2, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 743
    .end local v2    # "in":Ljava/io/InputStream;
    .local v3, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v7

    if-eqz v7, :cond_1

    .line 744
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v5    # "s":Ljava/lang/String;
    .local v6, "s":Ljava/lang/String;
    move-object v5, v6

    .line 753
    .end local v6    # "s":Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    :goto_0
    if-eqz v3, :cond_0

    .line 755
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 756
    const/4 v2, 0x0

    .line 763
    .end local v0    # "buffer":[B
    .end local v3    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_1
    return-object v5

    .line 746
    .restart local v0    # "buffer":[B
    .restart local v3    # "in":Ljava/io/InputStream;
    :cond_1
    :try_start_3
    new-instance v6, Ljava/lang/String;

    const-string v7, "FAIL"

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .end local v5    # "s":Ljava/lang/String;
    .restart local v6    # "s":Ljava/lang/String;
    move-object v5, v6

    .end local v6    # "s":Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    goto :goto_0

    .line 757
    :catch_0
    move-exception v1

    .line 758
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 748
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v1

    .line 749
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 753
    if-eqz v2, :cond_0

    .line 755
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 756
    const/4 v2, 0x0

    goto :goto_1

    .line 757
    :catch_2
    move-exception v1

    .line 758
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 750
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 751
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 753
    if-eqz v2, :cond_0

    .line 755
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 756
    const/4 v2, 0x0

    goto :goto_1

    .line 757
    :catch_4
    move-exception v1

    .line 758
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 753
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_4
    if-eqz v2, :cond_2

    .line 755
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 756
    const/4 v2, 0x0

    .line 759
    :cond_2
    :goto_5
    throw v7

    .line 757
    :catch_5
    move-exception v1

    .line 758
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 753
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v7

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_4

    .line 750
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_6
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_3

    .line 748
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_2
.end method

.method private getIMEI()Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x10

    .line 782
    iget-object v6, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mContext:Landroid/content/Context;

    const-string v7, "phone"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 784
    .local v5, "telMgr":Landroid/telephony/TelephonyManager;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 786
    .local v4, "md5":Ljava/lang/StringBuffer;
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    .line 787
    .local v3, "imei":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 788
    const-string v3, "000000000000000"

    .line 789
    :cond_0
    if-eqz v5, :cond_1

    .line 790
    const/4 v0, 0x0

    .line 793
    .local v0, "digest":[B
    :try_start_0
    const-string v6, "MD5"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/security/MessageDigest;->digest([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 801
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v6, v0

    if-ge v2, v6, :cond_1

    .line 802
    aget-byte v6, v0, v2

    and-int/lit16 v6, v6, 0xf0

    shr-int/lit8 v6, v6, 0x4

    invoke-static {v6, v8}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 803
    aget-byte v6, v0, v2

    and-int/lit8 v6, v6, 0xf

    shr-int/lit8 v6, v6, 0x0

    invoke-static {v6, v8}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 801
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 796
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 798
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 807
    .end local v0    # "digest":[B
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    .line 809
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method private getMCC()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 686
    const-string v0, ""

    .line 687
    .local v0, "mcc":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mContext:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 690
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_0

    .line 691
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 692
    .local v1, "networkOperator":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v5, :cond_0

    .line 693
    const/4 v3, 0x0

    invoke-virtual {v1, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 696
    .end local v1    # "networkOperator":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private getMNC()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 700
    const-string v0, "00"

    .line 701
    .local v0, "mnc":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mContext:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 702
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_0

    .line 703
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 704
    .local v1, "networkOperator":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v5, :cond_0

    .line 705
    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 708
    .end local v1    # "networkOperator":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private getMultipleResultUpdateCheck()V
    .locals 8

    .prologue
    .line 443
    const-string v4, "UpdateCheckThread"

    const-string v5, "getMultipleResultUpdateCheck()"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    const-string v4, "460"

    iget-object v5, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMCC:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "03"

    iget-object v5, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMNC:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 447
    const-string v4, "UpdateCheckThread"

    const-string v5, "checkDownload : CTC network "

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mDownloadUriCTC:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mDownloadUriCTC:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 449
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mDownloadUriCTC:Ljava/lang/String;

    .line 450
    .local v2, "server_url":Ljava/lang/String;
    const-string v4, "UpdateCheckThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "checkDownload : server_url = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    :goto_0
    invoke-direct {p0, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->makeMultipleDownloadURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 461
    const/4 v1, 0x0

    .line 465
    .local v1, "result":Z
    :try_start_0
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 466
    .local v3, "url":Ljava/net/URL;
    invoke-direct {p0, v3}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->checkMultipleDownload(Ljava/net/URL;)Z
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 470
    const-string v4, "UpdateCheckThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "end StubDownload : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    .end local v3    # "url":Ljava/net/URL;
    :goto_1
    return-void

    .line 452
    .end local v1    # "result":Z
    .end local v2    # "server_url":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mDownloadServerUrl:Ljava/lang/String;

    .line 453
    .restart local v2    # "server_url":Ljava/lang/String;
    const-string v4, "UpdateCheckThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "checkDownload : server_url = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 456
    .end local v2    # "server_url":Ljava/lang/String;
    :cond_1
    const-string v4, "UpdateCheckThread"

    const-string v5, "checkDownload : no CTC"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mDownloadServerUrl:Ljava/lang/String;

    .restart local v2    # "server_url":Ljava/lang/String;
    goto :goto_0

    .line 467
    .restart local v1    # "result":Z
    :catch_0
    move-exception v0

    .line 468
    .local v0, "e1":Ljava/net/MalformedURLException;
    :try_start_1
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 470
    const-string v4, "UpdateCheckThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "end StubDownload : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .end local v0    # "e1":Ljava/net/MalformedURLException;
    :catchall_0
    move-exception v4

    const-string v5, "UpdateCheckThread"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "end StubDownload : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    throw v4
.end method

.method private getPD()Ljava/lang/String;
    .locals 5

    .prologue
    .line 813
    const-string v3, "0"

    .line 814
    .local v3, "rtn_str":Ljava/lang/String;
    const/4 v2, 0x0

    .line 816
    .local v2, "result":Z
    new-instance v1, Ljava/io/File;

    sget-object v4, Lcom/sec/kidsplat/installer/UpdateCheckThread;->PD_TEST_PATH:Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 819
    .local v1, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    .line 820
    const/4 v4, 0x1

    if-ne v2, v4, :cond_0

    .line 821
    const-string v3, "1"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 826
    :cond_0
    :goto_0
    return-object v3

    .line 823
    :catch_0
    move-exception v0

    .line 824
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getUrlForCTC(Ljava/lang/String;)Z
    .locals 17
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 233
    const-string v14, "UpdateCheckThread"

    const-string v15, "getUrlForCTC()"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    const/4 v1, 0x0

    .line 238
    .local v1, "bReturn":Z
    new-instance v5, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v5}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 239
    .local v5, "httpclient":Lorg/apache/http/client/HttpClient;
    new-instance v6, Lorg/apache/http/client/methods/HttpGet;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 240
    .local v6, "httpget":Lorg/apache/http/client/methods/HttpGet;
    const/4 v10, 0x0

    .line 243
    .local v10, "response":Lorg/apache/http/HttpResponse;
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 244
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 247
    .local v8, "parser":Lorg/xmlpull/v1/XmlPullParser;
    :try_start_1
    invoke-interface {v5, v6}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v10

    .line 249
    const-string v14, "UpdateCheckThread"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " checkDownload() response : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v10}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    invoke-interface {v10}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 253
    .local v3, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v3, :cond_0

    .line 255
    :try_start_2
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v7

    .line 256
    .local v7, "instream":Ljava/io/InputStream;
    const/4 v14, 0x0

    invoke-interface {v8, v7, v14}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 267
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v7    # "instream":Ljava/io/InputStream;
    :cond_0
    :goto_0
    :try_start_3
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v9

    .line 269
    .local v9, "parserEvent":I
    const-string v11, ""

    .line 271
    .local v11, "serverUri":Ljava/lang/String;
    :goto_1
    const/4 v14, 0x1

    if-eq v9, v14, :cond_4

    .line 273
    const/4 v14, 0x2

    if-ne v9, v14, :cond_1

    .line 274
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    .line 275
    .local v12, "tag":Ljava/lang/String;
    const-string v14, "serverURL"

    invoke-virtual {v14, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 276
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v13

    .line 277
    .local v13, "type":I
    const/4 v14, 0x4

    if-ne v13, v14, :cond_1

    .line 278
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v11

    .line 282
    .end local v12    # "tag":Ljava/lang/String;
    .end local v13    # "type":I
    :cond_1
    const/4 v14, 0x3

    if-ne v9, v14, :cond_2

    .line 283
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    .line 284
    .restart local v12    # "tag":Ljava/lang/String;
    const-string v14, "serverURL"

    invoke-virtual {v14, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 285
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "http://"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/stub/stubUpdateCheck.as"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mUpdateUriCTC:Ljava/lang/String;

    .line 286
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "https://"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/stub/stubDownload.as"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mDownloadUriCTC:Ljava/lang/String;

    .line 287
    const-string v14, "UpdateCheckThread"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " mUpdateUri : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mUpdateUriCTC:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", mDownloadUri :  "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mDownloadUriCTC:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    .end local v12    # "tag":Ljava/lang/String;
    :cond_2
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v9

    goto/16 :goto_1

    .line 257
    .end local v9    # "parserEvent":I
    .end local v11    # "serverUri":Ljava/lang/String;
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    :catch_0
    move-exception v2

    .line 258
    .local v2, "e":Ljava/lang/IllegalStateException;
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 263
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    :catch_1
    move-exception v2

    .line 264
    .local v2, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 293
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_2
    move-exception v2

    .line 294
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_6
    const-string v14, "UpdateCheckThread"

    const-string v15, "xml parsing error"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 296
    const/4 v1, 0x0

    .line 310
    if-eqz v5, :cond_3

    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 311
    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 312
    const-string v14, "UpdateCheckThread"

    const-string v15, "shutdown"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    .end local v2    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :cond_3
    :goto_2
    return v1

    .line 259
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_3
    move-exception v2

    .line 260
    .local v2, "e":Ljava/io/IOException;
    :try_start_7
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/net/SocketException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 297
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_4
    move-exception v2

    .line 298
    .local v2, "e":Ljava/net/SocketException;
    :try_start_8
    invoke-virtual {v2}, Ljava/net/SocketException;->printStackTrace()V

    .line 299
    const-string v14, "UpdateCheckThread"

    const-string v15, "network is unavailable"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 300
    const/4 v1, 0x0

    .line 310
    if-eqz v5, :cond_3

    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 311
    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 312
    const-string v14, "UpdateCheckThread"

    const-string v15, "shutdown"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 292
    .end local v2    # "e":Ljava/net/SocketException;
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v9    # "parserEvent":I
    .restart local v11    # "serverUri":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x1

    .line 310
    if-eqz v5, :cond_3

    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 311
    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 312
    const-string v14, "UpdateCheckThread"

    const-string v15, "shutdown"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 301
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v9    # "parserEvent":I
    .end local v11    # "serverUri":Ljava/lang/String;
    :catch_5
    move-exception v2

    .line 302
    .local v2, "e":Ljava/net/UnknownHostException;
    :try_start_9
    invoke-virtual {v2}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 303
    const-string v14, "UpdateCheckThread"

    const-string v15, "server is not response"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 304
    const/4 v1, 0x0

    .line 310
    if-eqz v5, :cond_3

    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 311
    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 312
    const-string v14, "UpdateCheckThread"

    const-string v15, "shutdown"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 305
    .end local v2    # "e":Ljava/net/UnknownHostException;
    :catch_6
    move-exception v2

    .line 306
    .local v2, "e":Ljava/io/IOException;
    :try_start_a
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 307
    const-string v14, "UpdateCheckThread"

    const-string v15, "network error"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 308
    const/4 v1, 0x0

    .line 310
    if-eqz v5, :cond_3

    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 311
    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 312
    const-string v14, "UpdateCheckThread"

    const-string v15, "shutdown"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 310
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v14

    if-eqz v5, :cond_5

    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v15

    if-eqz v15, :cond_5

    .line 311
    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v15

    invoke-interface {v15}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 312
    const-string v15, "UpdateCheckThread"

    const-string v16, "shutdown"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    throw v14
.end method

.method private init()V
    .locals 7

    .prologue
    .line 84
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    if-eqz v4, :cond_0

    .line 85
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v4}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getCheckSizeAppUrl()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mCheckServerUrl:Ljava/lang/String;

    .line 86
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v4}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getDownloadUrl()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mDownloadServerUrl:Ljava/lang/String;

    .line 87
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v4}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getCheckCtcUrl()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mCTCServerUrl:Ljava/lang/String;

    .line 88
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v4}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mAppApkName:Ljava/lang/String;

    .line 89
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mAppApkName:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 90
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mAppApkName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".apk"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mAppApkName:Ljava/lang/String;

    .line 94
    :cond_0
    const-string v4, "UpdateCheckThread"

    const-string v5, "UpdateCheckThread : run() "

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szModel:Ljava/lang/String;

    .line 96
    const-string v2, "SAMSUNG-"

    .line 98
    .local v2, "szPrefix":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szModel:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 99
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szModel:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v2, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szModel:Ljava/lang/String;

    .line 106
    :cond_1
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mContext:Landroid/content/Context;

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 107
    .local v0, "cManager":Landroid/net/ConnectivityManager;
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 108
    .local v1, "mobile":Landroid/net/NetworkInfo;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 110
    .local v3, "wifi":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 111
    const-string v4, "505"

    iput-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMCC:Ljava/lang/String;

    .line 112
    const-string v4, "00"

    iput-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMNC:Ljava/lang/String;

    .line 122
    :goto_0
    return-void

    .line 113
    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 114
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->getMCC()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMCC:Ljava/lang/String;

    .line 115
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->getMNC()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMNC:Ljava/lang/String;

    .line 116
    const-string v4, "UpdateCheckThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " run() : szMCC = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMCC:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " szMNC = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMNC:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 118
    :cond_3
    const-string v4, "UpdateCheckThread"

    const-string v5, "Connection failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isCSCExistFile()Z
    .locals 5

    .prologue
    .line 767
    const/4 v2, 0x0

    .line 768
    .local v2, "result":Z
    new-instance v1, Ljava/io/File;

    const-string v3, "/system/csc/sales_code.dat"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 771
    .local v1, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    .line 772
    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 773
    const-string v3, "UpdateCheckThread"

    const-string v4, "CSC is not exist"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 778
    :cond_0
    :goto_0
    return v2

    .line 775
    :catch_0
    move-exception v0

    .line 776
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private makeKidsModeAppIdForDownload()Ljava/lang/String;
    .locals 4

    .prologue
    .line 201
    const-string v1, ""

    .line 202
    .local v1, "kidsModeAppId":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v2}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 203
    if-eqz v0, :cond_0

    .line 204
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 206
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v3, v0}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getPackageNameList(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 211
    :cond_1
    return-object v1
.end method

.method private makeKidsModeAppIdForUpdateCheck()Ljava/lang/String;
    .locals 4

    .prologue
    .line 185
    const-string v1, ""

    .line 186
    .local v1, "kidsModeAppId":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v2}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 187
    if-eqz v0, :cond_0

    .line 188
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 190
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v3, v0}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getPackageNameList(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 191
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 186
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 196
    :cond_1
    return-object v1
.end method

.method private makeMultipleDownloadURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "serverURL"    # Ljava/lang/String;

    .prologue
    .line 216
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?appId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->makeKidsModeAppIdForDownload()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 217
    const-string v0, "UpdateCheckThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "makeMultipleDownloadURL() appId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&encImei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->getIMEI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&deviceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&mcc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v0, "1"

    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->getPD()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "000"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 222
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&mnc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMNC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 223
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&csc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->getCSC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 224
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&sdkVer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&pd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->getPD()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 228
    return-object p1

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMCC:Ljava/lang/String;

    goto :goto_0
.end method

.method private makeMultipleUpdateCheckURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "serverURL"    # Ljava/lang/String;

    .prologue
    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?appId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->makeKidsModeAppIdForUpdateCheck()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&deviceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&mcc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v0, "1"

    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->getPD()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "000"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&mnc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMNC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&csc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->getCSC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&sdkVer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&pd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->getPD()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 180
    return-object p1

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMCC:Ljava/lang/String;

    goto :goto_0
.end method

.method private multipleCheckUpdate(Ljava/net/URL;)Z
    .locals 24
    .param p1, "url"    # Ljava/net/URL;

    .prologue
    .line 322
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v5

    .line 323
    .local v5, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v5}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v11

    .line 324
    .local v11, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-virtual/range {p1 .. p1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v19

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v11, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 325
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v12

    .line 328
    .local v12, "parserEvent":I
    const-string v7, ""

    .local v7, "id":Ljava/lang/String;
    const-string v13, ""

    .local v13, "result":Ljava/lang/String;
    const-string v18, ""

    .line 329
    .local v18, "versionCode":Ljava/lang/String;
    :goto_0
    const/16 v19, 0x1

    move/from16 v0, v19

    if-eq v12, v0, :cond_9

    .line 330
    const/16 v19, 0x2

    move/from16 v0, v19

    if-ne v12, v0, :cond_4

    .line 331
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v16

    .line 333
    .local v16, "tag":Ljava/lang/String;
    const-string v19, "appId"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 334
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v17

    .line 335
    .local v17, "type":I
    const/16 v19, 0x4

    move/from16 v0, v17

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 336
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v7

    .line 337
    const-string v19, "UpdateCheckThread"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "appId : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    .end local v17    # "type":I
    :cond_0
    const-string v19, "resultCode"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 341
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v17

    .line 342
    .restart local v17    # "type":I
    const/16 v19, 0x4

    move/from16 v0, v17

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 343
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v13

    .line 344
    const-string v19, "UpdateCheckThread"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "StubUpdateCheck id : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", result : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", integer : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    const-string v19, ""

    move-object/from16 v0, v19

    if-eq v7, v0, :cond_1

    .line 346
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getPackageIndex(Ljava/lang/String;)I

    move-result v8

    .line 347
    .local v8, "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->updateAvailable:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v8, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 351
    .end local v8    # "index":I
    .end local v17    # "type":I
    :cond_1
    const-string v19, "resultMsg"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 352
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v17

    .line 353
    .restart local v17    # "type":I
    const/16 v19, 0x4

    move/from16 v0, v17

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 354
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v13

    .line 355
    const-string v19, "UpdateCheckThread"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "StubUpdateCheck resultMsg : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    .end local v17    # "type":I
    :cond_2
    const-string v19, "versionCode"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 359
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v17

    .line 360
    .restart local v17    # "type":I
    const/16 v19, 0x4

    move/from16 v0, v17

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 361
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v18

    .line 362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsUpdateMode:Z

    move/from16 v19, v0

    if-eqz v19, :cond_4

    .line 363
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getPackageIndex(Ljava/lang/String;)I

    move-result v8

    .line 364
    .restart local v8    # "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getPackageNameList(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->verifyUpdatePackage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    .line 365
    .local v10, "needToUpdate":Z
    if-nez v10, :cond_3

    .line 366
    const-string v19, "UpdateCheckThread"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "lastest "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getPackageNameList(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " has already installed"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v8, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 369
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->VERSION_CODE:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const-string v19, "UpdateCheckThread"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "versionCode : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    .end local v8    # "index":I
    .end local v10    # "needToUpdate":Z
    .end local v16    # "tag":Ljava/lang/String;
    .end local v17    # "type":I
    :cond_4
    const/16 v19, 0x3

    move/from16 v0, v19

    if-ne v12, v0, :cond_6

    .line 379
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v16

    .line 380
    .restart local v16    # "tag":Ljava/lang/String;
    const-string v19, "appInfo"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 381
    const-string v19, "UpdateCheckThread"

    const-string v20, "appInfo, reset id and result"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    const-string v7, ""

    .line 383
    const-string v13, ""

    .line 385
    :cond_5
    const-string v19, "result"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 387
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->updateAvailable:Ljava/util/List;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_7

    .line 388
    const-string v19, "UpdateCheckThread"

    const-string v20, "call getResultUpdateCheck()"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    invoke-direct/range {p0 .. p0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->getMultipleResultUpdateCheck()V

    .line 396
    .end local v16    # "tag":Ljava/lang/String;
    :cond_6
    :goto_1
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v12

    goto/16 :goto_0

    .line 391
    .restart local v16    # "tag":Ljava/lang/String;
    :cond_7
    const-string v19, "UpdateCheckThread"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Not available : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->updateAvailable:Ljava/util/List;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-interface/range {v22 .. v23}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getPackageNameList(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 398
    .end local v5    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v7    # "id":Ljava/lang/String;
    .end local v11    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v12    # "parserEvent":I
    .end local v13    # "result":Ljava/lang/String;
    .end local v16    # "tag":Ljava/lang/String;
    .end local v18    # "versionCode":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 399
    .local v4, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_1
    const-string v19, "UpdateCheckThread"

    const-string v20, "xml parsing error"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401
    const/16 v20, 0x0

    .line 420
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_e

    .line 421
    const-wide/16 v14, 0x0

    .line 422
    .local v14, "sum":D
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v6, v0, :cond_d

    .line 424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Boolean;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_8

    .line 425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Double;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v22

    add-double v14, v14, v22

    .line 422
    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 420
    .end local v4    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .end local v6    # "i":I
    .end local v14    # "sum":D
    .restart local v5    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v7    # "id":Ljava/lang/String;
    .restart local v11    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v12    # "parserEvent":I
    .restart local v13    # "result":Ljava/lang/String;
    .restart local v18    # "versionCode":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_c

    .line 421
    const-wide/16 v14, 0x0

    .line 422
    .restart local v14    # "sum":D
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v6, v0, :cond_b

    .line 424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Boolean;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_a

    .line 425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Double;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v20

    add-double v14, v14, v20

    .line 422
    :cond_a
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 428
    :cond_b
    const-string v19, "UpdateCheckThread"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "all package sizes : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14, v15}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->setTotalSize(D)V

    .line 430
    const-string v19, "APK_SIZE"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v14, v15}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;D)V

    .line 431
    const-string v19, "DOWNLOAD_STATUS"

    const/16 v20, 0x6

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    .line 439
    .end local v5    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v6    # "i":I
    .end local v7    # "id":Ljava/lang/String;
    .end local v11    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v12    # "parserEvent":I
    .end local v13    # "result":Ljava/lang/String;
    .end local v14    # "sum":D
    .end local v18    # "versionCode":Ljava/lang/String;
    :goto_4
    const/16 v19, 0x1

    :goto_5
    return v19

    .line 433
    .restart local v5    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v7    # "id":Ljava/lang/String;
    .restart local v11    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v12    # "parserEvent":I
    .restart local v13    # "result":Ljava/lang/String;
    .restart local v18    # "versionCode":Ljava/lang/String;
    :cond_c
    const-string v19, "UpdateCheckThread"

    const-string v20, "send RETURNCODE_ERROR"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const-string v19, "UpdateCheckThread"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "(mConfig.getAppCount() : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    const-string v19, "UpdateCheckThread"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "mID.size() : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    const-string v19, "RETURNCODE_ERROR"

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    goto :goto_4

    .line 428
    .end local v5    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v7    # "id":Ljava/lang/String;
    .end local v11    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v12    # "parserEvent":I
    .end local v13    # "result":Ljava/lang/String;
    .end local v18    # "versionCode":Ljava/lang/String;
    .restart local v4    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v6    # "i":I
    .restart local v14    # "sum":D
    :cond_d
    const-string v19, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "all package sizes : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14, v15}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->setTotalSize(D)V

    .line 430
    const-string v19, "APK_SIZE"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v14, v15}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;D)V

    .line 431
    const-string v19, "DOWNLOAD_STATUS"

    const/16 v21, 0x6

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    .end local v6    # "i":I
    .end local v14    # "sum":D
    :goto_6
    move/from16 v19, v20

    .line 436
    goto/16 :goto_5

    .line 433
    :cond_e
    const-string v19, "UpdateCheckThread"

    const-string v21, "send RETURNCODE_ERROR"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const-string v19, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "(mConfig.getAppCount() : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    const-string v19, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "mID.size() : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    const-string v19, "RETURNCODE_ERROR"

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    goto :goto_6

    .line 402
    .end local v4    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v4

    .line 403
    .local v4, "e":Ljava/net/SocketException;
    :try_start_2
    invoke-virtual {v4}, Ljava/net/SocketException;->printStackTrace()V

    .line 404
    const-string v19, "UpdateCheckThread"

    const-string v20, "network is unavailable"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    .line 406
    .local v9, "msg":Landroid/os/Message;
    const/16 v19, 0x7

    move/from16 v0, v19

    iput v0, v9, Landroid/os/Message;->what:I

    .line 407
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 408
    const/16 v20, 0x0

    .line 420
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_11

    .line 421
    const-wide/16 v14, 0x0

    .line 422
    .restart local v14    # "sum":D
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v6, v0, :cond_10

    .line 424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Boolean;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_f

    .line 425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Double;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v22

    add-double v14, v14, v22

    .line 422
    :cond_f
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    .line 428
    :cond_10
    const-string v19, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "all package sizes : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14, v15}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->setTotalSize(D)V

    .line 430
    const-string v19, "APK_SIZE"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v14, v15}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;D)V

    .line 431
    const-string v19, "DOWNLOAD_STATUS"

    const/16 v21, 0x6

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    .end local v6    # "i":I
    .end local v14    # "sum":D
    :goto_8
    move/from16 v19, v20

    .line 436
    goto/16 :goto_5

    .line 433
    :cond_11
    const-string v19, "UpdateCheckThread"

    const-string v21, "send RETURNCODE_ERROR"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const-string v19, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "(mConfig.getAppCount() : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    const-string v19, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "mID.size() : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    const-string v19, "RETURNCODE_ERROR"

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    goto :goto_8

    .line 409
    .end local v4    # "e":Ljava/net/SocketException;
    .end local v9    # "msg":Landroid/os/Message;
    :catch_2
    move-exception v4

    .line 410
    .local v4, "e":Ljava/net/UnknownHostException;
    :try_start_3
    invoke-virtual {v4}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 411
    const-string v19, "UpdateCheckThread"

    const-string v20, "server is not response"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 420
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_14

    .line 421
    const-wide/16 v14, 0x0

    .line 422
    .restart local v14    # "sum":D
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v6, v0, :cond_13

    .line 424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Boolean;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_12

    .line 425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Double;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v20

    add-double v14, v14, v20

    .line 422
    :cond_12
    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    .line 428
    :cond_13
    const-string v19, "UpdateCheckThread"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "all package sizes : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14, v15}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->setTotalSize(D)V

    .line 430
    const-string v19, "APK_SIZE"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v14, v15}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;D)V

    .line 431
    const-string v19, "DOWNLOAD_STATUS"

    const/16 v20, 0x6

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    goto/16 :goto_4

    .line 433
    .end local v6    # "i":I
    .end local v14    # "sum":D
    :cond_14
    const-string v19, "UpdateCheckThread"

    const-string v20, "send RETURNCODE_ERROR"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const-string v19, "UpdateCheckThread"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "(mConfig.getAppCount() : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    const-string v19, "UpdateCheckThread"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "mID.size() : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    const-string v19, "RETURNCODE_ERROR"

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    goto/16 :goto_4

    .line 412
    .end local v4    # "e":Ljava/net/UnknownHostException;
    :catch_3
    move-exception v4

    .line 413
    .local v4, "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 414
    const-string v19, "UpdateCheckThread"

    const-string v20, "network error"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    .line 416
    .restart local v9    # "msg":Landroid/os/Message;
    const/16 v19, 0x7

    move/from16 v0, v19

    iput v0, v9, Landroid/os/Message;->what:I

    .line 417
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 418
    const/16 v20, 0x0

    .line 420
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_17

    .line 421
    const-wide/16 v14, 0x0

    .line 422
    .restart local v14    # "sum":D
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v6, v0, :cond_16

    .line 424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Boolean;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_15

    .line 425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Double;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v22

    add-double v14, v14, v22

    .line 422
    :cond_15
    add-int/lit8 v6, v6, 0x1

    goto :goto_a

    .line 428
    :cond_16
    const-string v19, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "all package sizes : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14, v15}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->setTotalSize(D)V

    .line 430
    const-string v19, "APK_SIZE"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v14, v15}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;D)V

    .line 431
    const-string v19, "DOWNLOAD_STATUS"

    const/16 v21, 0x6

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    .end local v6    # "i":I
    .end local v14    # "sum":D
    :goto_b
    move/from16 v19, v20

    .line 436
    goto/16 :goto_5

    .line 433
    :cond_17
    const-string v19, "UpdateCheckThread"

    const-string v21, "send RETURNCODE_ERROR"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const-string v19, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "(mConfig.getAppCount() : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    const-string v19, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "mID.size() : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    const-string v19, "RETURNCODE_ERROR"

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    goto :goto_b

    .line 420
    .end local v4    # "e":Ljava/io/IOException;
    .end local v9    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v19

    move-object/from16 v20, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_1a

    .line 421
    const-wide/16 v14, 0x0

    .line 422
    .restart local v14    # "sum":D
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v6, v0, :cond_19

    .line 424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Boolean;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_18

    .line 425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Double;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v22

    add-double v14, v14, v22

    .line 422
    :cond_18
    add-int/lit8 v6, v6, 0x1

    goto :goto_c

    .line 428
    :cond_19
    const-string v19, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "all package sizes : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14, v15}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->setTotalSize(D)V

    .line 430
    const-string v19, "APK_SIZE"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v14, v15}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;D)V

    .line 431
    const-string v19, "DOWNLOAD_STATUS"

    const/16 v21, 0x6

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    .line 436
    .end local v6    # "i":I
    .end local v14    # "sum":D
    :goto_d
    throw v20

    .line 433
    :cond_1a
    const-string v19, "UpdateCheckThread"

    const-string v21, "send RETURNCODE_ERROR"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const-string v19, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "(mConfig.getAppCount() : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    const-string v19, "UpdateCheckThread"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "mID.size() : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    const-string v19, "RETURNCODE_ERROR"

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    goto :goto_d
.end method

.method private sendMessage(Ljava/lang/String;D)V
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "arg"    # D

    .prologue
    .line 658
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 659
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x0

    iput v2, v1, Landroid/os/Message;->what:I

    .line 660
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 661
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "MESSAGE_CMD"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    const-string v2, "APK_SIZE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 664
    const-string v2, "APK_SIZE"

    invoke-virtual {v0, v2, p2, p3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 666
    :cond_0
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 667
    iget-boolean v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mFlagCancel:Z

    if-nez v2, :cond_1

    .line 668
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 669
    :cond_1
    return-void
.end method

.method private sendMessage(Ljava/lang/String;I)V
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "arg"    # I

    .prologue
    .line 636
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 637
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x0

    iput v2, v1, Landroid/os/Message;->what:I

    .line 638
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 639
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "MESSAGE_CMD"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    const-string v2, "DOWNLOAD_STATUS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 642
    const-string v2, "DOWNLOAD_STATUS"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 643
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->MESSAGE_ID:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mID:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 644
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->MESSAGE_RESULT:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mResult:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 645
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->MESSAGE_DATA_URI:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mDownloadUri:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 652
    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 653
    iget-boolean v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mFlagCancel:Z

    if-nez v2, :cond_1

    .line 654
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 655
    :cond_1
    return-void

    .line 646
    :cond_2
    const-string v2, "DOWNLOA_RATIO"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 647
    const-string v2, "DOWNLOA_RATIO"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 648
    :cond_3
    const-string v2, "RETURNCODE_ERROR"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 649
    const-string v2, "RETURNCODE_ERROR"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private sendMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "arg"    # Ljava/lang/String;

    .prologue
    .line 672
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 673
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x0

    iput v2, v1, Landroid/os/Message;->what:I

    .line 674
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 675
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "MESSAGE_CMD"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    const-string v2, "VERSION_CODE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 678
    const-string v2, "VERSION_CODE"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    :cond_0
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 681
    iget-boolean v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mFlagCancel:Z

    if-nez v2, :cond_1

    .line 682
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 683
    :cond_1
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 632
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mFlagCancel:Z

    .line 633
    return-void
.end method

.method public run()V
    .locals 4

    .prologue
    .line 127
    const-string v2, "460"

    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMCC:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "03"

    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->szMNC:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 128
    const-string v2, "UpdateCheckThread"

    const-string v3, "UpdateCheckThread : CTC network "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mCTCServerUrl:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->getUrlForCTC(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mUpdateUriCTC:Ljava/lang/String;

    .line 131
    .local v0, "server_url":Ljava/lang/String;
    const-string v2, "UpdateCheckThread"

    const-string v3, "UpdateCheckThread : CTC get URL OK!! "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->makeMultipleUpdateCheckURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 142
    .local v1, "server_url2":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/sec/kidsplat/installer/UpdateCheckThread;->doMultipleCheckUpdate(Ljava/lang/String;)Z

    .line 143
    return-void

    .line 133
    .end local v0    # "server_url":Ljava/lang/String;
    .end local v1    # "server_url2":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mCheckServerUrl:Ljava/lang/String;

    .line 134
    .restart local v0    # "server_url":Ljava/lang/String;
    const-string v2, "UpdateCheckThread"

    const-string v3, "UpdateCheckThread : CTC get URL fail!! "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 137
    .end local v0    # "server_url":Ljava/lang/String;
    :cond_1
    const-string v2, "UpdateCheckThread"

    const-string v3, "UpdateCheckThread : no CTC "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    iget-object v0, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mCheckServerUrl:Ljava/lang/String;

    .restart local v0    # "server_url":Ljava/lang/String;
    goto :goto_0
.end method

.method public verifyUpdatePackage(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "versionCode"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 618
    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateCheckThread;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 620
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {v2, p1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 621
    .local v1, "info":Landroid/content/pm/PackageInfo;
    const-string v4, "UpdateCheckThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", installed package versionCode : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", package versionCode in server : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    iget v4, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-lt v4, v5, :cond_0

    .line 629
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :goto_0
    return v3

    .line 625
    :catch_0
    move-exception v0

    .line 626
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "UpdateCheckThread"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Package not found : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    const-string v3, "UpdateCheckThread"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", need to be updated"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    const/4 v3, 0x1

    goto :goto_0
.end method
