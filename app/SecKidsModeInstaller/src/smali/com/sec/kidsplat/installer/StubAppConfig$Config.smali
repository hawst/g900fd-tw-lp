.class Lcom/sec/kidsplat/installer/StubAppConfig$Config;
.super Ljava/lang/Object;
.source "StubAppConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/kidsplat/installer/StubAppConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Config"
.end annotation


# static fields
.field private static final DEFAULT_IMAGE_CNT:I = 0x3


# instance fields
.field private appInfoImageNb:I

.field private appName:Ljava/lang/String;

.field private appNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private checkCtcUrl:Ljava/lang/String;

.field private checkSizeAppUrl:Ljava/lang/String;

.field private downloadUrl:Ljava/lang/String;

.field public mIsNeedUpdateKidsMode:Z

.field public mIsUpdateMode:Z

.field public needToBeUpdatedAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private packageName:Ljava/lang/String;

.field private packageNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public packageSizeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private signature:Ljava/lang/String;

.field private storeStignature:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/kidsplat/installer/StubAppConfig;

.field private totalSize:D

.field public updateAvailable:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/sec/kidsplat/installer/StubAppConfig;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 56
    iput-object p1, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->this$0:Lcom/sec/kidsplat/installer/StubAppConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->appInfoImageNb:I

    .line 64
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->totalSize:D

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageNameList:Ljava/util/List;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->appNameList:Ljava/util/List;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->updateAvailable:Ljava/util/List;

    .line 71
    iput-boolean v2, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsUpdateMode:Z

    .line 72
    iput-boolean v2, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsNeedUpdateKidsMode:Z

    return-void
.end method


# virtual methods
.method public getAppCount()I
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageNameList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAppInfoImageCnt()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->appInfoImageNb:I

    return v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->appName:Ljava/lang/String;

    return-object v0
.end method

.method public getAppNameList(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->appNameList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 219
    const-string v0, "KidsPlatformStub"

    const-string v1, "getAppNameList(), index is wrong"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/4 v0, 0x0

    .line 222
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->appNameList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getCheckCtcUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->checkCtcUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getCheckSizeAppUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->checkSizeAppUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getDownloadUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->downloadUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageIndex(Ljava/lang/String;)I
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 163
    iget-object v1, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageNameList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 164
    .local v0, "index":I
    const-string v1, "KidsPlatformStub"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPackageIndex() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageNameList(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageNameList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 210
    const-string v0, "KidsPlatformStub"

    const-string v1, "getPackageNameList(), index is wrong"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const/4 v0, 0x0

    .line 214
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageNameList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->signature:Ljava/lang/String;

    return-object v0
.end method

.method public getStoreStignature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->storeStignature:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalSize()D
    .locals 2

    .prologue
    .line 135
    iget-wide v0, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->totalSize:D

    return-wide v0
.end method

.method public hasPackageInInstallList(Ljava/lang/String;)Z
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 226
    const/4 v0, 0x1

    .line 228
    .local v0, "hasPkg":Z
    iget-object v1, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageNameList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 229
    const-string v1, "KidsPlatformStub"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hasPackageInInstallList return false : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    const/4 v0, 0x0

    .line 233
    :cond_0
    return v0
.end method

.method public setAppInfoImageCnt(I)V
    .locals 0
    .param p1, "nb"    # I

    .prologue
    .line 107
    iput p1, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->appInfoImageNb:I

    .line 108
    return-void
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->appName:Ljava/lang/String;

    .line 148
    return-void
.end method

.method public setCheckCtcUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "checkCtcUrl"    # Ljava/lang/String;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->checkCtcUrl:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public setCheckSizeAppUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "checkSizeAppUrl"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->checkSizeAppUrl:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public setDownloadUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "downloadUrl"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->downloadUrl:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageName:Ljava/lang/String;

    .line 155
    return-void
.end method

.method public setPackageNameList(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 169
    const-string v2, "KidsPlatformStub"

    const-string v3, "setPackageNameList()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    invoke-static {p1}, Lcom/sec/kidsplat/installer/StubAppConfig;->isVoiceCapable(Landroid/content/Context;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 174
    .local v1, "isCallAvailable":Ljava/lang/Boolean;
    const-string v2, "KidsPlatformStub"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isCallAvailable() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    const-string v2, "CHM"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 177
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 178
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageNameList:Ljava/util/List;

    .line 179
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->appNameList:Ljava/util/List;

    .line 180
    const-string v2, "KidsPlatformStub"

    const-string v3, "setPackageNameList() : call nokidsapps"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageNameList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 202
    iget-object v2, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    iget-object v2, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    iget-object v2, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->updateAvailable:Ljava/util/List;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 182
    .end local v0    # "i":I
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageNameList:Ljava/util/List;

    .line 183
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070007

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->appNameList:Ljava/util/List;

    .line 184
    const-string v2, "KidsPlatformStub"

    const-string v3, "setPackageNameList() : nocall nokidsapps"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 188
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 189
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f070000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageNameList:Ljava/util/List;

    .line 190
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->appNameList:Ljava/util/List;

    .line 191
    const-string v2, "KidsPlatformStub"

    const-string v3, "setPackageNameList() : call"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 193
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageNameList:Ljava/util/List;

    .line 194
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->appNameList:Ljava/util/List;

    .line 195
    const-string v2, "KidsPlatformStub"

    const-string v3, "setPackageNameList() : nocall"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 206
    .restart local v0    # "i":I
    :cond_3
    return-void
.end method

.method public setSignature(Ljava/lang/String;)V
    .locals 0
    .param p1, "signature"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->signature:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public setStoreStignature(Ljava/lang/String;)V
    .locals 0
    .param p1, "storeStignature"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->storeStignature:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public setTotalSize(D)V
    .locals 1
    .param p1, "size"    # D

    .prologue
    .line 131
    iput-wide p1, p0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->totalSize:D

    .line 132
    return-void
.end method
