.class Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;
.super Landroid/database/ContentObserver;
.source "KidsModeUpdateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;


# direct methods
.method constructor <init>(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 8
    .param p1, "selfChange"    # Z

    .prologue
    const-wide/16 v4, 0x1f4

    const/4 v7, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 50
    const-string v2, "KidsModeUpdateService "

    const-string v3, "onChange () "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iget-object v2, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    iget-object v3, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    invoke-virtual {v3}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v6, "kids_home_mode"

    invoke-static {v3, v6, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v0, :cond_0

    :goto_0
    # setter for: Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->isKidsMode:Z
    invoke-static {v2, v0}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->access$102(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;Z)Z

    .line 52
    const-string v0, "KidsModeUpdateService "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isKidsMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    # getter for: Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->isKidsMode:Z
    invoke-static {v2}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->access$100(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    iget-object v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    # getter for: Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->isKidsMode:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->access$100(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    iget-object v1, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    # getter for: Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->timer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->access$200(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->stopTimer(Ljava/util/Timer;)V

    .line 55
    iget-object v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    # getter for: Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->intent_timer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->access$400(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;)Ljava/util/Timer;

    move-result-object v0

    new-instance v1, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer_intent;

    iget-object v2, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    invoke-direct {v1, v2, v7}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer_intent;-><init>(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;)V

    const-wide/16 v2, 0x64

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 59
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 51
    goto :goto_0

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    # getter for: Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->timer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->access$200(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;)Ljava/util/Timer;

    move-result-object v0

    new-instance v1, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer;

    iget-object v2, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    invoke-direct {v1, v2, v7}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer;-><init>(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;)V

    const-wide/16 v2, 0x0

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    goto :goto_1
.end method
