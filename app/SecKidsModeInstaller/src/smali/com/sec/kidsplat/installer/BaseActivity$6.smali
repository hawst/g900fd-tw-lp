.class Lcom/sec/kidsplat/installer/BaseActivity$6;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/kidsplat/installer/BaseActivity;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/kidsplat/installer/BaseActivity;


# direct methods
.method constructor <init>(Lcom/sec/kidsplat/installer/BaseActivity;)V
    .locals 0

    .prologue
    .line 540
    iput-object p1, p0, Lcom/sec/kidsplat/installer/BaseActivity$6;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x1

    .line 543
    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$6;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->userChoice:Z
    invoke-static {v1, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3802(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    .line 544
    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$6;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->isChinaModel()Z
    invoke-static {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$3900(Lcom/sec/kidsplat/installer/BaseActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 545
    const-string v1, "IS_CHINA_USING_DIALOG_SHOW"

    iget-object v2, p0, Lcom/sec/kidsplat/installer/BaseActivity$6;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 546
    .local v0, "prefchina":Ljava/lang/String;
    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 547
    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$6;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isChinaUsingPopupDone:Z
    invoke-static {v1, v3}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4002(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    .line 551
    :goto_0
    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$6;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$800(Lcom/sec/kidsplat/installer/BaseActivity;)Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->isNetworkConnected(Landroid/content/Context;)Z
    invoke-static {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$900(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$6;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # getter for: Lcom/sec/kidsplat/installer/BaseActivity;->isChinaUsingPopupDone:Z
    invoke-static {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4000(Lcom/sec/kidsplat/installer/BaseActivity;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 552
    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$6;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->showDataUsingDialogForChina()V
    invoke-static {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4100(Lcom/sec/kidsplat/installer/BaseActivity;)V

    .line 559
    .end local v0    # "prefchina":Ljava/lang/String;
    :goto_1
    return-void

    .line 549
    .restart local v0    # "prefchina":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$6;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    const/4 v2, 0x0

    # setter for: Lcom/sec/kidsplat/installer/BaseActivity;->isChinaUsingPopupDone:Z
    invoke-static {v1, v2}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4002(Lcom/sec/kidsplat/installer/BaseActivity;Z)Z

    goto :goto_0

    .line 554
    :cond_1
    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$6;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->startUpdateThread()V
    invoke-static {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4200(Lcom/sec/kidsplat/installer/BaseActivity;)V

    goto :goto_1

    .line 556
    .end local v0    # "prefchina":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/sec/kidsplat/installer/BaseActivity$6;->this$0:Lcom/sec/kidsplat/installer/BaseActivity;

    # invokes: Lcom/sec/kidsplat/installer/BaseActivity;->startUpdateThread()V
    invoke-static {v1}, Lcom/sec/kidsplat/installer/BaseActivity;->access$4200(Lcom/sec/kidsplat/installer/BaseActivity;)V

    goto :goto_1
.end method
