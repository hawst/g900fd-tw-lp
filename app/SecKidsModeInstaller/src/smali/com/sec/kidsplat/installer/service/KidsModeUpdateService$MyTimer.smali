.class Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer;
.super Ljava/util/TimerTask;
.source "KidsModeUpdateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyTimer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;


# direct methods
.method private constructor <init>(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;
    .param p2, "x1"    # Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$1;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer;-><init>(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 114
    const-string v0, "KidsModeUpdateService "

    const-string v1, "kidsmode_service_timer run"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    # getter for: Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->isKidsMode:Z
    invoke-static {v0}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->access$100(Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    iget-object v1, p0, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService$MyTimer;->this$0:Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/kidsplat/installer/service/KidsModeUpdateService;->exitKidsMode(Landroid/content/Context;)V

    .line 117
    :cond_0
    return-void
.end method
