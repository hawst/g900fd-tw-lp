.class public Lcom/sec/kidsplat/installer/UpdateDownloadTask;
.super Landroid/os/AsyncTask;
.source "UpdateDownloadTask.java"

# interfaces
.implements Lcom/sec/kidsplat/installer/OnInstalledPackaged;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;",
        "Lcom/sec/kidsplat/installer/OnInstalledPackaged;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "UpdateDownloadTask"

.field private static mCurrentIndex:I

.field private static mFirstDownload:Z

.field private static mInstallResult:[I

.field private static mRequestInstallation:[Z

.field private static final normalMode:Landroid/content/ComponentName;


# instance fields
.field private MESSAGE_DATA_URI:Ljava/lang/String;

.field private MESSAGE_ID:Ljava/lang/String;

.field private MESSAGE_RESULT:Ljava/lang/String;

.field private _HFileUtil:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/kidsplat/installer/HFileUtil;",
            ">;"
        }
    .end annotation
.end field

.field private _Handler:Landroid/os/Handler;

.field private mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

.field private mContext:Landroid/content/Context;

.field private mCurrentLauncher:Landroid/content/ComponentName;

.field private mDownloadSize:D

.field private mDownloadUri:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFlagCancel:Z

.field private mHandler:Landroid/os/Handler;

.field private mID:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mResult:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStartFromFirst:Z

.field private mTotalSize:D


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 38
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mFirstDownload:Z

    .line 59
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.sec.android.app.launcher"

    const-string v2, "com.android.launcher2.Launcher"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->normalMode:Landroid/content/ComponentName;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Boolean;Lcom/sec/kidsplat/installer/StubAppConfig$Config;D)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p6, "startFromFirst"    # Ljava/lang/Boolean;
    .param p7, "config"    # Lcom/sec/kidsplat/installer/StubAppConfig$Config;
    .param p8, "totalSize"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/os/Handler;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/sec/kidsplat/installer/StubAppConfig$Config;",
            "D)V"
        }
    .end annotation

    .prologue
    .local p3, "mDataID":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, "mDataResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p5, "mDataUri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 61
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 39
    const-string v1, "id of the message"

    iput-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->MESSAGE_ID:Ljava/lang/String;

    .line 40
    const-string v1, "result of the message"

    iput-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->MESSAGE_RESULT:Ljava/lang/String;

    .line 41
    const-string v1, "uri of the message"

    iput-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->MESSAGE_DATA_URI:Ljava/lang/String;

    .line 42
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_Handler:Landroid/os/Handler;

    .line 44
    iput-boolean v4, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mStartFromFirst:Z

    .line 45
    iput-boolean v4, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mFlagCancel:Z

    .line 46
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mID:Ljava/util/ArrayList;

    .line 48
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadUri:Ljava/util/ArrayList;

    .line 49
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mResult:Ljava/util/ArrayList;

    .line 58
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentLauncher:Landroid/content/ComponentName;

    .line 62
    const-string v1, "UpdateDownloadTask"

    const-string v2, "UpdateDownloadTask()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iput-object p1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mContext:Landroid/content/Context;

    .line 64
    iput-object p7, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    .line 65
    iput-wide p8, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mTotalSize:D

    .line 66
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    .line 67
    sput v4, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentIndex:I

    .line 68
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v1

    new-array v1, v1, [Z

    sput-object v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mRequestInstallation:[Z

    .line 69
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v1

    new-array v1, v1, [I

    sput-object v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mInstallResult:[I

    .line 70
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 71
    sget-object v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mRequestInstallation:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    .line 72
    sget-object v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mInstallResult:[I

    const/4 v2, -0x1

    aput v2, v1, v0

    .line 73
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/kidsplat/installer/HFileUtil;

    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v3, v0}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppNameList(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/sec/kidsplat/installer/HFileUtil;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/HFileUtil;->prepare()Z

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_0
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->deleteDifferentSizeFile()V

    .line 77
    iput-object p2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mHandler:Landroid/os/Handler;

    .line 78
    iput-object p3, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mID:Ljava/util/ArrayList;

    .line 79
    iput-object p5, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadUri:Ljava/util/ArrayList;

    .line 80
    iput-object p4, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mResult:Ljava/util/ArrayList;

    .line 81
    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mStartFromFirst:Z

    .line 82
    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->saveCurrentLauncherComponent()V

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/StubAppConfig$Config;
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/kidsplat/installer/UpdateDownloadTask;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/kidsplat/installer/UpdateDownloadTask;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->installApk(I)Z

    move-result v0

    return v0
.end method

.method private checkApkSignature(Ljava/io/File;)Z
    .locals 3
    .param p1, "f"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 585
    new-instance v0, Lcom/sec/kidsplat/installer/SigChecker;

    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/kidsplat/installer/SigChecker;-><init>(Landroid/content/Context;)V

    .line 586
    .local v0, "c":Lcom/sec/kidsplat/installer/SigChecker;
    const-string v1, "UpdateDownloadTask"

    const-string v2, "checkApkSignature()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v2}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getSignature()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/kidsplat/installer/SigChecker;->validate(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v2}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getStoreStignature()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/kidsplat/installer/SigChecker;->validate(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 589
    :cond_0
    const/4 v1, 0x1

    .line 592
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private checkPackageNameList()Z
    .locals 4

    .prologue
    .line 108
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 109
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v1, v0}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getPackageNameList(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mID:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 110
    const-string v2, "UpdateDownloadTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkPackageNameList fail, mConfig.getPackageNameList(i) : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v3, v0}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getPackageNameList(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", mID.get(i) : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mID:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const/4 v1, 0x0

    .line 114
    :goto_1
    return v1

    .line 108
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private completeKidsPlatformInstall()V
    .locals 2

    .prologue
    .line 144
    const-string v0, "UpdateDownloadTask"

    const-string v1, "completeKidsPlatformInstall()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->setCurrentLauncher()V

    .line 152
    const-string v0, "UpdateDownloadTask"

    const-string v1, "completeKidsPlatformInstall() : STATUS_DOWNLOADED_ALL_COMPLETED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    const-string v0, "DOWNLOAD_STATUS"

    const/16 v1, 0x8

    invoke-direct {p0, v0, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    .line 154
    return-void
.end method

.method private deleteDifferentSizeFile()V
    .locals 6

    .prologue
    .line 118
    const-string v1, "UpdateDownloadTask"

    const-string v2, "deleteDifferentSizeFile"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 120
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-object v1, v1, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/HFileUtil;->length()J

    move-result-wide v4

    long-to-double v4, v4

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/HFileUtil;->delete()Z

    .line 119
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 124
    :cond_0
    const-string v2, "UpdateDownloadTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "server and local file size is same : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/HFileUtil;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 127
    :cond_1
    return-void
.end method

.method private downloadApk(I)Z
    .locals 22
    .param p1, "index"    # I

    .prologue
    .line 227
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-boolean v13, v13, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsUpdateMode:Z

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-object v13, v13, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    if-nez v13, :cond_1

    .line 228
    const-string v18, "UpdateDownloadTask"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "downloadApk() : skip download "

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mID:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v19, " because lastest version has already installed"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v18

    invoke-static {v0, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const/4 v13, 0x1

    .line 315
    :cond_0
    :goto_0
    return v13

    .line 231
    :cond_1
    const-string v18, "UpdateDownloadTask"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "downloadApk(), index : "

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v19, ", package name : "

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mID:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v19, ", resultCode : "

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mResult:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v18

    invoke-static {v0, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    const-string v18, "UpdateDownloadTask"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "URI :  "

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadUri:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v18

    invoke-static {v0, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    if-nez p1, :cond_2

    .line 234
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    .line 235
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->checkPackageNameList()Z

    move-result v13

    if-eqz v13, :cond_b

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mResult:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    const-string v18, "1"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_b

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadUri:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    const-string v18, ""

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_b

    .line 236
    const/4 v8, 0x0

    .line 237
    .local v8, "file":Ljava/io/File;
    const/4 v5, 0x0

    .line 238
    .local v5, "InStream":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 239
    .local v4, "Fout":Ljava/io/FileOutputStream;
    new-instance v10, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v10}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 240
    .local v10, "httpclient":Lorg/apache/http/client/HttpClient;
    new-instance v9, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v9}, Lorg/apache/http/client/methods/HttpGet;-><init>()V

    .line 241
    .local v9, "httpGet":Lorg/apache/http/client/methods/HttpGet;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v13}, Lcom/sec/kidsplat/installer/HFileUtil;->getFile()Ljava/io/File;

    move-result-object v8

    .line 242
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 243
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mStartFromFirst:Z

    if-eqz v13, :cond_4

    .line 244
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    .line 245
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v13}, Lcom/sec/kidsplat/installer/HFileUtil;->getFile()Ljava/io/File;

    move-result-object v8

    .line 246
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mStartFromFirst:Z

    .line 253
    :cond_3
    :goto_1
    const-wide/16 v16, 0x0

    .line 254
    .local v16, "sizeTotal":D
    :try_start_0
    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v14

    .line 255
    .local v14, "sizeDownload":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    move-wide/from16 v18, v0

    long-to-double v0, v14

    move-wide/from16 v20, v0

    add-double v18, v18, v20

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    .line 256
    new-instance v18, Ljava/net/URI;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadUri:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v0, v13}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Lorg/apache/http/client/methods/HttpGet;->setURI(Ljava/net/URI;)V

    .line 257
    invoke-interface {v10, v9}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v12

    .line 258
    .local v12, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v13

    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v5

    .line 259
    sget-boolean v13, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mFirstDownload:Z

    if-eqz v13, :cond_5

    .line 260
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v13

    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v16, v0

    .line 261
    const-string v13, "UpdateDownloadTask"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "1st sizeTotal : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v13, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v13}, Lcom/sec/kidsplat/installer/HFileUtil;->getFileName()Ljava/lang/String;

    move-result-object v13

    const v19, 0x8001

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v13, v1}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v4

    .line 270
    const/16 v13, 0x400

    new-array v6, v13, [B

    .line 271
    .local v6, "buffer":[B
    const/4 v11, 0x0

    .line 272
    .local v11, "len1":I
    const-string v13, "DOWNLOAD_STATUS"

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v13, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    .line 273
    :goto_3
    invoke-virtual {v5, v6}, Ljava/io/InputStream;->read([B)I

    move-result v11

    if-lez v11, :cond_9

    .line 274
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mFlagCancel:Z

    if-eqz v13, :cond_6

    .line 275
    const-string v13, "UpdateDownloadTask"

    const-string v18, "CANCELLED(mFlagCancel)"

    move-object/from16 v0, v18

    invoke-static {v13, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279
    const/4 v13, 0x0

    .line 298
    if-eqz v4, :cond_0

    .line 299
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 300
    :catch_0
    move-exception v7

    .line 301
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 248
    .end local v6    # "buffer":[B
    .end local v7    # "e":Ljava/io/IOException;
    .end local v11    # "len1":I
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "sizeDownload":J
    .end local v16    # "sizeTotal":D
    :cond_4
    const-string v13, "Range"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "bytes="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "-"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v9, v13, v0}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const/4 v13, 0x0

    sput-boolean v13, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mFirstDownload:Z

    goto/16 :goto_1

    .line 263
    .restart local v12    # "response":Lorg/apache/http/HttpResponse;
    .restart local v14    # "sizeDownload":J
    .restart local v16    # "sizeTotal":D
    :cond_5
    :try_start_2
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v13

    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v18

    add-long v18, v18, v14

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v16, v0

    .line 264
    const-string v13, "UpdateDownloadTask"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "sizeTotal : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " sizeDownload : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v13, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 290
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "sizeDownload":J
    :catch_1
    move-exception v7

    .line 291
    .local v7, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 292
    const-string v13, "UpdateDownloadTask"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Download fail - "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v13, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    const-string v13, "DOWNLOAD_STATUS"

    const/16 v18, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v13, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 295
    const/4 v13, 0x0

    .line 298
    if-eqz v4, :cond_0

    .line 299
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 300
    :catch_2
    move-exception v7

    .line 301
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 281
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v6    # "buffer":[B
    .restart local v11    # "len1":I
    .restart local v12    # "response":Lorg/apache/http/HttpResponse;
    .restart local v14    # "sizeDownload":J
    :cond_6
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->isCancelled()Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v13

    if-eqz v13, :cond_7

    .line 282
    const/4 v13, 0x0

    .line 298
    if-eqz v4, :cond_0

    .line 299
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    .line 300
    :catch_3
    move-exception v7

    .line 301
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 283
    .end local v7    # "e":Ljava/io/IOException;
    :cond_7
    const/4 v13, 0x0

    :try_start_7
    invoke-virtual {v4, v6, v13, v11}, Ljava/io/FileOutputStream;->write([BII)V

    .line 284
    int-to-long v0, v11

    move-wide/from16 v18, v0

    add-long v14, v14, v18

    .line 285
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    move-wide/from16 v18, v0

    int-to-double v0, v11

    move-wide/from16 v20, v0

    add-double v18, v18, v20

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    .line 286
    const-string v13, "DOWNLOA_RATIO"

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x4059000000000000L    # 100.0

    mul-double v18, v18, v20

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mTotalSize:D

    move-wide/from16 v20, v0

    div-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v13, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_3

    .line 297
    .end local v6    # "buffer":[B
    .end local v11    # "len1":I
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "sizeDownload":J
    :catchall_0
    move-exception v13

    .line 298
    if-eqz v4, :cond_8

    .line 299
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 302
    :cond_8
    :goto_4
    throw v13

    .line 289
    .restart local v6    # "buffer":[B
    .restart local v11    # "len1":I
    .restart local v12    # "response":Lorg/apache/http/HttpResponse;
    .restart local v14    # "sizeDownload":J
    :cond_9
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 298
    if-eqz v4, :cond_a

    .line 299
    :try_start_a
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 310
    :cond_a
    :goto_5
    const-string v13, "UpdateDownloadTask"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Download complete, index : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v13, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    const-string v13, "DOWNLOAD_STATUS"

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v13, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    .line 312
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 300
    :catch_4
    move-exception v7

    .line 301
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 300
    .end local v6    # "buffer":[B
    .end local v7    # "e":Ljava/io/IOException;
    .end local v11    # "len1":I
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "sizeDownload":J
    :catch_5
    move-exception v7

    .line 301
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 314
    .end local v4    # "Fout":Ljava/io/FileOutputStream;
    .end local v5    # "InStream":Ljava/io/InputStream;
    .end local v7    # "e":Ljava/io/IOException;
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .end local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v16    # "sizeTotal":D
    :cond_b
    const-string v13, "UpdateDownloadTask"

    const-string v18, "Download condition is fail"

    move-object/from16 v0, v18

    invoke-static {v13, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method private downloadApkNoProxy(I)Z
    .locals 26
    .param p1, "index"    # I

    .prologue
    .line 318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsUpdateMode:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Boolean;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v21

    if-nez v21, :cond_1

    .line 319
    const-string v21, "UpdateDownloadTask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "downloadApkNoProxy() : skip download "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " because lastest version has already installed"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    const/16 v21, 0x1

    .line 421
    :cond_0
    :goto_0
    return v21

    .line 322
    :cond_1
    const-string v22, "UpdateDownloadTask"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "downloadApkNoProxy(), index : "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v23, ", resultCode : "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mResult:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    if-nez p1, :cond_2

    .line 324
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    .line 325
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->checkPackageNameList()Z

    move-result v21

    if-eqz v21, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mResult:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    const-string v22, "1"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadUri:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    const-string v22, ""

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_d

    .line 326
    const/4 v9, 0x0

    .line 327
    .local v9, "file":Ljava/io/File;
    const/4 v5, 0x0

    .line 328
    .local v5, "InStream":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 329
    .local v4, "Fout":Ljava/io/FileOutputStream;
    new-instance v12, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v12}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 330
    .local v12, "httpclient":Lorg/apache/http/client/HttpClient;
    new-instance v10, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v10}, Lorg/apache/http/client/methods/HttpGet;-><init>()V

    .line 332
    .local v10, "httpGet":Lorg/apache/http/client/methods/HttpGet;
    const/16 v15, 0x78

    .line 333
    .local v15, "timeout":I
    invoke-interface {v12}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v11

    .line 334
    .local v11, "httpParams":Lorg/apache/http/params/HttpParams;
    const v21, 0x1d4c0

    move/from16 v0, v21

    invoke-static {v11, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 335
    const v21, 0x1d4c0

    move/from16 v0, v21

    invoke-static {v11, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 336
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual/range {v21 .. v21}, Lcom/sec/kidsplat/installer/HFileUtil;->getFile()Ljava/io/File;

    move-result-object v9

    .line 337
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_3

    .line 338
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mStartFromFirst:Z

    move/from16 v21, v0

    if-eqz v21, :cond_5

    .line 339
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual/range {v21 .. v21}, Lcom/sec/kidsplat/installer/HFileUtil;->getFile()Ljava/io/File;

    move-result-object v9

    .line 341
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mStartFromFirst:Z

    .line 349
    :cond_3
    :goto_1
    const-wide/16 v18, 0x0

    .line 350
    .local v18, "sizeTotal":D
    :try_start_0
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v16

    .line 351
    .local v16, "sizeDownload":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    move-wide/from16 v22, v0

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v24, v0

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    .line 352
    new-instance v20, Ljava/net/URL;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadUri:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 353
    .local v20, "url":Ljava/net/URL;
    sget-object v21, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    invoke-virtual/range {v20 .. v21}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v7

    check-cast v7, Ljava/net/HttpURLConnection;

    .line 355
    .local v7, "conn":Ljava/net/HttpURLConnection;
    const-string v21, "UpdateDownloadTask"

    const-string v22, "HttpURLConnection() : no proxy set"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    if-eqz v7, :cond_4

    .line 357
    const v21, 0x1d4c0

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 358
    const v21, 0x1d4c0

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 359
    const-string v21, "GET"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 360
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 361
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v14

    .line 362
    .local v14, "response":I
    const-string v21, "UpdateDownloadTask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "response : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 364
    sget-boolean v21, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mFirstDownload:Z

    if-eqz v21, :cond_6

    .line 365
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v21

    move/from16 v0, v21

    int-to-double v0, v0

    move-wide/from16 v18, v0

    .line 366
    const-string v21, "UpdateDownloadTask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "1st sizeTotal : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    .end local v14    # "response":I
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual/range {v21 .. v21}, Lcom/sec/kidsplat/installer/HFileUtil;->getFileName()Ljava/lang/String;

    move-result-object v21

    const v23, 0x8001

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v4

    .line 375
    const/16 v21, 0x400

    move/from16 v0, v21

    new-array v6, v0, [B

    .line 376
    .local v6, "buffer":[B
    const/4 v13, 0x0

    .line 377
    .local v13, "len1":I
    const-string v21, "DOWNLOAD_STATUS"

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    .line 378
    :goto_3
    invoke-virtual {v5, v6}, Ljava/io/InputStream;->read([B)I

    move-result v13

    if-lez v13, :cond_a

    .line 379
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mFlagCancel:Z

    move/from16 v21, v0

    if-eqz v21, :cond_7

    .line 380
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383
    const/16 v21, 0x0

    .line 404
    if-eqz v4, :cond_0

    .line 405
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 406
    :catch_0
    move-exception v8

    .line 407
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 343
    .end local v6    # "buffer":[B
    .end local v7    # "conn":Ljava/net/HttpURLConnection;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v13    # "len1":I
    .end local v16    # "sizeDownload":J
    .end local v18    # "sizeTotal":D
    .end local v20    # "url":Ljava/net/URL;
    :cond_5
    const-string v21, "Range"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "bytes="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "-"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v10, v0, v1}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const-string v21, "UpdateDownloadTask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "file length() : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    const/16 v21, 0x0

    sput-boolean v21, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mFirstDownload:Z

    goto/16 :goto_1

    .line 368
    .restart local v7    # "conn":Ljava/net/HttpURLConnection;
    .restart local v14    # "response":I
    .restart local v16    # "sizeDownload":J
    .restart local v18    # "sizeTotal":D
    .restart local v20    # "url":Ljava/net/URL;
    :cond_6
    :try_start_2
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v21

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v22, v22, v16

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v18, v0

    .line 369
    const-string v21, "UpdateDownloadTask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "sizeTotal : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " sizeDownload : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 396
    .end local v7    # "conn":Ljava/net/HttpURLConnection;
    .end local v14    # "response":I
    .end local v16    # "sizeDownload":J
    .end local v20    # "url":Ljava/net/URL;
    :catch_1
    move-exception v8

    .line 397
    .local v8, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 398
    const-string v21, "UpdateDownloadTask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Download fail - "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    const-string v21, "DOWNLOAD_STATUS"

    const/16 v22, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 401
    const/16 v21, 0x0

    .line 404
    if-eqz v4, :cond_0

    .line 405
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 406
    :catch_2
    move-exception v8

    .line 407
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 385
    .end local v8    # "e":Ljava/io/IOException;
    .restart local v6    # "buffer":[B
    .restart local v7    # "conn":Ljava/net/HttpURLConnection;
    .restart local v13    # "len1":I
    .restart local v16    # "sizeDownload":J
    .restart local v20    # "url":Ljava/net/URL;
    :cond_7
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->isCancelled()Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v21

    if-eqz v21, :cond_8

    .line 386
    const/16 v21, 0x0

    .line 404
    if-eqz v4, :cond_0

    .line 405
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    .line 406
    :catch_3
    move-exception v8

    .line 407
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 387
    .end local v8    # "e":Ljava/io/IOException;
    :cond_8
    const/16 v21, 0x0

    :try_start_7
    move/from16 v0, v21

    invoke-virtual {v4, v6, v0, v13}, Ljava/io/FileOutputStream;->write([BII)V

    .line 388
    int-to-long v0, v13

    move-wide/from16 v22, v0

    add-long v16, v16, v22

    .line 389
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    move-wide/from16 v22, v0

    int-to-double v0, v13

    move-wide/from16 v24, v0

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    .line 390
    const-string v21, "DOWNLOA_RATIO"

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    move-wide/from16 v22, v0

    const-wide/high16 v24, 0x4059000000000000L    # 100.0

    mul-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mTotalSize:D

    move-wide/from16 v24, v0

    div-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_3

    .line 403
    .end local v6    # "buffer":[B
    .end local v7    # "conn":Ljava/net/HttpURLConnection;
    .end local v13    # "len1":I
    .end local v16    # "sizeDownload":J
    .end local v20    # "url":Ljava/net/URL;
    :catchall_0
    move-exception v21

    .line 404
    if-eqz v4, :cond_9

    .line 405
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 408
    :cond_9
    :goto_4
    throw v21

    .line 393
    .restart local v6    # "buffer":[B
    .restart local v7    # "conn":Ljava/net/HttpURLConnection;
    .restart local v13    # "len1":I
    .restart local v16    # "sizeDownload":J
    .restart local v20    # "url":Ljava/net/URL;
    :cond_a
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 394
    if-eqz v7, :cond_b

    .line 395
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 404
    :cond_b
    if-eqz v4, :cond_c

    .line 405
    :try_start_a
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 416
    :cond_c
    :goto_5
    const-string v21, "UpdateDownloadTask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "downloadApkNoProxy complete, index : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    const-string v21, "DOWNLOAD_STATUS"

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    .line 418
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 406
    :catch_4
    move-exception v8

    .line 407
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 406
    .end local v6    # "buffer":[B
    .end local v7    # "conn":Ljava/net/HttpURLConnection;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v13    # "len1":I
    .end local v16    # "sizeDownload":J
    .end local v20    # "url":Ljava/net/URL;
    :catch_5
    move-exception v8

    .line 407
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 420
    .end local v4    # "Fout":Ljava/io/FileOutputStream;
    .end local v5    # "InStream":Ljava/io/InputStream;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v9    # "file":Ljava/io/File;
    .end local v10    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .end local v11    # "httpParams":Lorg/apache/http/params/HttpParams;
    .end local v12    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v15    # "timeout":I
    .end local v18    # "sizeTotal":D
    :cond_d
    const-string v21, "UpdateDownloadTask"

    const-string v22, "downloadApkNoProxy condition is fail"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    const/16 v21, 0x0

    goto/16 :goto_0
.end method

.method private installApk(I)Z
    .locals 9
    .param p1, "index"    # I

    .prologue
    .line 424
    const-string v7, "UpdateDownloadTask"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "installApk("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v6, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v6}, Lcom/sec/kidsplat/installer/HFileUtil;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    new-instance v4, Ljava/io/File;

    iget-object v6, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v6}, Lcom/sec/kidsplat/installer/HFileUtil;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 430
    .local v4, "folder":Ljava/io/File;
    const-string v6, "DOWNLOAD_STATUS"

    const/4 v7, 0x2

    invoke-direct {p0, v6, v7}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    .line 431
    const/4 v0, 0x0

    .line 432
    .local v0, "bIsSuccess":Z
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    .line 433
    const-string v6, "DOWNLOAD_STATUS"

    const/4 v7, 0x5

    invoke-direct {p0, v6, v7}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    move v1, v0

    .line 489
    .end local v0    # "bIsSuccess":Z
    .local v1, "bIsSuccess":I
    :goto_0
    return v1

    .line 438
    .end local v1    # "bIsSuccess":I
    .restart local v0    # "bIsSuccess":Z
    :cond_0
    :try_start_0
    invoke-direct {p0, v4}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->checkApkSignature(Ljava/io/File;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 439
    const-string v6, "UpdateDownloadTask"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " :: checkSignature fail"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    const-string v6, "DOWNLOAD_STATUS"

    const/16 v7, 0x9

    invoke-direct {p0, v6, v7}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    move v1, v0

    .line 441
    .restart local v1    # "bIsSuccess":I
    goto :goto_0

    .line 444
    .end local v1    # "bIsSuccess":I
    :cond_1
    const-string v6, "UpdateDownloadTask"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " :: checkSignature  success"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 451
    :goto_1
    :try_start_1
    new-instance v5, Lcom/sec/kidsplat/installer/ApplicationManager;

    iget-object v6, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/sec/kidsplat/installer/ApplicationManager;-><init>(Landroid/content/Context;)V

    .line 452
    .local v5, "mgr":Lcom/sec/kidsplat/installer/ApplicationManager;
    invoke-virtual {v5, p0}, Lcom/sec/kidsplat/installer/ApplicationManager;->setOnInstalledPackaged(Lcom/sec/kidsplat/installer/OnInstalledPackaged;)V

    .line 453
    sput p1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentIndex:I
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_4

    .line 471
    :try_start_2
    sget-object v6, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mRequestInstallation:[Z

    aget-boolean v6, v6, p1

    if-eqz v6, :cond_2

    .line 472
    sget-object v6, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mRequestInstallation:[Z

    const/4 v7, 0x0

    aput-boolean v7, v6, p1

    .line 473
    const-string v7, "temp"

    iget-object v6, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v6}, Lcom/sec/kidsplat/installer/HFileUtil;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    iget-object v6, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v6}, Lcom/sec/kidsplat/installer/HFileUtil;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/kidsplat/installer/ApplicationManager;->installPackage(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_4

    .line 483
    :cond_2
    :goto_2
    const/4 v0, 0x1

    .end local v5    # "mgr":Lcom/sec/kidsplat/installer/ApplicationManager;
    :goto_3
    move v1, v0

    .line 489
    .restart local v1    # "bIsSuccess":I
    goto/16 :goto_0

    .line 446
    .end local v1    # "bIsSuccess":I
    :catch_0
    move-exception v3

    .line 448
    .local v3, "e1":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 476
    .end local v3    # "e1":Ljava/io/IOException;
    .restart local v5    # "mgr":Lcom/sec/kidsplat/installer/ApplicationManager;
    :catch_1
    move-exception v2

    .line 477
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_2

    .line 484
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .end local v5    # "mgr":Lcom/sec/kidsplat/installer/ApplicationManager;
    :catch_2
    move-exception v2

    .line 485
    .local v2, "e":Ljava/lang/SecurityException;
    invoke-virtual {v2}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_3

    .line 478
    .end local v2    # "e":Ljava/lang/SecurityException;
    .restart local v5    # "mgr":Lcom/sec/kidsplat/installer/ApplicationManager;
    :catch_3
    move-exception v2

    .line 479
    .local v2, "e":Ljava/lang/IllegalAccessException;
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_2

    .line 486
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    .end local v5    # "mgr":Lcom/sec/kidsplat/installer/ApplicationManager;
    :catch_4
    move-exception v2

    .line 487
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_3

    .line 480
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v5    # "mgr":Lcom/sec/kidsplat/installer/ApplicationManager;
    :catch_5
    move-exception v2

    .line 481
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_2
.end method

.method private isInstallComplete()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 130
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mInstallResult:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 131
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-boolean v1, v1, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsUpdateMode:Z

    if-eqz v1, :cond_0

    .line 132
    sget-object v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mInstallResult:[I

    aget v1, v1, v0

    if-eq v1, v3, :cond_1

    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-object v1, v1, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    .line 140
    :goto_1
    return v1

    .line 135
    :cond_0
    sget-object v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mInstallResult:[I

    aget v1, v1, v0

    if-eq v1, v3, :cond_1

    move v1, v2

    .line 136
    goto :goto_1

    .line 130
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    :cond_2
    const-string v1, "UpdateDownloadTask"

    const-string v2, "isInstallComplete returns true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v3

    .line 140
    goto :goto_1
.end method

.method private sendMessage(Ljava/lang/String;I)V
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "arg"    # I

    .prologue
    .line 88
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mHandler:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 106
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 91
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x0

    iput v2, v1, Landroid/os/Message;->what:I

    .line 92
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 93
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "MESSAGE_CMD"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v2, "DOWNLOAD_STATUS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 95
    const-string v2, "DOWNLOAD_STATUS"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 96
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->MESSAGE_ID:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mID:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 97
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->MESSAGE_RESULT:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mResult:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 98
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->MESSAGE_DATA_URI:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadUri:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 104
    :cond_1
    :goto_1
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 105
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 99
    :cond_2
    const-string v2, "DOWNLOA_RATIO"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 100
    const-string v2, "DOWNLOA_RATIO"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 101
    :cond_3
    const-string v2, "RETURNCODE_ERROR"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 102
    const-string v2, "RETURNCODE_ERROR"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mFlagCancel:Z

    .line 86
    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 35
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 493
    const-string v1, "UpdateDownloadTask"

    const-string v2, "doInBackground()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 495
    iget-boolean v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mFlagCancel:Z

    if-eqz v1, :cond_1

    .line 496
    const-string v1, "UpdateDownloadTask"

    const-string v2, "download is canceled()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    :cond_0
    return-object v6

    .line 499
    :cond_1
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-object v1, v1, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/HFileUtil;->length()J

    move-result-wide v4

    long-to-double v4, v4

    cmpl-double v1, v2, v4

    if-nez v1, :cond_2

    .line 500
    const-string v2, "UpdateDownloadTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "skip download because it has already downloaded : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/HFileUtil;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    iget-wide v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-object v1, v1, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->packageSizeList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    add-double/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    .line 502
    const-string v1, "DOWNLOA_RATIO"

    iget-wide v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mDownloadSize:D

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    iget-wide v4, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mTotalSize:D

    div-double/2addr v2, v4

    double-to-int v2, v2

    invoke-direct {p0, v1, v2}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    .line 494
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 505
    :cond_2
    const-string v1, "46003"

    const-string v2, "gsm.operator.numeric"

    const-string v3, ""

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 506
    sget-object v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mRequestInstallation:[Z

    invoke-direct {p0, v0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->downloadApkNoProxy(I)Z

    move-result v2

    aput-boolean v2, v1, v0

    .line 510
    :goto_2
    const-string v1, "UpdateDownloadTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mRequestInstallation["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mRequestInstallation:[Z

    aget-boolean v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mStartFromFirst:Z

    goto :goto_1

    .line 508
    :cond_3
    sget-object v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mRequestInstallation:[Z

    invoke-direct {p0, v0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->downloadApk(I)Z

    move-result v2

    aput-boolean v2, v1, v0

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 35
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 517
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 518
    invoke-virtual {p0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 519
    sget-object v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mRequestInstallation:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_Handler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask$1;

    invoke-direct {v1, p0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask$1;-><init>(Lcom/sec/kidsplat/installer/UpdateDownloadTask;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 549
    :cond_0
    return-void
.end method

.method public packageInstalled(Ljava/lang/String;I)V
    .locals 5
    .param p1, "pkgname"    # Ljava/lang/String;
    .param p2, "returncode"    # I

    .prologue
    const/4 v4, 0x1

    .line 552
    const-string v1, "UpdateDownloadTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " returnCode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    if-eq p2, v4, :cond_1

    .line 554
    sget-object v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mInstallResult:[I

    sget v2, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentIndex:I

    const/4 v3, 0x0

    aput v3, v1, v2

    .line 555
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    sget v2, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/HFileUtil;->delete()Z

    .line 556
    const-string v1, "RETURNCODE_ERROR"

    invoke-direct {p0, v1, p2}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    .line 581
    :cond_0
    :goto_0
    return-void

    .line 558
    :cond_1
    sget-object v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mInstallResult:[I

    sget v2, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentIndex:I

    aput v4, v1, v2

    .line 559
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->_HFileUtil:Ljava/util/ArrayList;

    sget v2, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/kidsplat/installer/HFileUtil;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/HFileUtil;->delete()Z

    .line 560
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->isInstallComplete()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 561
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->completeKidsPlatformInstall()V

    goto :goto_0

    .line 564
    :cond_2
    sget v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentIndex:I

    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v2}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 565
    sget v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentIndex:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentIndex:I

    .line 566
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-boolean v1, v1, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsUpdateMode:Z

    if-eqz v1, :cond_3

    .line 567
    sget v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentIndex:I

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    invoke-virtual {v1}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 568
    iget-object v1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    iget-object v1, v1, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, v4, :cond_4

    .line 569
    sput v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentIndex:I

    .line 574
    .end local v0    # "i":I
    :cond_3
    sget v1, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentIndex:I

    invoke-direct {p0, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->installApk(I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 575
    const-string v1, "UpdateDownloadTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    sget v4, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentIndex:I

    invoke-virtual {v3, v4}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppNameList(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " install is successed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 567
    .restart local v0    # "i":I
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 577
    .end local v0    # "i":I
    :cond_5
    const-string v1, "UpdateDownloadTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    sget v4, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentIndex:I

    invoke-virtual {v3, v4}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppNameList(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " install is failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public saveCurrentLauncherComponent()V
    .locals 8

    .prologue
    .line 157
    :try_start_0
    iget-object v5, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 158
    .local v4, "pm":Landroid/content/pm/PackageManager;
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 159
    .local v3, "homeIntent":Landroid/content/Intent;
    const-string v5, "android.intent.category.HOME"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 161
    .local v2, "homeApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-virtual {v4, v2}, Landroid/content/pm/PackageManager;->getHomeActivities(Ljava/util/List;)Landroid/content/ComponentName;

    move-result-object v0

    .line 163
    .local v0, "currentDefaultHome":Landroid/content/ComponentName;
    const-string v5, "UpdateDownloadTask"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "currentDefaultHome : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    if-eqz v0, :cond_0

    .line 165
    iput-object v0, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentLauncher:Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    .end local v0    # "currentDefaultHome":Landroid/content/ComponentName;
    .end local v2    # "homeApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v3    # "homeIntent":Landroid/content/Intent;
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    :goto_0
    return-void

    .line 167
    :catch_0
    move-exception v1

    .line 168
    .local v1, "e":Ljava/lang/NoSuchMethodError;
    const-string v5, "TAG"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NoSuchMethodException "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setCurrentLauncher()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentLauncher:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mCurrentLauncher:Landroid/content/ComponentName;

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->setPreferredActivity(Landroid/content/ComponentName;)V

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_0
    sget-object v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->normalMode:Landroid/content/ComponentName;

    invoke-virtual {p0, v0}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->setPreferredActivity(Landroid/content/ComponentName;)V

    goto :goto_0
.end method

.method setPreferredActivity(Landroid/content/ComponentName;)V
    .locals 21
    .param p1, "newHomeMode"    # Landroid/content/ComponentName;

    .prologue
    .line 179
    new-instance v11, Landroid/content/Intent;

    const-string v18, "android.intent.action.MAIN"

    move-object/from16 v0, v18

    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 180
    .local v11, "intent":Landroid/content/Intent;
    const-string v18, "android.intent.category.HOME"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    .line 182
    .local v13, "pm":Landroid/content/pm/PackageManager;
    const v18, 0x10040

    move/from16 v0, v18

    invoke-virtual {v13, v11, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v17

    .line 183
    .local v17, "rList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v5, 0x0

    .line 184
    .local v5, "defaultHomeComp":Landroid/content/ComponentName;
    const/4 v6, 0x0

    .line 185
    .local v6, "defaultHomeMatch":I
    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    .line 186
    .local v9, "filter":Landroid/content/IntentFilter;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 187
    .local v15, "prefActList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ComponentName;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 188
    .local v4, "defaultHome":Ljava/lang/String;
    const-string v18, "UpdateDownloadTask"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "setPreferredActivity :: defaultHome:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    const/4 v2, 0x0

    .line 190
    .local v2, "comp":Landroid/content/ComponentName;
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/content/pm/ResolveInfo;

    .line 192
    .local v16, "r":Landroid/content/pm/ResolveInfo;
    :try_start_0
    move-object/from16 v0, v16

    iget v0, v0, Landroid/content/pm/ResolveInfo;->priority:I

    move/from16 v18, v0

    if-gtz v18, :cond_0

    .line 194
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    move/from16 v18, v0

    if-eqz v18, :cond_0

    .line 196
    new-instance v3, Landroid/content/ComponentName;

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v3, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2

    .line 197
    .end local v2    # "comp":Landroid/content/ComponentName;
    .local v3, "comp":Landroid/content/ComponentName;
    :try_start_1
    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V

    .line 199
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 200
    move-object v5, v3

    .line 201
    move-object/from16 v0, v16

    iget v6, v0, Landroid/content/pm/ResolveInfo;->match:I

    .line 202
    move-object/from16 v0, v16

    iget-object v9, v0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    .line 203
    const-string v18, "UpdateDownloadTask"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "ReolveInfo : - defaultHome"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    move-object v2, v3

    .line 211
    .end local v3    # "comp":Landroid/content/ComponentName;
    .restart local v2    # "comp":Landroid/content/ComponentName;
    goto/16 :goto_0

    .line 206
    .end local v2    # "comp":Landroid/content/ComponentName;
    .restart local v3    # "comp":Landroid/content/ComponentName;
    :cond_1
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V

    .line 207
    const-string v18, "UpdateDownloadTask"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "ReolveInfo : - cleared"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 209
    :catch_0
    move-exception v8

    move-object v2, v3

    .line 210
    .end local v3    # "comp":Landroid/content/ComponentName;
    .restart local v2    # "comp":Landroid/content/ComponentName;
    .local v8, "ex":Ljava/lang/SecurityException;
    :goto_2
    invoke-virtual {v8}, Ljava/lang/SecurityException;->printStackTrace()V

    goto/16 :goto_0

    .line 213
    .end local v8    # "ex":Ljava/lang/SecurityException;
    .end local v16    # "r":Landroid/content/pm/ResolveInfo;
    :cond_2
    if-eqz v5, :cond_4

    .line 214
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v14, v0, [Landroid/content/ComponentName;

    .line 215
    .local v14, "prefActArray":[Landroid/content/ComponentName;
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_3
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v12, v0, :cond_3

    .line 216
    invoke-virtual {v15, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/content/ComponentName;

    aput-object v18, v14, v12

    .line 215
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 220
    :cond_3
    :try_start_2
    invoke-virtual {v13, v9, v6, v14, v5}, Landroid/content/pm/PackageManager;->addPreferredActivity(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_1

    .line 225
    .end local v12    # "j":I
    .end local v14    # "prefActArray":[Landroid/content/ComponentName;
    :cond_4
    :goto_4
    return-void

    .line 221
    .restart local v12    # "j":I
    .restart local v14    # "prefActArray":[Landroid/content/ComponentName;
    :catch_1
    move-exception v7

    .line 222
    .local v7, "e":Ljava/lang/SecurityException;
    const-string v18, "UpdateDownloadTask"

    const-string v19, "security exception"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 209
    .end local v7    # "e":Ljava/lang/SecurityException;
    .end local v12    # "j":I
    .end local v14    # "prefActArray":[Landroid/content/ComponentName;
    .restart local v16    # "r":Landroid/content/pm/ResolveInfo;
    :catch_2
    move-exception v8

    goto :goto_2
.end method
