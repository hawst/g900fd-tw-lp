.class public Lcom/sec/kidsplat/installer/HFileUtil;
.super Ljava/lang/Object;
.source "HFileUtil.java"


# instance fields
.field private _AbsolutePath:Ljava/lang/String;

.field private _Context:Landroid/content/Context;

.field private _File:Ljava/io/File;

.field private _FileName:Ljava/lang/String;

.field private _bPrepared:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_bPrepared:Z

    .line 20
    iput-object p1, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_Context:Landroid/content/Context;

    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".apk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_FileName:Ljava/lang/String;

    .line 22
    return-void
.end method

.method private createDir(Ljava/lang/String;)Z
    .locals 3
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 45
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 46
    .local v0, "root":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v1

    .line 50
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 54
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private createInternalStorageFile(Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 58
    iget-object v2, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_Context:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "absolutePath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 60
    .local v1, "ret":Ljava/io/File;
    return-object v1
.end method


# virtual methods
.method public delete()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_bPrepared:Z

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    .line 40
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public exists()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_bPrepared:Z

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    .line 94
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAbsolutePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_AbsolutePath:Ljava/lang/String;

    return-object v0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_File:Ljava/io/File;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_FileName:Ljava/lang/String;

    return-object v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_bPrepared:Z

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 103
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public prepare()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 65
    iget-boolean v1, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_bPrepared:Z

    if-eqz v1, :cond_0

    .line 81
    :goto_0
    return v0

    .line 69
    :cond_0
    iget-object v1, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/kidsplat/installer/HFileUtil;->createDir(Ljava/lang/String;)Z

    .line 71
    iget-object v1, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_FileName:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/kidsplat/installer/HFileUtil;->createInternalStorageFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_File:Ljava/io/File;

    .line 73
    iget-object v1, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_File:Ljava/io/File;

    if-nez v1, :cond_1

    .line 74
    const/4 v0, 0x0

    goto :goto_0

    .line 77
    :cond_1
    iget-object v1, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_AbsolutePath:Ljava/lang/String;

    .line 79
    iput-boolean v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_bPrepared:Z

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    iput-object v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_Context:Landroid/content/Context;

    .line 30
    iput-object v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_File:Ljava/io/File;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/kidsplat/installer/HFileUtil;->_bPrepared:Z

    .line 32
    return-void
.end method
