.class Lcom/sec/kidsplat/installer/UpdateDownloadTask$1;
.super Ljava/lang/Object;
.source "UpdateDownloadTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/kidsplat/installer/UpdateDownloadTask;->onPostExecute(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/kidsplat/installer/UpdateDownloadTask;


# direct methods
.method constructor <init>(Lcom/sec/kidsplat/installer/UpdateDownloadTask;)V
    .locals 0

    .prologue
    .line 520
    iput-object p1, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask$1;->this$0:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 524
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask$1;->this$0:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    # getter for: Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;
    invoke-static {v2}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->access$000(Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-result-object v2

    iget-boolean v2, v2, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->mIsUpdateMode:Z

    if-nez v2, :cond_1

    .line 525
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask$1;->this$0:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    # invokes: Lcom/sec/kidsplat/installer/UpdateDownloadTask;->installApk(I)Z
    invoke-static {v2, v5}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->access$100(Lcom/sec/kidsplat/installer/UpdateDownloadTask;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 526
    const-string v2, "UpdateDownloadTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask$1;->this$0:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    # getter for: Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;
    invoke-static {v4}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->access$000(Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppNameList(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " install is successed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    :goto_0
    return-void

    .line 528
    :cond_0
    const-string v2, "UpdateDownloadTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask$1;->this$0:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    # getter for: Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;
    invoke-static {v4}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->access$000(Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppNameList(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " install is failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 531
    :cond_1
    const/4 v1, 0x0

    .line 532
    .local v1, "startIndex":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask$1;->this$0:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    # getter for: Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;
    invoke-static {v2}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->access$000(Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 533
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask$1;->this$0:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    # getter for: Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;
    invoke-static {v2}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->access$000(Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->needToBeUpdatedAppList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 534
    move v1, v0

    .line 538
    :cond_2
    iget-object v2, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask$1;->this$0:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    # invokes: Lcom/sec/kidsplat/installer/UpdateDownloadTask;->installApk(I)Z
    invoke-static {v2, v1}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->access$100(Lcom/sec/kidsplat/installer/UpdateDownloadTask;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 539
    const-string v2, "UpdateDownloadTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask$1;->this$0:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    # getter for: Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;
    invoke-static {v4}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->access$000(Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppNameList(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " install is successed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 532
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 541
    :cond_4
    const-string v2, "UpdateDownloadTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/kidsplat/installer/UpdateDownloadTask$1;->this$0:Lcom/sec/kidsplat/installer/UpdateDownloadTask;

    # getter for: Lcom/sec/kidsplat/installer/UpdateDownloadTask;->mConfig:Lcom/sec/kidsplat/installer/StubAppConfig$Config;
    invoke-static {v4}, Lcom/sec/kidsplat/installer/UpdateDownloadTask;->access$000(Lcom/sec/kidsplat/installer/UpdateDownloadTask;)Lcom/sec/kidsplat/installer/StubAppConfig$Config;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/kidsplat/installer/StubAppConfig$Config;->getAppNameList(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " install is failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
