.class public Lcom/sec/kidsplat/installer/receiver/KidsModeUpdateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "KidsModeUpdateReceiver.java"


# static fields
.field private static final ACTION_KIDS_MODE_CHANGE:Ljava/lang/String; = "com.android.launcher.action.KIDS_HOME_MODE_CHANGE"

.field private static final KIDS_MODE_CHANGE_PERMISSION:Ljava/lang/String; = "com.sec.kidsplat.parentalcontrol.broadcast.KIDS_HOME_MODE_CHANGE_PERMISSION"

.field private static final MINIMUM_VERSION:I = 0x72934d2

.field private static final TAG:Ljava/lang/String; = "KidsModeUpdateReceiver "

.field private static final VERSIONCODE_L:I = 0x15

.field private static kidsHomeVersion:I

.field static update:Z


# instance fields
.field cdTimer:Landroid/os/CountDownTimer;

.field mSettingContentObserver:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    sput v0, Lcom/sec/kidsplat/installer/receiver/KidsModeUpdateReceiver;->kidsHomeVersion:I

    .line 25
    sput-boolean v0, Lcom/sec/kidsplat/installer/receiver/KidsModeUpdateReceiver;->update:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private needToUpdate()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 58
    const-string v1, "KidsModeUpdateReceiver "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "needToUpdate() : kidsHomeVersion : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/kidsplat/installer/receiver/KidsModeUpdateReceiver;->kidsHomeVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    sget v1, Lcom/sec/kidsplat/installer/receiver/KidsModeUpdateReceiver;->kidsHomeVersion:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 64
    :goto_0
    return v0

    .line 62
    :cond_0
    const-string v1, "KidsModeUpdateReceiver "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Build.VERSION.SDK_INT : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ne v1, v2, :cond_1

    sget v1, Lcom/sec/kidsplat/installer/receiver/KidsModeUpdateReceiver;->kidsHomeVersion:I

    const v2, 0x72934d2

    if-ge v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    sput-boolean v0, Lcom/sec/kidsplat/installer/receiver/KidsModeUpdateReceiver;->update:Z

    goto :goto_0
.end method

.method private versionCheck(Landroid/content/Context;)I
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 70
    .local v3, "pm":Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/content/pm/PackageInfo;

    invoke-direct {v2}, Landroid/content/pm/PackageInfo;-><init>()V

    .line 71
    .local v2, "pi":Landroid/content/pm/PackageInfo;
    const-string v1, "com.sec.android.app.kidshome"

    .line 72
    .local v1, "packageName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 75
    .local v4, "version":I
    const/16 v5, 0x80

    :try_start_0
    invoke-virtual {v3, v1, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 76
    const-string v5, "KidsModeUpdateReceiver "

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "versionCheck() : kidsHomeVersion : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    iget v4, v2, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :goto_0
    return v4

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v4, -0x1

    .line 80
    const-string v5, "KidsModeUpdateReceiver "

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Package not found : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public exitKidsMode(ZLandroid/content/Context;)V
    .locals 4
    .param p1, "isLockScreen"    # Z
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 91
    const-string v1, "KidsModeUpdateReceiver "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exitkidsmode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.launcher.action.KIDS_HOME_MODE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 95
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "kidsmode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 97
    const-string v1, "kidsmode_from"

    const-string v2, "KidsHome"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    invoke-virtual {p2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 100
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    .line 35
    move-object v1, p1

    .line 37
    .local v1, "mContext":Landroid/content/Context;
    move-object v0, p2

    .line 38
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "KidsModeUpdateReceiver "

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "kidsmodeupdate : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    invoke-direct {p0, v1}, Lcom/sec/kidsplat/installer/receiver/KidsModeUpdateReceiver;->versionCheck(Landroid/content/Context;)I

    move-result v3

    sput v3, Lcom/sec/kidsplat/installer/receiver/KidsModeUpdateReceiver;->kidsHomeVersion:I

    .line 41
    const-string v3, "KidsModeUpdateReceiver "

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "kidsHomeVersion : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/sec/kidsplat/installer/receiver/KidsModeUpdateReceiver;->kidsHomeVersion:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    invoke-direct {p0}, Lcom/sec/kidsplat/installer/receiver/KidsModeUpdateReceiver;->needToUpdate()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 44
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 45
    .local v2, "serviceIntent":Landroid/content/Intent;
    const-string v3, "com.sec.kidsplat.installer"

    const-string v4, "com.sec.kidsplat.installer.service.KidsModeUpdateService"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 48
    const-string v3, "KidsModeUpdateReceiver "

    const-string v4, "kidsmode_service_start"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    const-string v3, "com.sec.kidsplat.installer.START_UPDATE_SERVICE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 54
    .end local v2    # "serviceIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
