.class public final Lcom/google/a/a/a/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/ads/b/c;
.implements Lcom/google/android/gms/ads/b/e;


# instance fields
.field private a:Lcom/google/android/gms/ads/AdView;

.field private b:Lcom/google/android/gms/ads/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/ads/b/a;Landroid/os/Bundle;Landroid/os/Bundle;)Lcom/google/android/gms/ads/b;
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v3, Lcom/google/android/gms/ads/c;

    invoke-direct {v3}, Lcom/google/android/gms/ads/c;-><init>()V

    invoke-interface {p1}, Lcom/google/android/gms/ads/b/a;->a()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v4, v3, Lcom/google/android/gms/ads/c;->a:Lcom/google/android/gms/internal/bs;

    iput-object v0, v4, Lcom/google/android/gms/internal/bs;->e:Ljava/util/Date;

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/ads/b/a;->b()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v4, v3, Lcom/google/android/gms/ads/c;->a:Lcom/google/android/gms/internal/bs;

    iput v0, v4, Lcom/google/android/gms/internal/bs;->f:I

    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/ads/b/a;->c()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v5, v3, Lcom/google/android/gms/ads/c;->a:Lcom/google/android/gms/internal/bs;

    iget-object v5, v5, Lcom/google/android/gms/internal/bs;->a:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Lcom/google/android/gms/ads/b/a;->d()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v4, v3, Lcom/google/android/gms/ads/c;->a:Lcom/google/android/gms/internal/bs;

    iput-object v0, v4, Lcom/google/android/gms/internal/bs;->g:Landroid/location/Location;

    :cond_3
    invoke-interface {p1}, Lcom/google/android/gms/ads/b/a;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/google/android/gms/internal/kt;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/gms/ads/c;->a:Lcom/google/android/gms/internal/bs;

    iget-object v4, v4, Lcom/google/android/gms/internal/bs;->d:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_4
    const-string v0, "tagForChildDirectedTreatment"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v4, -0x1

    if-eq v0, v4, :cond_6

    const-string v0, "tagForChildDirectedTreatment"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_1
    iget-object v4, v3, Lcom/google/android/gms/ads/c;->a:Lcom/google/android/gms/internal/bs;

    if-eqz v0, :cond_5

    move v2, v1

    :cond_5
    iput v2, v4, Lcom/google/android/gms/internal/bs;->h:I

    :cond_6
    if-eqz p2, :cond_9

    :goto_2
    const-string v0, "gw"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "mad_hac"

    const-string v2, "mad_hac"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "adJson"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "_ad"

    const-string v2, "adJson"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    const-string v0, "_noRefresh"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-class v0, Lcom/google/a/a/a/a;

    iget-object v1, v3, Lcom/google/android/gms/ads/c;->a:Lcom/google/android/gms/internal/bs;

    iget-object v1, v1, Lcom/google/android/gms/internal/bs;->b:Landroid/os/Bundle;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    new-instance v0, Lcom/google/android/gms/ads/b;

    invoke-direct {v0, v3}, Lcom/google/android/gms/ads/b;-><init>(Lcom/google/android/gms/ads/c;)V

    return-object v0

    :cond_8
    move v0, v2

    goto :goto_1

    :cond_9
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/a/a/a/a;->a:Lcom/google/android/gms/ads/AdView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/a/a/a/a;->a:Lcom/google/android/gms/ads/AdView;

    iget-object v0, v0, Lcom/google/android/gms/ads/AdView;->a:Lcom/google/android/gms/internal/bt;

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    invoke-interface {v0}, Lcom/google/android/gms/internal/bh;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iput-object v2, p0, Lcom/google/a/a/a/a;->a:Lcom/google/android/gms/ads/AdView;

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a;->b:Lcom/google/android/gms/ads/e;

    if-eqz v0, :cond_2

    iput-object v2, p0, Lcom/google/a/a/a/a;->b:Lcom/google/android/gms/ads/e;

    :cond_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/ads/b/d;Landroid/os/Bundle;Lcom/google/android/gms/ads/d;Lcom/google/android/gms/ads/b/a;Landroid/os/Bundle;)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/ads/AdView;

    invoke-direct {v0, p1}, Lcom/google/android/gms/ads/AdView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/a/a/a/a;->a:Lcom/google/android/gms/ads/AdView;

    iget-object v0, p0, Lcom/google/a/a/a/a;->a:Lcom/google/android/gms/ads/AdView;

    new-instance v1, Lcom/google/android/gms/ads/d;

    iget v2, p4, Lcom/google/android/gms/ads/d;->h:I

    iget v3, p4, Lcom/google/android/gms/ads/d;->i:I

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/ads/d;-><init>(II)V

    iget-object v0, v0, Lcom/google/android/gms/ads/AdView;->a:Lcom/google/android/gms/internal/bt;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/ads/d;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    iget-object v1, v0, Lcom/google/android/gms/internal/bt;->e:[Lcom/google/android/gms/ads/d;

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad size can only be set once on AdView."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/bt;->a([Lcom/google/android/gms/ads/d;)V

    iget-object v0, p0, Lcom/google/a/a/a/a;->a:Lcom/google/android/gms/ads/AdView;

    const-string v1, "pubid"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/ads/AdView;->a:Lcom/google/android/gms/internal/bt;

    iget-object v2, v0, Lcom/google/android/gms/internal/bt;->f:Ljava/lang/String;

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad unit ID can only be set once on AdView."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object v1, v0, Lcom/google/android/gms/internal/bt;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/a/a/a/a;->a:Lcom/google/android/gms/ads/AdView;

    new-instance v1, Lcom/google/a/a/a/b;

    invoke-direct {v1, p0, p2}, Lcom/google/a/a/a/b;-><init>(Lcom/google/a/a/a/a;Lcom/google/android/gms/ads/b/d;)V

    iget-object v0, v0, Lcom/google/android/gms/ads/AdView;->a:Lcom/google/android/gms/internal/bt;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/bt;->a(Lcom/google/android/gms/ads/a;)V

    iget-object v0, p0, Lcom/google/a/a/a/a;->a:Lcom/google/android/gms/ads/AdView;

    invoke-static {p1, p5, p6, p3}, Lcom/google/a/a/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/ads/b/a;Landroid/os/Bundle;Landroid/os/Bundle;)Lcom/google/android/gms/ads/b;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/ads/AdView;->a:Lcom/google/android/gms/internal/bt;

    iget-object v1, v1, Lcom/google/android/gms/ads/b;->b:Lcom/google/android/gms/internal/br;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/bt;->a(Lcom/google/android/gms/internal/br;)V

    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/ads/b/f;Landroid/os/Bundle;Lcom/google/android/gms/ads/b/a;Landroid/os/Bundle;)V
    .locals 3

    new-instance v0, Lcom/google/android/gms/ads/e;

    invoke-direct {v0, p1}, Lcom/google/android/gms/ads/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/a/a/a/a;->b:Lcom/google/android/gms/ads/e;

    iget-object v0, p0, Lcom/google/a/a/a/a;->b:Lcom/google/android/gms/ads/e;

    const-string v1, "pubid"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/ads/e;->a:Lcom/google/android/gms/internal/bu;

    iget-object v2, v0, Lcom/google/android/gms/internal/bu;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad unit ID can only be set once on InterstitialAd."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object v1, v0, Lcom/google/android/gms/internal/bu;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/a/a/a/a;->b:Lcom/google/android/gms/ads/e;

    new-instance v1, Lcom/google/a/a/a/c;

    invoke-direct {v1, p0, p2}, Lcom/google/a/a/a/c;-><init>(Lcom/google/a/a/a/a;Lcom/google/android/gms/ads/b/f;)V

    iget-object v0, v0, Lcom/google/android/gms/ads/e;->a:Lcom/google/android/gms/internal/bu;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/bu;->a(Lcom/google/android/gms/ads/a;)V

    iget-object v0, p0, Lcom/google/a/a/a/a;->b:Lcom/google/android/gms/ads/e;

    invoke-static {p1, p4, p5, p3}, Lcom/google/a/a/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/ads/b/a;Landroid/os/Bundle;Landroid/os/Bundle;)Lcom/google/android/gms/ads/b;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/ads/e;->a:Lcom/google/android/gms/internal/bu;

    iget-object v1, v1, Lcom/google/android/gms/ads/b;->b:Lcom/google/android/gms/internal/br;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/bu;->a(Lcom/google/android/gms/internal/br;)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a;->a:Lcom/google/android/gms/ads/AdView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/a/a/a/a;->a:Lcom/google/android/gms/ads/AdView;

    iget-object v0, v0, Lcom/google/android/gms/ads/AdView;->a:Lcom/google/android/gms/internal/bt;

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    invoke-interface {v0}, Lcom/google/android/gms/internal/bh;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a;->a:Lcom/google/android/gms/ads/AdView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/a/a/a/a;->a:Lcom/google/android/gms/ads/AdView;

    iget-object v0, v0, Lcom/google/android/gms/ads/AdView;->a:Lcom/google/android/gms/internal/bt;

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    invoke-interface {v0}, Lcom/google/android/gms/internal/bh;->g()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final d()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/a/a/a/a;->a:Lcom/google/android/gms/ads/AdView;

    return-object v0
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a;->b:Lcom/google/android/gms/ads/e;

    iget-object v0, v0, Lcom/google/android/gms/ads/e;->a:Lcom/google/android/gms/internal/bu;

    :try_start_0
    const-string v1, "show"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/bu;->a(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/internal/bu;->e:Lcom/google/android/gms/internal/bh;

    invoke-interface {v0}, Lcom/google/android/gms/internal/bh;->h()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
