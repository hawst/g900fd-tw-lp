.class public final Lcom/google/r/a/a/a/a/b;
.super Lcom/google/android/gms/internal/se;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/se",
        "<",
        "Lcom/google/r/a/a/a/a/b;",
        ">;"
    }
.end annotation


# instance fields
.field public a:[Ljava/lang/String;

.field public b:[Ljava/lang/String;

.field public c:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/se;-><init>()V

    sget-object v0, Lcom/google/android/gms/internal/sp;->d:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/internal/sp;->d:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/internal/sp;->a:[I

    iput-object v0, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/r/a/a/a/a/b;->o:Lcom/google/android/gms/internal/sf;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/r/a/a/a/a/b;->p:I

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/gms/internal/se;->a()I

    move-result v4

    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    move v2, v1

    move v3, v1

    :goto_0
    iget-object v5, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    iget-object v5, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    aget-object v5, v5, v0

    if-eqz v5, :cond_0

    add-int/lit8 v3, v3, 0x1

    invoke-static {v5}, Lcom/google/android/gms/internal/sc;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    add-int v0, v4, v2

    mul-int/lit8 v2, v3, 0x1

    add-int/2addr v0, v2

    :goto_1
    iget-object v2, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    move v4, v1

    :goto_2
    iget-object v5, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    array-length v5, v5

    if-ge v2, v5, :cond_3

    iget-object v5, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    aget-object v5, v5, v2

    if-eqz v5, :cond_2

    add-int/lit8 v4, v4, 0x1

    invoke-static {v5}, Lcom/google/android/gms/internal/sc;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    add-int/2addr v0, v3

    mul-int/lit8 v2, v4, 0x1

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v1

    :goto_3
    iget-object v3, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    array-length v3, v3

    if-ge v1, v3, :cond_6

    iget-object v3, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    aget v3, v3, v1

    if-ltz v3, :cond_5

    invoke-static {v3}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_5
    const/16 v3, 0xa

    goto :goto_4

    :cond_6
    add-int/2addr v0, v2

    iget-object v1, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_7
    return v0

    :cond_8
    move v0, v4

    goto :goto_1
.end method

.method public final synthetic a(Lcom/google/android/gms/internal/sb;)Lcom/google/android/gms/internal/sm;
    .locals 6

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/r/a/a/a/a/b;->a(Lcom/google/android/gms/internal/sb;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/sp;->a(Lcom/google/android/gms/internal/sb;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/sp;->a(Lcom/google/android/gms/internal/sb;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/sp;->a(Lcom/google/android/gms/internal/sb;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    array-length v0, v0

    goto :goto_5

    :cond_9
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/sb;->b(I)I

    move-result v3

    iget v0, p1, Lcom/google/android/gms/internal/sb;->c:I

    iget v2, p1, Lcom/google/android/gms/internal/sb;->b:I

    sub-int v4, v0, v2

    move v0, v1

    :goto_7
    iget v2, p1, Lcom/google/android/gms/internal/sb;->e:I

    const v5, 0x7fffffff

    if-ne v2, v5, :cond_a

    const/4 v2, -0x1

    :goto_8
    if-lez v2, :cond_b

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->d()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_a
    iget v2, p1, Lcom/google/android/gms/internal/sb;->c:I

    iget v5, p1, Lcom/google/android/gms/internal/sb;->e:I

    sub-int v2, v5, v2

    goto :goto_8

    :cond_b
    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/sb;->c(I)V

    iget-object v2, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    if-nez v2, :cond_d

    move v2, v1

    :goto_9
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_c

    iget-object v4, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    :goto_a
    array-length v4, v0

    if-ge v2, v4, :cond_e

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    :cond_d
    iget-object v2, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    array-length v2, v2

    goto :goto_9

    :cond_e
    iput-object v0, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    iput v3, p1, Lcom/google/android/gms/internal/sb;->e:I

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->h()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/android/gms/internal/sc;)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/android/gms/internal/sc;->a(ILjava/lang/String;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/android/gms/internal/sc;->a(ILjava/lang/String;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    array-length v0, v0

    if-lez v0, :cond_4

    :goto_2
    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    array-length v0, v0

    if-ge v1, v0, :cond_4

    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lcom/google/android/gms/internal/sc;->a(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    invoke-super {p0, p1}, Lcom/google/android/gms/internal/se;->a(Lcom/google/android/gms/internal/sc;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lcom/google/r/a/a/a/a/b;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/r/a/a/a/a/b;

    iget-object v1, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/sh;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/sh;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    iget-object v2, p1, Lcom/google/r/a/a/a/a/b;->c:[I

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/sh;->a([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/r/a/a/a/a/b;->a(Lcom/google/android/gms/internal/se;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/google/r/a/a/a/a/b;->a:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/internal/sh;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/r/a/a/a/a/b;->b:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/internal/sh;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/r/a/a/a/a/b;->c:[I

    invoke-static {v1}, Lcom/google/android/gms/internal/sh;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/r/a/a/a/a/b;->c()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
