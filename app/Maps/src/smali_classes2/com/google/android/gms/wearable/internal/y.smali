.class Lcom/google/android/gms/wearable/internal/y;
.super Lcom/google/android/gms/wearable/internal/ba;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/wearable/internal/ba",
        "<",
        "Lcom/google/android/gms/wearable/k;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:[B


# direct methods
.method constructor <init>(Lcom/google/android/gms/wearable/internal/w;Lcom/google/android/gms/common/api/o;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 0

    iput-object p3, p0, Lcom/google/android/gms/wearable/internal/y;->d:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/wearable/internal/y;->e:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/wearable/internal/y;->f:[B

    invoke-direct {p0, p2}, Lcom/google/android/gms/wearable/internal/ba;-><init>(Lcom/google/android/gms/common/api/o;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/u;
    .locals 2

    new-instance v0, Lcom/google/android/gms/wearable/internal/aa;

    const/4 v1, -0x1

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/wearable/internal/aa;-><init>(Lcom/google/android/gms/common/api/Status;I)V

    return-object v0
.end method

.method protected final synthetic b(Lcom/google/android/gms/common/api/f;)V
    .locals 5

    check-cast p1, Lcom/google/android/gms/wearable/internal/aw;

    iget-object v1, p0, Lcom/google/android/gms/wearable/internal/y;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/wearable/internal/y;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/wearable/internal/y;->f:[B

    invoke-virtual {p1}, Lcom/google/android/gms/wearable/internal/aw;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/internal/k;

    new-instance v4, Lcom/google/android/gms/wearable/internal/av;

    invoke-direct {v4, p0}, Lcom/google/android/gms/wearable/internal/av;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v4, v1, v2, v3}, Lcom/google/android/gms/wearable/internal/k;->a(Lcom/google/android/gms/wearable/internal/e;Ljava/lang/String;Ljava/lang/String;[B)V

    return-void
.end method
