.class Lcom/google/android/gms/car/w;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/m;

.field final synthetic b:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/t;Lcom/google/android/gms/car/m;Z)V
    .locals 0

    iput-object p2, p0, Lcom/google/android/gms/car/w;->a:Lcom/google/android/gms/car/m;

    iput-boolean p3, p0, Lcom/google/android/gms/car/w;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/car/w;->a:Lcom/google/android/gms/car/m;

    iget-boolean v0, p0, Lcom/google/android/gms/car/w;->b:Z

    iget-object v3, v2, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v3}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    invoke-virtual {v3}, Lcom/google/android/gms/car/em;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    invoke-virtual {v3}, Lcom/google/android/gms/car/em;->getWindow()Landroid/view/Window;

    move-result-object v3

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v3, v1, v0}, Lcom/google/android/gms/car/em;->a(Landroid/view/Window;ZZ)V

    :cond_0
    :try_start_0
    iget-object v0, v2, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    invoke-interface {v0}, Lcom/google/android/gms/car/dr;->l()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/car/m;->e()V

    goto :goto_1
.end method
