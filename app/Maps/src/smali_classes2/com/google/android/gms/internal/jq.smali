.class public final Lcom/google/android/gms/internal/jq;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field public final a:Lcom/google/android/gms/internal/fm;

.field public final b:Lcom/google/android/gms/internal/fo;

.field public final c:Lcom/google/android/gms/internal/dv;

.field public final d:Lcom/google/android/gms/internal/bd;

.field public final e:I

.field public final f:J

.field public final g:J

.field public final h:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/fm;Lcom/google/android/gms/internal/fo;Lcom/google/android/gms/internal/dv;Lcom/google/android/gms/internal/bd;IJJLorg/json/JSONObject;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/jq;->a:Lcom/google/android/gms/internal/fm;

    iput-object p2, p0, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iput-object p3, p0, Lcom/google/android/gms/internal/jq;->c:Lcom/google/android/gms/internal/dv;

    iput-object p4, p0, Lcom/google/android/gms/internal/jq;->d:Lcom/google/android/gms/internal/bd;

    iput p5, p0, Lcom/google/android/gms/internal/jq;->e:I

    iput-wide p6, p0, Lcom/google/android/gms/internal/jq;->f:J

    iput-wide p8, p0, Lcom/google/android/gms/internal/jq;->g:J

    iput-object p10, p0, Lcom/google/android/gms/internal/jq;->h:Lorg/json/JSONObject;

    return-void
.end method
