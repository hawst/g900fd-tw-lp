.class public Lcom/google/android/gms/internal/h;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/internal/j;

.field final b:Ljava/lang/Runnable;

.field c:Lcom/google/android/gms/internal/ba;

.field d:Z

.field e:Z

.field f:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/z;)V
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/j;

    sget-object v1, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/j;-><init>(Landroid/os/Handler;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/h;-><init>(Lcom/google/android/gms/internal/z;Lcom/google/android/gms/internal/j;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/internal/z;Lcom/google/android/gms/internal/j;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gms/internal/h;->d:Z

    iput-boolean v0, p0, Lcom/google/android/gms/internal/h;->e:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/internal/h;->f:J

    iput-object p2, p0, Lcom/google/android/gms/internal/h;->a:Lcom/google/android/gms/internal/j;

    new-instance v0, Lcom/google/android/gms/internal/i;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/internal/i;-><init>(Lcom/google/android/gms/internal/h;Lcom/google/android/gms/internal/z;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/h;->b:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/ba;J)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/internal/h;->d:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/internal/h;->c:Lcom/google/android/gms/internal/ba;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/h;->d:Z

    iput-wide p2, p0, Lcom/google/android/gms/internal/h;->f:J

    iget-boolean v0, p0, Lcom/google/android/gms/internal/h;->e:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Scheduling ad refresh "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " milliseconds from now."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/gms/internal/h;->a:Lcom/google/android/gms/internal/j;

    iget-object v1, p0, Lcom/google/android/gms/internal/h;->b:Ljava/lang/Runnable;

    iget-object v0, v0, Lcom/google/android/gms/internal/j;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
