.class public Lcom/google/android/gms/car/h;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/car/h;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/ey;

    invoke-direct {v0}, Lcom/google/android/gms/car/ey;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/h;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/h;->a:I

    return-void
.end method

.method public constructor <init>(IIIIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/car/h;->a:I

    iput p2, p0, Lcom/google/android/gms/car/h;->b:I

    iput p3, p0, Lcom/google/android/gms/car/h;->c:I

    iput p4, p0, Lcom/google/android/gms/car/h;->d:I

    iput p5, p0, Lcom/google/android/gms/car/h;->e:I

    iput p6, p0, Lcom/google/android/gms/car/h;->f:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/gms/car/ey;->a(Lcom/google/android/gms/car/h;Landroid/os/Parcel;)V

    return-void
.end method
