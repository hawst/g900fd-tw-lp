.class public Lcom/google/android/gms/car/ak;
.super Ljava/lang/Object;


# instance fields
.field final a:Lcom/google/android/gms/car/dd;

.field b:Lcom/google/android/gms/car/an;

.field c:Lcom/google/android/gms/car/am;

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field final i:Landroid/os/Handler;

.field private final j:Landroid/os/Handler$Callback;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/dd;Landroid/os/Looper;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/car/al;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/al;-><init>(Lcom/google/android/gms/car/ak;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ak;->j:Landroid/os/Handler$Callback;

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/ak;->j:Landroid/os/Handler$Callback;

    invoke-direct {v0, p2, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ak;->i:Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/gms/car/ak;->a:Lcom/google/android/gms/car/dd;

    return-void
.end method

.method static a(Landroid/os/RemoteException;)V
    .locals 2

    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RemoteException from car service:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ak;->a:Lcom/google/android/gms/car/dd;

    iget-object v1, p0, Lcom/google/android/gms/car/ak;->b:Lcom/google/android/gms/car/an;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/dd;->b(Lcom/google/android/gms/car/dg;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/ak;->c:Lcom/google/android/gms/car/am;

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/ak;->a(Landroid/os/RemoteException;)V

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/am;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/ak;->b:Lcom/google/android/gms/car/an;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/car/an;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/an;-><init>(Lcom/google/android/gms/car/ak;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ak;->b:Lcom/google/android/gms/car/an;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ak;->a:Lcom/google/android/gms/car/dd;

    iget-object v1, p0, Lcom/google/android/gms/car/ak;->b:Lcom/google/android/gms/car/an;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/dd;->a(Lcom/google/android/gms/car/dg;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/google/android/gms/car/ak;->c:Lcom/google/android/gms/car/am;

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/eq;->a(Ljava/lang/IllegalStateException;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/ak;->a(Landroid/os/RemoteException;)V

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0
.end method

.method public final a(I)Z
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ak;->a:Lcom/google/android/gms/car/dd;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/dd;->a(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/eq;->a(Ljava/lang/IllegalStateException;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/ak;->a(Landroid/os/RemoteException;)V

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0
.end method

.method public final a(II)Z
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ak;->a:Lcom/google/android/gms/car/dd;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/car/dd;->a(II)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/eq;->a(Ljava/lang/IllegalStateException;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/ak;->a(Landroid/os/RemoteException;)V

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0
.end method

.method public final a(ILjava/lang/String;II[BI)Z
    .locals 7

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ak;->a:Lcom/google/android/gms/car/dd;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/car/dd;->a(ILjava/lang/String;II[BI)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/eq;->a(Ljava/lang/IllegalStateException;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/ak;->a(Landroid/os/RemoteException;)V

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0
.end method
