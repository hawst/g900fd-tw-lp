.class public final Lcom/google/android/gms/internal/py;
.super Lcom/google/android/gms/common/data/f;

# interfaces
.implements Lcom/google/android/gms/people/model/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/data/f;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    return-void
.end method

.method private z()Lcom/google/android/gms/people/model/AccountMetadata;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/py;->a:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v0, v0, Lcom/google/android/gms/common/data/DataHolder;->f:Landroid/os/Bundle;

    const-string v1, "account_metadata"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v1, "account_name"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/AccountMetadata;

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    const-string v0, "_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "account_name"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    const-string v0, "page_gaia_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    const-string v0, "display_name"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "account_name"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    const-string v0, "gaia_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    const-string v0, "page_gaia_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string v0, "gaia_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string v0, "page_gaia_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/google/android/gms/internal/qa;->a:Lcom/google/android/gms/internal/qa;

    const-string v1, "avatar"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/qa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()Z
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/internal/py;->z()Lcom/google/android/gms/people/model/AccountMetadata;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const-string v2, "page_gaia_id"

    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    if-eqz v0, :cond_2

    iget-boolean v0, v1, Lcom/google/android/gms/people/model/AccountMetadata;->e:Z

    goto :goto_0

    :cond_2
    iget-boolean v0, v1, Lcom/google/android/gms/people/model/AccountMetadata;->c:Z

    goto :goto_0
.end method

.method public final k()Z
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/internal/py;->z()Lcom/google/android/gms/people/model/AccountMetadata;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const-string v2, "page_gaia_id"

    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    if-eqz v0, :cond_2

    iget-boolean v0, v1, Lcom/google/android/gms/people/model/AccountMetadata;->d:Z

    goto :goto_0

    :cond_2
    iget-boolean v0, v1, Lcom/google/android/gms/people/model/AccountMetadata;->c:Z

    goto :goto_0
.end method

.method public final l()J
    .locals 2

    const-string v0, "last_sync_start_time"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final m()J
    .locals 2

    const-string v0, "last_sync_finish_time"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final n()I
    .locals 1

    const-string v0, "last_sync_status"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final o()J
    .locals 2

    const-string v0, "last_successful_sync_time"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final p()Z
    .locals 1

    const-string v0, "sync_to_contacts"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 1

    const-string v0, "sync_circles_to_contacts"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 1

    const-string v0, "sync_evergreen_to_contacts"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final s()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "page_gaia_id"

    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/internal/py;->z()Lcom/google/android/gms/people/model/AccountMetadata;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_1

    :cond_2
    iget-boolean v0, v0, Lcom/google/android/gms/people/model/AccountMetadata;->b:Z

    goto :goto_1
.end method

.method public final t()I
    .locals 1

    const-string v0, "is_dasher"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    const-string v0, "dasher_domain"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/google/android/gms/internal/qa;->a:Lcom/google/android/gms/internal/qa;

    const-string v1, "cover_photo_url"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/qa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final w()I
    .locals 1

    const-string v0, "cover_photo_height"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final x()I
    .locals 1

    const-string v0, "cover_photo_width"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final y()Ljava/lang/String;
    .locals 1

    const-string v0, "cover_photo_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/py;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
