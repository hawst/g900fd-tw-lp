.class final Lcom/google/android/gms/feedback/c;
.super Lcom/google/android/gms/feedback/d;


# instance fields
.field final synthetic d:Landroid/graphics/Bitmap;

.field final synthetic e:Landroid/os/Bundle;

.field final synthetic f:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/o;Landroid/graphics/Bitmap;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    iput-object p2, p0, Lcom/google/android/gms/feedback/c;->d:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lcom/google/android/gms/feedback/c;->e:Landroid/os/Bundle;

    iput-object p4, p0, Lcom/google/android/gms/feedback/c;->f:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gms/feedback/d;-><init>(Lcom/google/android/gms/common/api/o;)V

    return-void
.end method


# virtual methods
.method protected final synthetic b(Lcom/google/android/gms/common/api/f;)V
    .locals 3

    check-cast p1, Lcom/google/android/gms/internal/nh;

    new-instance v0, Lcom/google/android/gms/feedback/e;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/e;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/feedback/c;->d:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/google/android/gms/feedback/e;->a:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/gms/feedback/c;->e:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/feedback/e;->c:Landroid/os/Bundle;

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/feedback/c;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/e;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/e;->a()Lcom/google/android/gms/feedback/FeedbackOptions;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/nh;->a(Lcom/google/android/gms/feedback/FeedbackOptions;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/d;->a(Lcom/google/android/gms/common/api/u;)V

    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/gms/feedback/d;->g:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/d;->a(Lcom/google/android/gms/common/api/u;)V

    goto :goto_0
.end method
