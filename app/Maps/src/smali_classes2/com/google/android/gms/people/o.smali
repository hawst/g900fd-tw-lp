.class Lcom/google/android/gms/people/o;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/m",
        "<",
        "Lcom/google/android/gms/people/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/people/n;


# direct methods
.method constructor <init>(Lcom/google/android/gms/people/n;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/people/o;->a:Lcom/google/android/gms/people/n;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Lcom/google/android/gms/people/c;

    iget-object v0, p0, Lcom/google/android/gms/people/o;->a:Lcom/google/android/gms/people/n;

    invoke-interface {p1}, Lcom/google/android/gms/people/c;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/common/a;

    iget v3, v1, Lcom/google/android/gms/common/api/Status;->g:I

    iget-object v1, v1, Lcom/google/android/gms/common/api/Status;->i:Landroid/app/PendingIntent;

    invoke-direct {v2, v3, v1}, Lcom/google/android/gms/common/a;-><init>(ILandroid/app/PendingIntent;)V

    invoke-interface {p1}, Lcom/google/android/gms/people/c;->f()Lcom/google/android/gms/people/model/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/people/n;->a(Lcom/google/android/gms/people/model/b;)V

    return-void
.end method
