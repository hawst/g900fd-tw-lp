.class public Lcom/google/android/gms/internal/oi;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lcom/google/android/gms/internal/or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/or",
            "<",
            "Lcom/google/android/gms/internal/of;",
            ">;"
        }
    .end annotation
.end field

.field b:Z

.field public c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/gms/location/f;",
            "Lcom/google/android/gms/internal/ok;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field private e:Landroid/content/ContentProviderClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/internal/or",
            "<",
            "Lcom/google/android/gms/internal/of;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/oi;->e:Landroid/content/ContentProviderClient;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/oi;->b:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/oi;->c:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/google/android/gms/internal/oi;->d:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/internal/oi;->a:Lcom/google/android/gms/internal/or;

    return-void
.end method


# virtual methods
.method public final a()Landroid/location/Location;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/oi;->a:Lcom/google/android/gms/internal/or;

    invoke-interface {v0}, Lcom/google/android/gms/internal/or;->a()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/oi;->a:Lcom/google/android/gms/internal/or;

    invoke-interface {v0}, Lcom/google/android/gms/internal/or;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/of;

    iget-object v1, p0, Lcom/google/android/gms/internal/oi;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/of;->a(Ljava/lang/String;)Landroid/location/Location;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Lcom/google/android/gms/location/f;Landroid/os/Looper;)Lcom/google/android/gms/internal/ok;
    .locals 3

    if-nez p2, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    const-string v1, "Can\'t create handler inside thread that has not called Looper.prepare()"

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/oi;->c:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/oi;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/ok;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/ok;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/internal/ok;-><init>(Lcom/google/android/gms/location/f;Landroid/os/Looper;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/internal/oi;->c:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()Lcom/google/android/gms/location/LocationStatus;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/oi;->a:Lcom/google/android/gms/internal/or;

    invoke-interface {v0}, Lcom/google/android/gms/internal/or;->a()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/oi;->a:Lcom/google/android/gms/internal/or;

    invoke-interface {v0}, Lcom/google/android/gms/internal/or;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/of;

    iget-object v1, p0, Lcom/google/android/gms/internal/oi;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/of;->b(Ljava/lang/String;)Lcom/google/android/gms/location/LocationStatus;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
