.class public Lcom/google/android/gms/location/a/a;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/location/LocationRequest;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/location/a/a;->a:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/location/a/a;->b:Z

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.location.settings.CHECK_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/location/a/a;->a:Ljava/util/ArrayList;

    const-string v2, "locationRequests"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/d;->a(Ljava/lang/Iterable;Landroid/content/Intent;Ljava/lang/String;)V

    const-string v1, "showDialog"

    iget-boolean v2, p0, Lcom/google/android/gms/location/a/a;->b:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;)Lcom/google/android/gms/location/a/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/location/a/a;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/location/a/a;->b:Z

    return-object p0
.end method
