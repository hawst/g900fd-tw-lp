.class public final Lcom/google/android/gms/internal/rx;
.super Lcom/google/android/gms/internal/se;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/se",
        "<",
        "Lcom/google/android/gms/internal/rx;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile c:[Lcom/google/android/gms/internal/rx;


# instance fields
.field public a:I

.field public b:Lcom/google/android/gms/internal/ry;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/internal/se;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/rx;->a:I

    iput-object v1, p0, Lcom/google/android/gms/internal/rx;->b:Lcom/google/android/gms/internal/ry;

    iput-object v1, p0, Lcom/google/android/gms/internal/rx;->o:Lcom/google/android/gms/internal/sf;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/rx;->p:I

    return-void
.end method

.method public static b()[Lcom/google/android/gms/internal/rx;
    .locals 2

    sget-object v0, Lcom/google/android/gms/internal/rx;->c:[Lcom/google/android/gms/internal/rx;

    if-nez v0, :cond_1

    sget-object v1, Lcom/google/android/gms/internal/sh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/internal/rx;->c:[Lcom/google/android/gms/internal/rx;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/internal/rx;

    sput-object v0, Lcom/google/android/gms/internal/rx;->c:[Lcom/google/android/gms/internal/rx;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/google/android/gms/internal/rx;->c:[Lcom/google/android/gms/internal/rx;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final a()I
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/google/android/gms/internal/se;->a()I

    move-result v1

    const/4 v0, 0x1

    iget v2, p0, Lcom/google/android/gms/internal/rx;->a:I

    invoke-static {v0, v4}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v3

    if-ltz v2, :cond_1

    invoke-static {v2}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v0

    :goto_0
    add-int/2addr v0, v3

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/internal/rx;->b:Lcom/google/android/gms/internal/ry;

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/internal/rx;->b:Lcom/google/android/gms/internal/ry;

    invoke-static {v1, v4}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v1

    invoke-virtual {v2}, Lcom/google/android/gms/internal/sm;->d()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    :cond_0
    return v0

    :cond_1
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/gms/internal/sb;)Lcom/google/android/gms/internal/sm;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/internal/rx;->a(Lcom/google/android/gms/internal/sb;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/internal/rx;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/internal/rx;->b:Lcom/google/android/gms/internal/ry;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/ry;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ry;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/rx;->b:Lcom/google/android/gms/internal/ry;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/rx;->b:Lcom/google/android/gms/internal/ry;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/sb;->a(Lcom/google/android/gms/internal/sm;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/internal/sc;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/internal/rx;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/sc;->a(II)V

    iget-object v0, p0, Lcom/google/android/gms/internal/rx;->b:Lcom/google/android/gms/internal/ry;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/rx;->b:Lcom/google/android/gms/internal/ry;

    invoke-static {v2, v2}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/gms/internal/sc;->b(I)V

    iget v1, v0, Lcom/google/android/gms/internal/sm;->p:I

    if-gez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/sm;->d()I

    :cond_0
    iget v1, v0, Lcom/google/android/gms/internal/sm;->p:I

    invoke-virtual {p1, v1}, Lcom/google/android/gms/internal/sc;->b(I)V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/sm;->a(Lcom/google/android/gms/internal/sc;)V

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/gms/internal/se;->a(Lcom/google/android/gms/internal/sc;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/internal/rx;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/gms/internal/rx;

    iget v1, p0, Lcom/google/android/gms/internal/rx;->a:I

    iget v2, p1, Lcom/google/android/gms/internal/rx;->a:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/rx;->b:Lcom/google/android/gms/internal/ry;

    if-nez v1, :cond_3

    iget-object v1, p1, Lcom/google/android/gms/internal/rx;->b:Lcom/google/android/gms/internal/ry;

    if-nez v1, :cond_0

    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/rx;->a(Lcom/google/android/gms/internal/se;)Z

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/internal/rx;->b:Lcom/google/android/gms/internal/ry;

    iget-object v2, p1, Lcom/google/android/gms/internal/rx;->b:Lcom/google/android/gms/internal/ry;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/internal/ry;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/gms/internal/rx;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/internal/rx;->b:Lcom/google/android/gms/internal/ry;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    iget v0, v0, Lcom/google/android/gms/internal/sf;->d:I

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    :cond_0
    :goto_2
    add-int v0, v2, v1

    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/rx;->b:Lcom/google/android/gms/internal/ry;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ry;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/sf;->hashCode()I

    move-result v1

    goto :goto_2
.end method
