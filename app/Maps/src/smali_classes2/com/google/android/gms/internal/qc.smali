.class final Lcom/google/android/gms/internal/qc;
.super Lcom/google/android/gms/common/internal/h;

# interfaces
.implements Lcom/google/android/gms/people/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/e",
        "<",
        "Lcom/google/android/gms/internal/pv;",
        ">.com/google/android/gms/common/internal/h<",
        "Lcom/google/android/gms/common/api/m",
        "<",
        "Lcom/google/android/gms/people/c;",
        ">;>;",
        "Lcom/google/android/gms/people/c;"
    }
.end annotation


# instance fields
.field private final c:Lcom/google/android/gms/common/api/Status;

.field private final d:Lcom/google/android/gms/people/model/b;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/qb;Lcom/google/android/gms/common/api/m;Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/people/model/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/m",
            "<",
            "Lcom/google/android/gms/people/c;",
            ">;",
            "Lcom/google/android/gms/common/api/Status;",
            "Lcom/google/android/gms/people/model/b;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/h;-><init>(Lcom/google/android/gms/common/internal/e;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/gms/internal/qc;->c:Lcom/google/android/gms/common/api/Status;

    iput-object p4, p0, Lcom/google/android/gms/internal/qc;->d:Lcom/google/android/gms/people/model/b;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/qc;->c:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/gms/common/api/m;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/qc;->d:Lcom/google/android/gms/people/model/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/qc;->d:Lcom/google/android/gms/people/model/b;

    iget-object v1, v0, Lcom/google/android/gms/common/data/a;->a:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/common/data/a;->a:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->b()V

    :cond_0
    return-void
.end method

.method protected final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/qc;->d:Lcom/google/android/gms/people/model/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/qc;->d:Lcom/google/android/gms/people/model/b;

    invoke-virtual {v0}, Lcom/google/android/gms/people/model/b;->d()V

    :cond_0
    return-void
.end method

.method public final f()Lcom/google/android/gms/people/model/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/qc;->d:Lcom/google/android/gms/people/model/b;

    return-object v0
.end method
