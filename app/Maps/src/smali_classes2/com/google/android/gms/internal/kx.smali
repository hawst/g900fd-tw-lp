.class public Lcom/google/android/gms/internal/kx;
.super Landroid/webkit/WebViewClient;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field public final a:Lcom/google/android/gms/internal/gz;

.field final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/internal/da;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/lang/Object;

.field d:Lcom/google/android/gms/internal/sq;

.field e:Lcom/google/android/gms/internal/fp;

.field f:Lcom/google/android/gms/internal/kz;

.field g:Lcom/google/android/gms/internal/cp;

.field h:Z

.field i:Lcom/google/android/gms/internal/db;

.field j:Z

.field k:Lcom/google/android/gms/internal/ft;

.field final l:Lcom/google/android/gms/internal/fg;

.field m:Lcom/google/android/gms/internal/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/gz;Z)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/internal/fg;

    invoke-virtual {p1}, Lcom/google/android/gms/internal/gz;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/internal/bw;

    invoke-virtual {p1}, Lcom/google/android/gms/internal/gz;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/internal/bw;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/internal/fg;-><init>(Lcom/google/android/gms/internal/gz;Landroid/content/Context;Lcom/google/android/gms/internal/bw;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/internal/kx;-><init>(Lcom/google/android/gms/internal/gz;ZLcom/google/android/gms/internal/fg;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/internal/gz;ZLcom/google/android/gms/internal/fg;)V
    .locals 1

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/kx;->c:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/kx;->h:Z

    iput-object p1, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    iput-boolean p2, p0, Lcom/google/android/gms/internal/kx;->j:Z

    iput-object p3, p0, Lcom/google/android/gms/internal/kx;->l:Lcom/google/android/gms/internal/fg;

    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 4

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/da;

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/google/android/gms/internal/kf;->a(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v1

    const/4 v2, 0x2

    const-string v3, "Ads"

    invoke-static {v3, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/internal/da;->a(Lcom/google/android/gms/internal/gz;Ljava/util/Map;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "No GMSG handler found for GMSG: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/dt;)V
    .locals 6

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->e()Z

    move-result v1

    new-instance v0, Lcom/google/android/gms/internal/dw;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/gz;->d()Lcom/google/android/gms/internal/bd;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/gms/internal/bd;->e:Z

    if-nez v2, :cond_0

    move-object v2, v3

    :goto_0
    if-eqz v1, :cond_1

    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/internal/kx;->k:Lcom/google/android/gms/internal/ft;

    iget-object v1, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    iget-object v5, v1, Lcom/google/android/gms/internal/gz;->e:Lcom/google/android/gms/internal/gx;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/dw;-><init>(Lcom/google/android/gms/internal/dt;Lcom/google/android/gms/internal/sq;Lcom/google/android/gms/internal/fp;Lcom/google/android/gms/internal/ft;Lcom/google/android/gms/internal/gx;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/du;->a(Landroid/content/Context;Lcom/google/android/gms/internal/dw;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->d:Lcom/google/android/gms/internal/sq;

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/internal/kx;->e:Lcom/google/android/gms/internal/fp;

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/internal/sq;Lcom/google/android/gms/internal/fp;Lcom/google/android/gms/internal/cp;Lcom/google/android/gms/internal/ft;ZLcom/google/android/gms/internal/db;Lcom/google/android/gms/internal/a;)V
    .locals 3

    if-nez p7, :cond_0

    new-instance p7, Lcom/google/android/gms/internal/a;

    const/4 v0, 0x0

    invoke-direct {p7, v0}, Lcom/google/android/gms/internal/a;-><init>(Z)V

    :cond_0
    const-string v0, "/appEvent"

    new-instance v1, Lcom/google/android/gms/internal/co;

    invoke-direct {v1, p3}, Lcom/google/android/gms/internal/co;-><init>(Lcom/google/android/gms/internal/cp;)V

    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "/canOpenURLs"

    sget-object v1, Lcom/google/android/gms/internal/cq;->b:Lcom/google/android/gms/internal/da;

    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "/canOpenIntents"

    sget-object v1, Lcom/google/android/gms/internal/cq;->c:Lcom/google/android/gms/internal/da;

    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "/click"

    sget-object v1, Lcom/google/android/gms/internal/cq;->d:Lcom/google/android/gms/internal/da;

    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "/close"

    sget-object v1, Lcom/google/android/gms/internal/cq;->e:Lcom/google/android/gms/internal/da;

    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "/customClose"

    sget-object v1, Lcom/google/android/gms/internal/cq;->f:Lcom/google/android/gms/internal/da;

    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "/httpTrack"

    sget-object v1, Lcom/google/android/gms/internal/cq;->g:Lcom/google/android/gms/internal/da;

    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "/log"

    sget-object v1, Lcom/google/android/gms/internal/cq;->h:Lcom/google/android/gms/internal/da;

    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "/open"

    new-instance v1, Lcom/google/android/gms/internal/df;

    invoke-direct {v1, p6, p7}, Lcom/google/android/gms/internal/df;-><init>(Lcom/google/android/gms/internal/db;Lcom/google/android/gms/internal/a;)V

    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "/touch"

    sget-object v1, Lcom/google/android/gms/internal/cq;->i:Lcom/google/android/gms/internal/da;

    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "/video"

    sget-object v1, Lcom/google/android/gms/internal/cq;->j:Lcom/google/android/gms/internal/da;

    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "/mraid"

    new-instance v1, Lcom/google/android/gms/internal/de;

    invoke-direct {v1}, Lcom/google/android/gms/internal/de;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gms/internal/kx;->d:Lcom/google/android/gms/internal/sq;

    iput-object p2, p0, Lcom/google/android/gms/internal/kx;->e:Lcom/google/android/gms/internal/fp;

    iput-object p3, p0, Lcom/google/android/gms/internal/kx;->g:Lcom/google/android/gms/internal/cp;

    iput-object p6, p0, Lcom/google/android/gms/internal/kx;->i:Lcom/google/android/gms/internal/db;

    iput-object p4, p0, Lcom/google/android/gms/internal/kx;->k:Lcom/google/android/gms/internal/ft;

    iput-object p7, p0, Lcom/google/android/gms/internal/kx;->m:Lcom/google/android/gms/internal/a;

    iput-boolean p5, p0, Lcom/google/android/gms/internal/kx;->h:Z

    return-void
.end method

.method public final a(ZI)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->e()Z

    move-result v1

    new-instance v0, Lcom/google/android/gms/internal/dw;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->d()Lcom/google/android/gms/internal/bd;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/internal/bd;->e:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->e:Lcom/google/android/gms/internal/fp;

    iget-object v3, p0, Lcom/google/android/gms/internal/kx;->k:Lcom/google/android/gms/internal/ft;

    iget-object v4, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    iget-object v5, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    iget-object v7, v5, Lcom/google/android/gms/internal/gz;->e:Lcom/google/android/gms/internal/gx;

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/internal/dw;-><init>(Lcom/google/android/gms/internal/sq;Lcom/google/android/gms/internal/fp;Lcom/google/android/gms/internal/ft;Lcom/google/android/gms/internal/gz;ZILcom/google/android/gms/internal/gx;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/du;->a(Landroid/content/Context;Lcom/google/android/gms/internal/dw;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/kx;->d:Lcom/google/android/gms/internal/sq;

    goto :goto_0
.end method

.method public final a(ZILjava/lang/String;)V
    .locals 11

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->e()Z

    move-result v3

    new-instance v0, Lcom/google/android/gms/internal/dw;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->d()Lcom/google/android/gms/internal/bd;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/internal/bd;->e:Z

    if-nez v1, :cond_0

    move-object v1, v2

    :goto_0
    if-eqz v3, :cond_1

    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/internal/kx;->g:Lcom/google/android/gms/internal/cp;

    iget-object v4, p0, Lcom/google/android/gms/internal/kx;->k:Lcom/google/android/gms/internal/ft;

    iget-object v5, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    iget-object v6, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    iget-object v9, v6, Lcom/google/android/gms/internal/gz;->e:Lcom/google/android/gms/internal/gx;

    iget-object v10, p0, Lcom/google/android/gms/internal/kx;->i:Lcom/google/android/gms/internal/db;

    move v6, p1

    move v7, p2

    move-object v8, p3

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/internal/dw;-><init>(Lcom/google/android/gms/internal/sq;Lcom/google/android/gms/internal/fp;Lcom/google/android/gms/internal/cp;Lcom/google/android/gms/internal/ft;Lcom/google/android/gms/internal/gz;ZILjava/lang/String;Lcom/google/android/gms/internal/gx;Lcom/google/android/gms/internal/db;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/du;->a(Landroid/content/Context;Lcom/google/android/gms/internal/dw;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/kx;->d:Lcom/google/android/gms/internal/sq;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->e:Lcom/google/android/gms/internal/fp;

    goto :goto_1
.end method

.method public final a(ZILjava/lang/String;Ljava/lang/String;)V
    .locals 12

    iget-object v0, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->e()Z

    move-result v2

    new-instance v0, Lcom/google/android/gms/internal/dw;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->d()Lcom/google/android/gms/internal/bd;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/internal/bd;->e:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    if-eqz v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/internal/kx;->g:Lcom/google/android/gms/internal/cp;

    iget-object v4, p0, Lcom/google/android/gms/internal/kx;->k:Lcom/google/android/gms/internal/ft;

    iget-object v5, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    iget-object v6, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    iget-object v10, v6, Lcom/google/android/gms/internal/gz;->e:Lcom/google/android/gms/internal/gx;

    iget-object v11, p0, Lcom/google/android/gms/internal/kx;->i:Lcom/google/android/gms/internal/db;

    move v6, p1

    move v7, p2

    move-object v8, p3

    move-object/from16 v9, p4

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/internal/dw;-><init>(Lcom/google/android/gms/internal/sq;Lcom/google/android/gms/internal/fp;Lcom/google/android/gms/internal/cp;Lcom/google/android/gms/internal/ft;Lcom/google/android/gms/internal/gz;ZILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/gx;Lcom/google/android/gms/internal/db;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/du;->a(Landroid/content/Context;Lcom/google/android/gms/internal/dw;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/kx;->d:Lcom/google/android/gms/internal/sq;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->e:Lcom/google/android/gms/internal/fp;

    goto :goto_1
.end method

.method public final a()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/kx;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/kx;->j:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Loading resource: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "gmsg"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "mobileads.google.com"

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/kx;->a(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/kx;->f:Lcom/google/android/gms/internal/kz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/kx;->f:Lcom/google/android/gms/internal/kz;

    iget-object v1, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/kz;->a(Lcom/google/android/gms/internal/gz;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/kx;->f:Lcom/google/android/gms/internal/kz;

    :cond_0
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 9

    const/4 v1, 0x0

    const/4 v8, 0x1

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "AdWebView shouldOverrideUrlLoading: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v2, "gmsg"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "mobileads.google.com"

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/kx;->a(Landroid/net/Uri;)V

    :cond_0
    :goto_0
    move v0, v8

    :goto_1
    return v0

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/gms/internal/kx;->h:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    if-ne p1, v2, :cond_4

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v4, "http"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "https"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v2, v8

    :goto_2
    if-eqz v2, :cond_4

    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/gz;->willNotDraw()Z

    move-result v2

    if-nez v2, :cond_a

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    iget-object v2, v2, Lcom/google/android/gms/internal/gz;->d:Lcom/google/android/gms/internal/ne;

    if-eqz v2, :cond_5

    invoke-virtual {v2, v0}, Lcom/google/android/gms/internal/ne;->a(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v4}, Lcom/google/android/gms/internal/gz;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lcom/google/android/gms/internal/ne;->a(Landroid/net/Uri;Landroid/content/Context;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/google/android/gms/internal/ng; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_5
    move-object v2, v0

    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/internal/kx;->m:Lcom/google/android/gms/internal/a;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/internal/kx;->m:Lcom/google/android/gms/internal/a;

    iget-boolean v4, v0, Lcom/google/android/gms/internal/a;->c:Z

    if-eqz v4, :cond_6

    iget-boolean v0, v0, Lcom/google/android/gms/internal/a;->b:Z

    if-eqz v0, :cond_8

    :cond_6
    move v0, v8

    :goto_4
    if-eqz v0, :cond_9

    :cond_7
    new-instance v0, Lcom/google/android/gms/internal/dt;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/internal/dt;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/kx;->a(Lcom/google/android/gms/internal/dt;)V

    goto :goto_0

    :catch_0
    move-exception v2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Unable to append parameter to URL: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v2, v0

    goto :goto_3

    :cond_8
    move v0, v1

    goto :goto_4

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/internal/kx;->m:Lcom/google/android/gms/internal/a;

    iget-object v1, v0, Lcom/google/android/gms/internal/a;->a:Lcom/google/android/gms/internal/b;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/internal/a;->a:Lcom/google/android/gms/internal/b;

    invoke-interface {v0, p2}, Lcom/google/android/gms/internal/b;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AdWebView unable to handle URL: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method
