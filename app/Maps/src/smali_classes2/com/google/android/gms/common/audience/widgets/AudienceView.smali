.class public final Lcom/google/android/gms/common/audience/widgets/AudienceView;
.super Landroid/widget/FrameLayout;


# static fields
.field private static a:Landroid/content/Context;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/internal/ms;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/common/audience/widgets/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-static {p1}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Landroid/content/Context;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/internal/ms;

    iput-object v0, p0, Lcom/google/android/gms/common/audience/widgets/AudienceView;->c:Lcom/google/android/gms/internal/ms;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/common/audience/widgets/AudienceView;->b:Landroid/content/Context;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/audience/widgets/AudienceView;->c:Lcom/google/android/gms/internal/ms;

    invoke-virtual {p0}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/audience/widgets/AudienceView;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/common/audience/widgets/a;

    invoke-direct {v3, p0}, Lcom/google/android/gms/common/audience/widgets/a;-><init>(Lcom/google/android/gms/common/audience/widgets/AudienceView;)V

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/internal/ms;->a(Lcom/google/android/gms/a/o;Lcom/google/android/gms/a/o;Lcom/google/android/gms/internal/mv;)V

    iget-object v0, p0, Lcom/google/android/gms/common/audience/widgets/AudienceView;->c:Lcom/google/android/gms/internal/ms;

    invoke-interface {v0}, Lcom/google/android/gms/internal/ms;->a()Lcom/google/android/gms/a/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/a/r;->a(Lcom/google/android/gms/a/o;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/internal/ms;",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/common/g;->d(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a:Landroid/content/Context;

    :cond_0
    sget-object v0, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a:Landroid/content/Context;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    :try_start_0
    const-string v1, "com.google.android.gms.plus.audience.widgets.AudienceViewImpl$DynamiteHost"

    invoke-virtual {v0, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    new-instance v1, Landroid/util/Pair;

    invoke-static {v0}, Lcom/google/android/gms/internal/mt;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/ms;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a:Landroid/content/Context;

    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_1
    :goto_1
    new-instance v0, Landroid/util/Pair;

    new-instance v1, Lcom/google/android/gms/common/audience/widgets/b;

    invoke-direct {v1}, Lcom/google/android/gms/common/audience/widgets/b;-><init>()V

    invoke-direct {v0, v1, p0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    check-cast p1, Landroid/os/Bundle;

    const-string v0, "parent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/audience/widgets/AudienceView;->c:Lcom/google/android/gms/internal/ms;

    const-string v1, "impl"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/ms;->a(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "parent"

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :try_start_0
    const-string v1, "impl"

    iget-object v2, p0, Lcom/google/android/gms/common/audience/widgets/AudienceView;->c:Lcom/google/android/gms/internal/ms;

    invoke-interface {v2}, Lcom/google/android/gms/internal/ms;->b()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method
