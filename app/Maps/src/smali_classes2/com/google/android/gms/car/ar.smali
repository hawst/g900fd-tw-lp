.class Lcom/google/android/gms/car/ar;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/aq;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/aq;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/ar;->a:Lcom/google/android/gms/car/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 4

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x1

    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/car/ar;->a:Lcom/google/android/gms/car/aq;

    iget-object v2, v0, Lcom/google/android/gms/car/aq;->b:Ljava/util/HashMap;

    monitor-enter v2

    :try_start_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/car/CarSensorEvent;

    iget-object v1, p0, Lcom/google/android/gms/car/ar;->a:Lcom/google/android/gms/car/aq;

    iget-object v1, v1, Lcom/google/android/gms/car/aq;->b:Ljava/util/HashMap;

    iget v3, v0, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/au;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/google/android/gms/car/au;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/as;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/as;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/gms/car/ar;->a:Lcom/google/android/gms/car/aq;

    iget-object v1, v1, Lcom/google/android/gms/car/aq;->b:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/car/ar;->a:Lcom/google/android/gms/car/aq;

    iget-object v2, v2, Lcom/google/android/gms/car/aq;->c:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
