.class final Lcom/google/android/gms/car/k;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/car/f;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/o;)Lcom/google/android/gms/car/ak;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/car/a;->a(Lcom/google/android/gms/common/api/o;)V

    sget-object v0, Lcom/google/android/gms/car/a;->a:Lcom/google/android/gms/common/api/h;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/h;)Lcom/google/android/gms/common/api/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/eq;

    invoke-virtual {v0}, Lcom/google/android/gms/car/eq;->g()Lcom/google/android/gms/car/ak;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/o;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/car/a;->a(Lcom/google/android/gms/common/api/o;)V

    sget-object v0, Lcom/google/android/gms/car/a;->a:Lcom/google/android/gms/common/api/h;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/h;)Lcom/google/android/gms/common/api/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/eq;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/car/eq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/o;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/car/a;->a(Lcom/google/android/gms/common/api/o;)V

    sget-object v0, Lcom/google/android/gms/car/a;->a:Lcom/google/android/gms/common/api/h;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/h;)Lcom/google/android/gms/common/api/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/eq;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/car/eq;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/o;Ljava/lang/String;Z)Z
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/car/a;->a(Lcom/google/android/gms/common/api/o;)V

    sget-object v0, Lcom/google/android/gms/car/a;->a:Lcom/google/android/gms/common/api/h;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/h;)Lcom/google/android/gms/common/api/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/eq;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/car/eq;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/common/api/o;)Lcom/google/android/gms/car/ag;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/car/a;->a(Lcom/google/android/gms/common/api/o;)V

    sget-object v0, Lcom/google/android/gms/car/a;->a:Lcom/google/android/gms/common/api/h;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/h;)Lcom/google/android/gms/common/api/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/eq;

    invoke-virtual {v0}, Lcom/google/android/gms/car/eq;->h()Lcom/google/android/gms/car/ag;

    move-result-object v0

    return-object v0
.end method
