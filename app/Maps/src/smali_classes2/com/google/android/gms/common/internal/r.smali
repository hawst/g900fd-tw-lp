.class final Lcom/google/android/gms/common/internal/r;
.super Ljava/lang/Object;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Lcom/google/android/gms/common/internal/s;

.field final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/gms/common/internal/e",
            "<*>.com/google/android/gms/common/internal/k;>;"
        }
    .end annotation
.end field

.field d:I

.field e:Z

.field f:Landroid/os/IBinder;

.field g:Landroid/content/ComponentName;

.field final synthetic h:Lcom/google/android/gms/common/internal/q;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/q;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/common/internal/r;->h:Lcom/google/android/gms/common/internal/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/common/internal/r;->a:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/common/internal/s;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/internal/s;-><init>(Lcom/google/android/gms/common/internal/r;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/r;->b:Lcom/google/android/gms/common/internal/s;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/r;->c:Ljava/util/HashSet;

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/common/internal/r;->d:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/r;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/internal/r;->h:Lcom/google/android/gms/common/internal/q;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/q;->b(Lcom/google/android/gms/common/internal/q;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/r;->b:Lcom/google/android/gms/common/internal/s;

    const/16 v3, 0x81

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/r;->e:Z

    iget-boolean v0, p0, Lcom/google/android/gms/common/internal/r;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/common/internal/r;->d:I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/r;->h:Lcom/google/android/gms/common/internal/q;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/q;->b(Lcom/google/android/gms/common/internal/q;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/internal/r;->b:Lcom/google/android/gms/common/internal/s;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0
.end method
