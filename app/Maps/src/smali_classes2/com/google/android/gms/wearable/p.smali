.class public Lcom/google/android/gms/wearable/p;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/google/android/gms/wearable/a;

.field public static final b:Lcom/google/android/gms/wearable/i;

.field public static final c:Lcom/google/android/gms/common/api/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/h",
            "<",
            "Lcom/google/android/gms/wearable/internal/aw;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/google/android/gms/common/api/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a",
            "<",
            "Lcom/google/android/gms/wearable/r;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Lcom/google/android/gms/common/api/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/g",
            "<",
            "Lcom/google/android/gms/wearable/internal/aw;",
            "Lcom/google/android/gms/wearable/r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/android/gms/wearable/internal/bc;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/internal/bc;-><init>()V

    sput-object v0, Lcom/google/android/gms/wearable/p;->a:Lcom/google/android/gms/wearable/a;

    new-instance v0, Lcom/google/android/gms/wearable/internal/w;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/internal/w;-><init>()V

    sput-object v0, Lcom/google/android/gms/wearable/p;->b:Lcom/google/android/gms/wearable/i;

    new-instance v0, Lcom/google/android/gms/wearable/internal/ad;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/internal/ad;-><init>()V

    new-instance v0, Lcom/google/android/gms/wearable/internal/bb;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/internal/bb;-><init>()V

    new-instance v0, Lcom/google/android/gms/wearable/internal/am;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/internal/am;-><init>()V

    new-instance v0, Lcom/google/android/gms/common/api/h;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/wearable/p;->c:Lcom/google/android/gms/common/api/h;

    new-instance v0, Lcom/google/android/gms/wearable/q;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/q;-><init>()V

    sput-object v0, Lcom/google/android/gms/wearable/p;->e:Lcom/google/android/gms/common/api/g;

    new-instance v0, Lcom/google/android/gms/common/api/a;

    sget-object v1, Lcom/google/android/gms/wearable/p;->e:Lcom/google/android/gms/common/api/g;

    sget-object v2, Lcom/google/android/gms/wearable/p;->c:Lcom/google/android/gms/common/api/h;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/a;-><init>(Lcom/google/android/gms/common/api/g;Lcom/google/android/gms/common/api/h;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/wearable/p;->d:Lcom/google/android/gms/common/api/a;

    return-void
.end method
