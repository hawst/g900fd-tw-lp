.class public final Lcom/google/android/gms/internal/rv;
.super Lcom/google/android/gms/internal/se;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/se",
        "<",
        "Lcom/google/android/gms/internal/rv;",
        ">;"
    }
.end annotation


# instance fields
.field public a:[Lcom/google/android/gms/internal/rw;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/se;-><init>()V

    invoke-static {}, Lcom/google/android/gms/internal/rw;->b()[Lcom/google/android/gms/internal/rw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/rv;->o:Lcom/google/android/gms/internal/sf;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/rv;->p:I

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/gms/internal/se;->a()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    aget-object v3, v3, v0

    if-eqz v3, :cond_0

    const/4 v4, 0x1

    invoke-static {v4, v1}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/android/gms/internal/sm;->d()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v5

    add-int/2addr v3, v5

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    :cond_2
    return v0
.end method

.method public final synthetic a(Lcom/google/android/gms/internal/sb;)Lcom/google/android/gms/internal/sm;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/internal/rv;->a(Lcom/google/android/gms/internal/sb;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/sp;->a(Lcom/google/android/gms/internal/sb;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/internal/rw;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/internal/rw;

    invoke-direct {v3}, Lcom/google/android/gms/internal/rw;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/sb;->a(Lcom/google/android/gms/internal/sm;)V

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/internal/rw;

    invoke-direct {v3}, Lcom/google/android/gms/internal/rw;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/sb;->a(Lcom/google/android/gms/internal/sm;)V

    iput-object v2, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lcom/google/android/gms/internal/sc;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    array-length v0, v0

    if-lez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/gms/internal/sc;->b(I)V

    iget v2, v1, Lcom/google/android/gms/internal/sm;->p:I

    if-gez v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/internal/sm;->d()I

    :cond_0
    iget v2, v1, Lcom/google/android/gms/internal/sm;->p:I

    invoke-virtual {p1, v2}, Lcom/google/android/gms/internal/sc;->b(I)V

    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/sm;->a(Lcom/google/android/gms/internal/sc;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/gms/internal/se;->a(Lcom/google/android/gms/internal/sc;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/internal/rv;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/gms/internal/rv;

    iget-object v1, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    iget-object v2, p1, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/sh;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/rv;->a(Lcom/google/android/gms/internal/se;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/internal/rv;->a:[Lcom/google/android/gms/internal/rw;

    invoke-static {v1}, Lcom/google/android/gms/internal/sh;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v1, v1, 0x20f

    mul-int/lit8 v2, v1, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    iget v1, v1, Lcom/google/android/gms/internal/sf;->d:I

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_2

    :cond_0
    :goto_1
    add-int/2addr v0, v2

    return v0

    :cond_1
    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/sf;->hashCode()I

    move-result v0

    goto :goto_1
.end method
