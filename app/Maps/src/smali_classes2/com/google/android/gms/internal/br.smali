.class public final Lcom/google/android/gms/internal/br;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field final b:Ljava/util/Date;

.field final c:Ljava/lang/String;

.field final d:I

.field final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final f:Landroid/location/Location;

.field final g:Z

.field final h:Landroid/os/Bundle;

.field public final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final j:Ljava/lang/String;

.field final k:Lcom/google/android/gms/ads/search/a;

.field final l:I

.field final m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "emulator"

    invoke-static {v0}, Lcom/google/android/gms/internal/kt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/br;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/bs;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/br;-><init>(Lcom/google/android/gms/internal/bs;Lcom/google/android/gms/ads/search/a;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/internal/bs;Lcom/google/android/gms/ads/search/a;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/google/android/gms/internal/bs;->e:Ljava/util/Date;

    iput-object v0, p0, Lcom/google/android/gms/internal/br;->b:Ljava/util/Date;

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->c:Ljava/lang/String;

    iget v0, p1, Lcom/google/android/gms/internal/bs;->f:I

    iput v0, p0, Lcom/google/android/gms/internal/br;->d:I

    iget-object v0, p1, Lcom/google/android/gms/internal/bs;->a:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/br;->e:Ljava/util/Set;

    iget-object v0, p1, Lcom/google/android/gms/internal/bs;->g:Landroid/location/Location;

    iput-object v0, p0, Lcom/google/android/gms/internal/br;->f:Landroid/location/Location;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/br;->g:Z

    iget-object v0, p1, Lcom/google/android/gms/internal/bs;->b:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/google/android/gms/internal/br;->h:Landroid/os/Bundle;

    iget-object v0, p1, Lcom/google/android/gms/internal/bs;->c:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/br;->i:Ljava/util/Map;

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->j:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/internal/br;->k:Lcom/google/android/gms/ads/search/a;

    iget v0, p1, Lcom/google/android/gms/internal/bs;->h:I

    iput v0, p0, Lcom/google/android/gms/internal/br;->l:I

    iget-object v0, p1, Lcom/google/android/gms/internal/bs;->d:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/br;->m:Ljava/util/Set;

    return-void
.end method
