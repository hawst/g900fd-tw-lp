.class public Lcom/google/android/gms/internal/gb;
.super Lcom/google/android/gms/internal/gr;

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/gms/internal/ge;

.field private c:Lcom/google/android/gms/internal/gk;

.field private d:Lcom/google/android/gms/internal/fx;

.field private e:Lcom/google/android/gms/internal/gc;

.field private f:Lcom/google/android/gms/internal/gh;

.field private g:Lcom/google/android/gms/internal/gi;

.field private h:Ljava/lang/String;


# direct methods
.method public static a(Landroid/content/Context;ZLcom/google/android/gms/internal/ef;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.ads.purchase.InAppPurchaseActivity"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.ads.internal.purchase.useClientJar"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {v0, p2}, Lcom/google/android/gms/internal/ef;->a(Landroid/content/Intent;Lcom/google/android/gms/internal/ef;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/gb;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/ef;->a(Landroid/content/Intent;)Lcom/google/android/gms/internal/ef;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/internal/ef;->e:Lcom/google/android/gms/internal/gh;

    iput-object v1, p0, Lcom/google/android/gms/internal/gb;->f:Lcom/google/android/gms/internal/gh;

    iget-object v1, v0, Lcom/google/android/gms/internal/ef;->b:Lcom/google/android/gms/internal/gi;

    iput-object v1, p0, Lcom/google/android/gms/internal/gb;->g:Lcom/google/android/gms/internal/gi;

    iget-object v1, v0, Lcom/google/android/gms/internal/ef;->c:Lcom/google/android/gms/internal/gk;

    iput-object v1, p0, Lcom/google/android/gms/internal/gb;->c:Lcom/google/android/gms/internal/gk;

    new-instance v1, Lcom/google/android/gms/internal/fx;

    iget-object v2, p0, Lcom/google/android/gms/internal/gb;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/fx;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/internal/gb;->d:Lcom/google/android/gms/internal/fx;

    iget-object v0, v0, Lcom/google/android/gms/internal/ef;->d:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/internal/gb;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/gb;->a:Landroid/app/Activity;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/internal/gb;->a:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/gb;->a:Landroid/app/Activity;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 7

    const/4 v6, 0x0

    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_2

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p3}, Lcom/google/android/gms/internal/gg;->a(Landroid/content/Intent;)I

    move-result v0

    const/4 v1, -0x1

    if-ne p2, v1, :cond_3

    if-nez v0, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/internal/gb;->g:Lcom/google/android/gms/internal/gi;

    iget-object v3, p0, Lcom/google/android/gms/internal/gb;->h:Ljava/lang/String;

    invoke-virtual {v1, v3, p3}, Lcom/google/android/gms/internal/gi;->a(Ljava/lang/String;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/internal/gb;->c:Lcom/google/android/gms/internal/gk;

    invoke-interface {v1, v0}, Lcom/google/android/gms/internal/gk;->b(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/gb;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    iget-object v0, p0, Lcom/google/android/gms/internal/gb;->c:Lcom/google/android/gms/internal/gk;

    invoke-interface {v0}, Lcom/google/android/gms/internal/gk;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/internal/gb;->f:Lcom/google/android/gms/internal/gh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/gb;->f:Lcom/google/android/gms/internal/gh;

    iget-object v5, p0, Lcom/google/android/gms/internal/gb;->e:Lcom/google/android/gms/internal/gc;

    move v3, p2

    move-object v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/internal/gh;->a(Ljava/lang/String;ZILandroid/content/Intent;Lcom/google/android/gms/internal/gc;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    iput-object v6, p0, Lcom/google/android/gms/internal/gb;->h:Ljava/lang/String;

    :cond_2
    :goto_1
    return-void

    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/internal/gb;->b:Lcom/google/android/gms/internal/ge;

    iget-object v3, p0, Lcom/google/android/gms/internal/gb;->e:Lcom/google/android/gms/internal/gc;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/internal/ge;->a(Lcom/google/android/gms/internal/gc;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/internal/gb;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iput-object v6, p0, Lcom/google/android/gms/internal/gb;->h:Ljava/lang/String;

    goto :goto_1

    :catchall_0
    move-exception v0

    iput-object v6, p0, Lcom/google/android/gms/internal/gb;->h:Ljava/lang/String;

    throw v0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/gb;->a:Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/gb;->d:Lcom/google/android/gms/internal/fx;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/internal/fx;->a:Ljava/lang/Object;

    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 12

    iget-object v2, p0, Lcom/google/android/gms/internal/gb;->d:Lcom/google/android/gms/internal/fx;

    invoke-virtual {v2, p2}, Lcom/google/android/gms/internal/fx;->a(Landroid/os/IBinder;)V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/internal/gb;->g:Lcom/google/android/gms/internal/gi;

    invoke-static {}, Lcom/google/android/gms/internal/kf;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/internal/gb;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/internal/gb;->d:Lcom/google/android/gms/internal/fx;

    iget-object v3, p0, Lcom/google/android/gms/internal/gb;->a:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/internal/gb;->c:Lcom/google/android/gms/internal/gk;

    invoke-interface {v4}, Lcom/google/android/gms/internal/gk;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/internal/gb;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/internal/fx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    const-string v2, "BUY_INTENT"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/app/PendingIntent;

    move-object v3, v0

    if-nez v3, :cond_1

    invoke-static {v4}, Lcom/google/android/gms/internal/gg;->a(Landroid/os/Bundle;)I

    move-result v5

    iget-object v2, p0, Lcom/google/android/gms/internal/gb;->c:Lcom/google/android/gms/internal/gk;

    invoke-interface {v2, v5}, Lcom/google/android/gms/internal/gk;->b(I)V

    iget-object v2, p0, Lcom/google/android/gms/internal/gb;->c:Lcom/google/android/gms/internal/gk;

    invoke-interface {v2}, Lcom/google/android/gms/internal/gk;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v6, 0x0

    iget-object v2, p0, Lcom/google/android/gms/internal/gb;->f:Lcom/google/android/gms/internal/gh;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/internal/gb;->f:Lcom/google/android/gms/internal/gh;

    iget-object v7, p0, Lcom/google/android/gms/internal/gb;->e:Lcom/google/android/gms/internal/gc;

    invoke-interface/range {v2 .. v7}, Lcom/google/android/gms/internal/gh;->a(Ljava/lang/String;ZILandroid/content/Intent;Lcom/google/android/gms/internal/gc;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/internal/gb;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_1
    new-instance v2, Lcom/google/android/gms/internal/gc;

    iget-object v4, p0, Lcom/google/android/gms/internal/gb;->c:Lcom/google/android/gms/internal/gk;

    invoke-interface {v4}, Lcom/google/android/gms/internal/gk;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/internal/gb;->h:Ljava/lang/String;

    invoke-direct {v2, v4, v5}, Lcom/google/android/gms/internal/gc;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/gms/internal/gb;->e:Lcom/google/android/gms/internal/gc;

    iget-object v2, p0, Lcom/google/android/gms/internal/gb;->b:Lcom/google/android/gms/internal/ge;

    iget-object v4, p0, Lcom/google/android/gms/internal/gb;->e:Lcom/google/android/gms/internal/gc;

    if-eqz v4, :cond_2

    sget-object v5, Lcom/google/android/gms/internal/ge;->a:Ljava/lang/Object;

    monitor-enter v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ge;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    if-nez v6, :cond_3

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_1
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/internal/gb;->a:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v3

    const/16 v4, 0x3e9

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual/range {v2 .. v8}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_0
    move-exception v2

    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/internal/gb;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_3
    :try_start_3
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v8, "product_id"

    iget-object v9, v4, Lcom/google/android/gms/internal/gc;->c:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "developer_payload"

    iget-object v9, v4, Lcom/google/android/gms/internal/gc;->b:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "record_time"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "InAppPurchase"

    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9, v7}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    iput-wide v6, v4, Lcom/google/android/gms/internal/gc;->a:J

    invoke-virtual {v2}, Lcom/google/android/gms/internal/ge;->b()I

    move-result v4

    int-to-long v6, v4

    const-wide/16 v8, 0x4e20

    cmp-long v4, v6, v8

    if-lez v4, :cond_4

    invoke-virtual {v2}, Lcom/google/android/gms/internal/ge;->c()V

    :cond_4
    monitor-exit v5

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v2

    goto :goto_2
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/gb;->d:Lcom/google/android/gms/internal/fx;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/internal/fx;->a:Ljava/lang/Object;

    return-void
.end method
