.class Lcom/google/android/gms/internal/mg;
.super Lcom/google/android/gms/internal/mi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/mi",
        "<",
        "Lcom/google/android/gms/common/api/Status;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic d:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/me;Lcom/google/android/gms/common/api/o;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    iput-object p3, p0, Lcom/google/android/gms/internal/mg;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0, p2}, Lcom/google/android/gms/internal/mi;-><init>(Lcom/google/android/gms/common/api/o;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/u;
    .locals 0

    return-object p1
.end method

.method protected final synthetic b(Lcom/google/android/gms/common/api/f;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/mg;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method
