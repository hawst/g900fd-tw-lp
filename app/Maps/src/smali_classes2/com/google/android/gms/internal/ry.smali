.class public final Lcom/google/android/gms/internal/ry;
.super Lcom/google/android/gms/internal/se;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/se",
        "<",
        "Lcom/google/android/gms/internal/ry;",
        ">;"
    }
.end annotation


# instance fields
.field public a:[B

.field public b:Ljava/lang/String;

.field public c:D

.field public d:F

.field public e:J

.field public f:I

.field public g:I

.field public h:Z

.field public i:[Lcom/google/android/gms/internal/rw;

.field public j:[Lcom/google/android/gms/internal/rx;

.field public k:[Ljava/lang/String;

.field public l:[J

.field public m:[F

.field public n:J


# direct methods
.method public constructor <init>()V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/internal/se;-><init>()V

    sget-object v0, Lcom/google/android/gms/internal/sp;->e:[B

    iput-object v0, p0, Lcom/google/android/gms/internal/ry;->a:[B

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/internal/ry;->b:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/internal/ry;->c:D

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/ry;->d:F

    iput-wide v4, p0, Lcom/google/android/gms/internal/ry;->e:J

    iput v2, p0, Lcom/google/android/gms/internal/ry;->f:I

    iput v2, p0, Lcom/google/android/gms/internal/ry;->g:I

    iput-boolean v2, p0, Lcom/google/android/gms/internal/ry;->h:Z

    invoke-static {}, Lcom/google/android/gms/internal/rw;->b()[Lcom/google/android/gms/internal/rw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    invoke-static {}, Lcom/google/android/gms/internal/rx;->b()[Lcom/google/android/gms/internal/rx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    sget-object v0, Lcom/google/android/gms/internal/sp;->d:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/internal/sp;->b:[J

    iput-object v0, p0, Lcom/google/android/gms/internal/ry;->l:[J

    sget-object v0, Lcom/google/android/gms/internal/sp;->c:[F

    iput-object v0, p0, Lcom/google/android/gms/internal/ry;->m:[F

    iput-wide v4, p0, Lcom/google/android/gms/internal/ry;->n:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/ry;->o:Lcom/google/android/gms/internal/sf;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/ry;->p:I

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 10

    const-wide/16 v8, 0x0

    const/16 v2, 0xa

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/gms/internal/se;->a()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->a:[B

    sget-object v4, Lcom/google/android/gms/internal/sp;->e:[B

    invoke-static {v1, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iget-object v4, p0, Lcom/google/android/gms/internal/ry;->a:[B

    invoke-static {v1, v3}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v1

    array-length v5, v4

    invoke-static {v5}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v5

    array-length v4, v4

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->b:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v4, p0, Lcom/google/android/gms/internal/ry;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v1

    invoke-static {v4}, Lcom/google/android/gms/internal/sc;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    :cond_1
    iget-wide v4, p0, Lcom/google/android/gms/internal/ry;->c:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-wide v4, p0, Lcom/google/android/gms/internal/ry;->c:D

    invoke-static {v1, v3}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/google/android/gms/internal/ry;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v1, v4, :cond_3

    const/4 v1, 0x4

    iget v4, p0, Lcom/google/android/gms/internal/ry;->d:F

    invoke-static {v1, v3}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_3
    iget-wide v4, p0, Lcom/google/android/gms/internal/ry;->e:J

    cmp-long v1, v4, v8

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-wide v4, p0, Lcom/google/android/gms/internal/ry;->e:J

    invoke-static {v1, v3}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/android/gms/internal/sc;->b(J)I

    move-result v4

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/google/android/gms/internal/ry;->f:I

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget v4, p0, Lcom/google/android/gms/internal/ry;->f:I

    invoke-static {v1, v3}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v5

    if-ltz v4, :cond_9

    invoke-static {v4}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v1

    :goto_0
    add-int/2addr v1, v5

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/google/android/gms/internal/ry;->g:I

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget v4, p0, Lcom/google/android/gms/internal/ry;->g:I

    invoke-static {v1, v3}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v1

    shl-int/lit8 v5, v4, 0x1

    shr-int/lit8 v4, v4, 0x1f

    xor-int/2addr v4, v5

    invoke-static {v4}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v4

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    :cond_6
    iget-boolean v1, p0, Lcom/google/android/gms/internal/ry;->h:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-boolean v4, p0, Lcom/google/android/gms/internal/ry;->h:Z

    invoke-static {v1, v3}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    array-length v1, v1

    if-lez v1, :cond_b

    move v1, v0

    move v0, v3

    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    array-length v4, v4

    if-ge v0, v4, :cond_a

    iget-object v4, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    aget-object v4, v4, v0

    if-eqz v4, :cond_8

    const/16 v5, 0x9

    invoke-static {v5, v3}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/android/gms/internal/sm;->d()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v6

    add-int/2addr v4, v6

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_9
    move v1, v2

    goto :goto_0

    :cond_a
    move v0, v1

    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    array-length v1, v1

    if-lez v1, :cond_e

    move v1, v0

    move v0, v3

    :goto_2
    iget-object v4, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    array-length v4, v4

    if-ge v0, v4, :cond_d

    iget-object v4, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    aget-object v4, v4, v0

    if-eqz v4, :cond_c

    invoke-static {v2, v3}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/android/gms/internal/sm;->d()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v6

    add-int/2addr v4, v6

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_d
    move v0, v1

    :cond_e
    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_11

    move v1, v3

    move v2, v3

    move v4, v3

    :goto_3
    iget-object v5, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_10

    iget-object v5, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    aget-object v5, v5, v1

    if-eqz v5, :cond_f

    add-int/lit8 v4, v4, 0x1

    invoke-static {v5}, Lcom/google/android/gms/internal/sc;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_10
    add-int/2addr v0, v2

    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    :cond_11
    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->l:[J

    if-eqz v1, :cond_13

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->l:[J

    array-length v1, v1

    if-lez v1, :cond_13

    move v1, v3

    move v2, v3

    :goto_4
    iget-object v4, p0, Lcom/google/android/gms/internal/ry;->l:[J

    array-length v4, v4

    if-ge v1, v4, :cond_12

    iget-object v4, p0, Lcom/google/android/gms/internal/ry;->l:[J

    aget-wide v4, v4, v1

    invoke-static {v4, v5}, Lcom/google/android/gms/internal/sc;->b(J)I

    move-result v4

    add-int/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_12
    add-int/2addr v0, v2

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->l:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_13
    iget-wide v4, p0, Lcom/google/android/gms/internal/ry;->n:J

    cmp-long v1, v4, v8

    if-eqz v1, :cond_14

    const/16 v1, 0xd

    iget-wide v4, p0, Lcom/google/android/gms/internal/ry;->n:J

    invoke-static {v1, v3}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/android/gms/internal/sc;->b(J)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    :cond_14
    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->m:[F

    if-eqz v1, :cond_15

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->m:[F

    array-length v1, v1

    if-lez v1, :cond_15

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->m:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->m:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_15
    return v0
.end method

.method public final synthetic a(Lcom/google/android/gms/internal/sb;)Lcom/google/android/gms/internal/sm;
    .locals 6

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/internal/ry;->a(Lcom/google/android/gms/internal/sb;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->c()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/ry;->a:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/ry;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/internal/ry;->c:D

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/ry;->d:F

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/internal/ry;->e:J

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/ry;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v0

    ushr-int/lit8 v2, v0, 0x1

    and-int/lit8 v0, v0, 0x1

    neg-int v0, v0

    xor-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/gms/internal/ry;->g:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/internal/ry;->h:Z

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/sp;->a(Lcom/google/android/gms/internal/sb;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/internal/rw;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_3
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/android/gms/internal/rw;

    invoke-direct {v3}, Lcom/google/android/gms/internal/rw;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/sb;->a(Lcom/google/android/gms/internal/sm;)V

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    array-length v0, v0

    goto :goto_2

    :cond_4
    new-instance v3, Lcom/google/android/gms/internal/rw;

    invoke-direct {v3}, Lcom/google/android/gms/internal/rw;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/sb;->a(Lcom/google/android/gms/internal/sm;)V

    iput-object v2, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/sp;->a(Lcom/google/android/gms/internal/sb;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/internal/rx;

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Lcom/google/android/gms/internal/rx;

    invoke-direct {v3}, Lcom/google/android/gms/internal/rx;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/sb;->a(Lcom/google/android/gms/internal/sm;)V

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    array-length v0, v0

    goto :goto_4

    :cond_7
    new-instance v3, Lcom/google/android/gms/internal/rx;

    invoke-direct {v3}, Lcom/google/android/gms/internal/rx;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/sb;->a(Lcom/google/android/gms/internal/sm;)V

    iput-object v2, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/sp;->a(Lcom/google/android/gms/internal/sb;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v3, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_7
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_6

    :cond_a
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x60

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/sp;->a(Lcom/google/android/gms/internal/sb;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->l:[J

    if-nez v0, :cond_c

    move v0, v1

    :goto_8
    add-int/2addr v2, v0

    new-array v2, v2, [J

    if-eqz v0, :cond_b

    iget-object v3, p0, Lcom/google/android/gms/internal/ry;->l:[J

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_9
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->e()J

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->l:[J

    array-length v0, v0

    goto :goto_8

    :cond_d
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->e()J

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/internal/ry;->l:[J

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/sb;->b(I)I

    move-result v3

    iget v0, p1, Lcom/google/android/gms/internal/sb;->c:I

    iget v2, p1, Lcom/google/android/gms/internal/sb;->b:I

    sub-int v4, v0, v2

    move v0, v1

    :goto_a
    iget v2, p1, Lcom/google/android/gms/internal/sb;->e:I

    const v5, 0x7fffffff

    if-ne v2, v5, :cond_e

    const/4 v2, -0x1

    :goto_b
    if-lez v2, :cond_f

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->e()J

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_e
    iget v2, p1, Lcom/google/android/gms/internal/sb;->c:I

    iget v5, p1, Lcom/google/android/gms/internal/sb;->e:I

    sub-int v2, v5, v2

    goto :goto_b

    :cond_f
    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/sb;->c(I)V

    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->l:[J

    if-nez v2, :cond_11

    move v2, v1

    :goto_c
    add-int/2addr v0, v2

    new-array v0, v0, [J

    if-eqz v2, :cond_10

    iget-object v4, p0, Lcom/google/android/gms/internal/ry;->l:[J

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_10
    :goto_d
    array-length v4, v0

    if-ge v2, v4, :cond_12

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->e()J

    move-result-wide v4

    aput-wide v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    :cond_11
    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->l:[J

    array-length v2, v2

    goto :goto_c

    :cond_12
    iput-object v0, p0, Lcom/google/android/gms/internal/ry;->l:[J

    iput v3, p1, Lcom/google/android/gms/internal/sb;->e:I

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->h()V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/internal/ry;->n:J

    goto/16 :goto_0

    :sswitch_f
    const/16 v0, 0x75

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/sp;->a(Lcom/google/android/gms/internal/sb;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->m:[F

    if-nez v0, :cond_14

    move v0, v1

    :goto_e
    add-int/2addr v2, v0

    new-array v2, v2, [F

    if-eqz v0, :cond_13

    iget-object v3, p0, Lcom/google/android/gms/internal/ry;->m:[F

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_13
    :goto_f
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_15

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    :cond_14
    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->m:[F

    array-length v0, v0

    goto :goto_e

    :cond_15
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/internal/ry;->m:[F

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/sb;->b(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x4

    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->m:[F

    if-nez v0, :cond_17

    move v0, v1

    :goto_10
    add-int/2addr v3, v0

    new-array v3, v3, [F

    if-eqz v0, :cond_16

    iget-object v4, p0, Lcom/google/android/gms/internal/ry;->m:[F

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_16
    :goto_11
    array-length v4, v3

    if-ge v0, v4, :cond_18

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->f()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    :cond_17
    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->m:[F

    array-length v0, v0

    goto :goto_10

    :cond_18
    iput-object v3, p0, Lcom/google/android/gms/internal/ry;->m:[F

    iput v2, p1, Lcom/google/android/gms/internal/sb;->e:I

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sb;->h()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x19 -> :sswitch_3
        0x25 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x62 -> :sswitch_d
        0x68 -> :sswitch_e
        0x72 -> :sswitch_10
        0x75 -> :sswitch_f
    .end sparse-switch
.end method

.method public final a(Lcom/google/android/gms/internal/sc;)V
    .locals 10

    const-wide/16 v8, 0x0

    const/16 v7, 0x8

    const/4 v0, 0x1

    const/4 v6, 0x2

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->a:[B

    sget-object v3, Lcom/google/android/gms/internal/sp;->e:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->a:[B

    invoke-static {v0, v6}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/sc;->b(I)V

    array-length v3, v2

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/sc;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/android/gms/internal/sc;->b([B)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->b:Ljava/lang/String;

    invoke-virtual {p1, v6, v2}, Lcom/google/android/gms/internal/sc;->a(ILjava/lang/String;)V

    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/internal/ry;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/android/gms/internal/ry;->c:D

    invoke-static {v2, v0}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/gms/internal/sc;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    long-to-int v4, v2

    and-int/lit16 v4, v4, 0xff

    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/sc;->a(I)V

    shr-long v4, v2, v7

    long-to-int v4, v4

    and-int/lit16 v4, v4, 0xff

    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/sc;->a(I)V

    const/16 v4, 0x10

    shr-long v4, v2, v4

    long-to-int v4, v4

    and-int/lit16 v4, v4, 0xff

    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/sc;->a(I)V

    const/16 v4, 0x18

    shr-long v4, v2, v4

    long-to-int v4, v4

    and-int/lit16 v4, v4, 0xff

    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/sc;->a(I)V

    const/16 v4, 0x20

    shr-long v4, v2, v4

    long-to-int v4, v4

    and-int/lit16 v4, v4, 0xff

    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/sc;->a(I)V

    const/16 v4, 0x28

    shr-long v4, v2, v4

    long-to-int v4, v4

    and-int/lit16 v4, v4, 0xff

    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/sc;->a(I)V

    const/16 v4, 0x30

    shr-long v4, v2, v4

    long-to-int v4, v4

    and-int/lit16 v4, v4, 0xff

    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/sc;->a(I)V

    const/16 v4, 0x38

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit16 v2, v2, 0xff

    invoke-virtual {p1, v2}, Lcom/google/android/gms/internal/sc;->a(I)V

    :cond_2
    iget v2, p0, Lcom/google/android/gms/internal/ry;->d:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_3

    const/4 v2, 0x4

    iget v3, p0, Lcom/google/android/gms/internal/ry;->d:F

    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/internal/sc;->a(IF)V

    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/internal/ry;->e:J

    cmp-long v2, v2, v8

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/google/android/gms/internal/ry;->e:J

    invoke-static {v2, v1}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/gms/internal/sc;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/android/gms/internal/sc;->a(J)V

    :cond_4
    iget v2, p0, Lcom/google/android/gms/internal/ry;->f:I

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    iget v3, p0, Lcom/google/android/gms/internal/ry;->f:I

    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/internal/sc;->a(II)V

    :cond_5
    iget v2, p0, Lcom/google/android/gms/internal/ry;->g:I

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    iget v3, p0, Lcom/google/android/gms/internal/ry;->g:I

    invoke-static {v2, v1}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/gms/internal/sc;->b(I)V

    shl-int/lit8 v2, v3, 0x1

    shr-int/lit8 v3, v3, 0x1f

    xor-int/2addr v2, v3

    invoke-virtual {p1, v2}, Lcom/google/android/gms/internal/sc;->b(I)V

    :cond_6
    iget-boolean v2, p0, Lcom/google/android/gms/internal/ry;->h:Z

    if-eqz v2, :cond_7

    iget-boolean v2, p0, Lcom/google/android/gms/internal/ry;->h:Z

    invoke-static {v7, v1}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/sc;->b(I)V

    if-eqz v2, :cond_a

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/sc;->a(I)V

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    array-length v0, v0

    if-lez v0, :cond_b

    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    array-length v2, v2

    if-ge v0, v2, :cond_b

    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    aget-object v2, v2, v0

    if-eqz v2, :cond_9

    const/16 v3, 0x9

    invoke-static {v3, v6}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/sc;->b(I)V

    iget v3, v2, Lcom/google/android/gms/internal/sm;->p:I

    if-gez v3, :cond_8

    invoke-virtual {v2}, Lcom/google/android/gms/internal/sm;->d()I

    :cond_8
    iget v3, v2, Lcom/google/android/gms/internal/sm;->p:I

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/sc;->b(I)V

    invoke-virtual {v2, p1}, Lcom/google/android/gms/internal/sm;->a(Lcom/google/android/gms/internal/sc;)V

    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_a
    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    array-length v0, v0

    if-lez v0, :cond_e

    move v0, v1

    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    array-length v2, v2

    if-ge v0, v2, :cond_e

    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    aget-object v2, v2, v0

    if-eqz v2, :cond_d

    const/16 v3, 0xa

    invoke-static {v3, v6}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/sc;->b(I)V

    iget v3, v2, Lcom/google/android/gms/internal/sm;->p:I

    if-gez v3, :cond_c

    invoke-virtual {v2}, Lcom/google/android/gms/internal/sm;->d()I

    :cond_c
    iget v3, v2, Lcom/google/android/gms/internal/sm;->p:I

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/sc;->b(I)V

    invoke-virtual {v2, p1}, Lcom/google/android/gms/internal/sm;->a(Lcom/google/android/gms/internal/sc;)V

    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_10

    move v0, v1

    :goto_3
    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_10

    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-eqz v2, :cond_f

    const/16 v3, 0xb

    invoke-virtual {p1, v3, v2}, Lcom/google/android/gms/internal/sc;->a(ILjava/lang/String;)V

    :cond_f
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->l:[J

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->l:[J

    array-length v0, v0

    if-lez v0, :cond_11

    move v0, v1

    :goto_4
    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->l:[J

    array-length v2, v2

    if-ge v0, v2, :cond_11

    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/android/gms/internal/ry;->l:[J

    aget-wide v4, v3, v0

    invoke-static {v2, v1}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/gms/internal/sc;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/android/gms/internal/sc;->a(J)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_11
    iget-wide v2, p0, Lcom/google/android/gms/internal/ry;->n:J

    cmp-long v0, v2, v8

    if-eqz v0, :cond_12

    const/16 v0, 0xd

    iget-wide v2, p0, Lcom/google/android/gms/internal/ry;->n:J

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/sc;->b(I)V

    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/internal/sc;->a(J)V

    :cond_12
    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->m:[F

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->m:[F

    array-length v0, v0

    if-lez v0, :cond_13

    :goto_5
    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->m:[F

    array-length v0, v0

    if-ge v1, v0, :cond_13

    const/16 v0, 0xe

    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->m:[F

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lcom/google/android/gms/internal/sc;->a(IF)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_13
    invoke-super {p0, p1}, Lcom/google/android/gms/internal/se;->a(Lcom/google/android/gms/internal/sc;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x0

    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/internal/ry;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/gms/internal/ry;

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->a:[B

    iget-object v2, p1, Lcom/google/android/gms/internal/ry;->a:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->b:Ljava/lang/String;

    if-nez v1, :cond_3

    iget-object v1, p1, Lcom/google/android/gms/internal/ry;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/internal/ry;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    iget-wide v4, p1, Lcom/google/android/gms/internal/ry;->c:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/gms/internal/ry;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    iget v2, p1, Lcom/google/android/gms/internal/ry;->d:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/gms/internal/ry;->e:J

    iget-wide v4, p1, Lcom/google/android/gms/internal/ry;->e:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/gms/internal/ry;->f:I

    iget v2, p1, Lcom/google/android/gms/internal/ry;->f:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/gms/internal/ry;->g:I

    iget v2, p1, Lcom/google/android/gms/internal/ry;->g:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/internal/ry;->h:Z

    iget-boolean v2, p1, Lcom/google/android/gms/internal/ry;->h:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    iget-object v2, p1, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/sh;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    iget-object v2, p1, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/sh;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/sh;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->l:[J

    iget-object v2, p1, Lcom/google/android/gms/internal/ry;->l:[J

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/sh;->a([J[J)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->m:[F

    iget-object v2, p1, Lcom/google/android/gms/internal/ry;->m:[F

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/sh;->a([F[F)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/gms/internal/ry;->n:J

    iget-wide v4, p1, Lcom/google/android/gms/internal/ry;->n:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/ry;->a(Lcom/google/android/gms/internal/se;)Z

    move-result v0

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/internal/ry;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/internal/ry;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    const/16 v6, 0x20

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->a:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/gms/internal/ry;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    mul-int/lit8 v0, v0, 0x1f

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/internal/ry;->d:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/internal/ry;->e:J

    iget-wide v4, p0, Lcom/google/android/gms/internal/ry;->e:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/internal/ry;->f:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/internal/ry;->g:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/internal/ry;->h:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->i:[Lcom/google/android/gms/internal/rw;

    invoke-static {v2}, Lcom/google/android/gms/internal/sh;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->j:[Lcom/google/android/gms/internal/rx;

    invoke-static {v2}, Lcom/google/android/gms/internal/sh;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->k:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/internal/sh;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->l:[J

    invoke-static {v2}, Lcom/google/android/gms/internal/sh;->a([J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/internal/ry;->m:[F

    invoke-static {v2}, Lcom/google/android/gms/internal/sh;->a([F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/internal/ry;->n:J

    iget-wide v4, p0, Lcom/google/android/gms/internal/ry;->n:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    iget v0, v0, Lcom/google/android/gms/internal/sf;->d:I

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_4

    :cond_0
    :goto_3
    add-int v0, v2, v1

    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/ry;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_2
    const/16 v0, 0x4d5

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/sf;->hashCode()I

    move-result v1

    goto :goto_3
.end method
