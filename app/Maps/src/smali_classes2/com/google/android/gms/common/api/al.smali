.class Lcom/google/android/gms/common/api/al;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/ai;

.field private final b:I

.field private final c:Lcom/google/android/gms/common/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/ai;ILcom/google/android/gms/common/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/common/api/al;->a:Lcom/google/android/gms/common/api/ai;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/gms/common/api/al;->b:I

    iput-object p3, p0, Lcom/google/android/gms/common/api/al;->c:Lcom/google/android/gms/common/a;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/common/api/al;->c:Lcom/google/android/gms/common/a;

    iget v2, v0, Lcom/google/android/gms/common/a;->c:I

    if-eqz v2, :cond_1

    iget-object v0, v0, Lcom/google/android/gms/common/a;->b:Landroid/app/PendingIntent;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/al;->a:Lcom/google/android/gms/common/api/ai;

    iget-object v0, v0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    iget-object v0, v0, Landroid/support/v4/app/m;->b:Landroid/support/v4/app/s;

    invoke-virtual {v0}, Landroid/support/v4/app/r;->c()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/common/api/al;->a:Lcom/google/android/gms/common/api/ai;

    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    shl-int/lit8 v0, v0, 0x10

    add-int/lit8 v2, v0, 0x1

    iget-object v4, p0, Lcom/google/android/gms/common/api/al;->c:Lcom/google/android/gms/common/a;

    iget-object v0, p0, Lcom/google/android/gms/common/api/al;->a:Lcom/google/android/gms/common/api/ai;

    iget-object v0, v0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    iget v5, v4, Lcom/google/android/gms/common/a;->c:I

    if-eqz v5, :cond_2

    iget-object v5, v4, Lcom/google/android/gms/common/a;->b:Landroid/app/PendingIntent;

    if-eqz v5, :cond_2

    :goto_1
    if-eqz v1, :cond_0

    iget-object v1, v4, Lcom/google/android/gms/common/a;->b:Landroid/app/PendingIntent;

    invoke-virtual {v1}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v3

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/common/api/al;->a:Lcom/google/android/gms/common/api/ai;

    invoke-static {v0}, Lcom/google/android/gms/common/api/ai;->a(Lcom/google/android/gms/common/api/ai;)V

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/common/api/al;->c:Lcom/google/android/gms/common/a;

    iget v0, v0, Lcom/google/android/gms/common/a;->c:I

    invoke-static {v0}, Lcom/google/android/gms/common/g;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/common/api/al;->c:Lcom/google/android/gms/common/a;

    iget v0, v0, Lcom/google/android/gms/common/a;->c:I

    iget-object v1, p0, Lcom/google/android/gms/common/api/al;->a:Lcom/google/android/gms/common/api/ai;

    iget-object v1, v1, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    iget-object v2, p0, Lcom/google/android/gms/common/api/al;->a:Lcom/google/android/gms/common/api/ai;

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/gms/common/api/al;->a:Lcom/google/android/gms/common/api/ai;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/g;->a(ILandroid/app/Activity;Landroid/support/v4/app/j;ILandroid/content/DialogInterface$OnCancelListener;)Z

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/common/api/al;->a:Lcom/google/android/gms/common/api/ai;

    iget v1, p0, Lcom/google/android/gms/common/api/al;->b:I

    iget-object v2, p0, Lcom/google/android/gms/common/api/al;->c:Lcom/google/android/gms/common/a;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/api/ai;->a(Lcom/google/android/gms/common/api/ai;ILcom/google/android/gms/common/a;)V

    goto :goto_2
.end method
