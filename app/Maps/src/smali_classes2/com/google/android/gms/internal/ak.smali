.class public Lcom/google/android/gms/internal/ak;
.super Ljava/lang/Thread;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field a:Z

.field final b:Ljava/lang/Object;

.field final c:Lcom/google/android/gms/internal/ai;

.field final d:Lcom/google/android/gms/internal/ho;

.field final e:I

.field final f:I

.field final g:I

.field final h:I

.field private i:Z

.field private j:Z

.field private final k:Lcom/google/android/gms/internal/aj;

.field private final l:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/aj;Lcom/google/android/gms/internal/ai;Landroid/os/Bundle;Lcom/google/android/gms/internal/ho;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gms/internal/ak;->i:Z

    iput-boolean v0, p0, Lcom/google/android/gms/internal/ak;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/internal/ak;->j:Z

    iput-object p1, p0, Lcom/google/android/gms/internal/ak;->k:Lcom/google/android/gms/internal/aj;

    iput-object p2, p0, Lcom/google/android/gms/internal/ak;->c:Lcom/google/android/gms/internal/ai;

    iput-object p4, p0, Lcom/google/android/gms/internal/ak;->d:Lcom/google/android/gms/internal/ho;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ak;->b:Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/internal/by;->b:Lcom/google/android/gms/internal/my;

    iget-object v0, v0, Lcom/google/android/gms/internal/my;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/ak;->e:I

    sget-object v0, Lcom/google/android/gms/internal/by;->c:Lcom/google/android/gms/internal/my;

    iget-object v0, v0, Lcom/google/android/gms/internal/my;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/ak;->f:I

    sget-object v0, Lcom/google/android/gms/internal/by;->d:Lcom/google/android/gms/internal/my;

    iget-object v0, v0, Lcom/google/android/gms/internal/my;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/ak;->g:I

    sget-object v0, Lcom/google/android/gms/internal/by;->e:Lcom/google/android/gms/internal/my;

    iget-object v0, v0, Lcom/google/android/gms/internal/my;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/ak;->h:I

    sget-object v0, Lcom/google/android/gms/internal/by;->f:Lcom/google/android/gms/internal/my;

    iget-object v0, v0, Lcom/google/android/gms/internal/my;->a:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/ak;->l:I

    const-string v0, "ContentFetchTask"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ak;->setName(Ljava/lang/String;)V

    return-void
.end method

.method private b()Z
    .locals 7

    const/4 v3, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ak;->k:Lcom/google/android/gms/internal/aj;

    iget-object v2, v0, Lcom/google/android/gms/internal/aj;->b:Landroid/content/Context;

    if-nez v2, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    const-string v0, "activity"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const-string v1, "keyguard"

    invoke-virtual {v2, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    const-string v4, "power"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    if-nez v2, :cond_2

    :cond_1
    move v0, v3

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v3

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v5

    iget v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v5, v6, :cond_4

    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v4, 0x64

    if-ne v0, v4, :cond_5

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v3

    goto :goto_0
.end method


# virtual methods
.method final a(Landroid/view/View;Lcom/google/android/gms/internal/ah;)Lcom/google/android/gms/internal/ao;
    .locals 5

    const/4 v2, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-instance v1, Lcom/google/android/gms/internal/ao;

    invoke-direct {v1, p0, v0, v0}, Lcom/google/android/gms/internal/ao;-><init>(Lcom/google/android/gms/internal/ak;II)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    instance-of v1, p1, Landroid/widget/TextView;

    if-eqz v1, :cond_1

    instance-of v1, p1, Landroid/widget/EditText;

    if-nez v1, :cond_1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/android/gms/internal/ah;->b(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/internal/ao;

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/gms/internal/ao;-><init>(Lcom/google/android/gms/internal/ak;II)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    instance-of v1, p1, Landroid/webkit/WebView;

    if-eqz v1, :cond_5

    instance-of v1, p1, Lcom/google/android/gms/internal/gz;

    if-nez v1, :cond_5

    iget-object v1, p2, Lcom/google/android/gms/internal/ah;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v3, p2, Lcom/google/android/gms/internal/ah;->d:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p2, Lcom/google/android/gms/internal/ah;->d:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    check-cast p1, Landroid/webkit/WebView;

    const/16 v1, 0x13

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v3, v1, :cond_2

    move v1, v2

    :goto_1
    if-nez v1, :cond_3

    move v1, v0

    :goto_2
    if-eqz v1, :cond_4

    new-instance v1, Lcom/google/android/gms/internal/ao;

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/gms/internal/ao;-><init>(Lcom/google/android/gms/internal/ak;II)V

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move v1, v0

    goto :goto_1

    :cond_3
    iget-object v1, p2, Lcom/google/android/gms/internal/ah;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    iget v3, p2, Lcom/google/android/gms/internal/ah;->d:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p2, Lcom/google/android/gms/internal/ah;->d:I

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    new-instance v1, Lcom/google/android/gms/internal/am;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/internal/am;-><init>(Lcom/google/android/gms/internal/ak;Lcom/google/android/gms/internal/ah;Landroid/webkit/WebView;)V

    invoke-virtual {p1, v1}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    move v1, v2

    goto :goto_2

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_4
    new-instance v1, Lcom/google/android/gms/internal/ao;

    invoke-direct {v1, p0, v0, v0}, Lcom/google/android/gms/internal/ao;-><init>(Lcom/google/android/gms/internal/ak;II)V

    move-object v0, v1

    goto :goto_0

    :cond_5
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_7

    check-cast p1, Landroid/view/ViewGroup;

    move v1, v0

    move v2, v0

    :goto_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_6

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v3, p2}, Lcom/google/android/gms/internal/ak;->a(Landroid/view/View;Lcom/google/android/gms/internal/ah;)Lcom/google/android/gms/internal/ao;

    move-result-object v3

    iget v4, v3, Lcom/google/android/gms/internal/ao;->a:I

    add-int/2addr v2, v4

    iget v3, v3, Lcom/google/android/gms/internal/ao;->b:I

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    new-instance v0, Lcom/google/android/gms/internal/ao;

    invoke-direct {v0, p0, v2, v1}, Lcom/google/android/gms/internal/ao;-><init>(Lcom/google/android/gms/internal/ak;II)V

    goto/16 :goto_0

    :cond_7
    new-instance v1, Lcom/google/android/gms/internal/ao;

    invoke-direct {v1, p0, v0, v0}, Lcom/google/android/gms/internal/ao;-><init>(Lcom/google/android/gms/internal/ak;II)V

    move-object v0, v1

    goto/16 :goto_0
.end method

.method public final a()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/ak;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/ak;->i:Z

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/ak;->i:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/ak;->start()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 3

    :cond_0
    :goto_0
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/internal/ak;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/internal/ak;->k:Lcom/google/android/gms/internal/aj;

    iget-object v1, v0, Lcom/google/android/gms/internal/aj;->a:Landroid/app/Activity;

    if-eqz v1, :cond_0

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    if-nez v0, :cond_3

    :cond_2
    :goto_1
    iget v0, p0, Lcom/google/android/gms/internal/ak;->l:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/internal/ak;->b:Ljava/lang/Object;

    monitor-enter v1

    :goto_3
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/gms/internal/ak;->a:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_5

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/internal/ak;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_3

    :catch_0
    move-exception v0

    goto :goto_3

    :cond_3
    :try_start_3
    new-instance v1, Lcom/google/android/gms/internal/al;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/internal/al;-><init>(Lcom/google/android/gms/internal/ak;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/internal/ak;->d:Lcom/google/android/gms/internal/ho;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/ho;->a(Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    :try_start_4
    iget-object v1, p0, Lcom/google/android/gms/internal/ak;->b:Ljava/lang/Object;

    monitor-enter v1
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    const/4 v0, 0x1

    :try_start_5
    iput-boolean v0, p0, Lcom/google/android/gms/internal/ak;->a:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "ContentFetchThread: paused, mPause = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/gms/internal/ak;->a:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    :cond_5
    :try_start_7
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v0
.end method
