.class public final Lcom/google/android/gms/b/a;
.super Ljava/lang/Object;


# static fields
.field private static a:Z

.field private static final b:Lcom/google/android/gms/common/api/r;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/b/a;->a:Z

    new-instance v0, Lcom/google/android/gms/b/b;

    invoke-direct {v0}, Lcom/google/android/gms/b/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/b/a;->b:Lcom/google/android/gms/common/api/r;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)V
    .locals 7

    sget-boolean v0, Lcom/google/android/gms/b/a;->a:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/gms/common/api/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/p;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/b/a;->b:Lcom/google/android/gms/common/api/r;

    iget-object v2, v0, Lcom/google/android/gms/common/api/p;->b:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/android/gms/internal/nm;->c:Lcom/google/android/gms/common/api/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/p;->a()Lcom/google/android/gms/common/api/o;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/api/o;->b()V

    sget-object v0, Lcom/google/android/gms/internal/nm;->d:Lcom/google/android/gms/internal/no;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/internal/no;->a(Lcom/google/android/gms/common/api/o;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/s;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/b/c;

    invoke-direct {v2, v1}, Lcom/google/android/gms/b/c;-><init>(Lcom/google/android/gms/common/api/o;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/s;->a(Lcom/google/android/gms/common/api/v;)V

    goto :goto_0
.end method

.method static synthetic a(Z)Z
    .locals 0

    sput-boolean p0, Lcom/google/android/gms/b/a;->a:Z

    return p0
.end method
