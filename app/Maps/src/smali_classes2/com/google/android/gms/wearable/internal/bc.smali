.class public final Lcom/google/android/gms/wearable/internal/bc;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/wearable/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/o;Landroid/net/Uri;)Lcom/google/android/gms/common/api/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/o;",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/gms/common/api/s",
            "<",
            "Lcom/google/android/gms/wearable/d;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/wearable/internal/be;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/wearable/internal/be;-><init>(Lcom/google/android/gms/wearable/internal/bc;Lcom/google/android/gms/common/api/o;Landroid/net/Uri;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/o;Lcom/google/android/gms/wearable/PutDataRequest;)Lcom/google/android/gms/common/api/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/o;",
            "Lcom/google/android/gms/wearable/PutDataRequest;",
            ")",
            "Lcom/google/android/gms/common/api/s",
            "<",
            "Lcom/google/android/gms/wearable/b;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/wearable/internal/bd;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/wearable/internal/bd;-><init>(Lcom/google/android/gms/wearable/internal/bc;Lcom/google/android/gms/common/api/o;Lcom/google/android/gms/wearable/PutDataRequest;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
