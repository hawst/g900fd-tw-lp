.class public interface abstract Lcom/google/android/gms/car/cf;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(Landroid/view/KeyEvent;)V
.end method

.method public abstract a(Lcom/google/android/gms/car/CarCall;)V
.end method

.method public abstract a(Lcom/google/android/gms/car/CarCall;I)V
.end method

.method public abstract a(Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall$Details;)V
.end method

.method public abstract a(Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall;)V
.end method

.method public abstract a(Lcom/google/android/gms/car/CarCall;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/car/CarCall;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/car/CarCall;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(ZII)V
.end method

.method public abstract b(Lcom/google/android/gms/car/CarCall;)V
.end method

.method public abstract b(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/car/CarCall;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract c(Lcom/google/android/gms/car/CarCall;)V
.end method

.method public abstract c(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/car/CarCall;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/car/CarCall;",
            ">;)V"
        }
    .end annotation
.end method
