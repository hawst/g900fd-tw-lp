.class final Lcom/google/android/gms/wearable/internal/au;
.super Lcom/google/android/gms/wearable/internal/ap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/wearable/internal/ap",
        "<",
        "Lcom/google/android/gms/wearable/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/m;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/m",
            "<",
            "Lcom/google/android/gms/wearable/b;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/gms/wearable/internal/ap;-><init>(Lcom/google/android/gms/common/api/m;)V

    iput-object p2, p0, Lcom/google/android/gms/wearable/internal/au;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wearable/internal/ao;)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/wearable/internal/bf;

    iget v1, p1, Lcom/google/android/gms/wearable/internal/ao;->b:I

    new-instance v2, Lcom/google/android/gms/common/api/Status;

    invoke-static {v1}, Lcom/google/android/gms/wearable/z;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/gms/wearable/internal/ao;->c:Lcom/google/android/gms/wearable/internal/m;

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wearable/internal/bf;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/wearable/f;)V

    iget-object v1, p0, Lcom/google/android/gms/wearable/internal/ap;->a:Lcom/google/android/gms/common/api/m;

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wearable/internal/ap;->a:Lcom/google/android/gms/common/api/m;

    :cond_0
    iget v0, p1, Lcom/google/android/gms/wearable/internal/ao;->b:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wearable/internal/au;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/FutureTask;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    goto :goto_0

    :cond_1
    return-void
.end method
