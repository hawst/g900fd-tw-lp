.class final Lcom/google/android/gms/internal/qe;
.super Lcom/google/android/gms/common/internal/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/e",
        "<",
        "Lcom/google/android/gms/internal/pv;",
        ">.com/google/android/gms/common/internal/h<",
        "Lcom/google/android/gms/people/m;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic c:Lcom/google/android/gms/internal/qb;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/qb;Lcom/google/android/gms/people/m;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/qe;->c:Lcom/google/android/gms/internal/qb;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/h;-><init>(Lcom/google/android/gms/common/internal/e;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/gms/internal/qe;->d:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/qe;->e:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/gms/internal/qe;->f:I

    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Lcom/google/android/gms/people/m;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/qe;->c:Lcom/google/android/gms/internal/qb;

    invoke-static {v0}, Lcom/google/android/gms/internal/qb;->a(Lcom/google/android/gms/internal/qb;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/qe;->c:Lcom/google/android/gms/internal/qb;

    invoke-static {v0}, Lcom/google/android/gms/internal/qb;->a(Lcom/google/android/gms/internal/qb;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    monitor-exit v1

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/internal/qe;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/internal/qe;->e:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/gms/internal/qe;->f:I

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected final c()V
    .locals 0

    return-void
.end method
