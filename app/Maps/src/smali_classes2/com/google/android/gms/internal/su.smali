.class Lcom/google/android/gms/internal/su;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic b:Lcom/google/android/gms/internal/z;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/z;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/su;->b:Lcom/google/android/gms/internal/z;

    iput-object p2, p0, Lcom/google/android/gms/internal/su;->a:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/su;->a:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/android/gms/internal/gg;->a(Landroid/content/Intent;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/su;->b:Lcom/google/android/gms/internal/z;

    invoke-static {v0}, Lcom/google/android/gms/internal/z;->a(Lcom/google/android/gms/internal/z;)Lcom/google/android/gms/internal/sv;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/internal/sv;->j:Lcom/google/android/gms/internal/jp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/su;->b:Lcom/google/android/gms/internal/z;

    invoke-static {v0}, Lcom/google/android/gms/internal/z;->a(Lcom/google/android/gms/internal/z;)Lcom/google/android/gms/internal/sv;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/internal/sv;->j:Lcom/google/android/gms/internal/jp;

    iget-object v0, v0, Lcom/google/android/gms/internal/jp;->b:Lcom/google/android/gms/internal/gz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/su;->b:Lcom/google/android/gms/internal/z;

    invoke-static {v0}, Lcom/google/android/gms/internal/z;->a(Lcom/google/android/gms/internal/z;)Lcom/google/android/gms/internal/sv;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/internal/sv;->j:Lcom/google/android/gms/internal/jp;

    iget-object v0, v0, Lcom/google/android/gms/internal/jp;->b:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->c()Lcom/google/android/gms/internal/du;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/su;->b:Lcom/google/android/gms/internal/z;

    invoke-static {v0}, Lcom/google/android/gms/internal/z;->a(Lcom/google/android/gms/internal/z;)Lcom/google/android/gms/internal/sv;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/internal/sv;->j:Lcom/google/android/gms/internal/jp;

    iget-object v0, v0, Lcom/google/android/gms/internal/jp;->b:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->c()Lcom/google/android/gms/internal/du;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/su;->b:Lcom/google/android/gms/internal/z;

    invoke-static {v0}, Lcom/google/android/gms/internal/z;->a(Lcom/google/android/gms/internal/z;)Lcom/google/android/gms/internal/sv;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/internal/sv;->v:Z

    return-void
.end method
