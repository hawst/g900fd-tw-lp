.class public Lcom/google/android/gms/car/eq;
.super Lcom/google/android/gms/common/internal/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/e",
        "<",
        "Lcom/google/android/gms/car/ay;",
        ">;"
    }
.end annotation


# instance fields
.field private final g:Ljava/lang/Object;

.field private h:Lcom/google/android/gms/car/aq;

.field private i:Lcom/google/android/gms/car/ak;

.field private final j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/google/android/gms/car/ag;

.field private final l:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final m:Lcom/google/android/gms/car/et;

.field private final n:Lcom/google/android/gms/common/api/q;

.field private o:Landroid/os/IBinder$DeathRecipient;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/car/e;Lcom/google/android/gms/common/api/q;Lcom/google/android/gms/common/api/r;)V
    .locals 7

    const/4 v6, 0x0

    new-array v5, v6, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/e;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/q;Lcom/google/android/gms/common/api/r;[Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/eq;->g:Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/eq;->j:Ljava/util/HashMap;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/car/eq;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Lcom/google/android/gms/car/et;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/et;-><init>(Lcom/google/android/gms/car/eq;)V

    iput-object v0, p0, Lcom/google/android/gms/car/eq;->m:Lcom/google/android/gms/car/et;

    new-instance v0, Lcom/google/android/gms/car/es;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/es;-><init>(Lcom/google/android/gms/car/eq;)V

    iput-object v0, p0, Lcom/google/android/gms/car/eq;->n:Lcom/google/android/gms/common/api/q;

    iget-object v0, p0, Lcom/google/android/gms/car/eq;->n:Lcom/google/android/gms/common/api/q;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/e;->e:Lcom/google/android/gms/common/internal/n;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/internal/n;->a(Lcom/google/android/gms/common/api/q;)V

    iget-object v0, p0, Lcom/google/android/gms/car/eq;->m:Lcom/google/android/gms/car/et;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/car/et;->a(Lcom/google/android/gms/car/e;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/eq;)Lcom/google/android/gms/car/et;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/eq;->m:Lcom/google/android/gms/car/et;

    return-object v0
.end method

.method private a(Landroid/os/RemoteException;)V
    .locals 3

    const/4 v1, 0x1

    const-string v0, "CAR.CLIENT"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Remote exception from car service:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->m:Lcom/google/android/gms/car/et;

    invoke-virtual {v0}, Lcom/google/android/gms/car/et;->a()V

    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->b()V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/car/eq;Landroid/os/RemoteException;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/car/eq;->a(Landroid/os/RemoteException;)V

    return-void
.end method

.method public static a(Ljava/lang/IllegalStateException;)V
    .locals 2

    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CarNotConnected"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0

    :cond_0
    throw p0
.end method

.method static synthetic b(Lcom/google/android/gms/car/eq;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/car/eq;->p()V

    return-void
.end method

.method private static b(Ljava/lang/IllegalStateException;)V
    .locals 2

    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CarNotConnected"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0

    :cond_0
    const-string v1, "CarNotSupported"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/car/ap;

    invoke-direct {v0}, Lcom/google/android/gms/car/ap;-><init>()V

    throw v0

    :cond_1
    throw p0
.end method

.method static synthetic c(Lcom/google/android/gms/car/eq;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/car/eq;->o()V

    return-void
.end method

.method private o()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/gms/car/eq;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->h:Lcom/google/android/gms/car/aq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/eq;->h:Lcom/google/android/gms/car/aq;

    iget-object v2, v0, Lcom/google/android/gms/car/aq;->b:Ljava/util/HashMap;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v3, v0, Lcom/google/android/gms/car/aq;->b:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    const/4 v3, 0x0

    iput-object v3, v0, Lcom/google/android/gms/car/aq;->a:Lcom/google/android/gms/car/at;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v2, v0, Lcom/google/android/gms/car/aq;->c:Ljava/util/HashMap;

    monitor-enter v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v0, v0, Lcom/google/android/gms/car/aq;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    const/4 v0, 0x0

    :try_start_4
    iput-object v0, p0, Lcom/google/android/gms/car/eq;->h:Lcom/google/android/gms/car/aq;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->k:Lcom/google/android/gms/car/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/eq;->k:Lcom/google/android/gms/car/ag;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    iget-object v2, v0, Lcom/google/android/gms/car/ag;->a:Lcom/google/android/gms/car/cx;

    iget-object v3, v0, Lcom/google/android/gms/car/ag;->b:Lcom/google/android/gms/car/aj;

    invoke-interface {v2, v3}, Lcom/google/android/gms/car/cx;->b(Lcom/google/android/gms/car/da;)V
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :goto_0
    const/4 v2, 0x0

    :try_start_6
    iput-object v2, v0, Lcom/google/android/gms/car/ag;->c:Lcom/google/android/gms/car/ai;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/eq;->k:Lcom/google/android/gms/car/ag;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->i:Lcom/google/android/gms/car/ak;

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/car/eq;->i:Lcom/google/android/gms/car/ak;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    iget-object v0, v2, Lcom/google/android/gms/car/ak;->a:Lcom/google/android/gms/car/dd;

    iget-object v3, v2, Lcom/google/android/gms/car/ak;->b:Lcom/google/android/gms/car/an;

    invoke-interface {v0, v3}, Lcom/google/android/gms/car/dd;->b(Lcom/google/android/gms/car/dg;)Z
    :try_end_7
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :goto_1
    const/4 v0, 0x0

    :try_start_8
    iput-object v0, v2, Lcom/google/android/gms/car/ak;->c:Lcom/google/android/gms/car/am;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/eq;->i:Lcom/google/android/gms/car/ak;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_9
    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :try_start_a
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    throw v0

    :catchall_2
    move-exception v0

    :try_start_b
    monitor-exit v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    :try_start_c
    throw v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/ak;->a(Landroid/os/RemoteException;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v2

    goto :goto_0

    :catch_3
    move-exception v2

    goto :goto_0
.end method

.method private declared-synchronized p()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->o:Landroid/os/IBinder$DeathRecipient;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/car/er;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/er;-><init>(Lcom/google/android/gms/car/eq;)V

    iput-object v0, p0, Lcom/google/android/gms/car/eq;->o:Landroid/os/IBinder$DeathRecipient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ay;

    invoke-interface {v0}, Lcom/google/android/gms/car/ay;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/eq;->o:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private declared-synchronized q()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->o:Landroid/os/IBinder$DeathRecipient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ay;

    invoke-interface {v0}, Lcom/google/android/gms/car/ay;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/eq;->o:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/gms/car/eq;->o:Landroid/os/IBinder$DeathRecipient;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/car/az;->a(Landroid/os/IBinder;)Lcom/google/android/gms/car/ay;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->l()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/eq;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ay;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/ay;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    monitor-exit v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/eq;->a(Landroid/os/RemoteException;)V

    monitor-exit v1

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->l()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/eq;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/car/ay;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    monitor-exit v1

    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/eq;->a(Landroid/os/RemoteException;)V

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->a()V

    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->l()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    const-string v0, "CAR.CLIENT"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "startActivityManager called for intent: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " from: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ay;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/ay;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No matching component for intent: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CarNotConnected"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    throw v0

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/eq;->a(Landroid/os/RemoteException;)V

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0

    :catch_2
    move-exception v0

    const-string v3, "CAR.CLIENT"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    :goto_1
    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SecurityException starting "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    throw v0

    :cond_5
    move v1, v2

    goto :goto_1

    :cond_6
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/internal/ab;Lcom/google/android/gms/common/internal/j;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "client_name"

    const-string v2, "car-1-0"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x645b68

    iget-object v2, p0, Lcom/google/android/gms/common/internal/e;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/ab;->o(Lcom/google/android/gms/common/internal/y;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method final a(Ljava/lang/String;Z)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->l()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/eq;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/car/ay;->a(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :try_start_1
    monitor-exit v1

    return v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/eq;->a(Landroid/os/RemoteException;)V

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/car/eq;->o()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ay;

    iget-object v1, p0, Lcom/google/android/gms/car/eq;->m:Lcom/google/android/gms/car/et;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/ay;->b(Lcom/google/android/gms/car/ci;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->m:Lcom/google/android/gms/car/et;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/et;->a(Lcom/google/android/gms/car/e;)V

    invoke-direct {p0}, Lcom/google/android/gms/car/eq;->q()V

    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->b()V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->l()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ay;

    invoke-interface {v0}, Lcom/google/android/gms/car/ay;->c()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/eq;->a(Landroid/os/RemoteException;)V

    move v0, v1

    goto :goto_0
.end method

.method public final d()I
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->l()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ay;

    invoke-interface {v0}, Lcom/google/android/gms/car/ay;->d()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    return v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/eq;->a(Landroid/os/RemoteException;)V

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CarNotConnected"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0

    :cond_1
    throw v0
.end method

.method public final e()Lcom/google/android/gms/car/CarUiInfo;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->l()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ay;

    invoke-interface {v0}, Lcom/google/android/gms/car/ay;->b()Lcom/google/android/gms/car/CarUiInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/eq;->a(Landroid/os/RemoteException;)V

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CarNotConnected"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0

    :cond_1
    throw v0
.end method

.method public final f()Lcom/google/android/gms/car/aq;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->l()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/eq;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->h:Lcom/google/android/gms/car/aq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ay;

    invoke-interface {v0}, Lcom/google/android/gms/car/ay;->e()Lcom/google/android/gms/car/du;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/car/aq;

    iget-object v3, p0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Looper;

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/car/aq;-><init>(Lcom/google/android/gms/car/du;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/gms/car/eq;->h:Lcom/google/android/gms/car/aq;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->h:Lcom/google/android/gms/car/aq;

    monitor-exit v1

    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/eq;->a(Landroid/os/RemoteException;)V

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/google/android/gms/car/eq;->b(Ljava/lang/IllegalStateException;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final g()Lcom/google/android/gms/car/ak;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->l()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/eq;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->i:Lcom/google/android/gms/car/ak;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ay;

    invoke-interface {v0}, Lcom/google/android/gms/car/ay;->g()Lcom/google/android/gms/car/dd;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/car/ak;

    iget-object v3, p0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Looper;

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/car/ak;-><init>(Lcom/google/android/gms/car/dd;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/gms/car/eq;->i:Lcom/google/android/gms/car/ak;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->i:Lcom/google/android/gms/car/ak;

    monitor-exit v1

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/eq;->b(Ljava/lang/IllegalStateException;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/eq;->a(Landroid/os/RemoteException;)V

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public final h()Lcom/google/android/gms/car/ag;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->l()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/eq;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->k:Lcom/google/android/gms/car/ag;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/eq;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ay;

    invoke-interface {v0}, Lcom/google/android/gms/car/ay;->l()Lcom/google/android/gms/car/cx;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/car/ag;

    iget-object v3, p0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Looper;

    invoke-direct {v2, v0, p0, v3}, Lcom/google/android/gms/car/ag;-><init>(Lcom/google/android/gms/car/cx;Lcom/google/android/gms/car/eq;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/gms/car/eq;->k:Lcom/google/android/gms/car/ag;

    iget-object v0, p0, Lcom/google/android/gms/car/eq;->k:Lcom/google/android/gms/car/ag;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v2, v0, Lcom/google/android/gms/car/ag;->a:Lcom/google/android/gms/car/cx;

    iget-object v0, v0, Lcom/google/android/gms/car/ag;->b:Lcom/google/android/gms/car/aj;

    invoke-interface {v2, v0}, Lcom/google/android/gms/car/cx;->a(Lcom/google/android/gms/car/da;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_0
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/car/eq;->k:Lcom/google/android/gms/car/ag;

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CarNotConnected"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CarNotConnected"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v0

    :cond_2
    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_2
    move-exception v0

    :try_start_7
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/eq;->a(Landroid/os/RemoteException;)V

    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catch_3
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/google/android/gms/car/ag;->a(Landroid/os/RemoteException;)V
    :try_end_8
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_0

    :cond_3
    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0
.end method

.method final i()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->l()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method protected final j()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.car.service.START"

    return-object v0
.end method

.method protected final k()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.car.ICar"

    return-object v0
.end method
