.class public final Lcom/google/android/gms/internal/ik;
.super Lcom/google/android/gms/internal/ii;

# interfaces
.implements Lcom/google/android/gms/common/api/q;
.implements Lcom/google/android/gms/common/api/r;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/internal/ih;

.field private final b:Lcom/google/android/gms/internal/il;

.field private final c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/fm;Lcom/google/android/gms/internal/ih;)V
    .locals 2

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/internal/ii;-><init>(Lcom/google/android/gms/internal/fm;Lcom/google/android/gms/internal/ih;)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ik;->c:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/gms/internal/ik;->a:Lcom/google/android/gms/internal/ih;

    new-instance v0, Lcom/google/android/gms/internal/il;

    iget-object v1, p2, Lcom/google/android/gms/internal/fm;->k:Lcom/google/android/gms/internal/gx;

    iget v1, v1, Lcom/google/android/gms/internal/gx;->d:I

    invoke-direct {v0, p1, p0, p0, v1}, Lcom/google/android/gms/internal/il;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/q;Lcom/google/android/gms/common/api/r;I)V

    iput-object v0, p0, Lcom/google/android/gms/internal/ik;->b:Lcom/google/android/gms/internal/il;

    iget-object v0, p0, Lcom/google/android/gms/internal/ik;->b:Lcom/google/android/gms/internal/il;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/il;->a()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/jy;->e:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/google/android/gms/internal/kb;->a(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/ik;->a:Lcom/google/android/gms/internal/ih;

    new-instance v1, Lcom/google/android/gms/internal/fo;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/fo;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/ih;->a(Lcom/google/android/gms/internal/fo;)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/ik;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ik;->b:Lcom/google/android/gms/internal/il;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/il;->l()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/ik;->b:Lcom/google/android/gms/internal/il;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/il;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ik;->b:Lcom/google/android/gms/internal/il;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/il;->b()V

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d()Lcom/google/android/gms/internal/ip;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/ik;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ik;->b:Lcom/google/android/gms/internal/il;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/il;->c()Lcom/google/android/gms/internal/ip;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    monitor-exit v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :goto_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catch_1
    move-exception v0

    goto :goto_1
.end method
