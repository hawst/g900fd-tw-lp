.class final Lcom/google/android/gms/internal/mc;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private static varargs a()Ljava/lang/Void;
    .locals 2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/google/android/gms/internal/mb;->b()Lcom/google/android/gms/ads/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/ads/a/a;->a()V
    :try_end_0
    .catch Lcom/google/android/gms/common/e; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/common/f; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    invoke-static {}, Lcom/google/android/gms/internal/mb;->c()Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-object v1

    :catch_0
    move-exception v0

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/internal/mb;->a(Z)Z

    invoke-static {v1}, Lcom/google/android/gms/internal/mb;->a(Lcom/google/android/gms/ads/a/a;)Lcom/google/android/gms/ads/a/a;

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v1}, Lcom/google/android/gms/internal/mb;->a(Lcom/google/android/gms/ads/a/a;)Lcom/google/android/gms/ads/a/a;

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-static {v1}, Lcom/google/android/gms/internal/mb;->a(Lcom/google/android/gms/ads/a/a;)Lcom/google/android/gms/ads/a/a;

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/internal/mc;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
