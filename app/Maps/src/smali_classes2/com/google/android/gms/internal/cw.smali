.class final Lcom/google/android/gms/internal/cw;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/da;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/gz;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/gz;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "1"

    const-string v1, "custom_close"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iget-object v1, p1, Lcom/google/android/gms/internal/gz;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p1, Lcom/google/android/gms/internal/gz;->f:Lcom/google/android/gms/internal/du;

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/google/android/gms/internal/gz;->f:Lcom/google/android/gms/internal/du;

    iget-object v3, v2, Lcom/google/android/gms/internal/du;->d:Lcom/google/android/gms/internal/dz;

    if-eqz v3, :cond_0

    iget-object v2, v2, Lcom/google/android/gms/internal/du;->d:Lcom/google/android/gms/internal/dz;

    iget-object v2, v2, Lcom/google/android/gms/internal/dz;->a:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_0
    :goto_1
    monitor-exit v1

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iput-boolean v0, p1, Lcom/google/android/gms/internal/gz;->i:Z

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
