.class public final Lcom/google/android/gms/internal/ja;
.super Lcom/google/android/gms/internal/iq;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:Lcom/google/android/gms/internal/ja;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/internal/jm;

.field private final e:Lcom/google/android/gms/internal/do;

.field private final f:Lcom/google/android/gms/internal/bx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ja;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/bx;Lcom/google/android/gms/internal/do;Lcom/google/android/gms/internal/jm;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/iq;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/ja;->c:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/gms/internal/ja;->d:Lcom/google/android/gms/internal/jm;

    iput-object p3, p0, Lcom/google/android/gms/internal/ja;->e:Lcom/google/android/gms/internal/do;

    iput-object p2, p0, Lcom/google/android/gms/internal/ja;->f:Lcom/google/android/gms/internal/bx;

    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/internal/bx;Lcom/google/android/gms/internal/do;Lcom/google/android/gms/internal/jm;Lcom/google/android/gms/internal/fm;)Lcom/google/android/gms/internal/fo;
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Lcom/google/android/gms/internal/jl;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/jl;-><init>(Landroid/content/Context;)V

    iget v1, v0, Lcom/google/android/gms/internal/jl;->l:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/fo;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/fo;-><init>(I)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v3, Lcom/google/android/gms/internal/jf;

    iget-object v1, p4, Lcom/google/android/gms/internal/fm;->f:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-direct {v3, v1}, Lcom/google/android/gms/internal/jf;-><init>(Ljava/lang/String;)V

    iget-object v1, p4, Lcom/google/android/gms/internal/fm;->c:Lcom/google/android/gms/internal/ba;

    iget-object v1, v1, Lcom/google/android/gms/internal/ba;->c:Landroid/os/Bundle;

    if-eqz v1, :cond_1

    iget-object v1, p4, Lcom/google/android/gms/internal/fm;->c:Lcom/google/android/gms/internal/ba;

    iget-object v1, v1, Lcom/google/android/gms/internal/ba;->c:Landroid/os/Bundle;

    const-string v2, "_ad"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {p0, p4, v1}, Lcom/google/android/gms/internal/je;->a(Landroid/content/Context;Lcom/google/android/gms/internal/fm;Ljava/lang/String;)Lcom/google/android/gms/internal/fo;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v5, p1, Lcom/google/android/gms/internal/bx;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/internal/bx;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/internal/bx;->c:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/gms/internal/bx;->d:Ljava/lang/String;

    invoke-static {p4, v0, v1, v2, v4}, Lcom/google/android/gms/internal/je;->a(Lcom/google/android/gms/internal/fm;Lcom/google/android/gms/internal/jl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/internal/fo;

    invoke-direct {v0, v6}, Lcom/google/android/gms/internal/fo;-><init>(I)V

    goto :goto_0

    :cond_2
    new-instance v4, Lcom/google/android/gms/internal/jd;

    invoke-direct {v4, v0}, Lcom/google/android/gms/internal/jd;-><init>(Ljava/lang/String;)V

    sget-object v6, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gms/internal/jb;

    move-object v1, p0

    move-object v2, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/jb;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/fm;Lcom/google/android/gms/internal/jf;Lcom/google/android/gms/internal/kz;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :try_start_0
    iget-object v0, v3, Lcom/google/android/gms/internal/jf;->c:Lcom/google/android/gms/internal/ki;

    const-wide/16 v4, 0xa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v4, v5, v1}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/jj;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_3

    :try_start_1
    new-instance v0, Lcom/google/android/gms/internal/fo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/fo;-><init>(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sget-object v1, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/internal/jc;

    invoke-direct {v2, v3}, Lcom/google/android/gms/internal/jc;-><init>(Lcom/google/android/gms/internal/jf;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v0, Lcom/google/android/gms/internal/fo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/fo;-><init>(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    sget-object v1, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/internal/jc;

    invoke-direct {v2, v3}, Lcom/google/android/gms/internal/jc;-><init>(Lcom/google/android/gms/internal/jf;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_3
    :try_start_3
    iget v1, v0, Lcom/google/android/gms/internal/jj;->f:I

    const/4 v2, -0x2

    if-eq v1, v2, :cond_4

    new-instance v1, Lcom/google/android/gms/internal/fo;

    iget v0, v0, Lcom/google/android/gms/internal/jj;->f:I

    invoke-direct {v1, v0}, Lcom/google/android/gms/internal/fo;-><init>(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    sget-object v0, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/internal/jc;

    invoke-direct {v2, v3}, Lcom/google/android/gms/internal/jc;-><init>(Lcom/google/android/gms/internal/jf;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-object v0, v1

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x0

    :try_start_4
    iget-boolean v2, v0, Lcom/google/android/gms/internal/jj;->d:Z

    if-eqz v2, :cond_5

    iget-object v1, p4, Lcom/google/android/gms/internal/fm;->g:Landroid/content/pm/PackageInfo;

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {p3}, Lcom/google/android/gms/internal/jm;->a()Ljava/lang/String;

    move-result-object v1

    :cond_5
    iget-object v2, p4, Lcom/google/android/gms/internal/fm;->k:Lcom/google/android/gms/internal/gx;

    iget-object v2, v2, Lcom/google/android/gms/internal/gx;->b:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/gms/internal/jj;->e:Ljava/lang/String;

    invoke-static {p0, v2, v4, v1, v0}, Lcom/google/android/gms/internal/ja;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/jj;)Lcom/google/android/gms/internal/fo;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/internal/jc;

    invoke-direct {v2, v3}, Lcom/google/android/gms/internal/jc;-><init>(Lcom/google/android/gms/internal/jf;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    sget-object v1, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/internal/jc;

    invoke-direct {v2, v3}, Lcom/google/android/gms/internal/jc;-><init>(Lcom/google/android/gms/internal/jf;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    throw v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/jj;)Lcom/google/android/gms/internal/fo;
    .locals 26

    :try_start_0
    new-instance v25, Lcom/google/android/gms/internal/ji;

    invoke-direct/range {v25 .. v25}, Lcom/google/android/gms/internal/ji;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AdRequestServiceImpl: Sending request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/net/URL;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v18

    move-object v4, v3

    move v3, v2

    :goto_0
    invoke-virtual {v4}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v5, 0x0

    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5, v2}, Lcom/google/android/gms/internal/kf;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/net/HttpURLConnection;)V

    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "x-afma-drt-cookie"

    move-object/from16 v0, p3

    invoke-virtual {v2, v5, v0}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz p4, :cond_1

    move-object/from16 v0, p4

    iget-object v5, v0, Lcom/google/android/gms/internal/jj;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    move-object/from16 v0, p4

    iget-object v5, v0, Lcom/google/android/gms/internal/jj;->b:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    array-length v6, v5

    invoke-virtual {v2, v6}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    new-instance v6, Ljava/io/BufferedOutputStream;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v6, v5}, Ljava/io/BufferedOutputStream;->write([B)V

    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V

    :cond_1
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v6

    const/16 v7, 0xc8

    if-lt v5, v7, :cond_2

    const/16 v7, 0x12c

    if-ge v5, v7, :cond_2

    invoke-virtual {v4}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v4, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-static {v4}, Lcom/google/android/gms/internal/kf;->a(Ljava/lang/Readable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v6, v4, v5}, Lcom/google/android/gms/internal/ja;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)V

    move-object/from16 v0, v25

    iput-object v3, v0, Lcom/google/android/gms/internal/ji;->b:Ljava/lang/String;

    move-object/from16 v0, v25

    iput-object v4, v0, Lcom/google/android/gms/internal/ji;->c:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Lcom/google/android/gms/internal/ji;->a(Ljava/util/Map;)V

    new-instance v3, Lcom/google/android/gms/internal/fo;

    move-object/from16 v0, v25

    iget-object v4, v0, Lcom/google/android/gms/internal/ji;->b:Ljava/lang/String;

    move-object/from16 v0, v25

    iget-object v5, v0, Lcom/google/android/gms/internal/ji;->c:Ljava/lang/String;

    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/google/android/gms/internal/ji;->d:Ljava/util/List;

    move-object/from16 v0, v25

    iget-object v7, v0, Lcom/google/android/gms/internal/ji;->g:Ljava/util/List;

    move-object/from16 v0, v25

    iget-wide v8, v0, Lcom/google/android/gms/internal/ji;->h:J

    move-object/from16 v0, v25

    iget-boolean v10, v0, Lcom/google/android/gms/internal/ji;->i:Z

    const-wide/16 v11, -0x1

    move-object/from16 v0, v25

    iget-object v13, v0, Lcom/google/android/gms/internal/ji;->j:Ljava/util/List;

    move-object/from16 v0, v25

    iget-wide v14, v0, Lcom/google/android/gms/internal/ji;->k:J

    move-object/from16 v0, v25

    iget v0, v0, Lcom/google/android/gms/internal/ji;->l:I

    move/from16 v16, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/google/android/gms/internal/ji;->a:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/google/android/gms/internal/ji;->e:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/google/android/gms/internal/ji;->f:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/google/android/gms/internal/ji;->m:Z

    move/from16 v22, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/google/android/gms/internal/ji;->n:Z

    move/from16 v23, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/google/android/gms/internal/ji;->o:Z

    move/from16 v24, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/google/android/gms/internal/ji;->p:Z

    move/from16 v25, v0

    invoke-direct/range {v3 .. v25}, Lcom/google/android/gms/internal/fo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;JZJLjava/util/List;JILjava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZZZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_1
    return-object v3

    :cond_2
    :try_start_3
    invoke-virtual {v4}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    invoke-static {v4, v6, v7, v5}, Lcom/google/android/gms/internal/ja;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)V

    const/16 v4, 0x12c

    if-lt v5, v4, :cond_4

    const/16 v4, 0x190

    if-ge v5, v4, :cond_4

    const-string v4, "Location"

    invoke-virtual {v2, v4}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v3, Lcom/google/android/gms/internal/fo;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/gms/internal/fo;-><init>(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error while connecting to ad server: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Lcom/google/android/gms/internal/fo;

    const/4 v2, 0x2

    invoke-direct {v3, v2}, Lcom/google/android/gms/internal/fo;-><init>(I)V

    goto :goto_1

    :cond_3
    :try_start_5
    new-instance v4, Ljava/net/URL;

    invoke-direct {v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    const/4 v5, 0x5

    if-le v3, v5, :cond_5

    new-instance v3, Lcom/google/android/gms/internal/fo;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/gms/internal/fo;-><init>(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_1

    :cond_4
    :try_start_7
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received error HTTP response code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    new-instance v3, Lcom/google/android/gms/internal/fo;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/gms/internal/fo;-><init>(I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_1

    :cond_5
    :try_start_9
    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Lcom/google/android/gms/internal/ji;->a(Ljava/util/Map;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :try_start_a
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v3
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/bx;Lcom/google/android/gms/internal/do;Lcom/google/android/gms/internal/jm;)Lcom/google/android/gms/internal/ja;
    .locals 3

    sget-object v1, Lcom/google/android/gms/internal/ja;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/internal/ja;->b:Lcom/google/android/gms/internal/ja;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/ja;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, p1, p2, p3}, Lcom/google/android/gms/internal/ja;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/bx;Lcom/google/android/gms/internal/do;Lcom/google/android/gms/internal/jm;)V

    sput-object v0, Lcom/google/android/gms/internal/ja;->b:Lcom/google/android/gms/internal/ja;

    :cond_0
    sget-object v0, Lcom/google/android/gms/internal/ja;->b:Lcom/google/android/gms/internal/ja;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    const/4 v0, 0x2

    const-string v1, "Ads"

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/fm;)Lcom/google/android/gms/internal/fo;
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/internal/ja;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/internal/ja;->f:Lcom/google/android/gms/internal/bx;

    iget-object v2, p0, Lcom/google/android/gms/internal/ja;->e:Lcom/google/android/gms/internal/do;

    iget-object v3, p0, Lcom/google/android/gms/internal/ja;->d:Lcom/google/android/gms/internal/jm;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/google/android/gms/internal/ja;->a(Landroid/content/Context;Lcom/google/android/gms/internal/bx;Lcom/google/android/gms/internal/do;Lcom/google/android/gms/internal/jm;Lcom/google/android/gms/internal/fm;)Lcom/google/android/gms/internal/fo;

    move-result-object v0

    return-object v0
.end method
