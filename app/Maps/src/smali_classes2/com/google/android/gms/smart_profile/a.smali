.class public final Lcom/google/android/gms/smart_profile/a;
.super Lcom/google/android/gms/a/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/a/t",
        "<",
        "Lcom/google/android/gms/smart_profile/e;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/google/android/gms/smart_profile/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/smart_profile/a;

    invoke-direct {v0}, Lcom/google/android/gms/smart_profile/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/smart_profile/a;->a:Lcom/google/android/gms/smart_profile/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const-string v0, "com.google.android.gms.smart_profile.HeaderViewCreatorImpl"

    invoke-direct {p0, v0}, Lcom/google/android/gms/a/t;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static b(Landroid/content/Context;)Lcom/google/android/gms/smart_profile/h;
    .locals 1

    sget-object v0, Lcom/google/android/gms/smart_profile/a;->a:Lcom/google/android/gms/smart_profile/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/smart_profile/a;->c(Landroid/content/Context;)Lcom/google/android/gms/smart_profile/h;

    move-result-object v0

    return-object v0
.end method

.method private c(Landroid/content/Context;)Lcom/google/android/gms/smart_profile/h;
    .locals 3

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/smart_profile/a;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/e;

    invoke-interface {v0, v1}, Lcom/google/android/gms/smart_profile/e;->a(Lcom/google/android/gms/a/o;)Lcom/google/android/gms/smart_profile/h;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/a/u;

    const-string v2, "Failed to create header view"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/a/u;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final synthetic a(Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/smart_profile/f;->a(Landroid/os/IBinder;)Lcom/google/android/gms/smart_profile/e;

    move-result-object v0

    return-object v0
.end method
