.class Lcom/google/android/gms/internal/fz;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/internal/gc;

.field final synthetic b:Landroid/content/Intent;

.field final synthetic c:Lcom/google/android/gms/internal/fy;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/fy;Lcom/google/android/gms/internal/gc;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/fz;->c:Lcom/google/android/gms/internal/fy;

    iput-object p2, p0, Lcom/google/android/gms/internal/fz;->a:Lcom/google/android/gms/internal/gc;

    iput-object p3, p0, Lcom/google/android/gms/internal/fz;->b:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/fz;->c:Lcom/google/android/gms/internal/fy;

    invoke-static {v0}, Lcom/google/android/gms/internal/fy;->a(Lcom/google/android/gms/internal/fy;)Lcom/google/android/gms/internal/gi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/fz;->a:Lcom/google/android/gms/internal/gc;

    iget-object v1, v1, Lcom/google/android/gms/internal/gc;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/internal/fz;->b:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/gi;->a(Ljava/lang/String;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/fz;->c:Lcom/google/android/gms/internal/fy;

    invoke-static {v0}, Lcom/google/android/gms/internal/fy;->c(Lcom/google/android/gms/internal/fy;)Lcom/google/android/gms/internal/gv;

    move-result-object v7

    new-instance v0, Lcom/google/android/gms/internal/gd;

    iget-object v1, p0, Lcom/google/android/gms/internal/fz;->c:Lcom/google/android/gms/internal/fy;

    invoke-static {v1}, Lcom/google/android/gms/internal/fy;->b(Lcom/google/android/gms/internal/fy;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/fz;->a:Lcom/google/android/gms/internal/gc;

    iget-object v2, v2, Lcom/google/android/gms/internal/gc;->c:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, -0x1

    iget-object v5, p0, Lcom/google/android/gms/internal/fz;->b:Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/gms/internal/fz;->a:Lcom/google/android/gms/internal/gc;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/gd;-><init>(Landroid/content/Context;Ljava/lang/String;ZILandroid/content/Intent;Lcom/google/android/gms/internal/gc;)V

    invoke-interface {v7, v0}, Lcom/google/android/gms/internal/gv;->a(Lcom/google/android/gms/internal/gs;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/fz;->c:Lcom/google/android/gms/internal/fy;

    invoke-static {v0}, Lcom/google/android/gms/internal/fy;->c(Lcom/google/android/gms/internal/fy;)Lcom/google/android/gms/internal/gv;

    move-result-object v7

    new-instance v0, Lcom/google/android/gms/internal/gd;

    iget-object v1, p0, Lcom/google/android/gms/internal/fz;->c:Lcom/google/android/gms/internal/fy;

    invoke-static {v1}, Lcom/google/android/gms/internal/fy;->b(Lcom/google/android/gms/internal/fy;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/fz;->a:Lcom/google/android/gms/internal/gc;

    iget-object v2, v2, Lcom/google/android/gms/internal/gc;->c:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, -0x1

    iget-object v5, p0, Lcom/google/android/gms/internal/fz;->b:Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/gms/internal/fz;->a:Lcom/google/android/gms/internal/gc;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/gd;-><init>(Landroid/content/Context;Ljava/lang/String;ZILandroid/content/Intent;Lcom/google/android/gms/internal/gc;)V

    invoke-interface {v7, v0}, Lcom/google/android/gms/internal/gv;->a(Lcom/google/android/gms/internal/gs;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
