.class public Lcom/google/android/gms/people/l;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/b;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/internal/qb;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;ILjava/lang/String;)V
    .locals 6

    new-instance v0, Lcom/google/android/gms/internal/qb;

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/qb;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/l;-><init>(Lcom/google/android/gms/internal/qb;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/internal/qb;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/people/l;->a:Lcom/google/android/gms/internal/qb;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/l;->a:Lcom/google/android/gms/internal/qb;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/qb;->a()V

    return-void
.end method

.method public final a(Lcom/google/android/gms/people/n;Z)V
    .locals 7

    const/4 v4, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/people/l;->a:Lcom/google/android/gms/internal/qb;

    new-instance v1, Lcom/google/android/gms/people/o;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/o;-><init>(Lcom/google/android/gms/people/n;)V

    move v3, p2

    move-object v5, v4

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/internal/qb;->a(Lcom/google/android/gms/common/api/m;ZZLjava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/people/m;I)Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/people/l;->a:Lcom/google/android/gms/internal/qb;

    invoke-virtual {v0, p1, v1, v1, p2}, Lcom/google/android/gms/internal/qb;->a(Lcom/google/android/gms/people/m;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/l;->a:Lcom/google/android/gms/internal/qb;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/qb;->b()V

    return-void
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/l;->a:Lcom/google/android/gms/internal/qb;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/qb;->l()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/l;->a:Lcom/google/android/gms/internal/qb;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/qb;->m()Z

    move-result v0

    return v0
.end method
