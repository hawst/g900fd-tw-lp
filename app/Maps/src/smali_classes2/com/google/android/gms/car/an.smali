.class Lcom/google/android/gms/car/an;
.super Lcom/google/android/gms/car/dh;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/ak;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/ak;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/an;->a:Lcom/google/android/gms/car/ak;

    invoke-direct {p0}, Lcom/google/android/gms/car/dh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/car/an;->a:Lcom/google/android/gms/car/ak;

    iget-object v0, v0, Lcom/google/android/gms/car/ak;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/an;->a:Lcom/google/android/gms/car/ak;

    iget-object v1, v1, Lcom/google/android/gms/car/ak;->i:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(IIIII)V
    .locals 3

    const/4 v1, 0x1

    const-string v0, "CAR.SENSOR"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "onStart("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/an;->a:Lcom/google/android/gms/car/ak;

    iput p1, v0, Lcom/google/android/gms/car/ak;->d:I

    iget-object v0, p0, Lcom/google/android/gms/car/an;->a:Lcom/google/android/gms/car/ak;

    iput p2, v0, Lcom/google/android/gms/car/ak;->e:I

    iget-object v0, p0, Lcom/google/android/gms/car/an;->a:Lcom/google/android/gms/car/ak;

    iput p3, v0, Lcom/google/android/gms/car/ak;->f:I

    iget-object v0, p0, Lcom/google/android/gms/car/an;->a:Lcom/google/android/gms/car/ak;

    iput p4, v0, Lcom/google/android/gms/car/ak;->g:I

    iget-object v0, p0, Lcom/google/android/gms/car/an;->a:Lcom/google/android/gms/car/ak;

    iput p5, v0, Lcom/google/android/gms/car/ak;->h:I

    iget-object v0, p0, Lcom/google/android/gms/car/an;->a:Lcom/google/android/gms/car/ak;

    iget-object v0, v0, Lcom/google/android/gms/car/ak;->i:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/car/an;->a:Lcom/google/android/gms/car/ak;

    iget-object v2, v2, Lcom/google/android/gms/car/ak;->i:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
