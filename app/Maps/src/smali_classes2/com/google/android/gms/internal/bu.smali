.class public Lcom/google/android/gms/internal/bu;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lcom/google/android/gms/internal/eh;

.field public final b:Landroid/content/Context;

.field public final c:Lcom/google/android/gms/internal/az;

.field public d:Lcom/google/android/gms/ads/a;

.field public e:Lcom/google/android/gms/internal/bh;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Lcom/google/android/gms/internal/az;->a()Lcom/google/android/gms/internal/az;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/internal/bu;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/az;Lcom/google/android/gms/ads/doubleclick/a;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/az;Lcom/google/android/gms/ads/doubleclick/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/eh;

    invoke-direct {v0}, Lcom/google/android/gms/internal/eh;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/bu;->a:Lcom/google/android/gms/internal/eh;

    iput-object p1, p0, Lcom/google/android/gms/internal/bu;->b:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/internal/bu;->c:Lcom/google/android/gms/internal/az;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/ads/a;)V
    .locals 2

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/internal/bu;->d:Lcom/google/android/gms/ads/a;

    iget-object v0, p0, Lcom/google/android/gms/internal/bu;->e:Lcom/google/android/gms/internal/bh;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/bu;->e:Lcom/google/android/gms/internal/bh;

    if-eqz p1, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/aw;

    invoke-direct {v0, p1}, Lcom/google/android/gms/internal/aw;-><init>(Lcom/google/android/gms/ads/a;)V

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/gms/internal/bh;->a(Lcom/google/android/gms/internal/bc;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/internal/br;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/bu;->e:Lcom/google/android/gms/internal/bh;

    if-nez v0, :cond_1

    const-string v0, "loadAd"

    iget-object v1, p0, Lcom/google/android/gms/internal/bu;->f:Ljava/lang/String;

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/bu;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/bu;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/internal/bd;

    invoke-direct {v1}, Lcom/google/android/gms/internal/bd;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/internal/bu;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/internal/bu;->a:Lcom/google/android/gms/internal/eh;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/internal/ax;->a(Landroid/content/Context;Lcom/google/android/gms/internal/bd;Ljava/lang/String;Lcom/google/android/gms/internal/eh;)Lcom/google/android/gms/internal/bh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bu;->e:Lcom/google/android/gms/internal/bh;

    iget-object v0, p0, Lcom/google/android/gms/internal/bu;->d:Lcom/google/android/gms/ads/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/bu;->e:Lcom/google/android/gms/internal/bh;

    new-instance v1, Lcom/google/android/gms/internal/aw;

    iget-object v2, p0, Lcom/google/android/gms/internal/bu;->d:Lcom/google/android/gms/ads/a;

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/aw;-><init>(Lcom/google/android/gms/ads/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/bh;->a(Lcom/google/android/gms/internal/bc;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/bu;->e:Lcom/google/android/gms/internal/bh;

    iget-object v1, p0, Lcom/google/android/gms/internal/bu;->c:Lcom/google/android/gms/internal/az;

    iget-object v1, p0, Lcom/google/android/gms/internal/bu;->b:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/google/android/gms/internal/az;->a(Landroid/content/Context;Lcom/google/android/gms/internal/br;)Lcom/google/android/gms/internal/ba;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/bh;->a(Lcom/google/android/gms/internal/ba;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/bu;->a:Lcom/google/android/gms/internal/eh;

    iget-object v1, p1, Lcom/google/android/gms/internal/br;->i:Ljava/util/Map;

    iput-object v1, v0, Lcom/google/android/gms/internal/eh;->a:Ljava/util/Map;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/bu;->e:Lcom/google/android/gms/internal/bh;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The ad unit ID must be set on InterstitialAd before "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is called."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method
