.class public Lcom/google/android/gms/appdatasearch/c;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/appdatasearch/DocumentSection;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Landroid/accounts/Account;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/appdatasearch/DocumentContents;
    .locals 6

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/gms/appdatasearch/DocumentContents;

    iget-boolean v3, p0, Lcom/google/android/gms/appdatasearch/c;->b:Z

    iget-object v4, p0, Lcom/google/android/gms/appdatasearch/c;->c:Landroid/accounts/Account;

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->a:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/gms/appdatasearch/c;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lcom/google/android/gms/appdatasearch/DocumentSection;

    invoke-interface {v0, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/DocumentSection;

    :goto_0
    invoke-direct {v2, v1, v3, v4, v0}, Lcom/google/android/gms/appdatasearch/DocumentContents;-><init>(Ljava/lang/String;ZLandroid/accounts/Account;[Lcom/google/android/gms/appdatasearch/DocumentSection;)V

    return-object v2

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;)Lcom/google/android/gms/appdatasearch/c;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/c;->c:Landroid/accounts/Account;

    return-object p0
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/DocumentSection;)Lcom/google/android/gms/appdatasearch/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->a:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->a:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/appdatasearch/c;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/appdatasearch/c;->b:Z

    return-object p0
.end method
