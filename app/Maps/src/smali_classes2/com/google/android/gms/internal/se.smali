.class public abstract Lcom/google/android/gms/internal/se;
.super Lcom/google/android/gms/internal/sm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcom/google/android/gms/internal/se",
        "<TM;>;>",
        "Lcom/google/android/gms/internal/sm;"
    }
.end annotation


# instance fields
.field public o:Lcom/google/android/gms/internal/sf;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/sm;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    if-eqz v1, :cond_0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    iget v2, v2, Lcom/google/android/gms/internal/sf;->d:I

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    iget-object v2, v2, Lcom/google/android/gms/internal/sf;->c:[Lcom/google/android/gms/internal/sg;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/gms/internal/sg;->a()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    :cond_1
    return v1
.end method

.method public a(Lcom/google/android/gms/internal/sc;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    iget v1, v1, Lcom/google/android/gms/internal/sf;->d:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    iget-object v1, v1, Lcom/google/android/gms/internal/sf;->c:[Lcom/google/android/gms/internal/sg;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/sg;->a(Lcom/google/android/gms/internal/sc;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/internal/sb;I)Z
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v0, p1, Lcom/google/android/gms/internal/sb;->c:I

    iget v3, p1, Lcom/google/android/gms/internal/sb;->b:I

    sub-int v3, v0, v3

    invoke-virtual {p1, p2}, Lcom/google/android/gms/internal/sb;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-static {p2}, Lcom/google/android/gms/internal/sp;->b(I)I

    move-result v4

    iget v0, p1, Lcom/google/android/gms/internal/sb;->c:I

    iget v5, p1, Lcom/google/android/gms/internal/sb;->b:I

    sub-int/2addr v0, v5

    sub-int v5, v0, v3

    if-nez v5, :cond_2

    sget-object v0, Lcom/google/android/gms/internal/sp;->e:[B

    :goto_1
    new-instance v3, Lcom/google/android/gms/internal/so;

    invoke-direct {v3, p2, v0}, Lcom/google/android/gms/internal/so;-><init>(I[B)V

    iget-object v0, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/internal/sf;

    invoke-direct {v0}, Lcom/google/android/gms/internal/sf;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    move-object v0, v1

    :goto_2
    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/sg;

    invoke-direct {v0}, Lcom/google/android/gms/internal/sg;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/internal/sf;->b(I)I

    move-result v5

    if-ltz v5, :cond_6

    iget-object v1, v1, Lcom/google/android/gms/internal/sf;->c:[Lcom/google/android/gms/internal/sg;

    aput-object v0, v1, v5

    :cond_1
    :goto_3
    iget-object v0, v0, Lcom/google/android/gms/internal/sg;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    new-array v0, v5, [B

    iget v6, p1, Lcom/google/android/gms/internal/sb;->b:I

    add-int/2addr v3, v6

    iget-object v6, p1, Lcom/google/android/gms/internal/sb;->a:[B

    invoke-static {v6, v3, v0, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/internal/sf;->b(I)I

    move-result v5

    if-ltz v5, :cond_4

    iget-object v6, v0, Lcom/google/android/gms/internal/sf;->c:[Lcom/google/android/gms/internal/sg;

    aget-object v6, v6, v5

    sget-object v7, Lcom/google/android/gms/internal/sf;->a:Lcom/google/android/gms/internal/sg;

    if-ne v6, v7, :cond_5

    :cond_4
    move-object v0, v1

    goto :goto_2

    :cond_5
    iget-object v0, v0, Lcom/google/android/gms/internal/sf;->c:[Lcom/google/android/gms/internal/sg;

    aget-object v0, v0, v5

    goto :goto_2

    :cond_6
    xor-int/lit8 v5, v5, -0x1

    iget v6, v1, Lcom/google/android/gms/internal/sf;->d:I

    if-ge v5, v6, :cond_7

    iget-object v6, v1, Lcom/google/android/gms/internal/sf;->c:[Lcom/google/android/gms/internal/sg;

    aget-object v6, v6, v5

    sget-object v7, Lcom/google/android/gms/internal/sf;->a:Lcom/google/android/gms/internal/sg;

    if-ne v6, v7, :cond_7

    iget-object v2, v1, Lcom/google/android/gms/internal/sf;->b:[I

    aput v4, v2, v5

    iget-object v1, v1, Lcom/google/android/gms/internal/sf;->c:[Lcom/google/android/gms/internal/sg;

    aput-object v0, v1, v5

    goto :goto_3

    :cond_7
    iget v6, v1, Lcom/google/android/gms/internal/sf;->d:I

    iget-object v7, v1, Lcom/google/android/gms/internal/sf;->b:[I

    array-length v7, v7

    if-lt v6, v7, :cond_8

    iget v6, v1, Lcom/google/android/gms/internal/sf;->d:I

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v1, v6}, Lcom/google/android/gms/internal/sf;->a(I)I

    move-result v6

    new-array v7, v6, [I

    new-array v6, v6, [Lcom/google/android/gms/internal/sg;

    iget-object v8, v1, Lcom/google/android/gms/internal/sf;->b:[I

    iget-object v9, v1, Lcom/google/android/gms/internal/sf;->b:[I

    array-length v9, v9

    invoke-static {v8, v2, v7, v2, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v8, v1, Lcom/google/android/gms/internal/sf;->c:[Lcom/google/android/gms/internal/sg;

    iget-object v9, v1, Lcom/google/android/gms/internal/sf;->c:[Lcom/google/android/gms/internal/sg;

    array-length v9, v9

    invoke-static {v8, v2, v6, v2, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v7, v1, Lcom/google/android/gms/internal/sf;->b:[I

    iput-object v6, v1, Lcom/google/android/gms/internal/sf;->c:[Lcom/google/android/gms/internal/sg;

    :cond_8
    iget v2, v1, Lcom/google/android/gms/internal/sf;->d:I

    sub-int/2addr v2, v5

    if-eqz v2, :cond_9

    iget-object v2, v1, Lcom/google/android/gms/internal/sf;->b:[I

    iget-object v6, v1, Lcom/google/android/gms/internal/sf;->b:[I

    add-int/lit8 v7, v5, 0x1

    iget v8, v1, Lcom/google/android/gms/internal/sf;->d:I

    sub-int/2addr v8, v5

    invoke-static {v2, v5, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, v1, Lcom/google/android/gms/internal/sf;->c:[Lcom/google/android/gms/internal/sg;

    iget-object v6, v1, Lcom/google/android/gms/internal/sf;->c:[Lcom/google/android/gms/internal/sg;

    add-int/lit8 v7, v5, 0x1

    iget v8, v1, Lcom/google/android/gms/internal/sf;->d:I

    sub-int/2addr v8, v5

    invoke-static {v2, v5, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9
    iget-object v2, v1, Lcom/google/android/gms/internal/sf;->b:[I

    aput v4, v2, v5

    iget-object v2, v1, Lcom/google/android/gms/internal/sf;->c:[Lcom/google/android/gms/internal/sg;

    aput-object v0, v2, v5

    iget v2, v1, Lcom/google/android/gms/internal/sf;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/android/gms/internal/sf;->d:I

    goto/16 :goto_3
.end method

.method public final a(Lcom/google/android/gms/internal/se;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)Z"
        }
    .end annotation

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    iget v2, v2, Lcom/google/android/gms/internal/sf;->d:I

    if-nez v2, :cond_3

    move v2, v1

    :goto_0
    if-eqz v2, :cond_5

    :cond_0
    iget-object v2, p1, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    iget v2, v2, Lcom/google/android/gms/internal/sf;->d:I

    if-nez v2, :cond_4

    move v2, v1

    :goto_1
    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    :goto_2
    return v0

    :cond_3
    move v2, v0

    goto :goto_0

    :cond_4
    move v2, v0

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    iget-object v1, p1, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/sf;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_2
.end method

.method public final c()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    iget v1, v1, Lcom/google/android/gms/internal/sf;->d:I

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_2

    :cond_0
    :goto_1
    return v0

    :cond_1
    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/se;->o:Lcom/google/android/gms/internal/sf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/sf;->hashCode()I

    move-result v0

    goto :goto_1
.end method
