.class public final Lcom/google/android/gms/internal/sb;
.super Ljava/lang/Object;


# instance fields
.field final a:[B

.field public b:I

.field public c:I

.field d:I

.field public e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method constructor <init>([BII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/internal/sb;->e:I

    const/16 v0, 0x40

    iput v0, p0, Lcom/google/android/gms/internal/sb;->i:I

    iput-object p1, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iput p2, p0, Lcom/google/android/gms/internal/sb;->b:I

    add-int v0, p2, p3

    iput v0, p0, Lcom/google/android/gms/internal/sb;->f:I

    iput p2, p0, Lcom/google/android/gms/internal/sb;->c:I

    return-void
.end method

.method private d(I)[B
    .locals 4

    if-gez p1, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/si;->b()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/google/android/gms/internal/sb;->e:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/gms/internal/sb;->e:I

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/sb;->e(I)V

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_1
    iget v0, p0, Lcom/google/android/gms/internal/sb;->f:I

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    sub-int/2addr v0, v1

    if-gt p1, v0, :cond_2

    new-array v0, p1, [B

    iget-object v1, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/2addr v1, p1

    iput v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    return-object v0

    :cond_2
    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0
.end method

.method private e(I)V
    .locals 2

    if-gez p1, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/si;->b()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/google/android/gms/internal/sb;->e:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/gms/internal/sb;->e:I

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/sb;->e(I)V

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_1
    iget v0, p0, Lcom/google/android/gms/internal/sb;->f:I

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    sub-int/2addr v0, v1

    if-gt p1, v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/gms/internal/sb;->c:I

    return-void

    :cond_2
    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v2, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    iput v0, p0, Lcom/google/android/gms/internal/sb;->d:I

    :goto_1
    return v0

    :cond_0
    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/sb;->d:I

    iget v0, p0, Lcom/google/android/gms/internal/sb;->d:I

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/android/gms/internal/si;->d()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_2
    iget v0, p0, Lcom/google/android/gms/internal/sb;->d:I

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/internal/sm;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/internal/sb;->h:I

    iget v2, p0, Lcom/google/android/gms/internal/sb;->i:I

    if-lt v1, v2, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/si;->g()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/sb;->b(I)I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/internal/sb;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/internal/sb;->h:I

    invoke-virtual {p1, p0}, Lcom/google/android/gms/internal/sm;->a(Lcom/google/android/gms/internal/sb;)Lcom/google/android/gms/internal/sm;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/internal/sb;->d:I

    if-eq v2, v1, :cond_1

    invoke-static {}, Lcom/google/android/gms/internal/si;->e()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_1
    iget v1, p0, Lcom/google/android/gms/internal/sb;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/gms/internal/sb;->h:I

    iput v0, p0, Lcom/google/android/gms/internal/sb;->e:I

    invoke-virtual {p0}, Lcom/google/android/gms/internal/sb;->h()V

    return-void
.end method

.method public final a(I)Z
    .locals 3

    const/4 v0, 0x1

    invoke-static {p1}, Lcom/google/android/gms/internal/sp;->a(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lcom/google/android/gms/internal/si;->f()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/sb;->d()I

    :cond_0
    :goto_0
    return v0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/sb;->g()J

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/internal/sb;->e(I)V

    goto :goto_0

    :cond_1
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/gms/internal/sb;->a()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/android/gms/internal/sb;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/internal/sp;->b(I)I

    move-result v1

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/sp;->a(II)I

    move-result v1

    iget v2, p0, Lcom/google/android/gms/internal/sb;->d:I

    if-eq v2, v1, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/si;->e()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :pswitch_4
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0}, Lcom/google/android/gms/internal/sb;->f()I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final b(I)I
    .locals 2

    if-gez p1, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/si;->b()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/google/android/gms/internal/sb;->e:I

    if-le v0, v1, :cond_1

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_1
    iput v0, p0, Lcom/google/android/gms/internal/sb;->e:I

    invoke-virtual {p0}, Lcom/google/android/gms/internal/sb;->h()V

    return v1
.end method

.method public final b()Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v1

    iget v0, p0, Lcom/google/android/gms/internal/sb;->f:I

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    sub-int/2addr v0, v2

    if-gt v1, v0, :cond_0

    if-lez v1, :cond_0

    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v3, p0, Lcom/google/android/gms/internal/sb;->c:I

    const-string v4, "UTF-8"

    invoke-direct {v0, v2, v3, v1, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/gms/internal/sb;->d(I)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 4

    iget v0, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v1, p0, Lcom/google/android/gms/internal/sb;->b:I

    sub-int/2addr v0, v1

    if-le p1, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is beyond current "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v3, p0, Lcom/google/android/gms/internal/sb;->b:I

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-gez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lcom/google/android/gms/internal/sb;->b:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/gms/internal/sb;->c:I

    return-void
.end method

.method public final c()[B
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/gms/internal/sb;->d()I

    move-result v1

    iget v0, p0, Lcom/google/android/gms/internal/sb;->f:I

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    sub-int/2addr v0, v2

    if-gt v1, v0, :cond_0

    if-lez v1, :cond_0

    new-array v0, v1, [B

    iget-object v2, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v3, p0, Lcom/google/android/gms/internal/sb;->c:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/gms/internal/sb;->d(I)[B

    move-result-object v0

    goto :goto_0
.end method

.method public final d()I
    .locals 5

    iget v0, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v1, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v0, v0, v1

    if-ltz v0, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    and-int/lit8 v0, v0, 0x7f

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v2, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v1, v2, :cond_3

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v1, v1, v2

    if-ltz v1, :cond_4

    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    goto :goto_0

    :cond_4
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v2, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v1, v2, :cond_5

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v1, v1, v2

    if-ltz v1, :cond_6

    shl-int/lit8 v1, v1, 0xe

    or-int/2addr v0, v1

    goto :goto_0

    :cond_6
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0xe

    or-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v2, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v1, v2, :cond_7

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v1, v1, v2

    if-ltz v1, :cond_8

    shl-int/lit8 v1, v1, 0x15

    or-int/2addr v0, v1

    goto :goto_0

    :cond_8
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0x15

    or-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v2, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v1, v2, :cond_9

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v1, v1, v2

    shl-int/lit8 v2, v1, 0x1c

    or-int/2addr v0, v2

    if-gez v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    const/4 v2, 0x5

    if-ge v1, v2, :cond_b

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v3, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v2, v3, :cond_a

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v3, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v2, v2, v3

    if-gez v2, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_b
    invoke-static {}, Lcom/google/android/gms/internal/si;->c()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0
.end method

.method public final e()J
    .locals 6

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    :goto_0
    const/16 v3, 0x40

    if-ge v2, v3, :cond_2

    iget v3, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v4, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v3, v4, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v4, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v3, v3, v4

    and-int/lit8 v4, v3, 0x7f

    int-to-long v4, v4

    shl-long/2addr v4, v2

    or-long/2addr v0, v4

    and-int/lit16 v3, v3, 0x80

    if-nez v3, :cond_1

    return-wide v0

    :cond_1
    add-int/lit8 v2, v2, 0x7

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/android/gms/internal/si;->c()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0
.end method

.method public final f()I
    .locals 6

    iget v0, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v1, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v0, v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v2, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v1, v2, :cond_1

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v1, v1, v2

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v3, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v2, v3, :cond_2

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v3, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v2, v2, v3

    iget v3, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v4, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v3, v4, :cond_3

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v4, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v3, v3, v4

    and-int/lit16 v0, v0, 0xff

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    and-int/lit16 v1, v2, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    and-int/lit16 v1, v3, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method public final g()J
    .locals 14

    const-wide/16 v12, 0xff

    iget v0, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v1, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v0, v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v2, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v1, v2, :cond_1

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v1, v1, v2

    iget v2, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v3, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v2, v3, :cond_2

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v3, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v2, v2, v3

    iget v3, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v4, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v3, v4, :cond_3

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v4, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v3, v3, v4

    iget v4, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v5, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v4, v5, :cond_4

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_4
    iget-object v4, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v5, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v4, v4, v5

    iget v5, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v6, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v5, v6, :cond_5

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_5
    iget-object v5, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v6, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v5, v5, v6

    iget v6, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v7, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v6, v7, :cond_6

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_6
    iget-object v6, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v7, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v6, v6, v7

    iget v7, p0, Lcom/google/android/gms/internal/sb;->c:I

    iget v8, p0, Lcom/google/android/gms/internal/sb;->f:I

    if-ne v7, v8, :cond_7

    invoke-static {}, Lcom/google/android/gms/internal/si;->a()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0

    :cond_7
    iget-object v7, p0, Lcom/google/android/gms/internal/sb;->a:[B

    iget v8, p0, Lcom/google/android/gms/internal/sb;->c:I

    add-int/lit8 v9, v8, 0x1

    iput v9, p0, Lcom/google/android/gms/internal/sb;->c:I

    aget-byte v7, v7, v8

    int-to-long v8, v0

    and-long/2addr v8, v12

    int-to-long v0, v1

    and-long/2addr v0, v12

    const/16 v10, 0x8

    shl-long/2addr v0, v10

    or-long/2addr v0, v8

    int-to-long v8, v2

    and-long/2addr v8, v12

    const/16 v2, 0x10

    shl-long/2addr v8, v2

    or-long/2addr v0, v8

    int-to-long v2, v3

    and-long/2addr v2, v12

    const/16 v8, 0x18

    shl-long/2addr v2, v8

    or-long/2addr v0, v2

    int-to-long v2, v4

    and-long/2addr v2, v12

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v5

    and-long/2addr v2, v12

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v6

    and-long/2addr v2, v12

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v7

    and-long/2addr v2, v12

    const/16 v4, 0x38

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public h()V
    .locals 2

    iget v0, p0, Lcom/google/android/gms/internal/sb;->f:I

    iget v1, p0, Lcom/google/android/gms/internal/sb;->g:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/internal/sb;->f:I

    iget v0, p0, Lcom/google/android/gms/internal/sb;->f:I

    iget v1, p0, Lcom/google/android/gms/internal/sb;->e:I

    if-le v0, v1, :cond_0

    iget v1, p0, Lcom/google/android/gms/internal/sb;->e:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/internal/sb;->g:I

    iget v0, p0, Lcom/google/android/gms/internal/sb;->f:I

    iget v1, p0, Lcom/google/android/gms/internal/sb;->g:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/internal/sb;->f:I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/sb;->g:I

    goto :goto_0
.end method
