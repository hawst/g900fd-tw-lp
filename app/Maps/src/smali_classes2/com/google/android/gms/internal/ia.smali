.class public Lcom/google/android/gms/internal/ia;
.super Lcom/google/android/gms/internal/jy;

# interfaces
.implements Lcom/google/android/gms/internal/kz;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/internal/hz;

.field final b:Lcom/google/android/gms/internal/gz;

.field final c:Ljava/lang/Object;

.field d:Lcom/google/android/gms/internal/fo;

.field private final g:Lcom/google/android/gms/internal/ei;

.field private final h:Ljava/lang/Object;

.field private final i:Landroid/content/Context;

.field private final j:Lcom/google/android/gms/internal/jq;

.field private k:Z

.field private l:Lcom/google/android/gms/internal/dq;

.field private m:Lcom/google/android/gms/internal/dv;

.field private n:Lcom/google/android/gms/internal/ed;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/jq;Lcom/google/android/gms/internal/gz;Lcom/google/android/gms/internal/ei;Lcom/google/android/gms/internal/hz;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/jy;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ia;->h:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ia;->c:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/ia;->k:Z

    iput-object p1, p0, Lcom/google/android/gms/internal/ia;->i:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/internal/ia;->j:Lcom/google/android/gms/internal/jq;

    iget-object v0, p2, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iput-object v0, p0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    iput-object p3, p0, Lcom/google/android/gms/internal/ia;->b:Lcom/google/android/gms/internal/gz;

    iput-object p4, p0, Lcom/google/android/gms/internal/ia;->g:Lcom/google/android/gms/internal/ei;

    iput-object p5, p0, Lcom/google/android/gms/internal/ia;->a:Lcom/google/android/gms/internal/hz;

    iget-object v0, p2, Lcom/google/android/gms/internal/jq;->c:Lcom/google/android/gms/internal/dv;

    iput-object v0, p0, Lcom/google/android/gms/internal/ia;->m:Lcom/google/android/gms/internal/dv;

    return-void
.end method

.method private a(J)V
    .locals 3

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/internal/ia;->b(J)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/if;

    const-string v1, "Timed out waiting for WebView to finish loading."

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/if;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/internal/ia;->k:Z

    if-eqz v0, :cond_0

    return-void
.end method

.method private b(J)Z
    .locals 5

    const-wide/32 v0, 0xea60

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, p1

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/internal/ia;->c:Ljava/lang/Object;

    invoke-virtual {v2, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/internal/if;

    const-string v1, "Ad request cancelled."

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/if;-><init>(Ljava/lang/String;I)V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->c:Ljava/lang/Object;

    move-object/from16 v31, v0

    monitor-enter v31

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ia;->j:Lcom/google/android/gms/internal/jq;

    iget-object v12, v2, Lcom/google/android/gms/internal/jq;->a:Lcom/google/android/gms/internal/fm;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ia;->j:Lcom/google/android/gms/internal/jq;

    iget v6, v2, Lcom/google/android/gms/internal/jq;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    iget-boolean v2, v2, Lcom/google/android/gms/internal/fo;->h:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/internal/ia;->h:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1
    .catch Lcom/google/android/gms/internal/if; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    new-instance v2, Lcom/google/android/gms/internal/dq;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/internal/ia;->i:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/internal/ia;->g:Lcom/google/android/gms/internal/ei;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/internal/ia;->m:Lcom/google/android/gms/internal/dv;

    invoke-direct {v2, v7, v12, v8, v9}, Lcom/google/android/gms/internal/dq;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/fm;Lcom/google/android/gms/internal/ei;Lcom/google/android/gms/internal/dv;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/internal/ia;->l:Lcom/google/android/gms/internal/dq;

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ia;->l:Lcom/google/android/gms/internal/dq;

    const-wide/32 v8, 0xea60

    invoke-virtual {v2, v4, v5, v8, v9}, Lcom/google/android/gms/internal/dq;->a(JJ)Lcom/google/android/gms/internal/ed;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/internal/ia;->n:Lcom/google/android/gms/internal/ed;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ia;->n:Lcom/google/android/gms/internal/ed;

    iget v2, v2, Lcom/google/android/gms/internal/ed;->a:I

    packed-switch v2, :pswitch_data_0

    new-instance v2, Lcom/google/android/gms/internal/if;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected mediation result: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/internal/ia;->n:Lcom/google/android/gms/internal/ed;

    iget v4, v4, Lcom/google/android/gms/internal/ed;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/internal/if;-><init>(Ljava/lang/String;I)V

    throw v2
    :try_end_3
    .catch Lcom/google/android/gms/internal/if; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    move-exception v2

    :try_start_4
    iget v6, v2, Lcom/google/android/gms/internal/if;->a:I

    const/4 v3, 0x3

    if-eq v6, v3, :cond_0

    const/4 v3, -0x1

    if-ne v6, v3, :cond_6

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/internal/if;->getMessage()Ljava/lang/String;

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    if-nez v2, :cond_7

    new-instance v2, Lcom/google/android/gms/internal/fo;

    invoke-direct {v2, v6}, Lcom/google/android/gms/internal/fo;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    :goto_1
    sget-object v2, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/gms/internal/ib;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/gms/internal/ib;-><init>(Lcom/google/android/gms/internal/ia;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    :goto_2
    :pswitch_0
    new-instance v2, Lcom/google/android/gms/internal/jp;

    iget-object v3, v12, Lcom/google/android/gms/internal/fm;->c:Lcom/google/android/gms/internal/ba;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/internal/ia;->b:Lcom/google/android/gms/internal/gz;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    iget-object v5, v5, Lcom/google/android/gms/internal/fo;->d:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    iget-object v7, v7, Lcom/google/android/gms/internal/fo;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    iget-object v8, v8, Lcom/google/android/gms/internal/fo;->j:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    iget v9, v9, Lcom/google/android/gms/internal/fo;->l:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    iget-wide v10, v10, Lcom/google/android/gms/internal/fo;->k:J

    iget-object v12, v12, Lcom/google/android/gms/internal/fm;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    iget-boolean v13, v13, Lcom/google/android/gms/internal/fo;->h:Z

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/internal/ia;->n:Lcom/google/android/gms/internal/ed;

    if-eqz v14, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/internal/ia;->n:Lcom/google/android/gms/internal/ed;

    iget-object v14, v14, Lcom/google/android/gms/internal/ed;->b:Lcom/google/android/gms/internal/ds;

    :goto_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gms/internal/ia;->n:Lcom/google/android/gms/internal/ed;

    if-eqz v15, :cond_9

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gms/internal/ia;->n:Lcom/google/android/gms/internal/ed;

    iget-object v15, v15, Lcom/google/android/gms/internal/ed;->c:Lcom/google/android/gms/internal/el;

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->n:Lcom/google/android/gms/internal/ed;

    move-object/from16 v16, v0

    if-eqz v16, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->n:Lcom/google/android/gms/internal/ed;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gms/internal/ed;->d:Ljava/lang/String;

    move-object/from16 v16, v0

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->m:Lcom/google/android/gms/internal/dv;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->n:Lcom/google/android/gms/internal/ed;

    move-object/from16 v18, v0

    if-eqz v18, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->n:Lcom/google/android/gms/internal/ed;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/android/gms/internal/ed;->e:Lcom/google/android/gms/internal/ea;

    move-object/from16 v18, v0

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/google/android/gms/internal/fo;->i:J

    move-wide/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->j:Lcom/google/android/gms/internal/jq;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/android/gms/internal/jq;->d:Lcom/google/android/gms/internal/bd;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/google/android/gms/internal/fo;->g:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->j:Lcom/google/android/gms/internal/jq;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/google/android/gms/internal/jq;->f:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-wide v0, v0, Lcom/google/android/gms/internal/fo;->n:J

    move-wide/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/google/android/gms/internal/fo;->o:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->j:Lcom/google/android/gms/internal/jq;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/google/android/gms/internal/jq;->h:Lorg/json/JSONObject;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-direct/range {v2 .. v30}, Lcom/google/android/gms/internal/jp;-><init>(Lcom/google/android/gms/internal/ba;Lcom/google/android/gms/internal/gz;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLcom/google/android/gms/internal/ds;Lcom/google/android/gms/internal/el;Ljava/lang/String;Lcom/google/android/gms/internal/dv;Lcom/google/android/gms/internal/ea;JLcom/google/android/gms/internal/bd;JJJLjava/lang/String;Lorg/json/JSONObject;Lcom/google/android/gms/internal/cj;)V

    sget-object v3, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/gms/internal/ic;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2}, Lcom/google/android/gms/internal/ic;-><init>(Lcom/google/android/gms/internal/ia;Lcom/google/android/gms/internal/jp;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    monitor-exit v31
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return-void

    :catchall_0
    move-exception v2

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v2
    :try_end_6
    .catch Lcom/google/android/gms/internal/if; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v2

    :try_start_7
    monitor-exit v31
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v2

    :pswitch_1
    :try_start_8
    new-instance v2, Lcom/google/android/gms/internal/if;

    const-string v3, "No fill from any mediation ad networks."

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/internal/if;-><init>(Ljava/lang/String;I)V

    throw v2

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    iget-boolean v2, v2, Lcom/google/android/gms/internal/fo;->p:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ia;->b:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/gz;->d()Lcom/google/android/gms/internal/bd;

    move-result-object v2

    iget-boolean v3, v2, Lcom/google/android/gms/internal/bd;->e:Z

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ia;->i:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ia;->i:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    :goto_7
    new-instance v7, Lcom/google/android/gms/internal/hw;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/internal/ia;->b:Lcom/google/android/gms/internal/gz;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v8, v3, v2}, Lcom/google/android/gms/internal/hw;-><init>(Lcom/google/android/gms/internal/kz;Lcom/google/android/gms/internal/gz;II)V

    sget-object v2, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/gms/internal/ie;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v7}, Lcom/google/android/gms/internal/ie;-><init>(Lcom/google/android/gms/internal/ia;Lcom/google/android/gms/internal/hw;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/gms/internal/ia;->a(J)V

    invoke-virtual {v7}, Lcom/google/android/gms/internal/hw;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Lcom/google/android/gms/internal/if;

    const-string v3, "AdNetwork sent passback url"

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/internal/if;-><init>(Ljava/lang/String;I)V

    throw v2

    :cond_3
    iget v3, v2, Lcom/google/android/gms/internal/bd;->g:I

    iget v2, v2, Lcom/google/android/gms/internal/bd;->d:I

    goto :goto_7

    :cond_4
    iget-boolean v2, v7, Lcom/google/android/gms/internal/hw;->e:Z

    if-nez v2, :cond_1

    new-instance v2, Lcom/google/android/gms/internal/if;

    const-string v3, "AdNetwork timed out"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/internal/if;-><init>(Ljava/lang/String;I)V

    throw v2

    :cond_5
    sget-object v2, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/gms/internal/id;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/gms/internal/id;-><init>(Lcom/google/android/gms/internal/ia;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/gms/internal/ia;->a(J)V
    :try_end_8
    .catch Lcom/google/android/gms/internal/if; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_2

    :cond_6
    :try_start_9
    invoke-virtual {v2}, Lcom/google/android/gms/internal/if;->getMessage()Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    new-instance v2, Lcom/google/android/gms/internal/fo;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    iget-wide v4, v3, Lcom/google/android/gms/internal/fo;->k:J

    invoke-direct {v2, v6, v4, v5}, Lcom/google/android/gms/internal/fo;-><init>(IJ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    goto/16 :goto_1

    :cond_8
    const/4 v14, 0x0

    goto/16 :goto_3

    :cond_9
    const/4 v15, 0x0

    goto/16 :goto_4

    :cond_a
    const-class v16, Lcom/google/a/a/a/a;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Class;->getName()Ljava/lang/String;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v16

    goto/16 :goto_5

    :cond_b
    const/16 v18, 0x0

    goto/16 :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/internal/gz;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/ia;->c:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/internal/ia;->k:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/ia;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/gms/internal/ia;->h:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ia;->b:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->stopLoading()V

    iget-object v0, p0, Lcom/google/android/gms/internal/ia;->b:Lcom/google/android/gms/internal/gz;

    invoke-static {v0}, Lcom/google/android/gms/internal/kf;->a(Landroid/webkit/WebView;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/ia;->l:Lcom/google/android/gms/internal/dq;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/ia;->l:Lcom/google/android/gms/internal/dq;

    iget-object v2, v0, Lcom/google/android/gms/internal/dq;->a:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 v3, 0x1

    :try_start_1
    iput-boolean v3, v0, Lcom/google/android/gms/internal/dq;->b:Z

    iget-object v3, v0, Lcom/google/android/gms/internal/dq;->c:Lcom/google/android/gms/internal/eb;

    if-eqz v3, :cond_1

    iget-object v0, v0, Lcom/google/android/gms/internal/dq;->c:Lcom/google/android/gms/internal/eb;

    iget-object v3, v0, Lcom/google/android/gms/internal/eb;->e:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v4, v0, Lcom/google/android/gms/internal/eb;->g:Lcom/google/android/gms/internal/el;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/google/android/gms/internal/eb;->g:Lcom/google/android/gms/internal/el;

    invoke-interface {v4}, Lcom/google/android/gms/internal/el;->c()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :goto_0
    const/4 v4, -0x1

    :try_start_3
    iput v4, v0, Lcom/google/android/gms/internal/eb;->h:I

    iget-object v0, v0, Lcom/google/android/gms/internal/eb;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_2
    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    return-void

    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v0

    :catch_0
    move-exception v4

    goto :goto_0
.end method
