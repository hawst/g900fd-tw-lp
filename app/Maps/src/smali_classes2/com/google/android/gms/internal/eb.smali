.class public final Lcom/google/android/gms/internal/eb;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/ee;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/internal/ds;

.field final b:Lcom/google/android/gms/internal/ba;

.field final c:Lcom/google/android/gms/internal/bd;

.field final d:Landroid/content/Context;

.field final e:Ljava/lang/Object;

.field final f:Lcom/google/android/gms/internal/gx;

.field g:Lcom/google/android/gms/internal/el;

.field h:I

.field private final i:Ljava/lang/String;

.field private final j:Lcom/google/android/gms/internal/ei;

.field private final k:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/internal/ei;Lcom/google/android/gms/internal/dv;Lcom/google/android/gms/internal/ds;Lcom/google/android/gms/internal/ba;Lcom/google/android/gms/internal/bd;Lcom/google/android/gms/internal/gx;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/eb;->e:Ljava/lang/Object;

    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/gms/internal/eb;->h:I

    iput-object p1, p0, Lcom/google/android/gms/internal/eb;->d:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/gms/internal/eb;->j:Lcom/google/android/gms/internal/ei;

    iput-object p5, p0, Lcom/google/android/gms/internal/eb;->a:Lcom/google/android/gms/internal/ds;

    const-string v0, "com.google.ads.mediation.customevent.CustomEventAdapter"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/internal/eb;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/eb;->i:Ljava/lang/String;

    :goto_0
    iget-wide v0, p4, Lcom/google/android/gms/internal/dv;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p4, Lcom/google/android/gms/internal/dv;->b:J

    :goto_1
    iput-wide v0, p0, Lcom/google/android/gms/internal/eb;->k:J

    iput-object p6, p0, Lcom/google/android/gms/internal/eb;->b:Lcom/google/android/gms/internal/ba;

    iput-object p7, p0, Lcom/google/android/gms/internal/eb;->c:Lcom/google/android/gms/internal/bd;

    iput-object p8, p0, Lcom/google/android/gms/internal/eb;->f:Lcom/google/android/gms/internal/gx;

    return-void

    :cond_0
    iput-object p2, p0, Lcom/google/android/gms/internal/eb;->i:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x2710

    goto :goto_1
.end method

.method private a(JJJJ)V
    .locals 7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long v2, v0, p1

    sub-long v2, p3, v2

    sub-long/2addr v0, p5

    sub-long v0, p7, v0

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gtz v4, :cond_1

    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/internal/eb;->h:I

    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/internal/eb;->e:Ljava/lang/Object;

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-virtual {v4, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/eb;->h:I

    goto :goto_0
.end method

.method private b()Ljava/lang/String;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/eb;->a:Lcom/google/android/gms/internal/ds;

    iget-object v0, v0, Lcom/google/android/gms/internal/ds;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/eb;->j:Lcom/google/android/gms/internal/ei;

    iget-object v1, p0, Lcom/google/android/gms/internal/eb;->a:Lcom/google/android/gms/internal/ds;

    iget-object v1, v1, Lcom/google/android/gms/internal/ds;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/ei;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gms.ads.mediation.customevent.CustomEventAdapter"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.google.ads.mediation.customevent.CustomEventAdapter"
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_1
    const-string v0, "com.google.ads.mediation.customevent.CustomEventAdapter"

    goto :goto_0
.end method


# virtual methods
.method public final a(JJ)Lcom/google/android/gms/internal/ed;
    .locals 13

    iget-object v10, p0, Lcom/google/android/gms/internal/eb;->e:Ljava/lang/Object;

    monitor-enter v10

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    new-instance v11, Lcom/google/android/gms/internal/ea;

    invoke-direct {v11}, Lcom/google/android/gms/internal/ea;-><init>()V

    sget-object v0, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/ec;

    invoke-direct {v1, p0, v11}, Lcom/google/android/gms/internal/ec;-><init>(Lcom/google/android/gms/internal/eb;Lcom/google/android/gms/internal/ea;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-wide v4, p0, Lcom/google/android/gms/internal/eb;->k:J

    :goto_0
    iget v0, p0, Lcom/google/android/gms/internal/eb;->h:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    move-object v1, p0

    move-wide v6, p1

    move-wide/from16 v8, p3

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/internal/eb;->a(JJJJ)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/gms/internal/ed;

    iget-object v1, p0, Lcom/google/android/gms/internal/eb;->a:Lcom/google/android/gms/internal/ds;

    iget-object v2, p0, Lcom/google/android/gms/internal/eb;->g:Lcom/google/android/gms/internal/el;

    iget-object v3, p0, Lcom/google/android/gms/internal/eb;->i:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/internal/eb;->h:I

    move-object v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/ed;-><init>(Lcom/google/android/gms/internal/ds;Lcom/google/android/gms/internal/el;Ljava/lang/String;Lcom/google/android/gms/internal/ea;I)V

    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method a()Lcom/google/android/gms/internal/el;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Instantiating mediation adapter: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/eb;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/eb;->j:Lcom/google/android/gms/internal/ei;

    iget-object v1, p0, Lcom/google/android/gms/internal/eb;->i:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/ei;->a(Ljava/lang/String;)Lcom/google/android/gms/internal/el;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Could not instantiate mediation adapter: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/eb;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/eb;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput p1, p0, Lcom/google/android/gms/internal/eb;->h:I

    iget-object v0, p0, Lcom/google/android/gms/internal/eb;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
