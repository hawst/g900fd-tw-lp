.class public Lcom/google/android/gms/car/aq;
.super Ljava/lang/Object;


# instance fields
.field a:Lcom/google/android/gms/car/at;

.field final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/gms/car/au;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final d:Landroid/os/Handler;

.field private final e:Lcom/google/android/gms/car/du;

.field private final f:Landroid/os/Handler$Callback;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/du;Landroid/os/Looper;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/aq;->b:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/aq;->c:Ljava/util/HashMap;

    new-instance v0, Lcom/google/android/gms/car/ar;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ar;-><init>(Lcom/google/android/gms/car/aq;)V

    iput-object v0, p0, Lcom/google/android/gms/car/aq;->f:Landroid/os/Handler$Callback;

    iput-object p1, p0, Lcom/google/android/gms/car/aq;->e:Lcom/google/android/gms/car/du;

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/aq;->f:Landroid/os/Handler$Callback;

    invoke-direct {v0, p2, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/gms/car/aq;->d:Landroid/os/Handler;

    return-void
.end method

.method private a(Lcom/google/android/gms/car/as;Ljava/lang/Integer;Ljava/util/Iterator;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/car/as;",
            "Ljava/lang/Integer;",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/car/aq;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/au;

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/google/android/gms/car/au;->a:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/car/au;->a:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/car/au;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/aq;->e:Lcom/google/android/gms/car/du;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/car/aq;->a:Lcom/google/android/gms/car/at;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/car/du;->a(ILcom/google/android/gms/car/eb;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    if-nez p3, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/aq;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-interface {p3}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(II)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/car/aq;->e:Lcom/google/android/gms/car/du;

    iget-object v2, p0, Lcom/google/android/gms/car/aq;->a:Lcom/google/android/gms/car/at;

    invoke-interface {v1, p1, p2, v2}, Lcom/google/android/gms/car/du;->a(IILcom/google/android/gms/car/eb;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/eq;->a(Ljava/lang/IllegalStateException;)V

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private a()[I
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/aq;->e:Lcom/google/android/gms/car/du;

    invoke-interface {v0}, Lcom/google/android/gms/car/du;->a()[I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/eq;->a(Ljava/lang/IllegalStateException;)V

    :goto_1
    const/4 v0, 0x0

    new-array v0, v0, [I

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/car/as;)V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/car/aq;->b:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/aq;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/gms/car/aq;->a(Lcom/google/android/gms/car/as;Ljava/lang/Integer;Ljava/util/Iterator;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(I)Z
    .locals 5

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/car/aq;->a()[I

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    if-ne p1, v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/as;II)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p2, :cond_0

    const/16 v0, 0x15

    if-le p2, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid sensor type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-eqz p3, :cond_2

    const/4 v0, 0x3

    if-eq p3, v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong rate "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v4, p0, Lcom/google/android/gms/car/aq;->b:Ljava/util/HashMap;

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/aq;->a:Lcom/google/android/gms/car/at;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/car/at;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/at;-><init>(Lcom/google/android/gms/car/aq;)V

    iput-object v0, p0, Lcom/google/android/gms/car/aq;->a:Lcom/google/android/gms/car/at;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/aq;->b:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/au;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/android/gms/car/au;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/car/au;-><init>(Lcom/google/android/gms/car/aq;I)V

    iget-object v3, p0, Lcom/google/android/gms/car/aq;->b:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v0

    move v0, v2

    :goto_0
    iget-object v5, v3, Lcom/google/android/gms/car/au;->a:Ljava/util/LinkedList;

    invoke-virtual {v5, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, v3, Lcom/google/android/gms/car/au;->a:Ljava/util/LinkedList;

    invoke-virtual {v5, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_4
    iget v5, v3, Lcom/google/android/gms/car/au;->b:I

    if-le v5, p3, :cond_6

    iput p3, v3, Lcom/google/android/gms/car/au;->b:I

    move v3, v2

    :goto_1
    if-eqz v3, :cond_5

    move v0, v2

    :cond_5
    if-eqz v0, :cond_7

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/car/aq;->a(II)Z

    move-result v0

    if-nez v0, :cond_7

    monitor-exit v4

    move v0, v1

    :goto_2
    return v0

    :cond_6
    move v3, v1

    goto :goto_1

    :cond_7
    monitor-exit v4

    move v0, v2

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_8
    move-object v3, v0

    move v0, v1

    goto :goto_0
.end method
