.class public final Lcom/google/android/gms/people/i;
.super Ljava/lang/Object;


# instance fields
.field a:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/people/h;
    .locals 2

    iget v0, p0, Lcom/google/android/gms/people/i;->a:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide valid client application ID!"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/people/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/h;-><init>(Lcom/google/android/gms/people/i;)V

    return-object v0
.end method

.method public final a(I)Lcom/google/android/gms/people/i;
    .locals 0

    iput p1, p0, Lcom/google/android/gms/people/i;->a:I

    return-object p0
.end method
