.class public Lcom/google/android/gms/internal/iu;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/android/gms/internal/jp;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Ljava/lang/Object;

.field b:Z

.field c:I

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/internal/ko;

.field private final f:Lcom/google/android/gms/internal/z;

.field private final g:Lcom/google/android/gms/internal/y;

.field private final h:Lcom/google/android/gms/internal/jq;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/z;Lcom/google/android/gms/internal/y;Lcom/google/android/gms/internal/ko;Lcom/google/android/gms/internal/jq;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/iu;->a:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gms/internal/iu;->d:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/internal/iu;->f:Lcom/google/android/gms/internal/z;

    iput-object p4, p0, Lcom/google/android/gms/internal/iu;->e:Lcom/google/android/gms/internal/ko;

    iput-object p3, p0, Lcom/google/android/gms/internal/iu;->g:Lcom/google/android/gms/internal/y;

    iput-object p5, p0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/iu;->b:Z

    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/gms/internal/iu;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/iu;->i:Ljava/util/List;

    return-void
.end method

.method private a(Lcom/google/android/gms/internal/cj;)Lcom/google/android/gms/internal/jp;
    .locals 31

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/internal/iu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/gms/internal/iu;->c:I

    if-nez p1, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/internal/iu;->c:I

    const/4 v4, -0x2

    if-ne v2, v4, :cond_0

    const/4 v6, 0x0

    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, -0x2

    if-eq v6, v2, :cond_1

    const/16 v30, 0x0

    :goto_0
    new-instance v2, Lcom/google/android/gms/internal/jp;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    iget-object v3, v3, Lcom/google/android/gms/internal/jq;->a:Lcom/google/android/gms/internal/fm;

    iget-object v3, v3, Lcom/google/android/gms/internal/fm;->c:Lcom/google/android/gms/internal/ba;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    iget-object v5, v5, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget-object v5, v5, Lcom/google/android/gms/internal/fo;->d:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    iget-object v7, v7, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget-object v7, v7, Lcom/google/android/gms/internal/fo;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/internal/iu;->i:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    iget-object v9, v9, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget v9, v9, Lcom/google/android/gms/internal/fo;->l:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    iget-object v10, v10, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget-wide v10, v10, Lcom/google/android/gms/internal/fo;->k:J

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    iget-object v12, v12, Lcom/google/android/gms/internal/jq;->a:Lcom/google/android/gms/internal/fm;

    iget-object v12, v12, Lcom/google/android/gms/internal/fm;->i:Ljava/lang/String;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const-wide/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/android/gms/internal/jq;->d:Lcom/google/android/gms/internal/bd;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/google/android/gms/internal/fo;->g:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/google/android/gms/internal/jq;->f:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-wide v0, v0, Lcom/google/android/gms/internal/jq;->g:J

    move-wide/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/google/android/gms/internal/fo;->o:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/google/android/gms/internal/jq;->h:Lorg/json/JSONObject;

    move-object/from16 v29, v0

    invoke-direct/range {v2 .. v30}, Lcom/google/android/gms/internal/jp;-><init>(Lcom/google/android/gms/internal/ba;Lcom/google/android/gms/internal/gz;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLcom/google/android/gms/internal/ds;Lcom/google/android/gms/internal/el;Ljava/lang/String;Lcom/google/android/gms/internal/dv;Lcom/google/android/gms/internal/ea;JLcom/google/android/gms/internal/bd;JJJLjava/lang/String;Lorg/json/JSONObject;Lcom/google/android/gms/internal/cj;)V

    return-object v2

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_1
    move-object/from16 v30, p1

    goto/16 :goto_0
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private b()Lcom/google/android/gms/internal/jp;
    .locals 8

    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/iu;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v3, v7

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/iu;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v2, v7

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/iu;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v1, v7

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/internal/iu;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, v7

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/iu;->a(Lcom/google/android/gms/internal/cj;)Lcom/google/android/gms/internal/jp;

    move-result-object v0

    :goto_4
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/iu;->g:Lcom/google/android/gms/internal/y;

    iget-object v2, p0, Lcom/google/android/gms/internal/iu;->d:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    iget-object v0, v0, Lcom/google/android/gms/internal/jq;->a:Lcom/google/android/gms/internal/fm;

    iget-object v3, v0, Lcom/google/android/gms/internal/fm;->k:Lcom/google/android/gms/internal/gx;

    const-string v5, "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/native_ads.html"

    new-instance v4, Lcom/google/android/gms/internal/ki;

    invoke-direct {v4}, Lcom/google/android/gms/internal/ki;-><init>()V

    sget-object v6, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gms/internal/aa;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/aa;-><init>(Lcom/google/android/gms/internal/y;Landroid/content/Context;Lcom/google/android/gms/internal/gx;Lcom/google/android/gms/internal/ki;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-interface {v4}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/w;

    iget-object v1, p0, Lcom/google/android/gms/internal/iu;->f:Lcom/google/android/gms/internal/z;

    iget-object v2, p0, Lcom/google/android/gms/internal/iu;->f:Lcom/google/android/gms/internal/z;

    iget-object v3, p0, Lcom/google/android/gms/internal/iu;->f:Lcom/google/android/gms/internal/z;

    iget-object v4, p0, Lcom/google/android/gms/internal/iu;->f:Lcom/google/android/gms/internal/z;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/gms/internal/iu;->f:Lcom/google/android/gms/internal/z;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/internal/w;->a(Lcom/google/android/gms/internal/sq;Lcom/google/android/gms/internal/fp;Lcom/google/android/gms/internal/cp;Lcom/google/android/gms/internal/ft;ZLcom/google/android/gms/internal/db;)V

    move-object v3, v0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/internal/ki;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ki;-><init>()V

    const-string v1, "/nativeAdPreProcess"

    new-instance v2, Lcom/google/android/gms/internal/iv;

    invoke-direct {v2, p0, v3, v0}, Lcom/google/android/gms/internal/iv;-><init>(Lcom/google/android/gms/internal/iu;Lcom/google/android/gms/internal/w;Lcom/google/android/gms/internal/ki;)V

    invoke-interface {v3, v1, v2}, Lcom/google/android/gms/internal/w;->a(Ljava/lang/String;Lcom/google/android/gms/internal/da;)V

    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/google/android/gms/internal/iu;->h:Lcom/google/android/gms/internal/jq;

    iget-object v2, v2, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget-object v2, v2, Lcom/google/android/gms/internal/fo;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "google.afma.nativeAds.preProcessJsonGmsg"

    invoke-interface {v3, v2, v1}, Lcom/google/android/gms/internal/w;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ki;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    move-object v2, v0

    goto :goto_1

    :cond_2
    const-string v0, "template_id"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Lcom/google/android/gms/internal/iy;

    invoke-direct {v0}, Lcom/google/android/gms/internal/iy;-><init>()V

    move-object v1, v0

    goto :goto_2

    :cond_3
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/gms/internal/iz;

    invoke-direct {v0}, Lcom/google/android/gms/internal/iz;-><init>()V

    move-object v1, v0

    goto/16 :goto_2

    :cond_4
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/internal/iu;->a:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v4, 0x1

    :try_start_1
    iput-boolean v4, p0, Lcom/google/android/gms/internal/iu;->b:Z

    iput v0, p0, Lcom/google/android/gms/internal/iu;->c:I

    monitor-exit v1

    move-object v1, v7

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_0
    move-exception v0

    :goto_5
    iget-boolean v0, p0, Lcom/google/android/gms/internal/iu;->b:Z

    if-nez v0, :cond_5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/internal/iu;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v2, 0x1

    :try_start_3
    iput-boolean v2, p0, Lcom/google/android/gms/internal/iu;->b:Z

    iput v0, p0, Lcom/google/android/gms/internal/iu;->c:I

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_5
    invoke-direct {p0, v7}, Lcom/google/android/gms/internal/iu;->a(Lcom/google/android/gms/internal/cj;)Lcom/google/android/gms/internal/jp;

    move-result-object v0

    goto/16 :goto_4

    :cond_6
    :try_start_4
    const-string v0, "tracking_urls_and_actions"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v4, "impression_tracking_urls"

    invoke-static {v0, v4}, Lcom/google/android/gms/internal/iu;->a(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    move-object v0, v7

    :goto_6
    iput-object v0, p0, Lcom/google/android/gms/internal/iu;->i:Ljava/util/List;

    invoke-interface {v1, p0, v2}, Lcom/google/android/gms/internal/ix;->a(Lcom/google/android/gms/internal/iu;Lorg/json/JSONObject;)Lcom/google/android/gms/internal/cj;

    move-result-object v0

    if-nez v0, :cond_8

    move-object v0, v7

    goto/16 :goto_3

    :cond_7
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_6

    :cond_8
    new-instance v1, Lcom/google/android/gms/internal/ci;

    iget-object v4, p0, Lcom/google/android/gms/internal/iu;->f:Lcom/google/android/gms/internal/z;

    invoke-direct {v1, v4, v3, v2}, Lcom/google/android/gms/internal/ci;-><init>(Lcom/google/android/gms/internal/z;Lcom/google/android/gms/internal/w;Lorg/json/JSONObject;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/cj;->a(Lcom/google/android/gms/internal/ci;)V
    :try_end_4
    .catch Ljava/util/concurrent/CancellationException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_3

    :catch_1
    move-exception v0

    goto :goto_5

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    :catch_2
    move-exception v0

    goto :goto_5

    :catch_3
    move-exception v0

    goto :goto_5

    :catch_4
    move-exception v0

    goto :goto_5
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Ljava/lang/String;Z)Ljava/util/concurrent/Future;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/concurrent/Future",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    if-eqz p3, :cond_2

    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_0

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :cond_0
    if-eqz p3, :cond_3

    const-string v1, "url"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v0, 0x0

    if-eqz p3, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/iu;->a(I)V

    :cond_1
    new-instance v0, Lcom/google/android/gms/internal/kj;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/kj;-><init>(Ljava/lang/Object;)V

    :goto_2
    return-object v0

    :cond_2
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v1, "url"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/internal/iu;->e:Lcom/google/android/gms/internal/ko;

    new-instance v2, Lcom/google/android/gms/internal/iw;

    invoke-direct {v2, p0, p3}, Lcom/google/android/gms/internal/iw;-><init>(Lcom/google/android/gms/internal/iu;Z)V

    new-instance v3, Lcom/google/android/gms/internal/kq;

    invoke-direct {v3, v1, v0, v2}, Lcom/google/android/gms/internal/kq;-><init>(Lcom/google/android/gms/internal/ko;Ljava/lang/String;Lcom/google/android/gms/internal/kr;)V

    invoke-static {v3}, Lcom/google/android/gms/internal/kb;->a(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(I)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/iu;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/internal/iu;->b:Z

    iput p1, p0, Lcom/google/android/gms/internal/iu;->c:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(IZ)V
    .locals 2

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/iu;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/internal/iu;->b:Z

    iput p1, p0, Lcom/google/android/gms/internal/iu;->c:I

    monitor-exit v1

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/iu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/iu;->b:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/iu;->b()Lcom/google/android/gms/internal/jp;

    move-result-object v0

    return-object v0
.end method
