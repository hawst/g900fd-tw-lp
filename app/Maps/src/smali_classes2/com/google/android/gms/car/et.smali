.class Lcom/google/android/gms/car/et;
.super Lcom/google/android/gms/car/cj;


# instance fields
.field private a:Lcom/google/android/gms/car/e;

.field private volatile b:Z

.field private final c:Ljava/lang/Object;

.field private final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/gms/car/eq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/eq;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/car/cj;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/et;->b:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/et;->c:Ljava/lang/Object;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/car/et;->d:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private a(Lcom/google/android/gms/car/e;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/car/et;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/eq;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Looper;

    new-instance v2, Lcom/google/android/gms/car/eu;

    invoke-direct {v2, p0, v0, p1, p2}, Lcom/google/android/gms/car/eu;-><init>(Lcom/google/android/gms/car/et;Lcom/google/android/gms/car/eq;Lcom/google/android/gms/car/e;I)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/et;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/eq;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/car/et;->c:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/gms/car/et;->b:Z

    if-eqz v3, :cond_2

    const/4 v1, 0x1

    :cond_2
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/gms/car/et;->b:Z

    iget-object v3, p0, Lcom/google/android/gms/car/et;->a:Lcom/google/android/gms/car/e;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0}, Lcom/google/android/gms/car/eq;->c(Lcom/google/android/gms/car/eq;)V

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/et;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/eq;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/common/internal/e;->b:Landroid/os/Looper;

    new-instance v2, Lcom/google/android/gms/car/ev;

    invoke-direct {v2, p0, v0, v3}, Lcom/google/android/gms/car/ev;-><init>(Lcom/google/android/gms/car/et;Lcom/google/android/gms/car/eq;Lcom/google/android/gms/car/e;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(I)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/car/et;->c:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/gms/car/et;->b:Z

    if-nez v3, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/car/et;->b:Z

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/car/et;->a:Lcom/google/android/gms/car/e;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-direct {p0, v1, p1}, Lcom/google/android/gms/car/et;->a(Lcom/google/android/gms/car/e;I)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/e;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/et;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/eq;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/car/et;->c:Ljava/lang/Object;

    monitor-enter v3

    const/4 v4, 0x0

    :try_start_0
    iput-boolean v4, p0, Lcom/google/android/gms/car/et;->b:Z

    iput-object p1, p0, Lcom/google/android/gms/car/et;->a:Lcom/google/android/gms/car/e;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/car/et;->c:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1
    .catch Lcom/google/android/gms/car/ao; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    invoke-virtual {v0}, Lcom/google/android/gms/car/eq;->c()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-boolean v4, p0, Lcom/google/android/gms/car/et;->b:Z

    if-nez v4, :cond_2

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/car/et;->b:Z

    :goto_1
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v0}, Lcom/google/android/gms/car/eq;->d()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/car/et;->a(Lcom/google/android/gms/car/e;I)V
    :try_end_3
    .catch Lcom/google/android/gms/car/ao; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0
    :try_end_6
    .catch Lcom/google/android/gms/car/ao; {:try_start_6 .. :try_end_6} :catch_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method
