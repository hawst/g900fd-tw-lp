.class Lcom/google/android/gms/wearable/internal/be;
.super Lcom/google/android/gms/wearable/internal/ba;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/wearable/internal/ba",
        "<",
        "Lcom/google/android/gms/wearable/d;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic d:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wearable/internal/bc;Lcom/google/android/gms/common/api/o;Landroid/net/Uri;)V
    .locals 0

    iput-object p3, p0, Lcom/google/android/gms/wearable/internal/be;->d:Landroid/net/Uri;

    invoke-direct {p0, p2}, Lcom/google/android/gms/wearable/internal/ba;-><init>(Lcom/google/android/gms/common/api/o;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/u;
    .locals 2

    new-instance v0, Lcom/google/android/gms/wearable/internal/bg;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/wearable/internal/bg;-><init>(Lcom/google/android/gms/common/api/Status;I)V

    return-object v0
.end method

.method protected final synthetic b(Lcom/google/android/gms/common/api/f;)V
    .locals 3

    check-cast p1, Lcom/google/android/gms/wearable/internal/aw;

    iget-object v1, p0, Lcom/google/android/gms/wearable/internal/be;->d:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/google/android/gms/wearable/internal/aw;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/internal/k;

    new-instance v2, Lcom/google/android/gms/wearable/internal/ar;

    invoke-direct {v2, p0}, Lcom/google/android/gms/wearable/internal/ar;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/wearable/internal/k;->c(Lcom/google/android/gms/wearable/internal/e;Landroid/net/Uri;)V

    return-void
.end method
