.class public Lcom/google/android/gms/internal/jt;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/dj;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final g:Lcom/google/android/gms/internal/jt;


# instance fields
.field public final b:Ljava/lang/String;

.field final c:Ljava/lang/Object;

.field final d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/gms/internal/jr;",
            ">;"
        }
    .end annotation
.end field

.field final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/internal/jw;",
            ">;"
        }
    .end annotation
.end field

.field f:Z

.field private final h:Lcom/google/android/gms/internal/ju;

.field private i:Lcom/google/android/gms/common/b/c;

.field private j:Ljava/math/BigInteger;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Landroid/content/Context;

.field private o:Lcom/google/android/gms/internal/gx;

.field private p:Lcom/google/android/gms/internal/aj;

.field private q:Lcom/google/android/gms/internal/ak;

.field private r:Lcom/google/android/gms/internal/ai;

.field private s:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private t:Z

.field private u:Landroid/os/Bundle;

.field private v:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/jt;

    invoke-direct {v0}, Lcom/google/android/gms/internal/jt;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    iget-object v0, v0, Lcom/google/android/gms/internal/jt;->b:Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/internal/jt;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    sget-object v0, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    iput-object v0, p0, Lcom/google/android/gms/internal/jt;->j:Ljava/math/BigInteger;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/jt;->d:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/jt;->e:Ljava/util/HashMap;

    iput-boolean v1, p0, Lcom/google/android/gms/internal/jt;->k:Z

    iput-boolean v3, p0, Lcom/google/android/gms/internal/jt;->l:Z

    iput-boolean v1, p0, Lcom/google/android/gms/internal/jt;->m:Z

    iput-boolean v3, p0, Lcom/google/android/gms/internal/jt;->f:Z

    iput-object v2, p0, Lcom/google/android/gms/internal/jt;->p:Lcom/google/android/gms/internal/aj;

    iput-object v2, p0, Lcom/google/android/gms/internal/jt;->q:Lcom/google/android/gms/internal/ak;

    iput-object v2, p0, Lcom/google/android/gms/internal/jt;->r:Lcom/google/android/gms/internal/ai;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/jt;->s:Ljava/util/LinkedList;

    iput-boolean v1, p0, Lcom/google/android/gms/internal/jt;->t:Z

    invoke-static {}, Lcom/google/android/gms/internal/by;->a()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/jt;->u:Landroid/os/Bundle;

    invoke-static {}, Lcom/google/android/gms/internal/kf;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/jt;->b:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/internal/ju;

    iget-object v1, p0, Lcom/google/android/gms/internal/jt;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ju;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/jt;->h:Lcom/google/android/gms/internal/ju;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/jv;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/internal/jt;->b(Landroid/content/Context;Lcom/google/android/gms/internal/jv;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static a()Lcom/google/android/gms/internal/jt;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    return-object v0
.end method

.method public static a(ILjava/lang/String;)Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    iget-object v1, v0, Lcom/google/android/gms/internal/jt;->o:Lcom/google/android/gms/internal/gx;

    iget-boolean v1, v1, Lcom/google/android/gms/internal/gx;->e:Z

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/internal/jt;->n:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    :goto_1
    return-object p1

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/internal/jt;->n:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/g;->c(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/gx;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    iget-object v1, v0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, v0, Lcom/google/android/gms/internal/jt;->m:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/internal/jt;->n:Landroid/content/Context;

    iput-object p1, v0, Lcom/google/android/gms/internal/jt;->o:Lcom/google/android/gms/internal/gx;

    const-string v2, "admob"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "use_https"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v0, Lcom/google/android/gms/internal/jt;->l:Z

    invoke-static {p0}, Lcom/google/android/gms/internal/my;->a(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/gms/common/g;->a(Landroid/content/Context;)I

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/google/android/gms/internal/by;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/internal/dj;->a(Landroid/os/Bundle;)V

    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/internal/jt;->a(Ljava/lang/Thread;)V

    iget-object v2, p1, Lcom/google/android/gms/internal/gx;->b:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/google/android/gms/internal/kf;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/internal/jt;->v:Ljava/lang/String;

    new-instance v2, Lcom/google/android/gms/common/b/d;

    invoke-direct {v2}, Lcom/google/android/gms/common/b/d;-><init>()V

    iput-object v2, v0, Lcom/google/android/gms/internal/jt;->i:Lcom/google/android/gms/common/b/c;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/gms/internal/jt;->m:Z

    :cond_0
    monitor-exit v1

    return-void

    :cond_1
    new-instance v2, Lcom/google/android/gms/internal/di;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/internal/di;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/dj;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 3

    sget-object v0, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    iget-object v1, v0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, v0, Lcom/google/android/gms/internal/jt;->l:Z

    if-eq p1, v2, :cond_0

    iput-boolean p1, v0, Lcom/google/android/gms/internal/jt;->l:Z

    const-string v0, "admob"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "use_https"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ljava/lang/Thread;)V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/jt;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->n:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/internal/jt;->o:Lcom/google/android/gms/internal/gx;

    invoke-static {v0, p1, v2}, Lcom/google/android/gms/internal/ho;->a(Landroid/content/Context;Ljava/lang/Thread;Lcom/google/android/gms/internal/gx;)Lcom/google/android/gms/internal/ho;

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->s:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Ljava/lang/Throwable;)V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    iget-boolean v1, v0, Lcom/google/android/gms/internal/jt;->m:Z

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/android/gms/internal/ho;

    iget-object v2, v0, Lcom/google/android/gms/internal/jt;->n:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/android/gms/internal/jt;->o:Lcom/google/android/gms/internal/gx;

    invoke-direct {v1, v2, v0, v3, v3}, Lcom/google/android/gms/internal/ho;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/gx;Ljava/lang/Thread$UncaughtExceptionHandler;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-virtual {v1, p0}, Lcom/google/android/gms/internal/ho;->a(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public static a(Ljava/util/HashSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/gms/internal/jr;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    iget-object v1, v0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/internal/jt;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(Landroid/content/Context;Lcom/google/android/gms/internal/jv;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 6

    iget-object v2, p0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v0, "app"

    iget-object v1, p0, Lcom/google/android/gms/internal/jt;->h:Lcom/google/android/gms/internal/ju;

    invoke-virtual {v1, p1, p3}, Lcom/google/android/gms/internal/ju;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/internal/jt;->e:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/jw;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/jw;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    const-string v0, "slots"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/jr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/jr;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    const-string v0, "ads"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->d:Ljava/util/HashSet;

    invoke-interface {p2, v0}, Lcom/google/android/gms/internal/jv;->a(Ljava/util/HashSet;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v3
.end method

.method public static c()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    invoke-direct {v0}, Lcom/google/android/gms/internal/jt;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/android/gms/internal/ju;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    invoke-direct {v0}, Lcom/google/android/gms/internal/jt;->k()Lcom/google/android/gms/internal/ju;

    move-result-object v0

    return-object v0
.end method

.method public static e()Z
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    invoke-direct {v0}, Lcom/google/android/gms/internal/jt;->l()Z

    move-result v0

    return v0
.end method

.method public static f()Z
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    invoke-direct {v0}, Lcom/google/android/gms/internal/jt;->m()Z

    move-result v0

    return v0
.end method

.method public static g()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    invoke-direct {v0}, Lcom/google/android/gms/internal/jt;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static h()Landroid/os/Bundle;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    invoke-direct {v0}, Lcom/google/android/gms/internal/jt;->o()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private i()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/jt;->f:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private j()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->j:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/internal/jt;->j:Ljava/math/BigInteger;

    sget-object v3, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/internal/jt;->j:Ljava/math/BigInteger;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private k()Lcom/google/android/gms/internal/ju;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->h:Lcom/google/android/gms/internal/ju;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private l()Z
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/jt;->k:Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/internal/jt;->k:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private m()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/jt;->l:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private n()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->v:Ljava/lang/String;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private o()Landroid/os/Bundle;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->u:Landroid/os/Bundle;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lcom/google/android/gms/internal/ak;
    .locals 10

    const/4 v1, 0x0

    const/4 v0, 0x0

    sget-object v2, Lcom/google/android/gms/internal/jt;->g:Lcom/google/android/gms/internal/jt;

    invoke-direct {v2}, Lcom/google/android/gms/internal/jt;->o()Landroid/os/Bundle;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/internal/by;->a:Lcom/google/android/gms/internal/my;

    iget-object v3, v3, Lcom/google/android/gms/internal/my;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0xe

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v3, v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/internal/jt;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/internal/jt;->p:Lcom/google/android/gms/internal/aj;

    if-nez v2, :cond_4

    instance-of v2, p1, Landroid/app/Activity;

    if-nez v2, :cond_3

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    new-instance v2, Lcom/google/android/gms/internal/aj;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    check-cast p1, Landroid/app/Activity;

    invoke-direct {v2, v0, p1}, Lcom/google/android/gms/internal/aj;-><init>(Landroid/app/Application;Landroid/app/Activity;)V

    iput-object v2, p0, Lcom/google/android/gms/internal/jt;->p:Lcom/google/android/gms/internal/aj;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->r:Lcom/google/android/gms/internal/ai;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/gms/internal/ai;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ai;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/jt;->r:Lcom/google/android/gms/internal/ai;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->q:Lcom/google/android/gms/internal/ak;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/gms/internal/ak;

    iget-object v2, p0, Lcom/google/android/gms/internal/jt;->p:Lcom/google/android/gms/internal/aj;

    iget-object v3, p0, Lcom/google/android/gms/internal/jt;->r:Lcom/google/android/gms/internal/ai;

    iget-object v4, p0, Lcom/google/android/gms/internal/jt;->u:Landroid/os/Bundle;

    new-instance v5, Lcom/google/android/gms/internal/ho;

    iget-object v6, p0, Lcom/google/android/gms/internal/jt;->n:Landroid/content/Context;

    iget-object v7, p0, Lcom/google/android/gms/internal/jt;->o:Lcom/google/android/gms/internal/gx;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/gms/internal/ho;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/gx;Ljava/lang/Thread$UncaughtExceptionHandler;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/internal/ak;-><init>(Lcom/google/android/gms/internal/aj;Lcom/google/android/gms/internal/ai;Landroid/os/Bundle;Lcom/google/android/gms/internal/ho;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/jt;->q:Lcom/google/android/gms/internal/ak;

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->q:Lcom/google/android/gms/internal/ak;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ak;->a()V

    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->q:Lcom/google/android/gms/internal/ak;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/internal/jt;->t:Z

    iput-object p1, p0, Lcom/google/android/gms/internal/jt;->u:Landroid/os/Bundle;

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->s:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/internal/jt;->n:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->s:Ljava/util/LinkedList;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    iget-object v3, p0, Lcom/google/android/gms/internal/jt;->o:Lcom/google/android/gms/internal/gx;

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/internal/ho;->a(Landroid/content/Context;Ljava/lang/Thread;Lcom/google/android/gms/internal/gx;)Lcom/google/android/gms/internal/ho;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final b()Lcom/google/android/gms/common/b/c;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/jt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/jt;->i:Lcom/google/android/gms/common/b/c;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
