.class public final Lcom/google/android/gms/internal/dq;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field final a:Ljava/lang/Object;

.field b:Z

.field c:Lcom/google/android/gms/internal/eb;

.field private final d:Lcom/google/android/gms/internal/fm;

.field private final e:Lcom/google/android/gms/internal/ei;

.field private final f:Landroid/content/Context;

.field private final g:Lcom/google/android/gms/internal/dv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/fm;Lcom/google/android/gms/internal/ei;Lcom/google/android/gms/internal/dv;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/dq;->a:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/dq;->b:Z

    iput-object p1, p0, Lcom/google/android/gms/internal/dq;->f:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/internal/dq;->d:Lcom/google/android/gms/internal/fm;

    iput-object p3, p0, Lcom/google/android/gms/internal/dq;->e:Lcom/google/android/gms/internal/ei;

    iput-object p4, p0, Lcom/google/android/gms/internal/dq;->g:Lcom/google/android/gms/internal/dv;

    return-void
.end method


# virtual methods
.method public final a(JJ)Lcom/google/android/gms/internal/ed;
    .locals 17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/internal/dq;->g:Lcom/google/android/gms/internal/dv;

    iget-object v4, v4, Lcom/google/android/gms/internal/dv;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/gms/internal/ds;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Trying mediation network: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v9, Lcom/google/android/gms/internal/ds;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v9, Lcom/google/android/gms/internal/ds;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_1
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gms/internal/dq;->a:Ljava/lang/Object;

    monitor-enter v15

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/gms/internal/dq;->b:Z

    if-eqz v4, :cond_3

    new-instance v4, Lcom/google/android/gms/internal/ed;

    const/4 v5, -0x1

    invoke-direct {v4, v5}, Lcom/google/android/gms/internal/ed;-><init>(I)V

    monitor-exit v15

    :cond_2
    :goto_1
    return-object v4

    :cond_3
    new-instance v4, Lcom/google/android/gms/internal/eb;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/internal/dq;->f:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/internal/dq;->e:Lcom/google/android/gms/internal/ei;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/internal/dq;->g:Lcom/google/android/gms/internal/dv;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/internal/dq;->d:Lcom/google/android/gms/internal/fm;

    iget-object v10, v10, Lcom/google/android/gms/internal/fm;->c:Lcom/google/android/gms/internal/ba;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/internal/dq;->d:Lcom/google/android/gms/internal/fm;

    iget-object v11, v11, Lcom/google/android/gms/internal/fm;->d:Lcom/google/android/gms/internal/bd;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/internal/dq;->d:Lcom/google/android/gms/internal/fm;

    iget-object v12, v12, Lcom/google/android/gms/internal/fm;->k:Lcom/google/android/gms/internal/gx;

    invoke-direct/range {v4 .. v12}, Lcom/google/android/gms/internal/eb;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/internal/ei;Lcom/google/android/gms/internal/dv;Lcom/google/android/gms/internal/ds;Lcom/google/android/gms/internal/ba;Lcom/google/android/gms/internal/bd;Lcom/google/android/gms/internal/gx;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/gms/internal/dq;->c:Lcom/google/android/gms/internal/eb;

    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/internal/dq;->c:Lcom/google/android/gms/internal/eb;

    move-wide/from16 v0, p1

    move-wide/from16 v2, p3

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/google/android/gms/internal/eb;->a(JJ)Lcom/google/android/gms/internal/ed;

    move-result-object v4

    iget v5, v4, Lcom/google/android/gms/internal/ed;->a:I

    if-eqz v5, :cond_2

    iget-object v5, v4, Lcom/google/android/gms/internal/ed;->c:Lcom/google/android/gms/internal/el;

    if-eqz v5, :cond_1

    sget-object v5, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    new-instance v6, Lcom/google/android/gms/internal/dr;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v4}, Lcom/google/android/gms/internal/dr;-><init>(Lcom/google/android/gms/internal/dq;Lcom/google/android/gms/internal/ed;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :cond_4
    new-instance v4, Lcom/google/android/gms/internal/ed;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Lcom/google/android/gms/internal/ed;-><init>(I)V

    goto :goto_1
.end method
