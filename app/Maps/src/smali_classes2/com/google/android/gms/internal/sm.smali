.class public abstract Lcom/google/android/gms/internal/sm;
.super Ljava/lang/Object;


# instance fields
.field public volatile p:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/sm;->p:I

    return-void
.end method

.method public static final a(Lcom/google/android/gms/internal/sm;[BII)Lcom/google/android/gms/internal/sm;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/internal/sm;",
            ">(TT;[BII)TT;"
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/google/android/gms/internal/sb;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/internal/sb;-><init>([BII)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/sm;->a(Lcom/google/android/gms/internal/sb;)Lcom/google/android/gms/internal/sm;

    const/4 v1, 0x0

    iget v0, v0, Lcom/google/android/gms/internal/sb;->d:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/si;->e()Lcom/google/android/gms/internal/si;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Lcom/google/android/gms/internal/si; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object p0
.end method

.method public static final a(Lcom/google/android/gms/internal/sm;)[B
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/internal/sm;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/sm;->p:I

    new-array v0, v0, [B

    const/4 v1, 0x0

    array-length v2, v0

    :try_start_0
    new-instance v3, Lcom/google/android/gms/internal/sc;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/gms/internal/sc;-><init>([BII)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/internal/sm;->a(Lcom/google/android/gms/internal/sc;)V

    iget v1, v3, Lcom/google/android/gms/internal/sc;->a:I

    iget v2, v3, Lcom/google/android/gms/internal/sc;->b:I

    sub-int/2addr v1, v2

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did not write as much data as expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract a(Lcom/google/android/gms/internal/sb;)Lcom/google/android/gms/internal/sm;
.end method

.method public a(Lcom/google/android/gms/internal/sc;)V
    .locals 0

    return-void
.end method

.method public final d()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/sm;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/sm;->p:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/android/gms/internal/sn;->a(Lcom/google/android/gms/internal/sm;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
