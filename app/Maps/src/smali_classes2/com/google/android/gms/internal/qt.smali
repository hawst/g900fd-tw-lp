.class public Lcom/google/android/gms/internal/qt;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/people/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/o;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/o;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/gms/common/api/s",
            "<",
            "Lcom/google/android/gms/people/e;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/internal/qv;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/internal/qv;-><init>(Lcom/google/android/gms/internal/qt;Lcom/google/android/gms/common/api/o;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/o;Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/gms/common/api/s;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/o;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II)",
            "Lcom/google/android/gms/common/api/s",
            "<",
            "Lcom/google/android/gms/people/e;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/internal/qu;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/qu;-><init>(Lcom/google/android/gms/internal/qt;Lcom/google/android/gms/common/api/o;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
