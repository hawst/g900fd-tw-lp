.class public final Lcom/google/android/gms/maps/h;
.super Ljava/lang/Object;


# direct methods
.method public static a(Landroid/content/Context;)I
    .locals 2

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/maps/internal/cv;->a(Landroid/content/Context;)Lcom/google/android/gms/maps/internal/am;
    :try_end_0
    .catch Lcom/google/android/gms/common/e; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :try_start_1
    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/am;->a()Lcom/google/android/gms/maps/internal/a;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/d;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/d;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :catch_1
    move-exception v0

    iget v0, v0, Lcom/google/android/gms/common/e;->a:I

    :goto_0
    return v0

    :cond_1
    :try_start_2
    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/am;->b()Lcom/google/android/gms/maps/model/internal/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/maps/model/b;->a:Lcom/google/android/gms/maps/model/internal/j;

    if-nez v1, :cond_3

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Lcom/google/android/gms/maps/model/internal/j;

    sput-object v0, Lcom/google/android/gms/maps/model/b;->a:Lcom/google/android/gms/maps/model/internal/j;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
