.class Lcom/google/android/gms/internal/i;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/internal/z;

.field final synthetic b:Lcom/google/android/gms/internal/h;

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/gms/internal/z;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/h;Lcom/google/android/gms/internal/z;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/gms/internal/i;->b:Lcom/google/android/gms/internal/h;

    iput-object p2, p0, Lcom/google/android/gms/internal/i;->a:Lcom/google/android/gms/internal/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/google/android/gms/internal/i;->a:Lcom/google/android/gms/internal/z;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/i;->c:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/gms/internal/i;->b:Lcom/google/android/gms/internal/h;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/internal/h;->d:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/i;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/z;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/i;->b:Lcom/google/android/gms/internal/h;

    iget-object v2, v1, Lcom/google/android/gms/internal/h;->c:Lcom/google/android/gms/internal/ba;

    iget-object v1, v0, Lcom/google/android/gms/internal/z;->a:Lcom/google/android/gms/internal/sv;

    iget-object v1, v1, Lcom/google/android/gms/internal/sv;->a:Lcom/google/android/gms/internal/z$a;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/z$a;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v3, v1, Landroid/view/View;

    if-eqz v3, :cond_1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/gms/internal/kf;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lcom/google/android/gms/internal/z;->c:Z

    if-nez v1, :cond_1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/z;->a(Lcom/google/android/gms/internal/ba;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/internal/z;->b:Lcom/google/android/gms/internal/h;

    const-wide/32 v4, 0xea60

    invoke-virtual {v0, v2, v4, v5}, Lcom/google/android/gms/internal/h;->a(Lcom/google/android/gms/internal/ba;J)V

    goto :goto_0
.end method
