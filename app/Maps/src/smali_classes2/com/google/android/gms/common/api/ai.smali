.class public Lcom/google/android/gms/common/api/ai;
.super Landroid/support/v4/app/j;

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/support/v4/app/an;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/j;",
        "Landroid/content/DialogInterface$OnCancelListener;",
        "Landroid/support/v4/app/an",
        "<",
        "Lcom/google/android/gms/common/a;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/gms/common/api/ak;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:I

.field private d:Lcom/google/android/gms/common/a;

.field private final e:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/support/v4/app/j;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/api/ai;->c:I

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/ai;->e:Landroid/os/Handler;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/ai;->a:Landroid/util/SparseArray;

    return-void
.end method

.method public static a(Landroid/support/v4/app/m;)Lcom/google/android/gms/common/api/ai;
    .locals 4

    const-string v0, "Must be called from main thread of process"

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/m;->b:Landroid/support/v4/app/s;

    :try_start_0
    const-string v0, "GmsSupportLifecycleFragment"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/r;->a(Ljava/lang/String;)Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/ai;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    iget-boolean v2, v0, Landroid/support/v4/app/j;->w:Z

    if-eqz v2, :cond_2

    :cond_1
    new-instance v0, Lcom/google/android/gms/common/api/ai;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/ai;-><init>()V

    invoke-virtual {v1}, Landroid/support/v4/app/r;->a()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string v3, "GmsSupportLifecycleFragment"

    invoke-virtual {v2, v0, v3}, Landroid/support/v4/app/ae;->a(Landroid/support/v4/app/j;Ljava/lang/String;)Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ae;->a()I

    invoke-virtual {v1}, Landroid/support/v4/app/r;->b()Z

    :cond_2
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Fragment with tag GmsSupportLifecycleFragment is not a SupportLifecycleFragment"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(ILcom/google/android/gms/common/a;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/ai;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/ak;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/ai;->g()Landroid/support/v4/app/am;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/support/v4/app/am;->a(I)V

    iget-object v1, p0, Lcom/google/android/gms/common/api/ai;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    iget-object v0, v0, Lcom/google/android/gms/common/api/ak;->b:Lcom/google/android/gms/common/api/r;

    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Lcom/google/android/gms/common/api/r;->a(Lcom/google/android/gms/common/a;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/common/api/ai;->m()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/ai;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/ai;->m()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/ai;ILcom/google/android/gms/common/a;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/api/ai;->a(ILcom/google/android/gms/common/a;)V

    return-void
.end method

.method private m()V
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/common/api/ai;->b:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/api/ai;->c:I

    iput-object v6, p0, Lcom/google/android/gms/common/api/ai;->d:Lcom/google/android/gms/common/a;

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/ai;->g()Landroid/support/v4/app/am;

    move-result-object v2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/common/api/ai;->a:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/common/api/ai;->a:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/gms/common/api/ai;->b(I)Lcom/google/android/gms/common/api/aj;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-boolean v5, v4, Lcom/google/android/gms/common/api/aj;->i:Z

    if-eqz v5, :cond_0

    iput-boolean v1, v4, Lcom/google/android/gms/common/api/aj;->i:Z

    iget-boolean v5, v4, Landroid/support/v4/a/a;->c:Z

    if-eqz v5, :cond_0

    iget-boolean v5, v4, Landroid/support/v4/a/a;->d:Z

    if-nez v5, :cond_0

    iget-object v4, v4, Lcom/google/android/gms/common/api/aj;->h:Lcom/google/android/gms/common/api/o;

    invoke-interface {v4}, Lcom/google/android/gms/common/api/o;->b()V

    :cond_0
    invoke-virtual {v2, v3, v6, p0}, Landroid/support/v4/app/am;->a(ILandroid/os/Bundle;Landroid/support/v4/app/an;)Landroid/support/v4/a/a;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/support/v4/a/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/support/v4/a/a",
            "<",
            "Lcom/google/android/gms/common/a;",
            ">;"
        }
    .end annotation

    new-instance v1, Lcom/google/android/gms/common/api/aj;

    iget-object v2, p0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    iget-object v0, p0, Lcom/google/android/gms/common/api/ai;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/ak;

    iget-object v0, v0, Lcom/google/android/gms/common/api/ak;->a:Lcom/google/android/gms/common/api/o;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/api/aj;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/o;)V

    return-object v1
.end method

.method public final a(II)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/common/api/ai;->m()V

    :goto_1
    return-void

    :pswitch_0
    iget-object v2, p0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    invoke-static {v2}, Lcom/google/android/gms/common/g;->a(Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :pswitch_1
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/gms/common/api/ai;->c:I

    iget-object v1, p0, Lcom/google/android/gms/common/api/ai;->d:Lcom/google/android/gms/common/a;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/api/ai;->a(ILcom/google/android/gms/common/a;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/j;->a(Landroid/app/Activity;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/ai;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/api/ai;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/common/api/ai;->b(I)Lcom/google/android/gms/common/api/aj;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/ai;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/ak;

    iget-object v0, v0, Lcom/google/android/gms/common/api/ak;->a:Lcom/google/android/gms/common/api/o;

    iget-object v3, v3, Lcom/google/android/gms/common/api/aj;->h:Lcom/google/android/gms/common/api/o;

    if-eq v0, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/ai;->g()Landroid/support/v4/app/am;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Landroid/support/v4/app/am;->b(ILandroid/os/Bundle;Landroid/support/v4/app/an;)Landroid/support/v4/a/a;

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/ai;->g()Landroid/support/v4/app/am;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Landroid/support/v4/app/am;->a(ILandroid/os/Bundle;Landroid/support/v4/app/an;)Landroid/support/v4/a/a;

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/j;->a(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "resolving_error"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/ai;->b:Z

    const-string v0, "failed_client_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/api/ai;->c:I

    iget v0, p0, Lcom/google/android/gms/common/api/ai;->c:I

    if-ltz v0, :cond_0

    new-instance v1, Lcom/google/android/gms/common/a;

    const-string v0, "failed_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v0, "failed_resolution"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/a;-><init>(ILandroid/app/PendingIntent;)V

    iput-object v1, p0, Lcom/google/android/gms/common/api/ai;->d:Lcom/google/android/gms/common/a;

    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v4/a/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/a/a",
            "<",
            "Lcom/google/android/gms/common/a;",
            ">;)V"
        }
    .end annotation

    iget v0, p1, Landroid/support/v4/a/a;->a:I

    iget v1, p0, Lcom/google/android/gms/common/api/ai;->c:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/ai;->m()V

    :cond_0
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/a;Ljava/lang/Object;)V
    .locals 3

    const/4 v1, 0x1

    check-cast p2, Lcom/google/android/gms/common/a;

    iget v0, p2, Lcom/google/android/gms/common/a;->c:I

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    iget v0, p1, Landroid/support/v4/a/a;->a:I

    iget v1, p0, Lcom/google/android/gms/common/api/ai;->c:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/ai;->m()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget v0, p1, Landroid/support/v4/a/a;->a:I

    iget-boolean v2, p0, Lcom/google/android/gms/common/api/ai;->b:Z

    if-nez v2, :cond_0

    iput-boolean v1, p0, Lcom/google/android/gms/common/api/ai;->b:Z

    iput v0, p0, Lcom/google/android/gms/common/api/ai;->c:I

    iput-object p2, p0, Lcom/google/android/gms/common/api/ai;->d:Lcom/google/android/gms/common/a;

    iget-object v1, p0, Lcom/google/android/gms/common/api/ai;->e:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/common/api/al;

    invoke-direct {v2, p0, v0, p2}, Lcom/google/android/gms/common/api/al;-><init>(Lcom/google/android/gms/common/api/ai;ILcom/google/android/gms/common/a;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method final b(I)Lcom/google/android/gms/common/api/aj;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/ai;->g()Landroid/support/v4/app/am;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/am;->b(I)Landroid/support/v4/a/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/aj;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unknown loader in SupportLifecycleFragment"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()V
    .locals 4

    invoke-super {p0}, Landroid/support/v4/app/j;->b()V

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/ai;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/common/api/ai;->a:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/ai;->g()Landroid/support/v4/app/am;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/api/ai;->a:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/am;->a(ILandroid/os/Bundle;Landroid/support/v4/app/an;)Landroid/support/v4/a/a;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/j;->e(Landroid/os/Bundle;)V

    const-string v0, "resolving_error"

    iget-boolean v1, p0, Lcom/google/android/gms/common/api/ai;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget v0, p0, Lcom/google/android/gms/common/api/ai;->c:I

    if-ltz v0, :cond_0

    const-string v0, "failed_client_id"

    iget v1, p0, Lcom/google/android/gms/common/api/ai;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "failed_status"

    iget-object v1, p0, Lcom/google/android/gms/common/api/ai;->d:Lcom/google/android/gms/common/a;

    iget v1, v1, Lcom/google/android/gms/common/a;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "failed_resolution"

    iget-object v1, p0, Lcom/google/android/gms/common/api/ai;->d:Lcom/google/android/gms/common/a;

    iget-object v1, v1, Lcom/google/android/gms/common/a;->b:Landroid/app/PendingIntent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    iget v0, p0, Lcom/google/android/gms/common/api/ai;->c:I

    iget-object v1, p0, Lcom/google/android/gms/common/api/ai;->d:Lcom/google/android/gms/common/a;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/api/ai;->a(ILcom/google/android/gms/common/a;)V

    return-void
.end method
