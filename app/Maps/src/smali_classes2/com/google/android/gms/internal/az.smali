.class public Lcom/google/android/gms/internal/az;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# static fields
.field public static final a:Lcom/google/android/gms/internal/az;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/az;

    invoke-direct {v0}, Lcom/google/android/gms/internal/az;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/az;->a:Lcom/google/android/gms/internal/az;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/android/gms/internal/az;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/az;->a:Lcom/google/android/gms/internal/az;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/br;)Lcom/google/android/gms/internal/ba;
    .locals 17

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/br;->b:Ljava/util/Date;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    :goto_0
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/gms/internal/br;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v7, v0, Lcom/google/android/gms/internal/br;->d:I

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/br;->e:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    :goto_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/br;->m:Ljava/util/Set;

    invoke-static/range {p0 .. p0}, Lcom/google/android/gms/internal/kt;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    move-object/from16 v0, p1

    iget v10, v0, Lcom/google/android/gms/internal/br;->l:I

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/gms/internal/br;->f:Landroid/location/Location;

    const-class v2, Lcom/google/a/a/a/a;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/internal/br;->h:Landroid/os/Bundle;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    move-object/from16 v0, p1

    iget-boolean v11, v0, Lcom/google/android/gms/internal/br;->g:Z

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/gms/internal/br;->j:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/br;->k:Lcom/google/android/gms/ads/search/a;

    if-eqz v2, :cond_2

    new-instance v13, Lcom/google/android/gms/internal/bo;

    invoke-direct {v13, v2}, Lcom/google/android/gms/internal/bo;-><init>(Lcom/google/android/gms/ads/search/a;)V

    :goto_2
    new-instance v2, Lcom/google/android/gms/internal/ba;

    const/4 v3, 0x4

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/internal/br;->h:Landroid/os/Bundle;

    move-object/from16 v16, v0

    invoke-direct/range {v2 .. v16}, Lcom/google/android/gms/internal/ba;-><init>(IJLandroid/os/Bundle;ILjava/util/List;ZIZLjava/lang/String;Lcom/google/android/gms/internal/bo;Landroid/location/Location;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v2

    :cond_0
    const-wide/16 v4, -0x1

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    goto :goto_1

    :cond_2
    const/4 v13, 0x0

    goto :goto_2
.end method
