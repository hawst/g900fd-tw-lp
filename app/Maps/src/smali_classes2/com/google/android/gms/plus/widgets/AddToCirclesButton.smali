.class public final Lcom/google/android/gms/plus/widgets/AddToCirclesButton;
.super Landroid/widget/FrameLayout;


# static fields
.field private static a:Landroid/content/Context;


# instance fields
.field private final b:Lcom/google/android/gms/internal/rd;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {p1}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a(Landroid/content/Context;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/internal/rd;

    iput-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->b:Lcom/google/android/gms/internal/rd;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->b:Lcom/google/android/gms/internal/rd;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v2

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v1}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v3

    invoke-interface {v0, v2, v1, v3}, Lcom/google/android/gms/internal/rd;->a(Lcom/google/android/gms/a/o;Lcom/google/android/gms/a/o;Lcom/google/android/gms/a/o;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->b:Lcom/google/android/gms/internal/rd;

    invoke-interface {v0}, Lcom/google/android/gms/internal/rd;->a()Lcom/google/android/gms/a/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/a/r;->a(Lcom/google/android/gms/a/o;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->c:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/internal/rd;",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/common/g;->d(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:Landroid/content/Context;

    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:Landroid/content/Context;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    :try_start_0
    const-string v1, "com.google.android.gms.plus.circlesbutton.AddToCirclesButtonImpl$DynamiteHost"

    invoke-virtual {v0, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    new-instance v1, Landroid/util/Pair;

    invoke-static {v0}, Lcom/google/android/gms/internal/re;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/rd;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:Landroid/content/Context;

    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_1
    :goto_1
    new-instance v0, Landroid/util/Pair;

    new-instance v1, Lcom/google/android/gms/plus/widgets/a;

    invoke-direct {v1}, Lcom/google/android/gms/plus/widgets/a;-><init>()V

    invoke-direct {v0, v1, p0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method
