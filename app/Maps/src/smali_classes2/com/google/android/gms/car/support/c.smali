.class final Lcom/google/android/gms/car/support/c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/car/support/c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:[I

.field final b:I

.field final c:I

.field final d:Ljava/lang/String;

.field final e:I

.field final f:I

.field final g:Ljava/lang/CharSequence;

.field final h:I

.field final i:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/support/bi;

    invoke-direct {v0}, Lcom/google/android/gms/car/support/bi;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/support/c;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/c;->a:[I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/c;->b:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/c;->c:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/c;->d:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/c;->e:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/c;->f:I

    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/gms/car/support/c;->g:Ljava/lang/CharSequence;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/c;->h:I

    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/gms/car/support/c;->i:Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/support/bg;)V
    .locals 8

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/google/android/gms/car/support/bg;->b:Lcom/google/android/gms/car/support/bh;

    move-object v1, v0

    move v0, v3

    :goto_0
    if-eqz v1, :cond_1

    iget-object v2, v1, Lcom/google/android/gms/car/support/bh;->i:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/android/gms/car/support/bh;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v1, v1, Lcom/google/android/gms/car/support/bh;->a:Lcom/google/android/gms/car/support/bh;

    goto :goto_0

    :cond_1
    iget v1, p1, Lcom/google/android/gms/car/support/bg;->d:I

    mul-int/lit8 v1, v1, 0x7

    add-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/gms/car/support/c;->a:[I

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/bg;->g:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not on back stack"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p1, Lcom/google/android/gms/car/support/bg;->b:Lcom/google/android/gms/car/support/bh;

    move-object v5, v0

    move v0, v3

    :goto_1
    if-eqz v5, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v2, v0, 0x1

    iget v4, v5, Lcom/google/android/gms/car/support/bh;->c:I

    aput v4, v1, v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v4, v2, 0x1

    iget-object v0, v5, Lcom/google/android/gms/car/support/bh;->d:Lcom/google/android/gms/car/support/ab;

    if-eqz v0, :cond_3

    iget-object v0, v5, Lcom/google/android/gms/car/support/bh;->d:Lcom/google/android/gms/car/support/ab;

    iget v0, v0, Lcom/google/android/gms/car/support/ab;->f:I

    :goto_2
    aput v0, v1, v2

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v1, v4, 0x1

    iget v2, v5, Lcom/google/android/gms/car/support/bh;->e:I

    aput v2, v0, v4

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v2, v1, 0x1

    iget v4, v5, Lcom/google/android/gms/car/support/bh;->f:I

    aput v4, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v1, v2, 0x1

    iget v4, v5, Lcom/google/android/gms/car/support/bh;->g:I

    aput v4, v0, v2

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v2, v1, 0x1

    iget v4, v5, Lcom/google/android/gms/car/support/bh;->h:I

    aput v4, v0, v1

    iget-object v0, v5, Lcom/google/android/gms/car/support/bh;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    iget-object v0, v5, Lcom/google/android/gms/car/support/bh;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v1, v2, 0x1

    aput v6, v0, v2

    move v2, v3

    :goto_3
    if-ge v2, v6, :cond_4

    iget-object v7, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v4, v1, 0x1

    iget-object v0, v5, Lcom/google/android/gms/car/support/bh;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    iget v0, v0, Lcom/google/android/gms/car/support/ab;->f:I

    aput v0, v7, v1

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v1, v4

    goto :goto_3

    :cond_3
    const/4 v0, -0x1

    goto :goto_2

    :cond_4
    move v0, v1

    :goto_4
    iget-object v1, v5, Lcom/google/android/gms/car/support/bh;->a:Lcom/google/android/gms/car/support/bh;

    move-object v5, v1

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v0, v2, 0x1

    aput v3, v1, v2

    goto :goto_4

    :cond_6
    iget v0, p1, Lcom/google/android/gms/car/support/bg;->e:I

    iput v0, p0, Lcom/google/android/gms/car/support/c;->b:I

    iget v0, p1, Lcom/google/android/gms/car/support/bg;->f:I

    iput v0, p0, Lcom/google/android/gms/car/support/c;->c:I

    iget-object v0, p1, Lcom/google/android/gms/car/support/bg;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/car/support/c;->d:Ljava/lang/String;

    iget v0, p1, Lcom/google/android/gms/car/support/bg;->i:I

    iput v0, p0, Lcom/google/android/gms/car/support/c;->e:I

    iget v0, p1, Lcom/google/android/gms/car/support/bg;->j:I

    iput v0, p0, Lcom/google/android/gms/car/support/c;->f:I

    iget-object v0, p1, Lcom/google/android/gms/car/support/bg;->k:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/gms/car/support/c;->g:Ljava/lang/CharSequence;

    iget v0, p1, Lcom/google/android/gms/car/support/bg;->l:I

    iput v0, p0, Lcom/google/android/gms/car/support/c;->h:I

    iget-object v0, p1, Lcom/google/android/gms/car/support/bg;->m:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/gms/car/support/c;->i:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/car/support/g;)Lcom/google/android/gms/car/support/bg;
    .locals 10

    const/4 v9, 0x1

    const/4 v1, 0x0

    new-instance v4, Lcom/google/android/gms/car/support/bg;

    invoke-direct {v4, p1}, Lcom/google/android/gms/car/support/bg;-><init>(Lcom/google/android/gms/car/support/g;)V

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/car/support/c;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_3

    new-instance v5, Lcom/google/android/gms/car/support/bh;

    invoke-direct {v5}, Lcom/google/android/gms/car/support/bh;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v3, v0, 0x1

    aget v0, v2, v0

    iput v0, v5, Lcom/google/android/gms/car/support/bh;->c:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v2, v3, 0x1

    aget v0, v0, v3

    if-ltz v0, :cond_0

    iget-object v3, p1, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    iput-object v0, v5, Lcom/google/android/gms/car/support/bh;->d:Lcom/google/android/gms/car/support/ab;

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v3, v2, 0x1

    aget v0, v0, v2

    iput v0, v5, Lcom/google/android/gms/car/support/bh;->e:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v2, v3, 0x1

    aget v0, v0, v3

    iput v0, v5, Lcom/google/android/gms/car/support/bh;->f:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v3, v2, 0x1

    aget v0, v0, v2

    iput v0, v5, Lcom/google/android/gms/car/support/bh;->g:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v2, v3, 0x1

    aget v0, v0, v3

    iput v0, v5, Lcom/google/android/gms/car/support/bh;->h:I

    iget-object v3, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v0, v2, 0x1

    aget v6, v3, v2

    if-lez v6, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, v5, Lcom/google/android/gms/car/support/bh;->i:Ljava/util/ArrayList;

    move v2, v1

    :goto_2
    if-ge v2, v6, :cond_1

    iget-object v7, p1, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/google/android/gms/car/support/c;->a:[I

    add-int/lit8 v3, v0, 0x1

    aget v0, v8, v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    iget-object v7, v5, Lcom/google/android/gms/car/support/bh;->i:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_2

    :cond_0
    const/4 v0, 0x0

    iput-object v0, v5, Lcom/google/android/gms/car/support/bh;->d:Lcom/google/android/gms/car/support/ab;

    goto :goto_1

    :cond_1
    iget-object v2, v4, Lcom/google/android/gms/car/support/bg;->b:Lcom/google/android/gms/car/support/bh;

    if-nez v2, :cond_2

    iput-object v5, v4, Lcom/google/android/gms/car/support/bg;->c:Lcom/google/android/gms/car/support/bh;

    iput-object v5, v4, Lcom/google/android/gms/car/support/bg;->b:Lcom/google/android/gms/car/support/bh;

    :goto_3
    iput v1, v5, Lcom/google/android/gms/car/support/bh;->e:I

    iput v1, v5, Lcom/google/android/gms/car/support/bh;->f:I

    iput v1, v5, Lcom/google/android/gms/car/support/bh;->g:I

    iput v1, v5, Lcom/google/android/gms/car/support/bh;->h:I

    iget v2, v4, Lcom/google/android/gms/car/support/bg;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v4, Lcom/google/android/gms/car/support/bg;->d:I

    goto/16 :goto_0

    :cond_2
    iget-object v2, v4, Lcom/google/android/gms/car/support/bg;->c:Lcom/google/android/gms/car/support/bh;

    iput-object v2, v5, Lcom/google/android/gms/car/support/bh;->b:Lcom/google/android/gms/car/support/bh;

    iget-object v2, v4, Lcom/google/android/gms/car/support/bg;->c:Lcom/google/android/gms/car/support/bh;

    iput-object v5, v2, Lcom/google/android/gms/car/support/bh;->a:Lcom/google/android/gms/car/support/bh;

    iput-object v5, v4, Lcom/google/android/gms/car/support/bg;->c:Lcom/google/android/gms/car/support/bh;

    goto :goto_3

    :cond_3
    iget v0, p0, Lcom/google/android/gms/car/support/c;->b:I

    iput v0, v4, Lcom/google/android/gms/car/support/bg;->e:I

    iget v0, p0, Lcom/google/android/gms/car/support/c;->c:I

    iput v0, v4, Lcom/google/android/gms/car/support/bg;->f:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->d:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/car/support/bg;->h:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/gms/car/support/c;->e:I

    iput v0, v4, Lcom/google/android/gms/car/support/bg;->i:I

    iput-boolean v9, v4, Lcom/google/android/gms/car/support/bg;->g:Z

    iget v0, p0, Lcom/google/android/gms/car/support/c;->f:I

    iput v0, v4, Lcom/google/android/gms/car/support/bg;->j:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->g:Ljava/lang/CharSequence;

    iput-object v0, v4, Lcom/google/android/gms/car/support/bg;->k:Ljava/lang/CharSequence;

    iget v0, p0, Lcom/google/android/gms/car/support/c;->h:I

    iput v0, v4, Lcom/google/android/gms/car/support/bg;->l:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->i:Ljava/lang/CharSequence;

    iput-object v0, v4, Lcom/google/android/gms/car/support/bg;->m:Ljava/lang/CharSequence;

    invoke-virtual {v4, v9}, Lcom/google/android/gms/car/support/bg;->a(I)V

    return-object v4
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    iget v0, p0, Lcom/google/android/gms/car/support/c;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/gms/car/support/c;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/c;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/gms/car/support/c;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->g:Ljava/lang/CharSequence;

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    iget v0, p0, Lcom/google/android/gms/car/support/c;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->i:Ljava/lang/CharSequence;

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    return-void
.end method
