.class public Lcom/google/android/gms/internal/du;
.super Lcom/google/android/gms/internal/fv;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# static fields
.field private static final j:I


# instance fields
.field final a:Landroid/app/Activity;

.field b:Lcom/google/android/gms/internal/dy;

.field c:Lcom/google/android/gms/internal/gz;

.field d:Lcom/google/android/gms/internal/dz;

.field e:Z

.field f:Z

.field g:Landroid/widget/FrameLayout;

.field h:Landroid/webkit/WebChromeClient$CustomViewCallback;

.field i:Landroid/widget/RelativeLayout;

.field private k:Lcom/google/android/gms/internal/dw;

.field private l:Lcom/google/android/gms/internal/fl;

.field private m:Z

.field private n:Z

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, v0, v0, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/gms/internal/du;->j:I

    return-void
.end method

.method static a(IIII)Landroid/widget/RelativeLayout$LayoutParams;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p2, p3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p0, p1, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/dw;)V
    .locals 3

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v0, "com.google.android.gms.ads.AdActivity"

    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.ads.internal.overlay.useClientJar"

    iget-object v2, p1, Lcom/google/android/gms/internal/dw;->n:Lcom/google/android/gms/internal/gx;

    iget-boolean v2, v2, Lcom/google/android/gms/internal/gx;->e:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {v1, p1}, Lcom/google/android/gms/internal/dw;->a(Landroid/content/Intent;Lcom/google/android/gms/internal/dw;)V

    const/16 v0, 0x15

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    const/high16 v0, 0x80000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    instance-of v0, p0, Landroid/app/Activity;

    if-nez v0, :cond_1

    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_1
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Z)V
    .locals 14

    const/high16 v5, 0x1000000

    const/16 v3, 0x400

    const/4 v13, -0x1

    const/4 v4, 0x0

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/du;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    invoke-virtual {v0, v2}, Landroid/app/Activity;->requestWindowFeature(I)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/internal/du;->o:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v1, v1, Lcom/google/android/gms/internal/dw;->q:Lcom/google/android/gms/internal/ad;

    iget-boolean v1, v1, Lcom/google/android/gms/internal/ad;->c:Z

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget v1, v1, Lcom/google/android/gms/internal/dw;->k:I

    iget-object v3, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    invoke-virtual {v3, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v1, v3, :cond_3

    invoke-virtual {v0, v5, v5}, Landroid/view/Window;->setFlags(II)V

    :cond_3
    new-instance v0, Lcom/google/android/gms/internal/du$b;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v3, v3, Lcom/google/android/gms/internal/dw;->p:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/du$b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/du;->i:Landroid/widget/RelativeLayout;

    iget-boolean v0, p0, Lcom/google/android/gms/internal/du;->o:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->i:Landroid/widget/RelativeLayout;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->i:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    iput-boolean v2, p0, Lcom/google/android/gms/internal/du;->e:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->e:Lcom/google/android/gms/internal/gz;

    iget-object v0, v0, Lcom/google/android/gms/internal/gz;->a:Lcom/google/android/gms/internal/kx;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/kx;->a()Z

    move-result v3

    if-eqz p1, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v1, v1, Lcom/google/android/gms/internal/dw;->e:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->d()Lcom/google/android/gms/internal/bd;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v5, v5, Lcom/google/android/gms/internal/dw;->n:Lcom/google/android/gms/internal/gx;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/gz;->a(Landroid/content/Context;Lcom/google/android/gms/internal/bd;ZZLcom/google/android/gms/internal/ne;Lcom/google/android/gms/internal/gx;)Lcom/google/android/gms/internal/gz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    iget-object v5, v0, Lcom/google/android/gms/internal/gz;->a:Lcom/google/android/gms/internal/kx;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v8, v0, Lcom/google/android/gms/internal/dw;->f:Lcom/google/android/gms/internal/cp;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v9, v0, Lcom/google/android/gms/internal/dw;->j:Lcom/google/android/gms/internal/ft;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v11, v0, Lcom/google/android/gms/internal/dw;->o:Lcom/google/android/gms/internal/db;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->e:Lcom/google/android/gms/internal/gz;

    iget-object v0, v0, Lcom/google/android/gms/internal/gz;->a:Lcom/google/android/gms/internal/kx;

    iget-object v12, v0, Lcom/google/android/gms/internal/kx;->m:Lcom/google/android/gms/internal/a;

    move-object v6, v4

    move-object v7, v4

    move v10, v2

    invoke-virtual/range {v5 .. v12}, Lcom/google/android/gms/internal/kx;->a(Lcom/google/android/gms/internal/sq;Lcom/google/android/gms/internal/fp;Lcom/google/android/gms/internal/cp;Lcom/google/android/gms/internal/ft;ZLcom/google/android/gms/internal/db;Lcom/google/android/gms/internal/a;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    iget-object v0, v0, Lcom/google/android/gms/internal/gz;->a:Lcom/google/android/gms/internal/kx;

    new-instance v1, Lcom/google/android/gms/internal/fj;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/fj;-><init>(Lcom/google/android/gms/internal/du;)V

    iput-object v1, v0, Lcom/google/android/gms/internal/kx;->f:Lcom/google/android/gms/internal/kz;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->m:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v1, v1, Lcom/google/android/gms/internal/dw;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gz;->loadUrl(Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    iget-object v1, v0, Lcom/google/android/gms/internal/gz;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p0, v0, Lcom/google/android/gms/internal/gz;->f:Lcom/google/android/gms/internal/du;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_4

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_4

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/internal/du;->o:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    sget v1, Lcom/google/android/gms/internal/du;->j:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gz;->setBackgroundColor(I)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->i:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0, v1, v13, v13}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;II)V

    if-nez p1, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->b()V

    :cond_6
    invoke-virtual {p0, v3}, Lcom/google/android/gms/internal/du;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/gz;->i:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->d:Lcom/google/android/gms/internal/dz;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->d:Lcom/google/android/gms/internal/dz;

    iget-object v1, v0, Lcom/google/android/gms/internal/dz;->a:Landroid/widget/ImageButton;

    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_7
    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->i:Landroid/widget/RelativeLayout;

    sget v1, Lcom/google/android/gms/internal/du;->j:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->i:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v5, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v6, v0, Lcom/google/android/gms/internal/dw;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v7, v0, Lcom/google/android/gms/internal/dw;->i:Ljava/lang/String;

    const-string v8, "text/html"

    const-string v9, "UTF-8"

    move-object v10, v4

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/gms/internal/gz;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_a
    new-instance v0, Lcom/google/android/gms/internal/fk;

    const-string v1, "No URL or HTML to display in ad overlay."

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/fk;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->e:Lcom/google/android/gms/internal/gz;

    iput-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    iget-object v0, v0, Lcom/google/android/gms/internal/gz;->b:Lcom/google/android/gms/internal/kv;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/kv;->setBaseContext(Landroid/content/Context;)V

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private g()V
    .locals 4

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/du;->n:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v2, p0, Lcom/google/android/gms/internal/du;->n:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    const-string v2, "version"

    iget-object v3, v0, Lcom/google/android/gms/internal/gz;->e:Lcom/google/android/gms/internal/gx;

    iget-object v3, v3, Lcom/google/android/gms/internal/gx;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "onhide"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/internal/gz;->a(Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->i:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->l:Lcom/google/android/gms/internal/fl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    const/4 v1, 0x0

    iget-object v2, v0, Lcom/google/android/gms/internal/gz;->c:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iput-boolean v1, v0, Lcom/google/android/gms/internal/gz;->h:Z

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->f()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->l:Lcom/google/android/gms/internal/fl;

    iget-object v0, v0, Lcom/google/android/gms/internal/fl;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    iget-object v2, p0, Lcom/google/android/gms/internal/du;->l:Lcom/google/android/gms/internal/fl;

    iget v2, v2, Lcom/google/android/gms/internal/fl;->a:I

    iget-object v3, p0, Lcom/google/android/gms/internal/du;->l:Lcom/google/android/gms/internal/fl;

    iget-object v3, v3, Lcom/google/android/gms/internal/fl;->b:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->d:Lcom/google/android/gms/internal/fp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->d:Lcom/google/android/gms/internal/fp;

    invoke-interface {v0}, Lcom/google/android/gms/internal/fp;->r()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/du;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget v0, v0, Lcom/google/android/gms/internal/dw;->k:I

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->g:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->i:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->e:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iput-object v2, p0, Lcom/google/android/gms/internal/du;->g:Landroid/widget/FrameLayout;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->h:Landroid/webkit/WebChromeClient$CustomViewCallback;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->h:Landroid/webkit/WebChromeClient$CustomViewCallback;

    invoke-interface {v0}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    iput-object v2, p0, Lcom/google/android/gms/internal/du;->h:Landroid/webkit/WebChromeClient$CustomViewCallback;

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->f:Z

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "com.google.android.gms.ads.internal.overlay.hasResumed"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->m:Z

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/dw;->a(Landroid/content/Intent;)Lcom/google/android/gms/internal/dw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/internal/fk;

    const-string v1, "Could not get info for ad overlay."

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/fk;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/android/gms/internal/fk; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fk;->getMessage()Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->q:Lcom/google/android/gms/internal/ad;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->q:Lcom/google/android/gms/internal/ad;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/ad;->b:Z

    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->o:Z

    :goto_1
    if-nez p1, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->d:Lcom/google/android/gms/internal/fp;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->d:Lcom/google/android/gms/internal/fp;

    invoke-interface {v0}, Lcom/google/android/gms/internal/fp;->s()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget v0, v0, Lcom/google/android/gms/internal/dw;->l:I

    if-eq v0, v2, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->c:Lcom/google/android/gms/internal/sq;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->c:Lcom/google/android/gms/internal/sq;

    invoke-interface {v0}, Lcom/google/android/gms/internal/sq;->u()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget v0, v0, Lcom/google/android/gms/internal/dw;->l:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lcom/google/android/gms/internal/fk;

    const-string v1, "Could not determine ad overlay type."

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/fk;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->o:Z

    goto :goto_1

    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/du;->b(Z)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/gms/internal/fl;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v1, v1, Lcom/google/android/gms/internal/dw;->e:Lcom/google/android/gms/internal/gz;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/fl;-><init>(Lcom/google/android/gms/internal/gz;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/du;->l:Lcom/google/android/gms/internal/fl;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/du;->b(Z)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/du;->b(Z)V

    goto :goto_0

    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/du;->m:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v1, v1, Lcom/google/android/gms/internal/dw;->b:Lcom/google/android/gms/internal/dt;

    iget-object v2, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-object v2, v2, Lcom/google/android/gms/internal/dw;->j:Lcom/google/android/gms/internal/ft;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/fh;->a(Landroid/content/Context;Lcom/google/android/gms/internal/dt;Lcom/google/android/gms/internal/ft;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_1
    .catch Lcom/google/android/gms/internal/fk; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 4

    const/4 v3, -0x2

    if-eqz p1, :cond_0

    const/16 v0, 0x32

    :goto_0
    new-instance v1, Lcom/google/android/gms/internal/dz;

    iget-object v2, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/internal/dz;-><init>(Landroid/app/Activity;I)V

    iput-object v1, p0, Lcom/google/android/gms/internal/du;->d:Lcom/google/android/gms/internal/dz;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    if-eqz p1, :cond_1

    const/16 v0, 0xb

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->d:Lcom/google/android/gms/internal/dz;

    iget-object v2, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget-boolean v2, v2, Lcom/google/android/gms/internal/dw;->h:Z

    iget-object v3, v0, Lcom/google/android/gms/internal/dz;->a:Landroid/widget/ImageButton;

    if-eqz v2, :cond_2

    const/4 v0, 0x4

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->i:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/google/android/gms/internal/du;->d:Lcom/google/android/gms/internal/dz;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    const/16 v0, 0x20

    goto :goto_0

    :cond_1
    const/16 v0, 0x9

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->k:Lcom/google/android/gms/internal/dw;

    iget v0, v0, Lcom/google/android/gms/internal/dw;->l:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/du;->m:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    invoke-static {v0}, Lcom/google/android/gms/internal/kf;->b(Landroid/webkit/WebView;)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->m:Z

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "com.google.android.gms.ads.internal.overlay.hasResumed"

    iget-boolean v1, p0, Lcom/google/android/gms/internal/du;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->b:Lcom/google/android/gms/internal/dy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->b:Lcom/google/android/gms/internal/dy;

    iget-object v0, v0, Lcom/google/android/gms/internal/dy;->d:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/du;->a()V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->l:Lcom/google/android/gms/internal/fl;

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    invoke-static {v0}, Lcom/google/android/gms/internal/kf;->a(Landroid/webkit/WebView;)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/internal/du;->g()V

    return-void
.end method

.method public final d()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/du;->g()V

    return-void
.end method

.method public final e()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->b:Lcom/google/android/gms/internal/dy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->b:Lcom/google/android/gms/internal/dy;

    iget-object v1, v0, Lcom/google/android/gms/internal/dy;->c:Lcom/google/android/gms/internal/fq;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/gms/internal/fq;->b:Z

    sget-object v2, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    iget-object v1, v1, Lcom/google/android/gms/internal/fq;->a:Ljava/lang/Runnable;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, v0, Lcom/google/android/gms/internal/dy;->d:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->i:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->c:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/internal/du;->g()V

    return-void
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->e:Z

    return-void
.end method
