.class public final Lcom/google/android/gms/internal/di;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/q;
.implements Lcom/google/android/gms/common/api/r;


# instance fields
.field private final a:Lcom/google/android/gms/internal/dj;

.field private final b:Lcom/google/android/gms/internal/dk;

.field private final c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/dj;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/internal/di;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/dj;Z)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/dj;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/di;->c:Ljava/lang/Object;

    iput-object p2, p0, Lcom/google/android/gms/internal/di;->a:Lcom/google/android/gms/internal/dj;

    new-instance v0, Lcom/google/android/gms/internal/dk;

    const v1, 0x645b68

    invoke-direct {v0, p1, p0, p0, v1}, Lcom/google/android/gms/internal/dk;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/q;Lcom/google/android/gms/common/api/r;I)V

    iput-object v0, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/dk;->a()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    invoke-static {}, Lcom/google/android/gms/internal/by;->a()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/di;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/dk;->c()Lcom/google/android/gms/internal/dl;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Lcom/google/android/gms/internal/dl;->a()Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/dk;->l()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/dk;->m()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/dk;->b()V

    :cond_2
    :goto_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lcom/google/android/gms/internal/di;->a:Lcom/google/android/gms/internal/dj;

    invoke-interface {v1, v0}, Lcom/google/android/gms/internal/dj;->a(Landroid/os/Bundle;)V

    return-void

    :catch_0
    move-exception v2

    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/dk;->l()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/dk;->m()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/dk;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_1
    move-exception v2

    :try_start_3
    iget-object v2, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/dk;->l()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/dk;->m()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/dk;->b()V

    goto :goto_0

    :catchall_1
    move-exception v0

    iget-object v2, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/dk;->l()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/dk;->m()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/internal/di;->b:Lcom/google/android/gms/internal/dk;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/dk;->b()V

    :cond_6
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/di;->a:Lcom/google/android/gms/internal/dj;

    invoke-static {}, Lcom/google/android/gms/internal/by;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/dj;->a(Landroid/os/Bundle;)V

    return-void
.end method
