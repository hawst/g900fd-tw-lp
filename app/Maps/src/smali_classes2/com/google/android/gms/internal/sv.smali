.class Lcom/google/android/gms/internal/sv;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field public final a:Lcom/google/android/gms/internal/z$a;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/content/Context;

.field public final d:Lcom/google/android/gms/internal/ne;

.field public final e:Lcom/google/android/gms/internal/gx;

.field public f:Lcom/google/android/gms/internal/bc;

.field public g:Lcom/google/android/gms/internal/jy;

.field public h:Lcom/google/android/gms/internal/jy;

.field public i:Lcom/google/android/gms/internal/bd;

.field public j:Lcom/google/android/gms/internal/jp;

.field public k:Lcom/google/android/gms/internal/jq;

.field public l:Lcom/google/android/gms/internal/jr;

.field public m:Lcom/google/android/gms/internal/bn;

.field public n:Lcom/google/android/gms/internal/gv;

.field public o:Lcom/google/android/gms/internal/gn;

.field public p:Lcom/google/android/gms/internal/cd;

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/google/android/gms/internal/gi;

.field public s:Lcom/google/android/gms/internal/jw;

.field public t:Landroid/view/View;

.field public u:I

.field public v:Z

.field public w:Z

.field x:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/gms/internal/jr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/bd;Ljava/lang/String;Lcom/google/android/gms/internal/gx;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/internal/sv;->s:Lcom/google/android/gms/internal/jw;

    iput-object v1, p0, Lcom/google/android/gms/internal/sv;->t:Landroid/view/View;

    iput v0, p0, Lcom/google/android/gms/internal/sv;->u:I

    iput-boolean v0, p0, Lcom/google/android/gms/internal/sv;->v:Z

    iput-boolean v0, p0, Lcom/google/android/gms/internal/sv;->w:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/sv;->x:Ljava/util/HashSet;

    iget-boolean v0, p2, Lcom/google/android/gms/internal/bd;->e:Z

    if-eqz v0, :cond_0

    iput-object v1, p0, Lcom/google/android/gms/internal/sv;->a:Lcom/google/android/gms/internal/z$a;

    :goto_0
    iput-object p2, p0, Lcom/google/android/gms/internal/sv;->i:Lcom/google/android/gms/internal/bd;

    iput-object p3, p0, Lcom/google/android/gms/internal/sv;->b:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/internal/sv;->c:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/gms/internal/sv;->e:Lcom/google/android/gms/internal/gx;

    new-instance v0, Lcom/google/android/gms/internal/ne;

    new-instance v1, Lcom/google/android/gms/internal/e;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/e;-><init>(Lcom/google/android/gms/internal/sv;)V

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ne;-><init>(Lcom/google/android/gms/internal/jk;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/sv;->d:Lcom/google/android/gms/internal/ne;

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/gms/internal/z$a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/internal/z$a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/sv;->a:Lcom/google/android/gms/internal/z$a;

    iget-object v0, p0, Lcom/google/android/gms/internal/sv;->a:Lcom/google/android/gms/internal/z$a;

    iget v1, p2, Lcom/google/android/gms/internal/bd;->g:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/z$a;->setMinimumWidth(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/sv;->a:Lcom/google/android/gms/internal/z$a;

    iget v1, p2, Lcom/google/android/gms/internal/bd;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/z$a;->setMinimumHeight(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/sv;->a:Lcom/google/android/gms/internal/z$a;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/z$a;->setVisibility(I)V

    goto :goto_0
.end method
