.class final Lcom/google/android/gms/internal/jb;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/google/android/gms/internal/fm;

.field final synthetic c:Lcom/google/android/gms/internal/jf;

.field final synthetic d:Lcom/google/android/gms/internal/kz;

.field final synthetic e:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/fm;Lcom/google/android/gms/internal/jf;Lcom/google/android/gms/internal/kz;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/jb;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/internal/jb;->b:Lcom/google/android/gms/internal/fm;

    iput-object p3, p0, Lcom/google/android/gms/internal/jb;->c:Lcom/google/android/gms/internal/jf;

    iput-object p4, p0, Lcom/google/android/gms/internal/jb;->d:Lcom/google/android/gms/internal/kz;

    iput-object p5, p0, Lcom/google/android/gms/internal/jb;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/jb;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/internal/bd;

    invoke-direct {v1}, Lcom/google/android/gms/internal/bd;-><init>()V

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/google/android/gms/internal/jb;->b:Lcom/google/android/gms/internal/fm;

    iget-object v5, v3, Lcom/google/android/gms/internal/fm;->k:Lcom/google/android/gms/internal/gx;

    move v3, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/gz;->a(Landroid/content/Context;Lcom/google/android/gms/internal/bd;ZZLcom/google/android/gms/internal/ne;Lcom/google/android/gms/internal/gx;)Lcom/google/android/gms/internal/gz;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gz;->setWillNotDraw(Z)V

    iget-object v1, p0, Lcom/google/android/gms/internal/jb;->c:Lcom/google/android/gms/internal/jf;

    const-string v2, "setAdWebView must be called on the main thread."

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    if-eq v3, v4, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object v0, v1, Lcom/google/android/gms/internal/jf;->d:Lcom/google/android/gms/internal/gz;

    iget-object v1, v0, Lcom/google/android/gms/internal/gz;->a:Lcom/google/android/gms/internal/kx;

    const-string v2, "/invalidRequest"

    iget-object v3, p0, Lcom/google/android/gms/internal/jb;->c:Lcom/google/android/gms/internal/jf;

    iget-object v3, v3, Lcom/google/android/gms/internal/jf;->e:Lcom/google/android/gms/internal/da;

    iget-object v4, v1, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v4, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "/loadAdURL"

    iget-object v3, p0, Lcom/google/android/gms/internal/jb;->c:Lcom/google/android/gms/internal/jf;

    iget-object v3, v3, Lcom/google/android/gms/internal/jf;->f:Lcom/google/android/gms/internal/da;

    iget-object v4, v1, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v4, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "/log"

    sget-object v3, Lcom/google/android/gms/internal/cq;->h:Lcom/google/android/gms/internal/da;

    iget-object v4, v1, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v4, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/internal/jb;->d:Lcom/google/android/gms/internal/kz;

    iput-object v2, v1, Lcom/google/android/gms/internal/kx;->f:Lcom/google/android/gms/internal/kz;

    iget-object v1, p0, Lcom/google/android/gms/internal/jb;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gz;->loadUrl(Ljava/lang/String;)V

    return-void
.end method
