.class Lcom/google/android/gms/internal/sg;
.super Ljava/lang/Object;


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/internal/so;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/sg;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method final a()I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/internal/sg;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/so;

    iget v3, v0, Lcom/google/android/gms/internal/so;->a:I

    invoke-static {v3}, Lcom/google/android/gms/internal/sc;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    iget-object v0, v0, Lcom/google/android/gms/internal/so;->b:[B

    array-length v0, v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    return v1
.end method

.method final a(Lcom/google/android/gms/internal/sc;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/sg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/so;

    iget v2, v0, Lcom/google/android/gms/internal/so;->a:I

    invoke-virtual {p1, v2}, Lcom/google/android/gms/internal/sc;->b(I)V

    iget-object v0, v0, Lcom/google/android/gms/internal/so;->b:[B

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/sc;->b([B)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/internal/sg;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/gms/internal/sg;

    iget-object v0, p0, Lcom/google/android/gms/internal/sg;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/gms/internal/sg;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/sg;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/google/android/gms/internal/sg;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/sg;->a()I

    move-result v0

    new-array v0, v0, [B

    const/4 v1, 0x0

    array-length v2, v0

    new-instance v3, Lcom/google/android/gms/internal/sc;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/gms/internal/sc;-><init>([BII)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/internal/sg;->a(Lcom/google/android/gms/internal/sc;)V

    invoke-virtual {p1}, Lcom/google/android/gms/internal/sg;->a()I

    move-result v1

    new-array v1, v1, [B

    const/4 v2, 0x0

    array-length v3, v1

    new-instance v4, Lcom/google/android/gms/internal/sc;

    invoke-direct {v4, v1, v2, v3}, Lcom/google/android/gms/internal/sc;-><init>([BII)V

    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/sg;->a(Lcom/google/android/gms/internal/sc;)V

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public hashCode()I
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/sg;->a()I

    move-result v0

    new-array v0, v0, [B

    const/4 v1, 0x0

    array-length v2, v0

    new-instance v3, Lcom/google/android/gms/internal/sc;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/gms/internal/sc;-><init>([BII)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/internal/sg;->a(Lcom/google/android/gms/internal/sc;)V

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
