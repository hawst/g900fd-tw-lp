.class public final Lcom/google/android/gms/internal/by;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# static fields
.field public static a:Lcom/google/android/gms/internal/my;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/my",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static b:Lcom/google/android/gms/internal/my;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/my",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static c:Lcom/google/android/gms/internal/my;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/my",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static d:Lcom/google/android/gms/internal/my;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/my",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static e:Lcom/google/android/gms/internal/my;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/my",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static f:Lcom/google/android/gms/internal/my;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/my",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/16 v6, 0xa

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    const-string v0, "gads:sdk_core_location"

    const-string v1, "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/sdk-core-v40.html"

    sget-object v2, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    invoke-virtual {v2, v0, v1}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/my;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/my;

    const-string v0, "gads:sdk_core_experiment_id"

    sget-object v1, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v4}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v4}, Lcom/google/android/gms/internal/my;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/my;

    const-string v0, "gads:sdk_crash_report_enabled"

    sget-object v1, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/my;->a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/my;

    const-string v0, "gads:sdk_crash_report_full_stacktrace"

    sget-object v1, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/my;->a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/my;

    const-string v0, "gads:block_autoclicks"

    sget-object v1, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/my;->a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/my;

    const-string v0, "gads:block_autoclicks_experiment_id"

    sget-object v1, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v4}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v4}, Lcom/google/android/gms/internal/my;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/my;

    const-string v0, "gads:enable_content_fetching"

    sget-object v1, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/my;->a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/my;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/by;->a:Lcom/google/android/gms/internal/my;

    const-string v0, "gads:content_length_weight"

    sget-object v1, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v5}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/my;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/internal/my;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/by;->b:Lcom/google/android/gms/internal/my;

    const-string v0, "gads:content_age_weight"

    sget-object v1, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v5}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/my;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/internal/my;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/by;->c:Lcom/google/android/gms/internal/my;

    const-string v0, "gads:min_content_len"

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    invoke-virtual {v2, v0, v1}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/my;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/internal/my;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/by;->d:Lcom/google/android/gms/internal/my;

    const-string v0, "gads:fingerprint_number"

    sget-object v1, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v6}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/my;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/internal/my;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/by;->e:Lcom/google/android/gms/internal/my;

    const-string v0, "gads:sleep_sec"

    sget-object v1, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v6}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/my;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/internal/my;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/by;->f:Lcom/google/android/gms/internal/my;

    const-string v0, "gads:spam_app_context:enabled"

    sget-object v1, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/my;->a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/my;

    const-string v0, "gads:spam_app_context:experiment_id"

    sget-object v1, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v4}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v4}, Lcom/google/android/gms/internal/my;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/my;

    return-void
.end method

.method public static a()Landroid/os/Bundle;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/by;->g:Landroid/os/Bundle;

    return-object v0
.end method
