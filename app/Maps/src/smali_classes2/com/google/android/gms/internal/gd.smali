.class public final Lcom/google/android/gms/internal/gd;
.super Lcom/google/android/gms/internal/gt;

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field private a:Z

.field private b:Landroid/content/Context;

.field private c:I

.field private d:Landroid/content/Intent;

.field private e:Lcom/google/android/gms/internal/fx;

.field private f:Lcom/google/android/gms/internal/gc;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZILandroid/content/Intent;Lcom/google/android/gms/internal/gc;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/gt;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/gd;->a:Z

    iput-object p2, p0, Lcom/google/android/gms/internal/gd;->g:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/gms/internal/gd;->c:I

    iput-object p5, p0, Lcom/google/android/gms/internal/gd;->d:Landroid/content/Intent;

    iput-boolean p3, p0, Lcom/google/android/gms/internal/gd;->a:Z

    iput-object p1, p0, Lcom/google/android/gms/internal/gd;->b:Landroid/content/Context;

    iput-object p6, p0, Lcom/google/android/gms/internal/gd;->f:Lcom/google/android/gms/internal/gc;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/gd;->a:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/gd;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/gd;->d:Landroid/content/Intent;

    return-object v0
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/gd;->c:I

    return v0
.end method

.method public final e()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/gd;->d:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/android/gms/internal/gg;->a(Landroid/content/Intent;)I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/internal/gd;->c:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/gms/internal/fx;

    iget-object v1, p0, Lcom/google/android/gms/internal/gd;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/fx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/gd;->e:Lcom/google/android/gms/internal/fx;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/internal/gd;->b:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/gd;->e:Lcom/google/android/gms/internal/fx;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/internal/fx;->a(Landroid/os/IBinder;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/gd;->d:Landroid/content/Intent;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/internal/gg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    const-string v2, "INAPP_PURCHASE_DATA"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/internal/gd;->e:Lcom/google/android/gms/internal/fx;

    iget-object v3, p0, Lcom/google/android/gms/internal/gd;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/internal/fx;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/gd;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/ge;->a(Landroid/content/Context;)Lcom/google/android/gms/internal/ge;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/internal/gd;->f:Lcom/google/android/gms/internal/gc;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/ge;->a(Lcom/google/android/gms/internal/gc;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/gd;->b:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/gd;->e:Lcom/google/android/gms/internal/fx;

    iput-object v1, v0, Lcom/google/android/gms/internal/fx;->a:Ljava/lang/Object;

    goto :goto_1
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/gd;->e:Lcom/google/android/gms/internal/fx;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/internal/fx;->a:Ljava/lang/Object;

    return-void
.end method
