.class public Lcom/google/android/gms/car/a/j;
.super Lcom/google/android/gms/car/a/h;


# instance fields
.field private final a:Landroid/view/inputmethod/InputConnection;

.field private final b:Lcom/google/android/gms/car/a/a;

.field private volatile c:Z


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputConnection;Lcom/google/android/gms/car/a/a;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/car/a/h;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/car/a/j;->a:Landroid/view/inputmethod/InputConnection;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/a/j;->c:Z

    iput-object p2, p0, Lcom/google/android/gms/car/a/j;->b:Lcom/google/android/gms/car/a/a;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/a/j;)Landroid/view/inputmethod/InputConnection;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/a/j;->a:Landroid/view/inputmethod/InputConnection;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/a/j;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    return p1
.end method


# virtual methods
.method public final a(I)Ljava/lang/CharSequence;
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/o;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/car/a/o;-><init>(Lcom/google/android/gms/car/a/j;I)V

    const-wide/16 v2, 0x3e8

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/car/eo;->a(Ljava/util/concurrent/Callable;Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final a(II)Ljava/lang/CharSequence;
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/k;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/car/a/k;-><init>(Lcom/google/android/gms/car/a/j;II)V

    const-wide/16 v2, 0x3e8

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/car/eo;->a(Ljava/util/concurrent/Callable;Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/a/d;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/a/j;->b:Lcom/google/android/gms/car/a/a;

    new-instance v1, Lcom/google/android/gms/car/a/ad;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/car/a/ad;-><init>(Lcom/google/android/gms/car/a/j;Lcom/google/android/gms/car/a/d;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/a/a;->setCarEditableListener(Lcom/google/android/gms/car/a/b;)V

    return-void
.end method

.method public final a()Z
    .locals 3

    new-instance v0, Lcom/google/android/gms/car/a/t;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/a/t;-><init>(Lcom/google/android/gms/car/a/j;)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/a/m;-><init>(Lcom/google/android/gms/car/a/j;Ljava/util/concurrent/Callable;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/view/KeyEvent;)Z
    .locals 3

    new-instance v0, Lcom/google/android/gms/car/a/aa;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/a/aa;-><init>(Lcom/google/android/gms/car/a/j;Landroid/view/KeyEvent;)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/a/m;-><init>(Lcom/google/android/gms/car/a/j;Ljava/util/concurrent/Callable;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;I)Z
    .locals 3

    new-instance v0, Lcom/google/android/gms/car/a/r;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/car/a/r;-><init>(Lcom/google/android/gms/car/a/j;Ljava/lang/CharSequence;I)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/a/m;-><init>(Lcom/google/android/gms/car/a/j;Ljava/util/concurrent/Callable;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 3

    new-instance v0, Lcom/google/android/gms/car/a/l;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/car/a/l;-><init>(Lcom/google/android/gms/car/a/j;Ljava/lang/String;Landroid/os/Bundle;)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/a/m;-><init>(Lcom/google/android/gms/car/a/j;Ljava/util/concurrent/Callable;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Z)Z
    .locals 3

    new-instance v0, Lcom/google/android/gms/car/a/ac;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/a/ac;-><init>(Lcom/google/android/gms/car/a/j;Z)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/a/m;-><init>(Lcom/google/android/gms/car/a/j;Ljava/util/concurrent/Callable;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(I)I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/p;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/car/a/p;-><init>(Lcom/google/android/gms/car/a/j;I)V

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/car/eo;->a(Ljava/util/concurrent/Callable;Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final b(II)Ljava/lang/CharSequence;
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/n;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/car/a/n;-><init>(Lcom/google/android/gms/car/a/j;II)V

    const-wide/16 v2, 0x3e8

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/car/eo;->a(Ljava/util/concurrent/Callable;Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/a/j;->b:Lcom/google/android/gms/car/a/a;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/a/a;->setInputEnabled(Z)V

    return-void
.end method

.method public final b()Z
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/car/a/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/a/y;-><init>(Lcom/google/android/gms/car/a/j;)V

    const/4 v1, 0x0

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/car/eo;->a(Ljava/util/concurrent/Callable;Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;I)Z
    .locals 3

    new-instance v0, Lcom/google/android/gms/car/a/u;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/car/a/u;-><init>(Lcom/google/android/gms/car/a/j;Ljava/lang/CharSequence;I)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/a/m;-><init>(Lcom/google/android/gms/car/a/j;Ljava/util/concurrent/Callable;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()Z
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/car/a/z;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/a/z;-><init>(Lcom/google/android/gms/car/a/j;)V

    const/4 v1, 0x0

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/car/eo;->a(Ljava/util/concurrent/Callable;Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public final c(I)Z
    .locals 3

    new-instance v0, Lcom/google/android/gms/car/a/w;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/a/w;-><init>(Lcom/google/android/gms/car/a/j;I)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/a/m;-><init>(Lcom/google/android/gms/car/a/j;Ljava/util/concurrent/Callable;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(II)Z
    .locals 3

    new-instance v0, Lcom/google/android/gms/car/a/q;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/car/a/q;-><init>(Lcom/google/android/gms/car/a/j;II)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/a/m;-><init>(Lcom/google/android/gms/car/a/j;Ljava/util/concurrent/Callable;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d(I)Z
    .locals 3

    new-instance v0, Lcom/google/android/gms/car/a/x;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/a/x;-><init>(Lcom/google/android/gms/car/a/j;I)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/a/m;-><init>(Lcom/google/android/gms/car/a/j;Ljava/util/concurrent/Callable;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d(II)Z
    .locals 3

    new-instance v0, Lcom/google/android/gms/car/a/s;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/car/a/s;-><init>(Lcom/google/android/gms/car/a/j;II)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/a/m;-><init>(Lcom/google/android/gms/car/a/j;Ljava/util/concurrent/Callable;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final e(I)Z
    .locals 3

    new-instance v0, Lcom/google/android/gms/car/a/ab;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/a/ab;-><init>(Lcom/google/android/gms/car/a/j;I)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/a/m;-><init>(Lcom/google/android/gms/car/a/j;Ljava/util/concurrent/Callable;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final e(II)Z
    .locals 3

    new-instance v0, Lcom/google/android/gms/car/a/v;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/car/a/v;-><init>(Lcom/google/android/gms/car/a/j;II)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/a/j;->c:Z

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/car/a/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/a/m;-><init>(Lcom/google/android/gms/car/a/j;Ljava/util/concurrent/Callable;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method
