.class public final Lcom/google/android/gms/internal/jp;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field public final a:Lcom/google/android/gms/internal/ba;

.field public final b:Lcom/google/android/gms/internal/gz;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:I

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:I

.field public final h:J

.field public final i:Ljava/lang/String;

.field public final j:Lorg/json/JSONObject;

.field public final k:Z

.field public final l:Lcom/google/android/gms/internal/ds;

.field public final m:Lcom/google/android/gms/internal/el;

.field public final n:Ljava/lang/String;

.field public final o:Lcom/google/android/gms/internal/dv;

.field public final p:Lcom/google/android/gms/internal/ea;

.field public final q:Lcom/google/android/gms/internal/bd;

.field public final r:J

.field public final s:J

.field public final t:Ljava/lang/String;

.field public final u:Lcom/google/android/gms/internal/cj;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/ba;Lcom/google/android/gms/internal/gz;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLcom/google/android/gms/internal/ds;Lcom/google/android/gms/internal/el;Ljava/lang/String;Lcom/google/android/gms/internal/dv;Lcom/google/android/gms/internal/ea;JLcom/google/android/gms/internal/bd;JJJLjava/lang/String;Lorg/json/JSONObject;Lcom/google/android/gms/internal/cj;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/ba;",
            "Lcom/google/android/gms/internal/gz;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IJ",
            "Ljava/lang/String;",
            "Z",
            "Lcom/google/android/gms/internal/ds;",
            "Lcom/google/android/gms/internal/el;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/internal/dv;",
            "Lcom/google/android/gms/internal/ea;",
            "J",
            "Lcom/google/android/gms/internal/bd;",
            "JJJ",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            "Lcom/google/android/gms/internal/cj;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/jp;->a:Lcom/google/android/gms/internal/ba;

    iput-object p2, p0, Lcom/google/android/gms/internal/jp;->b:Lcom/google/android/gms/internal/gz;

    if-eqz p3, :cond_0

    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lcom/google/android/gms/internal/jp;->c:Ljava/util/List;

    iput p4, p0, Lcom/google/android/gms/internal/jp;->d:I

    if-eqz p5, :cond_1

    invoke-static {p5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_1
    iput-object v2, p0, Lcom/google/android/gms/internal/jp;->e:Ljava/util/List;

    if-eqz p6, :cond_2

    invoke-static {p6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_2
    iput-object v2, p0, Lcom/google/android/gms/internal/jp;->f:Ljava/util/List;

    iput p7, p0, Lcom/google/android/gms/internal/jp;->g:I

    iput-wide p8, p0, Lcom/google/android/gms/internal/jp;->h:J

    iput-object p10, p0, Lcom/google/android/gms/internal/jp;->i:Ljava/lang/String;

    iput-boolean p11, p0, Lcom/google/android/gms/internal/jp;->k:Z

    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/gms/internal/jp;->l:Lcom/google/android/gms/internal/ds;

    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/gms/internal/jp;->m:Lcom/google/android/gms/internal/el;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/internal/jp;->n:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/internal/jp;->o:Lcom/google/android/gms/internal/dv;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/internal/jp;->p:Lcom/google/android/gms/internal/ea;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/internal/jp;->q:Lcom/google/android/gms/internal/bd;

    move-wide/from16 v0, p22

    iput-wide v0, p0, Lcom/google/android/gms/internal/jp;->r:J

    move-wide/from16 v0, p24

    iput-wide v0, p0, Lcom/google/android/gms/internal/jp;->s:J

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/android/gms/internal/jp;->t:Ljava/lang/String;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/android/gms/internal/jp;->j:Lorg/json/JSONObject;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/google/android/gms/internal/jp;->u:Lcom/google/android/gms/internal/cj;

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/jq;Lcom/google/android/gms/internal/gz;Lcom/google/android/gms/internal/ds;Lcom/google/android/gms/internal/el;Ljava/lang/String;Lcom/google/android/gms/internal/ea;Lcom/google/android/gms/internal/cj;)V
    .locals 31

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/jq;->a:Lcom/google/android/gms/internal/fm;

    iget-object v3, v2, Lcom/google/android/gms/internal/fm;->c:Lcom/google/android/gms/internal/ba;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget-object v5, v2, Lcom/google/android/gms/internal/fo;->d:Ljava/util/List;

    move-object/from16 v0, p1

    iget v6, v0, Lcom/google/android/gms/internal/jq;->e:I

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget-object v7, v2, Lcom/google/android/gms/internal/fo;->f:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget-object v8, v2, Lcom/google/android/gms/internal/fo;->j:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget v9, v2, Lcom/google/android/gms/internal/fo;->l:I

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget-wide v10, v2, Lcom/google/android/gms/internal/fo;->k:J

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/jq;->a:Lcom/google/android/gms/internal/fm;

    iget-object v12, v2, Lcom/google/android/gms/internal/fm;->i:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget-boolean v13, v2, Lcom/google/android/gms/internal/fo;->h:Z

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/internal/jq;->c:Lcom/google/android/gms/internal/dv;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget-wide v0, v2, Lcom/google/android/gms/internal/fo;->i:J

    move-wide/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/internal/jq;->d:Lcom/google/android/gms/internal/bd;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget-wide v0, v2, Lcom/google/android/gms/internal/fo;->g:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/google/android/gms/internal/jq;->f:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/google/android/gms/internal/jq;->g:J

    move-wide/from16 v26, v0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget-object v0, v2, Lcom/google/android/gms/internal/fo;->o:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/internal/jq;->h:Lorg/json/JSONObject;

    move-object/from16 v29, v0

    move-object/from16 v2, p0

    move-object/from16 v4, p2

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move-object/from16 v16, p5

    move-object/from16 v18, p6

    move-object/from16 v30, p7

    invoke-direct/range {v2 .. v30}, Lcom/google/android/gms/internal/jp;-><init>(Lcom/google/android/gms/internal/ba;Lcom/google/android/gms/internal/gz;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLcom/google/android/gms/internal/ds;Lcom/google/android/gms/internal/el;Ljava/lang/String;Lcom/google/android/gms/internal/dv;Lcom/google/android/gms/internal/ea;JLcom/google/android/gms/internal/bd;JJJLjava/lang/String;Lorg/json/JSONObject;Lcom/google/android/gms/internal/cj;)V

    return-void
.end method
