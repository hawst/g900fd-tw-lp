.class public abstract Lcom/google/android/gms/smart_profile/f;
.super Landroid/os/Binder;

# interfaces
.implements Lcom/google/android/gms/smart_profile/e;


# direct methods
.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/smart_profile/e;
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.smart_profile.IHeaderViewCreator"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/smart_profile/e;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/smart_profile/e;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/smart_profile/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/smart_profile/g;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2

    const/4 v1, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    const-string v0, "com.google.android.gms.smart_profile.IHeaderViewCreator"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v0, "com.google.android.gms.smart_profile.IHeaderViewCreator"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/a/p;->a(Landroid/os/IBinder;)Lcom/google/android/gms/a/o;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/f;->a(Lcom/google/android/gms/a/o;)Lcom/google/android/gms/smart_profile/h;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/smart_profile/h;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
