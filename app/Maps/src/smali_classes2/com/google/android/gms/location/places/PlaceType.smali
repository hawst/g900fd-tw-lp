.class public final Lcom/google/android/gms/location/places/PlaceType;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/location/places/f;


# instance fields
.field final a:I

.field final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "accounting"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "airport"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "amusement_park"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "aquarium"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "art_gallery"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "atm"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "bakery"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "bank"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "bar"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "beauty_salon"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "bicycle_store"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "book_store"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "bowling_alley"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "bus_station"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "cafe"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "campground"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "car_dealer"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "car_rental"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "car_repair"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "car_wash"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "casino"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "cemetery"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "church"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "city_hall"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "clothing_store"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "convenience_store"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "courthouse"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "dentist"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "department_store"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "doctor"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "electrician"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "electronics_store"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "embassy"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "establishment"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "finance"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "fire_station"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "florist"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "food"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "funeral_home"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "furniture_store"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "gas_station"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "general_contractor"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "grocery_or_supermarket"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "gym"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "hair_care"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "hardware_store"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "health"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "hindu_temple"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "home_goods_store"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "hospital"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "insurance_agency"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "jewelry_store"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "laundry"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "lawyer"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "library"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "liquor_store"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "local_government_office"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "locksmith"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "lodging"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "meal_delivery"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "meal_takeaway"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "mosque"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "movie_rental"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "movie_theater"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "moving_company"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "museum"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "night_club"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "painter"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "park"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "parking"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "pet_store"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "pharmacy"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "physiotherapist"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "place_of_worship"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "plumber"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "police"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "post_office"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "real_estate_agency"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "restaurant"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "roofing_contractor"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "rv_park"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "school"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "shoe_store"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "shopping_mall"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "spa"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "stadium"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "storage"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "store"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "subway_station"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "synagogue"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "taxi_stand"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "train_station"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "travel_agency"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "university"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "veterinary_care"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "zoo"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "administrative_area_level_1"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "administrative_area_level_2"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "administrative_area_level_3"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "colloquial_area"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "country"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "floor"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "geocode"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "intersection"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "locality"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "natural_feature"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "neighborhood"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "political"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "point_of_interest"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "post_box"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "postal_code"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "postal_code_prefix"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "postal_town"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "premise"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "room"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "route"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "street_address"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "sublocality"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "sublocality_level_1"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "sublocality_level_2"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "sublocality_level_3"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "sublocality_level_4"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "sublocality_level_5"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "subpremise"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "transit_station"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    const-string v0, "other"

    new-instance v1, Lcom/google/android/gms/location/places/PlaceType;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/location/places/f;

    invoke-direct {v0}, Lcom/google/android/gms/location/places/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->CREATOR:Lcom/google/android/gms/location/places/f;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Given String is empty or null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput p1, p0, Lcom/google/android/gms/location/places/PlaceType;->a:I

    iput-object p2, p0, Lcom/google/android/gms/location/places/PlaceType;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/location/places/PlaceType;->CREATOR:Lcom/google/android/gms/location/places/f;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lcom/google/android/gms/location/places/PlaceType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/location/places/PlaceType;->b:Ljava/lang/String;

    check-cast p1, Lcom/google/android/gms/location/places/PlaceType;

    iget-object v1, p1, Lcom/google/android/gms/location/places/PlaceType;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/PlaceType;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/PlaceType;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/location/places/PlaceType;->CREATOR:Lcom/google/android/gms/location/places/f;

    invoke-static {p0, p1}, Lcom/google/android/gms/location/places/f;->a(Lcom/google/android/gms/location/places/PlaceType;Landroid/os/Parcel;)V

    return-void
.end method
