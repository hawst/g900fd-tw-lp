.class Lcom/google/android/gms/wallet/fragment/c;
.super Lcom/google/android/gms/a/b;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/a/b",
        "<",
        "Lcom/google/android/gms/wallet/fragment/b;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field final synthetic d:Lcom/google/android/gms/wallet/fragment/WalletFragment;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/fragment/WalletFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-direct {p0}, Lcom/google/android/gms/a/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/widget/FrameLayout;)V
    .locals 6

    const/4 v1, -0x1

    const/4 v0, -0x2

    new-instance v2, Landroid/widget/Button;

    iget-object v3, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v3}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->a(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Landroid/app/Fragment;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0c0024

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(I)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v3}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->e(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v3}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->e(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;->d:Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;

    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v4}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->a(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Landroid/app/Fragment;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    const-string v5, "buyButtonWidth"

    invoke-virtual {v3, v5, v4, v1}, Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;->a(Ljava/lang/String;Landroid/util/DisplayMetrics;I)I

    move-result v1

    const-string v5, "buyButtonHeight"

    invoke-virtual {v3, v5, v4, v0}, Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;->a(Ljava/lang/String;Landroid/util/DisplayMetrics;I)I

    move-result v0

    :cond_0
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v1, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method protected final a(Lcom/google/android/gms/a/s;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/a/s",
            "<",
            "Lcom/google/android/gms/wallet/fragment/b;",
            ">;)V"
        }
    .end annotation

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->a(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v1}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->b(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/fragment/b;

    move-result-object v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v1}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->c(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v1}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->d(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/a/k;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v2}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->e(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v3}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->f(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/fragment/a;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/internal/rs;->a(Landroid/app/Activity;Lcom/google/android/gms/a/l;Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;Lcom/google/android/gms/internal/rm;)Lcom/google/android/gms/internal/rj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    new-instance v2, Lcom/google/android/gms/wallet/fragment/b;

    invoke-direct {v2, v0}, Lcom/google/android/gms/wallet/fragment/b;-><init>(Lcom/google/android/gms/internal/rj;)V

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->a(Lcom/google/android/gms/wallet/fragment/WalletFragment;Lcom/google/android/gms/wallet/fragment/b;)Lcom/google/android/gms/wallet/fragment/b;

    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->a(Lcom/google/android/gms/wallet/fragment/WalletFragment;Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;)Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;
    :try_end_0
    .catch Lcom/google/android/gms/common/e; {:try_start_0 .. :try_end_0} :catch_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->b(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/fragment/b;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/gms/a/s;->a(Lcom/google/android/gms/a/a;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->g(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->b(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/fragment/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v1}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->g(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    move-result-object v1

    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/wallet/fragment/b;->a:Lcom/google/android/gms/internal/rj;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/rj;->a(Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->a(Lcom/google/android/gms/wallet/fragment/WalletFragment;Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;)Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->h(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/MaskedWalletRequest;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->b(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/fragment/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v1}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->h(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/MaskedWalletRequest;

    move-result-object v1

    :try_start_2
    iget-object v0, v0, Lcom/google/android/gms/wallet/fragment/b;->a:Lcom/google/android/gms/internal/rj;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/rj;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->a(Lcom/google/android/gms/wallet/fragment/WalletFragment;Lcom/google/android/gms/wallet/MaskedWalletRequest;)Lcom/google/android/gms/wallet/MaskedWalletRequest;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->i(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->b(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/fragment/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v1}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->i(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v1

    :try_start_3
    iget-object v0, v0, Lcom/google/android/gms/wallet/fragment/b;->a:Lcom/google/android/gms/internal/rj;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/rj;->a(Lcom/google/android/gms/wallet/MaskedWallet;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->a(Lcom/google/android/gms/wallet/fragment/WalletFragment;Lcom/google/android/gms/wallet/MaskedWallet;)Lcom/google/android/gms/wallet/MaskedWallet;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->j(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->b(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Lcom/google/android/gms/wallet/fragment/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v1}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->j(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :try_start_4
    iget-object v0, v0, Lcom/google/android/gms/wallet/fragment/b;->a:Lcom/google/android/gms/internal/rj;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/rj;->a(Z)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->a(Lcom/google/android/gms/wallet/fragment/WalletFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    :cond_3
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_4
    move-exception v0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/fragment/c;->d:Lcom/google/android/gms/wallet/fragment/WalletFragment;

    invoke-static {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragment;->a(Lcom/google/android/gms/wallet/fragment/WalletFragment;)Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/g;->a(Landroid/content/Context;)I

    move-result v1

    const/4 v2, -0x1

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/common/g;->a(ILandroid/app/Activity;I)Z

    return-void
.end method
