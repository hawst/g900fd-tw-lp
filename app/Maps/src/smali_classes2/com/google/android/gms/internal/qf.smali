.class final Lcom/google/android/gms/internal/qf;
.super Lcom/google/android/gms/internal/pp;


# instance fields
.field final synthetic a:Lcom/google/android/gms/internal/qb;

.field private final b:Lcom/google/android/gms/people/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/qb;Lcom/google/android/gms/people/m;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/qf;->a:Lcom/google/android/gms/internal/qb;

    invoke-direct {p0}, Lcom/google/android/gms/internal/pp;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/internal/qf;->b:Lcom/google/android/gms/people/m;

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 7

    const/4 v2, 0x3

    const-string v0, "PeopleService"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    const-string v0, "PeopleClient"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bundle callback: status="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nresolution="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nbundle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const-string v0, "PeopleService"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    if-eqz p1, :cond_3

    const-string v0, "PeopleClient"

    const-string v0, "Non-success data changed callback received."

    const/4 v0, 0x5

    const-string v1, "PeopleService"

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/google/android/gms/internal/qf;->a:Lcom/google/android/gms/internal/qb;

    new-instance v0, Lcom/google/android/gms/internal/qe;

    iget-object v1, p0, Lcom/google/android/gms/internal/qf;->a:Lcom/google/android/gms/internal/qb;

    iget-object v2, p0, Lcom/google/android/gms/internal/qf;->b:Lcom/google/android/gms/people/m;

    const-string v3, "account"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "pagegaiaid"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "scope"

    invoke-virtual {p3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/qe;-><init>(Lcom/google/android/gms/internal/qb;Lcom/google/android/gms/people/m;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/internal/qb;->b(Lcom/google/android/gms/common/internal/h;)V

    goto :goto_1
.end method
