.class Lcom/google/android/gms/common/api/aj;
.super Landroid/support/v4/a/a;

# interfaces
.implements Lcom/google/android/gms/common/api/q;
.implements Lcom/google/android/gms/common/api/r;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/a/a",
        "<",
        "Lcom/google/android/gms/common/a;",
        ">;",
        "Lcom/google/android/gms/common/api/q;",
        "Lcom/google/android/gms/common/api/r;"
    }
.end annotation


# instance fields
.field public final h:Lcom/google/android/gms/common/api/o;

.field i:Z

.field private j:Lcom/google/android/gms/common/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/o;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v4/a/a;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/gms/common/api/aj;->h:Lcom/google/android/gms/common/api/o;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/aj;->i:Z

    sget-object v0, Lcom/google/android/gms/common/a;->a:Lcom/google/android/gms/common/a;

    iput-object v0, p0, Lcom/google/android/gms/common/api/aj;->j:Lcom/google/android/gms/common/a;

    iget-boolean v1, p0, Landroid/support/v4/a/a;->c:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Landroid/support/v4/a/a;->d:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v4/a/a;->b:Landroid/support/v4/a/b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v4/a/a;->b:Landroid/support/v4/a/b;

    invoke-interface {v1, p0, v0}, Landroid/support/v4/a/b;->a(Landroid/support/v4/a/a;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/aj;->i:Z

    iput-object p1, p0, Lcom/google/android/gms/common/api/aj;->j:Lcom/google/android/gms/common/a;

    iget-boolean v0, p0, Landroid/support/v4/a/a;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v4/a/a;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/a/a;->b:Landroid/support/v4/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/a/a;->b:Landroid/support/v4/a/b;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/a/b;->a(Landroid/support/v4/a/a;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected final b()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/a/a;->b()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/aj;->h:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/q;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/aj;->h:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/r;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/aj;->j:Lcom/google/android/gms/common/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/aj;->j:Lcom/google/android/gms/common/a;

    iget-object v1, p0, Landroid/support/v4/a/a;->b:Landroid/support/v4/a/b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v4/a/a;->b:Landroid/support/v4/a/b;

    invoke-interface {v1, p0, v0}, Landroid/support/v4/a/b;->a(Landroid/support/v4/a/a;Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/aj;->h:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/api/aj;->h:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/aj;->i:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/api/aj;->h:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->b()V

    :cond_1
    return-void
.end method

.method protected final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/aj;->h:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->c()V

    return-void
.end method

.method protected final f()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/aj;->j:Lcom/google/android/gms/common/a;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/aj;->i:Z

    iget-object v0, p0, Lcom/google/android/gms/common/api/aj;->h:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/o;->b(Lcom/google/android/gms/common/api/q;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/aj;->h:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/o;->b(Lcom/google/android/gms/common/api/r;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/aj;->h:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->c()V

    return-void
.end method
