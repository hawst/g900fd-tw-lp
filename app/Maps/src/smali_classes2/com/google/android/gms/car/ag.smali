.class public Lcom/google/android/gms/car/ag;
.super Ljava/lang/Object;


# instance fields
.field final a:Lcom/google/android/gms/car/cx;

.field final b:Lcom/google/android/gms/car/aj;

.field volatile c:Lcom/google/android/gms/car/ai;

.field final d:Landroid/os/Handler;

.field private final e:Lcom/google/android/gms/car/eq;

.field private final f:Landroid/os/Handler$Callback;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/cx;Lcom/google/android/gms/car/eq;Landroid/os/Looper;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/car/ah;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ah;-><init>(Lcom/google/android/gms/car/ag;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ag;->f:Landroid/os/Handler$Callback;

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/ag;->f:Landroid/os/Handler$Callback;

    invoke-direct {v0, p3, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ag;->d:Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/gms/car/ag;->a:Lcom/google/android/gms/car/cx;

    new-instance v0, Lcom/google/android/gms/car/aj;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/aj;-><init>(Lcom/google/android/gms/car/ag;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ag;->b:Lcom/google/android/gms/car/aj;

    iput-object p2, p0, Lcom/google/android/gms/car/ag;->e:Lcom/google/android/gms/car/eq;

    return-void
.end method

.method static a(Landroid/os/RemoteException;)V
    .locals 2

    const-string v0, "CAR.MSG"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RemoteException from car service:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    new-instance v0, Lcom/google/android/gms/car/ao;

    invoke-direct {v0}, Lcom/google/android/gms/car/ao;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/ag;->c:Lcom/google/android/gms/car/ai;

    return-void
.end method

.method public final a(III)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/ag;->e:Lcom/google/android/gms/car/eq;

    invoke-virtual {v0}, Lcom/google/android/gms/car/eq;->i()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ag;->a:Lcom/google/android/gms/car/cx;

    iget-object v1, p0, Lcom/google/android/gms/car/ag;->b:Lcom/google/android/gms/car/aj;

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/google/android/gms/car/cx;->a(Lcom/google/android/gms/car/da;III)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/eq;->a(Ljava/lang/IllegalStateException;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/ag;->a(Landroid/os/RemoteException;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/ai;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/ag;->e:Lcom/google/android/gms/car/eq;

    invoke-virtual {v0}, Lcom/google/android/gms/car/eq;->i()V

    iput-object p1, p0, Lcom/google/android/gms/car/ag;->c:Lcom/google/android/gms/car/ai;

    return-void
.end method

.method public final a(I)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/ag;->e:Lcom/google/android/gms/car/eq;

    invoke-virtual {v0}, Lcom/google/android/gms/car/eq;->i()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ag;->a:Lcom/google/android/gms/car/cx;

    iget-object v1, p0, Lcom/google/android/gms/car/ag;->b:Lcom/google/android/gms/car/aj;

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/car/cx;->a(Lcom/google/android/gms/car/da;I)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/eq;->a(Ljava/lang/IllegalStateException;)V

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/ag;->a(Landroid/os/RemoteException;)V

    goto :goto_1
.end method

.method public final b(I)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ag;->a:Lcom/google/android/gms/car/cx;

    iget-object v1, p0, Lcom/google/android/gms/car/ag;->b:Lcom/google/android/gms/car/aj;

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/car/cx;->b(Lcom/google/android/gms/car/da;I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method
