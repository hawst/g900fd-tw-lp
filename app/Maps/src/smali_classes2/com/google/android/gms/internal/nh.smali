.class public Lcom/google/android/gms/internal/nh;
.super Lcom/google/android/gms/common/internal/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/e",
        "<",
        "Lcom/google/android/gms/internal/ni;",
        ">;"
    }
.end annotation


# instance fields
.field private g:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/q;Lcom/google/android/gms/common/api/r;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/e;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/q;Lcom/google/android/gms/common/api/r;[Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/gms/internal/nh;->g:Landroid/content/Context;

    return-void
.end method

.method private b(Lcom/google/android/gms/feedback/FeedbackOptions;)Lcom/google/android/gms/feedback/ErrorReport;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/nh;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v3}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    if-eqz p1, :cond_14

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->c:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->c:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->c:Landroid/os/Bundle;

    iput-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->b:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->e:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    :cond_2
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    if-nez v0, :cond_7

    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    if-nez v0, :cond_8

    move-object v0, v1

    :goto_1
    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwMethodName:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->N:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    if-nez v0, :cond_9

    move-object v0, v1

    :goto_2
    iget v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwLineNumber:I

    iput v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->L:I

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    if-nez v0, :cond_a

    move-object v0, v1

    :goto_3
    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwClassName:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->M:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    if-nez v0, :cond_b

    move-object v0, v1

    :goto_4
    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->O:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    if-nez v0, :cond_c

    move-object v0, v1

    :goto_5
    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionClassName:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->J:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    if-nez v0, :cond_d

    move-object v0, v1

    :goto_6
    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionMessage:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->P:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    if-nez v0, :cond_e

    :goto_7
    iget-object v0, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwFileName:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->K:Ljava/lang/String;

    :cond_3
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->d:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->R:Ljava/lang/String;

    :cond_4
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->g:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->Q:Ljava/lang/String;

    :cond_5
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->i:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    :cond_6
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->h:Lcom/google/android/gms/common/data/BitmapTeleporter;

    if-eqz v0, :cond_10

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->h:Lcom/google/android/gms/common/data/BitmapTeleporter;

    iput-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->T:Lcom/google/android/gms/common/data/BitmapTeleporter;

    iget-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->T:Lcom/google/android/gms/common/data/BitmapTeleporter;

    if-nez v2, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot set null temp directory"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    goto :goto_0

    :cond_8
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    goto :goto_1

    :cond_9
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    goto :goto_2

    :cond_a
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    goto :goto_3

    :cond_b
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    goto :goto_4

    :cond_c
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    goto :goto_5

    :cond_d
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    goto :goto_6

    :cond_e
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    iget-object v1, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    goto :goto_7

    :cond_f
    iput-object v2, v0, Lcom/google/android/gms/common/data/BitmapTeleporter;->d:Ljava/io/File;

    :cond_10
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_13

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/feedback/FileTeleporter;

    if-nez v2, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot set null temp directory"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    iput-object v2, v0, Lcom/google/android/gms/feedback/FileTeleporter;->e:Ljava/io/File;

    goto :goto_8

    :cond_12
    iget-object v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->j:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/gms/feedback/FileTeleporter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/feedback/FileTeleporter;

    iput-object v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->V:[Lcom/google/android/gms/feedback/FileTeleporter;

    :cond_13
    iget-boolean v0, p1, Lcom/google/android/gms/feedback/FeedbackOptions;->k:Z

    iput-boolean v0, v3, Lcom/google/android/gms/feedback/ErrorReport;->X:Z

    :cond_14
    return-object v3
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/internal/nj;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/ni;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/ab;Lcom/google/android/gms/common/internal/j;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v1, 0x645b68

    iget-object v2, p0, Lcom/google/android/gms/common/internal/e;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/ab;->m(Lcom/google/android/gms/common/internal/y;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/feedback/FeedbackOptions;)Z
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/nh;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/ni;

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/nh;->b(Lcom/google/android/gms/feedback/FeedbackOptions;)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/ni;->a(Lcom/google/android/gms/feedback/ErrorReport;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final j()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.feedback.internal.IFeedbackService"

    return-object v0
.end method

.method protected final k()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.feedback.internal.IFeedbackService"

    return-object v0
.end method
