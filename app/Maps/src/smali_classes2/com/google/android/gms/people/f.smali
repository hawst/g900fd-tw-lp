.class public final Lcom/google/android/gms/people/f;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/google/android/gms/common/api/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/h",
            "<",
            "Lcom/google/android/gms/internal/qb;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/android/gms/common/api/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/g",
            "<",
            "Lcom/google/android/gms/internal/qb;",
            "Lcom/google/android/gms/people/h;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lcom/google/android/gms/common/api/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a",
            "<",
            "Lcom/google/android/gms/people/h;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/google/android/gms/people/a;

.field public static final e:Lcom/google/android/gms/people/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/h;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/f;->a:Lcom/google/android/gms/common/api/h;

    new-instance v0, Lcom/google/android/gms/people/g;

    invoke-direct {v0}, Lcom/google/android/gms/people/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/f;->b:Lcom/google/android/gms/common/api/g;

    new-instance v0, Lcom/google/android/gms/common/api/a;

    sget-object v1, Lcom/google/android/gms/people/f;->b:Lcom/google/android/gms/common/api/g;

    sget-object v2, Lcom/google/android/gms/people/f;->a:Lcom/google/android/gms/common/api/h;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/a;-><init>(Lcom/google/android/gms/common/api/g;Lcom/google/android/gms/common/api/h;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/people/f;->c:Lcom/google/android/gms/common/api/a;

    new-instance v0, Lcom/google/android/gms/internal/pm;

    invoke-direct {v0}, Lcom/google/android/gms/internal/pm;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/qp;

    invoke-direct {v0}, Lcom/google/android/gms/internal/qp;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/f;->d:Lcom/google/android/gms/people/a;

    new-instance v0, Lcom/google/android/gms/internal/qs;

    invoke-direct {v0}, Lcom/google/android/gms/internal/qs;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/qt;

    invoke-direct {v0}, Lcom/google/android/gms/internal/qt;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/f;->e:Lcom/google/android/gms/people/d;

    new-instance v0, Lcom/google/android/gms/internal/rb;

    invoke-direct {v0}, Lcom/google/android/gms/internal/rb;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/qk;

    invoke-direct {v0}, Lcom/google/android/gms/internal/qk;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/qy;

    invoke-direct {v0}, Lcom/google/android/gms/internal/qy;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/qz;

    invoke-direct {v0}, Lcom/google/android/gms/internal/qz;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/qo;

    invoke-direct {v0}, Lcom/google/android/gms/internal/qo;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/ra;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ra;-><init>()V

    return-void
.end method
