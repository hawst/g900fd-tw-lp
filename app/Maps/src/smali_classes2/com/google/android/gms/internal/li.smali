.class public Lcom/google/android/gms/internal/li;
.super Lcom/google/android/gms/internal/kx;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/gz;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/internal/kx;-><init>(Lcom/google/android/gms/internal/gz;Z)V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 7

    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    const/4 v2, 0x1

    :try_start_0
    invoke-static {p0, p1, v1, v0, v2}, Lcom/google/android/gms/internal/kf;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/net/HttpURLConnection;Z)V

    const-string v1, "Cache-Control"

    const-string v2, "max-stale=3600"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-static {v1}, Lcom/google/android/gms/internal/kf;->a(Ljava/lang/Readable;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/webkit/WebResourceResponse;

    const-string v3, "application/javascript"

    const-string v4, "UTF-8"

    new-instance v5, Ljava/io/ByteArrayInputStream;

    const-string v6, "UTF-8"

    invoke-virtual {v1, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v3, v4, v5}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    return-object v2

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v1
.end method


# virtual methods
.method public shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 7

    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mraid.js"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-super {p0, p1, p2}, Lcom/google/android/gms/internal/kx;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    instance-of v1, p1, Lcom/google/android/gms/internal/gz;

    if-nez v1, :cond_1

    invoke-super {p0, p1, p2}, Lcom/google/android/gms/internal/kx;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v1

    goto :goto_0

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/internal/gz;

    move-object v1, v0

    iget-object v2, v1, Lcom/google/android/gms/internal/gz;->a:Lcom/google/android/gms/internal/kx;

    iget-object v3, v2, Lcom/google/android/gms/internal/kx;->c:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x0

    :try_start_1
    iput-boolean v4, v2, Lcom/google/android/gms/internal/kx;->h:Z

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/google/android/gms/internal/kx;->j:Z

    iget-object v4, v2, Lcom/google/android/gms/internal/kx;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v4}, Lcom/google/android/gms/internal/gz;->c()Lcom/google/android/gms/internal/du;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-static {}, Lcom/google/android/gms/internal/kt;->b()Z

    move-result v5

    if-nez v5, :cond_3

    sget-object v5, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    new-instance v6, Lcom/google/android/gms/internal/ky;

    invoke-direct {v6, v2, v4}, Lcom/google/android/gms/internal/ky;-><init>(Lcom/google/android/gms/internal/kx;Lcom/google/android/gms/internal/du;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->d()Lcom/google/android/gms/internal/bd;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/gms/internal/bd;->e:Z

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/li;->a:Lcom/google/android/gms/internal/gz;

    iget-object v2, v2, Lcom/google/android/gms/internal/gz;->e:Lcom/google/android/gms/internal/gx;

    iget-object v2, v2, Lcom/google/android/gms/internal/gx;->b:Ljava/lang/String;

    const-string v3, "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/mraid/v2/mraid_app_interstitial.js"

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/li;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v1

    goto :goto_0

    :cond_3
    :try_start_3
    iget-object v2, v4, Lcom/google/android/gms/internal/du;->i:Landroid/widget/RelativeLayout;

    iget-object v5, v4, Lcom/google/android/gms/internal/du;->d:Lcom/google/android/gms/internal/dz;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Lcom/google/android/gms/internal/du;->a(Z)V

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not fetch MRAID JS. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-super {p0, p1, p2}, Lcom/google/android/gms/internal/kx;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v1

    goto :goto_0

    :cond_4
    :try_start_5
    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->e()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/li;->a:Lcom/google/android/gms/internal/gz;

    iget-object v2, v2, Lcom/google/android/gms/internal/gz;->e:Lcom/google/android/gms/internal/gx;

    iget-object v2, v2, Lcom/google/android/gms/internal/gx;->b:Ljava/lang/String;

    const-string v3, "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/mraid/v2/mraid_app_expanded_banner.js"

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/li;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v1

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/li;->a:Lcom/google/android/gms/internal/gz;

    iget-object v2, v2, Lcom/google/android/gms/internal/gz;->e:Lcom/google/android/gms/internal/gx;

    iget-object v2, v2, Lcom/google/android/gms/internal/gx;->b:Ljava/lang/String;

    const-string v3, "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/mraid/v2/mraid_app_banner.js"

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/li;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    move-result-object v1

    goto/16 :goto_0
.end method
