.class final Lcom/google/android/gms/internal/qh;
.super Lcom/google/android/gms/internal/pp;


# instance fields
.field final synthetic a:Lcom/google/android/gms/internal/qb;

.field private final b:Lcom/google/android/gms/common/api/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/m",
            "<",
            "Lcom/google/android/gms/people/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/qb;Lcom/google/android/gms/common/api/m;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/m",
            "<",
            "Lcom/google/android/gms/people/c;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/internal/qh;->a:Lcom/google/android/gms/internal/qb;

    invoke-direct {p0}, Lcom/google/android/gms/internal/pp;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/internal/qh;->b:Lcom/google/android/gms/common/api/m;

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v3, 0x3

    const-string v1, "PeopleService"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    const-string v1, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Owner callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nresolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nholder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const-string v1, "PeopleService"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_0
    invoke-static {p1, v0, p2}, Lcom/google/android/gms/internal/qb;->a(ILjava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    if-nez p3, :cond_2

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/internal/qh;->a:Lcom/google/android/gms/internal/qb;

    new-instance v3, Lcom/google/android/gms/internal/qc;

    iget-object v4, p0, Lcom/google/android/gms/internal/qh;->a:Lcom/google/android/gms/internal/qb;

    iget-object v5, p0, Lcom/google/android/gms/internal/qh;->b:Lcom/google/android/gms/common/api/m;

    invoke-direct {v3, v4, v5, v1, v0}, Lcom/google/android/gms/internal/qc;-><init>(Lcom/google/android/gms/internal/qb;Lcom/google/android/gms/common/api/m;Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/people/model/b;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/internal/qb;->b(Lcom/google/android/gms/common/internal/h;)V

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/gms/people/model/b;

    invoke-direct {v0, p3}, Lcom/google/android/gms/people/model/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_1
.end method
