.class public abstract Lcom/google/android/gms/common/api/l;
.super Lcom/google/android/gms/common/api/j;

# interfaces
.implements Lcom/google/android/gms/common/api/af;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::",
        "Lcom/google/android/gms/common/api/u;",
        "A::",
        "Lcom/google/android/gms/common/api/f;",
        ">",
        "Lcom/google/android/gms/common/api/j",
        "<TR;>;",
        "Lcom/google/android/gms/common/api/af",
        "<TA;>;"
    }
.end annotation


# instance fields
.field private final d:Lcom/google/android/gms/common/api/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/h",
            "<TA;>;"
        }
    .end annotation
.end field

.field private e:Lcom/google/android/gms/common/api/ad;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/h;Lcom/google/android/gms/common/api/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/h",
            "<TA;>;",
            "Lcom/google/android/gms/common/api/o;",
            ")V"
        }
    .end annotation

    invoke-interface {p2}, Lcom/google/android/gms/common/api/o;->a()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/j;-><init>(Landroid/os/Looper;)V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/gms/common/api/h;

    iput-object p1, p0, Lcom/google/android/gms/common/api/l;->d:Lcom/google/android/gms/common/api/h;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/ad;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/common/api/l;->e:Lcom/google/android/gms/common/api/ad;

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/f;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)V"
        }
    .end annotation

    const/4 v4, 0x0

    const/16 v3, 0x8

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/l;->b(Lcom/google/android/gms/common/api/f;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v3, v2, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/api/l;->b(Lcom/google/android/gms/common/api/Status;)V

    throw v0

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/api/l;->b(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p1, Lcom/google/android/gms/common/api/Status;->g:I

    if-gtz v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    const-string v1, "Failed result must not be success"

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/l;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/u;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/l;->a(Lcom/google/android/gms/common/api/u;)V

    return-void
.end method

.method public abstract b(Lcom/google/android/gms/common/api/f;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)V"
        }
    .end annotation
.end method

.method protected final c()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/common/api/j;->c()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/l;->e:Lcom/google/android/gms/common/api/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/l;->e:Lcom/google/android/gms/common/api/ad;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/ad;->a(Lcom/google/android/gms/common/api/af;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/l;->e:Lcom/google/android/gms/common/api/ad;

    :cond_0
    return-void
.end method

.method public final d()Lcom/google/android/gms/common/api/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/common/api/h",
            "<TA;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/api/l;->d:Lcom/google/android/gms/common/api/h;

    return-object v0
.end method

.method public final e()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
