.class public Lcom/google/android/gms/internal/de;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/da;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# static fields
.field static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/de;->a:Ljava/util/Map;

    const-string v1, "resize"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/internal/de;->a:Ljava/util/Map;

    const-string v1, "playVideo"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/internal/de;->a:Ljava/util/Map;

    const-string v1, "storePicture"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/internal/de;->a:Ljava/util/Map;

    const-string v1, "createCalendarEvent"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/gz;Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/gz;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const v7, 0x7f0c0005

    const v6, 0x7f0c0004

    const/4 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "a"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/internal/de;->a:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    new-instance v3, Lcom/google/android/gms/internal/fa;

    invoke-direct {v3, p1, p2}, Lcom/google/android/gms/internal/fa;-><init>(Lcom/google/android/gms/internal/gz;Ljava/util/Map;)V

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->i:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->g:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->d()Lcom/google/android/gms/internal/bd;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/gms/internal/bd;->e:Z

    if-nez v0, :cond_0

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->g:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/kf;->d(Landroid/content/Context;)[I

    move-result-object v4

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->h:Ljava/util/Map;

    const-string v5, "width"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->h:Ljava/util/Map;

    const-string v5, "width"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/internal/kf;->b(Ljava/lang/String;)I

    move-result v0

    aget v5, v4, v2

    invoke-static {v0, v5}, Lcom/google/android/gms/internal/fa;->a(II)Z

    move-result v5

    if-eqz v5, :cond_1

    iput v0, v3, Lcom/google/android/gms/internal/fa;->b:I

    :cond_1
    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->h:Ljava/util/Map;

    const-string v5, "height"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->h:Ljava/util/Map;

    const-string v5, "height"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/internal/kf;->b(Ljava/lang/String;)I

    move-result v0

    aget v4, v4, v1

    invoke-static {v0, v4}, Lcom/google/android/gms/internal/fa;->b(II)Z

    move-result v4

    if-eqz v4, :cond_2

    iput v0, v3, Lcom/google/android/gms/internal/fa;->c:I

    :cond_2
    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->h:Ljava/util/Map;

    const-string v4, "offsetX"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->h:Ljava/util/Map;

    const-string v4, "offsetX"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/internal/kf;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, v3, Lcom/google/android/gms/internal/fa;->d:I

    :cond_3
    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->h:Ljava/util/Map;

    const-string v4, "offsetY"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->h:Ljava/util/Map;

    const-string v4, "offsetY"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/internal/kf;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, v3, Lcom/google/android/gms/internal/fa;->e:I

    :cond_4
    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->h:Ljava/util/Map;

    const-string v4, "allowOffscreen"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->h:Ljava/util/Map;

    const-string v4, "allowOffscreen"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/android/gms/internal/fa;->f:Z

    :cond_5
    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->h:Ljava/util/Map;

    const-string v4, "customClosePosition"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    sget-object v4, Lcom/google/android/gms/internal/fa;->a:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    :cond_6
    iget v0, v3, Lcom/google/android/gms/internal/fa;->b:I

    if-ltz v0, :cond_8

    iget v0, v3, Lcom/google/android/gms/internal/fa;->c:I

    if-ltz v0, :cond_8

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->i:Landroid/content/Context;

    const-string v4, "window"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v0, v3, Lcom/google/android/gms/internal/fa;->b:I

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/kt;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    add-int/lit8 v5, v0, 0x10

    iget v0, v3, Lcom/google/android/gms/internal/fa;->c:I

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/kt;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    add-int/lit8 v4, v0, 0x10

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->g:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_7

    instance-of v6, v0, Landroid/view/ViewGroup;

    if-eqz v6, :cond_7

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v6, v3, Lcom/google/android/gms/internal/fa;->g:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_7
    new-instance v6, Landroid/widget/LinearLayout;

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->i:Landroid/content/Context;

    invoke-direct {v6, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    new-instance v7, Landroid/widget/PopupWindow;

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->i:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    invoke-virtual {v7, v5}, Landroid/widget/PopupWindow;->setWidth(I)V

    iget-boolean v0, v3, Lcom/google/android/gms/internal/fa;->f:Z

    if-nez v0, :cond_9

    move v0, v1

    :goto_2
    invoke-virtual {v7, v0}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    invoke-virtual {v7, v6}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->g:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v6, v0, v8, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->i:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget v1, v3, Lcom/google/android/gms/internal/fa;->d:I

    iget v4, v3, Lcom/google/android/gms/internal/fa;->e:I

    invoke-virtual {v7, v0, v2, v1, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    iget-object v0, v3, Lcom/google/android/gms/internal/fa;->g:Lcom/google/android/gms/internal/gz;

    new-instance v1, Lcom/google/android/gms/internal/bd;

    iget-object v2, v3, Lcom/google/android/gms/internal/fa;->i:Landroid/content/Context;

    new-instance v4, Lcom/google/android/gms/ads/d;

    iget v5, v3, Lcom/google/android/gms/internal/fa;->b:I

    iget v6, v3, Lcom/google/android/gms/internal/fa;->c:I

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/ads/d;-><init>(II)V

    invoke-direct {v1, v2, v4}, Lcom/google/android/gms/internal/bd;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/d;)V

    iget-object v2, v0, Lcom/google/android/gms/internal/gz;->c:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iput-object v1, v0, Lcom/google/android/gms/internal/gz;->g:Lcom/google/android/gms/internal/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->requestLayout()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3}, Lcom/google/android/gms/internal/fa;->a()V

    :try_start_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "state"

    const-string v2, "resized"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, v3, Lcom/google/android/gms/internal/fa;->g:Lcom/google/android/gms/internal/gz;

    const-string v2, "onStateChanged"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/internal/gz;->b(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_0

    :cond_8
    move v0, v2

    goto/16 :goto_1

    :cond_9
    move v0, v2

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :pswitch_2
    new-instance v0, Lcom/google/android/gms/internal/ex;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/internal/ex;-><init>(Lcom/google/android/gms/internal/gz;Ljava/util/Map;)V

    new-instance v1, Lcom/google/android/gms/internal/bw;

    iget-object v2, v0, Lcom/google/android/gms/internal/ex;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/bw;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/google/android/gms/internal/bw;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, v0, Lcom/google/android/gms/internal/ex;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0c0006

    const-string v3, "Create calendar event"

    invoke-static {v2, v3}, Lcom/google/android/gms/internal/jt;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0c0007

    const-string v3, "Allow Ad to create a calendar event?"

    invoke-static {v2, v3}, Lcom/google/android/gms/internal/jt;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v2, "Accept"

    invoke-static {v6, v2}, Lcom/google/android/gms/internal/jt;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/internal/ey;

    invoke-direct {v3, v0}, Lcom/google/android/gms/internal/ey;-><init>(Lcom/google/android/gms/internal/ex;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const-string v2, "Decline"

    invoke-static {v7, v2}, Lcom/google/android/gms/internal/jt;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/internal/ez;

    invoke-direct {v3, v0}, Lcom/google/android/gms/internal/ez;-><init>(Lcom/google/android/gms/internal/ex;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    :pswitch_3
    new-instance v3, Lcom/google/android/gms/internal/fb;

    invoke-direct {v3, p1, p2}, Lcom/google/android/gms/internal/fb;-><init>(Lcom/google/android/gms/internal/gz;Ljava/util/Map;)V

    new-instance v0, Lcom/google/android/gms/internal/bw;

    iget-object v4, v3, Lcom/google/android/gms/internal/fb;->c:Landroid/content/Context;

    invoke-direct {v0, v4}, Lcom/google/android/gms/internal/bw;-><init>(Landroid/content/Context;)V

    const-string v4, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    iget-object v0, v0, Lcom/google/android/gms/internal/bw;->a:Landroid/content/Context;

    const-string v4, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v0, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_a

    :goto_3
    if-eqz v1, :cond_0

    iget-object v0, v3, Lcom/google/android/gms/internal/fb;->b:Ljava/util/Map;

    const-string v1, "iurl"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v3, Lcom/google/android/gms/internal/fb;->b:Ljava/util/Map;

    const-string v1, "iurl"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid image url:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_a
    move v1, v2

    goto :goto_3

    :cond_b
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/internal/kf;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v4, v3, Lcom/google/android/gms/internal/fb;->c:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0c0002

    const-string v5, "Save image"

    invoke-static {v4, v5}, Lcom/google/android/gms/internal/jt;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0c0003

    const-string v5, "Allow Ad to store image in Picture gallery?"

    invoke-static {v4, v5}, Lcom/google/android/gms/internal/jt;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v4, "Accept"

    invoke-static {v6, v4}, Lcom/google/android/gms/internal/jt;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/internal/fc;

    invoke-direct {v5, v3, v0, v1}, Lcom/google/android/gms/internal/fc;-><init>(Lcom/google/android/gms/internal/fb;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const-string v0, "Decline"

    invoke-static {v7, v0}, Lcom/google/android/gms/internal/jt;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/internal/fd;

    invoke-direct {v1, v3}, Lcom/google/android/gms/internal/fd;-><init>(Lcom/google/android/gms/internal/fb;)V

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
