.class Lcom/google/android/gms/internal/ec;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/internal/ea;

.field final synthetic b:Lcom/google/android/gms/internal/eb;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/eb;Lcom/google/android/gms/internal/ea;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/ec;->b:Lcom/google/android/gms/internal/eb;

    iput-object p2, p0, Lcom/google/android/gms/internal/ec;->a:Lcom/google/android/gms/internal/ea;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    iget-object v0, p0, Lcom/google/android/gms/internal/ec;->b:Lcom/google/android/gms/internal/eb;

    iget-object v13, v0, Lcom/google/android/gms/internal/eb;->e:Ljava/lang/Object;

    monitor-enter v13

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ec;->b:Lcom/google/android/gms/internal/eb;

    iget v0, v0, Lcom/google/android/gms/internal/eb;->h:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    monitor-exit v13

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ec;->b:Lcom/google/android/gms/internal/eb;

    iget-object v1, p0, Lcom/google/android/gms/internal/ec;->b:Lcom/google/android/gms/internal/eb;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/eb;->a()Lcom/google/android/gms/internal/el;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/internal/eb;->g:Lcom/google/android/gms/internal/el;

    iget-object v0, p0, Lcom/google/android/gms/internal/ec;->b:Lcom/google/android/gms/internal/eb;

    iget-object v0, v0, Lcom/google/android/gms/internal/eb;->g:Lcom/google/android/gms/internal/el;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/ec;->b:Lcom/google/android/gms/internal/eb;

    const/4 v1, 0x4

    iget-object v2, v0, Lcom/google/android/gms/internal/eb;->e:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iput v1, v0, Lcom/google/android/gms/internal/eb;->h:I

    iget-object v0, v0, Lcom/google/android/gms/internal/eb;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit v13

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/ec;->a:Lcom/google/android/gms/internal/ea;

    iget-object v1, p0, Lcom/google/android/gms/internal/ec;->b:Lcom/google/android/gms/internal/eb;

    iget-object v2, v0, Lcom/google/android/gms/internal/ea;->a:Ljava/lang/Object;

    monitor-enter v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iput-object v1, v0, Lcom/google/android/gms/internal/ea;->b:Lcom/google/android/gms/internal/ee;

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    iget-object v14, p0, Lcom/google/android/gms/internal/ec;->b:Lcom/google/android/gms/internal/eb;

    iget-object v5, p0, Lcom/google/android/gms/internal/ec;->a:Lcom/google/android/gms/internal/ea;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    iget-object v0, v14, Lcom/google/android/gms/internal/eb;->f:Lcom/google/android/gms/internal/gx;

    iget v0, v0, Lcom/google/android/gms/internal/gx;->d:I

    const v1, 0x3e8fa0

    if-ge v0, v1, :cond_3

    iget-object v0, v14, Lcom/google/android/gms/internal/eb;->c:Lcom/google/android/gms/internal/bd;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/bd;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, v14, Lcom/google/android/gms/internal/eb;->g:Lcom/google/android/gms/internal/el;

    iget-object v1, v14, Lcom/google/android/gms/internal/eb;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v1

    iget-object v2, v14, Lcom/google/android/gms/internal/eb;->b:Lcom/google/android/gms/internal/ba;

    iget-object v3, v14, Lcom/google/android/gms/internal/eb;->a:Lcom/google/android/gms/internal/ds;

    iget-object v3, v3, Lcom/google/android/gms/internal/ds;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v5}, Lcom/google/android/gms/internal/el;->a(Lcom/google/android/gms/a/o;Lcom/google/android/gms/internal/ba;Ljava/lang/String;Lcom/google/android/gms/internal/eo;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_1
    :try_start_8
    monitor-exit v13
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_0

    :catchall_2
    move-exception v0

    :try_start_9
    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_2
    :try_start_b
    iget-object v0, v14, Lcom/google/android/gms/internal/eb;->g:Lcom/google/android/gms/internal/el;

    iget-object v1, v14, Lcom/google/android/gms/internal/eb;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v1

    iget-object v2, v14, Lcom/google/android/gms/internal/eb;->c:Lcom/google/android/gms/internal/bd;

    iget-object v3, v14, Lcom/google/android/gms/internal/eb;->b:Lcom/google/android/gms/internal/ba;

    iget-object v4, v14, Lcom/google/android/gms/internal/eb;->a:Lcom/google/android/gms/internal/ds;

    iget-object v4, v4, Lcom/google/android/gms/internal/ds;->g:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/internal/el;->a(Lcom/google/android/gms/a/o;Lcom/google/android/gms/internal/bd;Lcom/google/android/gms/internal/ba;Ljava/lang/String;Lcom/google/android/gms/internal/eo;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    const/4 v0, 0x5

    :try_start_c
    iget-object v1, v14, Lcom/google/android/gms/internal/eb;->e:Ljava/lang/Object;

    monitor-enter v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :try_start_d
    iput v0, v14, Lcom/google/android/gms/internal/eb;->h:I

    iget-object v0, v14, Lcom/google/android/gms/internal/eb;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    goto :goto_1

    :catchall_3
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :try_start_e
    throw v0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :cond_3
    :try_start_f
    iget-object v0, v14, Lcom/google/android/gms/internal/eb;->c:Lcom/google/android/gms/internal/bd;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/bd;->e:Z

    if-eqz v0, :cond_4

    iget-object v0, v14, Lcom/google/android/gms/internal/eb;->g:Lcom/google/android/gms/internal/el;

    iget-object v1, v14, Lcom/google/android/gms/internal/eb;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v1

    iget-object v2, v14, Lcom/google/android/gms/internal/eb;->b:Lcom/google/android/gms/internal/ba;

    iget-object v3, v14, Lcom/google/android/gms/internal/eb;->a:Lcom/google/android/gms/internal/ds;

    iget-object v3, v3, Lcom/google/android/gms/internal/ds;->g:Ljava/lang/String;

    iget-object v4, v14, Lcom/google/android/gms/internal/eb;->a:Lcom/google/android/gms/internal/ds;

    iget-object v4, v4, Lcom/google/android/gms/internal/ds;->a:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/internal/el;->a(Lcom/google/android/gms/a/o;Lcom/google/android/gms/internal/ba;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/eo;)V

    goto :goto_1

    :cond_4
    iget-object v6, v14, Lcom/google/android/gms/internal/eb;->g:Lcom/google/android/gms/internal/el;

    iget-object v0, v14, Lcom/google/android/gms/internal/eb;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v7

    iget-object v8, v14, Lcom/google/android/gms/internal/eb;->c:Lcom/google/android/gms/internal/bd;

    iget-object v9, v14, Lcom/google/android/gms/internal/eb;->b:Lcom/google/android/gms/internal/ba;

    iget-object v0, v14, Lcom/google/android/gms/internal/eb;->a:Lcom/google/android/gms/internal/ds;

    iget-object v10, v0, Lcom/google/android/gms/internal/ds;->g:Ljava/lang/String;

    iget-object v0, v14, Lcom/google/android/gms/internal/eb;->a:Lcom/google/android/gms/internal/ds;

    iget-object v11, v0, Lcom/google/android/gms/internal/ds;->a:Ljava/lang/String;

    move-object v12, v5

    invoke-interface/range {v6 .. v12}, Lcom/google/android/gms/internal/el;->a(Lcom/google/android/gms/a/o;Lcom/google/android/gms/internal/bd;Lcom/google/android/gms/internal/ba;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/eo;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_f} :catch_0
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto :goto_1
.end method
