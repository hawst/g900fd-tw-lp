.class public final Lcom/google/android/gms/internal/eu;
.super Lcom/google/android/gms/internal/em;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<NETWORK_EXTRAS::",
        "Lcom/google/a/a/h;",
        "SERVER_PARAMETERS:",
        "Lcom/google/a/a/e;",
        ">",
        "Lcom/google/android/gms/internal/em;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/a/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/a/a/b",
            "<TNETWORK_EXTRAS;TSERVER_PARAMETERS;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/a/a/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TNETWORK_EXTRAS;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/a/a/b;Lcom/google/a/a/h;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/a/a/b",
            "<TNETWORK_EXTRAS;TSERVER_PARAMETERS;>;TNETWORK_EXTRAS;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/internal/em;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;

    iput-object p2, p0, Lcom/google/android/gms/internal/eu;->b:Lcom/google/a/a/h;

    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/google/a/a/e;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TSERVER_PARAMETERS;"
        }
    .end annotation

    if-eqz p1, :cond_0

    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/HashMap;

    invoke-virtual {v2}, Lorg/json/JSONObject;->length()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    move-object v1, v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;

    invoke-interface {v0}, Lcom/google/a/a/b;->b()Ljava/lang/Class;

    move-result-object v2

    const/4 v0, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/e;

    invoke-virtual {v0, v1}, Lcom/google/a/a/e;->a(Ljava/util/Map;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/a/o;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;

    instance-of v0, v0, Lcom/google/a/a/c;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediationAdapter is not a MediationBannerAdapter: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;

    check-cast v0, Lcom/google/a/a/c;

    invoke-interface {v0}, Lcom/google/a/a/c;->c()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/a/r;

    invoke-direct {v1, v0}, Lcom/google/android/gms/a/r;-><init>(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/a/o;Lcom/google/android/gms/internal/ba;Ljava/lang/String;Lcom/google/android/gms/internal/eo;)V
    .locals 6

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/internal/eu;->a(Lcom/google/android/gms/a/o;Lcom/google/android/gms/internal/ba;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/eo;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/a/o;Lcom/google/android/gms/internal/ba;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/eo;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;

    instance-of v0, v0, Lcom/google/a/a/d;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediationAdapter is not a MediationInterstitialAdapter: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;

    new-instance v0, Lcom/google/android/gms/internal/ev;

    invoke-direct {v0, p5}, Lcom/google/android/gms/internal/ev;-><init>(Lcom/google/android/gms/internal/eo;)V

    invoke-static {p1}, Lcom/google/android/gms/a/r;->a(Lcom/google/android/gms/a/o;)Ljava/lang/Object;

    iget v0, p2, Lcom/google/android/gms/internal/ba;->g:I

    invoke-direct {p0, p3}, Lcom/google/android/gms/internal/eu;->a(Ljava/lang/String;)Lcom/google/a/a/e;

    invoke-static {p2}, Lcom/google/android/gms/internal/ew;->a(Lcom/google/android/gms/internal/ba;)Lcom/google/a/a/a;

    iget-object v0, p0, Lcom/google/android/gms/internal/eu;->b:Lcom/google/a/a/h;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/a/o;Lcom/google/android/gms/internal/bd;Lcom/google/android/gms/internal/ba;Ljava/lang/String;Lcom/google/android/gms/internal/eo;)V
    .locals 7

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/internal/eu;->a(Lcom/google/android/gms/a/o;Lcom/google/android/gms/internal/bd;Lcom/google/android/gms/internal/ba;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/eo;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/a/o;Lcom/google/android/gms/internal/bd;Lcom/google/android/gms/internal/ba;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/eo;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;

    instance-of v0, v0, Lcom/google/a/a/c;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediationAdapter is not a MediationBannerAdapter: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;

    new-instance v0, Lcom/google/android/gms/internal/ev;

    invoke-direct {v0, p6}, Lcom/google/android/gms/internal/ev;-><init>(Lcom/google/android/gms/internal/eo;)V

    invoke-static {p1}, Lcom/google/android/gms/a/r;->a(Lcom/google/android/gms/a/o;)Ljava/lang/Object;

    iget v0, p3, Lcom/google/android/gms/internal/ba;->g:I

    invoke-direct {p0, p4}, Lcom/google/android/gms/internal/eu;->a(Ljava/lang/String;)Lcom/google/a/a/e;

    invoke-static {p2}, Lcom/google/android/gms/internal/ew;->a(Lcom/google/android/gms/internal/bd;)Lcom/google/a/c;

    invoke-static {p3}, Lcom/google/android/gms/internal/ew;->a(Lcom/google/android/gms/internal/ba;)Lcom/google/a/a/a;

    iget-object v0, p0, Lcom/google/android/gms/internal/eu;->b:Lcom/google/a/a/h;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;

    instance-of v0, v0, Lcom/google/a/a/d;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediationAdapter is not a MediationInterstitialAdapter: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
.end method

.method public final c()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/eu;->a:Lcom/google/a/a/b;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
.end method

.method public final d()V
    .locals 1

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
.end method

.method public final e()V
    .locals 1

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
.end method
