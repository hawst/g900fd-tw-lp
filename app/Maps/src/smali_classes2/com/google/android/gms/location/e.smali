.class public Lcom/google/android/gms/location/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/b;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/internal/oo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/oo;

    const-string v1, "location"

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/google/android/gms/internal/oo;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/location/e;->a:Lcom/google/android/gms/internal/oo;

    return-void
.end method


# virtual methods
.method public final a()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/e;->a:Lcom/google/android/gms/internal/oo;

    iget-object v0, v0, Lcom/google/android/gms/internal/oo;->h:Lcom/google/android/gms/internal/oi;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/oi;->a()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/f;)V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/location/e;->a:Lcom/google/android/gms/internal/oo;

    invoke-static {p1}, Lcom/google/android/gms/internal/ob;->a(Lcom/google/android/gms/location/LocationRequest;)Lcom/google/android/gms/internal/ob;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, v0, Lcom/google/android/gms/internal/oo;->h:Lcom/google/android/gms/internal/oi;

    monitor-enter v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/internal/oo;->h:Lcom/google/android/gms/internal/oi;

    iget-object v4, v0, Lcom/google/android/gms/internal/oi;->a:Lcom/google/android/gms/internal/or;

    invoke-interface {v4}, Lcom/google/android/gms/internal/or;->a()V

    invoke-virtual {v0, p2, v2}, Lcom/google/android/gms/internal/oi;->a(Lcom/google/android/gms/location/f;Landroid/os/Looper;)Lcom/google/android/gms/internal/ok;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/internal/oi;->a:Lcom/google/android/gms/internal/or;

    invoke-interface {v0}, Lcom/google/android/gms/internal/or;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/of;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/internal/of;->a(Lcom/google/android/gms/internal/ob;Lcom/google/android/gms/location/h;)V

    monitor-exit v3

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/location/f;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/location/e;->a:Lcom/google/android/gms/internal/oo;

    iget-object v1, v0, Lcom/google/android/gms/internal/oo;->h:Lcom/google/android/gms/internal/oi;

    iget-object v0, v1, Lcom/google/android/gms/internal/oi;->a:Lcom/google/android/gms/internal/or;

    invoke-interface {v0}, Lcom/google/android/gms/internal/or;->a()V

    const-string v0, "Invalid null listener"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    :try_start_1
    iget-object v2, v1, Lcom/google/android/gms/internal/oi;->c:Ljava/util/HashMap;

    monitor-enter v2
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    iget-object v0, v1, Lcom/google/android/gms/internal/oi;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/ok;

    if-eqz v0, :cond_1

    const/4 v3, 0x0

    iput-object v3, v0, Lcom/google/android/gms/internal/ok;->a:Landroid/os/Handler;

    iget-object v1, v1, Lcom/google/android/gms/internal/oi;->a:Lcom/google/android/gms/internal/or;

    invoke-interface {v1}, Lcom/google/android/gms/internal/or;->b()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/of;

    invoke-interface {v1, v0}, Lcom/google/android/gms/internal/of;->a(Lcom/google/android/gms/location/h;)V

    :cond_1
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
.end method

.method public final b()Lcom/google/android/gms/location/LocationStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/e;->a:Lcom/google/android/gms/internal/oo;

    iget-object v0, v0, Lcom/google/android/gms/internal/oo;->h:Lcom/google/android/gms/internal/oi;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/oi;->b()Lcom/google/android/gms/location/LocationStatus;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/e;->a:Lcom/google/android/gms/internal/oo;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/oo;->a()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/e;->a:Lcom/google/android/gms/internal/oo;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/oo;->b()V

    return-void
.end method

.method public final e()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/e;->a:Lcom/google/android/gms/internal/oo;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/oo;->l()Z

    move-result v0

    return v0
.end method
