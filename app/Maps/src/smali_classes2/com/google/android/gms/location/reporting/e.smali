.class public Lcom/google/android/gms/location/reporting/e;
.super Ljava/lang/Object;


# instance fields
.field final a:Landroid/accounts/Account;

.field final b:Ljava/lang/String;

.field final c:J

.field d:J

.field e:J


# direct methods
.method constructor <init>(Landroid/accounts/Account;Ljava/lang/String;J)V
    .locals 3

    const-wide v0, 0x7fffffffffffffffL

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/google/android/gms/location/reporting/e;->d:J

    iput-wide v0, p0, Lcom/google/android/gms/location/reporting/e;->e:J

    const-string v0, "account"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Landroid/accounts/Account;

    iput-object p1, p0, Lcom/google/android/gms/location/reporting/e;->a:Landroid/accounts/Account;

    const-string v0, "reason"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast p2, Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/location/reporting/e;->b:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/gms/location/reporting/e;->c:J

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/location/reporting/UploadRequest;
    .locals 1

    new-instance v0, Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-direct {v0, p0}, Lcom/google/android/gms/location/reporting/UploadRequest;-><init>(Lcom/google/android/gms/location/reporting/e;)V

    return-object v0
.end method

.method public final a(J)Lcom/google/android/gms/location/reporting/e;
    .locals 1

    iput-wide p1, p0, Lcom/google/android/gms/location/reporting/e;->d:J

    iput-wide p1, p0, Lcom/google/android/gms/location/reporting/e;->e:J

    return-object p0
.end method
