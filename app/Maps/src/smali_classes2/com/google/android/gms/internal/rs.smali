.class public Lcom/google/android/gms/internal/rs;
.super Lcom/google/android/gms/a/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/a/t",
        "<",
        "Lcom/google/android/gms/internal/rp;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Lcom/google/android/gms/internal/rs;


# direct methods
.method protected constructor <init>()V
    .locals 1

    const-string v0, "com.google.android.gms.wallet.dynamite.WalletDynamiteCreatorImpl"

    invoke-direct {p0, v0}, Lcom/google/android/gms/a/t;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/gms/a/l;Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;Lcom/google/android/gms/internal/rm;)Lcom/google/android/gms/internal/rj;
    .locals 2

    invoke-static {p0}, Lcom/google/android/gms/common/g;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/gms/common/e;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/e;-><init>(I)V

    throw v1

    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/gms/internal/rs;->a:Lcom/google/android/gms/internal/rs;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/rs;

    invoke-direct {v0}, Lcom/google/android/gms/internal/rs;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/rs;->a:Lcom/google/android/gms/internal/rs;

    :cond_1
    sget-object v0, Lcom/google/android/gms/internal/rs;->a:Lcom/google/android/gms/internal/rs;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/rs;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/rp;

    invoke-static {p0}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/google/android/gms/internal/rp;->a(Lcom/google/android/gms/a/o;Lcom/google/android/gms/a/l;Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;Lcom/google/android/gms/internal/rm;)Lcom/google/android/gms/internal/rj;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/a/u; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/internal/rq;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/rp;

    move-result-object v0

    return-object v0
.end method
