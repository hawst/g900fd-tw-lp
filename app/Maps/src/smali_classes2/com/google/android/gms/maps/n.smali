.class Lcom/google/android/gms/maps/n;
.super Lcom/google/android/gms/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/a/b",
        "<",
        "Lcom/google/android/gms/maps/l;",
        ">;"
    }
.end annotation


# instance fields
.field public d:Lcom/google/android/gms/a/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/a/s",
            "<",
            "Lcom/google/android/gms/maps/l;",
            ">;"
        }
    .end annotation
.end field

.field e:Landroid/app/Activity;

.field private final f:Landroid/app/Fragment;

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/maps/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/app/Fragment;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/a/b;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/maps/n;->g:Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/maps/n;->f:Landroid/app/Fragment;

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/a/s;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/a/s",
            "<",
            "Lcom/google/android/gms/maps/l;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/maps/n;->d:Lcom/google/android/gms/a/s;

    invoke-virtual {p0}, Lcom/google/android/gms/maps/n;->c()V

    return-void
.end method

.method public final c()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/maps/n;->e:Landroid/app/Activity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/maps/n;->d:Lcom/google/android/gms/a/s;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/a/b;->a:Lcom/google/android/gms/a/a;

    if-nez v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/n;->e:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/gms/maps/h;->a(Landroid/content/Context;)I

    iget-object v0, p0, Lcom/google/android/gms/maps/n;->e:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/gms/maps/internal/cv;->a(Landroid/content/Context;)Lcom/google/android/gms/maps/internal/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/maps/n;->e:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/am;->c(Lcom/google/android/gms/a/o;)Lcom/google/android/gms/maps/internal/v;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/maps/n;->d:Lcom/google/android/gms/a/s;

    new-instance v2, Lcom/google/android/gms/maps/l;

    iget-object v3, p0, Lcom/google/android/gms/maps/n;->f:Landroid/app/Fragment;

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/maps/l;-><init>(Landroid/app/Fragment;Lcom/google/android/gms/maps/internal/v;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/a/s;->a(Lcom/google/android/gms/a/a;)V

    iget-object v0, p0, Lcom/google/android/gms/maps/n;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/j;

    iget-object v1, p0, Lcom/google/android/gms/a/b;->a:Lcom/google/android/gms/a/a;

    check-cast v1, Lcom/google/android/gms/maps/l;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/common/e; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    iget-object v3, v1, Lcom/google/android/gms/maps/l;->a:Lcom/google/android/gms/maps/internal/v;

    new-instance v4, Lcom/google/android/gms/maps/m;

    invoke-direct {v4, v1, v0}, Lcom/google/android/gms/maps/m;-><init>(Lcom/google/android/gms/maps/l;Lcom/google/android/gms/maps/j;)V

    invoke-interface {v3, v4}, Lcom/google/android/gms/maps/internal/v;->a(Lcom/google/android/gms/maps/internal/co;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/common/e; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v1, Lcom/google/android/gms/maps/model/d;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/d;-><init>(Landroid/os/RemoteException;)V

    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/common/e; {:try_start_2 .. :try_end_2} :catch_2

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/d;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/d;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/maps/n;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/gms/common/e; {:try_start_3 .. :try_end_3} :catch_2

    :cond_1
    :goto_1
    return-void

    :catch_2
    move-exception v0

    goto :goto_1
.end method
