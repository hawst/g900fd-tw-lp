.class public final Lcom/google/android/gms/feedback/a;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/google/android/gms/common/api/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/google/android/gms/common/api/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/h",
            "<",
            "Lcom/google/android/gms/internal/nh;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lcom/google/android/gms/common/api/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/g",
            "<",
            "Lcom/google/android/gms/internal/nh;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/h;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/a;->b:Lcom/google/android/gms/common/api/h;

    new-instance v0, Lcom/google/android/gms/feedback/b;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/a;->c:Lcom/google/android/gms/common/api/g;

    new-instance v0, Lcom/google/android/gms/common/api/a;

    sget-object v1, Lcom/google/android/gms/feedback/a;->c:Lcom/google/android/gms/common/api/g;

    sget-object v2, Lcom/google/android/gms/feedback/a;->b:Lcom/google/android/gms/common/api/h;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/a;-><init>(Lcom/google/android/gms/common/api/g;Lcom/google/android/gms/common/api/h;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/feedback/a;->a:Lcom/google/android/gms/common/api/a;

    return-void
.end method

.method static synthetic a()Lcom/google/android/gms/common/api/h;
    .locals 1

    sget-object v0, Lcom/google/android/gms/feedback/a;->b:Lcom/google/android/gms/common/api/h;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/api/o;Landroid/graphics/Bitmap;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/android/gms/common/api/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/o;",
            "Landroid/graphics/Bitmap;",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/gms/common/api/s",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lcom/google/android/gms/feedback/c;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/feedback/c;-><init>(Lcom/google/android/gms/common/api/o;Landroid/graphics/Bitmap;Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
