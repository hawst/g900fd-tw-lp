.class public interface abstract Lcom/google/android/gms/internal/pv;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(Landroid/net/Uri;)Landroid/os/Bundle;
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;J)Landroid/os/Bundle;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;JZ)Landroid/os/Bundle;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;JZZ)Landroid/os/Bundle;
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Lcom/google/android/gms/common/data/DataHolder;IIJ)Lcom/google/android/gms/common/internal/u;
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Lcom/google/android/gms/internal/pl;Lcom/google/android/gms/internal/pu;)Lcom/google/android/gms/common/internal/u;
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Lcom/google/android/gms/people/model/AvatarReference;Lcom/google/android/gms/internal/qg;)Lcom/google/android/gms/common/internal/u;
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;I)Lcom/google/android/gms/common/internal/u;
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/gms/common/internal/u;
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;JZ)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Landroid/os/Bundle;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Lcom/google/android/gms/internal/pl;Ljava/util/List;Lcom/google/android/gms/internal/ps;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/pq;",
            "Lcom/google/android/gms/internal/pl;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/internal/ps;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;II)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;II)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;Z)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;ZII)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/pq;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJ)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/pq;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZJ)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/pq;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZJ",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;II)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/pq;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZJ",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;III)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/pq;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZJ",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/pq;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/internal/jz;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/pq;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/internal/jz;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZII)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Z[Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;ZZLjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/pq;ZZLjava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a()Z
.end method

.method public abstract b(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.end method

.method public abstract b(Lcom/google/android/gms/internal/pq;JZ)Lcom/google/android/gms/common/internal/u;
.end method

.method public abstract b(Lcom/google/android/gms/internal/pq;Ljava/lang/String;)Lcom/google/android/gms/common/internal/u;
.end method

.method public abstract b(Lcom/google/android/gms/internal/pq;Ljava/lang/String;II)Lcom/google/android/gms/common/internal/u;
.end method

.method public abstract b(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/gms/common/internal/u;
.end method

.method public abstract b(Lcom/google/android/gms/internal/pq;Landroid/os/Bundle;)V
.end method

.method public abstract b(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract b(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract b(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract b(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract c(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/internal/u;
.end method

.method public abstract c(Lcom/google/android/gms/internal/pq;Ljava/lang/String;Ljava/lang/String;)V
.end method
