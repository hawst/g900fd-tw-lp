.class public final Lcom/google/android/gms/smart_profile/HeaderView;
.super Landroid/widget/FrameLayout;


# instance fields
.field private a:Lcom/google/android/gms/smart_profile/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/smart_profile/HeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/smart_profile/a;->b(Landroid/content/Context;)Lcom/google/android/gms/smart_profile/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/HeaderView;->a:Lcom/google/android/gms/smart_profile/h;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/HeaderView;->a:Lcom/google/android/gms/smart_profile/h;

    invoke-interface {v0}, Lcom/google/android/gms/smart_profile/h;->a()Lcom/google/android/gms/a/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/a/r;->a(Lcom/google/android/gms/a/o;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/HeaderView;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Lcom/google/android/gms/a/u; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected final onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/HeaderView;->a:Lcom/google/android/gms/smart_profile/h;

    invoke-interface {v0}, Lcom/google/android/gms/smart_profile/h;->b()Lcom/google/android/gms/smart_profile/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/smart_profile/b;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
