.class public interface abstract Lcom/google/android/gms/internal/of;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()Landroid/location/Location;
.end method

.method public abstract a(Ljava/lang/String;)Landroid/location/Location;
.end method

.method public abstract a(JZLandroid/app/PendingIntent;)V
.end method

.method public abstract a(Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Landroid/app/PendingIntent;Lcom/google/android/gms/internal/oa;Ljava/lang/String;)V
.end method

.method public abstract a(Landroid/location/Location;)V
.end method

.method public abstract a(Landroid/location/Location;I)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/oa;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/ob;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/ob;Lcom/google/android/gms/location/h;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/on;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/GeofencingRequest;Landroid/app/PendingIntent;Lcom/google/android/gms/internal/oa;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/h;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/h;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/h;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/internal/on;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/on;Lcom/google/android/gms/internal/os;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceReport;Lcom/google/android/gms/internal/on;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/internal/on;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/UserAddedPlace;Lcom/google/android/gms/internal/on;Lcom/google/android/gms/internal/os;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/UserDataType;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/util/List;Lcom/google/android/gms/internal/on;Lcom/google/android/gms/internal/os;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/location/places/UserDataType;",
            "Lcom/google/android/gms/maps/model/LatLngBounds;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/internal/on;",
            "Lcom/google/android/gms/internal/os;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/on;Lcom/google/android/gms/internal/os;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLngBounds;ILcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/on;Lcom/google/android/gms/internal/os;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLngBounds;ILjava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/on;Lcom/google/android/gms/internal/os;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/internal/on;Lcom/google/android/gms/internal/os;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/internal/on;Lcom/google/android/gms/internal/os;)V
.end method

.method public abstract a(Ljava/util/List;Landroid/app/PendingIntent;Lcom/google/android/gms/internal/oa;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/internal/od;",
            ">;",
            "Landroid/app/PendingIntent;",
            "Lcom/google/android/gms/internal/oa;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/List;Lcom/google/android/gms/internal/on;Lcom/google/android/gms/internal/os;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/internal/on;",
            "Lcom/google/android/gms/internal/os;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Z)V
.end method

.method public abstract a([Ljava/lang/String;Lcom/google/android/gms/internal/oa;Ljava/lang/String;)V
.end method

.method public abstract b()Landroid/os/IBinder;
.end method

.method public abstract b(Ljava/lang/String;)Lcom/google/android/gms/location/LocationStatus;
.end method

.method public abstract b(Landroid/app/PendingIntent;)V
.end method

.method public abstract b(Lcom/google/android/gms/internal/on;Landroid/app/PendingIntent;)V
.end method

.method public abstract b(Ljava/lang/String;Lcom/google/android/gms/internal/on;Lcom/google/android/gms/internal/os;)V
.end method

.method public abstract c()Landroid/os/IBinder;
.end method
