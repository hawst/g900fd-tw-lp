.class Lcom/google/android/gms/maps/g;
.super Lcom/google/android/gms/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/a/b",
        "<",
        "Lcom/google/android/gms/maps/e;",
        ">;"
    }
.end annotation


# instance fields
.field public d:Lcom/google/android/gms/a/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/a/s",
            "<",
            "Lcom/google/android/gms/maps/e;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/view/ViewGroup;

.field private final f:Landroid/content/Context;

.field private final g:Lcom/google/android/gms/maps/GoogleMapOptions;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/maps/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/a/b;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/maps/g;->h:Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/maps/g;->e:Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/google/android/gms/maps/g;->f:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/gms/maps/g;->g:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/a/s;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/a/s",
            "<",
            "Lcom/google/android/gms/maps/e;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/maps/g;->d:Lcom/google/android/gms/a/s;

    iget-object v1, p0, Lcom/google/android/gms/maps/g;->d:Lcom/google/android/gms/a/s;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/a/b;->a:Lcom/google/android/gms/a/a;

    if-nez v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/maps/g;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/maps/internal/cv;->a(Landroid/content/Context;)Lcom/google/android/gms/maps/internal/am;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/maps/g;->f:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/maps/g;->g:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/maps/internal/am;->a(Lcom/google/android/gms/a/o;Lcom/google/android/gms/maps/GoogleMapOptions;)Lcom/google/android/gms/maps/internal/m;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/maps/g;->d:Lcom/google/android/gms/a/s;

    new-instance v3, Lcom/google/android/gms/maps/e;

    iget-object v4, p0, Lcom/google/android/gms/maps/g;->e:Landroid/view/ViewGroup;

    invoke-direct {v3, v4, v1}, Lcom/google/android/gms/maps/e;-><init>(Landroid/view/ViewGroup;Lcom/google/android/gms/maps/internal/m;)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/a/s;->a(Lcom/google/android/gms/a/a;)V

    iget-object v1, p0, Lcom/google/android/gms/maps/g;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/maps/i;

    move-object v2, v0

    iget-object v1, p0, Lcom/google/android/gms/a/b;->a:Lcom/google/android/gms/a/a;

    check-cast v1, Lcom/google/android/gms/maps/e;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/common/e; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    iget-object v4, v1, Lcom/google/android/gms/maps/e;->a:Lcom/google/android/gms/maps/internal/m;

    new-instance v5, Lcom/google/android/gms/maps/f;

    invoke-direct {v5, v1, v2}, Lcom/google/android/gms/maps/f;-><init>(Lcom/google/android/gms/maps/e;Lcom/google/android/gms/maps/i;)V

    invoke-interface {v4, v5}, Lcom/google/android/gms/maps/internal/m;->a(Lcom/google/android/gms/maps/internal/bq;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/common/e; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    new-instance v2, Lcom/google/android/gms/maps/model/d;

    invoke-direct {v2, v1}, Lcom/google/android/gms/maps/model/d;-><init>(Landroid/os/RemoteException;)V

    throw v2
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/common/e; {:try_start_2 .. :try_end_2} :catch_2

    :catch_1
    move-exception v1

    new-instance v2, Lcom/google/android/gms/maps/model/d;

    invoke-direct {v2, v1}, Lcom/google/android/gms/maps/model/d;-><init>(Landroid/os/RemoteException;)V

    throw v2

    :cond_0
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/maps/g;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/gms/common/e; {:try_start_3 .. :try_end_3} :catch_2

    :cond_1
    :goto_1
    return-void

    :catch_2
    move-exception v1

    goto :goto_1
.end method
