.class public Lcom/google/android/gms/car/CarPhoneStatus$CarCall;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/car/CarPhoneStatus$CarCall;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:I

.field public b:I

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/fg;

    invoke-direct {v0}, Lcom/google/android/gms/car/fg;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->a:I

    return-void
.end method

.method public constructor <init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->a:I

    iput p2, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->b:I

    iput-object p4, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->d:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->c:I

    iput-object p5, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->g:[B

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/gms/car/fg;->a(Lcom/google/android/gms/car/CarPhoneStatus$CarCall;Landroid/os/Parcel;)V

    return-void
.end method
