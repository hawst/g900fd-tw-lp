.class public Lcom/google/android/gms/internal/fg;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/internal/gz;

.field final b:Lcom/google/android/gms/internal/bw;

.field c:Landroid/util/DisplayMetrics;

.field d:F

.field e:I

.field f:I

.field g:I

.field h:I

.field i:I

.field j:[I

.field private final k:Landroid/content/Context;

.field private final l:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/gz;Landroid/content/Context;Lcom/google/android/gms/internal/bw;)V
    .locals 5

    const/4 v4, 0x0

    const/high16 v3, 0x43200000    # 160.0f

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/gms/internal/fg;->e:I

    iput v0, p0, Lcom/google/android/gms/internal/fg;->f:I

    iput v0, p0, Lcom/google/android/gms/internal/fg;->h:I

    iput v0, p0, Lcom/google/android/gms/internal/fg;->i:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/gms/internal/fg;->j:[I

    iput-object p1, p0, Lcom/google/android/gms/internal/fg;->a:Lcom/google/android/gms/internal/gz;

    iput-object p2, p0, Lcom/google/android/gms/internal/fg;->k:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/gms/internal/fg;->b:Lcom/google/android/gms/internal/bw;

    const-string v0, "window"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/gms/internal/fg;->l:Landroid/view/WindowManager;

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/fg;->c:Landroid/util/DisplayMetrics;

    iget-object v0, p0, Lcom/google/android/gms/internal/fg;->l:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/fg;->c:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/fg;->c:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/google/android/gms/internal/fg;->d:F

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/fg;->g:I

    iget-object v0, p0, Lcom/google/android/gms/internal/fg;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/kf;->c(Landroid/content/Context;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/internal/fg;->c:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v1, v1

    div-float v1, v3, v1

    iget-object v2, p0, Lcom/google/android/gms/internal/fg;->c:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    mul-float/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/internal/fg;->e:I

    iget-object v2, p0, Lcom/google/android/gms/internal/fg;->c:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v0, v2, v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/fg;->f:I

    iget-object v0, p0, Lcom/google/android/gms/internal/fg;->a:Lcom/google/android/gms/internal/gz;

    iget-object v1, p0, Lcom/google/android/gms/internal/fg;->j:[I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gz;->getLocationOnScreen([I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/fg;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0, v4, v4}, Lcom/google/android/gms/internal/gz;->measure(II)V

    iget-object v0, p0, Lcom/google/android/gms/internal/fg;->c:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    div-float v0, v3, v0

    iget-object v1, p0, Lcom/google/android/gms/internal/fg;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/internal/fg;->h:I

    iget-object v1, p0, Lcom/google/android/gms/internal/fg;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/fg;->i:I

    return-void
.end method
