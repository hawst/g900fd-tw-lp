.class public Lcom/google/android/gms/car/CarMediaBrowserRootNode;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/car/CarMediaBrowserRootNode;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:I

.field public b:Ljava/lang/String;

.field public c:[Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/fa;

    invoke-direct {v0}, Lcom/google/android/gms/car/fa;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->a:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->a:I

    iput-object p2, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->c:[Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/fa;->a(Lcom/google/android/gms/car/CarMediaBrowserRootNode;Landroid/os/Parcel;I)V

    return-void
.end method
