.class final Lcom/google/android/gms/wearable/internal/ar;
.super Lcom/google/android/gms/wearable/internal/ap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/wearable/internal/ap",
        "<",
        "Lcom/google/android/gms/wearable/d;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/m",
            "<",
            "Lcom/google/android/gms/wearable/d;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/gms/wearable/internal/ap;-><init>(Lcom/google/android/gms/common/api/m;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wearable/internal/p;)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/wearable/internal/bg;

    iget v1, p1, Lcom/google/android/gms/wearable/internal/p;->b:I

    new-instance v2, Lcom/google/android/gms/common/api/Status;

    invoke-static {v1}, Lcom/google/android/gms/wearable/z;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    iget v1, p1, Lcom/google/android/gms/wearable/internal/p;->c:I

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wearable/internal/bg;-><init>(Lcom/google/android/gms/common/api/Status;I)V

    iget-object v1, p0, Lcom/google/android/gms/wearable/internal/ap;->a:Lcom/google/android/gms/common/api/m;

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wearable/internal/ap;->a:Lcom/google/android/gms/common/api/m;

    :cond_0
    return-void
.end method
