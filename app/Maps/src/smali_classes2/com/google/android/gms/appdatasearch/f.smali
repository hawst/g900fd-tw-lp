.class public final Lcom/google/android/gms/appdatasearch/f;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/google/android/gms/appdatasearch/DocumentId;

.field private b:J

.field private c:I

.field private d:Lcom/google/android/gms/appdatasearch/DocumentContents;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/appdatasearch/f;->b:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/appdatasearch/f;->c:I

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/appdatasearch/UsageInfo;
    .locals 7

    new-instance v0, Lcom/google/android/gms/appdatasearch/UsageInfo;

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/f;->a:Lcom/google/android/gms/appdatasearch/DocumentId;

    iget-wide v2, p0, Lcom/google/android/gms/appdatasearch/f;->b:J

    iget v4, p0, Lcom/google/android/gms/appdatasearch/f;->c:I

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/gms/appdatasearch/f;->d:Lcom/google/android/gms/appdatasearch/DocumentContents;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/UsageInfo;-><init>(Lcom/google/android/gms/appdatasearch/DocumentId;JILjava/lang/String;Lcom/google/android/gms/appdatasearch/DocumentContents;)V

    return-object v0
.end method

.method public final a(I)Lcom/google/android/gms/appdatasearch/f;
    .locals 0

    iput p1, p0, Lcom/google/android/gms/appdatasearch/f;->c:I

    return-object p0
.end method

.method public final a(J)Lcom/google/android/gms/appdatasearch/f;
    .locals 1

    iput-wide p1, p0, Lcom/google/android/gms/appdatasearch/f;->b:J

    return-object p0
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/DocumentContents;)Lcom/google/android/gms/appdatasearch/f;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/f;->d:Lcom/google/android/gms/appdatasearch/DocumentContents;

    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/f;
    .locals 2

    new-instance v0, Lcom/google/android/gms/appdatasearch/DocumentId;

    const-string v1, ""

    invoke-direct {v0, p1, v1, p2}, Lcom/google/android/gms/appdatasearch/DocumentId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/f;->a:Lcom/google/android/gms/appdatasearch/DocumentId;

    return-object p0
.end method
