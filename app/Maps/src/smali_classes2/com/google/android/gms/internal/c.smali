.class public Lcom/google/android/gms/internal/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/b;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/internal/jq;

.field private final b:Lcom/google/android/gms/internal/gz;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/jq;Lcom/google/android/gms/internal/gz;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/c;->a:Lcom/google/android/gms/internal/jq;

    iput-object p2, p0, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/gz;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "https"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "//pagead2.googlesyndication.com/pagead/gen_204"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "id"

    const-string v2, "gmob-apps-blocked-navigation"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "navigationURL"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/c;->a:Lcom/google/android/gms/internal/jq;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/internal/c;->a:Lcom/google/android/gms/internal/jq;

    iget-object v1, v1, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/internal/c;->a:Lcom/google/android/gms/internal/jq;

    iget-object v1, v1, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget-object v1, v1, Lcom/google/android/gms/internal/fo;->o:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "debugDialog"

    iget-object v2, p0, Lcom/google/android/gms/internal/c;->a:Lcom/google/android/gms/internal/jq;

    iget-object v2, v2, Lcom/google/android/gms/internal/jq;->b:Lcom/google/android/gms/internal/fo;

    iget-object v2, v2, Lcom/google/android/gms/internal/fo;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/gz;

    iget-object v2, v2, Lcom/google/android/gms/internal/gz;->e:Lcom/google/android/gms/internal/gx;

    iget-object v2, v2, Lcom/google/android/gms/internal/gx;->b:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/internal/kf;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
