.class public Lcom/google/android/gms/location/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/b;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/internal/oo;


# virtual methods
.method public final a(JLandroid/app/PendingIntent;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/location/a;->a:Lcom/google/android/gms/internal/oo;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/internal/oo;->a(JLandroid/app/PendingIntent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/location/a;->a:Lcom/google/android/gms/internal/oo;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/oo;->a(Landroid/app/PendingIntent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
