.class public final Lcom/google/android/gms/internal/bo;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/bv;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:Ljava/lang/String;

.field public final k:I

.field public final l:Ljava/lang/String;

.field public final m:I

.field public final n:I

.field public final o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/bv;

    invoke-direct {v0}, Lcom/google/android/gms/internal/bv;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/bo;->CREATOR:Lcom/google/android/gms/internal/bv;

    return-void
.end method

.method constructor <init>(IIIIIIIIILjava/lang/String;ILjava/lang/String;IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/bo;->a:I

    iput p2, p0, Lcom/google/android/gms/internal/bo;->b:I

    iput p3, p0, Lcom/google/android/gms/internal/bo;->c:I

    iput p4, p0, Lcom/google/android/gms/internal/bo;->d:I

    iput p5, p0, Lcom/google/android/gms/internal/bo;->e:I

    iput p6, p0, Lcom/google/android/gms/internal/bo;->f:I

    iput p7, p0, Lcom/google/android/gms/internal/bo;->g:I

    iput p8, p0, Lcom/google/android/gms/internal/bo;->h:I

    iput p9, p0, Lcom/google/android/gms/internal/bo;->i:I

    iput-object p10, p0, Lcom/google/android/gms/internal/bo;->j:Ljava/lang/String;

    iput p11, p0, Lcom/google/android/gms/internal/bo;->k:I

    iput-object p12, p0, Lcom/google/android/gms/internal/bo;->l:Ljava/lang/String;

    iput p13, p0, Lcom/google/android/gms/internal/bo;->m:I

    iput p14, p0, Lcom/google/android/gms/internal/bo;->n:I

    iput-object p15, p0, Lcom/google/android/gms/internal/bo;->o:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/ads/search/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/bo;->a:I

    iget v0, p1, Lcom/google/android/gms/ads/search/a;->a:I

    iput v0, p0, Lcom/google/android/gms/internal/bo;->b:I

    iget v0, p1, Lcom/google/android/gms/ads/search/a;->b:I

    iput v0, p0, Lcom/google/android/gms/internal/bo;->c:I

    iget v0, p1, Lcom/google/android/gms/ads/search/a;->c:I

    iput v0, p0, Lcom/google/android/gms/internal/bo;->d:I

    iget v0, p1, Lcom/google/android/gms/ads/search/a;->d:I

    iput v0, p0, Lcom/google/android/gms/internal/bo;->e:I

    iget v0, p1, Lcom/google/android/gms/ads/search/a;->e:I

    iput v0, p0, Lcom/google/android/gms/internal/bo;->f:I

    iget v0, p1, Lcom/google/android/gms/ads/search/a;->f:I

    iput v0, p0, Lcom/google/android/gms/internal/bo;->g:I

    iget v0, p1, Lcom/google/android/gms/ads/search/a;->g:I

    iput v0, p0, Lcom/google/android/gms/internal/bo;->h:I

    iget v0, p1, Lcom/google/android/gms/ads/search/a;->h:I

    iput v0, p0, Lcom/google/android/gms/internal/bo;->i:I

    iget-object v0, p1, Lcom/google/android/gms/ads/search/a;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/bo;->j:Ljava/lang/String;

    iget v0, p1, Lcom/google/android/gms/ads/search/a;->j:I

    iput v0, p0, Lcom/google/android/gms/internal/bo;->k:I

    iget-object v0, p1, Lcom/google/android/gms/ads/search/a;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/bo;->l:Ljava/lang/String;

    iget v0, p1, Lcom/google/android/gms/ads/search/a;->l:I

    iput v0, p0, Lcom/google/android/gms/internal/bo;->m:I

    iget v0, p1, Lcom/google/android/gms/ads/search/a;->m:I

    iput v0, p0, Lcom/google/android/gms/internal/bo;->n:I

    iget-object v0, p1, Lcom/google/android/gms/ads/search/a;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/bo;->o:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/gms/internal/bv;->a(Lcom/google/android/gms/internal/bo;Landroid/os/Parcel;)V

    return-void
.end method
