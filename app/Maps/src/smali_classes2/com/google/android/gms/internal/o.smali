.class Lcom/google/android/gms/internal/o;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/x;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/google/android/gms/internal/n;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/n;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/o;->b:Lcom/google/android/gms/internal/n;

    iput-object p2, p0, Lcom/google/android/gms/internal/o;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/internal/o;->b:Lcom/google/android/gms/internal/n;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/n;->a(Lcom/google/android/gms/internal/n;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/internal/o;->b:Lcom/google/android/gms/internal/n;

    iget-object v1, p0, Lcom/google/android/gms/internal/o;->a:Landroid/view/View;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v0, Lcom/google/android/gms/internal/n;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->drainTo(Ljava/util/Collection;)I

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/o;->b:Lcom/google/android/gms/internal/n;

    iget-object v1, v0, Lcom/google/android/gms/internal/n;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lcom/google/android/gms/internal/n;->e:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    monitor-exit v1

    :goto_1
    return-void

    :cond_1
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/gms/internal/r;

    invoke-direct {v3, v0}, Lcom/google/android/gms/internal/r;-><init>(Lcom/google/android/gms/internal/n;)V

    iput-object v3, v0, Lcom/google/android/gms/internal/n;->e:Landroid/content/BroadcastReceiver;

    iget-object v3, v0, Lcom/google/android/gms/internal/n;->b:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/android/gms/internal/n;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
