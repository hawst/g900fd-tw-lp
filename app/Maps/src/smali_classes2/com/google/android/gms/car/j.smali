.class final Lcom/google/android/gms/car/j;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/car/d;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/o;Landroid/content/Intent;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/car/a;->a(Lcom/google/android/gms/common/api/o;)V

    sget-object v0, Lcom/google/android/gms/car/a;->a:Lcom/google/android/gms/common/api/h;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/h;)Lcom/google/android/gms/common/api/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/eq;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/car/eq;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/o;)Z
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/car/a;->a(Lcom/google/android/gms/common/api/o;)V

    sget-object v0, Lcom/google/android/gms/car/a;->a:Lcom/google/android/gms/common/api/h;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/h;)Lcom/google/android/gms/common/api/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/eq;

    invoke-virtual {v0}, Lcom/google/android/gms/car/eq;->c()Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/common/api/o;)Lcom/google/android/gms/car/CarUiInfo;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/car/a;->a(Lcom/google/android/gms/common/api/o;)V

    sget-object v0, Lcom/google/android/gms/car/a;->a:Lcom/google/android/gms/common/api/h;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/h;)Lcom/google/android/gms/common/api/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/eq;

    invoke-virtual {v0}, Lcom/google/android/gms/car/eq;->e()Lcom/google/android/gms/car/CarUiInfo;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/o;)Lcom/google/android/gms/car/aq;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/car/a;->a(Lcom/google/android/gms/common/api/o;)V

    sget-object v0, Lcom/google/android/gms/car/a;->a:Lcom/google/android/gms/common/api/h;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/o;->a(Lcom/google/android/gms/common/api/h;)Lcom/google/android/gms/common/api/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/eq;

    invoke-virtual {v0}, Lcom/google/android/gms/car/eq;->f()Lcom/google/android/gms/car/aq;

    move-result-object v0

    return-object v0
.end method
