.class public final Lcom/google/android/gms/internal/bt;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lcom/google/android/gms/internal/eh;

.field public final b:Lcom/google/android/gms/internal/az;

.field public c:Lcom/google/android/gms/ads/a;

.field public d:Lcom/google/android/gms/internal/bh;

.field public e:[Lcom/google/android/gms/ads/d;

.field public f:Ljava/lang/String;

.field public g:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/gms/internal/az;->a()Lcom/google/android/gms/internal/az;

    move-result-object v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/gms/internal/bt;-><init>(Landroid/view/ViewGroup;Landroid/util/AttributeSet;ZLcom/google/android/gms/internal/az;)V

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Landroid/util/AttributeSet;Z)V
    .locals 1

    invoke-static {}, Lcom/google/android/gms/internal/az;->a()Lcom/google/android/gms/internal/az;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/internal/bt;-><init>(Landroid/view/ViewGroup;Landroid/util/AttributeSet;ZLcom/google/android/gms/internal/az;)V

    return-void
.end method

.method private constructor <init>(Landroid/view/ViewGroup;Landroid/util/AttributeSet;ZLcom/google/android/gms/internal/az;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/bt;-><init>(Landroid/view/ViewGroup;Landroid/util/AttributeSet;ZLcom/google/android/gms/internal/az;Lcom/google/android/gms/internal/bh;)V

    return-void
.end method

.method private constructor <init>(Landroid/view/ViewGroup;Landroid/util/AttributeSet;ZLcom/google/android/gms/internal/az;Lcom/google/android/gms/internal/bh;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/eh;

    invoke-direct {v0}, Lcom/google/android/gms/internal/eh;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/bt;->a:Lcom/google/android/gms/internal/eh;

    iput-object p1, p0, Lcom/google/android/gms/internal/bt;->g:Landroid/view/ViewGroup;

    iput-object p4, p0, Lcom/google/android/gms/internal/bt;->b:Lcom/google/android/gms/internal/az;

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    :try_start_0
    new-instance v0, Lcom/google/android/gms/internal/bg;

    invoke-direct {v0, v1, p2}, Lcom/google/android/gms/internal/bg;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    if-nez p3, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/internal/bg;->a:[Lcom/google/android/gms/ads/d;

    array-length v2, v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "The adSizes XML attribute is only allowed on PublisherAdViews."

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v2, Lcom/google/android/gms/internal/bd;

    sget-object v3, Lcom/google/android/gms/ads/d;->a:Lcom/google/android/gms/ads/d;

    invoke-direct {v2, v1, v3}, Lcom/google/android/gms/internal/bd;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/d;)V

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v2, v1, v0}, Lcom/google/android/gms/internal/kt;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/internal/bd;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v2, v0, Lcom/google/android/gms/internal/bg;->a:[Lcom/google/android/gms/ads/d;

    iput-object v2, p0, Lcom/google/android/gms/internal/bt;->e:[Lcom/google/android/gms/ads/d;

    iget-object v0, v0, Lcom/google/android/gms/internal/bg;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/bt;->f:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/bd;

    iget-object v2, p0, Lcom/google/android/gms/internal/bt;->e:[Lcom/google/android/gms/ads/d;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/bd;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/d;)V

    const-string v1, "Ads by Google"

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/internal/kt;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/internal/bd;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iput-object p5, p0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/ads/d;
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    invoke-interface {v0}, Lcom/google/android/gms/internal/bh;->k()Lcom/google/android/gms/internal/bd;

    move-result-object v0

    iget v1, v0, Lcom/google/android/gms/internal/bd;->f:I

    iget v2, v0, Lcom/google/android/gms/internal/bd;->c:I

    iget-object v3, v0, Lcom/google/android/gms/internal/bd;->b:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/ads/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/ads/d;-><init>(IILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->e:[Lcom/google/android/gms/ads/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->e:[Lcom/google/android/gms/ads/d;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/ads/a;)V
    .locals 2

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/internal/bt;->c:Lcom/google/android/gms/ads/a;

    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    if-eqz p1, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/aw;

    invoke-direct {v0, p1}, Lcom/google/android/gms/internal/aw;-><init>(Lcom/google/android/gms/ads/a;)V

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/gms/internal/bh;->a(Lcom/google/android/gms/internal/bc;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/internal/br;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->e:[Lcom/google/android/gms/ads/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad size and ad unit ID must be set before loadAd is called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/internal/bd;

    iget-object v2, p0, Lcom/google/android/gms/internal/bt;->e:[Lcom/google/android/gms/ads/d;

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/internal/bd;-><init>(Landroid/content/Context;[Lcom/google/android/gms/ads/d;)V

    iget-object v2, p0, Lcom/google/android/gms/internal/bt;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/internal/bt;->a:Lcom/google/android/gms/internal/eh;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/internal/ax;->a(Landroid/content/Context;Lcom/google/android/gms/internal/bd;Ljava/lang/String;Lcom/google/android/gms/internal/eh;)Lcom/google/android/gms/internal/bh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->c:Lcom/google/android/gms/ads/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    new-instance v1, Lcom/google/android/gms/internal/aw;

    iget-object v2, p0, Lcom/google/android/gms/internal/bt;->c:Lcom/google/android/gms/ads/a;

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/aw;-><init>(Lcom/google/android/gms/ads/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/bh;->a(Lcom/google/android/gms/internal/bc;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/internal/bt;->b()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    iget-object v1, p0, Lcom/google/android/gms/internal/bt;->b:Lcom/google/android/gms/internal/az;

    iget-object v1, p0, Lcom/google/android/gms/internal/bt;->g:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/gms/internal/az;->a(Landroid/content/Context;Lcom/google/android/gms/internal/br;)Lcom/google/android/gms/internal/ba;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/bh;->a(Lcom/google/android/gms/internal/ba;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->a:Lcom/google/android/gms/internal/eh;

    iget-object v1, p1, Lcom/google/android/gms/internal/br;->i:Ljava/util/Map;

    iput-object v1, v0, Lcom/google/android/gms/internal/eh;->a:Ljava/util/Map;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method public final varargs a([Lcom/google/android/gms/ads/d;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/gms/internal/bt;->e:[Lcom/google/android/gms/ads/d;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    new-instance v1, Lcom/google/android/gms/internal/bd;

    iget-object v2, p0, Lcom/google/android/gms/internal/bt;->g:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/internal/bt;->e:[Lcom/google/android/gms/ads/d;

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/internal/bd;-><init>(Landroid/content/Context;[Lcom/google/android/gms/ads/d;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/bh;->a(Lcom/google/android/gms/internal/bd;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/bt;->d:Lcom/google/android/gms/internal/bh;

    invoke-interface {v0}, Lcom/google/android/gms/internal/bh;->c()Lcom/google/android/gms/a/o;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/bt;->g:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/gms/a/r;->a(Lcom/google/android/gms/a/o;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
