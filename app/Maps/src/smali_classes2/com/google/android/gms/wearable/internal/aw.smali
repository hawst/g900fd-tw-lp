.class public Lcom/google/android/gms/wearable/internal/aw;
.super Lcom/google/android/gms/common/internal/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/e",
        "<",
        "Lcom/google/android/gms/wearable/internal/k;",
        ">;"
    }
.end annotation


# instance fields
.field final g:Ljava/util/concurrent/ExecutorService;

.field private final h:Lcom/google/android/gms/wearable/internal/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/wearable/internal/o",
            "<",
            "Lcom/google/android/gms/wearable/c;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/google/android/gms/wearable/internal/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/wearable/internal/o",
            "<",
            "Lcom/google/android/gms/wearable/j;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/google/android/gms/wearable/internal/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/wearable/internal/o",
            "<",
            "Lcom/google/android/gms/wearable/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/q;Lcom/google/android/gms/common/api/r;)V
    .locals 6

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/e;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/q;Lcom/google/android/gms/common/api/r;[Ljava/lang/String;)V

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wearable/internal/aw;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/google/android/gms/wearable/internal/q;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/internal/q;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/internal/aw;->h:Lcom/google/android/gms/wearable/internal/o;

    new-instance v0, Lcom/google/android/gms/wearable/internal/s;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/internal/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/internal/aw;->i:Lcom/google/android/gms/wearable/internal/o;

    new-instance v0, Lcom/google/android/gms/wearable/internal/u;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/internal/u;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/internal/aw;->j:Lcom/google/android/gms/wearable/internal/o;

    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/wearable/internal/l;->a(Landroid/os/IBinder;)Lcom/google/android/gms/wearable/internal/k;

    move-result-object v0

    return-object v0
.end method

.method protected final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "WearableClient"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onPostInitHandler: statusCode "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wearable/internal/aw;->h:Lcom/google/android/gms/wearable/internal/o;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/wearable/internal/o;->a(Landroid/os/IBinder;)V

    iget-object v0, p0, Lcom/google/android/gms/wearable/internal/aw;->i:Lcom/google/android/gms/wearable/internal/o;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/wearable/internal/o;->a(Landroid/os/IBinder;)V

    iget-object v0, p0, Lcom/google/android/gms/wearable/internal/aw;->j:Lcom/google/android/gms/wearable/internal/o;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/wearable/internal/o;->a(Landroid/os/IBinder;)V

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/common/internal/e;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/internal/ab;Lcom/google/android/gms/common/internal/j;)V
    .locals 2

    const v0, 0x645b68

    iget-object v1, p0, Lcom/google/android/gms/common/internal/e;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/gms/common/internal/ab;->e(Lcom/google/android/gms/common/internal/y;ILjava/lang/String;)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wearable/internal/aw;->h:Lcom/google/android/gms/wearable/internal/o;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wearable/internal/o;->a(Lcom/google/android/gms/wearable/internal/aw;)V

    iget-object v0, p0, Lcom/google/android/gms/wearable/internal/aw;->i:Lcom/google/android/gms/wearable/internal/o;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wearable/internal/o;->a(Lcom/google/android/gms/wearable/internal/aw;)V

    iget-object v0, p0, Lcom/google/android/gms/wearable/internal/aw;->j:Lcom/google/android/gms/wearable/internal/o;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wearable/internal/o;->a(Lcom/google/android/gms/wearable/internal/aw;)V

    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->b()V

    return-void
.end method

.method protected final j()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.wearable.BIND"

    return-object v0
.end method

.method protected final k()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.wearable.internal.IWearableService"

    return-object v0
.end method
