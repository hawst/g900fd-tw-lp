.class public Lcom/google/android/gms/internal/ac;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/w;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/internal/gz;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/gx;)V
    .locals 6

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/google/android/gms/internal/bd;

    invoke-direct {v1}, Lcom/google/android/gms/internal/bd;-><init>()V

    const/4 v4, 0x0

    move-object v0, p1

    move v3, v2

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/gz;->a(Landroid/content/Context;Lcom/google/android/gms/internal/bd;ZZLcom/google/android/gms/internal/ne;Lcom/google/android/gms/internal/gx;)Lcom/google/android/gms/internal/gz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/ac;->a:Lcom/google/android/gms/internal/gz;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ac;->a:Lcom/google/android/gms/internal/gz;

    invoke-static {v0}, Lcom/google/android/gms/internal/kf;->a(Landroid/webkit/WebView;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/internal/sq;Lcom/google/android/gms/internal/fp;Lcom/google/android/gms/internal/cp;Lcom/google/android/gms/internal/ft;ZLcom/google/android/gms/internal/db;)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/gms/internal/ac;->a:Lcom/google/android/gms/internal/gz;

    iget-object v0, v0, Lcom/google/android/gms/internal/gz;->a:Lcom/google/android/gms/internal/kx;

    new-instance v7, Lcom/google/android/gms/internal/a;

    const/4 v1, 0x0

    invoke-direct {v7, v1}, Lcom/google/android/gms/internal/a;-><init>(Z)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/internal/kx;->a(Lcom/google/android/gms/internal/sq;Lcom/google/android/gms/internal/fp;Lcom/google/android/gms/internal/cp;Lcom/google/android/gms/internal/ft;ZLcom/google/android/gms/internal/db;Lcom/google/android/gms/internal/a;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/internal/x;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/ac;->a:Lcom/google/android/gms/internal/gz;

    iget-object v0, v0, Lcom/google/android/gms/internal/gz;->a:Lcom/google/android/gms/internal/kx;

    new-instance v1, Lcom/google/android/gms/internal/ag;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/internal/ag;-><init>(Lcom/google/android/gms/internal/ac;Lcom/google/android/gms/internal/x;)V

    iput-object v1, v0, Lcom/google/android/gms/internal/kx;->f:Lcom/google/android/gms/internal/kz;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/af;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/internal/af;-><init>(Lcom/google/android/gms/internal/ac;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/internal/kt;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/internal/da;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ac;->a:Lcom/google/android/gms/internal/gz;

    iget-object v0, v0, Lcom/google/android/gms/internal/gz;->a:Lcom/google/android/gms/internal/kx;

    iget-object v0, v0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/ae;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/internal/ae;-><init>(Lcom/google/android/gms/internal/ac;Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {}, Lcom/google/android/gms/internal/kt;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/google/android/gms/internal/kt;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ac;->a:Lcom/google/android/gms/internal/gz;

    invoke-static {v0}, Lcom/google/android/gms/internal/kf;->b(Landroid/webkit/WebView;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/ac;->a:Lcom/google/android/gms/internal/gz;

    iget-object v0, v0, Lcom/google/android/gms/internal/gz;->a:Lcom/google/android/gms/internal/kx;

    const/4 v1, 0x0

    iget-object v0, v0, Lcom/google/android/gms/internal/kx;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ac;->a:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->destroy()V

    return-void
.end method
