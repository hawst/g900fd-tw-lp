.class public Lcom/google/android/gms/internal/oo;
.super Lcom/google/android/gms/internal/nx;


# instance fields
.field public final h:Lcom/google/android/gms/internal/oi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/internal/nx;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/internal/oi;

    iget-object v1, p0, Lcom/google/android/gms/internal/oo;->g:Lcom/google/android/gms/internal/or;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/internal/oi;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/or;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/oo;->h:Lcom/google/android/gms/internal/oi;

    iget-object v0, p0, Lcom/google/android/gms/internal/oo;->g:Lcom/google/android/gms/internal/or;

    new-instance v1, Lcom/google/android/gms/internal/nv;

    invoke-direct {v1, p1, v2, v2, v0}, Lcom/google/android/gms/internal/nv;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/or;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/oo;->g:Lcom/google/android/gms/internal/or;

    new-instance v1, Lcom/google/android/gms/internal/ly;

    invoke-direct {v1, p1, v0}, Lcom/google/android/gms/internal/ly;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/or;)V

    return-void
.end method


# virtual methods
.method public final a(JLandroid/app/PendingIntent;)V
    .locals 5

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->l()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-ltz v0, :cond_2

    move v0, v1

    :goto_0
    const-string v2, "detectionIntervalMillis must be >= 0"

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/internal/oo;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/of;

    invoke-interface {v0, p1, p2, v1, p3}, Lcom/google/android/gms/internal/of;->a(JZLandroid/app/PendingIntent;)V

    return-void
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/e;->l()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/oo;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/of;

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/of;->a(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public final b()V
    .locals 6

    iget-object v2, p0, Lcom/google/android/gms/internal/oo;->h:Lcom/google/android/gms/internal/oi;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/oo;->l()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/internal/oo;->h:Lcom/google/android/gms/internal/oi;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v4, v3, Lcom/google/android/gms/internal/oi;->c:Ljava/util/HashMap;

    monitor-enter v4
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v0, v3, Lcom/google/android/gms/internal/oi;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/ok;

    if-eqz v0, :cond_0

    iget-object v1, v3, Lcom/google/android/gms/internal/oi;->a:Lcom/google/android/gms/internal/or;

    invoke-interface {v1}, Lcom/google/android/gms/internal/or;->b()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/of;

    invoke-interface {v1, v0}, Lcom/google/android/gms/internal/of;->a(Lcom/google/android/gms/location/h;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catch_0
    move-exception v0

    :try_start_5
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_1
    move-exception v0

    :cond_1
    :goto_1
    :try_start_6
    invoke-super {p0}, Lcom/google/android/gms/internal/nx;->b()V

    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    return-void

    :cond_2
    :try_start_7
    iget-object v0, v3, Lcom/google/android/gms/internal/oi;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    iget-object v1, p0, Lcom/google/android/gms/internal/oo;->h:Lcom/google/android/gms/internal/oi;

    iget-boolean v0, v1, Lcom/google/android/gms/internal/oi;->b:Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    if-eqz v0, :cond_1

    const/4 v3, 0x0

    :try_start_9
    iget-object v0, v1, Lcom/google/android/gms/internal/oi;->a:Lcom/google/android/gms/internal/or;

    invoke-interface {v0}, Lcom/google/android/gms/internal/or;->a()V

    iget-object v0, v1, Lcom/google/android/gms/internal/oi;->a:Lcom/google/android/gms/internal/or;

    invoke-interface {v0}, Lcom/google/android/gms/internal/or;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/of;

    invoke-interface {v0, v3}, Lcom/google/android/gms/internal/of;->a(Z)V

    iput-boolean v3, v1, Lcom/google/android/gms/internal/oi;->b:Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_a
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_b
    monitor-exit v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    throw v0
.end method
