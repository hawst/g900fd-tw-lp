.class public Lcom/google/android/gms/internal/fn;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/gms/internal/dw;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Lcom/google/android/gms/internal/dw;Landroid/os/Parcel;I)V
    .locals 6

    const/4 v0, 0x1

    const/4 v5, 0x4

    const/4 v1, 0x0

    const v2, -0xb0bb

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    iget v3, p0, Lcom/google/android/gms/internal/dw;->a:I

    invoke-static {p1, v0, v5}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;II)V

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/gms/internal/dw;->b:Lcom/google/android/gms/internal/dt;

    invoke-static {p1, v3, v4, p2, v1}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/gms/internal/dw;->c:Lcom/google/android/gms/internal/sq;

    invoke-static {v4}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/a/o;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {p1, v3, v4, v1}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    iget-object v3, p0, Lcom/google/android/gms/internal/dw;->d:Lcom/google/android/gms/internal/fp;

    invoke-static {v3}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/a/o;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {p1, v5, v3, v1}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/gms/internal/dw;->e:Lcom/google/android/gms/internal/gz;

    invoke-static {v4}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/a/o;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {p1, v3, v4, v1}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/gms/internal/dw;->f:Lcom/google/android/gms/internal/cp;

    invoke-static {v4}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/a/o;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {p1, v3, v4, v1}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/gms/internal/dw;->g:Ljava/lang/String;

    invoke-static {p1, v3, v4, v1}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v3, 0x8

    iget-boolean v4, p0, Lcom/google/android/gms/internal/dw;->h:Z

    invoke-static {p1, v3, v5}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;II)V

    if-eqz v4, :cond_0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v0, 0x9

    iget-object v3, p0, Lcom/google/android/gms/internal/dw;->i:Ljava/lang/String;

    invoke-static {p1, v0, v3, v1}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v0, 0xa

    iget-object v3, p0, Lcom/google/android/gms/internal/dw;->j:Lcom/google/android/gms/internal/ft;

    invoke-static {v3}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/a/o;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {p1, v0, v3, v1}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/16 v0, 0xb

    iget v3, p0, Lcom/google/android/gms/internal/dw;->k:I

    invoke-static {p1, v0, v5}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;II)V

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v0, 0xc

    iget v3, p0, Lcom/google/android/gms/internal/dw;->l:I

    invoke-static {p1, v0, v5}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;II)V

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v0, 0xd

    iget-object v3, p0, Lcom/google/android/gms/internal/dw;->m:Ljava/lang/String;

    invoke-static {p1, v0, v3, v1}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v0, 0xe

    iget-object v3, p0, Lcom/google/android/gms/internal/dw;->n:Lcom/google/android/gms/internal/gx;

    invoke-static {p1, v0, v3, p2, v1}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v0, 0xf

    iget-object v3, p0, Lcom/google/android/gms/internal/dw;->o:Lcom/google/android/gms/internal/db;

    invoke-static {v3}, Lcom/google/android/gms/a/r;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/o;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/a/o;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {p1, v0, v3, v1}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/16 v0, 0x11

    iget-object v3, p0, Lcom/google/android/gms/internal/dw;->q:Lcom/google/android/gms/internal/ad;

    invoke-static {p1, v0, v3, p2, v1}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v0, 0x10

    iget-object v3, p0, Lcom/google/android/gms/internal/dw;->p:Ljava/lang/String;

    invoke-static {p1, v0, v3, v1}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    invoke-static {p1, v2}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Landroid/os/Parcel;I)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 22

    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/safeparcel/a;->a(Landroid/os/Parcel;)I

    move-result v20

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    move/from16 v0, v20

    if-ge v2, v0, :cond_1

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const v21, 0xffff

    and-int v21, v21, v2

    packed-switch v21, :pswitch_data_0

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/a;->a(Landroid/os/Parcel;I)V

    goto :goto_0

    :pswitch_0
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/common/internal/safeparcel/a;->a(Landroid/os/Parcel;II)V

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    goto :goto_0

    :pswitch_1
    sget-object v4, Lcom/google/android/gms/internal/dt;->CREATOR:Lcom/google/android/gms/internal/fi;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v4}, Lcom/google/android/gms/common/internal/safeparcel/a;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/internal/dt;

    move-object v4, v2

    goto :goto_0

    :pswitch_2
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/a;->d(Landroid/os/Parcel;I)Landroid/os/IBinder;

    move-result-object v5

    goto :goto_0

    :pswitch_3
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/a;->d(Landroid/os/Parcel;I)Landroid/os/IBinder;

    move-result-object v6

    goto :goto_0

    :pswitch_4
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/a;->d(Landroid/os/Parcel;I)Landroid/os/IBinder;

    move-result-object v7

    goto :goto_0

    :pswitch_5
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/a;->d(Landroid/os/Parcel;I)Landroid/os/IBinder;

    move-result-object v8

    goto :goto_0

    :pswitch_6
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/a;->c(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    :pswitch_7
    const/4 v10, 0x4

    move-object/from16 v0, p1

    invoke-static {v0, v2, v10}, Lcom/google/android/gms/common/internal/safeparcel/a;->a(Landroid/os/Parcel;II)V

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v10, 0x1

    goto :goto_0

    :cond_0
    const/4 v10, 0x0

    goto :goto_0

    :pswitch_8
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/a;->c(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    :pswitch_9
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/a;->d(Landroid/os/Parcel;I)Landroid/os/IBinder;

    move-result-object v12

    goto :goto_0

    :pswitch_a
    const/4 v13, 0x4

    move-object/from16 v0, p1

    invoke-static {v0, v2, v13}, Lcom/google/android/gms/common/internal/safeparcel/a;->a(Landroid/os/Parcel;II)V

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v13

    goto :goto_0

    :pswitch_b
    const/4 v14, 0x4

    move-object/from16 v0, p1

    invoke-static {v0, v2, v14}, Lcom/google/android/gms/common/internal/safeparcel/a;->a(Landroid/os/Parcel;II)V

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v14

    goto/16 :goto_0

    :pswitch_c
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/a;->c(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_0

    :pswitch_d
    sget-object v16, Lcom/google/android/gms/internal/gx;->CREATOR:Lcom/google/android/gms/internal/ku;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/common/internal/safeparcel/a;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/internal/gx;

    move-object/from16 v16, v2

    goto/16 :goto_0

    :pswitch_e
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/a;->d(Landroid/os/Parcel;I)Landroid/os/IBinder;

    move-result-object v17

    goto/16 :goto_0

    :pswitch_f
    sget-object v19, Lcom/google/android/gms/internal/ad;->CREATOR:Lcom/google/android/gms/internal/f;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/common/internal/safeparcel/a;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/internal/ad;

    move-object/from16 v19, v2

    goto/16 :goto_0

    :pswitch_10
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/a;->c(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_0

    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    move/from16 v0, v20

    if-eq v2, v0, :cond_2

    new-instance v2, Lcom/google/android/gms/common/internal/safeparcel/b;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Overread allowed size end="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/common/internal/safeparcel/b;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v2

    :cond_2
    new-instance v2, Lcom/google/android/gms/internal/dw;

    invoke-direct/range {v2 .. v19}, Lcom/google/android/gms/internal/dw;-><init>(ILcom/google/android/gms/internal/dt;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Ljava/lang/String;ZLjava/lang/String;Landroid/os/IBinder;IILjava/lang/String;Lcom/google/android/gms/internal/gx;Landroid/os/IBinder;Ljava/lang/String;Lcom/google/android/gms/internal/ad;)V

    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_10
        :pswitch_f
    .end packed-switch
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/internal/dw;

    return-object v0
.end method
