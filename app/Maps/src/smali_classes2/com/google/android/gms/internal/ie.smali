.class Lcom/google/android/gms/internal/ie;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/internal/hw;

.field final synthetic b:Lcom/google/android/gms/internal/ia;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/ia;Lcom/google/android/gms/internal/hw;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/ie;->b:Lcom/google/android/gms/internal/ia;

    iput-object p2, p0, Lcom/google/android/gms/internal/ie;->a:Lcom/google/android/gms/internal/hw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/ie;->b:Lcom/google/android/gms/internal/ia;

    iget-object v6, v0, Lcom/google/android/gms/internal/ia;->c:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ie;->b:Lcom/google/android/gms/internal/ia;

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    iget v0, v0, Lcom/google/android/gms/internal/fo;->e:I

    const/4 v2, -0x2

    if-eq v0, v2, :cond_0

    monitor-exit v6

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ie;->b:Lcom/google/android/gms/internal/ia;

    iget-object v0, v0, Lcom/google/android/gms/internal/ia;->b:Lcom/google/android/gms/internal/gz;

    iget-object v0, v0, Lcom/google/android/gms/internal/gz;->a:Lcom/google/android/gms/internal/kx;

    iget-object v2, p0, Lcom/google/android/gms/internal/ie;->b:Lcom/google/android/gms/internal/ia;

    iput-object v2, v0, Lcom/google/android/gms/internal/kx;->f:Lcom/google/android/gms/internal/kz;

    iget-object v0, p0, Lcom/google/android/gms/internal/ie;->a:Lcom/google/android/gms/internal/hw;

    iget-object v2, p0, Lcom/google/android/gms/internal/ie;->b:Lcom/google/android/gms/internal/ia;

    iget-object v2, v2, Lcom/google/android/gms/internal/ia;->d:Lcom/google/android/gms/internal/fo;

    new-instance v3, Lcom/google/android/gms/internal/lk;

    iget-object v4, v0, Lcom/google/android/gms/internal/hw;->c:Lcom/google/android/gms/internal/gz;

    iget-object v5, v2, Lcom/google/android/gms/internal/fo;->q:Ljava/lang/String;

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/gms/internal/lk;-><init>(Lcom/google/android/gms/internal/hw;Lcom/google/android/gms/internal/gz;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/google/android/gms/internal/hw;->c:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/internal/gz;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, v0, Lcom/google/android/gms/internal/hw;->c:Lcom/google/android/gms/internal/gz;

    iget-object v3, v2, Lcom/google/android/gms/internal/fo;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    iget-object v2, v2, Lcom/google/android/gms/internal/fo;->c:Ljava/lang/String;

    const-string v3, "text/html"

    const-string v4, "UTF-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/internal/gz;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v1, v2, Lcom/google/android/gms/internal/fo;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/internal/kf;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_1
.end method
